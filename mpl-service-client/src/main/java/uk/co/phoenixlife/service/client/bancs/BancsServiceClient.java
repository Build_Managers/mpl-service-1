/*
 * BancsServiceClient.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.client.bancs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dto.MPLException;
import uk.co.phoenixlife.service.dto.bancspost.BancsCalcCashInTaxRequest;
import uk.co.phoenixlife.service.dto.dashboard.PremiumSummariesResponse;
import uk.co.phoenixlife.service.dto.dashboard.ValuationsResponse;
import uk.co.phoenixlife.service.dto.exception.PolicyNotFoundException;
import uk.co.phoenixlife.service.dto.response.BancsPolicyExistResponse;
import uk.co.phoenixlife.service.dto.response.ErrorResponseDTO;
import uk.co.phoenixlife.service.dto.response.PolicyDetailResponse;
import uk.co.phoenixlife.service.utils.CommonUtils;

/**
 * BancsServiceClient.java
 */
@Component
public class BancsServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @Value("${undertow.rest}")
    private String undertowUrl;

    @Value("${bancsPolicyExistsCheckAPI}")
    private String bancsPolicyExistsCheckAPI;

    @Value("${bancsPartyBasicDetailAPI}")
    private String bancsPartyBasicDetailAPI;

    @Value("${bancsIDAndVDataAPI}")
    private String bancsIDAndVDataAPI;

    @Value("${bancsPartyCommDetailsAPI}")
    private String bancsPartyCommDetailAPI;

    @Value("${bancsPoliciesOwnedByAPartyAPI}")
    private String policiesOwnedByAPartyAPI;

    @Value("${bancsPolDetailsAPI}")
    private String bancsPolDetailsAPI;

    @Value("${bancsClaimListAPI}")
    private String bancsClaimListAPI;

    @Value("${bancsRetQuoteValAPI}")
    private String bancsRetQuoteValAPI;

    @Value("${bancsPolicySummaryAPI}")
    private String bancsPolicySummaryAPI;

    @Value("${bancsValuationsAPI}")
    private String bancsValuationsAPI;

    @Value("${bancsPremiumSummariesAPI}")
    private String bancsPremiumSummariesAPI;

    @Value("${bancsCalcCashInTaxAPI}")
    private String bancsCalcCashInTaxAPI;

    @Value("${bancsPartyBasicDetailByPartyNoAPI}")
    private String partyBasicDetailByPartyNoAPI;

    private static final Logger LOGGER = LoggerFactory.getLogger(BancsServiceClient.class);

    @Cacheable(value = "policiesOwnedByAPartyCache", key = "#xGUID")
    public String policiesOwnedByParty(final String uniqueCustomerNumber, final String xGUID)
            throws Exception {

        String jsonResponse = CommonUtils.getEmptyString();
        HttpHeaders headers;
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLConstants.XGUIDKEYFORAPI, xGUID);
        ResponseEntity<String> response = null;
        HttpEntity<String> entity;
        entity = new HttpEntity<String>(getDefaultHeaders(xGUID));
        response = restTemplate.exchange(undertowUrl + policiesOwnedByAPartyAPI, HttpMethod.GET, entity, String.class,
                uniqueCustomerNumber);
        jsonResponse = response.getBody();
        LOGGER.debug("Cached policiesOwnedByParty {} {} ", uniqueCustomerNumber, jsonResponse);
        return jsonResponse;
    }

    @Cacheable(value = "partyBasicDetailByPartyNoResponseCache", key = "#xGUID")
    public String getPartyBasicDetailByPartyNoResponse(final String bancsUniqueCustNo, final String xGUID) {
        // TODO Auto-generated method stub

        String jsonResponse = CommonUtils.getEmptyString();
        ResponseEntity<String> response = null;
        HttpEntity<String> entity;
        entity = new HttpEntity<String>(getDefaultHeaders(xGUID));
        response = restTemplate.exchange(
                new StringBuffer(undertowUrl.trim()).append(partyBasicDetailByPartyNoAPI.trim()).toString(),
                HttpMethod.GET, entity, String.class, bancsUniqueCustNo);
        jsonResponse = response.getBody();
        LOGGER.debug("Cached partyBasicDetailByPartyNoResponseCache {} {} ", bancsUniqueCustNo, jsonResponse);
        return jsonResponse;

    }

    /**
     * Method to get PolicyDetail of a policy
     *
     * @param policyNumber
     *            policy Number of the user
     * @param xGUID
     *            xGUID of the user
     * @return Policy Detail or Error
     */
    public PolicyDetailResponse policyDetail(final String policyNumber, final String xGUID) {
        // TODO Auto-generated method stub
        PolicyDetailResponse policyResponse = null;
        ResponseEntity<String> response = null;
        try {
            HttpHeaders headers;
            headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            // headers.set(MPLConstants.XGUIDKEYFORAPI, xGUID);
            HttpEntity<String> entity;
            entity = new HttpEntity<String>(headers);
            response = restTemplate.exchange(undertowUrl + bancsPolicySummaryAPI, HttpMethod.GET, entity, String.class,
                    policyNumber);
            final ObjectMapper mapper = new ObjectMapper();
            policyResponse = mapper.readValue(response.getBody(), PolicyDetailResponse.class);
        } catch (final Exception exception) {
            // LOGGER.error("{} FAILED BancsPoliciesDetails Exception trying to
            // map with ErrorResponse{}", xGUID, exception);
            // final ErrorResponseDTO error =
            // mapper.readValue(response.getBody(), ErrorResponseDTO.class);
            // LOGGER.error("{} FAILED BancsPoliciesDetails Received Error
            // Response{}:: ", xGUID, error);
        }
        return policyResponse;
    }

    /**
     * Method to get Valuations
     *
     * @param policyNumber
     *            policy Number of the user
     * @param xGUID
     *            xGUID of the user
     * @return Valuation/ Error
     */
    public ValuationsResponse valuations(final String policyNumber, final String xGUID) {
        // TODO Auto-generated method stub
        ValuationsResponse valuationResponse = null;
        ResponseEntity<String> response = null;
        try {
            HttpHeaders headers;
            headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            // headers.set(MPLConstants.XGUIDKEYFORAPI, xGUID);
            HttpEntity<String> entity;
            entity = new HttpEntity<String>(headers);
            response = restTemplate.exchange(undertowUrl + bancsValuationsAPI, HttpMethod.GET, entity, String.class,
                    policyNumber);
            final ObjectMapper mapper = new ObjectMapper();
            valuationResponse = mapper.readValue(response.getBody(), ValuationsResponse.class);
        } catch (final Exception exception) {
            // LOGGER.error("{} FAILED BancsPoliciesDetails Exception trying to
            // map with ErrorResponse{}", xGUID, exception);
            // final ErrorResponseDTO error =
            // mapper.readValue(response.getBody(), ErrorResponseDTO.class);
            // LOGGER.error("{} FAILED BancsPoliciesDetails Received Error
            // Response{}:: ", xGUID, error);
        }
        return valuationResponse;
    }

    /**
     * Method to get premiumSummaries
     *
     * @param policyNumber
     *            Policy Number entered by user
     * @param xGUID
     *            xGUID of the user
     * @return PremiumSummaries Response
     */
    public PremiumSummariesResponse premiumSummaries(final String policyNumber, final String xGUID) {
        // TODO Auto-generated method stub
        PremiumSummariesResponse premiumSummariesResponse = null;
        ResponseEntity<String> response = null;
        try {
            HttpHeaders headers;
            headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            // headers.set(MPLConstants.XGUIDKEYFORAPI, xGUID);
            HttpEntity<String> entity;
            entity = new HttpEntity<String>(headers);
            response = restTemplate.exchange(undertowUrl + bancsPremiumSummariesAPI, HttpMethod.GET, entity, String.class,
                    policyNumber);
            final ObjectMapper mapper = new ObjectMapper();
            premiumSummariesResponse = mapper.readValue(response.getBody(), PremiumSummariesResponse.class);
        } catch (final Exception exception) {
            // LOGGER.error("{} FAILED BancsPoliciesDetails Exception trying to
            // map with ErrorResponse{}", xGUID, exception);
            // final ErrorResponseDTO error =
            // mapper.readValue(response.getBody(), ErrorResponseDTO.class);
            // LOGGER.error("{} FAILED BancsPoliciesDetails Received Error
            // Response{}:: ", xGUID, error);
        }
        return premiumSummariesResponse;
    }

    /**
     * Method to get PolicyExist Response from BaNCS
     *
     * @param policyNumber
     *            PolicyNumber of the user
     * @param xGUID
     *            xGUID of the user
     * @return PolicyExist/ Error Response
     * @throws Exception
     *             exception
     */
    public BancsPolicyExistResponse getPolicyExistsResponse(final String policyNumber, final String xGUID) throws Exception {
        BancsPolicyExistResponse policyExistResponse = null;
        ResponseEntity<String> response = null;
        HttpEntity<String> entity;
        entity = new HttpEntity<String>(getDefaultHeaders(xGUID));
        response = restTemplate.exchange(undertowUrl + bancsPolicyExistsCheckAPI, HttpMethod.GET, entity, String.class,
                policyNumber);
        final String jsonResponse = response.getBody();
        if (jsonResponse.contains(MPLServiceConstants.ERRORSTRING)) {
            LOGGER.error("BancsPolicyExistCheck received Error response from BaNCS for xGUID {}, {}", xGUID, jsonResponse);
            final ErrorResponseDTO error = objectMapper.readValue(jsonResponse, ErrorResponseDTO.class);
            if (error.getErrorDetail().getInternalErrorCode()
                    .equalsIgnoreCase(HttpStatus.INTERNAL_SERVER_ERROR.toString())) {
                LOGGER.error("Throw new MPLException for Internal Server Code 500");
                throw new MPLException();
            } else {
                LOGGER.error("Throwing PolicyNotFoundException");
                throw new PolicyNotFoundException("Policy does not exist in BANCS");
            }

        } else {
            policyExistResponse = objectMapper.readValue(jsonResponse, BancsPolicyExistResponse.class);
        }
        LOGGER.debug("Cached BancsPolicyExistCheck {} {} ", policyNumber, jsonResponse);

        return policyExistResponse;
    }

    /**
     * Method to get PartyBasicDetail Response from BaNCS
     *
     * @param policyNumber
     *            PolicyNumber of the user
     * @param xGUID
     *            xGUID of the User
     * @return PartyBasicDetail or Error
     * @throws Exception
     *             exception
     */
    @Cacheable(value = "partyBasicDetailCache", key = "#xGUID")
    public String getPartyBasicDetailResponse(final String policyNumber, final String xGUID)
            throws Exception {
        String jsonResponse = CommonUtils.getEmptyString();
        ResponseEntity<String> response = null;
        HttpEntity<String> entity;
        entity = new HttpEntity<String>(getDefaultHeaders(xGUID));
        response = restTemplate.exchange(undertowUrl + bancsPartyBasicDetailAPI, HttpMethod.GET, entity, String.class,
                policyNumber);
        if (CommonUtils.isObjectNotNull(response)) {
            jsonResponse = response.getBody();
        }
        LOGGER.debug("Cached PartyBasicDetailResponse {} {} ", policyNumber, jsonResponse);
        return jsonResponse;
    }

    /**
     * Method to get IDAndVData Response from BaNCS
     *
     * @param policyNumber
     *            Policy number of the user
     * @param xGUID
     *            xGUID of the user
     * @return IDAndVData or Error
     * @throws Exception
     *             exception
     */
    @Cacheable(value = "iDAndVDataCache", key = "#xGUID")
    public String getIDAndVDataResponse(final String policyNumber, final String xGUID)
            throws Exception {
        String jsonResponse = CommonUtils.getEmptyString();
        ResponseEntity<String> response = null;
        HttpEntity<String> entity;
        entity = new HttpEntity<String>(getDefaultHeaders(xGUID));
        response = restTemplate.exchange(undertowUrl + bancsIDAndVDataAPI, HttpMethod.GET, entity, String.class,
                policyNumber);
        if (CommonUtils.isObjectNotNull(response)) {
            jsonResponse = response.getBody();
        }
        LOGGER.debug("Cached iDAndVDataCache {} {} ", policyNumber, jsonResponse);
        return jsonResponse;
    }

    /**
     * Method to get BancsPartyCommDetails Response from BaNCS
     *
     * @param bancsUniqueCustNo
     *            custNo of User
     * @param xGUID
     *            xGUID of User
     * @return PartyCommDetail or Error
     * @throws Exception
     *             exception
     */
    @Cacheable(value = "bancsCalcCashInTaxAPICache", key = "#xGUID")
    public String bancsCalcCashInTaxAPI(final String bancsPolicyNumber, final BancsCalcCashInTaxRequest calcTaxRequest,
            final String xGUID)
            throws Exception {
        HttpHeaders headers;
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        // headers.set(MPLConstants.XGUIDKEYFORAPI, xGUID);
        String jsonResponse = CommonUtils.getEmptyString();
        ResponseEntity<String> response = null;
        HttpEntity<BancsCalcCashInTaxRequest> entity;
        entity = new HttpEntity<BancsCalcCashInTaxRequest>(calcTaxRequest, getDefaultHeaders(xGUID));
        response = restTemplate.exchange(undertowUrl + bancsCalcCashInTaxAPI, HttpMethod.POST, entity, String.class,
                bancsPolicyNumber);
        if (CommonUtils.isObjectNotNull(response)) {
            jsonResponse = response.getBody();
        }
        LOGGER.debug("Cached bancsCalcCashInTaxAPICache {} {} ", bancsPolicyNumber, jsonResponse);
        return jsonResponse;
    }

    private HttpHeaders getDefaultHeaders(final String xGUID) {

        HttpHeaders headers;
        headers = new HttpHeaders();

        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);

        return headers;
    }

    /**
     * Method to get BancsPartyCommDetails Response from BaNCS
     *
     * @param bancsUniqueCustNo
     *            custNo of User
     * @param xGUID
     *            xGUID of User
     * @return PartyCommDetail or Error
     * @throws Exception
     *             exception
     */
    @Cacheable(value = "partyCommDetailCache", key = "#xGUID")
    public String getPartyCommDetailsResponse(final String bancsUniqueCustNo, final String xGUID)
            throws Exception {
        String jsonResponse = CommonUtils.getEmptyString();
        ResponseEntity<String> response = null;
        HttpEntity<String> entity;
        entity = new HttpEntity<String>(getDefaultHeaders(xGUID));
        response = restTemplate.exchange(undertowUrl + bancsPartyCommDetailAPI, HttpMethod.GET, entity, String.class,
                bancsUniqueCustNo);
        if (CommonUtils.isObjectNotNull(response)) {
            jsonResponse = response.getBody();
        }
        LOGGER.debug("Cached PartyCommDetailsResponse {} {} ", bancsUniqueCustNo, jsonResponse);
        return jsonResponse;
    }

    /**
     * Method to get bancsPolDetails Response from BaNCS
     *
     * @param bancsPolNum
     *            bancsPolNum of User
     * @param xGUID
     *            xGUID of User
     * @return bancsPolDetails or Error
     * @throws Exception
     *             exception
     */
    @Cacheable(value = "polDetailsCache", key = "#xGUIDBancsPolNumCacheKey")
    public String getPolDetailsResponse(final String bancsPolNum, final String xGUID, final String xGUIDBancsPolNumCacheKey)
            throws Exception {
        String jsonResponse = CommonUtils.getEmptyString();
        ResponseEntity<String> response = null;
        HttpEntity<String> entity;
        entity = new HttpEntity<String>(getDefaultHeaders(xGUID));
        response = restTemplate.exchange(undertowUrl + bancsPolDetailsAPI, HttpMethod.GET, entity, String.class,
                bancsPolNum);
        if (CommonUtils.isObjectNotNull(response)) {
            jsonResponse = response.getBody();
        }
        LOGGER.debug("Cached polDetailsCache {} {} ", bancsPolNum, jsonResponse);
        return jsonResponse;
    }

    /**
     * Method to get bancsClaimList Response from BaNCS
     *
     * @param bancsPolNum
     *            bancsPolNum of User
     * @param xGUID
     *            xGUID of User
     * @return bancsClaimList or Error
     * @throws Exception
     *             exception
     */
    @Cacheable(value = "claimListCache", key = "#xGUIDBancsPolNumCacheKey")
    public String getClaimListResponse(final String bancsPolNum, final String xGUID, final String xGUIDBancsPolNumCacheKey)
            throws Exception {
        String jsonResponse = CommonUtils.getEmptyString();
        ResponseEntity<String> response = null;
        HttpEntity<String> entity;
        entity = new HttpEntity<String>(getDefaultHeaders(xGUID));
        response = restTemplate.exchange(undertowUrl + bancsClaimListAPI, HttpMethod.GET, entity, String.class,
                bancsPolNum);
        if (CommonUtils.isObjectNotNull(response)) {
            jsonResponse = response.getBody();
        }
        LOGGER.debug("Cached claimListCache {} {} ", bancsPolNum, jsonResponse);
        return jsonResponse;
    }

    /**
     * Method to get bancsClaimList Response from BaNCS
     *
     * @param bancsPolNum
     *            bancsPolNum of User
     * @param bancsUniqueCustNo
     *            bancsUniqueCustNo of User
     * @param xGUID
     *            xGUID of User
     * @return bancsClaimList or Error
     * @throws Exception
     *             exception
     */
    @Cacheable(value = "retQuoteCache", key = "#xGUIDBancsPolNumCacheKey")
    public String getRetQuoteResponse(final String bancsPolNum, final String bancsUniqueCustNo, final String xGUID,
            final String xGUIDBancsPolNumCacheKey)
            throws Exception {
        String jsonResponse = CommonUtils.getEmptyString();
        ResponseEntity<String> response = null;
        HttpEntity<String> entity;
        entity = new HttpEntity<String>(getDefaultHeaders(xGUID));
        response = restTemplate.exchange(undertowUrl + bancsRetQuoteValAPI, HttpMethod.GET, entity, String.class,
                bancsPolNum, bancsUniqueCustNo);
        if (CommonUtils.isObjectNotNull(response)) {
            jsonResponse = response.getBody();
        }
        LOGGER.debug("Cached retQuoteCache {} {} ", bancsPolNum, jsonResponse);
        return jsonResponse;
    }

    @CacheEvict(value = { "partyBasicDetailCache", "iDAndVDataCache", "partyCommDetailCache",
            "partyBasicDetailByPartyNoResponseCache", "bancsCalcCashInTaxAPICache", "policiesOwnedByAPartyCache",
            "finalDashboardCache" }, key = "#xGUID")
    public void clearCache(final String xGUID) {
        LOGGER.debug("Cached Cleared");
    }

    @CacheEvict(value = { "polDetailsCache", "claimListCache",
            "retQuoteCache" }, key = "#xGUIDBancsPolNumCacheKey")
    public void clearXGUIDBancsPolNumCache(final String xGUIDBancsPolNumCacheKey) {
        LOGGER.debug("xGUIDBancsPolNum Cache Cleared");
    }

    @CacheEvict(value = { "finalDashboardCache" }, key = "#xGUID")
    public void clearFinalDashboardCache(final String xGUID) {
        LOGGER.debug("Final Dashboard Cache cleared");
    }
}
