/*
 * BancsUpdateServiceClient.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.client.bancs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dto.bancspost.BancsCreateQuoteRequest;
import uk.co.phoenixlife.service.dto.bancspost.BancsEmailPhoneUserIdRequest;
import uk.co.phoenixlife.service.dto.bancspost.BancsLogCorrespondenceRequest;

/**
 * BancsUpdateServiceClient.java
 */
@Component
public class BancsUpdateServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${undertow.rest}")
    private String undertowRestUrl;

    @Value("${bancsEmailPhoneUseridAPI}")
    private String bancsEmailPhoneUseridUrl;

    @Value("${bancsCreateQuoteAPI}")
    private String bancsCreateQuoteUrl;

    @Value("${bancsLogCorrespondenceAPI}")
    private String bancsLogCorrespondenceUrl;

    /**
     * Method to send updated personal detail fields to BaNCs
     *
     * @param bancsEmailPhoneUserIdRequest
     *            - BancsEmailPhoneUserIdRequest object
     * @param bancsCustomerNumber
     *            - bancsCustomerNumber
     * @param xGUID
     *            - xGUID (unique UUID identifier)
     * @return - JSON String
     * @throws Exception
     *             - {@link Exception}
     */
    public String sendEmailPhoneUserIdDetails(final BancsEmailPhoneUserIdRequest bancsEmailPhoneUserIdRequest,
            final String bancsCustomerNumber, final String xGUID) throws Exception {

        HttpHeaders headers;
        HttpEntity<BancsEmailPhoneUserIdRequest> bancsRequest;
        ResponseEntity<String> bancsResponse;

        headers = getDefaultHeaders(xGUID);

        bancsRequest = new HttpEntity<BancsEmailPhoneUserIdRequest>(bancsEmailPhoneUserIdRequest, headers);

        bancsResponse = restTemplate.exchange(
                new StringBuffer(undertowRestUrl.trim()).append(bancsEmailPhoneUseridUrl.trim()).toString(),
                HttpMethod.POST, bancsRequest, String.class, bancsCustomerNumber);

        return bancsResponse.getBody();
    }

    /**
     * Method to send quote details to BaNCs
     *
     * @param bancsCreateQuoteRequest
     *            - BancsCreateQuoteRequest object
     * @param bancsPolicyNumber
     *            - bancsPolicyNumber
     * @param bancsCustomerNumber
     *            - bancsCustomerNumber
     * @param xGUID
     *            - xGUID (unique UUID identifier)
     * @return - JSON String
     * @throws Exception
     *             - {@link Exception}
     */
    public String sendQuoteDetails(final BancsCreateQuoteRequest bancsCreateQuoteRequest,
            final String bancsPolicyNumber, final String bancsCustomerNumber, final String xGUID) throws Exception {

        HttpHeaders headers;
        HttpEntity<BancsCreateQuoteRequest> bancsRequest;
        ResponseEntity<String> bancsResponse;

        headers = getDefaultHeaders(xGUID);

        bancsRequest = new HttpEntity<BancsCreateQuoteRequest>(bancsCreateQuoteRequest, headers);

        bancsResponse = restTemplate.exchange(
                new StringBuffer(undertowRestUrl.trim()).append(bancsCreateQuoteUrl.trim()).toString(),
                HttpMethod.POST, bancsRequest, String.class, bancsPolicyNumber, bancsCustomerNumber);

        return bancsResponse.getBody();
    }

    /**
     * Method to send log correspondence details to BaNCs
     *
     * @param bancsLogCorrespondenceRequest
     *            - BancsLogCorrespondenceRequest object
     * @param bancsPolicyNumber
     *            - bancsPolicyNumber
     * @param xGUID
     *            - xGUID (unique UUID identifier)
     * @return - JSON String
     * @throws Exception
     *             - {@link Exception}
     */
    public String sendLogCorrespondence(final BancsLogCorrespondenceRequest bancsLogCorrespondenceRequest,
            final String bancsPolicyNumber, final String xGUID) throws Exception {

        HttpHeaders headers;
        HttpEntity<BancsLogCorrespondenceRequest> bancsRequest;
        ResponseEntity<String> bancsResponse;

        headers = getDefaultHeaders(xGUID);

        bancsRequest = new HttpEntity<BancsLogCorrespondenceRequest>(bancsLogCorrespondenceRequest, headers);

        bancsResponse = restTemplate.exchange(
                new StringBuffer(undertowRestUrl.trim()).append(bancsLogCorrespondenceUrl.trim()).toString(),
                HttpMethod.POST, bancsRequest, String.class, bancsPolicyNumber);

        return bancsResponse.getBody();
    }

    private HttpHeaders getDefaultHeaders(final String xGUID) {

        HttpHeaders headers;
        headers = new HttpHeaders();

        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);

        return headers;
    }
}
