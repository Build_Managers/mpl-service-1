/*
 * MPLServiceClientConfig.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.client.config;

import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * MPLServiceClientConfig.java
 */
@Configuration
//@Import({ CacheConfig.class })

public class MPLServiceClientConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(MPLServiceClientConfig.class);

    @Value("${rest.timeout}")
    private String timeOut;

    @Value("${rest.maxconnection}")
    private String maxConnection;

    @Value("${rest.maxconnectionperroute}")
    private String maxConnectionPerRoute;

    /**
     * Bean configuration for RestTemplate
     *
     * @return - {@link RestTemplate}
     */
    @Bean
    public RestTemplate restTemplate(final ClientHttpRequestFactory clientHttpRequestFactory) {

        RestTemplate restTemplate;
        restTemplate = new RestTemplate(clientHttpRequestFactory);

        LOGGER.debug("RestTemplate bean configured...");
        return restTemplate;
    }

    @Bean
    public ObjectMapper objectMapper() {

        ObjectMapper objectMapper = new ObjectMapper();
        /*
         * mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
         * false); mapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, true);
         */
        LOGGER.debug("objectMapper bean configured...");
        return objectMapper;
    }

    /**
     * Bean configuration for ClientHttpRequestFactory
     *
     * @return - {@link ClientHttpRequestFactory}
     */
    @Bean
    public ClientHttpRequestFactory clientHttpRequestFactory() {

        PoolingHttpClientConnectionManager connectionManager;
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory;
        int timeout, maxconnection, maxconnectionperroute;

        timeout = Integer.valueOf(timeOut);
        maxconnection = Integer.valueOf(maxConnection);
        maxconnectionperroute = Integer.valueOf(maxConnectionPerRoute);

        connectionManager = new PoolingHttpClientConnectionManager();

        connectionManager.setMaxTotal(maxconnection);
        connectionManager.setDefaultMaxPerRoute(maxconnectionperroute);
        clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(HttpClientBuilder
                .create()
                .setConnectionManager(connectionManager)
                .build());
        clientHttpRequestFactory.setConnectTimeout(timeout);
        clientHttpRequestFactory.setConnectionRequestTimeout(timeout);
        clientHttpRequestFactory.setReadTimeout(timeout);

        LOGGER.debug("ClientHttpRequestFactory bean configured...");

        return clientHttpRequestFactory;
    }

}
