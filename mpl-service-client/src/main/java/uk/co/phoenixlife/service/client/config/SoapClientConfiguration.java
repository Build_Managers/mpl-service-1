/*
 * SoapClientConfiguration.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.client.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;
import uk.co.phoenixlife.service.client.pca.PcaFindClient;
import uk.co.phoenixlife.service.client.pca.PcaFindService;
import uk.co.phoenixlife.service.client.pca.PcaRetrieveClient;
import uk.co.phoenixlife.service.client.pca.PcaRetrieveService;
//import uk.co.phoenixlife.service.client.pca.PcaRetrieveClient;
//import uk.co.phoenixlife.service.client.pca.PcaRetrieveService;
import uk.co.phoenixlife.service.client.pca.PostCodeAnywhereClient;
import uk.co.phoenixlife.service.client.vocalink.VocalinkClient;
import uk.co.phoenixlife.service.constants.MPLConstants;

/**
 * SoapClientConfiguration.java
 */
@Configuration
@PropertySource({ "file:/app-wildfly/mpl_config/mpl-service.properties" })
@ComponentScan(basePackages = { "uk.co.phoenixlife.service.client" })
public class SoapClientConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(SoapClientConfiguration.class);

    @Value("${undertow.paaf}")
    private String undertowPaaf;

    @Value("${undertow.bankwizard}")
    private String undertowBankWizard;

    @Value("${suppress.bankwizard}")
    private String suppressFlag;

    @Value("${pcapredict.key}")
    private String key;

    /**
     * Bean configuration for Jaxb2Marshaller
     *
     * @return - {@link Jaxb2Marshaller}
     */
    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller;
        marshaller = new Jaxb2Marshaller();
        marshaller.setContextPaths("uk.co.phoenixlife.service.dto.pca", "uk.co.phoenixlife.service.dto.bankwizard",
                "uk.co.phoenixlife.service.dto.pca.find", "uk.co.phoenixlife.service.dto.pca.retrieve");
        LOGGER.info("JAXB marshaller registered.");
        return marshaller;
    }

    /**
     * Bean configuration for PostCodeAnywhereClient
     *
     * @param marshaller
     *            - {@link Jaxb2Marshaller}
     * @return - PostCodeAnywhereClient object
     */
    @Bean
    public PostCodeAnywhereClient pcaClient(final Jaxb2Marshaller marshaller) {
        PostCodeAnywhereClient client;
        String url;

        client = new PostCodeAnywhereClient();
        url = undertowPaaf.trim().concat(MPLConstants.PCA_CONTEXT);

        client.setDefaultUri(url);
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        LOGGER.info("PostCodeAnywhereClient registered with default url : {}", url);
        return client;
    }

    /**
     * Bean configuration for BankWizardClient
     *
     * @param marshaller
     *            - {@link Jaxb2Marshaller}
     * @return - BankWizardClient object
    
    @Bean
    public VocalinkClient vocalinkClient(final Jaxb2Marshaller marshaller) {
    	VocalinkClient client;
        String url;

        client = new VocalinkClient();
        url = undertowBankWizard.trim().concat(MPLConstants.BANK_WIZARD_CONTEXT);

        client.setDefaultUri(url);
        client.setSuppressFlag(suppressFlag);
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        LOGGER.info("BankWizardClient registered with default url : {} and suppress flag {}", url, suppressFlag);
        return client;
    }
 */

    /**
     * Bean configuration for PCAPredictFindClient.
     *
     * @return PCAPredictFindClient object.
     */
    @Bean
    public PcaFindClient addressFindClient() {
        PcaFindClient client;
        client = new PcaFindClient(key);
        LOGGER.info("PCAPredictFindClient registered with key : {}", key);
        return client;
    }

    /**
     * Bean configuration for PCAPredictRetrieveClient.
     *
     * @return PCAPredictRetrieveClient object.*/
    
    @Bean
    public PcaRetrieveClient addressRetrieveClient() {
        PcaRetrieveClient client;

        client = new PcaRetrieveClient(key);
        LOGGER.info("PCAPredictRetrieveClient registered with key : {}", key);
        return client;
    }

    /**
     * Bean configuration for PCAPredictRetrieveService.
     *
     * @param marshaller
     *            - {@link Jaxb2Marshaller}
     * @return PCAPredictRetrieveService object.*/
     
    @Bean
    public PcaRetrieveService addressRetrieveService(final Jaxb2Marshaller marshaller) {
        PcaRetrieveService retrieveClient;
        String url;
        WebServiceTemplate webServiceTemplate;

        webServiceTemplate = new WebServiceTemplate();
        retrieveClient = new PcaRetrieveService();

        url = undertowPaaf.trim().concat(MPLConstants.PCA_RETRIEVE_CONTEXT);

        webServiceTemplate.setCheckConnectionForFault(false);
        webServiceTemplate.setDefaultUri(url);

        retrieveClient.setWebServiceTemplate(webServiceTemplate);
        //retrieveClient.setDefaultUri(url);
        retrieveClient.setMarshaller(marshaller);
        retrieveClient.setUnmarshaller(marshaller);
        LOGGER.info("PCAPredictRetrieveService registered with default url : {}", url);
        return retrieveClient;
    }

    /**
     * Bean configuration for PCAPredictFindService.
     *
     * @param marshaller
     *            - {@link Jaxb2Marshaller}
     * @return PCAPredictFindService object.
     */
    @Bean
    public PcaFindService addressFindService(final Jaxb2Marshaller marshaller) {
        PcaFindService findClient;
        String url;
        WebServiceTemplate webServiceTemplate;

        webServiceTemplate = new WebServiceTemplate();

        findClient = new PcaFindService();

        url = undertowPaaf.trim().concat(MPLConstants.PCA_FIND_CONTEXT);

        webServiceTemplate.setCheckConnectionForFault(false);
        webServiceTemplate.setDefaultUri(url);

        findClient.setWebServiceTemplate(webServiceTemplate);
        //findClient.setDefaultUri(url);
        findClient.setMarshaller(marshaller);
        findClient.setUnmarshaller(marshaller);

        LOGGER.info("PCAPredictFindService registered with default url : {}",
                findClient.getWebServiceTemplate().getDefaultUri());
        return findClient;
    }

    /**
     * Bean configuration for PropertySourcesPlaceholderConfigurer *
     *
     * @return - {@link PropertySourcesPlaceholderConfigurer}
     */
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfig() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
