/*
 * EmailServiceClient.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.client.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import uk.co.phoenixlife.service.dto.email.EmailRequestDTO;
import uk.co.phoenixlife.service.dto.email.EmailResponseDTO;

/**
 * EmailServiceClient.java
 */
@Component
public class EmailServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${undertow.mail}")
    private String emailAPIUrl;

    /**
     * Method to send mail through EmailAPI
     * 
     * @param emailRequest
     *            - EmailRequestDTO object
     * @return - EmailResponseDTO object
     * @throws Exception
     *             - {@link Exception}
     */
    public EmailResponseDTO sendMail(final EmailRequestDTO emailRequest) throws Exception {

        HttpHeaders headers;
        HttpEntity<EmailRequestDTO> request;
        ResponseEntity<EmailResponseDTO> response;

        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        request = new HttpEntity<EmailRequestDTO>(emailRequest, headers);

        response = restTemplate.exchange(emailAPIUrl, HttpMethod.POST, request, EmailResponseDTO.class);

        return response.getBody();
    }
}
