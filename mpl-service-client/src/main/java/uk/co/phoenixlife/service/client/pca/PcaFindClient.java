package uk.co.phoenixlife.service.client.pca;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import uk.co.phoenixlife.service.dto.pca.find.CaptureInteractiveFindV100;
import uk.co.phoenixlife.service.dto.pca.find.CaptureInteractiveFindV100ArrayOfResults;
import uk.co.phoenixlife.service.dto.pca.find.CaptureInteractiveFindV100Results;
import uk.co.phoenixlife.service.utils.CommonUtils;

/**
 * The Class AddressFindClient.
 */
@Component
public class PcaFindClient extends CaptureInteractiveFindV100ArrayOfResults {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(PcaFindClient.class);

    /** The address find service. */
    @Autowired
    private PcaFindService addressFindService;

    /** The key. */
    private String key;

    /**
     * Instantiates a new PCA predict find client.
     *
     * @param pcaKey
     *            the pca key
     */
    public PcaFindClient(final String pcaKey) {
        this.key = pcaKey;
    }

    /**
     * Instantiates a new PCA predict find client.
     */
    public PcaFindClient() {
    }

    /**
     * Find.
     *
     * @param postcode
     *            the postcode
     * @return the capture interactive find V 100 array of results
     */
    public CaptureInteractiveFindV100ArrayOfResults find(final String postcode) {
        List<CaptureInteractiveFindV100Results> listOfResults;

        listOfResults = new ArrayList<CaptureInteractiveFindV100Results>();

        CaptureInteractiveFindV100 findRequest = new CaptureInteractiveFindV100();

        findRequest.setKey(key);
        findRequest.setText(postcode);
        findRequest.setCountries("GB");
        findRequest.setLanguage("en");

        LOGGER.info(" --->>> Calling findAddress() method");

        listOfResults = addressFindService.findAddress(findRequest, listOfResults);
		
		LOGGER.info("Addr. Service Obj : {}", addressFindService.toString());
		
        if (CommonUtils.isobjectNotNull(listOfResults)) {
            super.captureInteractiveFindV100Results = listOfResults;
        }
        return this;
    }

    /**
     * Sets the capture interactive find V 100 results.
     *
     * @param results
     *            the new capture interactive find V 100 results
     */
    public void setCaptureInteractiveFindV100Results(
            final List<CaptureInteractiveFindV100Results> results) {
        captureInteractiveFindV100Results = results;
    }

}
