package uk.co.phoenixlife.service.client.pca;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.XmlMappingException;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.SoapFault;
import org.springframework.ws.soap.client.SoapFaultClientException;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.dto.pca.find.CaptureInteractiveFindV100;
import uk.co.phoenixlife.service.dto.pca.find.CaptureInteractiveFindV100Response;
import uk.co.phoenixlife.service.dto.pca.find.CaptureInteractiveFindV100Results;

/**
 * The Class PCAPredictFindService.
 */
@Component
public class PcaFindService extends WebServiceGatewaySupport {

    private static final Logger LOGGER = LoggerFactory.getLogger(PcaFindService.class);

    /**
     * Find address.
     *
     * @param findRequest
     *            the find request
     * @param listOfResults
     *            the list of results
     * @return the list
     * @throws XmlMappingException
     *             the xml mapping exception
     */
    
    public List<CaptureInteractiveFindV100Results> findAddress(
            final CaptureInteractiveFindV100 findRequest,
            final List<CaptureInteractiveFindV100Results> listOfResults) {
        Iterator<CaptureInteractiveFindV100Results> iterator;

        try {
            CaptureInteractiveFindV100Response response = (CaptureInteractiveFindV100Response) getWebServiceTemplate()
					.marshalSendAndReceive(getWebServiceTemplate().getDefaultUri(), findRequest,
							new SoapActionCallback(MPLConstants.PCA_FIND_SOAP_ACTION));
            iterator = response.getCaptureInteractiveFindV100Result()
                    .getCaptureInteractiveFindV100Results().iterator();

            while (iterator.hasNext()) {
                CaptureInteractiveFindV100Results results = iterator
                        .next();

                if (results.getType().equalsIgnoreCase("Address")) {
                    listOfResults.add(results);
                } else if (!results.getType().equalsIgnoreCase("Address")
                        && filter(findRequest, results)) {
                    findRequest.setContainer(results.getId());
                    findAddress(findRequest, listOfResults);
                } else {
                    continue;
                }
            }

        } catch (SoapFaultClientException se) {
            SoapFault soapFault = se.getSoapFault();
            LOGGER.error(
                    "Failed Find Postcode : Fault Code = {} | Fault String = {} | Fault Description : {}",
                    soapFault.getFaultCode(), soapFault.getFaultStringOrReason());
        }
        return listOfResults;
    }

    /**
     * Filter.
     *
     * @param findRequest
     *            the find request
     * @param results
     *            the results
     * @return true, if successful
     */
    private boolean filter(final CaptureInteractiveFindV100 findRequest,
            final CaptureInteractiveFindV100Results results) {
        boolean status = false;

        final String postCodeInResults = results.getText().replace(" ", "");
        final String postCodeInRequest = findRequest.getText().replace(" ", "");

        if (postCodeInResults.equalsIgnoreCase(postCodeInRequest)) {
            status = true;
        } else {
            status = false;
        }
        return status;
    }

    /*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PCA Find Service Initialized";
	}

}
