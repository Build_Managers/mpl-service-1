package uk.co.phoenixlife.service.client.pca;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100;
import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100ArrayOfResults;
import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100Results;
import uk.co.phoenixlife.service.dto.pca.retrieve.ObjectFactory;

/**
 * The Class PCAPredictRetrieveClient.
 */
@Component
public class PcaRetrieveClient extends CaptureInteractiveRetrieveV100ArrayOfResults {

    private static final Logger LOGGER = LoggerFactory.getLogger(PcaRetrieveClient.class);

    /** The address retrieve service. */
    @Autowired
    private PcaRetrieveService addressRetrieveService;

    /** The retrieve object factory. */
    private ObjectFactory retrieveObjectFactory = new ObjectFactory();

    /** The key. */
    private String key;


    /**
     * Instantiates a new PCA predict retrieve client.
     *
     * @param pcaKey
     *            the pca key
     */
    public PcaRetrieveClient(final String pcaKey) {
        this.key = pcaKey;
    }

    /**
     * Instantiates a new PCA predict retrieve client.
     */
    public PcaRetrieveClient() {

    }

    /**
     * Retrieve.
     *
     * @param retrieveRequest
     *            the retrieve request
     * @return the capture interactive retrieve V 100 response
     */
    public CaptureInteractiveRetrieveV100ArrayOfResults retrieve(
            final CaptureInteractiveRetrieveV100 retrieveRequest) {

        final CaptureInteractiveRetrieveV100ArrayOfResults retrieveArray = retrieveObjectFactory
                .createCaptureInteractiveRetrieveV100ArrayOfResults();
        final List<CaptureInteractiveRetrieveV100Results> listOfRetrieveResults = retrieveArray
                .getCaptureInteractiveRetrieveV100Results();

		LOGGER.info("Calling retrieveAddress()");
        super.captureInteractiveRetrieveV100Results = addressRetrieveService
                .retrieveAddress(retrieveRequest, listOfRetrieveResults);
				
		LOGGER.info("Addr. Service Obj : {}", addressRetrieveService.toString());

        return this;
    }

    /**
     * Capture interactive retrieve V 100.
     *
     * @param id
     *            the id
     * @return the capture interactive retrieve V 100 response
     */
    public CaptureInteractiveRetrieveV100ArrayOfResults captureInteractiveRetrieveV100(
            final String id) {
        CaptureInteractiveRetrieveV100 retrieveRequest = retrieveObjectFactory
                .createCaptureInteractiveRetrieveV100();

        retrieveRequest.setKey(key);
        retrieveRequest.setId(id);

        return retrieve(retrieveRequest);
    }

}
