package uk.co.phoenixlife.service.client.pca;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.SoapFault;
import org.springframework.ws.soap.client.SoapFaultClientException;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100;
import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100Response;
import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100Results;


@Component
public class PcaRetrieveService extends WebServiceGatewaySupport {

    private static final Logger LOGGER = LoggerFactory.getLogger(PcaRetrieveService.class);

    /**
     * Retrieve.
     *
     * @param retrieveRequest
     *            the retrieve request
     * @param listOfRetrieveResults
     *            the list of retrieve results
     * @return the capture interactive find V 100 response
     */
    public List<CaptureInteractiveRetrieveV100Results> retrieveAddress(
            CaptureInteractiveRetrieveV100 retrieveRequest,
            List<CaptureInteractiveRetrieveV100Results> listOfRetrieveResults) {
        Iterator<CaptureInteractiveRetrieveV100Results> iterator;

        try {
            CaptureInteractiveRetrieveV100Response response = (CaptureInteractiveRetrieveV100Response) getWebServiceTemplate()
					.marshalSendAndReceive(getWebServiceTemplate().getDefaultUri(), retrieveRequest,
							new SoapActionCallback(MPLConstants.PCA_RETRIEVE_SOAP_ACTION));

            iterator = response.getCaptureInteractiveRetrieveV100Result()
                    .getCaptureInteractiveRetrieveV100Results().iterator();

            while (iterator.hasNext()) {
                CaptureInteractiveRetrieveV100Results results = iterator
                        .next();
                listOfRetrieveResults.add(results);
            }
        } catch (SoapFaultClientException se) {
            SoapFault soapFault = se.getSoapFault();
            LOGGER.error(
                    "Failed Retrieve Postcode : Fault Code = {} | Fault String = {} | Fault Description : {}",
                    soapFault.getFaultCode(), soapFault.getFaultStringOrReason());
        }

        return listOfRetrieveResults;
    }

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PCA Retrieve Service Initialized";
	}

}
