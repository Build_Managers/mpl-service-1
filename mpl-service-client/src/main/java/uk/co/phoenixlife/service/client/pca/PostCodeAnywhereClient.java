/*
 * PostCodeAnywhereClient.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.client.pca;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import uk.co.phoenixlife.service.dto.pca.ByPostcode;
import uk.co.phoenixlife.service.dto.pca.ByPostcodeResponse;
import uk.co.phoenixlife.service.dto.pca.EnContentType;
import uk.co.phoenixlife.service.dto.pca.EnLanguage;
import uk.co.phoenixlife.service.dto.pca.FetchAddress;
import uk.co.phoenixlife.service.dto.pca.FetchAddressResponse;

/**
 * PostCodeAnywhereClient.java
 */
@Component
public class PostCodeAnywhereClient extends WebServiceGatewaySupport {

    private static final Logger LOGGER = LoggerFactory.getLogger(PostCodeAnywhereClient.class);

    /**
     * Method to get possible address's for a postcode
     *
     * @param postcode
     *            - postcode
     * @param xGUID
     *            - xGUID
     * @return - ByPostcodeResponse object
     */
    public ByPostcodeResponse byPostCode(final String postcode, final String xGUID) {

        LOGGER.info("{} Invoking byPostCode of PostCodeAnywhereClient---->>>>");
        ByPostcodeResponse response;
        ByPostcode request;

        request = new ByPostcode();
        request.setPostcode(postcode);

        response = (ByPostcodeResponse) getWebServiceTemplate()
                .marshalSendAndReceive(getDefaultUri(), request,
                        new SoapActionCallback("PostcodeAnywhere2/ByPostcode"));

        LOGGER.debug("{} Response from PCA, No of result received: {}", response);
        LOGGER.debug("{} Error flag : {}",response.getByPostcodeResult().isIsError());

        return response;
    }

    /**
     * Method to get detailed address for a postcode
     *
     * @param addressId
     *            - address id
     * @param xGUID
     *            - xGUID
     * @return - FetchAddressResponse object
     */
    public FetchAddressResponse fetchAddress(final String addressId, final String xGUID) {

        LOGGER.info("{} Invoking fetchAddress of PostCodeAnywhereClient ");

        FetchAddressResponse response;
        FetchAddress request;

        request = new FetchAddress();
        request.setId(addressId);
        request.setLanguage(EnLanguage.EN_LANGUAGE_ENGLISH);
        request.setContentType(EnContentType.EN_CONTENT_STANDARD_ADDRESS);

        response = (FetchAddressResponse) getWebServiceTemplate()
                .marshalSendAndReceive(getDefaultUri(), request,
                        new SoapActionCallback("PostcodeAnywhere2/FetchAddress"));

        LOGGER.debug("{} Address details :{}",
                response.getFetchAddressResult().getResults().getAddress().get(0).getLine1());
        return response;
    }

}
