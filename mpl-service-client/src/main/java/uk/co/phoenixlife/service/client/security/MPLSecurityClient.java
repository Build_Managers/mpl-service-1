package uk.co.phoenixlife.service.client.security;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import uk.co.phoenixlife.security.service.dto.ForgotPasswordDto;
import uk.co.phoenixlife.security.service.dto.UserRegistrationDTO;
import uk.co.phoenixlife.security.service.dto.UserRegistrationFormDTO;
import uk.co.phoenixlife.service.constants.MPLServiceConstants;

@Component
public class MPLSecurityClient {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${url.mplSecurity}")
    private String mplSecurityUrl;

	@Value("${header.mplSecurity}")
	private String mplSecurityHeaderVal;

 public static final Logger LOGGER = LoggerFactory.getLogger(MPLSecurityClient.class);
	
	
	public String checkUser(final String userName) throws Exception {
		ResponseEntity<String> response;

		HttpEntity<String> entity;
		entity = new HttpEntity<String>(getDefaultHeaders());
		LOGGER.info("url of security service {} ",mplSecurityUrl + "/self-service/checkUser/{userName}");
		response = restTemplate.exchange(mplSecurityUrl + "/self-service/checkUser/{userName}", HttpMethod.GET, entity,
				String.class, userName);
		return response.getBody();
	}

	public String insertUserDetailInLDAP(final UserRegistrationFormDTO userRegistrationFormDTO) throws Exception {
		ResponseEntity<String> response;

		HttpEntity<Object> entity;
		entity = new HttpEntity<Object>(userRegistrationFormDTO, getDefaultHeaders());
		LOGGER.info("url of security service {}",mplSecurityUrl + "/self-service/insertUserDetailInLDAP");
		response = restTemplate.exchange(mplSecurityUrl + "/self-service/insertUserDetailInLDAP", HttpMethod.POST,
				entity, String.class);
		return response.getBody();
	}

	public String updateUserDetailInLDAP(final UserRegistrationFormDTO userRegistrationFormDTO) throws Exception {
		ResponseEntity<String> response;

		HttpEntity<Object> entity;
		entity = new HttpEntity<Object>(userRegistrationFormDTO, getDefaultHeaders());
		LOGGER.info("url of security service {} ",mplSecurityUrl + "/self-service/updateUserDetailInLDAP");
		response = restTemplate.exchange(mplSecurityUrl + "/self-service/updateUserDetailInLDAP", HttpMethod.POST,
				entity, String.class);
		return response.getBody();
	}

	public UserRegistrationDTO checkUserNameAlreadyExist(final String userName) throws Exception {
		ResponseEntity<UserRegistrationDTO> response;

		HttpEntity<Object> entity;
		entity = new HttpEntity<Object>(getDefaultHeaders());
		LOGGER.info("url of security service {} ",mplSecurityUrl + "/self-service/checkUserNameAlreadyExist/{userName}");
		response = restTemplate.exchange(mplSecurityUrl + "/self-service/checkUserNameAlreadyExist/{userName}",
				HttpMethod.POST, entity, UserRegistrationDTO.class, userName);
		return response.getBody();
	}

	public Boolean checkEmailAlreadyExist(final UserRegistrationFormDTO userRegistrationFormDTO) throws Exception {
		ResponseEntity<Boolean> response;

		HttpEntity<Object> entity;
		entity = new HttpEntity<Object>(userRegistrationFormDTO, getDefaultHeaders());
		String URL = mplSecurityUrl + "/self-service/checkEmailAlreadyExist";
		LOGGER.info("url of security service {} ", URL);
		response = restTemplate.exchange(mplSecurityUrl + "/self-service/checkEmailAlreadyExist", HttpMethod.POST,
				entity, Boolean.class);
		return response.getBody();
	}

	public UserRegistrationFormDTO activateAccount(final String token) {
		ResponseEntity<UserRegistrationFormDTO> response;

		HttpEntity<Object> entity;
		entity = new HttpEntity<Object>(getDefaultHeaders());
		LOGGER.info("url of security service {} ",mplSecurityUrl + "/self-service/validateToken/{actid}");
		response = restTemplate.exchange(mplSecurityUrl + "/self-service/validateToken/{actid}", HttpMethod.POST,
				entity, UserRegistrationFormDTO.class, token);
		return response.getBody();
	}

	public String fetchSecurityQuestion(final ForgotPasswordDto forgotPasswordDto) throws Exception {
		ResponseEntity<String> response;

		HttpEntity<Object> entity;
		entity = new HttpEntity<Object>(forgotPasswordDto, getDefaultHeaders());
		LOGGER.info("url of security service {} ",mplSecurityUrl + "/self-service/fetchSecurityQuestion");
		response = restTemplate.exchange(mplSecurityUrl + "/self-service/fetchSecurityQuestion",
				HttpMethod.POST, entity, String.class);
		return response.getBody();

	}

	public void lockAccount(final String userName, final boolean isRegistration) {

		HttpEntity<Object> entity;
		entity = new HttpEntity<Object>(getDefaultHeaders());
		LOGGER.info("url of security service {} ",mplSecurityUrl + "/self-service/lockAccount/{userName}/{isRegistration}");
		restTemplate.exchange(mplSecurityUrl + "/self-service/lockAccount/{userName}/{isRegistration}", HttpMethod.POST,
				entity, void.class, userName, isRegistration);
	}

	public void setAccountVerified(final String userName, final String status) {

		HttpEntity<Object> entity;
		entity = new HttpEntity<Object>(getDefaultHeaders());
		LOGGER.info("url of security service {} ",mplSecurityUrl + "/self-service/setAccountVerified/{userName}/{status}");
		restTemplate.exchange(mplSecurityUrl + "/self-service/setAccountVerified/{userName}/{status}", HttpMethod.POST,
				entity, void.class, userName, status);
	}

	public String submitSecurityAnswer(final ForgotPasswordDto forgotPasswordDto) throws Exception {
		ResponseEntity<String> response;

		HttpEntity<Object> entity;
		entity = new HttpEntity<Object>(forgotPasswordDto, getDefaultHeaders());
		LOGGER.info("url of security service {} ",mplSecurityUrl + "/self-service/submitSecurityAnswer");
		response = restTemplate.exchange(mplSecurityUrl + "/self-service/submitSecurityAnswer",
				HttpMethod.POST, entity, String.class);
		return response.getBody();

	}

	public String changePassUrlValidate(final String token) throws Exception {
		ResponseEntity<String> response;

		HttpEntity<Object> entity;
		entity = new HttpEntity<Object>(token, getDefaultHeaders());
		LOGGER.info("url of security service {} ",mplSecurityUrl + "/self-service/changePassUrlValidate");
		response = restTemplate.exchange(mplSecurityUrl + "/self-service/changePassUrlValidate", HttpMethod.POST,
				entity, String.class);
		return response.getBody();
	}

	public String changePassword(final ForgotPasswordDto forgotPasswordDto) throws Exception {
		ResponseEntity<String> response;

		HttpEntity<Object> entity;
		entity = new HttpEntity<Object>(forgotPasswordDto, getDefaultHeaders());
		LOGGER.info("url of security service {} ",mplSecurityUrl + "/self-service/changePasswordForgot");
		response = restTemplate.exchange(mplSecurityUrl + "/self-service/changePasswordForgot", HttpMethod.POST, entity,
				String.class);
		return response.getBody();

	}

    	public String increaseAnswerAttemptCount(final String userName) throws Exception {
		ResponseEntity<String> response;

		HttpEntity<Object> entity;
		entity = new HttpEntity<Object>(userName, getDefaultHeaders());
		LOGGER.info("url of security service {} ",mplSecurityUrl + "/self-service/increaseAnswerAttemptCount");
		response = restTemplate.exchange(mplSecurityUrl + "/self-service/increaseAnswerAttemptCount", HttpMethod.POST,
				entity, String.class);

		return response.getBody();
	}

	public UserRegistrationFormDTO ResetAccount(String token) throws Exception {

		ResponseEntity<UserRegistrationFormDTO> response;

		HttpEntity<Object> entity;
		entity = new HttpEntity<Object>(getDefaultHeaders());
		LOGGER.info("url of security service {} ",mplSecurityUrl + "/self-service/validateResetToken/{rpid}");
		response = restTemplate.exchange(mplSecurityUrl + "/self-service/validateResetToken/{rpid}", HttpMethod.POST,
				entity, UserRegistrationFormDTO.class, token);
		return response.getBody();

	}

	public String updateSecurityQuestionInLDAP(final UserRegistrationFormDTO userRegistrationFormDTO) throws Exception {
		ResponseEntity<String> response;
		HttpEntity<Object> entity;
		entity = new HttpEntity<Object>(userRegistrationFormDTO, getDefaultHeaders());
		LOGGER.info("url of security service {} ",mplSecurityUrl + "/self-service/updateSecurityQuestionInLDAP");
		response = restTemplate.exchange(mplSecurityUrl + "/self-service/updateSecurityQuestionInLDAP", HttpMethod.POST,
				entity, String.class);
		return response.getBody();
	}

	private HttpHeaders getDefaultHeaders() {

		final HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();

		HttpHeaders headers;
		headers = new HttpHeaders();
		headers.set(MPLServiceConstants.XGUID_HEADER, req.getHeader(MPLServiceConstants.XGUID_HEADER));
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set(MPLServiceConstants.TOKENHEADERNAME, mplSecurityHeaderVal);

		return headers;
	}
	
	public String fetchEmailIdFromLDAP(final String userName) throws Exception {
		ResponseEntity<String> response;

		HttpEntity<String> entity;
		entity = new HttpEntity<String>(getDefaultHeaders());
		LOGGER.info("url of security service ",mplSecurityUrl + "/self-service/fetchEmailIdFromLDAP/{userName}");
		response = restTemplate.exchange(mplSecurityUrl + "/self-service/fetchEmailIdFromLDAP/{userName}", HttpMethod.POST, entity,
				String.class, userName);
		return response.getBody();
	}
	public String fetchDetailsFromLDAP(final String userName) throws Exception {
		ResponseEntity<String> response;

		HttpEntity<String> entity;
		entity = new HttpEntity<String>(getDefaultHeaders());
		LOGGER.info("url of security service ",mplSecurityUrl + "/self-service/fetchDetailsFromLDAP/{userName}");
		response = restTemplate.exchange(mplSecurityUrl + "/self-service/fetchDetailsFromLDAP/{userName}", HttpMethod.POST, entity,
				String.class, userName);
		return response.getBody();
	}
}
