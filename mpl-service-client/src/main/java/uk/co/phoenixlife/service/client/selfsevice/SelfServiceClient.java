package uk.co.phoenixlife.service.client.selfsevice;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import uk.co.phoenixlife.service.dto.SecurityQuestionDTO;
import uk.co.phoenixlife.service.dto.SecurityQuestionsForAUserDTO;

/**
 * The Class AddressFindClient.
 */
@Component
public class SelfServiceClient {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SelfServiceClient.class);


    

    /**
     * Find.
     *
     * @param username
     *            the username
     * @return the SecurityQuestionDTO having questions
     */
    Map<String,String> map=new HashMap<>();
   
    public SecurityQuestionDTO getQuestions(String username)
    {
    	String user=username;
    	
    	map.put("key1", "What is your name1");
    	map.put("key2","What is your name2");
    	map.put("key3","What is your name3");
    	map.put("key4","What is your name4");
    	
    	SecurityQuestionDTO obj=new SecurityQuestionDTO();
    	obj.setSecurityQuestionsMap(map);
    	return obj;
    }

    
  public boolean checkAnswerOfSecurityQuestion(SecurityQuestionsForAUserDTO dtoObj) {
			
	  String questionKey=dtoObj.getQuestionKey();
	  String questionAnswer=dtoObj.getAnswer();
	 
	  
	  if(questionAnswer.equals("hello"))
	  {
		  return true;
	  }else
	  {
		  return false;
	  }
	  	
	}
}
