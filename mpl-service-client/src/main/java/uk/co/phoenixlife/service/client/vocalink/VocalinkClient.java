/*
 * BankWizardClient.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.client.vocalink;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.dto.bankvalidation.BankValidationRequest;
import uk.co.phoenixlife.service.dto.bankvalidation.BankValidationResponse;
import uk.co.phoenixlife.service.dto.bankvalidation.BankValidationService;


/**
 * BankWizardClient.java
 */
@Component
public class VocalinkClient {
	
	/** The rest template. */
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${undertow.bankvalidation}")
    private String vocalinkClientURL;
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(VocalinkClient.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see uk.co.phoenixlife.service.dto.bankvalidation.BankValidationService#
	 * validateBankDetails(uk.co.phoenixlife.service.dto.bankvalidation.
	 * BankValidationRequest, java.lang.String)
	 */

	public BankValidationResponse validateBankDetails(final BankValidationRequest vocalinkRequest, final String xGUID)
			throws Exception {

		
		HttpHeaders headers;
		ResponseEntity<BankValidationResponse> response;
		HttpEntity<Object> requestEntity;

		headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set(MPLConstants.XGUIDKEYFORAPI, xGUID);

		requestEntity = new HttpEntity<Object>(vocalinkRequest, headers);
		LOGGER.debug("Initiating Vocalink Call");
		response = restTemplate.exchange(vocalinkClientURL.trim().concat(MPLConstants.BANKVALIDATION_CONTEXT), HttpMethod.POST, requestEntity,
				BankValidationResponse.class);
		LOGGER.debug("Vocalink Call successfull returning result");
		return response.getBody();
	}
}

	
