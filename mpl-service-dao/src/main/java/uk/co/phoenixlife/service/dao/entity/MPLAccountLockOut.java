package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "MPL_ACCOUNT_LOCKOUT")

public class MPLAccountLockOut {

    @Id
    @GeneratedValue
    @Column(name = "MAL_ID")
    private long malId;

    @Column(name = "MCT_USERNAME")
    private String userName;

    @Column(name = "MAL_POL_SEARCH_COUNT")
    private int policySearchCount;

    @Column(name = "MAL_PERS_FAIL_COUNT")
    private int personalFailCount;

    @Column(name = "MAL_VERF_FAIL_COUNT")
    private int verificationFailCount;

    @Column(name = "MAL_ACCT_LOCK_STATUS")
    private char accountLockStatus;

    @Column(name = "MAL_CREATED_ON")
    private Timestamp createdOn;

    @Column(name = "MAL_LAST_UPDATED_ON")
    private Timestamp lastUpdatedOn;

    @Column(name = "MAL_LAST_UPDATED_BY")
    private String lastUpdatedBy;

    /**
     * Gets the malId
     * 
     * @return the malId
     */
    public long getMalId() {
        return malId;
    }

    /**
     * Sets the malId
     * 
     * @param malId
     *            the malId to set
     */
    public void setMalId(final long malId) {
        this.malId = malId;
    }

    /**
     * Gets the userName
     * 
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the userName
     * 
     * @param userName
     *            the userName to set
     */
    public void setUserName(final String userName) {
        this.userName = userName;
    }

    /**
     * Gets the policySearchCount
     * 
     * @return the policySearchCount
     */
    public int getPolicySearchCount() {
        return policySearchCount;
    }

    /**
     * Sets the policySearchCount
     * 
     * @param policySearchCount
     *            the policySearchCount to set
     */
    public void setPolicySearchCount(final int policySearchCount) {
        this.policySearchCount = policySearchCount;
    }

    /**
     * Gets the personalFailCount
     * 
     * @return the personalFailCount
     */
    public int getPersonalFailCount() {
        return personalFailCount;
    }

    /**
     * Sets the personalFailCount
     * 
     * @param personalFailCount
     *            the personalFailCount to set
     */
    public void setPersonalFailCount(final int personalFailCount) {
        this.personalFailCount = personalFailCount;
    }

    /**
     * Gets the verificationFailCount
     * 
     * @return the verificationFailCount
     */
    public int getVerificationFailCount() {
        return verificationFailCount;
    }

    /**
     * Sets the verificationFailCount
     * 
     * @param verificationFailCount
     *            the verificationFailCount to set
     */
    public void setVerificationFailCount(final int verificationFailCount) {
        this.verificationFailCount = verificationFailCount;
    }

    /**
     * Gets the accountLockStatus
     * 
     * @return the accountLockStatus
     */
    public char getAccountLockStatus() {
        return accountLockStatus;
    }

    /**
     * Sets the accountLockStatus
     * 
     * @param accountLockStatus
     *            the accountLockStatus to set
     */
    public void setAccountLockStatus(final char accountLockStatus) {
        this.accountLockStatus = accountLockStatus;
    }

    /**
     * Gets the createdOn
     * 
     * @return the createdOn
     */
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    /**
     * Sets the createdOn
     * 
     * @param createdOn
     *            the createdOn to set
     */
    public void setCreatedOn(final Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * Gets the lastUpdatedOn
     * 
     * @return the lastUpdatedOn
     */
    public Timestamp getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    /**
     * Sets the lastUpdatedOn
     * 
     * @param lastUpdatedOn
     *            the lastUpdatedOn to set
     */
    public void setLastUpdatedOn(final Timestamp lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    /**
     * Gets the lastUpdatedBy
     * 
     * @return the lastUpdatedBy
     */
    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    /**
     * Sets the lastUpdatedBy
     * 
     * @param lastUpdatedBy
     *            the lastUpdatedBy to set
     */
    public void setLastUpdatedBy(final String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

}
