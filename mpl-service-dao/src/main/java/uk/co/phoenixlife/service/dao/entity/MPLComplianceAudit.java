/*
 * MPLComplianceAudit.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnTransformer;

/**
 * MPLComplianceAudit.java
 */
@Entity
@Table(name = "MPL_COMPLIANCE_AUDIT")
@IdClass(MPLComplianceAudit.MPLComplianceAuditId.class)
public class MPLComplianceAudit {

    @Id
    @Column(name = "MCA_SESSION_ID")
    private String sessionId;

    @Id
    @Column(name = "MCA_EVENT_CODE")
    private String eventCode;

    @Column(name = "MCA_EVENT_VAL")
    @ColumnTransformer(
            read = "CONVERT(VARCHAR(500), DecryptByKey(MCA_EVENT_VAL))",
            write = "EncryptByKey (Key_GUID('MPLPortalDBSymKey'), ?)")
    private byte[] eventVal;

    @Id
    @Column(name = "MCA_EVENT_TS")
    private Timestamp eventTs;

    /**
     * MPLComplianceAuditId.java
     */
    public static class MPLComplianceAuditId implements Serializable {

        /** long */
        private static final long serialVersionUID = 1L;

        private String sessionId;
        private String eventCode;
        private Timestamp eventTs;

        /**
         * Gets the sessionId
         *
         * @return the sessionId
         */
        public String getSessionId() {
            return sessionId;
        }

        /**
         * Sets the sessionId
         *
         * @param psessionId
         *            the sessionId to set
         */
        public void setSessionId(final String psessionId) {
            sessionId = psessionId;
        }

        /**
         * Gets the eventCode
         *
         * @return the eventCode
         */
        public String getEventCode() {
            return eventCode;
        }

        /**
         * Sets the eventCode
         *
         * @param peventCode
         *            the eventCode to set
         */
        public void setEventCode(final String peventCode) {
            eventCode = peventCode;
        }

        /**
         * Gets the eventTs
         *
         * @return the eventTs
         */
        public Timestamp getEventTs() {
            return eventTs;
        }

        /**
         * Sets the eventTs
         *
         * @param pEventTs
         *            the eventTs to set
         */
        public void setEventTs(final Timestamp pEventTs) {
            eventTs = pEventTs;
        }

    }

    /**
     * Gets the sessionId
     *
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Sets the sessionId
     *
     * @param psessionId
     *            the sessionId to set
     */
    public void setSessionId(final String psessionId) {
        sessionId = psessionId;
    }

    /**
     * Gets the eventCode
     *
     * @return the eventCode
     */
    public String getEventCode() {
        return eventCode;
    }

    /**
     * Sets the eventCode
     *
     * @param peventCode
     *            the eventCode to set
     */
    public void setEventCode(final String peventCode) {
        eventCode = peventCode;
    }

    /**
     * Gets the eventVal
     *
     * @return the eventVal
     */
    public byte[] getEventVal() {
        return eventVal;
    }

    /**
     * Sets the eventVal
     *
     * @param peventVal
     *            the eventVal to set
     */
    public void setEventVal(final byte[] peventVal) {
        eventVal = peventVal;
    }

    /**
     * Gets the eventTs
     *
     * @return the eventTs
     */
    public Timestamp getEventTs() {
        return eventTs;
    }

    /**
     * Sets the eventTs
     *
     * @param pEventTs
     *            the eventTs to set
     */
    public void setEventTs(final Timestamp pEventTs) {
        eventTs = pEventTs;
    }

}
