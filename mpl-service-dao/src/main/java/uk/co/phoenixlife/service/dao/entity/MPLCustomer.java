/*
 * MPLCustomer.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnTransformer;

/**
 * MPLCustomer.java
 */
@Entity
@Table(name = "MPL_CUSTOMER")
public class MPLCustomer {

    private static final String ENCRYPT_BY_KEY = "EncryptByKey (Key_GUID('MPLPortalDBSymKey'), ?)";

    @Id
    @GeneratedValue
    @Column(name = "MCT_CUSTOMER_ID")
    private long customerId;

    @Column(name = "MCT_BANCS_CUST_NUM")
    private String bancsCustNum;

    @Column(name = "MCT_USERNAME")
    private String userName;

    @Column(name = "MCT_LEGACY_CUST_NUM")
    private String legacyCustNum;

    @Column(name = "MCT_TITLE")
    @ColumnTransformer(
            read = "CONVERT(VARCHAR(6), DecryptByKey(MCT_TITLE))",
            write = ENCRYPT_BY_KEY)
    private byte[] title;

    @Column(name = "MCT_FIRST_NAME")
    @ColumnTransformer(
            read = "CONVERT(VARCHAR(50), DecryptByKey(MCT_FIRST_NAME))",
            write = ENCRYPT_BY_KEY)
    private byte[] firstName;

    @Column(name = "MCT_LAST_NAME")
    @ColumnTransformer(
            read = "CONVERT(VARCHAR(50), DecryptByKey(MCT_LAST_NAME))",
            write = ENCRYPT_BY_KEY)
    private byte[] lastName;

    @Column(name = "MCT_EMAIL_ADDR")
    @ColumnTransformer(
            read = "CONVERT(VARCHAR(260), DecryptByKey(MCT_EMAIL_ADDR))",
            write = ENCRYPT_BY_KEY)
    private byte[] emailAddr;

    @Column(name = "MCT_PRIMARY_PH_NO")
    @ColumnTransformer(
            read = "CONVERT(VARCHAR(15), DecryptByKey(MCT_PRIMARY_PH_NO))",
            write = ENCRYPT_BY_KEY)
    private byte[] primaryPhNo;

    @Column(name = "MCT_PRIMARY_PH_TYPE")
    private String primaryPhType;

    @Column(name = "MCT_SEC_PH_NO")
    @ColumnTransformer(
            read = "CONVERT(VARCHAR(15), DecryptByKey(MCT_SEC_PH_NO))",
            write = ENCRYPT_BY_KEY)
    private byte[] secPhNo;

    @Column(name = "MCT_SEC_PH_TYPE")
    private String secPhType;

    @Column(name = "MCT_ADD_PH_NO")
    @ColumnTransformer(
            read = "CONVERT(VARCHAR(15), DecryptByKey(MCT_ADD_PH_NO))",
            write = ENCRYPT_BY_KEY)
    private byte[] addPhNo;

    @Column(name = "MCT_ADD_PH_TYPE")
    private String addPhType;

    @Column(name = "MCT_NINO")
    @ColumnTransformer(
            read = "CONVERT(VARCHAR(9), DecryptByKey(MCT_NINO))",
            write = ENCRYPT_BY_KEY)
    private byte[] nino;

    @Column(name = "MCT_ELIGIBLE_POLICIES_COUNT")
    private Integer eligiblePoliciesCount;

    @Column(name = "MCT_CREATED_ON")
    private Timestamp createdOn;

    @Column(name = "MCT_CREATED_BY")
    private String createdBy;

    @Column(name = "MCT_LAST_UPDATED_ON")
    private Timestamp lastUpdatedOn;

    @Column(name = "MCT_LAST_UPDATED_BY")
    private String lastUpdatedBy;

    @Column(name = "MCT_ACCT_CREATED_DATE")
    private Timestamp accountCreatedOn;

    /** Entity mapping Start **/

    @OneToMany(mappedBy = "policyCustomer", cascade = CascadeType.ALL)
    private Set<MPLCustomerPolicy> customerPolicies;

    @OneToMany(mappedBy = "updatedCustomer")
    private Set<MPLCustomerUpdateStatus> customerUpdateStatusList;

    @OneToMany(mappedBy = "emailCustomer")
    private Set<MPLEmailStatus> emailStatusList;

    /** Entity mapping End **/

    /**
     * Gets the customerId
     *
     * @return the customerId
     */
    public long getCustomerId() {
        return customerId;
    }

    /**
     * Sets the customerId
     *
     * @param pCustomerId
     *            the customerId to set
     */
    public void setCustomerId(final long pCustomerId) {
        customerId = pCustomerId;
    }

    /**
     * Gets the userName
     *
     * @return the userName
     */
    // public String getUserName() {
    // return userName;
    // }

    /**
     * Sets the userName
     *
     * @param pUserName
     *            the userName to set
     */
    // public void setUserName(final String pUserName) {
    // userName = pUserName;
    // }

    /**
     * Gets the acctCreatedDate
     *
     * @return the acctCreatedDate
     */
    // public Date getAcctCreatedDate() {
    // return acctCreatedDate;
    // }

    /**
     * Sets the acctCreatedDate
     *
     * @param pAcctCreatedDate
     *            the acctCreatedDate to set
     */
    // public void setAcctCreatedDate(final Date pAcctCreatedDate) {
    // acctCreatedDate = pAcctCreatedDate;
    // }

    /**
     * Gets the bancsCustNum
     *
     * @return the bancsCustNum
     */
    public String getBancsCustNum() {
        return bancsCustNum;
    }

    /**
     * Sets the bancsCustNum
     *
     * @param pBancsCustNum
     *            the bancsCustNum to set
     */
    public void setBancsCustNum(final String pBancsCustNum) {
        bancsCustNum = pBancsCustNum;
    }

    /**
     * Gets the legacyCustNum
     *
     * @return the legacyCustNum
     */
    public String getLegacyCustNum() {
        return legacyCustNum;
    }

    /**
     * Sets the legacyCustNum
     *
     * @param pLegacyCustNum
     *            the legacyCustNum to set
     */
    public void setLegacyCustNum(final String pLegacyCustNum) {
        legacyCustNum = pLegacyCustNum;
    }

    /**
     * Gets the title
     *
     * @return the title
     */
    public byte[] getTitle() {
        return title;
    }

    /**
     * Sets the title
     *
     * @param pTitle
     *            the title to set
     */
    public void setTitle(final byte[] pTitle) {
        title = pTitle;
    }

    /**
     * Gets the firstName
     *
     * @return the firstName
     */
    public byte[] getFirstName() {
        return firstName;
    }

    /**
     * Sets the firstName
     *
     * @param pFirstName
     *            the firstName to set
     */
    public void setFirstName(final byte[] pFirstName) {
        firstName = pFirstName;
    }

    /**
     * Gets the lastName
     *
     * @return the lastName
     */
    public byte[] getLastName() {
        return lastName;
    }

    /**
     * Sets the lastName
     *
     * @param pLastName
     *            the lastName to set
     */
    public void setLastName(final byte[] pLastName) {
        lastName = pLastName;
    }

    /**
     * Gets the emailAddr
     *
     * @return the emailAddr
     */
    public byte[] getEmailAddr() {
        return emailAddr;
    }

    /**
     * Sets the emailAddr
     *
     * @param pEmailAddr
     *            the emailAddr to set
     */
    public void setEmailAddr(final byte[] pEmailAddr) {
        emailAddr = pEmailAddr;
    }

    /**
     * Gets the primaryPhNo
     *
     * @return the primaryPhNo
     */
    public byte[] getPrimaryPhNo() {
        return primaryPhNo;
    }

    /**
     * Sets the primaryPhNo
     *
     * @param pPrimaryPhNo
     *            the primaryPhNo to set
     */
    public void setPrimaryPhNo(final byte[] pPrimaryPhNo) {
        primaryPhNo = pPrimaryPhNo;
    }

    /**
     * Gets the primaryPhType
     *
     * @return the primaryPhType
     */
    public String getPrimaryPhType() {
        return primaryPhType;
    }

    /**
     * Sets the primaryPhType
     *
     * @param pPrimaryPhType
     *            the primaryPhType to set
     */
    public void setPrimaryPhType(final String pPrimaryPhType) {
        primaryPhType = pPrimaryPhType;
    }

    /**
     * Gets the secPhNo
     *
     * @return the secPhNo
     */
    public byte[] getSecPhNo() {
        return secPhNo;
    }

    /**
     * Sets the secPhNo
     *
     * @param pSecPhNo
     *            the secPhNo to set
     */
    public void setSecPhNo(final byte[] pSecPhNo) {
        secPhNo = pSecPhNo;
    }

    /**
     * Gets the secPhType
     *
     * @return the secPhType
     */
    public String getSecPhType() {
        return secPhType;
    }

    /**
     * Sets the secPhType
     *
     * @param pSecPhType
     *            the secPhType to set
     */
    public void setSecPhType(final String pSecPhType) {
        secPhType = pSecPhType;
    }

    /**
     * Gets the addPhNo
     *
     * @return the addPhNo
     */
    public byte[] getAddPhNo() {
        return addPhNo;
    }

    /**
     * Sets the addPhNo
     *
     * @param pAddPhNo
     *            the addPhNo to set
     */
    public void setAddPhNo(final byte[] pAddPhNo) {
        addPhNo = pAddPhNo;
    }

    /**
     * Gets the addPhType
     *
     * @return the addPhType
     */
    public String getAddPhType() {
        return addPhType;
    }

    /**
     * Sets the addPhType
     *
     * @param pAddPhType
     *            the addPhType to set
     */
    public void setAddPhType(final String pAddPhType) {
        addPhType = pAddPhType;
    }

    /**
     * Gets the nino
     *
     * @return the nino
     */
    public byte[] getNino() {
        return nino;
    }

    /**
     * Sets the nino
     *
     * @param pNino
     *            the nino to set
     */
    public void setNino(final byte[] pNino) {
        nino = pNino;
    }

    /**
     * Gets the eligiblePoliciesCount
     *
     * @return the eligiblePoliciesCount
     */
    public Integer getEligiblePoliciesCount() {
        return eligiblePoliciesCount;
    }

    /**
     * Sets the eligiblePoliciesCount
     *
     * @param pEligiblePoliciesCount
     *            the eligiblePoliciesCount to set
     */
    public void setEligiblePoliciesCount(final Integer pEligiblePoliciesCount) {
        eligiblePoliciesCount = pEligiblePoliciesCount;
    }

    /**
     * Gets the createdOn
     *
     * @return the createdOn
     */
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    /**
     * Sets the createdOn
     *
     * @param pCreatedOn
     *            the createdOn to set
     */
    public void setCreatedOn(final Timestamp pCreatedOn) {
        createdOn = pCreatedOn;
    }

    /**
     * Gets the createdBy
     *
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the createdBy
     *
     * @param pCreatedBy
     *            the createdBy to set
     */
    public void setCreatedBy(final String pCreatedBy) {
        createdBy = pCreatedBy;
    }

    /**
     * Gets the lastUpdatedOn
     *
     * @return the lastUpdatedOn
     */
    public Timestamp getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    /**
     * Sets the lastUpdatedOn
     *
     * @param pLastUpdatedOn
     *            the lastUpdatedOn to set
     */
    public void setLastUpdatedOn(final Timestamp pLastUpdatedOn) {
        lastUpdatedOn = pLastUpdatedOn;
    }

    /**
     * Gets the lastUpdatedBy
     *
     * @return the lastUpdatedBy
     */
    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    /**
     * Sets the lastUpdatedBy
     *
     * @param pLastUpdatedBy
     *            the lastUpdatedBy to set
     */
    public void setLastUpdatedBy(final String pLastUpdatedBy) {
        lastUpdatedBy = pLastUpdatedBy;
    }

    /**
     * Gets the customerPolicies
     *
     * @return the customerPolicies
     */
    public Set<MPLCustomerPolicy> getCustomerPolicies() {
        return customerPolicies;
    }

    /**
     * Sets the customerPolicies
     *
     * @param pCustomerPolicies
     *            the customerPolicies to set
     */
    public void setCustomerPolicies(final Set<MPLCustomerPolicy> pCustomerPolicies) {
        customerPolicies = pCustomerPolicies;
    }

    /**
     * Gets the customerUpdateStatusList
     *
     * @return the customerUpdateStatusList
     */
    public Set<MPLCustomerUpdateStatus> getCustomerUpdateStatusList() {
        return customerUpdateStatusList;
    }

    /**
     * Sets the customerUpdateStatusList
     *
     * @param pCustomerUpdateStatusList
     *            the customerUpdateStatusList to set
     */
    public void setCustomerUpdateStatusList(final Set<MPLCustomerUpdateStatus> pCustomerUpdateStatusList) {
        customerUpdateStatusList = pCustomerUpdateStatusList;
    }

    /**
     * Gets the emailStatusList
     *
     * @return the emailStatusList
     */
    public Set<MPLEmailStatus> getEmailStatusList() {
        return emailStatusList;
    }

    /**
     * Sets the emailStatusList
     *
     * @param pEmailStatusList
     *            the emailStatusList to set
     */
    public void setEmailStatusList(final Set<MPLEmailStatus> pEmailStatusList) {
        emailStatusList = pEmailStatusList;
    }

    /**
     * Gets the userName
     *
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the userName
     *
     * @param userName
     *            the userName to set
     */
    public void setUserName(final String userName) {
        this.userName = userName;
    }

    /**
     * Gets the accountCreatedOn
     *
     * @return the accountCreatedOn
     */
    public Timestamp getAccountCreatedOn() {
        return accountCreatedOn;
    }

    /**
     * Sets the accountCreatedOn
     *
     * @param accountCreatedOn
     *            the accountCreatedOn to set
     */
    public void setAccountCreatedOn(final Timestamp accountCreatedOn) {
        this.accountCreatedOn = accountCreatedOn;
    }

}
