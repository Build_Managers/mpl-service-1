/*
 * MPLCustomerPolicy.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * MPLCustomerPolicy.java
 */
@Entity
@Table(name = "MPL_CUSTOMER_POLICY")
public class MPLCustomerPolicy {

    @Id
    @GeneratedValue
    @Column(name = "MCP_POLICY_ID")
    private long policyId;

    @Column(name = "MCP_LEGACY_POL_NUM")
    private String legacyPolNum;

    @Column(name = "MCP_BANCS_POL_NUM")
    private String bancsPolNum;

    @Column(name = "MCP_POLICY_LOCKED_STATUS")
    private char policyLockedStatus;

    @Column(name = "MCP_RETIREMENT_PACK_SOURCE")
    private char retirementPackSource;

    @Column(name = "MCP_CREATED_ON")
    private Timestamp createdOn;

    @Column(name = "MCP_CREATED_BY")
    private String createdBy;

    @Column(name = "MCP_LAST_UPDATED_ON")
    private Timestamp lastUpdatedOn;

    @Column(name = "MCP_LAST_UPDATED_BY")
    private String lastUpdatedBy;

    /** Entity mapping Start **/

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MCT_CUSTOMER_ID")
    private MPLCustomer policyCustomer;

    @OneToMany(mappedBy = "documentPolicy")
    private Set<MPLDocumentAudit> policyDocuments;

    @OneToMany(mappedBy = "verificationPolicy")
    private Set<MPLVerificationAttempts> policyVerificationAttempts;

    @OneToOne(mappedBy = "quotePolicy")
    private MPLQuoteDetails policyQuote;

    @OneToOne(mappedBy = "encashmentPolicy")
    private MPLPolicyEncashment policyEncashment;

    /** Entity mapping End **/

    /**
     * Gets the policyId
     *
     * @return the policyId
     */
    public long getPolicyId() {
        return policyId;
    }

    /**
     * Sets the policyId
     *
     * @param pPolicyId
     *            the policyId to set
     */
    public void setPolicyId(final long pPolicyId) {
        policyId = pPolicyId;
    }

    /**
     * Gets the legacyPolNum
     *
     * @return the legacyPolNum
     */
    public String getLegacyPolNum() {
        return legacyPolNum;
    }

    /**
     * Sets the legacyPolNum
     *
     * @param pLegacyPolNum
     *            the legacyPolNum to set
     */
    public void setLegacyPolNum(final String pLegacyPolNum) {
        legacyPolNum = pLegacyPolNum;
    }

    /**
     * Gets the bancsPolNum
     *
     * @return the bancsPolNum
     */
    public String getBancsPolNum() {
        return bancsPolNum;
    }

    /**
     * Sets the bancsPolNum
     *
     * @param pBancsPolNum
     *            the bancsPolNum to set
     */
    public void setBancsPolNum(final String pBancsPolNum) {
        bancsPolNum = pBancsPolNum;
    }

    /**
     * Gets the policyLockedStatus
     *
     * @return the policyLockedStatus
     */
    public char getPolicyLockedStatus() {
        return policyLockedStatus;
    }

    /**
     * Sets the policyLockedStatus
     *
     * @param pPolicyLockedStatus
     *            the policyLockedStatus to set
     */
    public void setPolicyLockedStatus(final char pPolicyLockedStatus) {
        policyLockedStatus = pPolicyLockedStatus;
    }

    /**
     * Gets the retirementPackSource
     *
     * @return the retirementPackSource
     */
    public char getRetirementPackSource() {
        return retirementPackSource;
    }

    /**
     * Sets the retirementPackSource
     *
     * @param pRetirementPackSource
     *            the retirementPackSource to set
     */
    public void setRetirementPackSource(final char pRetirementPackSource) {
        retirementPackSource = pRetirementPackSource;
    }

    /**
     * Gets the createdOn
     *
     * @return the createdOn
     */
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    /**
     * Sets the createdOn
     *
     * @param pCreatedOn
     *            the createdOn to set
     */
    public void setCreatedOn(final Timestamp pCreatedOn) {
        createdOn = pCreatedOn;
    }

    /**
     * Gets the createdBy
     *
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the createdBy
     *
     * @param pCreatedBy
     *            the createdBy to set
     */
    public void setCreatedBy(final String pCreatedBy) {
        createdBy = pCreatedBy;
    }

    /**
     * Gets the lastUpdatedOn
     *
     * @return the lastUpdatedOn
     */
    public Timestamp getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    /**
     * Sets the lastUpdatedOn
     *
     * @param pLastUpdatedOn
     *            the lastUpdatedOn to set
     */
    public void setLastUpdatedOn(final Timestamp pLastUpdatedOn) {
        lastUpdatedOn = pLastUpdatedOn;
    }

    /**
     * Gets the lastUpdatedBy
     *
     * @return the lastUpdatedBy
     */
    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    /**
     * Sets the lastUpdatedBy
     *
     * @param pLastUpdatedBy
     *            the lastUpdatedBy to set
     */
    public void setLastUpdatedBy(final String pLastUpdatedBy) {
        lastUpdatedBy = pLastUpdatedBy;
    }

    /**
     * Gets the policyCustomer
     *
     * @return the policyCustomer
     */
    public MPLCustomer getPolicyCustomer() {
        return policyCustomer;
    }

    /**
     * Sets the policyCustomer
     *
     * @param pPolicyCustomer
     *            the policyCustomer to set
     */
    public void setPolicyCustomer(final MPLCustomer pPolicyCustomer) {
        policyCustomer = pPolicyCustomer;
    }

    /**
     * Gets the policyDocuments
     *
     * @return the policyDocuments
     */

    public Set<MPLDocumentAudit> getPolicyDocuments() {
        return policyDocuments;
    }

    /**
     * Sets the policyDocuments
     *
     * @param pPolicyDocuments
     *            the policyDocuments to set
     */

    public void setPolicyDocuments(final Set<MPLDocumentAudit> pPolicyDocuments) {
        policyDocuments = pPolicyDocuments;
    }

    /**
     * Gets the policyVerificationAttempts
     *
     * @return the policyVerificationAttempts
     */
    public Set<MPLVerificationAttempts> getPolicyVerificationAttempts() {
        return policyVerificationAttempts;
    }

    /**
     * Sets the policyVerificationAttempts
     *
     * @param pPolicyVerificationAttempts
     *            the policyVerificationAttempts to set
     */
    public void setPolicyVerificationAttempts(final Set<MPLVerificationAttempts> pPolicyVerificationAttempts) {
        policyVerificationAttempts = pPolicyVerificationAttempts;
    }

    /**
     * Gets the policyQuote
     *
     * @return the policyQuote
     */
    public MPLQuoteDetails getPolicyQuote() {
        return policyQuote;
    }

    /**
     * Sets the policyQuote
     *
     * @param pPolicyQuote
     *            the policyQuote to set
     */
    public void setPolicyQuote(final MPLQuoteDetails pPolicyQuote) {
        policyQuote = pPolicyQuote;
    }

    /**
     * Gets the policyEncashment
     *
     * @return the policyEncashment
     */
    public MPLPolicyEncashment getPolicyEncashment() {
        return policyEncashment;
    }

    /**
     * Sets the policyEncashment
     *
     * @param pPolicyEncashment
     *            the policyEncashment to set
     */
    public void setPolicyEncashment(final MPLPolicyEncashment pPolicyEncashment) {
        policyEncashment = pPolicyEncashment;
    }

}
