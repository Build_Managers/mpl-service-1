/*
 * MPLCustomerUpdateStatus.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * MPLCustomerUpdateStatus.java
 */
@Entity
@Table(name = "MPL_CUST_UPDATE_STATUS")
public class MPLCustomerUpdateStatus {

    @Id
    @GeneratedValue
    @Column(name = "MUP_ID")
    private long updateId;

    @Column(name = "MCP_POLICY_ID")
    private Long policyId;
    
    @Column(name = "MUP_ATTEMPTS")
    private int attempts;
    
    @Column(name = "MUP_UPDATED_FIELD")
    private char updatedField;

    @Column(name = "MUP_STATUS")
    private char status;


	@Column(name = "MUP_TS")
    private Timestamp timestamp;

    /** Entity mapping Start **/

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MCT_CUSTOMER_ID")
    private MPLCustomer updatedCustomer;

    @OneToMany(mappedBy = "customerUpdateStatus", cascade = CascadeType.ALL)
    private Set<MPLCustomerUpdateAttempt> customerUpdateAttempts = new HashSet<MPLCustomerUpdateAttempt>();

    /** Entity mapping End **/

    /**
     * Gets the updateId
     *
     * @return the updateId
     */
    public long getUpdateId() {
        return updateId;
    }

    /**
     * Sets the updateId
     *
     * @param pUpdateId
     *            the updateId to set
     */
    public void setUpdateId(final long pUpdateId) {
        updateId = pUpdateId;
    }

    /**
     * Gets the policyId
     *
     * @return the policyId
     */
    public Long getPolicyId() {
        return policyId;
    }

    /**
     * Sets the policyId
     *
     * @param pPolicyId
     *            the policyId to set
     */
    public void setPolicyId(final Long pPolicyId) {
        policyId = pPolicyId;
    }
    /**
     * Gets the updatedField
     * @return updatedField
     */
    public char getUpdatedField() {
		return updatedField;
	}
    /**
     *  Sets the updatedField
     * @param updatedField
     */
	public void setUpdatedField(char updatedField) {
		this.updatedField = updatedField;
	}

    /**
     * Gets the attempt
     *
     * @return the attempt
     */
    public int getAttempts() {
        return attempts;
    }

    /**
     * Sets the attempt
     *
     * @param pAttempt
     *            the attempt to set
     */
    public void setAttempts(final int pAttempts) {
        attempts = pAttempts;
    }

    /**
     * Gets the status
     *
     * @return the status
     */
    public char getStatus() {
        return status;
    }

    /**
     * Sets the status
     *
     * @param pStatus
     *            the status to set
     */
    public void setStatus(final char pStatus) {
        status = pStatus;
    }

    /**
     * Gets the timestamp
     *
     * @return the timestamp
     */
    public Timestamp getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the timestamp
     *
     * @param pTimestamp
     *            the timestamp to set
     */
    public void setTimestamp(final Timestamp pTimestamp) {
        timestamp = pTimestamp;
    }

    /**
     * Gets the updatedCustomer
     *
     * @return the updatedCustomer
     */
    public MPLCustomer getUpdatedCustomer() {
        return updatedCustomer;
    }

    /**
     * Sets the updatedCustomer
     *
     * @param pUpdatedCustomer
     *            the updatedCustomer to set
     */
    public void setUpdatedCustomer(final MPLCustomer pUpdatedCustomer) {
        updatedCustomer = pUpdatedCustomer;
    }

    /**
     * Gets the customerUpdateAttempts
     *
     * @return the customerUpdateAttempts
     */
    public Set<MPLCustomerUpdateAttempt> getCustomerUpdateAttempts() {
        return customerUpdateAttempts;
    }

    /**
     * Sets the customerUpdateAttempts
     *
     * @param pCustomerUpdateAttempts
     *            the customerUpdateAttempts to set
     */
    public void setCustomerUpdateAttempts(final Set<MPLCustomerUpdateAttempt> pCustomerUpdateAttempts) {
        customerUpdateAttempts = pCustomerUpdateAttempts;
    }

}
