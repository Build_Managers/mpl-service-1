/*
 * MPLDocumentAudit.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * MPLDocumentAudit.java
 */
@Entity
@Table(name = "MPL_DOCUMENT_AUDIT")
public class MPLDocumentAudit {

    @Id
    @GeneratedValue
    @Column(name = "MDA_DOCUMENT_ID")
    private long documentId;

    @Column(name = "MDA_DOC_TYPE")
    private String docType;

    @Column(name = "MDA_DOC_NAME")
    private String docName;

    @Column(name = "MDA_DOCUMENT_CONTENTS")
    private byte[] documentContents;

    @Column(name = "MDA_CREATED_ON")
    private Timestamp createdOn;

    @Column(name = "MDA_CREATED_BY")
    private String createdBy;

    /** Entity mapping Start **/

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MCP_POLICY_ID")
    private MPLCustomerPolicy documentPolicy;

    @OneToOne(mappedBy = "updatedDocument", cascade = CascadeType.ALL)
    private MPLDocumentUpdateStatus documentUpdateStatus;

    /** Entity mapping End **/

    /**
     * Gets the documentId
     *
     * @return the documentId
     */
    public long getDocumentId() {
        return documentId;
    }

    /**
     * Sets the documentId
     *
     * @param pDocumentId
     *            the documentId to set
     */
    public void setDocumentId(final long pDocumentId) {
        documentId = pDocumentId;
    }

    /**
     * Gets the docType
     *
     * @return the docType
     */
    public String getDocType() {
        return docType;
    }

    /**
     * Sets the docType
     *
     * @param pDocType
     *            the docType to set
     */
    public void setDocType(final String pDocType) {
        docType = pDocType;
    }

    /**
     * Gets the docName
     *
     * @return the docName
     */
    public String getDocName() {
        return docName;
    }

    /**
     * Sets the docName
     *
     * @param pDocName
     *            the docName to set
     */
    public void setDocName(final String pDocName) {
        docName = pDocName;
    }

    /**
     * Gets the documentContents
     *
     * @return the documentContents
     */
    public byte[] getDocumentContents() {
        return documentContents;
    }

    /**
     * Sets the documentContents
     *
     * @param pDocumentContents
     *            the documentContents to set
     */
    public void setDocumentContents(final byte[] pDocumentContents) {
        documentContents = pDocumentContents;
    }

    /**
     * Gets the createdOn
     *
     * @return the createdOn
     */
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    /**
     * Sets the createdOn
     *
     * @param pCreatedOn
     *            the createdOn to set
     */
    public void setCreatedOn(final Timestamp pCreatedOn) {
        createdOn = pCreatedOn;
    }

    /**
     * Gets the createdBy
     *
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the createdBy
     *
     * @param pCreatedBy
     *            the createdBy to set
     */
    public void setCreatedBy(final String pCreatedBy) {
        createdBy = pCreatedBy;
    }

    /**
     * Gets the documentPolicy
     *
     * @return the documentPolicy
     */
    public MPLCustomerPolicy getDocumentPolicy() {
        return documentPolicy;
    }

    /**
     * Sets the documentPolicy
     *
     * @param pDocumentPolicy
     *            the documentPolicy to set
     */
    public void setDocumentPolicy(final MPLCustomerPolicy pDocumentPolicy) {
        documentPolicy = pDocumentPolicy;
    }

    /**
     * Gets the documentUpdateStatus
     *
     * @return the documentUpdateStatus
     */
    public MPLDocumentUpdateStatus getDocumentUpdateStatus() {
        return documentUpdateStatus;
    }

    /**
     * Sets the documentUpdateStatus
     *
     * @param pDocumentUpdateStatus
     *            the documentUpdateStatus to set
     */
    public void setDocumentUpdateStatus(final MPLDocumentUpdateStatus pDocumentUpdateStatus) {
        documentUpdateStatus = pDocumentUpdateStatus;
    }

}
