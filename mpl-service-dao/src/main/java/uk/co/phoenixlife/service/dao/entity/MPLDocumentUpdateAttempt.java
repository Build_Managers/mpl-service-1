/*
 * MPLDocumentUpdateAttempt.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * MPLDocumentUpdateAttempt.java
 */
@Entity
@Table(name = "MPL_DOC_UPDATE_ATTEMPT")
public class MPLDocumentUpdateAttempt {

    @Id
    @GeneratedValue
    @Column(name = "MDT_ID")
    private long attemptId;

    @Column(name = "MDT_SENT_FLG")
    private char sentFlag;

    @Column(name = "MDT_SENT_TS")
    private Timestamp sentTmst;

    @Column(name = "MDT_ACK_FLG")
    private char ackFlag;

    @Column(name = "MDT_ACK_TS")
    private Timestamp ackTmst;

    @Column(name = "MDT_FAILURE_REASON")
    private String failureReason;

    /** Entity mapping Start **/

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MDU_ID")
    private MPLDocumentUpdateStatus documentUpdateStatus;

    /** Entity mapping End **/

    /**
     * Gets the attemptId
     *
     * @return the attemptId
     */
    public long getAttemptId() {
        return attemptId;
    }

    /**
     * Sets the attemptId
     *
     * @param pAttemptId
     *            the attemptId to set
     */
    public void setAttemptId(final long pAttemptId) {
        attemptId = pAttemptId;
    }

    /**
     * Gets the sentFlag
     *
     * @return the sentFlag
     */
    public char getSentFlag() {
        return sentFlag;
    }

    /**
     * Sets the sentFlag
     *
     * @param pSentFlag
     *            the sentFlag to set
     */
    public void setSentFlag(final char pSentFlag) {
        sentFlag = pSentFlag;
    }

    /**
     * Gets the sentTmst
     *
     * @return the sentTmst
     */
    public Timestamp getSentTmst() {
        return sentTmst;
    }

    /**
     * Sets the sentTmst
     *
     * @param pSentTmst
     *            the sentTmst to set
     */
    public void setSentTmst(final Timestamp pSentTmst) {
        sentTmst = pSentTmst;
    }

    /**
     * Gets the ackFlag
     *
     * @return the ackFlag
     */
    public char getAckFlag() {
        return ackFlag;
    }

    /**
     * Sets the ackFlag
     *
     * @param pAckFlag
     *            the ackFlag to set
     */
    public void setAckFlag(final char pAckFlag) {
        ackFlag = pAckFlag;
    }

    /**
     * Gets the ackTmst
     *
     * @return the ackTmst
     */
    public Timestamp getAckTmst() {
        return ackTmst;
    }

    /**
     * Sets the ackTmst
     *
     * @param pAckTmst
     *            the ackTmst to set
     */
    public void setAckTmst(final Timestamp pAckTmst) {
        ackTmst = pAckTmst;
    }

    /**
     * Gets the failureReason
     *
     * @return the failureReason
     */
    public String getFailureReason() {
        return failureReason;
    }

    /**
     * Sets the failureReason
     *
     * @param pFailureReason
     *            the failureReason to set
     */
    public void setFailureReason(final String pFailureReason) {
        failureReason = pFailureReason;
    }

    /**
     * Gets the documentUpdateStatus
     *
     * @return the documentUpdateStatus
     */
    public MPLDocumentUpdateStatus getDocumentUpdateStatus() {
        return documentUpdateStatus;
    }

    /**
     * Sets the documentUpdateStatus
     *
     * @param pDocumentUpdateStatus
     *            the documentUpdateStatus to set
     */
    public void setDocumentUpdateStatus(final MPLDocumentUpdateStatus pDocumentUpdateStatus) {
        documentUpdateStatus = pDocumentUpdateStatus;
    }

}
