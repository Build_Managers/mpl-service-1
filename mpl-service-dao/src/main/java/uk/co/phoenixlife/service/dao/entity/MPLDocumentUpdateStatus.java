/*
 * MPLDocumentUpdateStatus.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * MPLDocumentUpdateStatus.java
 */
@Entity
@Table(name = "MPL_DOC_UPDATE_STATUS")
public class MPLDocumentUpdateStatus {

    @Id
    @GeneratedValue
    @Column(name = "MDU_ID")
    private long updateId;

    @Column(name = "MDU_ATTEMPTS")
    private int attempts;

    @Column(name = "MDU_STATUS")
    private char status;

    @Column(name = "MDU_TS")
    private Timestamp timestamp;

    /** Entity mapping Start **/

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MDA_DOCUMENT_ID")
    private MPLDocumentAudit updatedDocument;

    @OneToMany(mappedBy = "documentUpdateStatus", cascade = CascadeType.ALL)
    private Set<MPLDocumentUpdateAttempt> documentUpdateAttempts = new HashSet<MPLDocumentUpdateAttempt>();

    /** Entity mapping End **/

    /**
     * Gets the updateId
     *
     * @return the updateId
     */
    public long getUpdateId() {
        return updateId;
    }

    /**
     * Sets the updateId
     *
     * @param pUpdateId
     *            the updateId to set
     */
    public void setUpdateId(final long pUpdateId) {
        updateId = pUpdateId;
    }

    /**
     * Gets the attempt
     *
     * @return the attempt
     */
    public int getAttempts() {
        return attempts;
    }

    /**
     * Sets the attempt
     *
     * @param pAttempt
     *            the attempt to set
     */
    public void setAttempts(final int pAttempts) {
        attempts = pAttempts;
    }

    /**
     * Gets the status
     *
     * @return the status
     */
    public char getStatus() {
        return status;
    }

    /**
     * Sets the status
     *
     * @param pStatus
     *            the status to set
     */
    public void setStatus(final char pStatus) {
        status = pStatus;
    }

    /**
     * Gets the timestamp
     *
     * @return the timestamp
     */
    public Timestamp getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the timestamp
     *
     * @param pTimestamp
     *            the timestamp to set
     */
    public void setTimestamp(final Timestamp pTimestamp) {
        timestamp = pTimestamp;
    }

    /**
     * Gets the updatedDocument
     *
     * @return the updatedDocument
     */
    public MPLDocumentAudit getUpdatedDocument() {
        return updatedDocument;
    }

    /**
     * Sets the updatedDocument
     *
     * @param pUpdatedDocument
     *            the updatedDocument to set
     */
    public void setUpdatedDocument(final MPLDocumentAudit pUpdatedDocument) {
        updatedDocument = pUpdatedDocument;
    }

    /**
     * Gets the documentUpdateAttempts
     *
     * @return the documentUpdateAttempts
     */
    public Set<MPLDocumentUpdateAttempt> getDocumentUpdateAttempts() {
        return documentUpdateAttempts;
    }

    /**
     * Sets the documentUpdateAttempts
     *
     * @param pDocumentUpdateAttempts
     *            the documentUpdateAttempts to set
     */
    public void setDocumentUpdateAttempts(final Set<MPLDocumentUpdateAttempt> pDocumentUpdateAttempts) {
        documentUpdateAttempts = pDocumentUpdateAttempts;
    }

}
