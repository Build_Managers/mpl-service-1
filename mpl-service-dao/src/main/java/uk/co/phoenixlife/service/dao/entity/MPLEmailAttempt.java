/*
 * MPLEmailAttempt.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * MPLEmailAttempt.java
 */
@Entity
@Table(name = "MPL_EMAIL_ATTEMPT")
public class MPLEmailAttempt {

    @Id
    @GeneratedValue
    @Column(name = "MEA_ID")
    private long attemptId;

    @Column(name = "MEA_SENT_FLG")
    private char sentFlag;

    @Column(name = "MEA_SENT_TS")
    private Timestamp sentTmst;

    @Column(name = "MEA_FAILED_TS")
    private Timestamp failedTmst;

    @Column(name = "MEA_FAILURE_REASON")
    private String failureReason;

    /** Entity mapping Start **/

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MEM_ID")
    private MPLEmailStatus emailStatus;

    /** Entity mapping End **/

    /**
     * Gets the attemptId
     *
     * @return the attemptId
     */
    public long getAttemptId() {
        return attemptId;
    }

    /**
     * Sets the attemptId
     *
     * @param pAttemptId
     *            the attemptId to set
     */
    public void setAttemptId(final long pAttemptId) {
        attemptId = pAttemptId;
    }

    /**
     * Gets the sentFlag
     *
     * @return the sentFlag
     */
    public char getSentFlag() {
        return sentFlag;
    }

    /**
     * Sets the sentFlag
     *
     * @param pSentFlag
     *            the sentFlag to set
     */
    public void setSentFlag(final char pSentFlag) {
        sentFlag = pSentFlag;
    }

    /**
     * Gets the sentTmst
     *
     * @return the sentTmst
     */
    public Timestamp getSentTmst() {
        return sentTmst;
    }

    /**
     * Sets the sentTmst
     *
     * @param pSentTmst
     *            the sentTmst to set
     */
    public void setSentTmst(final Timestamp pSentTmst) {
        sentTmst = pSentTmst;
    }

    /**
     * Gets the ackTmst
     *
     * @return the ackTmst
     */
    public Timestamp getFailedTmst() {
        return failedTmst;
    }

    /**
     * Sets the ackTmst
     *
     * @param pAckTmst
     *            the ackTmst to set
     */
    public void setFailedTmst(final Timestamp pFailedTmst) {
    	failedTmst = pFailedTmst;
    }

    /**
     * Gets the failureReason
     *
     * @return the failureReason
     */
    public String getFailureReason() {
        return failureReason;
    }

    /**
     * Sets the failureReason
     *
     * @param pFailureReason
     *            the failureReason to set
     */
    public void setFailureReason(final String pFailureReason) {
        failureReason = pFailureReason;
    }

    /**
     * Gets the emailStatus
     *
     * @return the emailStatus
     */
    public MPLEmailStatus getEmailStatus() {
        return emailStatus;
    }

    /**
     * Sets the emailStatus
     *
     * @param pEmailStatus
     *            the emailStatus to set
     */
    public void setEmailStatus(final MPLEmailStatus pEmailStatus) {
        emailStatus = pEmailStatus;
    }

}
