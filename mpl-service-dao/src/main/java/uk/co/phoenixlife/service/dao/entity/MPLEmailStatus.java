/*
 * MPLEmailStatus.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * MPLEmailStatus.java
 */
@Entity
@Table(name = "MPL_EMAIL_STATUS")
public class MPLEmailStatus {

    @Id
    @GeneratedValue
    @Column(name = "MEM_ID")
    private long statusId;

    @Column(name = "MPE_ENCASHMENT_ID")
    private Long encashmentId;

    @Column(name = "MEM_EMAIL_TYPE")
    private String emailType;

    @Column(name = "MEM_ATTEMPTS")
    private int attempts;

    @Column(name = "MEM_STATUS")
    private char status;

    @Column(name = "MEM_TS")
    private Timestamp timestamp;

    /** Entity mapping Start **/

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MCT_CUSTOMER_ID")
    private MPLCustomer emailCustomer;

    /*
     * @ManyToOne(fetch = FetchType.LAZY)
     *
     * @JoinColumn(name = "MPE_ENCASHMENT_ID") private MPLPolicyEncashment
     * mplPolicyEncashment;
     */

    @OneToMany(mappedBy = "emailStatus", cascade = CascadeType.ALL)
    private Set<MPLEmailAttempt> emailAttempts = new HashSet<MPLEmailAttempt>();

    /** Entity mapping End **/

    /**
     * Gets the statusId
     *
     * @return the statusId
     */
    public long getStatusId() {
        return statusId;
    }

    /**
     * Sets the emailId
     *
     * @param pStatusId
     *            the statusId to set
     */
    public void setStatusId(final long pStatusId) {
        statusId = pStatusId;
    }

    /**
     * Gets the encashmentId
     *
     * @return the encashmentId
     */
    public Long getEncashmentId() {
        return encashmentId;
    }

    /**
     * Sets the encashmentId
     *
     * @param encashmentId
     *            the encashmentId to set
     */
    public void setEncashmentId(final Long encashmentId) {
        this.encashmentId = encashmentId;
    }

    /**
     * Gets the emailType
     *
     * @return the emailType
     */
    public String getEmailType() {
        return emailType;
    }

    /**
     * Sets the emailType
     *
     * @param pEmailType
     *            the emailType to set
     */
    public void setEmailType(final String pEmailType) {
        emailType = pEmailType;
    }

    /**
     * Gets the attempt
     *
     * @return the attempt
     */
    public int getAttempts() {
        return attempts;
    }

    /**
     * Sets the attempt
     *
     * @param pAttempt
     *            the attempt to set
     */
    public void setAttempts(final int pAttempts) {
        attempts = pAttempts;
    }

    /**
     * Gets the status
     *
     * @return the status
     */
    public char getStatus() {
        return status;
    }

    /**
     * Sets the status
     *
     * @param pStatus
     *            the status to set
     */
    public void setStatus(final char pStatus) {
        status = pStatus;
    }

    /**
     * Gets the timestamp
     *
     * @return the timestamp
     */
    public Timestamp getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the timestamp
     *
     * @param pTimestamp
     *            the timestamp to set
     */
    public void setTimestamp(final Timestamp pTimestamp) {
        timestamp = pTimestamp;
    }

    /**
     * Gets the emailCustomer
     *
     * @return the emailCustomer
     */
    public MPLCustomer getEmailCustomer() {
        return emailCustomer;
    }

    /**
     * Sets the emailCustomer
     *
     * @param emailCustomer
     *            the emailCustomer to set
     */
    public void setEmailCustomer(final MPLCustomer emailCustomer) {
        this.emailCustomer = emailCustomer;
    }

    /**
     * Gets the emailAttempts
     *
     * @return the emailAttempts
     */
    public Set<MPLEmailAttempt> getEmailAttempts() {
        return emailAttempts;
    }

    /**
     * Sets the emailAttempts
     *
     * @param pEmailAttempts
     *            the emailAttempts to set
     */
    public void setEmailAttempts(final Set<MPLEmailAttempt> pEmailAttempts) {
        emailAttempts = pEmailAttempts;
    }

}
