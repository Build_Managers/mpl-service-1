/*
 * MPLMiDropOut.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnTransformer;

/**
 * MPLMiDropOut.java
 */
@Entity
@Table(name = "MPL_MI_DROPOUT")
@IdClass(MPLMiDropOut.MPLMiDropOutId.class)
public class MPLMiDropOut {

    @Id
    @Column(name = "MMD_SESSION_ID")
    private String sessionId;

    @Column(name = "MMD_EMAIL_ADDRESS")
    @ColumnTransformer(
            read = "CONVERT(VARCHAR(260), DecryptByKey(MMD_EMAIL_ADDRESS))",
            write = "EncryptByKey (Key_GUID('MPLPortalDBSymKey'), ?)")
    private byte[] emailAddress;

    @Id
    @Column(name = "MMD_PAGE_ID")
    private String pageId;

    @Column(name = "MMD_TIME_SPENT")
    private int timeSpent;

    @Id
    @Column(name = "MMD_DATE")
    private Date dropOutDate;

    @Column(name = "MMD_CREATED_ON")
    private Timestamp createdOn;

    /**
     * MPLMiDropOutId.java
     */
    public static class MPLMiDropOutId implements Serializable {

        /** long */
        private static final long serialVersionUID = 1L;

        private String sessionId;
        private String pageId;
        private Date dropOutDate;

        /**
         * Gets the sessionId
         *
         * @return the sessionId
         */
        public String getSessionId() {
            return sessionId;
        }

        /**
         * Sets the sessionId
         *
         * @param pSessionId
         *            the sessionId to set
         */
        public void setSessionId(final String pSessionId) {
            sessionId = pSessionId;
        }

        /**
         * Gets the pageId
         *
         * @return the pageId
         */
        public String getPageId() {
            return pageId;
        }

        /**
         * Sets the pageId
         *
         * @param pPageId
         *            the pageId to set
         */
        public void setPageId(final String pPageId) {
            pageId = pPageId;
        }

        /**
         * Gets the dropOutDate
         *
         * @return the dropOutDate
         */
        public Date getDropOutDate() {
            return dropOutDate;
        }

        /**
         * Sets the dropOutDate
         *
         * @param pDropOutDate
         *            the dropOutDate to set
         */
        public void setDropOutDate(final Date pDropOutDate) {
            dropOutDate = pDropOutDate;
        }

    }

    /**
     * Gets the sessionId
     *
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Sets the sessionId
     *
     * @param pSessionId
     *            the sessionId to set
     */
    public void setSessionId(final String pSessionId) {
        sessionId = pSessionId;
    }

    /**
     * Gets the emailAddress
     *
     * @return the emailAddress
     */
    public byte[] getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the emailAddress
     *
     * @param pEmailAddress
     *            the emailAddress to set
     */
    public void setEmailAddress(final byte[] pEmailAddress) {
        emailAddress = pEmailAddress;
    }

    /**
     * Gets the pageId
     *
     * @return the pageId
     */
    public String getPageId() {
        return pageId;
    }

    /**
     * Sets the pageId
     *
     * @param pPageId
     *            the pageId to set
     */
    public void setPageId(final String pPageId) {
        pageId = pPageId;
    }

    /**
     * Gets the timeSpent
     *
     * @return the timeSpent
     */
    public int getTimeSpent() {
        return timeSpent;
    }

    /**
     * Sets the timeSpent
     *
     * @param pTimeSpent
     *            the timeSpent to set
     */
    public void setTimeSpent(final int pTimeSpent) {
        timeSpent = pTimeSpent;
    }

    /**
     * Gets the dropOutDate
     *
     * @return the dropOutDate
     */
    public Date getDropOutDate() {
        return dropOutDate;
    }

    /**
     * Sets the dropOutDate
     *
     * @param pDropOutDate
     *            the dropOutDate to set
     */
    public void setDropOutDate(final Date pDropOutDate) {
        dropOutDate = pDropOutDate;
    }

    /**
     * Gets the createdOn
     *
     * @return the createdOn
     */
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    /**
     * Sets the createdOn
     *
     * @param pCreatedOn
     *            the createdOn to set
     */
    public void setCreatedOn(final Timestamp pCreatedOn) {
        createdOn = pCreatedOn;
    }

}
