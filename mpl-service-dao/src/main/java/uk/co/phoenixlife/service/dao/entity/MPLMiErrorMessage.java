/*
 * MPLMiErrorMessage.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * MPLMiErrorMessage.java
 */
@Entity
@Table(name = "MPL_MI_ERRORMSG")
@IdClass(value = MPLMiErrorMessage.MPLMiErrorMessageId.class)
public class MPLMiErrorMessage {

    @Id
    @Column(name = "MME_PAGE_ID")
    private String pageId;

    @Id
    @Column(name = "MME_MESSAGE_ID")
    private String messageId;

    @Column(name = "MME_MESSAGE_COUNT")
    private int messageCount;

    @Id
    @Column(name = "MME_DATE")
    private Date errorDate;

    @Column(name = "MME_CREATED_ON")
    private Timestamp createdOn;

    /**
     * MPLMiErrorMessageId.java
     */
    public static class MPLMiErrorMessageId implements Serializable {

        /** long */
        private static final long serialVersionUID = 1L;

        private String pageId;
        private String messageId;
        private Date errorDate;

        /**
         * Gets the pageId
         *
         * @return the pageId
         */
        public String getPageId() {
            return pageId;
        }

        /**
         * Sets the pageId
         *
         * @param pPageId
         *            the pageId to set
         */
        public void setPageId(final String pPageId) {
            pageId = pPageId;
        }

        /**
         * Gets the messageId
         *
         * @return the messageId
         */
        public String getMessageId() {
            return messageId;
        }

        /**
         * Sets the messageId
         *
         * @param pMessageId
         *            the messageId to set
         */
        public void setMessageId(final String pMessageId) {
            messageId = pMessageId;
        }

        /**
         * Gets the errorDate
         *
         * @return the errorDate
         */
        public Date getErrorDate() {
            return errorDate;
        }

        /**
         * Sets the errorDate
         *
         * @param pErrorDate
         *            the errorDate to set
         */
        public void setErrorDate(final Date pErrorDate) {
            errorDate = pErrorDate;
        }

    }

    /**
     * Gets the pageId
     *
     * @return the pageId
     */
    public String getPageId() {
        return pageId;
    }

    /**
     * Sets the pageId
     *
     * @param pPageId
     *            the pageId to set
     */
    public void setPageId(final String pPageId) {
        pageId = pPageId;
    }

    /**
     * Gets the messageId
     *
     * @return the messageId
     */
    public String getMessageId() {
        return messageId;
    }

    /**
     * Sets the messageId
     *
     * @param pMessageId
     *            the messageId to set
     */
    public void setMessageId(final String pMessageId) {
        messageId = pMessageId;
    }

    /**
     * Gets the messageCount
     *
     * @return the messageCount
     */
    public int getMessageCount() {
        return messageCount;
    }

    /**
     * Sets the messageCount
     *
     * @param pMessageCount
     *            the messageCount to set
     */
    public void setMessageCount(final int pMessageCount) {
        messageCount = pMessageCount;
    }

    /**
     * Gets the errorDate
     *
     * @return the errorDate
     */
    public Date getErrorDate() {
        return errorDate;
    }

    /**
     * Sets the errorDate
     *
     * @param pErrorDate
     *            the errorDate to set
     */
    public void setErrorDate(final Date pErrorDate) {
        errorDate = pErrorDate;
    }

    /**
     * Gets the createdOn
     *
     * @return the createdOn
     */
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    /**
     * Sets the createdOn
     *
     * @param pCreatedOn
     *            the createdOn to set
     */
    public void setCreatedOn(final Timestamp pCreatedOn) {
        createdOn = pCreatedOn;
    }

}
