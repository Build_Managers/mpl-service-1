/*
 * MPLPolicyEncashment.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * MPLPolicyEncashment.java
 */
@Entity
@Table(name = "MPL_POLICY_ENCASHMENT")
public class MPLPolicyEncashment {

    @Id
    @GeneratedValue
    @Column(name = "MPE_ENCASHMENT_ID")
    private long encashmentId;

    @Column(name = "MPE_ENCASH_REF_NUM")
    private String encashRefNum;

    @Column(name = "MPE_CREATED_ON")
    private Timestamp createdOn;

    @Column(name = "MPE_CREATED_BY")
    private String createdBy;

    @Column(name = "MPE_TRANSACTION_ID")
    private String transactionId;

    /** Entity mapping Start **/

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MCP_POLICY_ID")
    private MPLCustomerPolicy encashmentPolicy;

    /*
     * @OneToOne(mappedBy = "updatedQuote", cascade = CascadeType.ALL, fetch =
     * FetchType.LAZY) private MPLEmailStatus emailStatus;
     */

    /** Entity mapping End **/

    /**
     * Gets the encashmentId
     *
     * @return the encashmentId
     */
    public long getEncashmentId() {
        return encashmentId;
    }

    /**
     * Sets the encashmentId
     *
     * @param pEncashmentId
     *            the encashmentId to set
     */
    public void setEncashmentId(final long pEncashmentId) {
        encashmentId = pEncashmentId;
    }

    /**
     * Gets the encashRefNum
     *
     * @return the encashRefNum
     */
    public String getEncashRefNum() {
        return encashRefNum;
    }

    /**
     * Sets the encashRefNum
     *
     * @param pEncashRefNum
     *            the encashRefNum to set
     */
    public void setEncashRefNum(final String pEncashRefNum) {
        encashRefNum = pEncashRefNum;
    }

    /**
     * Gets the createdOn
     *
     * @return the createdOn
     */
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    /**
     * Sets the createdOn
     *
     * @param pCreatedOn
     *            the createdOn to set
     */
    public void setCreatedOn(final Timestamp pCreatedOn) {
        createdOn = pCreatedOn;
    }

    /**
     * Gets the createdBy
     *
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the createdBy
     *
     * @param pCreatedBy
     *            the createdBy to set
     */
    public void setCreatedBy(final String pCreatedBy) {
        createdBy = pCreatedBy;
    }

    /**
     * Gets the transactionId
     *
     * @return the transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the transactionId
     *
     * @param pTransactionId
     *            the transactionId to set
     */
    public void setTransactionId(final String pTransactionId) {
        transactionId = pTransactionId;
    }

    /**
     * Gets the encashmentPolicy
     *
     * @return the encashmentPolicy
     */
    public MPLCustomerPolicy getEncashmentPolicy() {
        return encashmentPolicy;
    }

    /**
     * Sets the encashmentPolicy
     *
     * @param pEncashmentPolicy
     *            the encashmentPolicy to set
     */
    public void setEncashmentPolicy(final MPLCustomerPolicy pEncashmentPolicy) {
        encashmentPolicy = pEncashmentPolicy;
    }

}
