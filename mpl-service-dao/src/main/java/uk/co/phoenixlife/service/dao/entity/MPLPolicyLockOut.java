package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MPL_POLICY_LOCKOUT")
public class MPLPolicyLockOut {

    @Id
    @GeneratedValue
    @Column(name = "MPL_ID")
    private long mplId;

    @Column(name = "MPL_LEGACY_POL_NUM")
    private String legacyPolicynumber;

    @Column(name = "MPL_POL_VERIFICATION_COUNT")
    private int policyVerificationCount;

    @Column(name = "MPL_POL_LOCK_STATUS")
    private char policyLockStatus;

    @Column(name = "MPL_CREATED_ON")
    private Timestamp createdOn;

    @Column(name = "MPL_LAST_UPDATED_ON")
    private Timestamp lastUpdatedOn;

    @Column(name = "MPL_LAST_UPDATED_BY")
    private String lastUpdatedBy;

    /**
     * Gets the mplId
     * 
     * @return the mplId
     */
    public long getMplId() {
        return mplId;
    }

    /**
     * Sets the mplId
     * 
     * @param mplId
     *            the mplId to set
     */
    public void setMplId(final long mplId) {
        this.mplId = mplId;
    }

    /**
     * Gets the legacyPolicynumber
     *
     * @return the legacyPolicynumber
     */
    public String getLegacyPolicynumber() {
        return legacyPolicynumber;
    }

    /**
     * Sets the legacyPolicynumber
     *
     * @param legacyPolicynumber
     *            the legacyPolicynumber to set
     */
    public void setLegacyPolicynumber(final String legacyPolicynumber) {
        this.legacyPolicynumber = legacyPolicynumber;
    }

    /**
     * Gets the policyVerificationCount
     *
     * @return the policyVerificationCount
     */
    public int getPolicyVerificationCount() {
        return policyVerificationCount;
    }

    /**
     * Sets the policyVerificationCount
     *
     * @param policyVerificationCount
     *            the policyVerificationCount to set
     */
    public void setPolicyVerificationCount(final int policyVerificationCount) {
        this.policyVerificationCount = policyVerificationCount;
    }

    /**
     * Gets the policyLockStatus
     *
     * @return the policyLockStatus
     */
    public char getPolicyLockStatus() {
        return policyLockStatus;
    }

    /**
     * Sets the policyLockStatus
     *
     * @param policyLockStatus
     *            the policyLockStatus to set
     */
    public void setPolicyLockStatus(final char policyLockStatus) {
        this.policyLockStatus = policyLockStatus;
    }

    /**
     * Gets the createdOn
     *
     * @return the createdOn
     */
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    /**
     * Sets the createdOn
     *
     * @param createdOn
     *            the createdOn to set
     */
    public void setCreatedOn(final Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * Gets the lastUpdatedOn
     *
     * @return the lastUpdatedOn
     */
    public Timestamp getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    /**
     * Sets the lastUpdatedOn
     *
     * @param lastUpdatedOn
     *            the lastUpdatedOn to set
     */
    public void setLastUpdatedOn(final Timestamp lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    /**
     * Gets the lastUpdatedBy
     *
     * @return the lastUpdatedBy
     */
    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    /**
     * Sets the lastUpdatedBy
     *
     * @param lastUpdatedBy
     *            the lastUpdatedBy to set
     */
    public void setLastUpdatedBy(final String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

}
