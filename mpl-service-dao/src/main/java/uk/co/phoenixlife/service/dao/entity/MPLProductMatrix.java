/*
 * MPLProductMatrix.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * MPLProductMatrix.java
@Entity
@Table(name = "MPL_PRODUCT_MATRIX")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY, region = "product_matrix") */

public class MPLProductMatrix {

    @Id
    @Column(name = "MPM_ID")
    private long productRowId;

    @Column(name = "MPM_PRD_CODE")
    private String productCode;

    @Column(name = "MPM_PRD_NAME")
    private String productName;

    @Column(name = "MPM_POLICY_FLG")
    private String policyFlag;

    @Column(name = "MPM_FUND_FLG")
    private char fundFlag;

    @Column(name = "MPM_TRANSFER_FLG")
    private char transferFlag;

    @Column(name = "MPM_DEATH_FLG")
    private char deathFlag;

    @Column(name = "MPM_CURR_PREM_FLG")
    private char currentPremiumFlag;

    @Column(name = "MPM_GAR_FLG")
    private char garFlag;

    @Column(name = "MPM_LIFESTYLE_FLG")
    private char lifestyleFlag;

    @Column(name = "MPM_GMP_FLG")
    private char gmpFlag;

    @Column(name = "MPM_FUND_HOLDING_FLG")
    private char fundHoldingFlag;

    @Column(name = "MPM_CREATED_ON")
    private Timestamp createdOn;

    @Column(name = "MPM_CREATED_BY")
    private String createdBy;

    @Column(name = "MPM_LAST_UPDATED_ON")
    private Timestamp lastUpdatedOn;

    @Column(name = "MPM_LAST_UPDATED_BY")
    private String lastUpdatedBy;

    /**
     * Gets the productRowId
     *
     * @return the productRowId
     */
    public long getProductRowId() {
        return productRowId;
    }

    /**
     * Sets the productRowId
     *
     * @param pProductRowId
     *            the productRowId to set
     */
    public void setProductRowId(final long pProductRowId) {
        productRowId = pProductRowId;
    }

    /**
     * Gets the productCode
     *
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * Sets the productCode
     *
     * @param pProductCode
     *            the productCode to set
     */
    public void setProductCode(final String pProductCode) {
        productCode = pProductCode;
    }

    /**
     * Gets the productName
     *
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Sets the productName
     *
     * @param pProductName
     *            the productName to set
     */
    public void setProductName(final String pProductName) {
        productName = pProductName;
    }

    /**
     * Gets the policyFlag
     *
     * @return the policyFlag
     */
    public String getPolicyFlag() {
        return policyFlag;
    }

    /**
     * Sets the policyFlag
     *
     * @param pPolicyFlag
     *            the policyFlag to set
     */
    public void setPolicyFlag(final String pPolicyFlag) {
        policyFlag = pPolicyFlag;
    }

    /**
     * Gets the fundFlag
     *
     * @return the fundFlag
     */
    public char getFundFlag() {
        return fundFlag;
    }

    /**
     * Sets the fundFlag
     *
     * @param pFundFlag
     *            the fundFlag to set
     */
    public void setFundFlag(final char pFundFlag) {
        fundFlag = pFundFlag;
    }

    /**
     * Gets the transferFlag
     *
     * @return the transferFlag
     */
    public char getTransferFlag() {
        return transferFlag;
    }

    /**
     * Sets the transferFlag
     *
     * @param pTransferFlag
     *            the transferFlag to set
     */
    public void setTransferFlag(final char pTransferFlag) {
        transferFlag = pTransferFlag;
    }

    /**
     * Gets the deathFlag
     *
     * @return the deathFlag
     */
    public char getDeathFlag() {
        return deathFlag;
    }

    /**
     * Sets the deathFlag
     *
     * @param pDeathFlag
     *            the deathFlag to set
     */
    public void setDeathFlag(final char pDeathFlag) {
        deathFlag = pDeathFlag;
    }

    /**
     * Gets the currentPremiumFlag
     *
     * @return the currentPremiumFlag
     */
    public char getCurrentPremiumFlag() {
        return currentPremiumFlag;
    }

    /**
     * Sets the currentPremiumFlag
     *
     * @param pCurrentPremiumFlag
     *            the currentPremiumFlag to set
     */
    public void setCurrentPremiumFlag(final char pCurrentPremiumFlag) {
        currentPremiumFlag = pCurrentPremiumFlag;
    }

    /**
     * Gets the garFlag
     *
     * @return the garFlag
     */
    public char getGarFlag() {
        return garFlag;
    }

    /**
     * Sets the garFlag
     *
     * @param pGarFlag
     *            the garFlag to set
     */
    public void setGarFlag(final char pGarFlag) {
        garFlag = pGarFlag;
    }

    /**
     * Gets the lifestyleFlag
     *
     * @return the lifestyleFlag
     */
    public char getLifestyleFlag() {
        return lifestyleFlag;
    }

    /**
     * Sets the lifestyleFlag
     *
     * @param pLifestyleFlag
     *            the lifestyleFlag to set
     */
    public void setLifestyleFlag(final char pLifestyleFlag) {
        lifestyleFlag = pLifestyleFlag;
    }

    /**
     * Gets the gmpFlag
     *
     * @return the gmpFlag
     */
    public char getGmpFlag() {
        return gmpFlag;
    }

    /**
     * Sets the gmpFlag
     *
     * @param pGmpFlag
     *            the gmpFlag to set
     */
    public void setGmpFlag(final char pGmpFlag) {
        gmpFlag = pGmpFlag;
    }

    /**
     * Gets the fundHoldingFlag
     *
     * @return the fundHoldingFlag
     */
    public char getFundHoldingFlag() {
        return fundHoldingFlag;
    }

    /**
     * Sets the fundHoldingFlag
     *
     * @param pFundHoldingFlag
     *            the fundHoldingFlag to set
     */
    public void setFundHoldingFlag(final char pFundHoldingFlag) {
        fundHoldingFlag = pFundHoldingFlag;
    }

    /**
     * Gets the createdOn
     *
     * @return the createdOn
     */
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    /**
     * Sets the createdOn
     *
     * @param pCreatedOn
     *            the createdOn to set
     */
    public void setCreatedOn(final Timestamp pCreatedOn) {
        createdOn = pCreatedOn;
    }

    /**
     * Gets the createdBy
     *
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the createdBy
     *
     * @param pCreatedBy
     *            the createdBy to set
     */
    public void setCreatedBy(final String pCreatedBy) {
        createdBy = pCreatedBy;
    }

    /**
     * Gets the lastUpdatedOn
     *
     * @return the lastUpdatedOn
     */
    public Timestamp getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    /**
     * Sets the lastUpdatedOn
     *
     * @param pLastUpdatedOn
     *            the lastUpdatedOn to set
     */
    public void setLastUpdatedOn(final Timestamp pLastUpdatedOn) {
        lastUpdatedOn = pLastUpdatedOn;
    }

    /**
     * Gets the lastUpdatedBy
     *
     * @return the lastUpdatedBy
     */
    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    /**
     * Sets the lastUpdatedBy
     *
     * @param pLastUpdatedBy
     *            the lastUpdatedBy to set
     */
    public void setLastUpdatedBy(final String pLastUpdatedBy) {
        lastUpdatedBy = pLastUpdatedBy;
    }

}
