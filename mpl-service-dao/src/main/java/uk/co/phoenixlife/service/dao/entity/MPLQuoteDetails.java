/*
 * MPLQuoteDetails.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnTransformer;

/**
 * MPLQuoteDetails.java
 */
@Entity
@Table(name = "MPL_QUOTE_DETAILS")
public class MPLQuoteDetails {

    @Id
    @GeneratedValue
    @Column(name = "MQD_QUOTE_ID")
    private long quoteId;

    @Column(name = "MQD_PAYMENT_METHOD")
    private char paymentMethod;

    @Column(name = "MQD_SORT_CODE")
    @ColumnTransformer(
            read = "CONVERT(VARCHAR(10), DecryptByKey(MQD_SORT_CODE))",
            write = "EncryptByKey (Key_GUID('MPLPortalDBSymKey'), ?)")
    private byte[] sortCode;

    @Column(name = "MQD_BANK_ACCT_NO")
    @ColumnTransformer(
            read = "CONVERT(VARCHAR(10), DecryptByKey(MQD_BANK_ACCT_NO))",
            write = "EncryptByKey (Key_GUID('MPLPortalDBSymKey'), ?)")
    private byte[] bankAcctNo;

    @Column(name = "MQD_ROLL_NO")
    @ColumnTransformer(
            read = "CONVERT(VARCHAR(10), DecryptByKey(MQD_ROLL_NO))",
            write = "EncryptByKey (Key_GUID('MPLPortalDBSymKey'), ?)")
    private byte[] rollNo;

    @Column(name = "MQD_CASHIN_TYPE")
    private String cashInType;

    @Column(name = "MQD_CONTACT_CUSTOMER")
    private char contactCustomer;

    @Column(name = "MQD_CREATED_ON")
    private Timestamp createdOn;

    @Column(name = "MQD_CREATED_BY")
    private String createdBy;

    /** Entity mapping Start **/

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MCP_POLICY_ID")
    private MPLCustomerPolicy quotePolicy;

    @OneToOne(mappedBy = "updatedQuote", cascade = CascadeType.ALL)
    private MPLQuoteUpdateStatus quoteUpdateStatus;

    /** Entity mapping End **/

    /**
     * Gets the quoteId
     *
     * @return the quoteId
     */
    public long getQuoteId() {
        return quoteId;
    }

    /**
     * Sets the quoteId
     *
     * @param pQuoteId
     *            the quoteId to set
     */
    public void setQuoteId(final long pQuoteId) {
        quoteId = pQuoteId;
    }

    /**
     * Gets the paymentMethod
     *
     * @return the paymentMethod
     */
    public char getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets the paymentMethod
     *
     * @param pPaymentMethod
     *            the paymentMethod to set
     */
    public void setPaymentMethod(final char pPaymentMethod) {
        paymentMethod = pPaymentMethod;
    }

    /**
     * Gets the sortCode
     *
     * @return the sortCode
     */
    public byte[] getSortCode() {
        return sortCode;
    }

    /**
     * Sets the sortCode
     *
     * @param pSortCode
     *            the sortCode to set
     */
    public void setSortCode(final byte[] pSortCode) {
        sortCode = pSortCode;
    }

    /**
     * Gets the bankAcctNo
     *
     * @return the bankAcctNo
     */
    public byte[] getBankAcctNo() {
        return bankAcctNo;
    }

    /**
     * Sets the bankAcctNo
     *
     * @param pBankAcctNo
     *            the bankAcctNo to set
     */
    public void setBankAcctNo(final byte[] pBankAcctNo) {
        bankAcctNo = pBankAcctNo;
    }

    /**
     * Gets the rollNo
     *
     * @return the rollNo
     */
    public byte[] getRollNo() {
        return rollNo;
    }

    /**
     * Sets the rollNo
     *
     * @param pRollNo
     *            the rollNo to set
     */
    public void setRollNo(final byte[] pRollNo) {
        rollNo = pRollNo;
    }

    /**
     * Gets the cashInType
     *
     * @return the cashInType
     */
    public String getCashInType() {
        return cashInType;
    }

    /**
     * Sets the cashInType
     *
     * @param pCashInType
     *            the cashInType to set
     */
    public void setCashInType(final String pCashInType) {
        cashInType = pCashInType;
    }

    /**
     * Gets the contactCustomer
     *
     * @return the contactCustomer
     */
    public char getContactCustomer() {
        return contactCustomer;
    }

    /**
     * Sets the contactCustomer
     *
     * @param pContactCustomer
     *            the contactCustomer to set
     */
    public void setContactCustomer(final char pContactCustomer) {
        contactCustomer = pContactCustomer;
    }

    /**
     * Gets the createdOn
     *
     * @return the createdOn
     */
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    /**
     * Sets the createdOn
     *
     * @param pCreatedOn
     *            the createdOn to set
     */
    public void setCreatedOn(final Timestamp pCreatedOn) {
        createdOn = pCreatedOn;
    }

    /**
     * Gets the createdBy
     *
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the createdBy
     *
     * @param pCreatedBy
     *            the createdBy to set
     */
    public void setCreatedBy(final String pCreatedBy) {
        createdBy = pCreatedBy;
    }

    /**
     * Gets the quotePolicy
     *
     * @return the quotePolicy
     */
    public MPLCustomerPolicy getQuotePolicy() {
        return quotePolicy;
    }

    /**
     * Sets the quotePolicy
     *
     * @param pQuotePolicy
     *            the quotePolicy to set
     */
    public void setQuotePolicy(final MPLCustomerPolicy pQuotePolicy) {
        quotePolicy = pQuotePolicy;
    }

    /**
     * Gets the quoteUpdateStatus
     *
     * @return the quoteUpdateStatus
     */
    public MPLQuoteUpdateStatus getQuoteUpdateStatus() {
        return quoteUpdateStatus;
    }

    /**
     * Sets the quoteUpdateStatus
     *
     * @param pQuoteUpdateStatus
     *            the quoteUpdateStatus to set
     */
    public void setQuoteUpdateStatus(final MPLQuoteUpdateStatus pQuoteUpdateStatus) {
        quoteUpdateStatus = pQuoteUpdateStatus;
    }

}
