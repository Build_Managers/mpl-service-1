/*
 * MPLQuoteUpdateStatus.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * MPLQuoteUpdateStatus.java
 */
@Entity
@Table(name = "MPL_QUOTE_UPDATE_STATUS")
public class MPLQuoteUpdateStatus {

    @Id
    @GeneratedValue
    @Column(name = "MQU_ID")
    private long updateId;

    @Column(name = "MQU_ATTEMPTS")
    private int attempts;

    @Column(name = "MQU_STATUS")
    private char status;

    @Column(name = "MQU_TS")
    private Timestamp timestamp;

    /** Entity mapping Start **/

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MQD_QUOTE_ID")
    private MPLQuoteDetails updatedQuote;

    @OneToMany(mappedBy = "quoteUpdateStatus", cascade = CascadeType.ALL)
    private Set<MPLQuoteUpdateAttempt> quoteUpdateAttempts = new HashSet<MPLQuoteUpdateAttempt>();

    /** Entity mapping End **/

    /**
     * Gets the updateId
     *
     * @return the updateId
     */
    public long getUpdateId() {
        return updateId;
    }

    /**
     * Sets the updateId
     *
     * @param pUpdateId
     *            the updateId to set
     */
    public void setUpdateId(final long pUpdateId) {
        updateId = pUpdateId;
    }

    /**
     * Gets the attempt
     *
     * @return the attempt
     */
    public int getAttempts() {
        return attempts;
    }

    /**
     * Sets the attempt
     *
     * @param pAttempt
     *            the attempt to set
     */
    public void setAttempt(final int pAttempts) {
        attempts = pAttempts;
    }

    /**
     * Gets the status
     *
     * @return the status
     */
    public char getStatus() {
        return status;
    }

    /**
     * Sets the status
     *
     * @param pStatus
     *            the status to set
     */
    public void setStatus(final char pStatus) {
        status = pStatus;
    }

    /**
     * Gets the timestamp
     *
     * @return the timestamp
     */
    public Timestamp getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the timestamp
     *
     * @param pTimestamp
     *            the timestamp to set
     */
    public void setTimestamp(final Timestamp pTimestamp) {
        timestamp = pTimestamp;
    }

    /**
     * Gets the updatedQuote
     *
     * @return the updatedQuote
     */
    public MPLQuoteDetails getUpdatedQuote() {
        return updatedQuote;
    }

    /**
     * Sets the updatedQuote
     *
     * @param pUpdatedQuote
     *            the updatedQuote to set
     */
    public void setUpdatedQuote(final MPLQuoteDetails pUpdatedQuote) {
        updatedQuote = pUpdatedQuote;
    }

    /**
     * Gets the quoteUpdateAttempts
     *
     * @return the quoteUpdateAttempts
     */
    public Set<MPLQuoteUpdateAttempt> getQuoteUpdateAttempts() {
        return quoteUpdateAttempts;
    }

    /**
     * Sets the quoteUpdateAttempts
     *
     * @param pQuoteUpdateAttempts
     *            the quoteUpdateAttempts to set
     */
    public void setQuoteUpdateAttempts(final Set<MPLQuoteUpdateAttempt> pQuoteUpdateAttempts) {
        quoteUpdateAttempts = pQuoteUpdateAttempts;
    }

}
