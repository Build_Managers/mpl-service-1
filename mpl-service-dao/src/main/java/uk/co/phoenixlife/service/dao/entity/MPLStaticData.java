/*
 * MPLStaticData.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * MPLStaticData.java
 */
@Entity
@Table(name = "MPL_STATIC_DATA")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY, region = "static_data_table")
public class MPLStaticData {

    @Id
    @Column(name = "MSD_STATIC_DATA_ID")
    private long staticDataId;

    @Column(name = "MSD_STATIC_DATA_GROUP")
    private String staticDataGroup;

    @Column(name = "MSD_STATIC_DATA_KEY")
    private String staticDataKey;

    @Column(name = "MSD_STATIC_DATA_VALUE")
    private String staticDataValue;

    @Column(name = "MSD_QUESTION_LABEL")
    private String questionLabel;

    @Column(name = "MSD_CREATED_ON")
    private Timestamp createdOn;

    @Column(name = "MSD_CREATED_BY")
    private String createdBy;

    @Column(name = "MSD_LAST_UPDATED_ON")
    private Timestamp lastUpdatedOn;

    @Column(name = "MSD_LAST_UPDATED_BY")
    private String lastUpdatedBy;

    /**
     * Gets the staticDataId
     *
     * @return the staticDataId
     */
    public long getStaticDataId() {
        return staticDataId;
    }

    /**
     * Sets the staticDataId
     *
     * @param pStaticDataId
     *            the staticDataId to set
     */
    public void setStaticDataId(final long pStaticDataId) {
        staticDataId = pStaticDataId;
    }

    /**
     * Gets the staticDataGroup
     *
     * @return the staticDataGroup
     */
    public String getStaticDataGroup() {
        return staticDataGroup;
    }

    /**
     * Sets the staticDataGroup
     *
     * @param pStaticDataGroup
     *            the staticDataGroup to set
     */
    public void setStaticDataGroup(final String pStaticDataGroup) {
        staticDataGroup = pStaticDataGroup;
    }

    /**
     * Gets the staticDataKey
     *
     * @return the staticDataKey
     */
    public String getStaticDataKey() {
        return staticDataKey;
    }

    /**
     * Sets the staticDataKey
     *
     * @param pStaticDataKey
     *            the staticDataKey to set
     */
    public void setStaticDataKey(final String pStaticDataKey) {
        staticDataKey = pStaticDataKey;
    }

    /**
     * Gets the staticDataValue
     *
     * @return the staticDataValue
     */
    public String getStaticDataValue() {
        return staticDataValue;
    }

    /**
     * Sets the staticDataValue
     *
     * @param pStaticDataValue
     *            the staticDataValue to set
     */
    public void setStaticDataValue(final String pStaticDataValue) {
        staticDataValue = pStaticDataValue;
    }

    /**
     * Gets the questionLabel
     *
     * @return the questionLabel
     */
    public String getQuestionLabel() {
        return questionLabel;
    }

    /**
     * Sets the questionLabel
     *
     * @param pQuestionLabel
     *            the questionLabel to set
     */
    public void setQuestionLabel(final String pQuestionLabel) {
        questionLabel = pQuestionLabel;
    }

    /**
     * Gets the createdOn
     *
     * @return the createdOn
     */
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    /**
     * Sets the createdOn
     *
     * @param pCreatedOn
     *            the createdOn to set
     */
    public void setCreatedOn(final Timestamp pCreatedOn) {
        createdOn = pCreatedOn;
    }

    /**
     * Gets the createdBy
     *
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the createdBy
     *
     * @param pCreatedBy
     *            the createdBy to set
     */
    public void setCreatedBy(final String pCreatedBy) {
        createdBy = pCreatedBy;
    }

    /**
     * Gets the lastUpdatedOn
     *
     * @return the lastUpdatedOn
     */
    public Timestamp getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    /**
     * Sets the lastUpdatedOn
     *
     * @param pLastUpdatedOn
     *            the lastUpdatedOn to set
     */
    public void setLastUpdatedOn(final Timestamp pLastUpdatedOn) {
        lastUpdatedOn = pLastUpdatedOn;
    }

    /**
     * Gets the lastUpdatedBy
     *
     * @return the lastUpdatedBy
     */
    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    /**
     * Sets the lastUpdatedBy
     *
     * @param pLastUpdatedBy
     *            the lastUpdatedBy to set
     */
    public void setLastUpdatedBy(final String pLastUpdatedBy) {
        lastUpdatedBy = pLastUpdatedBy;
    }

}
