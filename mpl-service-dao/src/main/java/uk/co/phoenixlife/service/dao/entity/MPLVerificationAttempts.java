/*
 * MPLVerificationAttempts.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * MPLVerificationAttempts.java
 */
@Entity
@Table(name = "MPL_VERIFICATION_ATTEMPTS")
public class MPLVerificationAttempts {

    @Id
    @GeneratedValue
    @Column(name = "MVA_VERIFICATION_ID")
    private long verificationId;

    @Column(name = "MVA_QUESTION_ID")
    private String questionId;

    @Column(name = "MVA_QUESTION_SKIPPED")
    private char questionSkipped;

    @Column(name = "MVA_CREATED_ON")
    private Timestamp createdOn;

    @Column(name = "MVA_CREATED_BY")
    private String createdBy;

    /** Entity mapping Start **/

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MCP_POLICY_ID")
    private MPLCustomerPolicy verificationPolicy;

    /** Entity mapping End **/

    /**
     * Gets the verificationId
     *
     * @return the verificationId
     */
    public long getVerificationId() {
        return verificationId;
    }

    /**
     * Sets the verificationId
     *
     * @param pVerificationId
     *            the verificationId to set
     */
    public void setVerificationId(final long pVerificationId) {
        verificationId = pVerificationId;
    }

    /**
     * Gets the questionId
     *
     * @return the questionId
     */
    public String getQuestionId() {
        return questionId;
    }

    /**
     * Sets the questionId
     *
     * @param pQuestionId
     *            the questionId to set
     */
    public void setQuestionId(final String pQuestionId) {
        questionId = pQuestionId;
    }

    /**
     * Gets the questionSkipped
     *
     * @return the questionSkipped
     */
    public char getQuestionSkipped() {
        return questionSkipped;
    }

    /**
     * Sets the questionSkipped
     *
     * @param pQuestionSkipped
     *            the questionSkipped to set
     */
    public void setQuestionSkipped(final char pQuestionSkipped) {
        questionSkipped = pQuestionSkipped;
    }

    /**
     * Gets the createdOn
     *
     * @return the createdOn
     */
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    /**
     * Sets the createdOn
     *
     * @param pCreatedOn
     *            the createdOn to set
     */
    public void setCreatedOn(final Timestamp pCreatedOn) {
        createdOn = pCreatedOn;
    }

    /**
     * Gets the createdBy
     *
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the createdBy
     *
     * @param pCreatedBy
     *            the createdBy to set
     */
    public void setCreatedBy(final String pCreatedBy) {
        createdBy = pCreatedBy;
    }

    /**
     * Gets the verificationPolicy
     *
     * @return the verificationPolicy
     */
    public MPLCustomerPolicy getVerificationPolicy() {
        return verificationPolicy;
    }

    /**
     * Sets the verificationPolicy
     *
     * @param pVerificationPolicy
     *            the verificationPolicy to set
     */
    public void setVerificationPolicy(final MPLCustomerPolicy pVerificationPolicy) {
        verificationPolicy = pVerificationPolicy;
    }

}
