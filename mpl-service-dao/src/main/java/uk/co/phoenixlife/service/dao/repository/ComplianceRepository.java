/*
 * ComplianceRepository.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import uk.co.phoenixlife.service.dao.entity.MPLComplianceAudit;

/**
 * ComplianceRepository.java
 */
@Repository
public interface ComplianceRepository extends JpaRepository<MPLComplianceAudit, MPLComplianceAudit.MPLComplianceAuditId> {

}
