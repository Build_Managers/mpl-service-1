/*
 * ComplianceRepositoryImpl.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.dao.entity.MPLComplianceAudit;

/**
 * Repository implementation class for the MPLComplianceAudit table to save data
 * using the symmetric key encryption and decryption.
 */
@Repository
public class ComplianceRepositoryImpl {

    @PersistenceContext(unitName = "persistenceUnit")
    private EntityManager entityManager;

    @Autowired
    private ComplianceRepository repository;

    /**
     * Method to save the compliance data using the symmetric key encryption.
     *
     * @param complianceData
     *            - complianceData object
     * @return Entity object of MPLComplianceAudit
     */
    @Transactional
    public MPLComplianceAudit saveComplianceData(final MPLComplianceAudit complianceData) {
        /**
         * Open the symmetric key to encrypt the data passed to the MPLCustomer
         * entity. In the DB we have the "MPLPortalDBSymKey" as symmetric key
         * and the symmetric key is encrypted with the certificate
         * "MPLPortalCertificate".
         *
         */
        entityManager.createNativeQuery(MPLConstants.SYMMETRIC_KEY_OPEN).executeUpdate();
        /**
         * The save method of JPA is called to save the customer details.
         * The @ColumnTransformer's write will encrypt the column data before
         * saving the details to the DB.
         */
        MPLComplianceAudit savedData;
        savedData = repository.saveAndFlush(complianceData);
        /**
         * After saving the details to the DB, the symmetric key should be
         * closed.
         */
        entityManager.createNativeQuery(MPLConstants.SYMMETRIC_KEY_CLOSE).executeUpdate();
        return savedData;
    }

}
