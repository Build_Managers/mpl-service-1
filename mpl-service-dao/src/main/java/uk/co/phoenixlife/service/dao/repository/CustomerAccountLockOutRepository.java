package uk.co.phoenixlife.service.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import uk.co.phoenixlife.service.dao.entity.MPLAccountLockOut;

@Repository
public interface CustomerAccountLockOutRepository extends JpaRepository<MPLAccountLockOut, Long> {

    @Query("select malId from MPLAccountLockOut where userName =:userName")
    Long getMalIdOfAccountLockOut(@Param("userName") String userName);
}
