package uk.co.phoenixlife.service.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import uk.co.phoenixlife.service.dao.entity.MPLPolicyLockOut;

@Repository
public interface CustomerPolicyLockOutRepository extends JpaRepository<MPLPolicyLockOut, Long> {

    @Query("select policyLockStatus from MPLPolicyLockOut where legacyPolicynumber =:policyNumber")
    Character policyLockedStatus(@Param("policyNumber") String policyNumber);

    @Query("select mplId from MPLPolicyLockOut where legacyPolicynumber =:policyNumber")
    Long getPolicyLockOutObject(@Param("policyNumber") String policyNumber);
}
