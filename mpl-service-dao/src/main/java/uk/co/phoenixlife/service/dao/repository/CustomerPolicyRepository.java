/*
 * CustomerPolicyRepository.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import uk.co.phoenixlife.service.dao.entity.MPLCustomerPolicy;

/**
 * CustomerPolicyRepository.java
 */
@Repository
public interface CustomerPolicyRepository extends JpaRepository<MPLCustomerPolicy, Long> {

    /**
     * Query to get policy locked status
     *
     * @param policyNumber
     *            - policy number
     * @return - status
     */
    @Query("select pol.policyLockedStatus, cust.customerId, encash.encashmentId from MPLCustomerPolicy "
            + "pol left join pol.policyCustomer cust "
            + "left join pol.policyEncashment encash "
            + "where pol.legacyPolNum =:policyNumber")
    List<Object[]> policyLockedStatus(@Param("policyNumber") String policyNumber);

    /**
     * Query to change status of policy number
     *
     * @param policyId
     *            - policyId
     * @param updatedBy
     *            - updatedBy
     * @param updatedOn
     *            - updatedOn
     */
    @Modifying
    @Query("update MPLCustomerPolicy SET policyLockedStatus='Y', lastUpdatedBy=:updatedBy, lastUpdatedOn=:updatedOn "
            + "where policyId =:policyId")
    void updatePolicy(@Param("policyId") long policyId, @Param("updatedBy") String updatedBy,
            @Param("updatedOn") Timestamp updatedOn);

    @Query("select pol from MPLCustomerPolicy pol inner join pol.policyCustomer cust "
            + "where pol.legacyPolNum =:policyNumber and cust.customerId =:customerId")
    MPLCustomerPolicy getPolicies(@Param("customerId") Long customerId, @Param("policyNumber") String policyNumber);

    @Query("select pol from MPLCustomerPolicy pol inner join pol.policyCustomer cust "
            + "where cust.customerId =:customerId")
    List<MPLCustomerPolicy> getPoliciesByCustomerId(@Param("customerId") Long customerId);

    @Query("SELECT DISTINCT pol FROM MPLCustomerPolicy pol INNER JOIN FETCH pol.policyCustomer cust LEFT JOIN pol.policyQuote quote "
            + "LEFT JOIN quote.quoteUpdateStatus qtUpdt INNER JOIN FETCH pol.policyDocuments doc "
            + "INNER JOIN FETCH doc.documentUpdateStatus docUpdt WHERE ((quote.quoteId IS NULL AND docUpdt.status IN (' ','F')) "
            + "OR (qtUpdt.status='S' AND docUpdt.status IN (' ','F'))) AND docUpdt.attempts <:attempts "
            + "AND docUpdt.timestamp <:maxTmst ")
    List<MPLCustomerPolicy> getFailedPolicyDocumentUpdates(@Param("attempts") int attempts,
            @Param("maxTmst") Timestamp maxTmst);

    @Query("SELECT pol FROM MPLCustomerPolicy pol JOIN FETCH pol.policyCustomer cust WHERE pol.policyId =:policyId")
    MPLCustomerPolicy getEncashmentPolicy(@Param("policyId") long policyId);

    @Query("Select pol.policyId, encash.encashmentId from MPLCustomerPolicy pol LEFT OUTER JOIN pol.policyEncashment encash where pol.bancsPolNum=:bancsPolNum and pol.legacyPolNum=:legacyPolNum")
    List<Object[]> fetchPolicyAndEncashmentId(@Param("bancsPolNum") String bancsPolNum,
            @Param("legacyPolNum") String legacyPolNum);

}
