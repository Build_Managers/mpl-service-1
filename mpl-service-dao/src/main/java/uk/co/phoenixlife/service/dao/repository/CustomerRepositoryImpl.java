/*
 * CustomerRepositoryImpl.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.repository;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dao.entity.MPLCustomer;
import uk.co.phoenixlife.service.dao.entity.MPLCustomerPolicy;
import uk.co.phoenixlife.service.dao.entity.MPLCustomerUpdateStatus;
import uk.co.phoenixlife.service.dao.entity.MPLEmailStatus;

/**
 * Repository implementation class for the MPLCustomer table to fetch and save
 * data using the symmetric key encryption and decryption.
 */
@Repository
public class CustomerRepositoryImpl {

    private static final String CUSTOMER_ID = "customerId";

    @PersistenceContext(unitName = "persistenceUnit")
    private EntityManager entityManager;

    @Autowired
    private CustomerRepository repository;

    @Autowired
    private CustomerUpdateStatusRepository custUpdateRepo;
    @Autowired
    private CustomerPolicyRepository policyRepository;
    @Autowired
    private EmailStatusRepository emailStatusRepo;

    /**
     * Method to save the customer detail using the symmetric key encryption.
     *
     * @param customer
     *            - customer object
     * @return Entity object of MPLCustomer
     */
    @Transactional
    public MPLCustomer saveCustDetail(final MPLCustomer customer) {
        /**
         * Open the symmetric key to encrypt the data passed to the MPLCustomer
         * entity. In the DB we have the "MPLPortalDBSymKey" as symmetric key
         * and the symmetric key is encrypted with the certificate
         * "MPLPortalCertificate".
         *
         */
        entityManager.createNativeQuery(MPLServiceConstants.SYMMETRIC_KEY_OPEN).executeUpdate();
        /**
         * The save method of JPA is called to save the customer details.
         * The @ColumnTransformer's write will encrypt the column data before
         * saving the details to the DB.
         */
        MPLCustomer savedCustomer;
        savedCustomer = repository.saveAndFlush(customer);
        /**
         * After saving the details to the DB, the symmetric key should be
         * closed.
         */
        entityManager.createNativeQuery(MPLServiceConstants.SYMMETRIC_KEY_CLOSE).executeUpdate();
        return savedCustomer;
    }

    /**
     * Method to fetch the customer detail using the symmetric key decryption.
     *
     * @param customerId
     *            - customer id
     * @return MPLCustomer object
     */
    @Transactional
    public MPLCustomer findOne(final Long customerId) {
        /**
         * Open the symmetric key to decrypt the data passed to the MPLCustomer
         * entity. In the DB we have the "MPLPortalDBSymKey" as symmetric key
         * and the symmetric key is encrypted with the certificate
         * "MPLPortalCertificate".
         *
         */
        entityManager.createNativeQuery(MPLServiceConstants.SYMMETRIC_KEY_OPEN).executeUpdate();

        /**
         * findOne() method of JPA is called to fetch the customer details.
         * The @ColumnTransformer's read will decrypt the column data before
         * fetching the details from the DB.
         */
        MPLCustomer customer;
        customer = (MPLCustomer) entityManager
                .createQuery("SELECT cust FROM MPLCustomer cust WHERE cust.customerId =:customerId")
                .setParameter(CUSTOMER_ID, customerId).getSingleResult();
        /**
         * After saving the details to the DB, the symmetric key should be
         * closed.
         */
        entityManager.createNativeQuery(MPLServiceConstants.SYMMETRIC_KEY_CLOSE).executeUpdate();
        return customer;
    }

    /**
     * Method to update nino
     *
     * @param nino
     *            - National Insurance Number
     * @param customerId
     *            - customer id
     * @param updatedBy
     *            - updated by
     * @param updatedOn
     *            - updated timestamp
     */
    @Transactional
    public void updateNino(final String nino, final Long customerId, final String updatedBy, final Timestamp updatedOn) {

        entityManager.createNativeQuery(MPLServiceConstants.SYMMETRIC_KEY_OPEN).executeUpdate();
        entityManager
                .createNativeQuery("UPDATE MPL_CUSTOMER SET MCT_NINO = EncryptByKey (Key_GUID('MPLPortalDBSymKey'), :nino), "
                        + "MCT_LAST_UPDATED_BY =:updatedBy , MCT_LAST_UPDATED_ON =:updatedOn WHERE MCT_CUSTOMER_ID =:customerId")
                .setParameter("nino", nino.getBytes()).setParameter(CUSTOMER_ID, customerId)
                .setParameter("updatedOn", updatedOn).setParameter("updatedBy", updatedBy).executeUpdate();

        entityManager.createNativeQuery(MPLServiceConstants.SYMMETRIC_KEY_CLOSE).executeUpdate();
    }

    @Transactional
    public MPLCustomer fetchCustomer(final String userName) {

        MPLCustomer customer;
        entityManager.createNativeQuery(MPLServiceConstants.SYMMETRIC_KEY_OPEN).executeUpdate();

        customer = (MPLCustomer) entityManager
                .createQuery("SELECT cust FROM MPLCustomer cust WHERE cust.userName =:userName")
                .setParameter("userName", userName).getSingleResult();
        entityManager.createNativeQuery(MPLServiceConstants.SYMMETRIC_KEY_CLOSE).executeUpdate();

        return customer;
    }

    /**
     * Method to get failed attempts for customer details update (encashment)
     *
     * @param attempts
     *            - max attempt
     * @param maxTmst
     *            - max timestamp
     * @return - {@link List}
     */
    @Transactional
    public List<MPLCustomerUpdateStatus> getFailedCustUpdates(final int attempts, final Timestamp maxTmst) {

        List<MPLCustomerUpdateStatus> custFailedUpdates;
        entityManager.createNativeQuery(MPLServiceConstants.SYMMETRIC_KEY_OPEN).executeUpdate();

        custFailedUpdates = custUpdateRepo.getFailedCustUpdates(attempts, maxTmst);

        entityManager.createNativeQuery(MPLServiceConstants.SYMMETRIC_KEY_CLOSE).executeUpdate();

        return custFailedUpdates;
    }

    /**
     * Method to get failed attempts for personal details update
     *
     * @param attempts
     *            - max attempt
     * @param maxTmst
     *            - max timestamp
     * @return - {@link List}
     */
    @Transactional
    public List<MPLCustomerUpdateStatus> getFailedPersonalDetailsUpdates(final int attempts, final Timestamp maxTmst) {

        List<MPLCustomerUpdateStatus> custFailedPersonalDetailsUpdates;
        entityManager.createNativeQuery(MPLServiceConstants.SYMMETRIC_KEY_OPEN).executeUpdate();

        custFailedPersonalDetailsUpdates = custUpdateRepo.getFailedPersonalDetailsUpdates(attempts, maxTmst);

        entityManager.createNativeQuery(MPLServiceConstants.SYMMETRIC_KEY_CLOSE).executeUpdate();

        return custFailedPersonalDetailsUpdates;
    }

    @Transactional
    public List<MPLCustomerPolicy> getFailedDocUpdates(final int attempts, final Timestamp maxTmst) {

        List<MPLCustomerPolicy> docFailedUpdates;
        entityManager.createNativeQuery(MPLServiceConstants.SYMMETRIC_KEY_OPEN).executeUpdate();

        docFailedUpdates = policyRepository.getFailedPolicyDocumentUpdates(attempts, maxTmst);

        entityManager.createNativeQuery(MPLServiceConstants.SYMMETRIC_KEY_CLOSE).executeUpdate();

        return docFailedUpdates;
    }

    /**
     * Query to get all the emails for batch
     *
     * @param attempts
     *            - attempts
     * @param maxTmst
     *            - {@link Timestamp}
     * @return - {@link List}
     */
    @Transactional
    public List<MPLEmailStatus> getEmailsForBatch(final int attempts, final Timestamp maxTmst) {

        List<MPLEmailStatus> emails;

        entityManager.createNativeQuery(MPLServiceConstants.SYMMETRIC_KEY_OPEN).executeUpdate();
        emails = emailStatusRepo.getEmailsForBatch(attempts, maxTmst);
        entityManager.createNativeQuery(MPLServiceConstants.SYMMETRIC_KEY_CLOSE).executeUpdate();

        return emails;
    }

    @Transactional
    public MPLCustomerPolicy getEncashmentPolicy(final long policyId) {

        MPLCustomerPolicy policy;

        entityManager.createNativeQuery(MPLServiceConstants.SYMMETRIC_KEY_OPEN).executeUpdate();
        policy = policyRepository.getEncashmentPolicy(policyId);
        entityManager.createNativeQuery(MPLServiceConstants.SYMMETRIC_KEY_CLOSE).executeUpdate();

        return policy;
    }

}
