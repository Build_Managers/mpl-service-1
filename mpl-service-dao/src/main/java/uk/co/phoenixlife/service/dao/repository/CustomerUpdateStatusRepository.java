/*
 * CustomerUpdateStatusRepository.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import uk.co.phoenixlife.service.dao.entity.MPLCustomerUpdateStatus;

/**
 * CustomerUpdateStatusRepository.java
 */
@Repository
public interface CustomerUpdateStatusRepository extends JpaRepository<MPLCustomerUpdateStatus, Long> {

    /**
     * Query to get failedCustUpdates failed attempt
     *
     * @param attempts
     *            - attempts
     * @param maxTmst
     *            - maxTmst
     * @return - {@link List}
     */
    @Query("SELECT custUpdt FROM MPLCustomerUpdateStatus custUpdt JOIN FETCH custUpdt.updatedCustomer "
            + "WHERE custUpdt.status NOT IN ('S') AND custUpdt.attempts <:attempts AND custUpdt.timestamp <:maxTmst "
            + "AND custUpdt.policyId IS NOT NULL")
    List<MPLCustomerUpdateStatus> getFailedCustUpdates(@Param("attempts") int attempts, @Param("maxTmst") Timestamp maxTmst);

    /**
     * Query to get failedPersonalDetailsUpdates failed attempt
     *
     * @param attempts
     *            - attempts
     * @param maxTmst
     *            - maxTmst
     * @return - {@link List}
     */
    @Query("SELECT custUpdt FROM MPLCustomerUpdateStatus custUpdt JOIN FETCH custUpdt.updatedCustomer "
            + "WHERE custUpdt.status NOT IN ('S') AND custUpdt.attempts <:attempts AND custUpdt.timestamp <:maxTmst "
            + "AND custUpdt.policyId IS NULL")
    List<MPLCustomerUpdateStatus> getFailedPersonalDetailsUpdates(@Param("attempts") int attempts,
            @Param("maxTmst") Timestamp maxTmst);
}
