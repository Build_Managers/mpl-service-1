/*
 * DocumentAuditRepository.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import uk.co.phoenixlife.service.dao.entity.MPLDocumentAudit;

/**
 * DocumentAuditRepository.java
 */
@Repository
public interface DocumentAuditRepository extends JpaRepository<MPLDocumentAudit, Long> {

    /**
     * Query to get document
     *
     * @param policyId
     *            - policyId
     * @param docType
     *            - (AF/RP)
     * @return - {@link List}
     */
    @Query("SELECT doc FROM MPLDocumentAudit doc INNER JOIN doc.documentPolicy pol WHERE pol.policyId =:policyId "
            + "AND doc.docType =:docType ORDER BY doc.createdOn DESC")
    List<MPLDocumentAudit> findDocumentByPolicyIdAndDocType(@Param("policyId") long policyId,
            @Param("docType") String docType);

    /**
     * Method to get document ids for document deletion
     *
     * @param timestamp
     *            - {@link Timestamp}
     * @return - {@link List}
     */
    @Query("SELECT doc.documentId FROM MPLDocumentAudit doc INNER JOIN doc.documentUpdateStatus docUpdt "
            + "WHERE docUpdt.status IN ('S','X') AND doc.documentContents IS NOT NULL AND doc.createdOn <:timestamp")
    List<Long> documentsToBeDeleted(@Param("timestamp") Timestamp timestamp);

    /**
     * Update docContent to NULL (for document updated in BANCs)
     *
     * @param docsToBeDeleted
     *            - {@link List}
     * @return - no of rows updated
     */
    @Modifying
    @Transactional
    @Query("UPDATE MPLDocumentAudit SET documentContents = NULL WHERE documentId IN :docsToBeDeleted")
    int deleteDocumentContent(@Param("docsToBeDeleted") List<Long> docsToBeDeleted);

}
