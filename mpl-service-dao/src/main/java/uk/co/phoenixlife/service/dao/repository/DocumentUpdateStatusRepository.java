/*
 * DocumentUpdateStatusRepository.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import uk.co.phoenixlife.service.dao.entity.MPLDocumentUpdateStatus;

/**
 * DocumentUpdateStatusRepository.java
 */
@Repository
public interface DocumentUpdateStatusRepository extends JpaRepository<MPLDocumentUpdateStatus, Long> {

    // @Query("SELECT DISTINCT docStatus FROM MPLDocumentUpdateStatus docStatus
    // "
    // + "INNER JOIN docStatus.updatedDocument doc WHERE doc.documentId =
    // :docId")
    // MPLDocumentUpdateStatus findDocUpdateStatus(@Param("docId") long docId);

    @Modifying
    @Query("UPDATE MPLDocumentUpdateStatus SET status = ' ' WHERE status='X' AND updateId =:updateId")
    void updateRetirementPackBancsFlag(@Param("updateId") long updateId);

    /*
     * @Query("SELECT doc FROM MPLDocumentAudit doc JOIN doc.documentUpdateStatus docUpdt "
     * +
     * "JOIN doc.documentPolicy pol WHERE docUpdt.status =pol.policyId =:policyId AND doc.docType =:docType AND "
     * ) List<MPLDocumentAudit> findDocument(@Param("policyId") long
     * policyId, @Param("docType") String docType);
     */
}
