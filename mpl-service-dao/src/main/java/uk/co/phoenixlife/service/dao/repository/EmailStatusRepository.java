package uk.co.phoenixlife.service.dao.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import uk.co.phoenixlife.service.dao.entity.MPLEmailStatus;

@Repository
public interface EmailStatusRepository extends JpaRepository<MPLEmailStatus, Long> {

    /**
     * Query to get all emails for batch
     *
     * @param attempts
     *            - attempts
     * @param maxTmst
     *            - {@link Timestamp}
     * @return - {@link List}
     */
    @Query("SELECT emailStatus FROM MPLEmailStatus emailStatus JOIN FETCH emailStatus.emailCustomer "
            + "WHERE emailStatus.status NOT IN('S','X') AND emailStatus.attempts <:attempts AND "
            + "emailStatus.timestamp <:maxTmst")
    List<MPLEmailStatus> getEmailsForBatch(@Param("attempts") int attempts, @Param("maxTmst") Timestamp maxTmst);

    /**
     * Query to update email status (set to ' ' after create quote success)
     */
    @Modifying
    @Query("UPDATE MPLEmailStatus set status = ' ' WHERE encashmentId =:polEncashId")
    void updateEmailStatuse(@Param("polEncashId") long polEncashId);

}
