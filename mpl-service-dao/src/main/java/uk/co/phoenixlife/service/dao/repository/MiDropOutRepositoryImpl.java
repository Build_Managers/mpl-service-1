/*
 * MiDropOutRepositoryImpl.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.repository;

import java.sql.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.dao.entity.MPLMiDropOut;

/**
 * Repository implementation class for the MPLMiDropOut table to fetch and save
 * data using the symmetric key encryption and decryption.
 */
@Repository
public class MiDropOutRepositoryImpl {

    @PersistenceContext(unitName = "persistenceUnit")
    private EntityManager entityManager;

    @Autowired
    private MiDropOutReportRepository repository;

    /**
     * Method to save the dropOutData using the symmetric key encryption.
     *
     * @param dropOutData
     *            - dropOutData object
     */
    @Transactional
    public void saveDropOutData(final MPLMiDropOut dropOutData) {
        /**
         * Open the symmetric key to encrypt the data passed to the MPLCustomer
         * entity. In the DB we have the "MPLPortalDBSymKey" as symmetric key
         * and the symmetric key is encrypted with the certificate
         * "MPLPortalCertificate".
         *
         */
        entityManager.createNativeQuery(MPLConstants.SYMMETRIC_KEY_OPEN).executeUpdate();
        /**
         * The save method of JPA is called to save the customer details.
         * The @ColumnTransformer's write will encrypt the column data before
         * saving the details to the DB.
         */

        repository.saveAndFlush(dropOutData);

        /**
         * After saving the details to the DB, the symmetric key should be
         * closed.
         */
        entityManager.createNativeQuery(MPLConstants.SYMMETRIC_KEY_CLOSE).executeUpdate();
    }

    /**
     * Query to update mi drop data
     *
     * @param time
     *            - time
     * @param emailAddress
     *            - emailAddress
     * @param pageId
     *            - pageId
     * @param sessionId
     *            - sessionId
     * @param dropOutDate
     *            - dropOutDate
     */
    @Transactional
    public void updateDropOutTimeSpent(final int time, final String emailAddress, final String pageId,
            final String sessionId, final Date dropOutDate) {

        entityManager.createNativeQuery(MPLConstants.SYMMETRIC_KEY_OPEN).executeUpdate();

        entityManager
                .createNativeQuery(
                        "UPDATE MPL_MI_DROPOUT SET MMD_TIME_SPENT =:time + MMD_TIME_SPENT, "
                                + "MMD_EMAIL_ADDRESS = EncryptByKey (Key_GUID('MPLPortalDBSymKey'), :emailAddress) "
                                + "WHERE MMD_PAGE_ID =:pageId AND MMD_SESSION_ID =:sessionId AND MMD_DATE =:dropOutDate")
                .setParameter("time", time).setParameter("emailAddress", emailAddress.getBytes())
                .setParameter("pageId", pageId).setParameter("sessionId", sessionId)
                .setParameter("dropOutDate", dropOutDate).executeUpdate();

        entityManager.createNativeQuery(MPLConstants.SYMMETRIC_KEY_CLOSE).executeUpdate();
    }

}
