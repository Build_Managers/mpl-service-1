/*
 * MIReportRepository.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.repository;

import java.sql.Date;
import java.sql.Timestamp;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import uk.co.phoenixlife.service.dao.entity.MPLMiErrorMessage;

/**
 * MIReportRepository.java
 */
@Repository
public interface MiErrorMessageReportRepository
        extends JpaRepository<MPLMiErrorMessage, MPLMiErrorMessage.MPLMiErrorMessageId> {

    /**
     * Query to update error message count
     *
     * @param errorDate
     *            - errorDate
     * @param messageId
     *            - messageId
     * @param pageId
     *            - pageId
     * @param count
     *            - count
     * @param timestamp 
     */
    @Modifying
    @Query("UPDATE MPLMiErrorMessage SET messageCount =:count + messageCount, createdOn=:timestamp WHERE pageId =:pageId "
            + "AND messageId =:messageId AND errorDate =:errorDate")
    void updateErrorMessageCount(@Param("errorDate") Date errorDate, @Param("messageId") String messageId,
            @Param("pageId") String pageId, @Param("count") int count, @Param("timestamp") Timestamp timestamp);

}
