/*
 * PolicyEncashmentRepository.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import uk.co.phoenixlife.service.dao.entity.MPLPolicyEncashment;

/**
 * PolicyEncashmentRepository.java
 */
@Repository
public interface PolicyEncashmentRepository extends JpaRepository<MPLPolicyEncashment, Long> {

    /**
     * Query to check if policy encashed
     *
     * @param policyId
     *            - policyId
     * @return - count
     */
    @Query("SELECT COUNT(polEncash.encashmentId) FROM MPLPolicyEncashment polEncash INNER JOIN polEncash.encashmentPolicy pol "
            + "WHERE pol.policyId =:policyId")
    int checkIfAlreadyEncashed(@Param("policyId") long policyId);

    /**
     * Query to get transaction id for encashment
     *
     * @param policyId
     *            - policy id
     * @return - transaction id
     */
    @Query("SELECT polEncash.transactionId FROM MPLPolicyEncashment polEncash INNER JOIN polEncash.encashmentPolicy pol "
            + "WHERE pol.policyId =:policyId")
    String getEncashmentTransactionId(@Param("policyId") long policyId);

    /**
     * Query to update encahment reference
     *
     * @param referenceNumber
     *            - referenceNumber
     * @param polEncashId
     *            - polEncashId
     */
    @Modifying
    @Query("UPDATE MPLPolicyEncashment set encashRefNum =:referenceNumber WHERE encashmentId =:polEncashId")
    void updateQuoteReference(@Param("referenceNumber") String referenceNumber, @Param("polEncashId") long polEncashId);

}
