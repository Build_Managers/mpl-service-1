/*
 * ProductMatrixRepository.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.repository;

import java.util.List;

import javax.persistence.QueryHint;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import uk.co.phoenixlife.service.dao.entity.MPLProductMatrix;
import uk.co.phoenixlife.service.dto.staticdata.ProductMatrixDTO;

/**
 * ProductMatrixRepository.java
@Repository 
public interface ProductMatrixRepository /*extends JpaRepository<MPLProductMatrix, Long> {
 

    /**
     * Query to get product group with policy flag
     *
     * @return - {@link List}
    
    @Query("SELECT DISTINCT productCode, policyFlag FROM MPLProductMatrix")
    List<Object[]> getAllDistinctProductCodeAndFlag();
 */
    /**
     * Query to get product matrix details
     *
     * @param prodCode
     *            - product code
     * @param polFlag
     *            - policy flag
     * @return - ProductMatrixDTO object
    
    @QueryHints(value = {
            @QueryHint(name = "org.hibernate.cacheable", value = "true"),
            @QueryHint(name = "org.hibernate.cacheMode", value = "NORMAL"),
            @QueryHint(name = "org.hibernate.cacheRegion", value = "product_matrix") })
    @Query("SELECT new uk.co.phoenixlife.service.dto.staticdata.ProductMatrixDTO(fundFlag, transferFlag, "
            + "deathFlag, currentPremiumFlag, garFlag, lifestyleFlag, gmpFlag, fundHoldingFlag) "
            + "FROM MPLProductMatrix WHERE productName =:prodName AND policyFlag =:polFlag")
    ProductMatrixDTO getProductMatricDetail(@Param("prodName") String prodName, @Param("polFlag") String polFlag);
    
} */
