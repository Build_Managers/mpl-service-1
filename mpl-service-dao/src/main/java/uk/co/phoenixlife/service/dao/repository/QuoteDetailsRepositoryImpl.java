/*
 * QuoteDetailsRepositoryImpl.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.repository;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dao.entity.MPLQuoteDetails;
import uk.co.phoenixlife.service.dao.entity.MPLQuoteUpdateStatus;

/**
 * Repository implementation class for the MPLQuoteDetails table to fetch and
 * save data using the symmetric key encryption and decryption.
 */
@Repository
public class QuoteDetailsRepositoryImpl {

    @PersistenceContext(unitName = "persistenceUnit")
    private EntityManager entityManager;

    @Autowired
    private QuoteDetailsRepository repository;
    @Autowired
    private QuoteUpdateStatusRepository qteUpdtStatusRepo;

    /**
     * Method to save the quote detail using the symmetric key encryption.
     *
     * @param quote
     *            - quote object
     * @return Entity object of MPLQuoteDetails
     */
    public MPLQuoteDetails saveQuoteDetail(final MPLQuoteDetails quote) {
        /**
         * Open the symmetric key to encrypt the data passed to the MPLCustomer
         * entity. In the DB we have the "MPLPortalDBSymKey" as symmetric key
         * and the symmetric key is encrypted with the certificate
         * "MPLPortalCertificate".
         *
         */
        entityManager.createNativeQuery(MPLConstants.SYMMETRIC_KEY_OPEN).executeUpdate();
        /**
         * The save method of JPA is called to save the customer details.
         * The @ColumnTransformer's write will encrypt the column data before
         * saving the details to the DB.
         */
        MPLQuoteDetails savedQuote;
        savedQuote = repository.saveAndFlush(quote);
        /**
         * After saving the details to the DB, the symmetric key should be
         * closed.
         */
        entityManager.createNativeQuery(MPLConstants.SYMMETRIC_KEY_CLOSE).executeUpdate();
        return savedQuote;
    }

    /**
     * Method to fetch the quote detail using the symmetric key decryption.
     *
     * @param quoteId
     *            - quote id
     * @return MPLCustomer object
     */
    public MPLQuoteDetails findOne(final Long quoteId) {
        /**
         * Open the symmetric key to decrypt the data passed to the MPLCustomer
         * entity. In the DB we have the "MPLPortalDBSymKey" as symmetric key
         * and the symmetric key is encrypted with the certificate
         * "MPLPortalCertificate".
         *
         */
        entityManager.createNativeQuery(MPLConstants.SYMMETRIC_KEY_OPEN).executeUpdate();
        /**
         * findOne() method of JPA is called to fetch the customer details.
         * The @ColumnTransformer's read will decrypt the column data before
         * fetching the details from the DB.
         */
        MPLQuoteDetails quote;
        quote = repository.findOne(quoteId);
        /**
         * After saving the details to the DB, the symmetric key should be
         * closed.
         */
        entityManager.createNativeQuery(MPLConstants.SYMMETRIC_KEY_CLOSE).executeUpdate();

        return quote;
    }

    /**
     * Method to get quote details for bancs
     *
     * @param policyId
     *            - policyId
     * @return - Object array
     */
    public Object[] findQuoteDetails(final Long quotePolicyId) {

        entityManager.createNativeQuery(MPLConstants.SYMMETRIC_KEY_OPEN).executeUpdate();
        Object[] objArr;
        objArr = (Object[]) entityManager
                .createQuery("SELECT qd, pol.bancsPolNum, cust.bancsCustNum, polEncash.transactionId "
                        + "FROM MPLQuoteDetails qd LEFT JOIN qd.quotePolicy pol LEFT JOIN pol.policyCustomer cust "
                        + "LEFT JOIN pol.policyEncashment polEncash WHERE pol.policyId =:quotePolicyId")
                .setParameter("quotePolicyId", quotePolicyId).getSingleResult();

        entityManager.createNativeQuery(MPLConstants.SYMMETRIC_KEY_CLOSE).executeUpdate();

        return objArr;
    }

    @Transactional
    public List<MPLQuoteUpdateStatus> getFailedQuoteUpdates(final int attempts, final Timestamp maxTmst) {

        List<MPLQuoteUpdateStatus> qteFailedUpdates;
        entityManager.createNativeQuery(MPLServiceConstants.SYMMETRIC_KEY_OPEN).executeUpdate();

        qteFailedUpdates = qteUpdtStatusRepo.getFailedQuoteUpdates(attempts, maxTmst);

        entityManager.createNativeQuery(MPLServiceConstants.SYMMETRIC_KEY_CLOSE).executeUpdate();

        return qteFailedUpdates;
    }

}
