/*
 * QuoteUpdateStatusRepository.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import uk.co.phoenixlife.service.dao.entity.MPLQuoteUpdateStatus;

/**
 * QuoteUpdateStatusRepository.java
 */
@Repository
public interface QuoteUpdateStatusRepository extends JpaRepository<MPLQuoteUpdateStatus, Long> {

    @Query("SELECT qteUpdt FROM MPLQuoteUpdateStatus qteUpdt JOIN FETCH qteUpdt.updatedQuote qte JOIN FETCH qte.quotePolicy pol "
            + "JOIN FETCH pol.policyCustomer cust WHERE qteUpdt.status NOT IN ('S') AND qteUpdt.attempts <:attempts "
            + "AND qteUpdt.timestamp <:maxTmst")
    List<MPLQuoteUpdateStatus> getFailedQuoteUpdates(@Param("attempts") int attempts,
            @Param("maxTmst") Timestamp maxTmst);

    @Query("SELECT DISTINCT customer.customerId FROM MPLCustomerUpdateStatus custUpdateStatus INNER JOIN custUpdateStatus.updatedCustomer customer "
            + "WHERE custUpdateStatus.status NOT IN('S')"
            + "AND custUpdateStatus.attempts<:attempts AND (custUpdateStatus.timestamp<:maxTmst OR custUpdateStatus.timestamp=NULL)")
    List<Long>
        getKeysForEmailPhone(@Param("attempts") int attempts, @Param("maxTmst") Timestamp maxTmst);

    @Modifying
    @Query("UPDATE MPLQuoteUpdateStatus set sentBancsFlg='Y', sentBancsTs=:sentTime, bancsAckFlag=:ackFlag, bancsAckTs=:ackTime, "
            + "attempts=attempts+1 WHERE quoteId =:quoteId")
    int updateCreateQuoteAcknowledgement(@Param("sentTime") Timestamp sentTime, @Param("ackFlag") char ackFlag,
            @Param("ackTime") Timestamp ackTime, @Param("quoteId") long quoteId);

    @Modifying
    @Query("UPDATE MPLQuoteUpdateStatus set status=:statusFlag, attempts=attempts+1, timestamp=:sendTimestamp WHERE updatedQuote.quoteId =:quoteId")
    int updateQuoteStatus(@Param("quoteId") long quoteId, @Param("statusFlag") char statusFlag,
            @Param("sendTimestamp") Timestamp sendTimestamp);

    @Query("SELECT quoteStatus FROM MPLQuoteUpdateStatus quoteStatus LEFT JOIN quoteStatus.updatedQuote quote WHERE quote.quoteId =:quoteId")
    MPLQuoteUpdateStatus findByQuoteId(@Param("quoteId") long quoteId);

}
