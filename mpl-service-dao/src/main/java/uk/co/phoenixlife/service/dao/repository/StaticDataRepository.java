/*
 * StaticDataRepository.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.repository;

import java.util.List;

import javax.persistence.QueryHint;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

import uk.co.phoenixlife.service.dao.entity.MPLStaticData;

/**
 * StaticDataRepository.java
 */
@Repository
public interface StaticDataRepository extends JpaRepository<MPLStaticData, Long> {

    /**
     * Query to get group data
     *
     * @param group
     *            - group name
     * @return - {@link List}
     */
    @QueryHints(value = {
            @QueryHint(name = "org.hibernate.cacheable", value = "true"),
            @QueryHint(name = "org.hibernate.cacheMode", value = "NORMAL"),
            @QueryHint(name = "org.hibernate.cacheRegion", value = "static_data_table") })
    List<MPLStaticData> findByStaticDataGroup(String group);

    /**
     * Query to select unique groups
     *
     * @return - {@link List}
     */
    @Query("SELECT DISTINCT(data.staticDataGroup) FROM MPLStaticData data")
    List<String> getAllDistinctGroups();

}
