/*
 * VerificationAttemptsRepository.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import uk.co.phoenixlife.service.dao.entity.MPLVerificationAttempts;

/**
 * VerificationAttemptsRepository.java
 */
@Repository
public interface VerificationAttemptsRepository extends JpaRepository<MPLVerificationAttempts, Long> {

    /**
     * Query to get locked questions
     *
     * @param policyId
     *            - policyId
     * @return - {@link List}
     */
    @Query("select verify.questionId from MPLVerificationAttempts verify left join verify.verificationPolicy policy "
            + "where policy.policyId =:policyId")
    List<String> findlockedQuestionKey(@Param("policyId") long policyId);
}