--Google Recaptha KEYS:
--For UAT :
INSERT INTO MPLU01.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('Analytics', 'TrackingId', 'UA-97022150-1', NULL, GETDATE(), 'SYSTEM', NULL, NULL)

--For SIT:
INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('Analytics', 'TrackingId', 'UA-96643961-1', NULL, GETDATE(), 'SYSTEM', NULL, NULL)

--For ST:
INSERT INTO MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('Analytics', 'TrackingId', 'UA-96643961-1', NULL, GETDATE(), 'SYSTEM', NULL, NULL)

--For Local:
INSERT INTO MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('Analytics', 'TrackingId', 'UA-98866808-1', NULL, GETDATE(), 'SYSTEM', NULL, NULL)

INSERT INTO dbo.MPL_STATIC_DATA VALUES 
('Recaptcha', 'siteKey', '6LcKiRIUAAAAADZoKbSMIXIAZpEch3OW9yTr1YXV', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('Recaptcha', 'secretKey', '6LcKiRIUAAAAAA_QKn7kBDKF4cb3p6QQy_IsvQch', NULL, GETDATE(), 'SYSTEM', NULL, NULL)

--DML FOR STATIC DATA TABLE --
INSERT INTO DBO.MPL_STATIC_DATA VALUES
-- ID&V-Question Group Data (7) --
('ID&V-Question', 'lastPremPaid', 'What is the amount of the last premium you paid on this policy?', 'LAST PREMIUM PAID', GETDATE(), 'SYSTEM', NULL, NULL),
('ID&V-Question', 'sortCodebankAcct', 'What is the sort code and account number of the account from which you pay this policy contributions?', 'SORT CODE@ACCOUNT NUMBER', GETDATE(), 'SYSTEM', NULL, NULL),
('ID&V-Question', 'polStDate', 'What is the date you started this policy?', 'START DATE', GETDATE(), 'SYSTEM', NULL, NULL),
('ID&V-Question', 'niNo', 'What is your national insurance number?', 'NATIONAL INSURANCE NUMBER', GETDATE(), 'SYSTEM', NULL, NULL),
('ID&V-Question', 'fundName', 'Can you tell us the name of a fund your contributions are invested in?', 'INVESTMENT FUND NAME', GETDATE(), 'SYSTEM', NULL, NULL),
('ID&V-Question', 'benefitName', 'What was the amount you originally transferred into this plan?@What was the amount of the lump sum you paid into this plan?@What was the amount of the lump sum your employer paid into this plan?', 'ORIGINAL INVESTMENT PREMIUM', GETDATE(), 'SYSTEM', NULL, NULL),
('ID&V-Question', 'premiumDueDate', 'What day are the premiums debited from the account?', 'DAY PREMIUMS DEBITED', GETDATE(), 'SYSTEM', NULL, NULL),
-- MPLComplianceCode Group Data (48) --
('MPLComplianceCode', 'PG03_MPLCUSTID', 'MPL Customer Id', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG03_BNSCUSTID', 'BaNCS Unique Customer Number', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG02_POLICYNUM', 'Legacy Policy Number', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG02_EMAILADDR', 'Users email address', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG02_IPADDRESS', 'IP Address of users machine', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG08_RPBTNCLCK', 'Timestamp when the customer clicked ''Download my Retirement Pack'' if applicable', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG08_RPCHCKBOX', 'Timestamp when the customer checked ''I confirm that I have downloaded and saved a copy of my Retirement Pack', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG15_LFTALWLMT', 'Values and timestamp when the customer answers ''By taking your Phoenix Life pensions savings as a cash lump sum from this policy will you exceed the Lifetime Allowance Limit?', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG15_AMTDRPBY5', 'Value and timestamp when the customer answers ''Would you like to be contacted if the amount payable drops by more than 5%?', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG15_IMPCTDAMT', 'Value and timestamp when the customer answers ''Are you happy to continue with your application'' in the section ''Is there anything else that could impact the amount of savings I receive?', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG17_RCVGUIDANC', 'Value and timestamp when the customer answers ''Have you received guidance from Pension Wise about what you could do with the pension savings from this policy?', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG17_RCVADVICE', 'Value and timestamp when the customer answers ''Have you received advice from an independant financial adviser about what you could do with the pension savings from this policy?', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG18_TXIMPLCTN', 'Value and timestamp when the customer answers ''Do you understand the information above'' in the section ''Your application summary', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG18_ISSMALLPT', 'Value and timestamp when the customer answers ''Are you happy with your answers above?'' in the section ''Your application summary', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG18_EDITANSTS', 'Timestamp when the customer edits their answers in the Pension Guidance/Advice Confirmation section in ''Your application summary', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG18_DCLRCHKBX', 'timestamp when the customer checks ''Please tick to confirm your acceptance to this declaration', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG19_AFDWNLDTS', 'Timestamp when the customer clicks to download a copy of the confirmation of their  application as a PDF', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG02_NXTBTNCLK', 'Next button of email page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG03_PRVBTNCLK', 'Previous button of policy details page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG03_NXTBTNCLK', 'Next button of policy details page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG04_PRVBTNCLK', 'Previous button of Personal details page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG04_NXTBTNCLK', 'Next button of Personal details page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG05_PRVBTNCLK', 'Previous button of Contact details page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG05_NXTBTNCLK', 'Next button of Contact details page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG06_PRVBTNCLK', 'Previous button of Verification page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG06_NXTBTNCLK', 'Next button of Verification page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG07_CASHINCLK', 'Cash in application on Dashboard page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG08_NXTBTNCLK', 'Next button of Confirmation page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG08_PRVHPLCLK', 'Go to previous page hyperlink of Confirmation page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG09_NXTBTNCLK', 'Next button of Making sure page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG09_PRVHPLCLK', 'Go to previous page hyperlink of Making sure page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG10_NXTBTNCLK', 'Next button of Guaranteed income page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG10_PRVHPLCLK', 'Go to previous page hyperlink of Guaranteed income page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG11_NXTBTNCLK', 'Next button of Flexible income page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG11_PRVHPLCLK', 'Go to previous page hyperlink of Flexible income page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG12_NXTBTNCLK', 'Next button of Take more then one option page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG12_PRVHPLCLK', 'Go to previous page hyperlink of Take more then one option page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG13_NXTBTNCLK', 'Next button of Keep your pension page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG13_PRVHPLCLK', 'Go to previous page hyperlink of Keep your pension page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG14_NXTBTNCLK', 'Next button of Transfer your pension page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG14_PRVHPLCLK', 'Go to previous page hyperlink of Transfer your pension page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG15_NXTBTNCLK', 'Next button of Take all of your pensions savings page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG15_PRVHPLCLK', 'Go to previous page hyperlink of Take all of your pensions savings page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG16_NXTBTNCLK', 'Next button of Risk associated page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG16_PRVHPLCLK', 'Go to previous page hyperlink of Risk associated page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG17_NXTBTNCLK', 'Next button of Pension guidance and advice page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG17_PRVHPLCLK', 'Go to previous page hyperlink of Pension guidance and advice page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLComplianceCode', 'PG18_SMTBTNCLK', 'Submit button on dashboard personal details page clicked', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
-- MPLNavURLPGID Group Data (19) --
('MPLNavURLPGID', 'index', 'PG01', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLNavURLPGID', 'email', 'PG02', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLNavURLPGID', 'policy_details', 'PG03', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLNavURLPGID', 'personal_details', 'PG04', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLNavURLPGID', 'contact_details', 'PG05', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLNavURLPGID', 'verification', 'PG06', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLNavURLPGID', 'dashboard', 'PG07', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLNavURLPGID', 'confirmation', 'PG08', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLNavURLPGID', 'making_sure_you_made_right_choice', 'PG09', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLNavURLPGID', 'guaranteed_income_for_life', 'PG10', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLNavURLPGID', 'flexible_access', 'PG11', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLNavURLPGID', 'take_more_than_one_option', 'PG12', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLNavURLPGID', 'keep_your_pension_savings', 'PG13', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLNavURLPGID', 'transfer_your_pension_savings', 'PG14', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLNavURLPGID', 'take_all_of_your_pension_savings', 'PG15', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLNavURLPGID', 'risks_of_taking_all_of_my_pension_savings', 'PG16', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLNavURLPGID', 'pension_guidance_and_advice', 'PG17', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLNavURLPGID', 'dashboard_personal_details', 'PG18', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLNavURLPGID', 'encashment_completion', 'PG19', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
-- MPLAllowedPGID Group Data (19) --
('MPLAllowedPGID', 'PG01', 'ALL', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLAllowedPGID', 'PG02', 'PG01,PG02,PG03', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLAllowedPGID', 'PG03', 'PG02,PG03,PG04', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLAllowedPGID', 'PG04', 'PG03,PG04,PG05', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLAllowedPGID', 'PG05', 'PG04,PG05,PG06', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLAllowedPGID', 'PG06', 'PG05,PG06', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLAllowedPGID', 'PG07', 'PG06,PG07,PG08', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLAllowedPGID', 'PG08', 'PG07,PG08,PG09', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLAllowedPGID', 'PG09', 'PG08,PG09,PG10', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLAllowedPGID', 'PG10', 'PG09,PG10,PG11', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLAllowedPGID', 'PG11', 'PG10,PG11,PG12', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLAllowedPGID', 'PG12', 'PG11,PG12,PG13', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLAllowedPGID', 'PG13', 'PG12,PG13,PG14', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLAllowedPGID', 'PG14', 'PG13,PG14,PG15', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLAllowedPGID', 'PG15', 'PG14,PG15,PG16,PG18', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLAllowedPGID', 'PG16', 'PG15,PG16,PG17', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLAllowedPGID', 'PG17', 'PG16,PG17,PG18', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLAllowedPGID', 'PG18', 'PG17,PG18', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLAllowedPGID', 'PG19', 'PG18,PG19', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
-- PaymentMethod Group Data (2) --
('PaymentMethod', 'C', 'Cheque', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('PaymentMethod', 'D', 'Direct Credit', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
-- MPLValidationError Group Data (38) --
('MPLValidationError','E001', 'Please enter a valid email address, e.g example@example.com', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E002', 'Emails must match', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E003', 'Please enter your Policy Number', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E004', 'Please enter a valid policy number', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E005', 'We have been unable to verify your account and your policy account has been locked for your security. Please call our Contact Centre on XXX XXX XXX who will be able to assist', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E006','Your policy or plan number has not been recognised. Please ensure that you enter the number exactly as it shown on your policy documentation, which may include using the symbol '+char(39)+'/'+char(39)+'. Please try again or alternatively please call our Contact Centre on 0345 070 0029 who will be able to assist. We'+char(39)+'re open Monday to Fridays 8.30am - 5.30pm excluding bank holidays.', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E007','Please enter your Forename', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E008','Please enter your Surname', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E009','Please enter your Date of Birth', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E010','Please enter a valid Date of Birth', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E011','To cash-in your policy you must be aged 55 or over.Please contact us to discuss your options.', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E012','As you are aged 75 or over.Please contact us to discuss your options.', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E013','We have been unable to match your personal details to our policy records. Please ensure that you have entered your name exactly as it shown on your policy documentation.', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E014','Please enter your Title', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E015','Please Enter valid Phone number', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E016','The phone number you have entered does not appear to be a valid UK number. Please contact us if you continue to encounter any further problems.', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E017','Please Enter valid Phone number', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E018','Please select the appropriate Telephone Type', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E019','Please select the appropriate Telephone Type', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E020','Please select the appropriate Telephone Type', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E021','You can only select one Work, Home or Mobile number.', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E022','You can only select one Work, Home or Mobile number.', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E023','You can only select one Work, Home or Mobile number.', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E024','Please complete at least one contact number so that we are able to make contact with you should we have any queries in relation to your application.', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E025','We have been unable to locate your postcode. Please call our Contact Centre on 0345 070 0029 who will be able to assist. We'+char(39)+'re open Monday to Fridays 8.30am - 5.30pm excluding bank holidays.', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E026','We have been unable to verify your account. Please try again. There will be a limit to the number of attempts that you will be able to make.', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E027','We have been unable to verify your account and your policy record has been locked for your security. Please call our Contact Centre on 0345 070 0029 who will be able to assist. We'+char(39)+'re open Monday to Fridays 8.30am - 5.30pm excluding bank holidays.', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E028','Please enter in your National Insurance number', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E029','Please enter your Bank or Building Society Name', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E030','Please enter the Account holder(s) name', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E031','Please enter your Bank Account number', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E032','Please enter your Sort Code', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E033','The details you have provided have not been recognised as a valid bank/building society account. Please check the details you have inputted and re-enter using your correct details. Please contact us if you continue to encounter any further problems.', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E034','Please enter a valid National Insurance Number', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E035','Please select how you would like payment to be made', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E036','Please enter valid sort code', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E037','Please enter valid account number', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E100','A systems error has occurred which means you are unable to continue with your online application. We apologize for the inconvenience. Please ', NULL, GETDATE(), 'SYSTEM', NULL, NULL),
('MPLValidationError','E038','We have been unable to verify your account. Please call our Contact Centre on 0345 788 3311 who will be able to assist. We''re open Monday to Fridays 8.30am - 5.30pm excluding bank holidays.', NULL, GETDATE(), 'SYSTEM', NULL, NULL)



--Below are the two queries for CR 025 to change the error message for policy number page :-
UPDATE MPL_STATIC_DATA SET MSD_STATIC_DATA_VALUE = 'Your policy or plan number has not been recognised. Please ensure that you enter the number exactly as it is shown on your Annual Benefits Statement or recent correspondence, which may include using symbol "/" or spaces. Please try again or alternatively call our Contact Centre on 0345 788 3311 who will be happy to assist you. We''re open Monday to Fridays 8.30am - 5.30pm excluding bank holidays.' WHERE MSD_STATIC_DATA_KEY IN ('E004','E006');