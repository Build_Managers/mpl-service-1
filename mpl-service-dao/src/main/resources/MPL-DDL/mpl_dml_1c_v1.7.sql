USE MPL_PORTAL

INSERT INTO [DBO].[MPL_CUSTOMER_ACK_ARC] SELECT MCT_CUSTOMER_ID, MCT_SENT_BANCS_FLG, MCT_SENT_BANCS_TS, MCT_BANCS_ACK_FLG, MCT_BANCS_ACK_TS, MCT_ATTEMPTS FROM [DBO].[MPL_CUSTOMER];

INSERT INTO [DBO].[MPL_DOCUMENT_AUDIT_ACK_ARC] SELECT MDA_DOCUMENT_ID, MDA_SENT_BANCS_FLG, MDA_SENT_BANCS_TS, MDA_BANCS_ACK_FLG, MDA_BANCS_ACK_TS, MDA_ATTEMPTS FROM [DBO].[MPL_DOCUMENT_AUDIT];

INSERT INTO [DBO].[MPL_QUOTE_DETAILS_ACK_ARC] SELECT MQD_QUOTE_ID, MQD_SENT_BANCS_FLG, MQD_SENT_BANCS_TS, MQD_BANCS_ACK_FLG, MQD_BANCS_ACK_TS, MQD_ATTEMPTS FROM [DBO].[MPL_QUOTE_DETAILS];

INSERT INTO [DBO].[MPL_POLICY_ENCASHMENT_EMAIL_ACK_ARC] SELECT MPE_ENCAHMENT_ID, MPE_EMAIL_SENT_FLG, MPE_EMAIL_SENT_TS, MPE_EMAIL_FAILED_TS, MPE_EMAIL_ATTEMPTS FROM [DBO].[MPL_POLICY_ENCASHMENT];

********************************************************Added on 05-Dec for error message***************************
***********************************************************************************************************************
delete from [DBO].[[DBO].[MPL_STATIC_DATA]] where MSD_STATIC_DATA_GROUP='MPLValidationError'

insert into [DBO].[[DBO].[MPL_STATIC_DATA]] values('MPLValidationError', 'E001', 'Please enter a valid email address, e.g example@example.com', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[[DBO].[MPL_STATIC_DATA]] values('MPLValidationError', 'E002', 'Emails must match.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E003', 'Please enter a username using the rules as stated.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E004', 'The username you have entered has not been recognised, please try again. Please click ''Forgotten username'' if you have forgotten this.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E005', 'Please enter your username. Please click on ''Forgotten username'' if you have forgotten this.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E006', 'The username you have entered has not been recognised, please correct your username and retry. If you have forgotten your username, you can reset your details using the link below or alternatively, is it possible you have not yet registered to MyPhoenix?', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E007', 'The username you have entered is already in use, please create another.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E008', 'The username you have entered does not follow the rules as stated, please try again.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E009', 'Please enter a password using the rules as stated.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E010', 'Please enter your password. Please click on ''Forgotten password'' if you have forgotten this.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E011', 'The password you have entered has not been recognised, please try again. Please click ''Forgotten password.'' if you have forgotten this.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E012', 'The passwords you have entered do not match, please try again.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E013', 'The passwords you have entered do not follow the rules as noted, please try again.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E014', 'Please enter an answer with more than 3 characters.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E015', 'The security question you have entered already matches another.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E016', 'You have not answered all of the questions, please complete and click next to continue.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E017', 'Please answer your security question to continue.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E018', 'The answers to your questions do not match to the security answers you provided when setting up access to MyPhoenix. Please try again.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E019', 'The answers to your questions do not match to the security answers you provided when setting up access to MyPhoenix and your account has now been locked for security purposes. Please call our Contact Centre on xxx xxx xxx who will be able to assist. We�re open Monday to Fridays 8.30am � 5.30pm excluding bank holidays.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E020', 'Please enter your policy or plan number.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E021', 'Your policy or plan number has not been recognised. Please ensure that you enter the number exactly as it shown on your policy documentation, which may include using the symbol ''/''. Please try again or alternatively please call our Contact Centre on xxx xxx xxx who will be able assist. We''re open Monday to Fridays 8.30am-5.30pm excluding bank holidays.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E022', 'We have been unable to verify your account and your policy record has been locked for your security. Please call our Contact Centre on xxx xxx xxx who will be able assist. We''re open Monday to Fridays 8.30am-5.30pm excluding bank holidays.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E023', 'Your policy has now been locked for security purposes. Please call us on xxxxx xxxxxx where we''ll be happy to assist you.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E024', 'Your account has now been locked for security purposes.  Please call us on xxxxx xxxxxx where we�ll be happy to assist you.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E025', 'Please select your title.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E026', 'Please enter your first name as it appears on your policy/plan documentation.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E027', 'Please enter your surname as it appears on your policy/plan documentation.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E028', 'Please enter your Date of Birth.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E029', 'Please enter a valid Date of Birth.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E030', 'We have been unable to match your personal details to our policy records. Please ensure that you have entered your name exactly as it is shown on your policy or plan documentation. Please try again or alternatively please call our contact centre on xxx xxx xxx who will be able to assist. We''re open Monday to Friday 8.30am � 5.30pm excluding bank holidays.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E031', 'We have been unable to locate your postcode. Please ensure that you have entered your postcode correctly, including leaving a space between the two parts of your postcode. Alternatively, please call our Contact Centre on xxx xxx xxx who will be able to assist. We''re open Monday to Fridays 8.30am-5.30pm excluding bank holidays.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E032', 'We have been unable to match your address against our policy records. Please call our Contact Centre on xxx xxx xxx who will be able to assist. We''re open Monday to Fridays 8.30am-5.30pm excluding bank holidays.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E033', 'Please Enter valid Phone number.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E034', 'The phone number you have entered does not appear to be a valid UK number. Please check the details you have entered. If you would like further assistance please call our Contact Centre on xxx xxx xxx. We''re open Monday to Fridays 8.30am-5.30pm excluding bank holidays.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E035', 'Please answer the verification question to continue.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E036', 'We have been unable to verify your account. Please call our Contact Centre on xxx xxx xxx who will be able to assist. We''re open Monday to Fridays 8.30am-5.30pm excluding bank holidays.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E037', 'We have been unable to verify your account based on the answer you have provided, please try again. There will be a limit to the number of attempts that you will be able to make.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E038', 'Please enter in your National Insurance number.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E039', 'Please enter your Bank or Building Society Name.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E040', 'Please enter the Account holder(s) name.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E041', 'Please enter your Bank Account number.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E042', 'Please enter your Sort Code.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E043', 'The details you have provided have not been recognised as a valid bank/building society account. Please check the details you have inputted and re-enter using your correct details. Please contact us if you continue to encounter any further problems.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E044', 'Please enter a valid National Insurance Number.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E045', 'Please select how you would like payment to be made.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E046', 'Please enter valid Sort Code.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E047', 'Please enter valid Account Number.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E100', 'A systems error has occurred which means you are unable to continue. We apologise for the inconvenience caused. Please call our Contact Centre on xxx xxx xxx. We''re open Monday to Fridays 8.30am-5.30pm excluding bank holidays.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E101', 'A systems error has occurred which means you are unable to continue. We apologise for the inconvenience caused. Please call our Contact Centre on xxx xxx xxx. We''re open Monday to Fridays 8.30am-5.30pm excluding bank holidays.', null, GETDATE(),'SYSTEM', null, null);
insert into [DBO].[MPL_STATIC_DATA] values('MPLValidationError', 'E102', 'A systems error has occurred which means you are unable to continue. We apologise for the inconvenience caused. Please call our Contact Centre on xxx xxx xxx. We''re open Monday to Fridays 8.30am-5.30pm excluding bank holidays.', null, GETDATE(),'SYSTEM', null, null);

insert into dbo.mpl_static_data values ('MPLValidationError', 'E103', 'The email address you have entered is already registered, please log in or call us on 0345 070 0029 where we''ll be able to assist', NULL, GETDATE(),'SYSTEM',NULL,NULL);

*******************************************************************************************************************************************************************
**********************************************************************************************************************

SELECT * FROM MPL_STATIC_DATA WHERE MSD_STATIC_DATA_GROUP ='MPLNavURLPGID';

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'myphx/home', 'PG01', NULL, '2017-11-16 13:47:01.94', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'registration/email', 'PG02', NULL, '2017-11-16 13:47:01.94', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'registration/account', 'PG03', NULL, '2017-11-16 13:47:01.98', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'registration/security', 'PG04', NULL, '2017-11-16 13:47:01.983', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'registration/confirm', 'PG05', NULL, '2017-11-16 13:47:01.983', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'registration/activation', 'PG06', NULL, '2017-11-16 13:47:01.983', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'registration/activation_security_question', 'PG07', NULL, '2017-11-16 13:47:01.983', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'registration/policy_number', 'PG08', NULL, '2017-11-16 13:47:01.987', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'registration/your_details', 'PG09', NULL, '2017-11-16 13:47:02.03', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'registration/address', 'PG10', NULL, '2017-11-16 13:47:02.03', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'registration/contact_number', 'PG11', NULL, '2017-11-16 13:47:02.03', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'registration/verification', 'PG12', NULL, '2017-11-16 13:47:02.033', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'myphx/login', 'PG13', NULL, '2017-11-16 13:47:02.033', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'myphx/login_Security_Question_Answer', 'PG14', NULL, '2017-11-16 13:47:02.037', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'myphx/policy_dashboard', 'PG15', NULL, '2017-11-16 13:47:02.037', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'myphx/personal_details', 'PG16', NULL, '2017-11-16 13:47:02.037', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'myphx/contact', 'PG17', NULL, '2017-11-16 13:47:02.037', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'myphx/dashboard', 'PG18', NULL, '2017-11-16 13:47:02.037', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'dashboard/confirmation', 'PG19', NULL, '2017-11-16 13:47:02.037', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'dashboard/making_sure_you_made_right_choice', 'PG20', NULL, '2017-11-16 13:47:02.037', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'dashboard/guaranteed_income_for_life', 'PG21', NULL, '2017-11-16 13:47:02.04', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'dashboard/flexible_access', 'PG22', NULL, '2017-11-16 13:47:02.04', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'dashboard/take_more_than_one_option', 'PG23', NULL, '2017-11-16 13:47:02.04', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'dashboard/keep_your_pension_savings', 'PG24', NULL, '2017-11-16 13:47:02.04', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'dashboard/transfer_your_pension_savings', 'PG25', NULL, '2017-11-16 13:47:02.04', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'dashboard/take_all_of_your_pension_savings', 'PG26', NULL, '2017-11-16 13:47:02.04', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'dashboard/risks_of_taking_all_of_my_pension_savings', 'PG27', NULL, '2017-11-16 13:47:02.04', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'dashboard/pension_guidance_and_advice', 'PG28', NULL, '2017-11-16 13:47:02.04', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'dashboard/dashboard_personal_details', 'PG29', NULL, '2017-11-16 13:47:02.04', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'dashboard/encashment_completion', 'PG30', NULL, '2017-11-16 13:47:02.04', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'myphx/forgot_username', 'PG31', NULL, '2017-11-16 13:47:02.04', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'myphx/forgottenUsername2', 'PG32', NULL, '2017-11-16 13:47:02.04', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'myphx/forgot_password', 'PG33', NULL, '2017-11-16 13:47:02.04', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'myphx/securityquestions', 'PG34', NULL, '2017-11-16 13:47:02.04', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'Your personal details', 'PG35', NULL, '2017-11-16 13:47:02.043', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLNavURLPGID', 'Next Steps', 'PG36', NULL, '2017-11-16 13:47:02.043', 'SYSTEM', NULL, NULL)
GO



--------------------------------------------------------------ADDED DMLS TO STOP PAGE JUMP---------------------------------------------


INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG01', 'ALL', NULL, '2017-09-25 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG02', 'PG01,PG02', NULL, '2017-09-25 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG03', 'PG02,PG03,PG04', NULL, '2017-09-25 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG04', 'PG03,PG04', NULL, '2017-09-25 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG05', 'PG04,PG05', NULL, '2017-09-25 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG06', 'ALL', NULL, '2017-09-25 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG07', 'PG06,PG07', NULL, '2017-09-25 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG08', 'PG07,PG08,PG09', NULL, '2017-09-25 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG09', 'PG08,PG09,PG10', NULL, '2017-09-25 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG10', 'PG09,PG10,PG11', NULL, '2017-09-25 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG11', 'PG10,PG11,PG12', NULL, '2017-09-25 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG12', 'PG11,PG12', NULL, '2017-09-25 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG13', 'ALL', NULL, '2017-09-25 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG14', 'PG13,PG14', NULL, '2017-09-25 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG15', 'PG12,PG14,PG15', NULL, '2017-09-25 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG16', 'PG15,PG16,PG17', NULL, '2017-09-25 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG17', 'PG15,PG16,PG17', NULL, '2017-09-25 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG18', 'PG15,PG18,PG19', NULL, '2017-09-25 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG19', 'PG18,PG19,PG20', NULL, '2017-09-25 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG20', 'PG19,PG20,PG21', NULL, '2017-12-02 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG21', 'PG20,PG21,PG22', NULL, '2017-12-02 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG22', 'PG21,PG22,PG23', NULL, '2017-12-02 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG23', 'PG22,PG23,PG24', NULL, '2017-12-02 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG24', 'PG23,PG24,PG25', NULL, '2017-12-02 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG25', 'PG24,PG25,PG26', NULL, '2017-12-02 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG26', 'PG25,PG26,PG27,PG29', NULL, '2017-12-02 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG27', 'PG26,PG27,PG28', NULL, '2017-12-02 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG28', 'PG27,PG28,PG29', NULL, '2017-12-02 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG29', 'PG28,PG29', NULL, '2017-12-02 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG30', 'PG29,PG30', NULL, '2017-12-02 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG31', 'ALL', NULL, '2017-12-02 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG32', 'PG31,PG32', NULL, '2017-12-02 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG33', 'ALL', NULL, '2017-12-02 18:27:35.103', 'SYSTEM', NULL, NULL)
GO

INSERT INTO dbo.MPL_STATIC_DATA (MSD_STATIC_DATA_GROUP, MSD_STATIC_DATA_KEY, MSD_STATIC_DATA_VALUE, MSD_QUESTION_LABEL, MSD_CREATED_ON, MSD_CREATED_BY, MSD_LAST_UPDATED_ON, MSD_LAST_UPDATED_BY)
VALUES ('MPLAllowedPGID', 'PG34', 'PG33,PG34', NULL, '2017-12-02 18:27:35.103', 'SYSTEM', NULL, NULL)
GO
