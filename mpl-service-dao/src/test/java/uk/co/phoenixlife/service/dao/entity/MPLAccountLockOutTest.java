package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MPLAccountLockOutTest {
	@Test
    public void testMPLAccountLockOutSuccess() {

        final MPLAccountLockOut mPLAccountLockOut = new MPLAccountLockOut();
        mPLAccountLockOut.setMalId(10L);
        mPLAccountLockOut.setUserName("RICHMIL");
        mPLAccountLockOut.setPolicySearchCount(12);
        mPLAccountLockOut.setPersonalFailCount(11);
        mPLAccountLockOut.setVerificationFailCount(15);
        mPLAccountLockOut.setAccountLockStatus('C');
        mPLAccountLockOut.setCreatedOn(new Timestamp(10));
        mPLAccountLockOut.setLastUpdatedOn(new Timestamp(20));
        mPLAccountLockOut.setLastUpdatedBy("ABCD");
        
        Assert.assertNotNull(mPLAccountLockOut.getMalId());
        Assert.assertNotNull(mPLAccountLockOut.getUserName());
        Assert.assertNotNull(mPLAccountLockOut.getPolicySearchCount());
        Assert.assertNotNull(mPLAccountLockOut.getPersonalFailCount());
        Assert.assertNotNull(mPLAccountLockOut.getVerificationFailCount());
        Assert.assertNotNull(mPLAccountLockOut.getAccountLockStatus());
        Assert.assertNotNull(mPLAccountLockOut.getCreatedOn());
        Assert.assertNotNull(mPLAccountLockOut.getLastUpdatedOn());
        Assert.assertNotNull(mPLAccountLockOut.getLastUpdatedBy());
}
}
