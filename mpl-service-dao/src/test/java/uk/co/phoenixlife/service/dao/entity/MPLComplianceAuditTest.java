package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dao.entity.MPLComplianceAudit.MPLComplianceAuditId;

public class MPLComplianceAuditTest {
	@Test
	public void testMPLComplianceAuditSuccess(){
		final MPLComplianceAudit mplComplianceAudit = new MPLComplianceAudit();
        MPLComplianceAuditId mPLComplianceAuditId=new MPLComplianceAuditId();
        
        mplComplianceAudit.setEventCode("PG03_MPLCUSTID");
        mplComplianceAudit.setEventTs(Timestamp.valueOf(LocalDateTime.now()));
        mplComplianceAudit.setEventVal("12345".getBytes());
        mplComplianceAudit.setSessionId("ABCXXXX5123654789CND632587112585874");
        mPLComplianceAuditId.setEventCode("ABC");
        mPLComplianceAuditId.setEventTs(new Timestamp(0));
        mPLComplianceAuditId.setSessionId("XYZ");
     

        Assert.assertNotNull(mplComplianceAudit.getEventCode());
        Assert.assertNotNull(mplComplianceAudit.getEventTs());
        Assert.assertNotNull(mplComplianceAudit.getEventVal());
        Assert.assertNotNull(mplComplianceAudit.getSessionId());
        Assert.assertNotNull(mPLComplianceAuditId.getEventCode());
        Assert.assertNotNull(mPLComplianceAuditId.getEventTs());
        Assert.assertNotNull(mPLComplianceAuditId.getSessionId());
	}

}
