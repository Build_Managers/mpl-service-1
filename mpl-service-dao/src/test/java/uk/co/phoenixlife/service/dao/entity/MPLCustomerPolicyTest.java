package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MPLCustomerPolicyTest {
	 @Test 
     public void testMPLCustomerPolicySuccess()throws Exception
     {
    	 MPLCustomerPolicy mPLCustomerPolicy=new MPLCustomerPolicy();
    	 MPLCustomer mPLCustomer =new MPLCustomer();
    	 Set<MPLDocumentAudit> policyDocuments=new HashSet<MPLDocumentAudit>();
    	 Set<MPLVerificationAttempts> policyVerificationAttempts=new HashSet<MPLVerificationAttempts>();
    	 MPLQuoteDetails mPLQuoteDetails=new MPLQuoteDetails();
    	 MPLPolicyEncashment mPLPolicyEncashment=new MPLPolicyEncashment();
    	 
    	 mPLCustomerPolicy.setBancsPolNum("ABC");
    	 mPLCustomerPolicy.setCreatedBy("PQR");
    	 mPLCustomerPolicy.setCreatedOn(new Timestamp(0));
    	 mPLCustomerPolicy.setLastUpdatedBy("XYZ");
    	 mPLCustomerPolicy.setLastUpdatedOn(new Timestamp(0));
    	 mPLCustomerPolicy.setLegacyPolNum("abc");
    	 mPLCustomerPolicy.setPolicyCustomer(mPLCustomer);
    	 mPLCustomerPolicy.setPolicyDocuments(policyDocuments);
    	 mPLCustomerPolicy.setPolicyEncashment(mPLPolicyEncashment);
    	 mPLCustomerPolicy.setPolicyId(2500000);
    	 mPLCustomerPolicy.setPolicyLockedStatus('M');
    	 mPLCustomerPolicy.setPolicyQuote(mPLQuoteDetails);
    	 mPLCustomerPolicy.setPolicyVerificationAttempts(policyVerificationAttempts);
    	 mPLCustomerPolicy.setRetirementPackSource('A');
    	
    	 Assert.assertNotNull(mPLCustomerPolicy.getBancsPolNum());
    	 Assert.assertNotNull(mPLCustomerPolicy.getCreatedBy());
    	 Assert.assertNotNull(mPLCustomerPolicy.getCreatedOn());
    	 Assert.assertNotNull(mPLCustomerPolicy.getLastUpdatedBy());
    	 Assert.assertNotNull(mPLCustomerPolicy.getLastUpdatedOn());
    	 Assert.assertNotNull(mPLCustomerPolicy.getLegacyPolNum());
    	 Assert.assertNotNull(mPLCustomerPolicy.getPolicyCustomer());
    	 Assert.assertNotNull(mPLCustomerPolicy.getPolicyDocuments());
    	 Assert.assertNotNull(mPLCustomerPolicy.getPolicyEncashment());
    	 Assert.assertNotNull(mPLCustomerPolicy.getPolicyId());
    	 Assert.assertNotNull(mPLCustomerPolicy.getPolicyLockedStatus());
    	 Assert.assertNotNull(mPLCustomerPolicy.getPolicyQuote());
    	 Assert.assertNotNull(mPLCustomerPolicy.getPolicyVerificationAttempts());
    	 Assert.assertNotNull(mPLCustomerPolicy.getRetirementPackSource());	 
     }

}
