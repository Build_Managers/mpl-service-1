package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.constants.MPLConstants;

public class MPLCustomerTest {
	@Test
    public void testMPLCustomerSuccess() {

        final MPLCustomer mplcustomer = new MPLCustomer();
        mplcustomer.setCustomerId(5);
        mplcustomer.setBancsCustNum("91570200");
        mplcustomer.setUserName("RICHMIL");
        mplcustomer.setLegacyCustNum("BA-101");
        mplcustomer.setTitle("Mr".getBytes());
        mplcustomer.setFirstName("Richard".getBytes());
        mplcustomer.setLastName("Milsum".getBytes());
        mplcustomer.setEmailAddr("abc@xyz.com".getBytes());
        mplcustomer.setPrimaryPhNo("07123654789".getBytes());
        mplcustomer.setPrimaryPhType("Mobile");
        mplcustomer.setSecPhNo("02023654789".getBytes());
        mplcustomer.setSecPhType("Landline");
        mplcustomer.setAddPhNo("07123654123".getBytes());
        mplcustomer.setAddPhType("Any");
        mplcustomer.setNino("100".getBytes());
        mplcustomer.setEligiblePoliciesCount(0);
        mplcustomer.setCreatedOn(Timestamp.valueOf(LocalDateTime.now()));
        mplcustomer.setCreatedBy(MPLConstants.SYSTEM);
        mplcustomer.setLastUpdatedOn(Timestamp.valueOf(LocalDateTime.now()));
        mplcustomer.setLastUpdatedBy(MPLConstants.SYSTEM);
        final Set<MPLCustomerPolicy> policies = new HashSet<>();
        final Set<MPLCustomerUpdateStatus> customerUpdateStatusList = new HashSet<>();
        final Set<MPLEmailStatus> emailStatusList = new HashSet<>();
        mplcustomer.setEmailStatusList(emailStatusList);
        mplcustomer.setCustomerUpdateStatusList(customerUpdateStatusList);
        mplcustomer.setCustomerPolicies(policies);
        mplcustomer.setAccountCreatedOn(new Timestamp(10L));

        Assert.assertNotNull(mplcustomer.getCustomerId());
        Assert.assertNotNull(mplcustomer.getBancsCustNum());
        Assert.assertNotNull(mplcustomer.getUserName());
        Assert.assertNotNull(mplcustomer.getLegacyCustNum());
        Assert.assertNotNull(mplcustomer.getTitle());
        Assert.assertNotNull(mplcustomer.getFirstName());
        Assert.assertNotNull(mplcustomer.getLastName());
        Assert.assertNotNull(mplcustomer.getEmailAddr());
        Assert.assertNotNull(mplcustomer.getPrimaryPhNo());
        Assert.assertNotNull(mplcustomer.getPrimaryPhType());
        Assert.assertNotNull(mplcustomer.getSecPhNo());
        Assert.assertNotNull(mplcustomer.getSecPhType());
        Assert.assertNotNull(mplcustomer.getAddPhNo());
        Assert.assertNotNull(mplcustomer.getAddPhType());
        Assert.assertNotNull(mplcustomer.getNino());
        Assert.assertNotNull(mplcustomer.getEligiblePoliciesCount());
        Assert.assertNotNull(mplcustomer.getCreatedOn());
        Assert.assertNotNull(mplcustomer.getCreatedBy());
        Assert.assertNotNull(mplcustomer.getLastUpdatedOn());
        Assert.assertNotNull(mplcustomer.getLastUpdatedBy());
        Assert.assertNotNull(mplcustomer.getCustomerPolicies());
        Assert.assertNotNull(mplcustomer.getCustomerUpdateStatusList());
        Assert.assertNotNull(mplcustomer.getEmailStatusList());
        Assert.assertNotNull(mplcustomer.getAccountCreatedOn());
    }

}
