package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MPLCustomerUpdateAttemptTest {
	@Test
    public void testMPLCustomerUpdateAttemptSuccess() {

        final MPLCustomerUpdateAttempt mPLCustomerUpdateAttempt = new MPLCustomerUpdateAttempt();
        MPLCustomerUpdateStatus customerUpdateStatus = new MPLCustomerUpdateStatus();
        mPLCustomerUpdateAttempt.setAttemptId(10L);
        mPLCustomerUpdateAttempt.setSentFlag('y');
        mPLCustomerUpdateAttempt.setSentTmst(new Timestamp(10));
        mPLCustomerUpdateAttempt.setAckFlag('n');
        mPLCustomerUpdateAttempt.setAckTmst(new Timestamp(10L));
        mPLCustomerUpdateAttempt.setFailureReason("ABCD");
        mPLCustomerUpdateAttempt.setCustomerUpdateStatus(customerUpdateStatus);
        mPLCustomerUpdateAttempt.toString();
        
        Assert.assertNotNull(mPLCustomerUpdateAttempt.getAttemptId());
        Assert.assertNotNull(mPLCustomerUpdateAttempt.getSentFlag());
        Assert.assertNotNull(mPLCustomerUpdateAttempt.getSentTmst());
        Assert.assertNotNull(mPLCustomerUpdateAttempt.getAckFlag());
        Assert.assertNotNull(mPLCustomerUpdateAttempt.getAckTmst());
        Assert.assertNotNull(mPLCustomerUpdateAttempt.getFailureReason());
        Assert.assertNotNull(mPLCustomerUpdateAttempt.getCustomerUpdateStatus());
}
}