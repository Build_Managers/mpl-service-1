package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MPLCustomerUpdateStatusTest {
	@Test
    public void testMPLCustomerUpdateStatusSuccess() {

        final MPLCustomerUpdateStatus mPLCustomerUpdateStatus = new MPLCustomerUpdateStatus();
        MPLCustomer updatedCustomer = new MPLCustomer();
        Set<MPLCustomerUpdateAttempt> customerUpdateAttempts = new HashSet<MPLCustomerUpdateAttempt>();
        mPLCustomerUpdateStatus.setUpdateId(10);
        mPLCustomerUpdateStatus.setPolicyId(11L);
        mPLCustomerUpdateStatus.setStatus('A');
        mPLCustomerUpdateStatus.setTimestamp(new Timestamp(10));
        mPLCustomerUpdateStatus.setCustomerUpdateAttempts(customerUpdateAttempts);
        mPLCustomerUpdateStatus.setUpdatedCustomer(updatedCustomer);
        mPLCustomerUpdateStatus.toString();
        
        Assert.assertNotNull(mPLCustomerUpdateStatus.getUpdateId());
        Assert.assertNotNull(mPLCustomerUpdateStatus.getPolicyId());
        Assert.assertNotNull(mPLCustomerUpdateStatus.getStatus());
        Assert.assertNotNull(mPLCustomerUpdateStatus.getTimestamp());
        Assert.assertNotNull(mPLCustomerUpdateStatus.getCustomerUpdateAttempts());
        Assert.assertNotNull(mPLCustomerUpdateStatus.getUpdatedCustomer());
}
}