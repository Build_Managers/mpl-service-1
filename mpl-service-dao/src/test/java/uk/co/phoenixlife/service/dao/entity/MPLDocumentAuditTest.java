package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.constants.MPLConstants;

public class MPLDocumentAuditTest {
	@Test
    public void testMPLDocumentAuditSuccess() {

        final MPLDocumentAudit mplDocumentAuditTest = new MPLDocumentAudit();
        MPLDocumentUpdateStatus documentUpdateStatus = new MPLDocumentUpdateStatus();
       
        mplDocumentAuditTest.setDocumentUpdateStatus(documentUpdateStatus);
        mplDocumentAuditTest.setDocumentId(new Long(0));
        mplDocumentAuditTest.setDocumentPolicy(new MPLCustomerPolicy());
        mplDocumentAuditTest.setDocType("TXT");
        mplDocumentAuditTest.setDocName("Test");
        mplDocumentAuditTest.setDocumentContents("PDF Binary data".getBytes());
        mplDocumentAuditTest.setCreatedOn(Timestamp.valueOf(LocalDateTime.now()));
        mplDocumentAuditTest.setCreatedBy(MPLConstants.SYSTEM);

        Assert.assertNotNull(mplDocumentAuditTest.getDocumentId());
        Assert.assertNotNull(mplDocumentAuditTest.getDocumentPolicy());
        Assert.assertNotNull(mplDocumentAuditTest.getDocType());
        Assert.assertNotNull(mplDocumentAuditTest.getDocName());
        Assert.assertNotNull(mplDocumentAuditTest.getDocumentContents());
        Assert.assertNotNull(mplDocumentAuditTest.getDocumentUpdateStatus());
        Assert.assertNotNull(mplDocumentAuditTest.getCreatedOn());
        Assert.assertNotNull(mplDocumentAuditTest.getCreatedBy());

}
}
