package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MPLDocumentUpdateStatusTest {
	@Test
    public void testMPLDocumentUpdateStatusSuccess() {

        final MPLDocumentUpdateStatus mPLDocumentUpdateStatus = new MPLDocumentUpdateStatus();
        Set<MPLDocumentUpdateAttempt> documentUpdateAttempts = new HashSet<MPLDocumentUpdateAttempt>();
        MPLDocumentAudit updatedDocument = new MPLDocumentAudit();
       
        mPLDocumentUpdateStatus.setUpdateId(10);
        mPLDocumentUpdateStatus.setStatus('A');
        mPLDocumentUpdateStatus.setTimestamp(new Timestamp(10));
        mPLDocumentUpdateStatus.setUpdatedDocument(updatedDocument);
        mPLDocumentUpdateStatus.setDocumentUpdateAttempts(documentUpdateAttempts);
        
        Assert.assertNotNull(mPLDocumentUpdateStatus.getUpdateId());
        Assert.assertNotNull(mPLDocumentUpdateStatus.getStatus());
        Assert.assertNotNull(mPLDocumentUpdateStatus.getTimestamp());
        Assert.assertNotNull(mPLDocumentUpdateStatus.getUpdatedDocument());
        Assert.assertNotNull(mPLDocumentUpdateStatus.getDocumentUpdateAttempts());
}
}