package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MPLEmailAttemptTest {
	@Test
    public void testMPLEmailAttemptSuccess() {

        final MPLEmailAttempt mPLEmailAttempt = new MPLEmailAttempt();
        MPLEmailStatus emailStatus = new MPLEmailStatus();
        mPLEmailAttempt.setAttemptId(10);
        mPLEmailAttempt.setFailedTmst(new Timestamp(10));
        mPLEmailAttempt.setFailureReason("ABCD");
        mPLEmailAttempt.setSentFlag('A');
        mPLEmailAttempt.setSentTmst(new Timestamp(20));
        mPLEmailAttempt.setEmailStatus(emailStatus);
        
        Assert.assertNotNull(mPLEmailAttempt.getAttemptId());
        Assert.assertNotNull(mPLEmailAttempt.getFailedTmst());
        Assert.assertNotNull(mPLEmailAttempt.getFailureReason());
        Assert.assertNotNull(mPLEmailAttempt.getSentFlag());
        Assert.assertNotNull(mPLEmailAttempt.getSentTmst());
        Assert.assertNotNull(mPLEmailAttempt.getEmailStatus());
}
}