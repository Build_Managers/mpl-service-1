package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MPLEmailStatusTest {
	@Test
    public void testMPLEmailStatusSuccess() {

        final MPLEmailStatus mPLEmailStatus = new MPLEmailStatus();
        MPLCustomer customer = new  MPLCustomer();
        MPLPolicyEncashment encashment = new MPLPolicyEncashment();
        Set<MPLEmailAttempt> emailAttempts = new HashSet<>();
        mPLEmailStatus.setStatusId(10L);
        mPLEmailStatus.setEmailType("ABC");
        mPLEmailStatus.setTimestamp(new Timestamp(10));
        mPLEmailStatus.setStatus('Y');
        mPLEmailStatus.setEmailCustomer(customer);
        mPLEmailStatus.setEncashmentId(1L);
        mPLEmailStatus.setEmailAttempts(emailAttempts);
        
        Assert.assertNotNull(mPLEmailStatus.getStatusId());
        Assert.assertNotNull(mPLEmailStatus.getEmailType());
        Assert.assertNotNull(mPLEmailStatus.getTimestamp());
        Assert.assertNotNull(mPLEmailStatus.getStatus());
        Assert.assertNotNull(mPLEmailStatus.getEmailCustomer());
        Assert.assertNotNull(mPLEmailStatus.getEmailAttempts());
        Assert.assertNotNull(mPLEmailStatus.getEncashmentId());
        
        
        
        
}
}