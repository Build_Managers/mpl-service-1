package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MPLLDocumentUpdateAttemptTest {
	@Test
    public void testMPLLDocumentUpdateAttemptSuccess() {

        final MPLDocumentUpdateAttempt mPLLDocumentUpdateAttempt = new MPLDocumentUpdateAttempt();
        MPLDocumentUpdateStatus documentUpdateStatus = new MPLDocumentUpdateStatus();
        mPLLDocumentUpdateAttempt.setAttemptId(12);
        mPLLDocumentUpdateAttempt.setSentFlag('Y');
        mPLLDocumentUpdateAttempt.setSentTmst(new Timestamp(10L));
        mPLLDocumentUpdateAttempt.setAckFlag('N');
        mPLLDocumentUpdateAttempt.setAckTmst(new Timestamp(12));
        mPLLDocumentUpdateAttempt.setFailureReason("ABCD");
        mPLLDocumentUpdateAttempt.setDocumentUpdateStatus(documentUpdateStatus);
        
        Assert.assertNotNull(mPLLDocumentUpdateAttempt.getAttemptId());
        Assert.assertNotNull(mPLLDocumentUpdateAttempt.getSentFlag());
        Assert.assertNotNull(mPLLDocumentUpdateAttempt.getSentTmst());
        Assert.assertNotNull(mPLLDocumentUpdateAttempt.getAckFlag());
        Assert.assertNotNull(mPLLDocumentUpdateAttempt.getAckTmst());
        Assert.assertNotNull(mPLLDocumentUpdateAttempt.getFailureReason());
        Assert.assertNotNull(mPLLDocumentUpdateAttempt.getDocumentUpdateStatus());
        

}
}
