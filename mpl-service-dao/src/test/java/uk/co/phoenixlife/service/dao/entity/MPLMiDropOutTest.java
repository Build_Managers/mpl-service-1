package uk.co.phoenixlife.service.dao.entity;

import java.sql.Date;
import java.sql.Timestamp;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dao.entity.MPLMiDropOut.MPLMiDropOutId;

public class MPLMiDropOutTest {
	@Test 
	public void testMPLMiDropOutSuccess()
	{
		MPLMiDropOut mPLMiDropOut=new MPLMiDropOut();
		MPLMiDropOutId mPLMiDropOutId=new MPLMiDropOutId();
		
		
		mPLMiDropOut.setSessionId("ABC");
		mPLMiDropOut.setDropOutDate(new Date(2));
		mPLMiDropOut.setEmailAddress("abc@xyz.com".getBytes());
		mPLMiDropOut.setPageId("PQR");
		mPLMiDropOut.setTimeSpent(2);
		mPLMiDropOut.setCreatedOn(new Timestamp(0));
		mPLMiDropOutId.setDropOutDate(new Date(2L));
		mPLMiDropOutId.setPageId("XYZ");
		mPLMiDropOutId.setSessionId("PHP");
		
		Assert.assertNotNull(mPLMiDropOut.getCreatedOn());
		Assert.assertNotNull(mPLMiDropOut.getDropOutDate());
		Assert.assertNotNull(mPLMiDropOut.getEmailAddress());
		Assert.assertNotNull(mPLMiDropOut.getPageId());
		Assert.assertNotNull(mPLMiDropOut.getSessionId());
		Assert.assertNotNull(mPLMiDropOut.getTimeSpent());
		Assert.assertNotNull(mPLMiDropOutId.getDropOutDate());
	    Assert.assertNotNull(mPLMiDropOutId.getPageId());
		Assert.assertNotNull(mPLMiDropOutId.getSessionId());	
	}
}
