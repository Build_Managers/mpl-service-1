package uk.co.phoenixlife.service.dao.entity;

import java.sql.Date;
import java.sql.Timestamp;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dao.entity.MPLMiErrorMessage.MPLMiErrorMessageId;

public class MPLMiErrorMessageTest {
	@Test(enabled=true)
	public void testMPLMiErrorMessageSuccess()
	{
		MPLMiErrorMessage mPLMiErrorMessage=new MPLMiErrorMessage();
		MPLMiErrorMessageId mPLMiErrorMessageId=new MPLMiErrorMessageId();
		
		mPLMiErrorMessage.setCreatedOn(new Timestamp(0));
		mPLMiErrorMessage.setErrorDate(new Date(0));
		mPLMiErrorMessage.setMessageCount(60);
		mPLMiErrorMessage.setMessageId("ABC");
		mPLMiErrorMessage.setPageId("XYZ");
		mPLMiErrorMessageId.setMessageId("PQR");
		mPLMiErrorMessageId.setPageId("PHP");
		mPLMiErrorMessageId.setErrorDate(new Date(2));
		
        Assert.assertNotNull(mPLMiErrorMessage.getCreatedOn());
   	    Assert.assertNotNull(mPLMiErrorMessage.getErrorDate());
   	    Assert.assertNotNull(mPLMiErrorMessage.getMessageCount());
   	    Assert.assertNotNull(mPLMiErrorMessage.getMessageId());
   	    Assert.assertNotNull(mPLMiErrorMessage.getPageId());
   	    Assert.assertNotNull(mPLMiErrorMessageId.getErrorDate());
   	    Assert.assertNotNull(mPLMiErrorMessageId.getMessageId());
   	    Assert.assertNotNull(mPLMiErrorMessageId.getPageId());
   	    }

}
