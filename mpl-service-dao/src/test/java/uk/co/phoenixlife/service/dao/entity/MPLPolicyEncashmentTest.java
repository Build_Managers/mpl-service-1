package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.constants.MPLConstants;

public class MPLPolicyEncashmentTest {
	@Test
    public void testMPLlPolicyEncashmentSuccess() {

        final MPLPolicyEncashment mplPolicyEncashmenttest = new MPLPolicyEncashment();
        mplPolicyEncashmenttest.setEncashmentId(new Long(0));
        mplPolicyEncashmenttest.setEncashmentPolicy(new MPLCustomerPolicy());
        mplPolicyEncashmenttest.setEncashRefNum("123");
        mplPolicyEncashmenttest.setCreatedOn(Timestamp.valueOf(LocalDateTime.now()));
        mplPolicyEncashmenttest.setCreatedBy(MPLConstants.SYSTEM);
        mplPolicyEncashmenttest.setTransactionId("ABC");

        Assert.assertNotNull(mplPolicyEncashmenttest.getEncashmentId());
        Assert.assertNotNull(mplPolicyEncashmenttest.getEncashmentPolicy());
        Assert.assertNotNull(mplPolicyEncashmenttest.getEncashRefNum());
        Assert.assertNotNull(mplPolicyEncashmenttest.getCreatedOn());
        Assert.assertNotNull(mplPolicyEncashmenttest.getCreatedBy());
        Assert.assertNotNull(mplPolicyEncashmenttest.getTransactionId());

    }

}
