package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MPLPolicyLockOutTest {
	@Test
    public void testMPLPolicyLockOutSuccess() {

        final MPLPolicyLockOut mPLPolicyLockOut = new MPLPolicyLockOut();
        mPLPolicyLockOut.setMplId(190L);
        mPLPolicyLockOut.setPolicyVerificationCount(25);
        mPLPolicyLockOut.setCreatedOn(new Timestamp(10));
        mPLPolicyLockOut.setLastUpdatedBy("ABC");
        mPLPolicyLockOut.setLastUpdatedOn(new Timestamp(15));
        mPLPolicyLockOut.setLegacyPolicynumber("PQR");
        mPLPolicyLockOut.setPolicyLockStatus('A');
        
        Assert.assertNotNull( mPLPolicyLockOut.getMplId());
        Assert.assertNotNull( mPLPolicyLockOut.getCreatedOn());
        Assert.assertNotNull( mPLPolicyLockOut.getLastUpdatedBy());
        Assert.assertNotNull( mPLPolicyLockOut.getLastUpdatedOn());
        Assert.assertNotNull( mPLPolicyLockOut.getLegacyPolicynumber());
        Assert.assertNotNull( mPLPolicyLockOut.getPolicyLockStatus());
        Assert.assertNotNull( mPLPolicyLockOut.getPolicyVerificationCount());
}
}