package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MPLProductMatrixTest {
	@Test
    public void testMPLProductMatrixSuccess() {

        final MPLProductMatrix mPLProductMatrix = new MPLProductMatrix();
        mPLProductMatrix.setCreatedBy("ABC");
        mPLProductMatrix.setCreatedOn(new Timestamp(10));
        mPLProductMatrix.setCurrentPremiumFlag('Y');
        mPLProductMatrix.setDeathFlag('N');
        mPLProductMatrix.setFundFlag('A');
        mPLProductMatrix.setFundHoldingFlag('B');
        mPLProductMatrix.setGarFlag('C');
        mPLProductMatrix.setGmpFlag('D');
        mPLProductMatrix.setLastUpdatedBy("ABCD");
        mPLProductMatrix.setLastUpdatedOn(new Timestamp(12));
        mPLProductMatrix.setLifestyleFlag('E');
        mPLProductMatrix.setPolicyFlag("AB");
        mPLProductMatrix.setProductCode("XYZ");
        mPLProductMatrix.setProductName("PQRS");
        mPLProductMatrix.setProductRowId(10L);
        mPLProductMatrix.setTransferFlag('Y');
        
        Assert.assertNotNull( mPLProductMatrix.getCreatedBy());
        Assert.assertNotNull( mPLProductMatrix.getCreatedOn());
        Assert.assertNotNull( mPLProductMatrix.getCurrentPremiumFlag());
        Assert.assertNotNull( mPLProductMatrix.getDeathFlag());
        Assert.assertNotNull( mPLProductMatrix.getFundFlag());
        Assert.assertNotNull( mPLProductMatrix.getFundHoldingFlag());
        Assert.assertNotNull( mPLProductMatrix.getGarFlag());
        Assert.assertNotNull( mPLProductMatrix.getGmpFlag());
        Assert.assertNotNull( mPLProductMatrix.getLastUpdatedBy());
        Assert.assertNotNull( mPLProductMatrix.getLastUpdatedOn());
        Assert.assertNotNull( mPLProductMatrix.getLifestyleFlag());
        Assert.assertNotNull( mPLProductMatrix.getPolicyFlag());
        Assert.assertNotNull( mPLProductMatrix.getProductCode());
        Assert.assertNotNull( mPLProductMatrix.getProductName());
        Assert.assertNotNull( mPLProductMatrix.getProductRowId());
        Assert.assertNotNull( mPLProductMatrix.getTransferFlag());

}
}