package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.constants.MPLConstants;

public class MPLQuoteDetailsTest {
	 @Test
	    public void testMPLQuoteDetailsSuccess() {

	        final MPLQuoteDetails mplQuoteDetailstest = new MPLQuoteDetails();
	        MPLQuoteUpdateStatus quoteUpdateStatus = new MPLQuoteUpdateStatus();
	   
	        mplQuoteDetailstest.setQuoteUpdateStatus(quoteUpdateStatus);
	        mplQuoteDetailstest.setQuoteId(new Long(0));
	        mplQuoteDetailstest.setQuotePolicy(new MPLCustomerPolicy());
	        mplQuoteDetailstest.setPaymentMethod('N');
	        mplQuoteDetailstest.setSortCode("123".getBytes());
	        mplQuoteDetailstest.setBankAcctNo("343376756775657".getBytes());
	        mplQuoteDetailstest.setRollNo("2123".getBytes());
	        mplQuoteDetailstest.setCashInType("Cheque");
	        mplQuoteDetailstest.setContactCustomer('N');
	        mplQuoteDetailstest.setCreatedOn(Timestamp.valueOf(LocalDateTime.now()));
	        mplQuoteDetailstest.setCreatedBy(MPLConstants.SYSTEM);

	        Assert.assertNotNull(mplQuoteDetailstest.getQuoteId());
	        Assert.assertNotNull(mplQuoteDetailstest.getQuotePolicy());
	        Assert.assertNotNull(mplQuoteDetailstest.getPaymentMethod());
	        Assert.assertNotNull(mplQuoteDetailstest.getSortCode());
	        Assert.assertNotNull(mplQuoteDetailstest.getBankAcctNo());
	        Assert.assertNotNull(mplQuoteDetailstest.getRollNo());
	        Assert.assertNotNull(mplQuoteDetailstest.getCashInType());
	        Assert.assertNotNull(mplQuoteDetailstest.getContactCustomer());
	        Assert.assertNotNull(mplQuoteDetailstest.getQuoteUpdateStatus());
	        Assert.assertNotNull(mplQuoteDetailstest.getCreatedOn());
	        Assert.assertNotNull(mplQuoteDetailstest.getCreatedBy());

	    }

}
