package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MPLQuoteUpdateAttemptTest {
	@Test
    public void testMPLProductMatrixSuccess() {

        final MPLQuoteUpdateAttempt mPLQuoteUpdateAttempt = new MPLQuoteUpdateAttempt();
	
        MPLQuoteUpdateStatus quoteUpdateStatus = new MPLQuoteUpdateStatus();
        mPLQuoteUpdateAttempt.setAttemptId(10L);
        mPLQuoteUpdateAttempt.setSentFlag('y');
        mPLQuoteUpdateAttempt.setSentTmst(new Timestamp(15L));
        mPLQuoteUpdateAttempt.setAckFlag('n');
        mPLQuoteUpdateAttempt.setAckTmst(new Timestamp(10L));
        mPLQuoteUpdateAttempt.setFailureReason("ABCD");
        mPLQuoteUpdateAttempt.setQuoteUpdateStatus(quoteUpdateStatus);
        
        Assert.assertNotNull(mPLQuoteUpdateAttempt.getAttemptId());
        Assert.assertNotNull(mPLQuoteUpdateAttempt.getSentFlag());
        Assert.assertNotNull(mPLQuoteUpdateAttempt.getSentTmst());
        Assert.assertNotNull(mPLQuoteUpdateAttempt.getAckFlag());
        Assert.assertNotNull(mPLQuoteUpdateAttempt.getAckTmst());
        Assert.assertNotNull(mPLQuoteUpdateAttempt.getFailureReason());
        Assert.assertNotNull(mPLQuoteUpdateAttempt.getQuoteUpdateStatus());
}
}