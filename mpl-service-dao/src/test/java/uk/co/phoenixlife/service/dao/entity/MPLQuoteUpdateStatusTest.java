package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MPLQuoteUpdateStatusTest {
	@Test
    public void testMPLQuoteUpdateStatusSuccess() {

        final MPLQuoteUpdateStatus mPLQuoteUpdateStatus = new MPLQuoteUpdateStatus();
        Set<MPLQuoteUpdateAttempt> quoteUpdateAttempts = new HashSet<MPLQuoteUpdateAttempt>();
        MPLQuoteDetails updatedQuote = new MPLQuoteDetails();
        mPLQuoteUpdateStatus.setUpdateId(10);
        mPLQuoteUpdateStatus.setAttempt(12);
        mPLQuoteUpdateStatus.setStatus('A');
        mPLQuoteUpdateStatus.setTimestamp(new Timestamp(10));
        mPLQuoteUpdateStatus.setQuoteUpdateAttempts(quoteUpdateAttempts);
        mPLQuoteUpdateStatus.setUpdatedQuote(updatedQuote);
        
        Assert.assertNotNull(mPLQuoteUpdateStatus.getUpdateId());
        Assert.assertNotNull(mPLQuoteUpdateStatus.getStatus());
        Assert.assertNotNull(mPLQuoteUpdateStatus.getTimestamp());
        Assert.assertNotNull(mPLQuoteUpdateStatus.getQuoteUpdateAttempts());
        Assert.assertNotNull(mPLQuoteUpdateStatus.getUpdatedQuote());
}
}