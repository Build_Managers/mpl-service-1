package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MPLStaticDataTest {
	 @Test 
     public void testMPLStaticDataSuccess()throws Exception
     {
    	 MPLStaticData mplStaticData = new MPLStaticData();
    	 mplStaticData.setStaticDataId(new Long(0));
    	 mplStaticData.setStaticDataGroup("ABC");
    	 mplStaticData.setStaticDataKey("PQR");
    	 mplStaticData.setStaticDataValue("XYZ");
    	 mplStaticData.setCreatedOn(new Timestamp(0));
    	 mplStaticData.setCreatedBy("ABC01");
    	 mplStaticData.setLastUpdatedOn(new Timestamp(0));
    	 mplStaticData.setLastUpdatedBy("ABC02");
    	 mplStaticData.setQuestionLabel("PHP");
    	
    	 Assert.assertNotNull(mplStaticData.getStaticDataId());
    	 Assert.assertNotNull(mplStaticData.getStaticDataGroup());
    	 Assert.assertNotNull(mplStaticData.getStaticDataKey());
    	 Assert.assertNotNull(mplStaticData.getStaticDataValue());
    	 Assert.assertNotNull(mplStaticData.getCreatedOn());
    	 Assert.assertNotNull(mplStaticData.getCreatedBy());
    	 Assert.assertNotNull(mplStaticData.getLastUpdatedOn());
    	 Assert.assertNotNull(mplStaticData.getLastUpdatedBy());
    	 Assert.assertNotNull(mplStaticData.getQuestionLabel());
     }

}
