package uk.co.phoenixlife.service.dao.entity;

import java.sql.Timestamp;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MPLVerificationAttemptsTest {
	 @Test 
     public void testMPLVerificationAttempts()throws Exception
     {
    	 MPLVerificationAttempts mplVerificationAttempts = new MPLVerificationAttempts();
    	 MPLCustomerPolicy mPLCustomerPolicy=new MPLCustomerPolicy();
    	 

    	 mplVerificationAttempts.setVerificationId(4L);
    	 mplVerificationAttempts.setQuestionId("ABC");
    	 mplVerificationAttempts.setCreatedBy("XYZ");
    	 mplVerificationAttempts.setCreatedOn(new Timestamp(0));
    	 mplVerificationAttempts.setQuestionSkipped('M');
    	 mplVerificationAttempts.setVerificationPolicy(mPLCustomerPolicy);
    	 
    	 Assert.assertNotNull(mplVerificationAttempts.getVerificationId());
    	 Assert.assertNotNull(mplVerificationAttempts.getCreatedBy());
    	 Assert.assertNotNull(mplVerificationAttempts.getCreatedOn());
    	 Assert.assertNotNull(mplVerificationAttempts.getQuestionId());
    	 Assert.assertNotNull(mplVerificationAttempts.getQuestionSkipped());
    	 Assert.assertNotNull(mplVerificationAttempts.getVerificationPolicy());
     }
}
