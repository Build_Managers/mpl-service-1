package uk.co.phoenixlife.service.dao.repository;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dao.entity.MPLComplianceAudit;

public class ComplianceRepositoryImplTest {
	@InjectMocks
    private ComplianceRepositoryImpl repoImpl;

    @Mock
    private EntityManager entityManager;
    @Mock
    private ComplianceRepository repository;
    @Mock
    private Query query;

    @BeforeTest
    public void runBeforeEveryTest() {
        MockitoAnnotations.initMocks(this);
    }

    @AfterTest
    public void runAfterEveryTest() {
        repoImpl = null;
    }

    @Test(enabled = true)
    public void saveComplianceData() throws Exception {
        MPLComplianceAudit complianceData = new MPLComplianceAudit();
        MPLComplianceAudit savedDetails;
        complianceData.setSessionId("abcs-wdjajd-wdajdanjk-adakld");
        complianceData.setEventVal("GB 909995 A".getBytes());
        complianceData.setEventTs(Timestamp.valueOf(LocalDateTime.now()));
        complianceData.setEventCode("PG02_POLICY_NO");

        Mockito.when(entityManager.createNativeQuery(Matchers.anyString())).thenReturn(query);
        Mockito.when(repository.saveAndFlush(Matchers.any(MPLComplianceAudit.class))).thenReturn(complianceData);
        savedDetails = repoImpl.saveComplianceData(complianceData);

        Assert.assertEquals(complianceData, savedDetails);
    }

}
