package uk.co.phoenixlife.service.dao.repository;

import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dao.entity.MPLMiDropOut;

public class MiDropOutRepositoryImplTest {
	@InjectMocks
    private MiDropOutRepositoryImpl repoImpl;

    @Mock
    private EntityManager entityManager;
    @Mock
    private MiDropOutReportRepository repository;
    @Mock
    private Query query;

    @BeforeTest
    public void runBeforeEveryTest() {
        MockitoAnnotations.initMocks(this);
    }

    @AfterTest
    public void runAfterEveryTest() {
        repoImpl = null;
    }

    @Test(enabled = true)
    public void saveDropOutData() {
        MPLMiDropOut dropOutData = new MPLMiDropOut();
        dropOutData.setEmailAddress("richard@yahoo.com".getBytes());

        Mockito.when(entityManager.createNativeQuery(Matchers.anyString())).thenReturn(query);
        repoImpl.saveDropOutData(dropOutData);
        Assert.assertNotNull(repoImpl);
    }

    @Test(enabled = true)
    public void updateDropOutTimeSpent() {
        MPLMiDropOut dropOutData = new MPLMiDropOut();
        dropOutData.setEmailAddress("richard@yahoo.com".getBytes());

        Mockito.when(entityManager.createNativeQuery(Matchers.anyString())).thenReturn(query);
        Mockito.when(query.setParameter(Matchers.anyString(), Matchers.anyByte()))
                .thenReturn(query);
        repoImpl.updateDropOutTimeSpent(10, "richard@yahoo.com", "PG11", "abcd-efgh",
                Date.valueOf(LocalDate.now()));
        Assert.assertNotNull(repoImpl);
    }

}
