package uk.co.phoenixlife.service.dao.repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dao.entity.MPLQuoteDetails;

public class QuoteDetailsRepositoryImplTest {
	@InjectMocks
    private QuoteDetailsRepositoryImpl repoImpl;

    @Mock
    private EntityManager entityManager;
    @Mock
    private QuoteDetailsRepository repository;
    @Mock
    private Query query;

    @BeforeTest
    public void runBeforeEveryTest() {
        MockitoAnnotations.initMocks(this);
    }

    @AfterTest
    public void runAfterEveryTest() {
        repoImpl = null;
    }

    @Test(enabled = true)
    public void saveQuoteDetail() {
        MPLQuoteDetails dummy = new MPLQuoteDetails();
        MPLQuoteDetails quote;

        dummy.setBankAcctNo("1919191".getBytes());

        Mockito.when(entityManager.createNativeQuery(Matchers.anyString())).thenReturn(query);
        Mockito.when(repository.saveAndFlush(Matchers.any(MPLQuoteDetails.class)))
                .thenReturn(dummy);
        quote = repoImpl.saveQuoteDetail(dummy);

        Assert.assertEquals(new String(quote.getBankAcctNo()), "1919191");
    }

    @Test(enabled = true)
    public void findOne() {
        MPLQuoteDetails dummy = new MPLQuoteDetails();
        MPLQuoteDetails quote;

        dummy.setBankAcctNo("1919191".getBytes());

        Mockito.when(entityManager.createNativeQuery(Matchers.anyString())).thenReturn(query);
        Mockito.when(repository.findOne(Matchers.anyLong())).thenReturn(dummy);
        quote = repoImpl.findOne(1L);

        Assert.assertEquals(new String(quote.getBankAcctNo()), "1919191");
    }

    @Test(enabled = true)
    public void findQuoteDetails() {
        Object[] dummy = new Object[4];
        Object[] data;
        dummy[0] = "PPP/2121241";

        Mockito.when(entityManager.createNativeQuery(Matchers.anyString())).thenReturn(query);
        Mockito.when(entityManager.createQuery(Matchers.anyString())).thenReturn(query);
        Mockito.when(query.setParameter(Matchers.anyString(), Matchers.anyByte()))
                .thenReturn(query);
        Mockito.when(query.getSingleResult()).thenReturn(dummy);

        data = repoImpl.findQuoteDetails(1L);

        Assert.assertEquals(data[0], "PPP/2121241");
    }

}
