package uk.co.phoenixlife.service.constants;

public class AuthorConstants {

    public static String cashInType = "UFPLS";

    public static double pCLS = 0;

    public static double tax = 0;

    public static Double totValue = 0.00;

    public static String polNum = "-";

    public static double estimatedValue = -99.99;

    public static String gARorGAO = "-";

    public static Double mvaValue = 0.00;
    public static Double penValue = 0.00;

    public static Double cUCharge = 0.00;

    public static String mvaAuthor = "Y";

    public static Integer eligibleCount = 1;

    public static Integer lockedCount = 0;

    public static Boolean isEligible = true;

    public static String blankString = "";

}
