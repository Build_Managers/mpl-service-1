/*
 * MPLConstants.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.constants;

/**
 * MPLConstants.java
 */
public final class MPLConstants {

    public static final String CUSTOMERACTIVATION = "CUSTOMERACTIVATION";

    public static final String CUSTOMERENCASHMENT = "CUSTOMERENCASHMENT";

    public static final String ENCASHMENT_DASHBOARD = "encashmentDashboard";
    /**
     * Remember me cookie name
     */
    public static final String COOKIENAME = "uidmpl";
    
    
    /**
     * SMSESSION
     */
    public static final String SMSESSION = "SMSESSION";


    /**
     * Asterisk sign
     */
    public static final String POSTCODEMASKSIGN = "**";
    /**
     * Ampersand sign
     */
    public static final String AMPSIGN = "&";

    /**
     * Greater Than sign
     */
    public static final String GRTSIGN = ">";

    /**
     * Less than sign
     */
    public static final String LTSIGN = "<";

    /**
     * constant for 120f
     */
    public static final float ONEHUNDEREDTWENTYF = 120f;
    /**
     * constant for 40
     */
    public static final int FOURTY = 40;
    /**
     * constant for 792
     */
    public static final int SEVENHUNDEREDNINTYTWO = 792;
    /**
     * constant for 470
     */
    public static final int FOURHUNDEREDSEVENTY = 470;
    /**
     * constant for 820
     */
    public static final int EIGHTHUNDEREDTWENTY = 820;
    /**
     * constant for 570
     */
    public static final int FIVEHUNDEREDSEVENTY = 570;
    /**
     * constant for 10
     */
    public static final int TEN = 10;
    /**
     * constant for 20
     */
    public static final int TWENTY = 20;
    /**
     * constant for 50
     */
    public static final int FIFTY = 50;
    /**
     * constant for 60
     */
    public static final int SIXTY = 60;
    /**
     * constant for 30
     */
    public static final int THIRTY = 30;
    /**
     * constant for 600
     */
    public static final int SIXHUNDERED = 600;
    /**
     * constant for ZERO
     */
    public static final int ZERO = 0;
    /**
     * constant for ONE
     */
    public static final int ONE = 1;
    /**
     * constant for TWO
     */
    public static final int TWO = 2;

    /**
     * constant for THREE
     */
    public static final int THREE = 3;
    /**
     * constant for Four
     */
    public static final int FOUR = 4;

    /**
     * constant for FIVE
     */
    public static final int FIVE = 5;
    /**
     * constant for SIX
     */
    public static final int SIX = 6;
    /**
     * constant for 100
     */
    public static final String HUNDERED = "100";
    /**
     * constant for one point one
     */
    public static final double ONEPOINTONE = 1.1;

    /**
     * Wait Time for thread to complete
     */
    public static final int WAITTIME = 2 * 1000;

    /**
     * Wait attempt for thread to complete
     */
    public static final int WAITATTEMPTS = 3;
    /**
     * Constant for 5 minutes
     */
    public static final int FIVE_MINUTES = 1 * 60 * 1000;
    /**
     * constant TWENTY_FOUR
     */
    public static final int TWENTY_FOUR = 24;

    /**
     * 1000000 - 1 million
     */
    public static final int TEN_LACS = 1000000;

    /**
     * 100- Hundred_int
     */
    public static final int HUNDRED_INT = 100;

    /** constant for back slash. */
    public static final String BACKLSLASH = "/";

    /** constant for back true. */
    public static final String TRUE = "true";

    /** MPL service context *. */
    public static final String SERVICE_CONTEXT = "MPLService";

    /** constant for retirement *. */
    public static final String RET = "RET";

    /** constant for EME_LC. */
    public static final String EME_LC = "EME_LC";

    /** constant for EMR_LC. */
    public static final String EMR_LC = "EMR_LC";

    /** constant for Transfer protected. */
    public static final String TRANSFERPROTECTED = "TRI_PR";

    /** constant for Transfer non protected. */
    public static final String TRANSFERNONPROTECTED = "TRI_NPR";

    /** BANCs PCA SOAP Service context *. */
    public static final String PCA_CONTEXT = "PostcodeAnywhere/uk";

    /** The Constant PCA_FIND_CONTEXT. */
    public static final String PCA_FIND_CONTEXT = "PostcodeAnywhere/find/uk";

    /** The Constant PCA_FIND_SOAP_ACTION. */
    public static final String PCA_FIND_SOAP_ACTION = "http://services.postcodeanywhere.co.uk/Capture_Interactive_Find_v1_00";

    /** The Constant PCA_RETRIEVE_CONTEXT. */
    public static final String PCA_RETRIEVE_CONTEXT = "PostcodeAnywhere/retrieve/uk";

    /** The Constant PCA_RETRIEVE_SOAP_ACTION. */
    public static final String PCA_RETRIEVE_SOAP_ACTION = "http://services.postcodeanywhere.co.uk/Capture_Interactive_Retrieve_v1_00";

    /** BANCs Bank Wizard SOAP Service context *. */
    public static final String BANK_WIZARD_CONTEXT = "BankWizard/services";

    /** The Constant BANK_WIZARD_SOAP_ACTION. */
    public static final String BANK_WIZARD_SOAP_ACTION = "http://experian.co.uk/Validation";

    /** Bank Wizard WSDL version **/
    public static final String WSDL_VERSION = "1.0";

    /**
     * Check_Type
     */
    public static final String CHECK_TYPE = "VALIDATE_ONLY";

    /** ************ Constants for BANCs post calls start *************. */

    /**
     * constant for 'Y'
     */
    public static final char YES = 'Y';

    /** constant for 'N'. */
    public static final char NO = 'N';

    /**
     * constant for 'Y'
     */
    public static final char EMAILSUCCESS = 'S';

    /** constant for 'N'. */
    public static final char EMAILFAILED = 'F';

    /** constant for 'C'. */
    public static final char ALPHABET_C = 'C';

    /** The date format. */
    public static final String DATE_FORMAT_POLICY_START_DATE = "dd/MM/yyyy";

    /** constant for false. */
    public static final String FALSE = "false";

    /** constant for UK. */
    public static final String UNITED_KINGDOM = "UK";

    /** constant for Mobile. */
    public static final String MOBILE = "Mobile";

    /** constant for doc-code. */
    public static final String OUTBOUND = "OUTBOUND";

    /** constant for pdf. */
    public static final String MEDIA_TYPE_PDF = "pdf";

    /** constant for SYSTEM. */
    public static final String SYSTEM = "SYSTEM";

    /** constant for success. */
    public static final String SUCCESS = "success";

    /** constant for staticDataList. */
    public static final String STATICDATALIST = "staticDataList";

    /** constant for policyId. */
    public static final String POLICYID = "policyId";

    /** constant for EE SINV. */
    public static final String EESINV = "EME_SINV";

    /** constant for ER SINV. */
    public static final String ERSINV = "EMR_SINV";

    /** constant for shuffleAndShow. */
    public static final String SHUFFLEANDSHOW = "shuffleAndShow";

    /** constant for unchecked. */
    public static final String UNCHECKED = "unchecked";

    /** constant for ?policyId=. */
    public static final String PARAMETERPOLICYID = "?policyId=";

    /** constant for ?policynumber=. */
    public static final String PARAMETERPOLICYNUMBER = "?policynumber=";

    /** constant for count. */
    public static final String COUNT = "count";

    /** constant for fail. */
    public static final String FAIL = "fail";

    /** constant for exhaust. */
    public static final String EXHAUST = "exhaust";

    /** constant for verification. */
    public static final String VERIFICATION = "verification";

    /** constant for sessionCheckAnswerMap. */
    public static final String SESSIONCHECKANSWERMAP = "sessionCheckAnswerMap";

    /** constant for Direct Debit. */
    public static final String DIRECTDEBIT = "Direct Debit";

    /** constant for at the rate. */
    public static final String ATTHERATE = "@";

    /** constant for fundName question key. */
    public static final String FUNDNAME = "fundName";

    /** constant for niNo question key. */
    public static final String NINO = "niNo";

    /** constant for benefitName question key. */
    public static final String BENEFITNAME = "benefitName";

    /** constant for lastPremPaid question key. */
    public static final String LASTPREMIUMPAID = "lastPremPaid";

    /** constant for sortCodebankAcct question key. */
    public static final String SORTCODEACCOUNTNUMBER = "sortCodebankAcct";

    /** constant for polStDate question key. */
    public static final String POLICYSTARTDATE = "polStDate";

    /** constant for date format. */
    public static final String DATE_FORMAT = "dd/MM/YYYY";

    /** constant for date String customer. */
    public static final String CUSTOMER = "customer";

    /** constant for date String quote. */
    public static final String QUOTE = "quote";

    /** constant for date String bancslog. */
    public static final String BANCS_LOG = "bancslog";

    /** constant for Response. */
    public static final String RESPONSE_POST_CALL = "Response from BANCs Post call-->>{}";

    /** Bancs call error. */
    public static final String BANCS_ERROR = "Error occured during BANCs call -->>{}";

    /** Bancs call success. */
    public static final String BANCS_SUCCESS = "BANCs call successfully executed -->>{}";

    /** Constants for Error Details. */
    public static final String ERROR_DETAIL = "error_Detail";

    /** Constants for CONNECTION_ERROR */
    public static final String CONNECTION_ERROR = "Error connecting BANCS Server..";

    /** Constants for TAXATION QUE LIST. */
    public static final String QUELIST = "questionsList";

    /** The Constant SYSTEM ERROR MESSAGE. */
    public static final String SYSTEMERRORMESSAGE = "A systems error has occurred which means you are unable to continue with your online application. We apologise for the inconvenience. Please ";
    /** The Constant ISPOLICYLOCKEDRESTAPI. */
    public static final String ISPOLICYLOCKEDRESTAPI = "MPLService/policy/ispolicylocked";

    /** The Constant UPDATECUSTOMERPOLICYTABLEAPI. */
    public static final String UPDATECUSTOMERPOLICYTABLEAPI = "MPLService/policy/updatecustomerpolicytable";

    /** The Constant FINDQUESTIONKEYAPI. */
    public static final String FINDQUESTIONKEYAPI = "MPLService/policy/findquestionkey";

    /** The Constant UPDATEVERIFICATIONATTEMPTTABLEAPI. */
    public static final String UPDATEVERIFICATIONATTEMPTTABLEAPI = "MPLService/policy/updateverificationattemptstable";

    /** The Constant INSERTINTOCUSTOMERTABLERESTAPI. */
    public static final String INSERTINTOCUSTOMERTABLERESTAPI = "MPLService/policy/insertintocustomertable";

    /** The Constant UPDATECUSTOMERTABLERESTAPI. */
    public static final String UPDATECUSTOMERTABLERESTAPI = "MPLService/policy/updateCustomerTable";

    /** Constants for PENSION QUE LIST. */
    public static final String QUELISTPENSION = "questionsListPension";

    /** ************ Constants for BANCs post calls end *************. */

    /************** Constants for BaNCS Call ************************/
    public static final String MULTIPLEPOLICYKEY = "multiplePolicy";

    /** The Constant RETQUOTEVALSESSIONKEY. */
    public static final String RETQUOTEVALSESSIONKEY = "RetQuoteVal Session";

    /** The Constant CLAIMSESSIONKEY. */
    public static final String CLAIMSESSIONKEY = "ClaimList Session";

    /** The Constant POLDETAILSSESSIONKEY. */
    public static final String POLDETAILSSESSIONKEY = "PolDetails Session";

    /** ************ Constants for BaNCS Call ends *****************. */
    /************** Constants for Dashboard ************************/
    public static final String FINALDASHBOARDKEY = "finalDashboard";

    /************** Constants for EncashmentDashboard ************************/
    public static final String ENCASHMENT_DASHBOARDKEY = "encashmentDashboard";

    /** The Constant HHMMKEY. */
    public static final String HHMMKEY = "hhmm";

    /** The Constant HHMMFORMAT. */
    public static final String HHMMFORMAT = "HH:mm";

    /** ************ Constants for Dashboard ends *****************. */

    /************** Misc Constants ******************/
    /**
     * Constant for Analytics Group
     */
    public static final String GOOGLEANALYTICS_GROUP = "Analytics";

    /**
     * Tracking ID KEY
     */
    public static final String TRACKINGIDKEY = "TrackingId";

    /**
     * constant for redirecting to verification ftl
     */
    public static final String RENDERVERIFICATIONPAGE = "templates/components/verification.ftl";

    /** Error_page */
    public static final String REDIRECTERROR_PAGE = "redirect:error_page";

    /**
     * "templates/areas/main.ftl"
     */
    public static final String AREA_MAINPAGE = "templates/areas/main.ftl";
    /**
     * "templates/pages/main.ftl"
     */
    public static final String RENDER_MAINPAGE = "templates/pages/main.ftl";
    /**
     * constant for value policy number
     */

    public static final String POLICYNUM = "policyNumberInSession";
    /**
     * CustID Key
     */
    public static final String CUSTID = "customerIdList";
    /**
     * Value Key
     */
    public static final String VALUE = "value";

    /** The Constant QUESTIONLABEL. */
    public static final String QUESTIONLABEL = "questionLabel";

    /** The Constant EMAILPAGEFTL. */
    public static final String EMAILPAGEFTL = "templates/components/emailPage.ftl";

    /** The Constant PERSONALDETAILSFTL. */
    public static final String PERSONALDETAILSFTL = "templates/components/personalDetails.ftl";

    /** The Constant ENCASHMENTEMAILFTL. */
    public static final String ENCASHMENTEMAILFTL = "encashmentAckEmail.ftl";

    /** The Constant WHITESPACE. */
    public static final String WHITESPACE = " ";

    /** The Constant LOCKEDSTATUS. */
    public static final String LOCKEDSTATUS = "lockedStatus";

    /** The Constant NINOKEY. */
    public static final String NINOKEY = "niNo";

    /** The Constant PRIMARYCONTACTKEY. */
    public static final String PRIMARYCONTACTKEY = "mob";

    /** The Constant SECONDRYCONTACTKEY. */
    public static final String SECONDRYCONTACTKEY = "secContactNum";

    /** The Constant ADDTIONALCONTACTKEY. */
    public static final String ADDTIONALCONTACTKEY = "additionalContactNum";

    /** The Constant HOUSEKEY. */
    public static final String HOUSEKEY = "house";

    /** The Constant NLITERAL. */
    public static final String NLITERAL = "N";

    /** The Constant YES. */
    public static final String YESLITERAL = "YES";
    /** The Constant NO. */
    public static final String NOLITERAL = "NO";
    /** The Constant PolicyID */
    public static final String POLICYIDKEY = "policyId";

    /** The Constant CUSTOMER_ID_KEY. */
    public static final String CUSTOMER_ID_KEY = "customerId";

    /** The Constant BANKDETAILSNINOKEY. */
    public static final String BANKDETAILSNINOKEY = "bankDetailsNino";

    /** The Constant SITEKEY. */
    public static final String SITEKEY = "siteKey";

    /** The Constant VALIDDASHBOARDKEY */
    public static final String VALIDDASHBOARDKEY = "validDashboard";

    /** The Constant XGUIDKEY. */
    public static final String XGUIDKEY = "XGUID";
    /** xguid Small letters */
    public static final String XGUID_LOWERCASE = "xguid";
    /** The Constant XGUIDKEY. */
    public static final String XGUIDKEYFORAPI = "X-GUID";
    /** The Constant PAGEIDSESSIONKEY. */
    public static final String PAGEIDSESSIONKEY = "PGID";
    
    /** The Constant MPLAllowedPGID. */
    public static final String MPLALLOWEDPGID = "MPLAllowedPGID";
    
    /** The Constant MPLNavURLPGID. */
    public static final String MPLNAVURLPGID = "MPLNavURLPGID";
    
    
    /** Constants MPLVALIDATIONERROR */
    public static final String MPLVALIDATIONERROR = "MPLValidationError"; 
    
    /** The Constant IPADDRESSKEY. */

    public static final String IPADDRESSKEY = "ipAddress";
    /** The Constant STATICDATAGROUP. */

    public static final String STATICDATAGROUP = "MPLComplianceCode";
    /**
     * PG05
     */
    public static final String PG05 = "PG05";
    /**
     * PG02
     */
    public static final String PG02 = "PG02";
    /** PG04 */
    public static final String PG04 = "PG04";
    /** PG03 */
    public static final String PG03 = "PG03";
    /** E100 */
    public static final String E100 = "E100";

    /** The Constant COMPIPADDRESS. */
    public static final String COMPIPADDRESS = "PG02_IPADDRESS";
    /** The Constant COMPMPLCUSTID. */

    public static final String COMPMPLCUSTID = "PG03_MPLCUSTID";
    /** The Constant COMPBNSCUSTID. */

    public static final String COMPBNSCUSTID = "PG03_BNSCUSTID";
    /** The Constant COMPPOLICYNUM. */

    public static final String COMPPOLICYNUM = "PG02_POLICYNUM";
    /** The Constant COMPEMAILADD. */

    public static final String COMPEMAILADD = "PG02_EMAILADDR";
    /** The Constant COMPIDADDRES. */

    public static final String COMPIDADDRES = "PG02_IPADDRESS";
    /** The Constant COMPRPBTNCLK. */

    public static final String COMPRPBTNCLK = "PG08_RPBTNCLCK";
    /** The Constant COMPRPCHKBOX. */

    public static final String COMPRPCHKBOX = "PG08_RPCHCKBOX";
    /** The Constant COMPLFTALWLMT. */

    public static final String COMPLFTALWLMT = "PG15_LFTALWLMT";
    /** The Constant COMPAMTDRPBY5. */

    public static final String COMPAMTDRPBY5 = "PG15_AMTDRPBY5";
    /** The Constant COMPIMPCTDAMT. */

    public static final String COMPIMPCTDAMT = "PG15_IMPCTDAMT";
    /** The Constant COMPRCVGUIDANC. */

    public static final String COMPRCVGUIDANC = "PG17_RCVGUIDANC";
    /** The Constant COMPRCVADVICE. */

    public static final String COMPRCVADVICE = "PG17_RCVADVICE";
    /** The Constant COMPTXIMPLCTN. */

    public static final String COMPTXIMPLCTN = "PG18_TXIMPLCTN";
    /** The Constant COMPISSMALLPT. */

    public static final String COMPISSMALLPT = "PG18_ISSMALLPT";
    /** The Constant COMPEDITANSTS. */

    public static final String COMPEDITANSTS = "PG18_EDITANSTS";
    /** The Constant COMPDCLRCHKBX. */

    public static final String COMPDCLRCHKBX = "PG18_DCLRCHKBX";
    /** The Constant COMPAFDWLDTS. */

    public static final String COMPAFDWLDTS = "PG19_AFDWNLDTS";
    /** The Constant COMPPG02NXTBTNCLK. */

    public static final String COMPPG02NXTBTNCLK = "PG02_NXTBTNCLK";
    /** The Constant COMPPG03PRVBTNCLK. */

    public static final String COMPPG03PRVBTNCLK = "PG03_PRVBTNCLK";

    /** The Constant COMPG03NXTBTNCLK. */
    public static final String COMPG03NXTBTNCLK = "PG03_NXTBTNCLK";
    /** The Constant COMPPG04PRVBTNCLK. */

    public static final String COMPPG04PRVBTNCLK = "PG04_PRVBTNCLK";
    /** The Constant COMPPG04NXTBTNCLK. */

    public static final String COMPPG04NXTBTNCLK = "PG04_NXTBTNCLK";
    /** The Constant COMPPG05PRVBTNCLK. */

    public static final String COMPPG05PRVBTNCLK = "PG05_PRVBTNCLK";
    /** The Constant COMPPG05NXTBTNCLK. */

    public static final String COMPPG05NXTBTNCLK = "PG05_NXTBTNCLK";

    /** The Constant COMPPG06PRVBTNCLK. */
    public static final String COMPPG06PRVBTNCLK = "PG06_PRVBTNCLK";
    /** The Constant COMPPG06NXTBTNCLK. */

    public static final String COMPPG06NXTBTNCLK = "PG06_NXTBTNCLK";
    /** The Constant COMPPG07CASHINCLK. */

    public static final String COMPPG07CASHINCLK = "PG07_CASHINCLK";

    /** The Constant VERIFIEDCUSTKEY. */
    public static final String VERIFIEDCUSTKEY = "VERIFIED";

    /** The Constant TIMESTAMPFORMATCMPLNCEFILE. */

    public static final String TIMESTAMPFORMATCMPLNCEFILE = "yyyyMMddHHmm";

    /************** Misc Constants ends **************/

    /************** FTL Constants ******************/
    public static final String MVRKEYFTL = "mvr";

    /** The Constant EECKEYFTL. */
    public static final String EECKEYFTL = "eec";

    /** ************ FTL Constants ends *************. */
    /************** Constants for PDF end **************/
    /**
     * Constant for AF
     */
    public static final String APPLICATION_FORM = "AF";
    /**
     * Constant for appForm.ftl
     */
    public static final String APPLICATION_FORM_PDF_TEMPLATE = "appForm.ftl";

    /**
     * Constant for retirementPack.ftl
     */
    public static final String RETIREMENTPACK_PDF_TEMPLATE = "retirementPack.ftl";

    /**
     * Constant for dd MMM yyyy
     */
    public static final String DATE_PATTERN_D_MMM_YYYY = "d MMM yyyy";
    /**
     * Constant for SMALL_POT
     */
    public static final String SMALL_POT = "Small Pot";

    /** Constant for UFPLS. */
    public static final String UFPLS = "UFPLS";

    /** Constant for Cheque. */
    public static final String CHEQUE = "Cheque";

    /** Constant for Direct Credit. */
    public static final String DIRECT_CREDIT = "DirectCredit";

    /** ************ Constants for PDF end *************. */

    /************** Constants for Static Data Refresh start **************/
    /**
     * Constant for Recaptcha group
     */
    public static final String RECAPTCHA_GROUP = "Recaptcha";

    /** Constant for ID&V-Question group. */
    public static final String IDV_QUESTION_GROUP = "ID&V-Question";

    /** Constant for Validation-Error. */
    public static final String VALIDATION_ERROR_GROUP = "Validation-Error";

    /** Constant for SCHEDULER_TYPE. */
    public static final String SCHEDULER_TYPE = "{} scheduler completed for static data cache";

    /** ************ Constants for PDF end *************. */
    /************** Dashboard Client ****************/

    public static final String BANCSPOLNUMKEY = "bancsPolNum";

    /** The Constant BANCSUNIQUECUSTNOKEY. */
    public static final String BANCSUNIQUECUSTNOKEY = "bancsUniqueCustNo";

    /** The Constant IDANDVADATARESPONSEKEY. */
    public static final String IDANDVADATARESPONSEKEY = "bancsIdAndVDataResponse";

    /** The Constant POLNUMKEY. */
    public static final String POLNUMKEY = "polNum";

    /** The Constant PARTYBASICRESPONSEKEY. */
    public static final String PARTYBASICRESPONSEKEY = "partyBasicResponse";

    /** The Constant PARTYCOMMDETAILSRESPONSE. */
    public static final String PARTYCOMMDETAILSRESPONSE = "bancsPartyCommDetailsResponse";

    /** The Constant VALIDPOLDETAILSKEY. */
    public static final String VALIDPOLDETAILSKEY = "validPolDetails";

    /** The Constant VALIDCLAIMLISTKEY. */
    public static final String VALIDCLAIMLISTKEY = "validClaimList";

    /** The Constant VALIDRETQUOTEKEY. */
    public static final String VALIDRETQUOTEVALKEY = "validRetQuoteVal";

    /** The Constant POLDETAILBANCSPOLNUMSETKEY. */
    public static final String POLDETAILBANCSPOLNUMSETKEY = "polDetailsBancsPolNumSet";

    /** The Constant CLAIMLISTBANCSPOLNUMSETKEY. */
    public static final String CLAIMLISTBANCSPOLNUMSETKEY = "claimListBancsPolNumSet";

    /** The Constant RETQUOTEBANCSPOLNUMSETKEY. */
    public static final String RETQUOTEBANCSPOLNUMSETKEY = "retQuoteValBancsPolNumSet";

    /** The Constant EMAILIDKEY. */
    public static final String EMAILIDKEY = "emailAddress";

    /** The Constant POLICIESOWNEDBYPARTYRESPONSEKEY. */
    public static final String POLICIESOWNEDBYPARTYRESPONSEKEY = "policiesOwnedByPartyResponse";

    /** The Constant MULTIPLEOWNERKEY. */
    public static final String MULTIPLEOWNERKEY = "multipleOwner";

    /** The Constant DEFAULTDOUBLEBANCS. */
    public static final Double DEFAULTDOUBLEBANCS = -99.99;

    /** The Constant BANCSTOTALVALFLAG. */
    public static final Integer BANCSTOTALVALFLAG = 10000;

    /** The Constant EXCEPTIONKEY. */
    public static final String EXCEPTIONKEY = "exceptionOccured";

    /** The Constant YLITERAL. */
    public static final String YLITERAL = "y";

    /** The Constant CAPITALYLITERAL. */
    public static final String CAPITALYLITERAL = "Y";

    /** The Constant ISNULLKEY. */
    public static final String ISNULLKEY = "is NULL";

    /** The Constant ERRORINAPIKEY. */
    public static final String ERRORINAPIKEY = "Error occured during API call ---------->>>>> {}";

    /** The Constant ERRORINDTO. */
    public static final String ERRORINDTO = "Error occurred while mapping DTO as response {}";

    /** The Constant DCLITERAL. */
    public static final String DCLITERAL = "DC";

    /** The Constant UNITISEDLITERAL. */
    public static final String UNITISEDLITERAL = "N";

    /** The Constant GAULITERAL. */
    public static final String GAVLITERAL = "GAV";

    /** The Constant GAULITERAL. */
    public static final String GAULITERAL = "GAU";

    /** The Constant OWRLITERAL. */
    public static final String OWRLITERAL = "OWR";

    /** The Constant DEATHLITERAL. */
    public static final String DEATHLITERAL = "Death";

    /** The Constant DEPENDANTDRAWDOWNLITERAL. */
    public static final String DEPENDANTDRAWDOWNLITERAL = "Dependant Drawdown";

    /** The Constant FULLENCASHMENTLITERAL. */
    public static final String FULLENCASHMENTLITERAL = "Full Encashment";
    /** The Constant FULLENCASHMENTCD. */

    public static final String FULLENCASHMENTCD = "FENCH";

    /** The Constant FULLTRANSFERLITERAL. */
    public static final String FULLTRANSFERLITERAL = "Full Transfer";
    /** The Constant FULLTRANSFERCD. */

    public static final String FULLTRANSFERCD = "FTRF";

    /** The Constant RETIREMENTLITERAL. */
    public static final String RETIREMENTLITERAL = "Retirement";
    /** The Constant RETIREMENTCD. */

    public static final String RETIREMENTCD = "RET";

    /** The Constant ADMITTERLITERAL. */
    public static final String ADMITTERLITERAL = "Admitted";
    /** The Constant ADMITTERCD. */

    public static final String ADMITTERCD = "ADMT";

    /** The Constant SETTLEDLITERAL. */
    public static final String SETTLEDLITERAL = "Settled";
    /** The Constant SETTLEDCD. */

    public static final String SETTLEDCD = "SETL";

    /** The Constant SETTLEDLITERAL. */
    public static final String DRAFTLITERAL = "Draft";

    /** The Constant DRAFTCD. */
    public static final String DRAFTCD = "DFT";

    /** The Constant INNPPLITERAL. */
    public static final String INNPPLITERAL = "INNPP";

    /** The Constant WVRALITERAl. */
    public static final String WVRALITERAL = "WVRA";

    /** The Constant RLPULITERAL. */
    public static final String RLPULITERAL = "RLPU";

    /** The Constant INFLITERAL. */
    public static final String INFLITERAL = "INF";

    /** The Constant PUPLITERAL. */
    public static final String PUPLITERAL = "PUP";

    /** The Constant CNTRHLITERAL. */
    public static final String CNTRHLITERAL = "CNTRH";

    /** The Constant RESSLITERAL. */
    public static final String RESSLITERAL = "RESS";

    /** The Constant WVRILITERAL. */
    public static final String WVRILITERAL = "WVRI";

    /** The Constant RRPPILITERAL. */
    public static final String RRPPILITERAL = "RRPPI";

    /** The Constant RRPPALITERAL. */
    public static final String RRPPALITERAL = "RRPPA";

    /** The Constant CBRLITERAL. */
    public static final String CBRLITERAL = "CBR";

    /** The Constant BLITERAL. */
    public static final String BLITERAL = "B";

    /** The Constant ULITERAL. */
    public static final String ULITERAL = "U";

    /** The Constant MLITERAL. */
    public static final String MLITERAL = "M";

    /** The Constant TLITERAL. */
    public static final String TLITERAL = "T";

    /** The Constant NOTSTARTEDLITERAL. */
    public static final String NOTSTARTEDLITERAL = "Not Started";

    /** The Constant PENDINGLITERAL. */
    public static final String PENDINGLITERAL = "Pending";

    /** The Constant UKMAINLANDLITERAL. */
    public static final String UKMAINLANDLITERAL = "UK Mainland";
    /** The Constant FUNDTRUSTEEKEY. */

    public static final String FUNDTRUSTEEKEY = "Trustee With Profits Series 1";
    /** The Constant FUNDUNITISEDPROFITSKEY. */

    public static final String FUNDUNITISEDPROFITSKEY = "Unitised With Profits Series 1";
    /** The Constant FUNDUNITISEDPROFITKEY. */

    public static final String FUNDUNITISEDPROFITKEY = "Unitised With Profit Series 1";

    /************** Dashboard Client Ends ***********/

    /************** Constants for Dummy Annuity Quote ***********/
    /**
     * constant ANNUITY_OPTION_CODE
     */
    public static final String ANNUITY_OPTION_CODE = "ANNONLY";
    /**
     * constant ANNUITY_PYMT_FREQ_ANNUAL
     */
    public static final String ANNUITY_PYMT_FREQ_ANNUAL = "A";
    /**
     * constant ANNUITY_ARREARS
     */
    public static final String ANNUITY_ARREARS = "Arr";
    /**
     * constant ANNUITY_SINGLE
     */
    public static final String ANNUITY_SINGLE = "S";
    /**
     * constant ANNUITY_ESCALATION_LEVEL
     */
    public static final String ANNUITY_ESCALATION_LEVEL = "Level";

    /************** Constants for Symmetric Encryption ***********/
    /**
     * constant SYMMETRIC_KEY_OPEN
     */
    public static final String SYMMETRIC_KEY_OPEN = "OPEN SYMMETRIC KEY [MPLPortalDBSymKey] "
            + "DECRYPTION BY CERTIFICATE [MPLPortalCertificate];";
    /**
     * constant SYMMETRIC_KEY_CLOSE
     */
    public static final String SYMMETRIC_KEY_CLOSE = "CLOSE SYMMETRIC KEY [MPLPortalDBSymKey];";

    /************** Constants for Logging ***********/
    /**
     * constant BATCH
     */
    public static final String BATCH = "Scheduler_Batch";
    /**
     * constant REAL_TIME
     */
    public static final String REAL_TIME = "Asynchronous";

    /**
     * annuityQuotes
     */
    public static final String ANNUITYQUOTES = "annuityQuotes";

    /**
     * modalAmt
     */
    public static final String MODALAMT = "modalAmt";

    /** pensionWise */
    public static final String PENSIONWISE = "pensionWise";

    /** financeAdv */
    public static final String FINANCEADV = "financeAdv";

    /** allowanceLimit */
    public static final String ALLOWANCELIMIT = "allowanceLimit";

    /** policyNumber */
    public static final String POLICYNUMBER = "policyNumber";

    /**
     * taxationDetailList
     */
    public static final String TAXATION_DETAIL_LIST = "taxationDetailList";

    /**
     * tax key
     */
    public static final String TAX = "tax";
    /**
     * PCLS
     */
    public static final String PCLS = "pCLS";
    /**
     * AMTPAY
     */
    public static final String AMTPAY = "amtPay";

    /**
     * senttobancs constant
     */
    public static final String SENTTOBANCS = "senttobancs";

    /** RP Literal */
    public static final String RPLITERAL = "RP";

    /************* Generic Exception Constants ****************/
    /**
     * "templates/pages/generic_error.ftl"
     */
    public static final String PAGE_GENERIC_ERROR = "templates/pages/generic_error.ftl";

    /** errCode */
    public static final String ERRCODE = "errCode";

    /** errMsg */
    public static final String ERRMSG = "errMsg";
    /** IRE003 */
    public static final String IRE003 = "IRE003";

    public static final String DASHBOARDCASHINSOURCE = "DASHBOARD";
    public static final String TAXCASHINSOURCE = "TAXCASHIN";
    public static final String CUSTOMERDTO = "customerDTO";

    /** BANCs Bank Validation SOAP Service context *. */
    public static final String BANKVALIDATION_CONTEXT = "BankValidation/services";

    /** Email Status EMAILTYPE*. */
    public static final String EMAILTYPE = "ENC";
    /** Email error 500*. */
    public static final String ERROR500 = "Error 500: Fuse SMTP is down";
    /** No error reported in API */
    public static final String NA = " ";
    /** API Result SUCCESS */
    public static final char SUCCESSFLAG = 'S';
    /** API result FAILED */
    public static final char FAILEDFLAG = 'F';
    /** If BANCs didnt provided data or Response is NULL */
    public static final String NODATAFROMBANCS = "RESPONSE IS BLANK";

    /** Constant value of partybaiscDetailsByPartyNo lifeStatusLif */
    public static final String NOTIFIED_DEATH = "Notified death";
    public static final String CERTIFIED_DEATH = "Certified death";

    /******************* Generic Exception Constants Ends ************/

    /**
     * Default Constructor.
     */
    private MPLConstants() {
        super();
    }
}