/*
 * MPLConstant.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.constants;

/**
 * MPLConstant.java
 */
public final class MPLServiceConstants {

    /**
     * Constructor
     */
    private MPLServiceConstants() {
        super();
    }

    /** constant for char X */
    public static final char A = 'A';
    /** constant ALPHABET_C */
    public static final char ALPHABET_C = 'C';
    /** constant ALPHABET_U */
    public static final char ALPHABET_U = 'U';
    /** constant ALPHABET_X */
    public static final char ALPHABET_X = 'X';

    /** Constant for int 3 */
    public static final int THREE = 3;

    /** Constant for string 'X-GUID' */
    public static final String XGUID_HEADER = "X-GUID";
    /** Constant for string 'xguid' */
    public static final String XGUID_LOWERCASE = "xguid";
    /** constant for SYSTEM. */
    public static final String SYSTEM = "SYSTEM";
    /**
     * Constant for string "M"
     */
    public static final String MLETTER_CAPS = "M";

    /**
     * Constant for dd MMM yyyy
     */
    public static final String DATE_PATTERN_D_MMM_YYYY = "d MMM yyyy";

    /**
     * Constant for string "T"
     */
    public static final String TLETTER_CAPS = "T";

    /**
     * Constant for String "R"
     */
    public static final String RLETTER_CAPS = "R";
    /** The Constant NOTSTARTEDLITERAL. */
    public static final String NOTSTARTEDLITERAL = "Not Started";

    /** The Constant PENDINGLITERAL. */
    public static final String PENDINGLITERAL = "Pending";
    /**
     * Y character in caps
     */
    public static final char YCHARACTERCAPS = 'Y';
    /**
     * N character in caps
     */
    public static final char NCHARACTERCAPS = 'N';
    /**
     * Error String for object mapping
     */
    public static final String ERRORSTRING = "error_Detail";

    public static final String USERNAME_PATTERN = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,12}$";

    /********************** BATCH Constants Start *****************************/
    /** constant FIVE_MINUTES */
    public static final int FIVE_MINUTES = 5 * 60 * 1000;
    /** constant BATCH */
    public static final String BATCH = "Scheduler_Batch";
    /** constant REAL_TIME */
    public static final String REAL_TIME = "Asynchronous";
    /********************** BATCH Constants Ends ******************************/
    /** constant EMAIL_ERROR */
    public static final String EMAIL_ERROR = "Error 500: Fuse SMTP is down";
    /******************** BANCs Post Calls Constants Start ********************/
    /** constant YES */
    public static final char YES = 'Y';
    /** constant NO */
    public static final char NO = 'N';
    /** constant SUCCESS */
    public static final char SUCCESS = 'S';
    /** constant FAILURE */
    public static final char FAILURE = 'F';

    /** constant TRUE */
    public static final String TRUE = "true";

    /** constant TRUE */
    public static final String FALSE = "false";
    /** constant CHEQUE */
    public static final String CHEQUE = "Cheque";
    /** constant DIRECT_CREDIT */
    public static final String DIRECT_CREDIT = "DirectCredit";

    /** constant CUSTOMER */
    public static final String CUSTOMER = "customer";
    /** constant DATE_PATTERN_DDMMYYYY */
    public static final String DATE_PATTERN_DDMMYYYY = "dd/MM/YYYY";
    /** constant OUTBOUND */
    public static final String OUTBOUND = "OUTBOUND";
    /** constant MEDIA_TYPE_PDF */
    public static final String MEDIA_TYPE_PDF = "pdf";
    /******************** BANCs Post Calls Constants Ends ********************/

    /** Constatnt for MyPhoenix token header */
    public static final String TOKENHEADERNAME = "securityToken";

    /******************** Symmetric Encryption Constants *********************/

    /** constant SYMMETRIC_KEY_OPEN */
    public static final String SYMMETRIC_KEY_OPEN = "OPEN SYMMETRIC KEY [MPLPortalDBSymKey] "
            + "DECRYPTION BY CERTIFICATE [MPLPortalCertificate];";
    /** constant SYMMETRIC_KEY_CLOSE */
    public static final String SYMMETRIC_KEY_CLOSE = "CLOSE SYMMETRIC KEY [MPLPortalDBSymKey];";

    /******************* Personal Detail Updates Constants *******************/

    /** constant ENCASHMENT */
    public static final String ENCASHMENT = "encashment";
    /** constant USERID */
    public static final String USERID = "userid";
    /** constant EMAIL */
    public static final String EMAIL = "email";
    /** constant NINO */
    public static final String NINO = "nino";
    /** constant PHONE */
    public static final String PHONE = "phone";
    /** constant ADDRESS */
    public static final String ADDRESS = "address";

    /** constant UK */
    public static final String UNITED_KINGDOM = "UK";
    /** constant MOBILE */
    public static final String MOBILE = "Mobile";

}
