package uk.co.phoenixlife.service.dto;

public class AccountActivationLoginFormDTO {

    private String username;
    private String password;
    private String securityAnswer;
    private String policyNumber;
    private String title;
    private String firstName;
    private String lastname;
    private String day;
    private String month;
    private String year;
    private String phoneType;
    private String phoneNumber;
    private String postCode;
    private String questionId;
    private int countOfQuestions;
    private String secQues;

    /**
     * Gets the questionId
     * 
     * @return the questionId
     */
    public String getQuestionId() {
        return questionId;
    }

    /**
     * Sets the questionId
     * 
     * @param questionId
     *            the questionId to set
     */
    public void setQuestionId(final String questionId) {
        this.questionId = questionId;
    }

    /**
     * Gets the countOfQuestions
     * 
     * @return the countOfQuestions
     */
    public int getCountOfQuestions() {
        return countOfQuestions;
    }

    /**
     * Sets the countOfQuestions
     * 
     * @param countOfQuestions
     *            the countOfQuestions to set
     */
    public void setCountOfQuestions(final int countOfQuestions) {
        this.countOfQuestions = countOfQuestions;
    }

    /**
     * Gets the secQues
     * 
     * @return the secQues
     */
    public String getSecQues() {
        return secQues;
    }

    /**
     * Sets the secQues
     * 
     * @param secQues
     *            the secQues to set
     */
    public void setSecQues(final String secQues) {
        this.secQues = secQues;
    }

    /**
     * Gets the username
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username
     *
     * @param username
     *            the username to set
     */
    public void setUsername(final String username) {
        this.username = username;
    }

    /**
     * Gets the password
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password
     *
     * @param password
     *            the password to set
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * Gets the securityAnswer
     *
     * @return the securityAnswer
     */
    public String getSecurityAnswer() {
        return securityAnswer;
    }

    /**
     * Sets the securityAnswer
     *
     * @param securityAnswer
     *            the securityAnswer to set
     */
    public void setSecurityAnswer(final String securityAnswer) {
        this.securityAnswer = securityAnswer;
    }

    /**
     * Gets the policyNumber
     *
     * @return the policyNumber
     */
    public String getPolicyNumber() {
        return policyNumber;
    }

    /**
     * Sets the policyNumber
     *
     * @param policyNumber
     *            the policyNumber to set
     */
    public void setPolicyNumber(final String policyNumber) {
        this.policyNumber = policyNumber;
    }

    /**
     * Gets the title
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title
     *
     * @param title
     *            the title to set
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * Gets the firstName
     *
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the firstName
     *
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the lastname
     *
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * Sets the lastname
     *
     * @param lastname
     *            the lastname to set
     */
    public void setLastname(final String lastname) {
        this.lastname = lastname;
    }

    /**
     * Gets the day
     *
     * @return the day
     */
    public String getDay() {
        return day;
    }

    /**
     * Sets the day
     *
     * @param day
     *            the day to set
     */
    public void setDay(final String day) {
        this.day = day;
    }

    /**
     * Gets the month
     *
     * @return the month
     */
    public String getMonth() {
        return month;
    }

    /**
     * Sets the month
     *
     * @param month
     *            the month to set
     */
    public void setMonth(final String month) {
        this.month = month;
    }

    /**
     * Gets the year
     *
     * @return the year
     */
    public String getYear() {
        return year;
    }

    /**
     * Sets the year
     *
     * @param year
     *            the year to set
     */
    public void setYear(final String year) {
        this.year = year;
    }

    /**
     * Gets the phoneType
     *
     * @return the phoneType
     */
    public String getPhoneType() {
        return phoneType;
    }

    /**
     * Sets the phoneType
     *
     * @param phoneType
     *            the phoneType to set
     */
    public void setPhoneType(final String phoneType) {
        this.phoneType = phoneType;
    }

    /**
     * Gets the phoneNumber
     *
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Sets the phoneNumber
     *
     * @param phoneNumber
     *            the phoneNumber to set
     */
    public void setPhoneNumber(final String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Gets the postCode
     *
     * @return the postCode
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * Sets the postCode
     *
     * @param postCode
     *            the postCode to set
     */
    public void setPostCode(final String postCode) {
        this.postCode = postCode;
    }

}
