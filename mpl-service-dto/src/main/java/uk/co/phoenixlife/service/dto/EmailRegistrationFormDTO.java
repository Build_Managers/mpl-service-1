package uk.co.phoenixlife.service.dto;

public class EmailRegistrationFormDTO {

    private String email;
    private String confirmEmail;

    /**
     * Gets the email
     * 
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email
     * 
     * @param email
     *            the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * Gets the confirmEmail
     * 
     * @return the confirmEmail
     */
    public String getConfirmEmail() {
        return confirmEmail;
    }

    /**
     * Sets the confirmEmail
     * 
     * @param confirmEmail
     *            the confirmEmail to set
     */
    public void setConfirmEmail(final String confirmEmail) {
        this.confirmEmail = confirmEmail;
    }

}
