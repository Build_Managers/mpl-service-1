/*
 * ErrorListDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * ErrorListDTO.java
 */
public class ErrorListDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String pageId;
    private String nextPageId;
    private String messageId;
    private int messageCount;
    private int timeSpent;
    private String email;
    private String policyNumber;
    private String username;

    public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	/**
     * Gets the pageId
     *
     * @return the pageId
     */
    public String getPageId() {
        return pageId;
    }

    /**
     * Sets the pageId
     *
     * @param pPageId
     *            the pageId to set
     */
    public void setPageId(final String pPageId) {
        pageId = pPageId;
    }

    /**
     * Gets the nextPageId
     *
     * @return the nextPageId
     */
    public String getNextPageId() {
        return nextPageId;
    }

    /**
     * Sets the nextPageId
     *
     * @param pNextPageId
     *            the nextPageId to set
     */
    public void setNextPageId(final String pNextPageId) {
        nextPageId = pNextPageId;
    }

    /**
     * Gets the messageId
     *
     * @return the messageId
     */
    public String getMessageId() {
        return messageId;
    }

    /**
     * Sets the messageId
     *
     * @param pMessageId
     *            the messageId to set
     */
    public void setMessageId(final String pMessageId) {
        messageId = pMessageId;
    }

    /**
     * Gets the messageCount
     *
     * @return the messageCount
     */
    public int getMessageCount() {
        return messageCount;
    }

    /**
     * Sets the messageCount
     *
     * @param pMessageCount
     *            the messageCount to set
     */
    public void setMessageCount(final int pMessageCount) {
        messageCount = pMessageCount;
    }

    /**
     * Gets the timeSpent
     *
     * @return the timeSpent
     */
    public int getTimeSpent() {
        return timeSpent;
    }

    /**
     * Sets the timeSpent
     *
     * @param pTimeSpent
     *            the timeSpent to set
     */
    public void setTimeSpent(final int pTimeSpent) {
        timeSpent = pTimeSpent;
    }

    /**
     * Gets the email
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email
     *
     * @param pEmail
     *            the email to set
     */
    public void setEmail(final String pEmail) {
        email = pEmail;
    }

    /**
     * Gets the policyNumber
     *
     * @return the policyNumber
     */
    public String getPolicyNumber() {
        return policyNumber;
    }

    /**
     * Sets the policyNumber
     *
     * @param pPolicyNumber
     *            the policyNumber to set
     */
    public void setPolicyNumber(final String pPolicyNumber) {
        policyNumber = pPolicyNumber;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
