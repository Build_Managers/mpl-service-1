package uk.co.phoenixlife.service.dto;

public class LoginSecurityQuestionDTO {

    private String answer;
    private String secQues;
    private String questionId;
    private int countOfQuestions;

    /**
     * Gets the secQues
     * 
     * @return the secQues
     */
    public String getSecQues() {
        return secQues;
    }

    /**
     * Sets the secQues
     * 
     * @param secQues
     *            the secQues to set
     */
    public void setSecQues(final String secQues) {
        this.secQues = secQues;
    }

    /**
     * Gets the questionId
     * 
     * @return the questionId
     */
    public String getQuestionId() {
        return questionId;
    }

    /**
     * Sets the questionId
     * 
     * @param questionId
     *            the questionId to set
     */
    public void setQuestionId(final String questionId) {
        this.questionId = questionId;
    }

    /**
     * Gets the countOfQuestions
     * 
     * @return the countOfQuestions
     */
    public int getCountOfQuestions() {
        return countOfQuestions;
    }

    /**
     * Sets the countOfQuestions
     * 
     * @param countOfQuestions
     *            the countOfQuestions to set
     */
    public void setCountOfQuestions(final int countOfQuestions) {
        this.countOfQuestions = countOfQuestions;
    }

    /**
     * Gets the answer
     * 
     * @return the answer
     */
    public String getAnswer() {
        return answer;
    }

    /**
     * Sets the answer
     * 
     * @param answer
     *            the answer to set
     */
    public void setAnswer(final String answer) {
        this.answer = answer;
    }
}
