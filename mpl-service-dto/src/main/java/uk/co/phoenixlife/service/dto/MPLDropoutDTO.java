package uk.co.phoenixlife.service.dto;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * MPLDropoutDTO.java
 */
public class MPLDropoutDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String sessionId;
    private String emailAddress;
    private String pageId;
    private String nextPageId;
    private int timeSpent;
    private Date dropOutDate;
    private Timestamp createdOn;

    /**
     * Gets the sessionId
     *
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Sets the sessionId
     *
     * @param pSessionId
     *            the sessionId to set
     */
    public void setSessionId(final String pSessionId) {
        sessionId = pSessionId;
    }

    /**
     * Gets the emailAddress
     *
     * @return the emailAddress
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the emailAddress
     *
     * @param pEmailAddress
     *            the emailAddress to set
     */
    public void setEmailAddress(final String pEmailAddress) {
        emailAddress = pEmailAddress;
    }

    /**
     * Gets the pageId
     *
     * @return the pageId
     */
    public String getPageId() {
        return pageId;
    }

    /**
     * Sets the pageId
     *
     * @param pPageId
     *            the pageId to set
     */
    public void setPageId(final String pPageId) {
        pageId = pPageId;
    }

    /**
     * Gets the nextPageId
     *
     * @return the nextPageId
     */
    public String getNextPageId() {
        return nextPageId;
    }

    /**
     * Sets the nextPageId
     *
     * @param pNextPageId
     *            the nextPageId to set
     */
    public void setNextPageId(final String pNextPageId) {
        nextPageId = pNextPageId;
    }

    /**
     * Gets the timeSpent
     *
     * @return the timeSpent
     */
    public int getTimeSpent() {
        return timeSpent;
    }

    /**
     * Sets the timeSpent
     *
     * @param pTimeSpent
     *            the timeSpent to set
     */
    public void setTimeSpent(final int pTimeSpent) {
        timeSpent = pTimeSpent;
    }

    /**
     * Gets the dropOutDate
     *
     * @return the dropOutDate
     */
    public Date getDropOutDate() {
        return dropOutDate;
    }

    /**
     * Sets the dropOutDate
     *
     * @param pDropOutDate
     *            the dropOutDate to set
     */
    public void setDropOutDate(final Date pDropOutDate) {
        dropOutDate = pDropOutDate;
    }

    /**
     * Gets the createdOn
     *
     * @return the createdOn
     */
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    /**
     * Sets the createdOn
     *
     * @param pCreatedOn
     *            the createdOn to set
     */
    public void setCreatedOn(final Timestamp pCreatedOn) {
        createdOn = pCreatedOn;
    }

}
