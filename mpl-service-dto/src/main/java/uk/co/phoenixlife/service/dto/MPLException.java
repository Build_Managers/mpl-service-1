/*
 * MPLException.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto;

/**
 * MPLException.java
 */
public class MPLException extends Exception {

    /** long */
    private static final long serialVersionUID = -6041715042928561393L;

    private String message;

    /**
     * Default Constructor
     */
    public MPLException() {
        super();
    }

    /**
     * Constructor
     *
     * @param message
     *            - error message
     */
    public MPLException(final String message) {
        super(message);
        // this.message = message;
    }

    /**
     * Gets the message
     *
     * @return the message
     */
    @Override
    public String getMessage() {
        return message;
    }

    /**
     * Sets the message
     *
     * @param message
     *            the message to set
     */
    public void setMessage(final String message) {
        this.message = message;
    }

}
