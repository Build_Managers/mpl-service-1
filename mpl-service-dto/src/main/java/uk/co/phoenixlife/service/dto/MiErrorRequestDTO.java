package uk.co.phoenixlife.service.dto;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import uk.co.phoenixlife.service.dto.MPLDropoutDTO;
import uk.co.phoenixlife.service.dto.MiErrorRequestDTO;
import uk.co.phoenixlife.service.dto.compliance.ComplianceDTO;

/**
 * MiErrorRequestDTO.java
 */
public class MiErrorRequestDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private List<ErrorListDTO> errorListDTO;
    private MPLDropoutDTO dropOutDTO;
    private List<ComplianceDTO> list;
    /**
     * Gets the errorListDTO
     *
     * @return the errorListDTO
     */
    public List<ErrorListDTO> getErrorListDTO() {
        return errorListDTO;
    }

    public List<ComplianceDTO> getList() {
		return list;
	}

	public void setList(List<ComplianceDTO> list) {
		this.list = list;
	}

	/**
     * Sets the errorListDTO
     *
     * @param list
     *            the errorListDTO to set
     */
    public void setErrorListDTO(final List<ErrorListDTO> list) {
        errorListDTO = list;
    }

    /**
     * Gets the dropOutDTO
     *
     * @return the dropOutDTO
     */
    public MPLDropoutDTO getDropOutDTO() {
        return dropOutDTO;
    }

    /**
     * Sets the dropOutDTO
     *
     * @param pDropOutDTO
     *            the dropOutDTO to set
     */
    public void setDropOutDTO(final MPLDropoutDTO pDropOutDTO) {
        dropOutDTO = pDropOutDTO;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
