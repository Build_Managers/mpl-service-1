package uk.co.phoenixlife.service.dto;

public class PolicyHolderPersonalDetailsFormDTO {

    private String email;

    private String fullName;
    private String dateOfBirth;
    private String niNo;
    private String policyHolderAddress;

    private String oldPassword;
    private String newPassword;
    private String confirmNewPassword;
    private String primaryPhone;

    /**
     * Gets the email
     * 
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email
     * 
     * @param email
     *            the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * Gets the fullName
     * 
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * Sets the fullName
     * 
     * @param fullName
     *            the fullName to set
     */
    public void setFullName(final String fullName) {
        this.fullName = fullName;
    }

    /**
     * Gets the dateOfBirth
     * 
     * @return the dateOfBirth
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the dateOfBirth
     * 
     * @param dateOfBirth
     *            the dateOfBirth to set
     */
    public void setDateOfBirth(final String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * Gets the niNo
     * 
     * @return the niNo
     */
    public String getNiNo() {
        return niNo;
    }

    /**
     * Sets the niNo
     * 
     * @param niNo
     *            the niNo to set
     */
    public void setNiNo(final String niNo) {
        this.niNo = niNo;
    }

    /**
     * Gets the policyHolderAddress
     * 
     * @return the policyHolderAddress
     */
    public String getPolicyHolderAddress() {
        return policyHolderAddress;
    }

    /**
     * Sets the policyHolderAddress
     * 
     * @param policyHolderAddress
     *            the policyHolderAddress to set
     */
    public void setPolicyHolderAddress(final String policyHolderAddress) {
        this.policyHolderAddress = policyHolderAddress;
    }

    /**
     * Gets the oldPassword
     * 
     * @return the oldPassword
     */
    public String getOldPassword() {
        return oldPassword;
    }

    /**
     * Sets the oldPassword
     * 
     * @param oldPassword
     *            the oldPassword to set
     */
    public void setOldPassword(final String oldPassword) {
        this.oldPassword = oldPassword;
    }

    /**
     * Gets the newPassword
     * 
     * @return the newPassword
     */
    public String getNewPassword() {
        return newPassword;
    }

    /**
     * Sets the newPassword
     * 
     * @param newPassword
     *            the newPassword to set
     */
    public void setNewPassword(final String newPassword) {
        this.newPassword = newPassword;
    }

    /**
     * Gets the confirmNewPassword
     * 
     * @return the confirmNewPassword
     */
    public String getConfirmNewPassword() {
        return confirmNewPassword;
    }

    /**
     * Sets the confirmNewPassword
     * 
     * @param confirmNewPassword
     *            the confirmNewPassword to set
     */
    public void setConfirmNewPassword(final String confirmNewPassword) {
        this.confirmNewPassword = confirmNewPassword;
    }

    /**
     * Gets the primaryPhone
     * 
     * @return the primaryPhone
     */
    public String getPrimaryPhone() {
        return primaryPhone;
    }

    /**
     * Sets the primaryPhone
     * 
     * @param primaryPhone
     *            the primaryPhone to set
     */
    public void setPrimaryPhone(final String primaryPhone) {
        this.primaryPhone = primaryPhone;
    }

}
