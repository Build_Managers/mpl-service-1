package uk.co.phoenixlife.service.dto;

import java.util.Map;

public class SecurityQuestionDTO {

	private Map<String,String> securityQuestionsMap;
	

	/**
	 * Gets the list Of security questions
	 * 
	 * @return the list Of security questions
	 */
	
	public Map<String,String> getSecurityQuestionsMap() {
		return securityQuestionsMap;
	}

	/**
	 * Sets the list Of security questions
	 * 
	 * @param securityQuestionsList 
	 *            the list Of security questions to set
	 */
	public void setSecurityQuestionsMap(Map<String,String> securityQuestionsMap) {
		this.securityQuestionsMap = securityQuestionsMap;
	}

	
	

}
