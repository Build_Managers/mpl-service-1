/*
 * SecurityQuestionsDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto;

import java.io.Serializable;

/**
 * SecurityQuestionsDTO.java
 */
public class SecurityQuestionsForAUserDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String questionKey;
    private String question;
    private String answer;
   int countOfQuestions;
    
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getQuestionKey() {
		return questionKey;
	}
	public void setQuestionKey(String questionKey) {
		this.questionKey = questionKey;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public int getCountOfQuestions() {
		return countOfQuestions;
	}
	public void setCountOfQuestions(int countOfQuestions) {
		this.countOfQuestions = countOfQuestions;
	}
	
  
}
