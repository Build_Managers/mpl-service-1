package uk.co.phoenixlife.service.dto;

public class SecurityRegistrationFormDTO {

    private String question1;
    private String question2;
    private String question3;
    private String question4;
    private String answer1;
    private String answer2;
    private String answer3;
    private String answer4;

    /**
     * Gets the question1
     * 
     * @return the question1
     */
    public String getQuestion1() {
        return question1;
    }

    /**
     * Sets the question1
     * 
     * @param question1
     *            the question1 to set
     */
    public void setQuestion1(final String question1) {
        this.question1 = question1;
    }

    /**
     * Gets the question2
     * 
     * @return the question2
     */
    public String getQuestion2() {
        return question2;
    }

    /**
     * Sets the question2
     * 
     * @param question2
     *            the question2 to set
     */
    public void setQuestion2(final String question2) {
        this.question2 = question2;
    }

    /**
     * Gets the question3
     * 
     * @return the question3
     */
    public String getQuestion3() {
        return question3;
    }

    /**
     * Sets the question3
     * 
     * @param question3
     *            the question3 to set
     */
    public void setQuestion3(final String question3) {
        this.question3 = question3;
    }

    /**
     * Gets the question4
     * 
     * @return the question4
     */
    public String getQuestion4() {
        return question4;
    }

    /**
     * Sets the question4
     * 
     * @param question4
     *            the question4 to set
     */
    public void setQuestion4(final String question4) {
        this.question4 = question4;
    }

    /**
     * Gets the answer1
     * 
     * @return the answer1
     */
    public String getAnswer1() {
        return answer1;
    }

    /**
     * Sets the answer1
     * 
     * @param answer1
     *            the answer1 to set
     */
    public void setAnswer1(final String answer1) {
        this.answer1 = answer1;
    }

    /**
     * Gets the answer2
     * 
     * @return the answer2
     */
    public String getAnswer2() {
        return answer2;
    }

    /**
     * Sets the answer2
     * 
     * @param answer2
     *            the answer2 to set
     */
    public void setAnswer2(final String answer2) {
        this.answer2 = answer2;
    }

    /**
     * Gets the answer3
     * 
     * @return the answer3
     */
    public String getAnswer3() {
        return answer3;
    }

    /**
     * Sets the answer3
     * 
     * @param answer3
     *            the answer3 to set
     */
    public void setAnswer3(final String answer3) {
        this.answer3 = answer3;
    }

    /**
     * Gets the answer4
     * 
     * @return the answer4
     */
    public String getAnswer4() {
        return answer4;
    }

    /**
     * Sets the answer4
     * 
     * @param answer4
     *            the answer4 to set
     */
    public void setAnswer4(final String answer4) {
        this.answer4 = answer4;
    }

}
