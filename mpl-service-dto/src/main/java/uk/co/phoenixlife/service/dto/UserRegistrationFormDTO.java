package uk.co.phoenixlife.service.dto;

public class UserRegistrationFormDTO {

    private String username;
    private String password;
    private String confirmPassword;
    private String email;
    private String questionOne;
    private String questionTwo;
    private String questionThree;
    private String questionFour;
    private String answerOne;
    private String answerTwo;
    private String answerThree;
    private String answerFour;

    /**
     * Gets the username
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username
     *
     * @param username
     *            the username to set
     */
    public void setUsername(final String username) {
        this.username = username;
    }

    /**
     * Gets the password
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password
     *
     * @param password
     *            the password to set
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * Gets the confirmPassword
     *
     * @return the confirmPassword
     */
    public String getConfirmPassword() {
        return confirmPassword;
    }

    /**
     * Sets the confirmPassword
     *
     * @param confirmPassword
     *            the confirmPassword to set
     */
    public void setConfirmPassword(final String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    /**
     * Gets the email
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email
     *
     * @param email
     *            the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * Gets the questionOne
     *
     * @return the questionOne
     */
    public String getQuestionOne() {
        return questionOne;
    }

    /**
     * Sets the questionOne
     *
     * @param questionOne
     *            the questionOne to set
     */
    public void setQuestionOne(final String questionOne) {
        this.questionOne = questionOne;
    }

    /**
     * Gets the questionTwo
     *
     * @return the questionTwo
     */
    public String getQuestionTwo() {
        return questionTwo;
    }

    /**
     * Sets the questionTwo
     *
     * @param questionTwo
     *            the questionTwo to set
     */
    public void setQuestionTwo(final String questionTwo) {
        this.questionTwo = questionTwo;
    }

    /**
     * Gets the questionThree
     *
     * @return the questionThree
     */
    public String getQuestionThree() {
        return questionThree;
    }

    /**
     * Sets the questionThree
     *
     * @param questionThree
     *            the questionThree to set
     */
    public void setQuestionThree(final String questionThree) {
        this.questionThree = questionThree;
    }

    /**
     * Gets the questionFour
     *
     * @return the questionFour
     */
    public String getQuestionFour() {
        return questionFour;
    }

    /**
     * Sets the questionFour
     *
     * @param questionFour
     *            the questionFour to set
     */
    public void setQuestionFour(final String questionFour) {
        this.questionFour = questionFour;
    }

    /**
     * Gets the answerOne
     *
     * @return the answerOne
     */
    public String getAnswerOne() {
        return answerOne;
    }

    /**
     * Sets the answerOne
     *
     * @param answerOne
     *            the answerOne to set
     */
    public void setAnswerOne(final String answerOne) {
        this.answerOne = answerOne;
    }

    /**
     * Gets the answerTwo
     *
     * @return the answerTwo
     */
    public String getAnswerTwo() {
        return answerTwo;
    }

    /**
     * Sets the answerTwo
     *
     * @param answerTwo
     *            the answerTwo to set
     */
    public void setAnswerTwo(final String answerTwo) {
        this.answerTwo = answerTwo;
    }

    /**
     * Gets the answerThree
     *
     * @return the answerThree
     */
    public String getAnswerThree() {
        return answerThree;
    }

    /**
     * Sets the answerThree
     *
     * @param answerThree
     *            the answerThree to set
     */
    public void setAnswerThree(final String answerThree) {
        this.answerThree = answerThree;
    }

    /**
     * Gets the answerFour
     *
     * @return the answerFour
     */
    public String getAnswerFour() {
        return answerFour;
    }

    /**
     * Sets the answerFour
     *
     * @param answerFour
     *            the answerFour to set
     */
    public void setAnswerFour(final String answerFour) {
        this.answerFour = answerFour;
    }

}
