/*
 * CustomerActivationDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.activation;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import uk.co.phoenixlife.security.service.dto.ForgotPasswordDto;
import uk.co.phoenixlife.service.dto.staticdata.StaticIDVDataDTO;

/**
 * CustomerActivationDTO.java
 */
public class CustomerActivationDTO implements Serializable {
    /** long */
    private static final long serialVersionUID = 1L;
    private Boolean accountLocked;
    private String title;
    private String firstName;
    private String surName;
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate dateOfBirth;
    private String policyNumber;
    private String userName;
    private String identifiedBancsUniqueCustomerNumber;
    private String phoneType;
    private String phoneNumber;
    private List<StaticIDVDataDTO> staticDataList;
    private Long customerId;
    private Long policyId;
    private Integer lockedCount;
    private ConcurrentHashMap<String, ConcurrentHashMap<String, String>> checkAnswerMap;
    private Boolean accountActivated;
    private Boolean policyLocked;
    private Boolean exhaustFlag;
    private String email;
    private ForgotPasswordDto forgotPasswordDto;
    private String legacyCustNo;

    /**
     * Gets the legacyCustNo
     * 
     * @return the legacyCustNo
     */
    public String getLegacyCustNo() {
        return legacyCustNo;
    }

    /**
     * Sets the legacyCustNo
     * 
     * @param legacyCustNo
     *            the legacyCustNo to set
     */
    public void setLegacyCustNo(final String legacyCustNo) {
        this.legacyCustNo = legacyCustNo;
    }

    /**
     * Gets the forgotPasswordDto
     *
     * @return the forgotPasswordDto
     */
    public ForgotPasswordDto getForgotPasswordDto() {
        return forgotPasswordDto;
    }

    /**
     * Sets the forgotPasswordDto
     *
     * @param forgotPasswordDto
     *            the forgotPasswordDto to set
     */
    public void setForgotPasswordDto(final ForgotPasswordDto forgotPasswordDto) {
        this.forgotPasswordDto = forgotPasswordDto;
    }

    /**
     * Gets the email
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email
     *
     * @param email
     *            the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * Gets the accountLocked
     *
     * @return the accountLocked
     */
    public Boolean getAccountLocked() {
        return accountLocked;
    }

    /**
     * Sets the accountLocked
     *
     * @param accountLocked
     *            the accountLocked to set
     */
    public void setAccountLocked(final Boolean accountLocked) {
        this.accountLocked = accountLocked;
    }

    /**
     * Gets the policyNumber
     *
     * @return the policyNumber
     */
    public String getPolicyNumber() {
        return policyNumber;
    }

    /**
     * Sets the policyNumber
     *
     * @param policyNumber
     *            the policyNumber to set
     */
    public void setPolicyNumber(final String policyNumber) {
        this.policyNumber = policyNumber;
    }

    /**
     * Gets the title
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title
     *
     * @param title
     *            the title to set
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * Gets the firstName
     *
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the firstName
     *
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the surName
     *
     * @return the surName
     */
    public String getSurName() {
        return surName;
    }

    /**
     * Sets the surName
     *
     * @param surName
     *            the surName to set
     */
    public void setSurName(final String surName) {
        this.surName = surName;
    }

    /**
     * Gets the dateOfBirth
     *
     * @return the dateOfBirth
     */
    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the dateOfBirth
     *
     * @param dateOfBirth
     *            the dateOfBirth to set
     */
    public void setDateOfBirth(final LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * Gets the userName
     *
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the userName
     *
     * @param userName
     *            the userName to set
     */
    public void setUserName(final String userName) {
        this.userName = userName;
    }

    /**
     * Gets the identifiedBancsUniqueCustomerNumber
     *
     * @return the identifiedBancsUniqueCustomerNumber
     */
    public String getIdentifiedBancsUniqueCustomerNumber() {
        return identifiedBancsUniqueCustomerNumber;
    }

    /**
     * Sets the identifiedBancsUniqueCustomerNumber
     *
     * @param identifiedBancsUniqueCustomerNumber
     *            the identifiedBancsUniqueCustomerNumber to set
     */
    public void setIdentifiedBancsUniqueCustomerNumber(final String identifiedBancsUniqueCustomerNumber) {
        this.identifiedBancsUniqueCustomerNumber = identifiedBancsUniqueCustomerNumber;
    }

    /**
     * Gets the phoneType
     *
     * @return the phoneType
     */
    public String getPhoneType() {
        return phoneType;
    }

    /**
     * Sets the phoneType
     *
     * @param phoneType
     *            the phoneType to set
     */
    public void setPhoneType(final String phoneType) {
        this.phoneType = phoneType;
    }

    /**
     * Gets the phoneNumber
     *
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Sets the phoneNumber
     *
     * @param phoneNumber
     *            the phoneNumber to set
     */
    public void setPhoneNumber(final String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Gets the staticDataList
     *
     * @return the staticDataList
     */
    public List<StaticIDVDataDTO> getStaticDataList() {
        return staticDataList;
    }

    /**
     * Sets the staticDataList
     *
     * @param staticDataList
     *            the staticDataList to set
     */
    public void setStaticDataList(final List<StaticIDVDataDTO> staticDataList) {
        this.staticDataList = staticDataList;
    }

    /**
     * Gets the customerId
     *
     * @return the customerId
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * Sets the customerId
     *
     * @param customerId
     *            the customerId to set
     */
    public void setCustomerId(final Long customerId) {
        this.customerId = customerId;
    }

    /**
     * Gets the policyId
     *
     * @return the policyId
     */
    public Long getPolicyId() {
        return policyId;
    }

    /**
     * Sets the policyId
     *
     * @param policyId
     *            the policyId to set
     */
    public void setPolicyId(final Long policyId) {
        this.policyId = policyId;
    }

    /**
     * Gets the lockedCount
     *
     * @return the lockedCount
     */
    public Integer getLockedCount() {
        return lockedCount;
    }

    /**
     * Sets the lockedCount
     *
     * @param lockedCount
     *            the lockedCount to set
     */
    public void setLockedCount(final Integer lockedCount) {
        this.lockedCount = lockedCount;
    }

    /**
     * Gets the checkAnswerMap
     *
     * @return the checkAnswerMap
     */
    public ConcurrentHashMap<String, ConcurrentHashMap<String, String>> getCheckAnswerMap() {
        return checkAnswerMap;
    }

    /**
     * Sets the checkAnswerMap
     *
     * @param checkAnswerMap
     *            the checkAnswerMap to set
     */
    public void setCheckAnswerMap(final ConcurrentHashMap<String, ConcurrentHashMap<String, String>> checkAnswerMap) {
        this.checkAnswerMap = checkAnswerMap;
    }

    /**
     * Gets the accountActivated
     *
     * @return the accountActivated
     */
    public Boolean getAccountActivated() {
        return accountActivated;
    }

    /**
     * Sets the accountActivated
     *
     * @param accountActivated
     *            the accountActivated to set
     */
    public void setAccountActivated(final Boolean accountActivated) {
        this.accountActivated = accountActivated;
    }

    /**
     * Gets the policyLocked
     *
     * @return the policyLocked
     */
    public Boolean getPolicyLocked() {
        return policyLocked;
    }

    /**
     * Sets the policyLocked
     *
     * @param policyLocked
     *            the policyLocked to set
     */
    public void setPolicyLocked(final Boolean policyLocked) {
        this.policyLocked = policyLocked;
    }

    /**
     * Gets the exhaustFlag
     *
     * @return the exhaustFlag
     */
    public Boolean getExhaustFlag() {
        return exhaustFlag;
    }

    /**
     * Sets the exhaustFlag
     *
     * @param exhaustFlag
     *            the exhaustFlag to set
     */
    public void setExhaustFlag(final Boolean exhaustFlag) {
        this.exhaustFlag = exhaustFlag;
    }

}
