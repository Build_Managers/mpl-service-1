package uk.co.phoenixlife.service.dto.activation;

import java.util.List;

public class CustomerDetails {

    private String postCode;
    private List<CustomerPersonalDetailStatus> customerPersonalDetailStatusList;
    private Boolean postCodeFlag;
    private String identifiedBancsUniqueCustomerNumber;
    private String policyNumber;
    private Long customerId;
    private Long policyId;
    private Boolean userNameAlreadyExistFlag;
    private String legacyCustNo;

    /**
     * Gets the legacyCustNo
     * 
     * @return the legacyCustNo
     */
    public String getLegacyCustNo() {
        return legacyCustNo;
    }

    /**
     * Sets the legacyCustNo
     * 
     * @param legacyCustNo
     *            the legacyCustNo to set
     */
    public void setLegacyCustNo(final String legacyCustNo) {
        this.legacyCustNo = legacyCustNo;
    }

    /**
     * Gets the userNameAlreadyExistFlag
     *
     * @return the userNameAlreadyExistFlag
     */
    public Boolean getUserNameAlreadyExistFlag() {
        return userNameAlreadyExistFlag;
    }

    /**
     * Sets the userNameAlreadyExistFlag
     *
     * @param userNameAlreadyExistFlag
     *            the userNameAlreadyExistFlag to set
     */
    public void setUserNameAlreadyExistFlag(final Boolean userNameAlreadyExistFlag) {
        this.userNameAlreadyExistFlag = userNameAlreadyExistFlag;
    }

    /**
     * Gets the postCode
     *
     * @return the postCode
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * Sets the postCode
     *
     * @param postCode
     *            the postCode to set
     */
    public void setPostCode(final String postCode) {
        this.postCode = postCode;
    }

    /**
     * Gets the customerPersonalDetailStatusList
     *
     * @return the customerPersonalDetailStatusList
     */
    public List<CustomerPersonalDetailStatus> getCustomerPersonalDetailStatusList() {
        return customerPersonalDetailStatusList;
    }

    /**
     * Sets the customerPersonalDetailStatusList
     *
     * @param customerPersonalDetailStatusList
     *            the customerPersonalDetailStatusList to set
     */
    public void
           setCustomerPersonalDetailStatusList(final List<CustomerPersonalDetailStatus> customerPersonalDetailStatusList) {
        this.customerPersonalDetailStatusList = customerPersonalDetailStatusList;
    }

    /**
     * Gets the postCodeFlag
     *
     * @return the postCodeFlag
     */
    public Boolean getPostCodeFlag() {
        return postCodeFlag;
    }

    /**
     * Sets the postCodeFlag
     *
     * @param postCodeFlag
     *            the postCodeFlag to set
     */
    public void setPostCodeFlag(final Boolean postCodeFlag) {
        this.postCodeFlag = postCodeFlag;
    }

    /**
     * Gets the identifiedBancsUniqueCustomerNumber
     *
     * @return the identifiedBancsUniqueCustomerNumber
     */
    public String getIdentifiedBancsUniqueCustomerNumber() {
        return identifiedBancsUniqueCustomerNumber;
    }

    /**
     * Sets the identifiedBancsUniqueCustomerNumber
     *
     * @param identifiedBancsUniqueCustomerNumber
     *            the identifiedBancsUniqueCustomerNumber to set
     */
    public void setIdentifiedBancsUniqueCustomerNumber(final String identifiedBancsUniqueCustomerNumber) {
        this.identifiedBancsUniqueCustomerNumber = identifiedBancsUniqueCustomerNumber;
    }

    /**
     * Gets the policyNumber
     *
     * @return the policyNumber
     */
    public String getPolicyNumber() {
        return policyNumber;
    }

    /**
     * Sets the policyNumber
     *
     * @param policyNumber
     *            the policyNumber to set
     */
    public void setPolicyNumber(final String policyNumber) {
        this.policyNumber = policyNumber;
    }

    /**
     * Gets the customerId
     *
     * @return the customerId
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * Sets the customerId
     *
     * @param customerId
     *            the customerId to set
     */
    public void setCustomerId(final Long customerId) {
        this.customerId = customerId;
    }

    /**
     * Gets the policyId
     *
     * @return the policyId
     */
    public Long getPolicyId() {
        return policyId;
    }

    /**
     * Sets the policyId
     *
     * @param policyId
     *            the policyId to set
     */
    public void setPolicyId(final Long policyId) {
        this.policyId = policyId;
    }

}
