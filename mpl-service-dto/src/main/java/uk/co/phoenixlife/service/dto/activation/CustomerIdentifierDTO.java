/*
 * CustomerDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.activation;

import java.io.Serializable;
import java.util.List;

/**
 * CustomerDTO.java
 */
public class CustomerIdentifierDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 6563946167227280118L;

    private String userName;
    private String polNum;
    private String bancsUniqueCustNo;
    private List<String> listOfBancsPolNum;
    private String emailId;
    private String xGUID;
    private String firstName;
    private String lastName;
    private String title;

    /**
     * Gets the userName
     * 
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the userName
     * 
     * @param userName
     *            the userName to set
     */
    public void setUserName(final String userName) {
        this.userName = userName;
    }

    /**
     * Gets the emailId
     *
     * @return the emailId
     */
    public String getEmailId() {
        return emailId;
    }

    /**
     * Sets the emailId
     *
     * @param emailId
     *            the emailId to set
     */
    public void setEmailId(final String emailId) {
        this.emailId = emailId;
    }

    /**
     * Gets the xGUID
     *
     * @return the xGUID
     */
    public String getxGUID() {
        return xGUID;
    }

    /**
     * Sets the xGUID
     *
     * @param xGUID
     *            the xGUID to set
     */
    public void setxGUID(final String xGUID) {
        this.xGUID = xGUID;
    }

    /**
     * Gets the listOfBancsPolNum
     *
     * @return the listOfBancsPolNum
     */
    public List<String> getListOfBancsPolNum() {
        return listOfBancsPolNum;
    }

    /**
     * Sets the listOfBancsPolNum
     *
     * @param listOfBancsPolNum
     *            the listOfBancsPolNum to set
     */
    public void setListOfBancsPolNum(final List<String> listOfBancsPolNum) {
        this.listOfBancsPolNum = listOfBancsPolNum;
    }

    /**
     * Gets the polNum
     *
     * @return the polNum
     */
    public String getPolNum() {
        return polNum;
    }

    /**
     * Sets the polNum
     *
     * @param polNum
     *            the polNum to set
     */
    public void setPolNum(final String polNum) {
        this.polNum = polNum;
    }

    /**
     * Gets the bancsUniqueCustNo
     *
     * @return the bancsUniqueCustNo
     */
    public String getBancsUniqueCustNo() {
        return bancsUniqueCustNo;
    }

    /**
     * Sets the bancsUniqueCustNo
     *
     * @param bancsUniqueCustNo
     *            the bancsUniqueCustNo to set
     */
    public void setBancsUniqueCustNo(final String bancsUniqueCustNo) {
        this.bancsUniqueCustNo = bancsUniqueCustNo;
    }

    /**
     * Gets the firstName
     *
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the firstName
     *
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the lastName
     *
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the lastName
     *
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the title
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title
     *
     * @param title
     *            the title to set
     */
    public void setTitle(final String title) {
        this.title = title;
    }

}
