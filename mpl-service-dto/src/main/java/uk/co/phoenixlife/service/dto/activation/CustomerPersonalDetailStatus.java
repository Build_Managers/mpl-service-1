/*
 * CustomerPersonalDetailStatus.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.activation;

import java.io.Serializable;

/**
 * CustomerPersonalDetailStatus.java
 */
public class CustomerPersonalDetailStatus implements Serializable {
    private String bancsUniqueCustNo;
    private Boolean multipleOwnerFlag;
    private String myPhoenixUserName;
    private String legacyCustNo;

    /**
     * Default Constructor Constructor
     */
    public CustomerPersonalDetailStatus() {
        super();
    }

    public CustomerPersonalDetailStatus(final String bancsUniqueCustNo, final Boolean multipleOwnerFlag,
            final String myPhoenixUserName,
            final String legacyCustNo) {
        super();
        this.bancsUniqueCustNo = bancsUniqueCustNo;
        this.multipleOwnerFlag = multipleOwnerFlag;
        this.myPhoenixUserName = myPhoenixUserName;
        this.legacyCustNo = legacyCustNo;
    }

    /**
     * Gets the myPhoenixUserName
     *
     * @return the myPhoenixUserName
     */
    public String getMyPhoenixUserName() {
        return myPhoenixUserName;
    }

    /**
     * Sets the myPhoenixUserName
     *
     * @param myPhoenixUserName
     *            the myPhoenixUserName to set
     */
    public void setMyPhoenixUserName(final String myPhoenixUserName) {
        this.myPhoenixUserName = myPhoenixUserName;
    }

    /**
     * Gets the legacyCustNo
     * 
     * @return the legacyCustNo
     */
    public String getLegacyCustNo() {
        return legacyCustNo;
    }

    /**
     * Sets the legacyCustNo
     * 
     * @param legacyCustNo
     *            the legacyCustNo to set
     */
    public void setLegacyCustNo(final String legacyCustNo) {
        this.legacyCustNo = legacyCustNo;
    }

    /**
     * Gets the bancsUniqueCustNo
     *
     * @return the bancsUniqueCustNo
     */
    public String getBancsUniqueCustNo() {
        return bancsUniqueCustNo;
    }

    /**
     * Sets the bancsUniqueCustNo
     *
     * @param bancsUniqueCustNo
     *            the bancsUniqueCustNo to set
     */
    public void setBancsUniqueCustNo(final String bancsUniqueCustNo) {
        this.bancsUniqueCustNo = bancsUniqueCustNo;
    }

    /**
     * Gets the multipleOwnerFlag
     *
     * @return the multipleOwnerFlag
     */
    public Boolean getMultipleOwnerFlag() {
        return multipleOwnerFlag;
    }

    /**
     * Sets the multipleOwnerFlag
     *
     * @param multipleOwnerFlag
     *            the multipleOwnerFlag to set
     */
    public void setMultipleOwnerFlag(final Boolean multipleOwnerFlag) {
        this.multipleOwnerFlag = multipleOwnerFlag;
    }

}
