package uk.co.phoenixlife.service.dto.activation;

import java.util.List;

public class CustomerPersonalDetailStatusResponse {

    private List<CustomerPersonalDetailStatus> customerPersonalDetailStatus;
    private int errorCode;

    /**
     * Gets the customerPersonalDetailStatus
     * 
     * @return the customerPersonalDetailStatus
     */
    public List<CustomerPersonalDetailStatus> getCustomerPersonalDetailStatus() {
        return customerPersonalDetailStatus;
    }

    /**
     * Sets the customerPersonalDetailStatus
     * 
     * @param customerPersonalDetailStatus
     *            the customerPersonalDetailStatus to set
     */
    public void setCustomerPersonalDetailStatus(final List<CustomerPersonalDetailStatus> customerPersonalDetailStatus) {
        this.customerPersonalDetailStatus = customerPersonalDetailStatus;
    }

    /**
     * Gets the errorCode
     * 
     * @return the errorCode
     */
    public int getErrorCode() {
        return errorCode;
    }

    /**
     * Sets the errorCode
     * 
     * @param errorCode
     *            the errorCode to set
     */
    public void setErrorCode(final int errorCode) {
        this.errorCode = errorCode;
    }

}
