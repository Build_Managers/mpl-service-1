/**
 *
 */
package uk.co.phoenixlife.service.dto.activation;

import java.io.Serializable;

import uk.co.phoenixlife.service.dto.dashboard.Policy_Detail;
import uk.co.phoenixlife.service.dto.dashboard.Premium_Summaries;
import uk.co.phoenixlife.service.dto.dashboard.Valuations;

/**
 * Class to hold policydetails
 *
 * @author 1012746
 *
 */
public class CustomerPolicyDetail implements Serializable {
    private static final long serialVersionUID = 820552094553064852L;
    private Policy_Detail policyDetail;
    private Valuations valuations;
    private Premium_Summaries premiumSummaries;
    private String displayBenefitGuarantee;
    private Boolean cleanPolicy;

    /**
     * Gets the cleanPolicy
     *
     * @return the cleanPolicy
     */
    public Boolean getCleanPolicy() {
        return cleanPolicy;
    }

    /**
     * Sets the cleanPolicy
     *
     * @param cleanPolicy
     *            the cleanPolicy to set
     */
    public void setCleanPolicy(final Boolean cleanPolicy) {
        this.cleanPolicy = cleanPolicy;
    }

    /**
     * @return the displayBenefitGuarantee
     */
    public String getDisplayBenefitGuarantee() {
        return displayBenefitGuarantee;
    }

    /**
     * @param displayBenefitGuarantee
     *            the displayBenefitGuarantee to set
     */
    public void setDisplayBenefitGuarantee(final String displayBenefitGuarantee) {
        this.displayBenefitGuarantee = displayBenefitGuarantee;
    }

    /**
     * @return the policyDetail
     */
    public Policy_Detail getPolicyDetail() {
        return policyDetail;
    }

    /**
     * @param policyDetail
     *            the policyDetail to set
     */
    public void setPolicyDetail(final Policy_Detail policyDetail) {
        this.policyDetail = policyDetail;
    }

    /**
     * @return the valuations
     */
    public Valuations getValuations() {
        return valuations;
    }

    /**
     * @param valuations
     *            the valuations to set
     */
    public void setValuations(final Valuations valuations) {
        this.valuations = valuations;
    }

    /**
     * @return the premiumSummaries
     */
    public Premium_Summaries getPremiumSummaries() {
        return premiumSummaries;
    }

    /**
     * @param premiumSummaries
     *            the premiumSummaries to set
     */
    public void setPremiumSummaries(final Premium_Summaries premiumSummaries) {
        this.premiumSummaries = premiumSummaries;
    }

}
