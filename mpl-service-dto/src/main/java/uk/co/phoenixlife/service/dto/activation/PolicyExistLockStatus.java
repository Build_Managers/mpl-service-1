/*
 * PolicyExistLockStatus.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.activation;

/**
 * Policy Exist & lock status PolicyExistLockStatus.java
 */
public class PolicyExistLockStatus {
    private String policyNumber;
    private Integer flag;
    private String description;

    /**
     * Gets the policyNumber
     *
     * @return the policyNumber
     */
    public String getPolicyNumber() {
        return policyNumber;
    }

    /**
     * Sets the policyNumber
     *
     * @param policyNumber
     *            the policyNumber to set
     */
    public void setPolicyNumber(final String policyNumber) {
        this.policyNumber = policyNumber;
    }

    /**
     * Gets the flag
     *
     * @return the flag
     */
    public Integer getFlag() {
        return flag;
    }

    /**
     * Sets the flag
     *
     * @param flag
     *            the flag to set
     */
    public void setFlag(final Integer flag) {
        this.flag = flag;
    }

    /**
     * Gets the description
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description
     *
     * @param description
     *            the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

}
