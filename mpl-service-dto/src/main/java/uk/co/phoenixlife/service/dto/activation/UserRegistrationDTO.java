/*
 * UserRegistrationDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.activation;

import java.util.List;
import java.util.Set;

/**
 * UserRegistrationDTO.java
 *
 * @author 883555
 *
 */
public class UserRegistrationDTO {

    private String userName;
    private boolean userNameExistFlag;
    private Set<String> alreadyTakenUserNameSet;
    private List<String> suggestedUserNameList;

    /**
     * Gets the userName
     *
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the userName
     *
     * @param userName
     *            the userName to set
     */
    public void setUserName(final String userName) {
        this.userName = userName;
    }

    /**
     * Gets the alreadyTakenUserNameSet
     *
     * @return the alreadyTakenUserNameSet
     */
    public Set<String> getAlreadyTakenUserNameSet() {
        return alreadyTakenUserNameSet;
    }

    /**
     * Sets the alreadyTakenUserNameSet
     *
     * @param alreadyTakenUserNameSet
     *            the alreadyTakenUserNameSet to set
     */
    public void setAlreadyTakenUserNameSet(final Set<String> alreadyTakenUserNameSet) {
        this.alreadyTakenUserNameSet = alreadyTakenUserNameSet;
    }

    /**
     * Gets the suggestedUserNameList
     *
     * @return the suggestedUserNameList
     */
    public List<String> getSuggestedUserNameList() {
        return suggestedUserNameList;
    }

    /**
     * Sets the suggestedUserNameList
     *
     * @param suggestedUserNameList
     *            the suggestedUserNameList to set
     */
    public void setSuggestedUserNameList(final List<String> suggestedUserNameList) {
        this.suggestedUserNameList = suggestedUserNameList;
    }

    /**
     * Gets the userNameExistFlag
     * @return the userNameExistFlag
     */
    public boolean isUserNameExistFlag() {
        return userNameExistFlag;
    }

    /**
     * Sets the userNameExistFlag
     * @param userNameExistFlag the userNameExistFlag to set
     */
    public void setUserNameExistFlag(boolean userNameExistFlag) {
        this.userNameExistFlag = userNameExistFlag;
    }

}
