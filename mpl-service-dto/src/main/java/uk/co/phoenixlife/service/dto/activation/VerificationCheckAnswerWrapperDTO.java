package uk.co.phoenixlife.service.dto.activation;

public class VerificationCheckAnswerWrapperDTO {

    private CustomerActivationDTO customerActivationDTO;
    private VerificationDTO verificationDTO;

    /**
     * Gets the customerActivationDTO
     * 
     * @return the customerActivationDTO
     */
    public CustomerActivationDTO getCustomerActivationDTO() {
        return customerActivationDTO;
    }

    /**
     * Sets the customerActivationDTO
     * 
     * @param customerActivationDTO
     *            the customerActivationDTO to set
     */
    public void setCustomerActivationDTO(final CustomerActivationDTO customerActivationDTO) {
        this.customerActivationDTO = customerActivationDTO;
    }

    /**
     * Gets the verificationDTO
     * 
     * @return the verificationDTO
     */
    public VerificationDTO getVerificationDTO() {
        return verificationDTO;
    }

    /**
     * Sets the verificationDTO
     * 
     * @param verificationDTO
     *            the verificationDTO to set
     */
    public void setVerificationDTO(final VerificationDTO verificationDTO) {
        this.verificationDTO = verificationDTO;
    }
    
    public String toString(){
    	String returnValue = "";
    	if(this.getCustomerActivationDTO()!=null){
    		returnValue = "CustomerActivationDTO is not NULL";
    	}
    	else if(this.getVerificationDTO()!=null){
    		returnValue = returnValue + " VerificationDTO is not NULL";
    	}
    	else{
    		returnValue = returnValue+" if and else if not satisfied";
    	}
    		
    	return returnValue;
    }

}
