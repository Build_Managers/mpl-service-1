package uk.co.phoenixlife.service.dto.activation;

/**
 * VerificationDTO.java
 */
public class VerificationDTO {

    private String fundName;
    private String niNo;
    private String netinitPremium;
    private String benefitName;
    private String questionKey;
    private String question;
    private String questionLabel;
    private String answer;

    /**
     * Gets the fundName
     *
     * @return the fundName
     */
    public String getFundName() {
        return fundName;
    }

    /**
     * Sets the fundName
     *
     * @param pFundName
     *            the fundName to set
     */
    public void setFundName(final String pFundName) {
        fundName = pFundName;
    }

    /**
     * Gets the niNo
     *
     * @return the niNo
     */
    public String getNiNo() {
        return niNo;
    }

    /**
     * Sets the niNo
     *
     * @param pNiNo
     *            the niNo to set
     */
    public void setNiNo(final String pNiNo) {
        niNo = pNiNo;
    }

    /**
     * Gets the netinitPremium
     *
     * @return the netinitPremium
     */
    public String getNetinitPremium() {
        return netinitPremium;
    }

    /**
     * Sets the netinitPremium
     *
     * @param pNetinitPremium
     *            the netinitPremium to set
     */
    public void setNetinitPremium(final String pNetinitPremium) {
        netinitPremium = pNetinitPremium;
    }

    /**
     * Gets the benefitName
     *
     * @return the benefitName
     */
    public String getBenefitName() {
        return benefitName;
    }

    /**
     * Sets the benefitName
     *
     * @param pBenefitName
     *            the benefitName to set
     */
    public void setBenefitName(final String pBenefitName) {
        benefitName = pBenefitName;
    }

    /**
     * Gets the questionKey
     *
     * @return the questionKey
     */
    public String getQuestionKey() {
        return questionKey;
    }

    /**
     * Sets the questionKey
     *
     * @param pQuestionKey
     *            the questionKey to set
     */
    public void setQuestionKey(final String pQuestionKey) {
        questionKey = pQuestionKey;
    }

    /**
     * Gets the question
     *
     * @return the question
     */
    public String getQuestion() {
        return question;
    }

    /**
     * Sets the question
     *
     * @param pQuestion
     *            the question to set
     */
    public void setQuestion(final String pQuestion) {
        question = pQuestion;
    }

    /**
     * Gets the questionLabel
     *
     * @return the questionLabel
     */
    public String getQuestionLabel() {
        return questionLabel;
    }

    /**
     * Sets the questionLabel
     *
     * @param pQuestionLabel
     *            the questionLabel to set
     */
    public void setQuestionLabel(final String pQuestionLabel) {
        questionLabel = pQuestionLabel;
    }

    /**
     * Gets the answer
     *
     * @return the answer
     */
    public String getAnswer() {
        return answer;
    }

    /**
     * Sets the answer
     *
     * @param pAnswer
     *            the answer to set
     */
    public void setAnswer(final String pAnswer) {
        answer = pAnswer;
    }

}
