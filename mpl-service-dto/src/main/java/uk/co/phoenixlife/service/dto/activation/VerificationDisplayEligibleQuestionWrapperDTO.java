package uk.co.phoenixlife.service.dto.activation;

public class VerificationDisplayEligibleQuestionWrapperDTO {

    private CustomerActivationDTO customerActivationDTO;
    private Integer identifySource;
    private String questionKey;

    /**
     * Gets the identifySource
     *
     * @return the identifySource
     */
    public Integer getIdentifySource() {
        return identifySource;
    }

    /**
     * Sets the identifySource
     *
     * @param identifySource
     *            the identifySource to set
     */
    public void setIdentifySource(final Integer identifySource) {
        this.identifySource = identifySource;
    }

    /**
     * Gets the questionKey
     *
     * @return the questionKey
     */
    public String getQuestionKey() {
        return questionKey;
    }

    /**
     * Sets the questionKey
     *
     * @param questionKey
     *            the questionKey to set
     */
    public void setQuestionKey(final String questionKey) {
        this.questionKey = questionKey;
    }

    /**
     * Gets the customerActivationDTO
     *
     * @return the customerActivationDTO
     */
    public CustomerActivationDTO getCustomerActivationDTO() {
        return customerActivationDTO;
    }

    /**
     * Sets the customerActivationDTO
     * 
     * @param customerActivationDTO
     *            the customerActivationDTO to set
     */
    public void setCustomerActivationDTO(final CustomerActivationDTO customerActivationDTO) {
        this.customerActivationDTO = customerActivationDTO;
    }
}
