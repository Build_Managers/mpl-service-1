package uk.co.phoenixlife.service.dto.activation;

/**
 * VerificationFormDTO.java
 */
public class VerificationFormDTO {

    private String questionKey;
    private String question;
    private String answer1;
    private String answer;
    private String questionLabel;
    private String questionLabelForAccountNumber;

    /**
     * Gets the questionKey
     *
     * @return the questionKey
     */
    public String getQuestionKey() {
        return questionKey;
    }

    /**
     * Sets the questionKey
     *
     * @param pQuestionKey
     *            the questionKey to set
     */
    public void setQuestionKey(final String pQuestionKey) {
        questionKey = pQuestionKey;
    }

    /**
     * Gets the question
     *
     * @return the question
     */
    public String getQuestion() {
        return question;
    }

    /**
     * Sets the question
     *
     * @param pQuestion
     *            the question to set
     */
    public void setQuestion(final String pQuestion) {
        question = pQuestion;
    }

    /**
     * Gets the answer1
     *
     * @return the answer1
     */
    public String getAnswer1() {
        return answer1;
    }

    /**
     * Sets the answer1
     *
     * @param pAnswer1
     *            the answer1 to set
     */
    public void setAnswer1(final String pAnswer1) {
        answer1 = pAnswer1;
    }

    /**
     * Gets the answer
     *
     * @return the answer
     */
    public String getAnswer() {
        return answer;
    }

    /**
     * Sets the answer
     *
     * @param pAnswer
     *            the answer to set
     */
    public void setAnswer(final String pAnswer) {
        answer = pAnswer;
    }

    /**
     * Gets the questionLabel
     *
     * @return the questionLabel
     */
    public String getQuestionLabel() {
        return questionLabel;
    }

    /**
     * Sets the questionLabel
     *
     * @param pQuestionLabel
     *            the questionLabel to set
     */
    public void setQuestionLabel(final String pQuestionLabel) {
        questionLabel = pQuestionLabel;
    }

    /**
     * Gets the questionLabelForAccountNumber
     *
     * @return the questionLabelForAccountNumber
     */
    public String getQuestionLabelForAccountNumber() {
        return questionLabelForAccountNumber;
    }

    /**
     * Sets the questionLabelForAccountNumber
     *
     * @param pQuestionLabelForAccountNumber
     *            the questionLabelForAccountNumber to set
     */
    public void setQuestionLabelForAccountNumber(final String pQuestionLabelForAccountNumber) {
        questionLabelForAccountNumber = pQuestionLabelForAccountNumber;
    }

}
