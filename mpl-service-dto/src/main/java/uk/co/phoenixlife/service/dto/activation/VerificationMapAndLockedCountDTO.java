package uk.co.phoenixlife.service.dto.activation;

import java.util.concurrent.ConcurrentHashMap;

public class VerificationMapAndLockedCountDTO {

    private Integer lockedCount;
    private ConcurrentHashMap<String, ConcurrentHashMap<String, String>> checkAnswerMap;
    private String checkAnswerResponse;
    private Long customerId;
    private Long policyId;
    private Boolean accountActivated;

    /**
     * Gets the lockedCount
     *
     * @return the lockedCount
     */
    public Integer getLockedCount() {
        return lockedCount;
    }

    /**
     * Sets the lockedCount
     *
     * @param lockedCount
     *            the lockedCount to set
     */
    public void setLockedCount(final Integer lockedCount) {
        this.lockedCount = lockedCount;
    }

    /**
     * Gets the checkAnswerMap
     *
     * @return the checkAnswerMap
     */
    public ConcurrentHashMap<String, ConcurrentHashMap<String, String>> getCheckAnswerMap() {
        return checkAnswerMap;
    }

    /**
     * Sets the checkAnswerMap
     *
     * @param checkAnswerMap
     *            the checkAnswerMap to set
     */
    public void setCheckAnswerMap(final ConcurrentHashMap<String, ConcurrentHashMap<String, String>> checkAnswerMap) {
        this.checkAnswerMap = checkAnswerMap;
    }

    /**
     * Gets the checkAnswerResponse
     *
     * @return the checkAnswerResponse
     */
    public String getCheckAnswerResponse() {
        return checkAnswerResponse;
    }

    /**
     * Sets the checkAnswerResponse
     *
     * @param checkAnswerResponse
     *            the checkAnswerResponse to set
     */
    public void setCheckAnswerResponse(final String checkAnswerResponse) {
        this.checkAnswerResponse = checkAnswerResponse;
    }

    /**
     * Gets the customerId
     *
     * @return the customerId
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * Sets the customerId
     *
     * @param customerId
     *            the customerId to set
     */
    public void setCustomerId(final Long customerId) {
        this.customerId = customerId;
    }

    /**
     * Gets the policyId
     *
     * @return the policyId
     */
    public Long getPolicyId() {
        return policyId;
    }

    /**
     * Sets the policyId
     *
     * @param policyId
     *            the policyId to set
     */
    public void setPolicyId(final Long policyId) {
        this.policyId = policyId;
    }

    /**
     * Gets the accountActivated
     * 
     * @return the accountActivated
     */
    public Boolean getAccountActivated() {
        return accountActivated;
    }

    /**
     * Sets the accountActivated
     * 
     * @param accountActivated
     *            the accountActivated to set
     */
    public void setAccountActivated(final Boolean accountActivated) {
        this.accountActivated = accountActivated;
    }
}
