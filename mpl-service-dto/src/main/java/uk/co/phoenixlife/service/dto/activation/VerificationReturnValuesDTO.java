/*
 * VerificationReturnValuesDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.activation;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * VerificationReturnValuesDTO.java
 */
public class VerificationReturnValuesDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String question;
    private String questionkey;
    private String questionLabel;
    private String questionLabelForAccountNumber;
    private int count;
    private String lockedMessage;
    private String exhaustMessage;
    private String incorrectAnswerMessage;
    private String errorMsg;
    private String errorCode;
    private Integer lockedListCount;
    private ConcurrentHashMap<String, ConcurrentHashMap<String, String>> checkAnswerMap;
    private Long policyId;
    private Long customerId;

    /**
     * Default Constructor
     */
    public VerificationReturnValuesDTO() {
        super();
    }

    /**
     * Parameterized Constructor
     *
     * @param pquestion
     *            - question
     * @param pquestionkey
     *            - questionkey
     * @param pquestionLabel
     *            - questionLabel
     * @param pquestionLabelForAccountNumber
     *            - questionLabelForAccountNumber
     * @param pcount
     *            - count
     * @param plockedMessage
     *            - lockedMessage
     * @param pincorrectAnswerMessage
     *            - incorrectAnswerMessage
     * @param perrorMsg
     *            - errorMsg
     * @param perrorCode
     *            - errorCode
     * @param plockedListCount
     *            - lockedListCount
     * @param pexhaustMessage
     *            - exhaustMessage
     */
    public VerificationReturnValuesDTO(final String pquestion, final String pquestionkey, final String pquestionLabel,
            final String pquestionLabelForAccountNumber, final int pcount, final String plockedMessage,
            final String pincorrectAnswerMessage,
            final String perrorMsg, final String perrorCode, final Integer plockedListCount, final String pexhaustMessage) {
        super();
        question = pquestion;
        questionkey = pquestionkey;
        questionLabel = pquestionLabel;
        questionLabelForAccountNumber = pquestionLabelForAccountNumber;
        count = pcount;
        lockedMessage = plockedMessage;
        incorrectAnswerMessage = pincorrectAnswerMessage;
        errorMsg = perrorMsg;
        errorCode = perrorCode;
        lockedListCount = plockedListCount;
        exhaustMessage = pexhaustMessage;
    }

    /**
     * get exhaustMessage
     *
     * @return exhaustMessage
     */
    public String getExhaustMessage() {
        return exhaustMessage;
    }

    /**
     * set exhaustMessage
     *
     * @param pexhaustMessage
     *            - pexhaustMessage
     */
    public void setExhaustMessage(final String pexhaustMessage) {
        exhaustMessage = pexhaustMessage;
    }

    /**
     * get errorCode
     *
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * set errorCode
     *
     * @param perrorCode
     *            the errorCode to set
     */
    public void setErrorCode(final String perrorCode) {
        errorCode = perrorCode;
    }

    /**
     * get errorMsg
     *
     * @return the errorMsg
     */
    public String getErrorMsg() {
        return errorMsg;
    }

    /**
     * Set errorMsg
     *
     * @param perrorMsg
     *            the errorMsg to set
     */
    public void setErrorMsg(final String perrorMsg) {
        errorMsg = perrorMsg;
    }

    /**
     * Gets the questionLabelForAccountNumber
     *
     * @return the questionLabelForAccountNumber
     */
    public String getQuestionLabelForAccountNumber() {
        return questionLabelForAccountNumber;
    }

    /**
     * Sets the questionLabelForAccountNumber
     *
     * @param pQuestionLabelForAccountNumber
     *            the questionLabelForAccountNumber to set
     */
    public void setQuestionLabelForAccountNumber(final String pQuestionLabelForAccountNumber) {
        questionLabelForAccountNumber = pQuestionLabelForAccountNumber;
    }

    /**
     * Gets the question
     *
     * @return the question
     */
    public String getQuestion() {
        return question;
    }

    /**
     * Sets the question
     *
     * @param pQuestion
     *            the question to set
     */
    public void setQuestion(final String pQuestion) {
        question = pQuestion;
    }

    /**
     * Gets the questionkey
     *
     * @return the questionkey
     */
    public String getQuestionkey() {
        return questionkey;
    }

    /**
     * Sets the questionkey
     *
     * @param pQuestionkey
     *            the questionkey to set
     */
    public void setQuestionkey(final String pQuestionkey) {
        questionkey = pQuestionkey;
    }

    /**
     * Gets the questionLabel
     *
     * @return the questionLabel
     */
    public String getQuestionLabel() {
        return questionLabel;
    }

    /**
     * Sets the questionLabel
     *
     * @param pQuestionLabel
     *            the questionLabel to set
     */
    public void setQuestionLabel(final String pQuestionLabel) {
        questionLabel = pQuestionLabel;
    }

    /**
     * Gets the count
     *
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * Sets the count
     *
     * @param pCount
     *            the count to set
     */
    public void setCount(final int pCount) {
        count = pCount;
    }

    /**
     * Gets the lockedMessage
     *
     * @return the lockedMessage
     */
    public String getLockedMessage() {
        return lockedMessage;
    }

    /**
     * Sets the lockedMessage
     *
     * @param pLockedMessage
     *            the lockedMessage to set
     */
    public void setLockedMessage(final String pLockedMessage) {
        lockedMessage = pLockedMessage;
    }

    /**
     * Gets the incorrectAnswerMessage
     *
     * @return the incorrectAnswerMessage
     */
    public String getIncorrectAnswerMessage() {
        return incorrectAnswerMessage;
    }

    /**
     * Sets the incorrectAnswerMessage
     *
     * @param pIncorrectAnswerMessage
     *            the incorrectAnswerMessage to set
     */
    public void setIncorrectAnswerMessage(final String pIncorrectAnswerMessage) {
        incorrectAnswerMessage = pIncorrectAnswerMessage;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    /**
     * Gets the lockedListCount
     *
     * @return lockedListCount
     */
    public Integer getLockedListCount() {
        return lockedListCount;
    }

    /**
     * Sets the lockedListCount
     *
     * @param plockedListCount
     *            - plockedListCount
     */
    public void setLockedListCount(final Integer plockedListCount) {
        lockedListCount = plockedListCount;
    }

    /**
     * Get checkAnswerMap
     *
     * @return checkAnswerMap
     */
    public ConcurrentHashMap<String, ConcurrentHashMap<String, String>> getCheckAnswerMap() {
        return checkAnswerMap;
    }

    /**
     * set checkAnswerMap
     *
     * @param pcheckAnswerMap
     *            - pcheckAnswerMap
     */
    public void setCheckAnswerMap(final ConcurrentHashMap<String, ConcurrentHashMap<String, String>> pcheckAnswerMap) {
        checkAnswerMap = pcheckAnswerMap;
    }

    /**
     * Gets the policyId
     * 
     * @return the policyId
     */
    public Long getPolicyId() {
        return policyId;
    }

    /**
     * Sets the policyId
     * 
     * @param policyId
     *            the policyId to set
     */
    public void setPolicyId(final Long policyId) {
        this.policyId = policyId;
    }

    /**
     * Gets the customerId
     * 
     * @return the customerId
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * Sets the customerId
     * 
     * @param customerId
     *            the customerId to set
     */
    public void setCustomerId(final Long customerId) {
        this.customerId = customerId;
    }
}
