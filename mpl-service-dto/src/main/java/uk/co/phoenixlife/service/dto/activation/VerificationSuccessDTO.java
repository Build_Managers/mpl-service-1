package uk.co.phoenixlife.service.dto.activation;

/**
 * VerificationSuccessDTO.java
 */
public class VerificationSuccessDTO {

    private Integer lockedListCount;
    private Integer successFlag;
    private Long customerId;
    private Long policyId;

    /**
     * Gets the lockedListCount
     *
     * @return the lockedListCount
     */
    public Integer getLockedListCount() {
        return lockedListCount;
    }

    /**
     * Sets the lockedListCount
     *
     * @param pLockedListCount
     *            the lockedListCount to set
     */
    public void setLockedListCount(final Integer pLockedListCount) {
        lockedListCount = pLockedListCount;
    }

    /**
     * Gets the successFlag
     * 
     * @return the successFlag
     */
    public Integer getSuccessFlag() {
        return successFlag;
    }

    /**
     * Sets the successFlag
     * 
     * @param successFlag
     *            the successFlag to set
     */
    public void setSuccessFlag(final Integer successFlag) {
        this.successFlag = successFlag;
    }

    /**
     * Gets the customerId
     *
     * @return the customerId
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * Sets the customerId
     *
     * @param customerId
     *            the customerId to set
     */
    public void setCustomerId(final Long customerId) {
        this.customerId = customerId;
    }

    /**
     * Gets the policyId
     *
     * @return the policyId
     */
    public Long getPolicyId() {
        return policyId;
    }

    /**
     * Sets the policyId
     *
     * @param policyId
     *            the policyId to set
     */
    public void setPolicyId(final Long policyId) {
        this.policyId = policyId;
    }

}
