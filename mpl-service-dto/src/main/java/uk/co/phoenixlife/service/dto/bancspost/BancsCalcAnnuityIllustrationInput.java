/*
 * BancsCalcAnnuityIllustrationInput.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.bancspost;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * BancsCalcAnnuityIllustrationInput.java
 */
public class BancsCalcAnnuityIllustrationInput implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String optionCode;
    private String pmtFreq;
    private String advArr;
    private String lifeBasis;
    private String spouseBasis;
    private String spouseDOB;
    private int spousePct;
    private String escType;
    private double escRate;
    private String overlap;
    private String proportion;
    private int guarantee;

    /**
     * Gets the optionCode
     *
     * @return the optionCode
     */
    public String getOptionCode() {
        return optionCode;
    }

    /**
     * Sets the optionCode
     *
     * @param pOptionCode
     *            the optionCode to set
     */
    public void setOptionCode(final String pOptionCode) {
        optionCode = pOptionCode;
    }

    /**
     * Gets the pmtFreq
     *
     * @return the pmtFreq
     */
    public String getPmtFreq() {
        return pmtFreq;
    }

    /**
     * Sets the pmtFreq
     *
     * @param pPmtFreq
     *            the pmtFreq to set
     */
    public void setPmtFreq(final String pPmtFreq) {
        pmtFreq = pPmtFreq;
    }

    /**
     * Gets the advArr
     *
     * @return the advArr
     */
    public String getAdvArr() {
        return advArr;
    }

    /**
     * Sets the advArr
     *
     * @param pAdvArr
     *            the advArr to set
     */
    public void setAdvArr(final String pAdvArr) {
        advArr = pAdvArr;
    }

    /**
     * Gets the lifeBasis
     *
     * @return the lifeBasis
     */
    public String getLifeBasis() {
        return lifeBasis;
    }

    /**
     * Sets the lifeBasis
     *
     * @param pLifeBasis
     *            the lifeBasis to set
     */
    public void setLifeBasis(final String pLifeBasis) {
        lifeBasis = pLifeBasis;
    }

    /**
     * Gets the spouseBasis
     *
     * @return the spouseBasis
     */
    public String getSpouseBasis() {
        return spouseBasis;
    }

    /**
     * Sets the spouseBasis
     *
     * @param pSpouseBasis
     *            the spouseBasis to set
     */
    public void setSpouseBasis(final String pSpouseBasis) {
        spouseBasis = pSpouseBasis;
    }

    /**
     * Gets the spouseDOB
     *
     * @return the spouseDOB
     */
    public String getSpouseDOB() {
        return spouseDOB;
    }

    /**
     * Sets the spouseDOB
     *
     * @param pSpouseDOB
     *            the spouseDOB to set
     */
    public void setSpouseDOB(final String pSpouseDOB) {
        spouseDOB = pSpouseDOB;
    }

    /**
     * Gets the spousePct
     *
     * @return the spousePct
     */
    public int getSpousePct() {
        return spousePct;
    }

    /**
     * Sets the spousePct
     *
     * @param pSpousePct
     *            the spousePct to set
     */
    public void setSpousePct(final int pSpousePct) {
        spousePct = pSpousePct;
    }

    /**
     * Gets the escType
     *
     * @return the escType
     */
    public String getEscType() {
        return escType;
    }

    /**
     * Sets the escType
     *
     * @param pEscType
     *            the escType to set
     */
    public void setEscType(final String pEscType) {
        escType = pEscType;
    }

    /**
     * Gets the escRate
     *
     * @return the escRate
     */
    public double getEscRate() {
        return escRate;
    }

    /**
     * Sets the escRate
     *
     * @param pEscRate
     *            the escRate to set
     */
    public void setEscRate(final double pEscRate) {
        escRate = pEscRate;
    }

    /**
     * Gets the overlap
     *
     * @return the overlap
     */
    public String getOverlap() {
        return overlap;
    }

    /**
     * Sets the overlap
     *
     * @param pOverlap
     *            the overlap to set
     */
    public void setOverlap(final String pOverlap) {
        overlap = pOverlap;
    }

    /**
     * Gets the proportion
     *
     * @return the proportion
     */
    public String getProportion() {
        return proportion;
    }

    /**
     * Sets the proportion
     *
     * @param pProportion
     *            the proportion to set
     */
    public void setProportion(final String pProportion) {
        proportion = pProportion;
    }

    /**
     * Gets the guarantee
     *
     * @return the guarantee
     */
    public int getGuarantee() {
        return guarantee;
    }

    /**
     * Sets the guarantee
     *
     * @param pGuarantee
     *            the guarantee to set
     */
    public void setGuarantee(final int pGuarantee) {
        guarantee = pGuarantee;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
