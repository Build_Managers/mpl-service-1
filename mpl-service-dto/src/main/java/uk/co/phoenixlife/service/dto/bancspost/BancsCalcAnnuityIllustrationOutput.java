/*
 * BancsCalcAnnuityIllustrationOutput.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.bancspost;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * BancsCalcAnnuityIllustrationOutput.java
 */
public class BancsCalcAnnuityIllustrationOutput implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String optionCode;
    private double annuityRate;
    private double costOfAnnuity;
    private double modalAmt;
    private double pCLS;
    private double tax;

    /**
     * Gets the optionCode
     *
     * @return the optionCode
     */
    public String getOptionCode() {
        return optionCode;
    }

    /**
     * Sets the optionCode
     *
     * @param pOptionCode
     *            the optionCode to set
     */
    public void setOptionCode(final String pOptionCode) {
        optionCode = pOptionCode;
    }

    /**
     * Gets the annuityRate
     *
     * @return the annuityRate
     */
    public double getAnnuityRate() {
        return annuityRate;
    }

    /**
     * Sets the annuityRate
     *
     * @param pAnnuityRate
     *            the annuityRate to set
     */
    public void setAnnuityRate(final double pAnnuityRate) {
        annuityRate = pAnnuityRate;
    }

    /**
     * Gets the costOfAnnuity
     *
     * @return the costOfAnnuity
     */
    public double getCostOfAnnuity() {
        return costOfAnnuity;
    }

    /**
     * Sets the costOfAnnuity
     *
     * @param pCostOfAnnuity
     *            the costOfAnnuity to set
     */
    public void setCostOfAnnuity(final double pCostOfAnnuity) {
        costOfAnnuity = pCostOfAnnuity;
    }

    /**
     * Gets the modalAmt
     *
     * @return the modalAmt
     */
    public double getModalAmt() {
        return modalAmt;
    }

    /**
     * Sets the modalAmt
     *
     * @param pModalAmt
     *            the modalAmt to set
     */
    public void setModalAmt(final double pModalAmt) {
        modalAmt = pModalAmt;
    }

    /**
     * Gets the pCLS
     *
     * @return the pCLS
     */
    public double getpCLS() {
        return pCLS;
    }

    /**
     * Sets the pCLS
     *
     * @param pPCLS
     *            the pCLS to set
     */
    public void setpCLS(final double pPCLS) {
        pCLS = pPCLS;
    }

    /**
     * Gets the tax
     *
     * @return the tax
     */
    public double getTax() {
        return tax;
    }

    /**
     * Sets the tax
     *
     * @param pTax
     *            the tax to set
     */
    public void setTax(final double pTax) {
        tax = pTax;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
