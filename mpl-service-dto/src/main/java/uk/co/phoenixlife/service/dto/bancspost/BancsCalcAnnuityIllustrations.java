/*
 * BancsCalcAnnuityIllustrations.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.bancspost;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * BancsCalcAnnuityIllustrations.java
 */
public class BancsCalcAnnuityIllustrations implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private List<BancsCalcAnnuityIllustrationOutput> listOfOptions;

    /**
     * Gets the listOfOptions
     *
     * @return the listOfOptions
     */
    public List<BancsCalcAnnuityIllustrationOutput> getListOfOptions() {
        return listOfOptions;
    }

    /**
     * Sets the listOfOptions
     *
     * @param pListOfOptions
     *            the listOfOptions to set
     */
    public void setListOfOptions(final List<BancsCalcAnnuityIllustrationOutput> pListOfOptions) {
        listOfOptions = pListOfOptions;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
