/*
 * BancsCalcCashInTaxInput.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.bancspost;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * BancsCalcCashInTaxInput.java
 */
public class BancsCalcCashInTaxInput implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String bancsUniqueCustNo;
    private BancsCalcNonAnnuityIllustrationInput nonAnnuityIllustrationInput;
    private List<BancsCalcAnnuityIllustrationInput> annuityIllustrationInput;

    /**
     * Gets the bancsUniqueCustNo
     *
     * @return the bancsUniqueCustNo
     */
    public String getBancsUniqueCustNo() {
        return bancsUniqueCustNo;
    }

    /**
     * Sets the bancsUniqueCustNo
     *
     * @param pBancsUniqueCustNo
     *            the bancsUniqueCustNo to set
     */
    public void setBancsUniqueCustNo(final String pBancsUniqueCustNo) {
        bancsUniqueCustNo = pBancsUniqueCustNo;
    }

    /**
     * Gets the nonAnnuityIllustrationInput
     *
     * @return the nonAnnuityIllustrationInput
     */
    public BancsCalcNonAnnuityIllustrationInput getNonAnnuityIllustrationInput() {
        return nonAnnuityIllustrationInput;
    }

    /**
     * Sets the nonAnnuityIllustrationInput
     *
     * @param pNonAnnuityIllustrationInput
     *            the nonAnnuityIllustrationInput to set
     */
    public void setNonAnnuityIllustrationInput(final BancsCalcNonAnnuityIllustrationInput pNonAnnuityIllustrationInput) {
        nonAnnuityIllustrationInput = pNonAnnuityIllustrationInput;
    }

    /**
     * Gets the annuityIllustrationInput
     *
     * @return the annuityIllustrationInput
     */
    public List<BancsCalcAnnuityIllustrationInput> getAnnuityIllustrationInput() {
        return annuityIllustrationInput;
    }

    /**
     * Sets the annuityIllustrationInput
     *
     * @param pAnnuityIllustrationInput
     *            the annuityIllustrationInput to set
     */
    public void setAnnuityIllustrationInput(final List<BancsCalcAnnuityIllustrationInput> pAnnuityIllustrationInput) {
        annuityIllustrationInput = pAnnuityIllustrationInput;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
