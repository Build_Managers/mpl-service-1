/*
 * BancsCalcCashInTaxOutput.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.bancspost;

import java.io.Serializable;
import java.sql.Timestamp;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * BancsCalcCashInTaxOutput.java
 */
public class BancsCalcCashInTaxOutput implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String bancsUniqueCustNo;
    private String bancsPolNum;
    private Timestamp busTimeStamp;
    private BancsCalcNonAnnuityIllustrationOutput nonAnnuityIllustration;
    private BancsCalcAnnuityIllustrations annuityIllustration;

    /**
     * Gets the bancsUniqueCustNo
     *
     * @return the bancsUniqueCustNo
     */
    public String getBancsUniqueCustNo() {
        return bancsUniqueCustNo;
    }

    /**
     * Sets the bancsUniqueCustNo
     *
     * @param pBancsUniqueCustNo
     *            the bancsUniqueCustNo to set
     */
    public void setBancsUniqueCustNo(final String pBancsUniqueCustNo) {
        bancsUniqueCustNo = pBancsUniqueCustNo;
    }

    /**
     * Gets the bancsPolNum
     *
     * @return the bancsPolNum
     */
    public String getBancsPolNum() {
        return bancsPolNum;
    }

    /**
     * Sets the bancsPolNum
     *
     * @param pBancsPolNum
     *            the bancsPolNum to set
     */
    public void setBancsPolNum(final String pBancsPolNum) {
        bancsPolNum = pBancsPolNum;
    }

    /**
     * Gets the busTimeStamp
     *
     * @return the busTimeStamp
     */
    public Timestamp getBusTimeStamp() {
        return busTimeStamp;
    }

    /**
     * Sets the busTimeStamp
     *
     * @param pBusTimeStamp
     *            the busTimeStamp to set
     */
    public void setBusTimeStamp(final Timestamp pBusTimeStamp) {
        busTimeStamp = pBusTimeStamp;
    }

    /**
     * Gets the nonAnnuityIllustration
     *
     * @return the nonAnnuityIllustration
     */
    public BancsCalcNonAnnuityIllustrationOutput getNonAnnuityIllustration() {
        return nonAnnuityIllustration;
    }

    /**
     * Sets the nonAnnuityIllustration
     *
     * @param pNonAnnuityIllustration
     *            the nonAnnuityIllustration to set
     */
    public void setNonAnnuityIllustration(final BancsCalcNonAnnuityIllustrationOutput pNonAnnuityIllustration) {
        nonAnnuityIllustration = pNonAnnuityIllustration;
    }

    /**
     * Gets the annuityIllustration
     *
     * @return the annuityIllustration
     */
    public BancsCalcAnnuityIllustrations getAnnuityIllustration() {
        return annuityIllustration;
    }

    /**
     * Sets the annuityIllustration
     *
     * @param pAnnuityIllustration
     *            the annuityIllustration to set
     */
    public void setAnnuityIllustration(final BancsCalcAnnuityIllustrations pAnnuityIllustration) {
        annuityIllustration = pAnnuityIllustration;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
