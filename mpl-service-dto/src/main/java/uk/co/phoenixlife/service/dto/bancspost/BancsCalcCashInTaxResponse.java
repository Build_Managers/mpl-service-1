/*
 * BancsCalcCashInTaxResponse.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.bancspost;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * BancsCalcCashInTaxResponse.java
 */
public class BancsCalcCashInTaxResponse implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private BancsCalcCashInTaxOutput bancsCalcCashInTax;

    /**
     * Gets the bancsCalcCashInTax
     *
     * @return the bancsCalcCashInTax
     */
    public BancsCalcCashInTaxOutput getBancsCalcCashInTax() {
        return bancsCalcCashInTax;
    }

    /**
     * Sets the bancsCalcCashInTax
     *
     * @param pBancsCalcCashInTax
     *            the bancsCalcCashInTax to set
     */
    public void setBancsCalcCashInTax(final BancsCalcCashInTaxOutput pBancsCalcCashInTax) {
        bancsCalcCashInTax = pBancsCalcCashInTax;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
