/*
 * BancsCalcNonAnnuityIllustrationOutput.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.bancspost;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * BancsCalcNonAnnuityIllustrationOutput.java
 */
public class BancsCalcNonAnnuityIllustrationOutput implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private List<BancsCalcTaxOutput> listOfCashInTypes;

    /**
     * Gets the listOfCashInTypes
     *
     * @return the listOfCashInTypes
     */
    public List<BancsCalcTaxOutput> getListOfCashInTypes() {
        return listOfCashInTypes;
    }

    /**
     * Sets the listOfCashInTypes
     *
     * @param pListOfCashInTypes
     *            the listOfCashInTypes to set
     */
    public void setListOfCashInTypes(final List<BancsCalcTaxOutput> pListOfCashInTypes) {
        listOfCashInTypes = pListOfCashInTypes;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
