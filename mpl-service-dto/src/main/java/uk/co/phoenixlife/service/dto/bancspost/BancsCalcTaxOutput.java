/*
 * BancsCalcTaxOutput.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.bancspost;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * BancsCalcTaxOutput.java
 */
public class BancsCalcTaxOutput extends BancsCalcCashInType {

    /** long */
    private static final long serialVersionUID = 1L;

    private double pCLS;
    private double tax;

    /**
     * Gets the pCLS
     *
     * @return the pCLS
     */
    public double getpCLS() {
        return pCLS;
    }

    /**
     * Sets the pCLS
     *
     * @param pPCLS
     *            the pCLS to set
     */
    public void setpCLS(final double pPCLS) {
        pCLS = pPCLS;
    }

    /**
     * Gets the tax
     *
     * @return the tax
     */
    public double getTax() {
        return tax;
    }

    /**
     * Sets the tax
     *
     * @param pTax
     *            the tax to set
     */
    public void setTax(final double pTax) {
        tax = pTax;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
