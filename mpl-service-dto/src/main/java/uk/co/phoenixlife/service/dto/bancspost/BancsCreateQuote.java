/*
 * BancsCreateQuote.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.bancspost;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * BancsCreateQuote.java
 */
public class BancsCreateQuote implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String paytMethod;
    private String sortCode;
    private String bankAcct;
    private String rollNum;
    private String cashInType;
    private String suppressQte;
    private String contactCustomer;

    /**
     * Gets the paytMethod
     *
     * @return the paytMethod
     */
    public String getPaytMethod() {
        return paytMethod;
    }

    /**
     * Sets the paytMethod
     *
     * @param pPaytMethod
     *            the paytMethod to set
     */
    public void setPaytMethod(final String pPaytMethod) {
        paytMethod = pPaytMethod;
    }

    /**
     * Gets the sortCode
     *
     * @return the sortCode
     */
    public String getSortCode() {
        return sortCode;
    }

    /**
     * Sets the sortCode
     *
     * @param pSortCode
     *            the sortCode to set
     */
    public void setSortCode(final String pSortCode) {
        sortCode = pSortCode;
    }

    /**
     * Gets the bankAcct
     *
     * @return the bankAcct
     */
    public String getBankAcct() {
        return bankAcct;
    }

    /**
     * Sets the bankAcct
     *
     * @param pBankAcct
     *            the bankAcct to set
     */
    public void setBankAcct(final String pBankAcct) {
        bankAcct = pBankAcct;
    }

    /**
     * Gets the rollNum
     *
     * @return the rollNum
     */
    public String getRollNum() {
        return rollNum;
    }

    /**
     * Sets the rollNum
     *
     * @param pRollNum
     *            the rollNum to set
     */
    public void setRollNum(final String pRollNum) {
        rollNum = pRollNum;
    }

    /**
     * Gets the cashInType
     *
     * @return the cashInType
     */
    public String getCashInType() {
        return cashInType;
    }

    /**
     * Sets the cashInType
     *
     * @param pCashInType
     *            the cashInType to set
     */
    public void setCashInType(final String pCashInType) {
        cashInType = pCashInType;
    }

    /**
     * Gets the suppressQte
     *
     * @return the suppressQte
     */
    public String getSuppressQte() {
        return suppressQte;
    }

    /**
     * Sets the suppressQte
     *
     * @param pSuppressQte
     *            the suppressQte to set
     */
    public void setSuppressQte(final String pSuppressQte) {
        suppressQte = pSuppressQte;
    }

    /**
     * Gets the contactCustomer
     *
     * @return the contactCustomer
     */
    public String getContactCustomer() {
        return contactCustomer;
    }

    /**
     * Sets the contactCustomer
     *
     * @param pContactCustomer
     *            the contactCustomer to set
     */
    public void setContactCustomer(final String pContactCustomer) {
        contactCustomer = pContactCustomer;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
