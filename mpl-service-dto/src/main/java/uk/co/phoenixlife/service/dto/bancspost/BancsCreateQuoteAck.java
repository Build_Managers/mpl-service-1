/*
 * BancsCreateQuoteAck.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.bancspost;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * BancsCreateQuoteAck.java
 */
public class BancsCreateQuoteAck implements Serializable {

    /** long */
    private static final long serialVersionUID = 6351683285353971468L;

    private String quoteRef;
    private double totValue;
    private double mvaValue;
    private double penValue;
    private double cuCharge;
    private double pCLS;
    private double tax;

    /**
     * Gets the quoteRef
     *
     * @return the quoteRef
     */
    public String getQuoteRef() {
        return quoteRef;
    }

    /**
     * Sets the quoteRef
     *
     * @param pQuoteRef
     *            the quoteRef to set
     */
    public void setQuoteRef(final String pQuoteRef) {
        quoteRef = pQuoteRef;
    }

    /**
     * Gets the totValue
     *
     * @return the totValue
     */
    public double getTotValue() {
        return totValue;
    }

    /**
     * Sets the totValue
     *
     * @param pTotValue
     *            the totValue to set
     */
    public void setTotValue(final double pTotValue) {
        totValue = pTotValue;
    }

    /**
     * Gets the mvaValue
     *
     * @return the mvaValue
     */
    public double getMvaValue() {
        return mvaValue;
    }

    /**
     * Sets the mvaValue
     *
     * @param pMvaValue
     *            the mvaValue to set
     */
    public void setMvaValue(final double pMvaValue) {
        mvaValue = pMvaValue;
    }

    /**
     * Gets the penValue
     *
     * @return the penValue
     */
    public double getPenValue() {
        return penValue;
    }

    /**
     * Sets the penValue
     *
     * @param pPenValue
     *            the penValue to set
     */
    public void setPenValue(final double pPenValue) {
        penValue = pPenValue;
    }

    /**
     * Gets the cuCharge
     *
     * @return the cuCharge
     */
    public double getCuCharge() {
        return cuCharge;
    }

    /**
     * Sets the cuCharge
     *
     * @param pCuCharge
     *            the cuCharge to set
     */
    public void setCuCharge(final double pCuCharge) {
        cuCharge = pCuCharge;
    }

    /**
     * Gets the pCLS
     *
     * @return the pCLS
     */
    public double getpCLS() {
        return pCLS;
    }

    /**
     * Sets the pCLS
     *
     * @param pPCLS
     *            the pCLS to set
     */
    public void setpCLS(final double pPCLS) {
        pCLS = pPCLS;
    }

    /**
     * Gets the tax
     *
     * @return the tax
     */
    public double getTax() {
        return tax;
    }

    /**
     * Sets the tax
     *
     * @param pTax
     *            the tax to set
     */
    public void setTax(final double pTax) {
        tax = pTax;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
