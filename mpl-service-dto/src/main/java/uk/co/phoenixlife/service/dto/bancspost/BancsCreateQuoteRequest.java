/*
 * BancsCreateQuoteRequest.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.bancspost;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * BancsCreateQuoteRequest.java
 */
public class BancsCreateQuoteRequest implements Serializable {

    /** long */
    private static final long serialVersionUID = 909897453932581747L;

    private BancsCreateQuote bancsCreateQuote;

    /**
     * Gets the bancsCreateQuote
     *
     * @return the bancsCreateQuote
     */
    public BancsCreateQuote getBancsCreateQuote() {
        return bancsCreateQuote;
    }

    /**
     * Sets the bancsCreateQuote
     *
     * @param pBancsCreateQuote
     *            the bancsCreateQuote to set
     */
    public void setBancsCreateQuote(final BancsCreateQuote pBancsCreateQuote) {
        bancsCreateQuote = pBancsCreateQuote;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
