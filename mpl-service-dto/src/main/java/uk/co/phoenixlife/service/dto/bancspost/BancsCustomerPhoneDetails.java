/*
 * CustomerPhoneDetails.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.bancspost;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * CustomerPhoneDetails.java
 */
public class BancsCustomerPhoneDetails implements Serializable {

    /** long */
    private static final long serialVersionUID = 8936943962151551740L;

    private String phoneType;
    private char preferred;
    private String countryCode;
    private String std;
    private String number;

    /**
     * Gets the phoneType
     *
     * @return the phoneType
     */
    public String getPhoneType() {
        return phoneType;
    }

    /**
     * Sets the phoneType
     *
     * @param pPhoneType
     *            the phoneType to set
     */
    public void setPhoneType(final String pPhoneType) {
        phoneType = pPhoneType;
    }

    /**
     * Gets the preferred
     *
     * @return the preferred
     */
    public char getPreferred() {
        return preferred;
    }

    /**
     * Sets the preferred
     *
     * @param pPreferred
     *            the preferred to set
     */
    public void setPreferred(final char pPreferred) {
        preferred = pPreferred;
    }

    /**
     * Gets the countryCode
     *
     * @return the countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Sets the countryCode
     *
     * @param pCountryCode
     *            the countryCode to set
     */
    public void setCountryCode(final String pCountryCode) {
        countryCode = pCountryCode;
    }

    /**
     * Gets the std
     *
     * @return the std
     */
    public String getStd() {
        return std;
    }

    /**
     * Sets the std
     *
     * @param pStd
     *            the std to set
     */
    public void setStd(final String pStd) {
        std = pStd;
    }

    /**
     * Gets the number
     *
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * Sets the number
     *
     * @param pNumber
     *            the number to set
     */
    public void setNumber(final String pNumber) {
        number = pNumber;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
