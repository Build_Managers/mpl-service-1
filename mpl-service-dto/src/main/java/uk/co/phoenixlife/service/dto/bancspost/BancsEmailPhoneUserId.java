/*
 * BancsEmailPhoneUserId.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.bancspost;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * BancsEmailPhoneUserId.java
 */
public class BancsEmailPhoneUserId implements Serializable {

    /** long */
    private static final long serialVersionUID = 5758770358756435938L;

    private char updateMail;
    private String emailId;
    private char updateUserName;
    private String userName;
    private char updateNiNo;
    private String niNo;
    /*
     * private char updateAddress; private String houseNumber; private String
     * houseName; private String address1; private String address2; private
     * String cityTown; private String county; private String postCode; private
     * String country;
     */
    private List<BancsCustomerPhoneDetails> listOfPhones;

    /**
     * Gets the updateMail
     *
     * @return the updateMail
     */
    public char getUpdateMail() {
        return updateMail;
    }

    /**
     * Sets the updateMail
     *
     * @param pUpdateMail
     *            the updateMail to set
     */
    public void setUpdateMail(final char pUpdateMail) {
        updateMail = pUpdateMail;
    }

    /**
     * Gets the emailId
     *
     * @return the emailId
     */
    public String getEmailId() {
        return emailId;
    }

    /**
     * Sets the emailId
     *
     * @param pEmailId
     *            the emailId to set
     */
    public void setEmailId(final String pEmailId) {
        emailId = pEmailId;
    }

    /**
     * Gets the updateUserName
     *
     * @return the updateUserName
     */
    public char getUpdateUserName() {
        return updateUserName;
    }

    /**
     * Sets the updateUserName
     *
     * @param pUpdateUserName
     *            the updateUserName to set
     */
    public void setUpdateUserName(final char pUpdateUserName) {
        updateUserName = pUpdateUserName;
    }

    /**
     * Gets the userName
     *
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the userName
     *
     * @param pUserName
     *            the userName to set
     */
    public void setUserName(final String pUserName) {
        userName = pUserName;
    }

    /**
     * Gets the updateNiNo
     *
     * @return the updateNiNo
     */
    public char getUpdateNiNo() {
        return updateNiNo;
    }

    /**
     * Sets the updateNiNo
     *
     * @param pUpdateNiNo
     *            the updateNiNo to set
     */
    public void setUpdateNiNo(final char pUpdateNiNo) {
        updateNiNo = pUpdateNiNo;
    }

    /**
     * Gets the niNo
     *
     * @return the niNo
     */
    public String getNiNo() {
        return niNo;
    }

    /**
     * Sets the niNo
     *
     * @param pNiNo
     *            the niNo to set
     */
    public void setNiNo(final String pNiNo) {
        niNo = pNiNo;
    }

    /**
     * Gets the listOfPhones
     *
     * @return the listOfPhones
     */
    public List<BancsCustomerPhoneDetails> getListOfPhones() {
        return listOfPhones;
    }

    /**
     * Sets the listOfPhones
     *
     * @param pListOfPhones
     *            the listOfPhones to set
     */
    public void setListOfPhones(final List<BancsCustomerPhoneDetails> pListOfPhones) {
        listOfPhones = pListOfPhones;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
