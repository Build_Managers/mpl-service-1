/*
 * BancsEmailPhoneUserIdRequest.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.bancspost;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * BancsEmailPhoneUserIdRequest.java
 */
public class BancsEmailPhoneUserIdRequest implements Serializable {

    /** long */
    private static final long serialVersionUID = 2657691675833073359L;

    private BancsEmailPhoneUserId bancsEmailPhoneUserid;

    /**
     * Gets the bancsEmailPhoneUserid
     *
     * @return the bancsEmailPhoneUserid
     */
    public BancsEmailPhoneUserId getBancsEmailPhoneUserid() {
        return bancsEmailPhoneUserid;
    }

    /**
     * Sets the bancsEmailPhoneUserid
     *
     * @param pBancsEmailPhoneUserid
     *            the bancsEmailPhoneUserid to set
     */
    public void setBancsEmailPhoneUserid(final BancsEmailPhoneUserId pBancsEmailPhoneUserid) {
        bancsEmailPhoneUserid = pBancsEmailPhoneUserid;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
