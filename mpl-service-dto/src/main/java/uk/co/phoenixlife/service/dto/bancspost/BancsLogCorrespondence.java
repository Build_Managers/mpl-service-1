/*
 * BancsLogCorrespondence.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.bancspost;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * BancsLogCorrespondence.java
 */
public class BancsLogCorrespondence implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String bancsUniqueCustNo;
    private String legacyCustNo;
    private List<BancsLogCorrespondenceDocument> documentList;

    /**
     * Gets the bancsUniqueCustNo
     *
     * @return the bancsUniqueCustNo
     */
    public String getBancsUniqueCustNo() {
        return bancsUniqueCustNo;
    }

    /**
     * Sets the bancsUniqueCustNo
     *
     * @param pBancsUniqueCustNo
     *            the bancsUniqueCustNo to set
     */
    public void setBancsUniqueCustNo(final String pBancsUniqueCustNo) {
        bancsUniqueCustNo = pBancsUniqueCustNo;
    }

    /**
     * Gets the legacyCustNo
     *
     * @return the legacyCustNo
     */
    public String getLegacyCustNo() {
        return legacyCustNo;
    }

    /**
     * Sets the legacyCustNo
     *
     * @param pLegacyCustNo
     *            the legacyCustNo to set
     */
    public void setLegacyCustNo(final String pLegacyCustNo) {
        legacyCustNo = pLegacyCustNo;
    }

    /**
     * Gets the documentList
     *
     * @return the documentList
     */
    public List<BancsLogCorrespondenceDocument> getDocumentList() {
        return documentList;
    }

    /**
     * Sets the documentList
     *
     * @param pDocumentList
     *            the documentList to set
     */
    public void setDocumentList(final List<BancsLogCorrespondenceDocument> pDocumentList) {
        documentList = pDocumentList;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
