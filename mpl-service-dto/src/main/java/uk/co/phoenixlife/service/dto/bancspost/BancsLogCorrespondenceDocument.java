/*
 * BancsLogCorrespondenceDocument.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.bancspost;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * BancsLogCorrespondenceDocument.java
 */
public class BancsLogCorrespondenceDocument implements Serializable {

    /** long */
    private static final long serialVersionUID = -2489907008445319767L;

    private String docName;
    private String docCode;
    private String docContent;
    private String docDate;
    private String docMediaType;

    /**
     * Gets the docName
     *
     * @return the docName
     */
    public String getDocName() {
        return docName;
    }

    /**
     * Sets the docName
     *
     * @param pDocName
     *            the docName to set
     */
    public void setDocName(final String pDocName) {
        docName = pDocName;
    }

    /**
     * Gets the docCode
     *
     * @return the docCode
     */
    public String getDocCode() {
        return docCode;
    }

    /**
     * Sets the docCode
     *
     * @param pDocCode
     *            the docCode to set
     */
    public void setDocCode(final String pDocCode) {
        docCode = pDocCode;
    }

    /**
     * Gets the docContent
     *
     * @return the docContent
     */
    public String getDocContent() {
        return docContent;
    }

    /**
     * Sets the docContent
     *
     * @param pDocContent
     *            the docContent to set
     */
    public void setDocContent(final String pDocContent) {
        docContent = pDocContent;
    }

    /**
     * Gets the docDate
     *
     * @return the docDate
     */
    public String getDocDate() {
        return docDate;
    }

    /**
     * Sets the docDate
     *
     * @param pDocDate
     *            the docDate to set
     */
    public void setDocDate(final String pDocDate) {
        docDate = pDocDate;
    }

    /**
     * Gets the docMediaType
     *
     * @return the docMediaType
     */
    public String getDocMediaType() {
        return docMediaType;
    }

    /**
     * Sets the docMediaType
     *
     * @param pDocMediaType
     *            the docMediaType to set
     */
    public void setDocMediaType(final String pDocMediaType) {
        docMediaType = pDocMediaType;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
