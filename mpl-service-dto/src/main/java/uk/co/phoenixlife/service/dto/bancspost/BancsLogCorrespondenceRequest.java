/*
 * BancsLogCorrespondenceRequest.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.bancspost;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * BancsLogCorrespondenceRequest.java
 */
public class BancsLogCorrespondenceRequest implements Serializable {

    /** long */
    private static final long serialVersionUID = -7702678978279891773L;

    private BancsLogCorrespondence bancsLogCorrespondence;

    /**
     * Gets the bancsLogCorrespondence
     *
     * @return the bancsLogCorrespondence
     */
    public BancsLogCorrespondence getBancsLogCorrespondence() {
        return bancsLogCorrespondence;
    }

    /**
     * Sets the bancsLogCorrespondence
     *
     * @param pBancsLogCorrespondence
     *            the bancsLogCorrespondence to set
     */
    public void setBancsLogCorrespondence(final BancsLogCorrespondence pBancsLogCorrespondence) {
        bancsLogCorrespondence = pBancsLogCorrespondence;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
