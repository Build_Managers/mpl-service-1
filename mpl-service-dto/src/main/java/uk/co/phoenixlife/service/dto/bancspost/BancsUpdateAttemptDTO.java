/*
 * BancsUpdateAttemptDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.bancspost;

import java.io.Serializable;
import java.time.LocalDateTime;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * BancsUpdateAttemptDTO.java
 */
public class BancsUpdateAttemptDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = -6818072230226615189L;

    private char updateStatus;
    private LocalDateTime updateTmst;
    private char ackFlg;
    private LocalDateTime ackTmst;
    private String failureReason;
    private String quoteRef;

    /**
     * Default Constructor
     */
    public BancsUpdateAttemptDTO() {
        super();
    }

    /**
     * Constructor
     *
     * @param updateStatus
     * @param updateTmst
     * @param ackFlg
     * @param ackTmst
     * @param failureReason
     */
    public BancsUpdateAttemptDTO(final char updateStatus, final LocalDateTime updateTmst, final char ackFlg,
            final LocalDateTime ackTmst, final String failureReason) {
        super();
        this.updateStatus = updateStatus;
        this.updateTmst = updateTmst;
        this.ackFlg = ackFlg;
        this.ackTmst = ackTmst;
        this.failureReason = failureReason;
    }

    /**
     * Gets the updateStatus
     *
     * @return the updateStatus
     */
    public char getUpdateStatus() {
        return updateStatus;
    }

    /**
     * Sets the updateStatus
     *
     * @param updateStatus
     *            the updateStatus to set
     */
    public void setUpdateStatus(final char updateStatus) {
        this.updateStatus = updateStatus;
    }

    /**
     * Gets the updateTmst
     *
     * @return the updateTmst
     */
    public LocalDateTime getUpdateTmst() {
        return updateTmst;
    }

    /**
     * Sets the updateTmst
     *
     * @param updateTmst
     *            the updateTmst to set
     */
    public void setUpdateTmst(final LocalDateTime updateTmst) {
        this.updateTmst = updateTmst;
    }

    /**
     * Gets the ackFlg
     *
     * @return the ackFlg
     */
    public char getAckFlg() {
        return ackFlg;
    }

    /**
     * Sets the ackFlg
     *
     * @param ackFlg
     *            the ackFlg to set
     */
    public void setAckFlg(final char ackFlg) {
        this.ackFlg = ackFlg;
    }

    /**
     * Gets the ackTmst
     *
     * @return the ackTmst
     */
    public LocalDateTime getAckTmst() {
        return ackTmst;
    }

    /**
     * Sets the ackTmst
     *
     * @param ackTmst
     *            the ackTmst to set
     */
    public void setAckTmst(final LocalDateTime ackTmst) {
        this.ackTmst = ackTmst;
    }

    /**
     * Gets the failureReason
     *
     * @return the failureReason
     */
    public String getFailureReason() {
        return failureReason;
    }

    /**
     * Sets the failureReason
     *
     * @param failureReason
     *            the failureReason to set
     */
    public void setFailureReason(final String failureReason) {
        this.failureReason = failureReason;
    }

    /**
     * Gets the quoteRef
     * 
     * @return the quoteRef
     */
    public String getQuoteRef() {
        return quoteRef;
    }

    /**
     * Sets the quoteRef
     * 
     * @param quoteRef
     *            the quoteRef to set
     */
    public void setQuoteRef(final String quoteRef) {
        this.quoteRef = quoteRef;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
