/*
 * BankValidationRequest.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.bankvalidation;

import java.io.Serializable;

/**
 * VocalinkRequest.java
 */
public class BankValidationRequest implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The account code. */
	private String accountCode;

	/** The account no. */
	private String accountNo;

	/** The branch. */
	private String branch;

	/** The name. */
	private String name;

	/** The sort code. */
	private String sortCode;

	/**
	 * Gets the accountCode.
	 *
	 * @return the accountCode
	 */
	public String getAccountCode() {
		return accountCode;
	}

	/**
	 * Sets the accountCode.
	 *
	 * @param pAccountCode
	 *            the accountCode to set
	 */
	public void setAccountCode(final String pAccountCode) {
		accountCode = pAccountCode;
	}

	/**
	 * Gets the accountNo.
	 *
	 * @return the accountNo
	 */
	public String getAccountNo() {
		return accountNo;
	}

	/**
	 * Sets the accountNo.
	 *
	 * @param pAccountNo
	 *            the accountNo to set
	 */
	public void setAccountNo(final String pAccountNo) {
		accountNo = pAccountNo;
	}

	/**
	 * Gets the branch.
	 *
	 * @return the branch
	 */
	public String getBranch() {
		return branch;
	}

	/**
	 * Sets the branch.
	 *
	 * @param pBranch
	 *            the branch to set
	 */
	public void setBranch(final String pBranch) {
		branch = pBranch;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param pName
	 *            the name to set
	 */
	public void setName(final String pName) {
		name = pName;
	}

	/**
	 * Gets the sortCode.
	 *
	 * @return the sortCode
	 */
	public String getSortCode() {
		return sortCode;
	}

	/**
	 * Sets the sortCode.
	 *
	 * @param pSortCode
	 *            the sortCode to set
	 */
	public void setSortCode(final String pSortCode) {
		sortCode = pSortCode;
	}
}
