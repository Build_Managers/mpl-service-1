/*
 * BankValidationResponse.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.bankvalidation;

import java.io.Serializable;
import java.util.List;


/**
 * VocalinkResponse.java
 */
public class BankValidationResponse implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The account no. */
	private String accountNo;

	/** The address 01. */
	private String address01;

	/** The address 02. */
	private String address02;

	/** The address 03. */
	private String address03;

	/** The address 04. */
	private String address04;

	/** The bacs credits allowed. */
	private boolean bacsCreditsAllowed;

	/**
	 * Gets the account no.
	 *
	 * @return the account no
	 */
	public String getAccountNo() {
		return accountNo;
	}

	/**
	 * Sets the account no.
	 *
	 * @param accountNo
	 *            the new account no
	 */
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	/**
	 * Gets the address 01.
	 *
	 * @return the address 01
	 */
	public String getAddress01() {
		return address01;
	}

	/**
	 * Sets the address 01.
	 *
	 * @param address01
	 *            the new address 01
	 */
	public void setAddress01(String address01) {
		this.address01 = address01;
	}

	/**
	 * Gets the address 02.
	 *
	 * @return the address 02
	 */
	public String getAddress02() {
		return address02;
	}

	/**
	 * Sets the address 02.
	 *
	 * @param address02
	 *            the new address 02
	 */
	public void setAddress02(String address02) {
		this.address02 = address02;
	}

	/**
	 * Gets the address 03.
	 *
	 * @return the address 03
	 */
	public String getAddress03() {
		return address03;
	}

	/**
	 * Sets the address 03.
	 *
	 * @param address03
	 *            the new address 03
	 */
	public void setAddress03(String address03) {
		this.address03 = address03;
	}

	/**
	 * Gets the address 04.
	 *
	 * @return the address 04
	 */
	public String getAddress04() {
		return address04;
	}

	/**
	 * Sets the address 04.
	 *
	 * @param address04
	 *            the new address 04
	 */
	public void setAddress04(String address04) {
		this.address04 = address04;
	}

	/**
	 * Checks if is bacs credits allowed.
	 *
	 * @return true, if is bacs credits allowed
	 */
	public boolean isBacsCreditsAllowed() {
		return bacsCreditsAllowed;
	}

	/**
	 * Sets the bacs credits allowed.
	 *
	 * @param bacsCreditsAllowed
	 *            the new bacs credits allowed
	 */
	public void setBacsCreditsAllowed(boolean bacsCreditsAllowed) {
		this.bacsCreditsAllowed = bacsCreditsAllowed;
	}

	/**
	 * Gets the bank society name.
	 *
	 * @return the bank society name
	 */
	public String getBankSocietyName() {
		return bankSocietyName;
	}

	/**
	 * Sets the bank society name.
	 *
	 * @param bankSocietyName
	 *            the new bank society name
	 */
	public void setBankSocietyName(String bankSocietyName) {
		this.bankSocietyName = bankSocietyName;
	}

	/**
	 * Gets the bic bank.
	 *
	 * @return the bic bank
	 */
	public String getBicBank() {
		return bicBank;
	}

	/**
	 * Sets the bic bank.
	 *
	 * @param bicBank
	 *            the new bic bank
	 */
	public void setBicBank(String bicBank) {
		this.bicBank = bicBank;
	}

	/**
	 * Gets the bic branch.
	 *
	 * @return the bic branch
	 */
	public String getBicBranch() {
		return bicBranch;
	}

	/**
	 * Sets the bic branch.
	 *
	 * @param bicBranch
	 *            the new bic branch
	 */
	public void setBicBranch(String bicBranch) {
		this.bicBranch = bicBranch;
	}

	/**
	 * Gets the branch.
	 *
	 * @return the branch
	 */
	public String getBranch() {
		return branch;
	}

	/**
	 * Sets the branch.
	 *
	 * @param branch
	 *            the new branch
	 */
	public void setBranch(String branch) {
		this.branch = branch;
	}

	/**
	 * Checks if is building society credits allowed.
	 *
	 * @return true, if is building society credits allowed
	 */
	public boolean isBuildingSocietyCreditsAllowed() {
		return buildingSocietyCreditsAllowed;
	}

	/**
	 * Sets the building society credits allowed.
	 *
	 * @param buildingSocietyCreditsAllowed
	 *            the new building society credits allowed
	 */
	public void setBuildingSocietyCreditsAllowed(boolean buildingSocietyCreditsAllowed) {
		this.buildingSocietyCreditsAllowed = buildingSocietyCreditsAllowed;
	}

	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 *
	 * @param country
	 *            the new country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Gets the county.
	 *
	 * @return the county
	 */
	public String getCounty() {
		return county;
	}

	/**
	 * Sets the county.
	 *
	 * @param county
	 *            the new county
	 */
	public void setCounty(String county) {
		this.county = county;
	}

	/**
	 * Checks if is direct debit allowed.
	 *
	 * @return true, if is direct debit allowed
	 */
	public boolean isDirectDebitAllowed() {
		return directDebitAllowed;
	}

	/**
	 * Sets the direct debit allowed.
	 *
	 * @param directDebitAllowed
	 *            the new direct debit allowed
	 */
	public void setDirectDebitAllowed(boolean directDebitAllowed) {
		this.directDebitAllowed = directDebitAllowed;
	}

	/**
	 * Checks if is direct debit instructions allowed.
	 *
	 * @return true, if is direct debit instructions allowed
	 */
	public boolean isDirectDebitInstructionsAllowed() {
		return directDebitInstructionsAllowed;
	}

	/**
	 * Sets the direct debit instructions allowed.
	 *
	 * @param directDebitInstructionsAllowed
	 *            the new direct debit instructions allowed
	 */
	public void setDirectDebitInstructionsAllowed(boolean directDebitInstructionsAllowed) {
		this.directDebitInstructionsAllowed = directDebitInstructionsAllowed;
	}

	/**
	 * Checks if is dividend interest payments allowed.
	 *
	 * @return true, if is dividend interest payments allowed
	 */
	public boolean isDividendInterestPaymentsAllowed() {
		return dividendInterestPaymentsAllowed;
	}

	/**
	 * Sets the dividend interest payments allowed.
	 *
	 * @param dividendInterestPaymentsAllowed
	 *            the new dividend interest payments allowed
	 */
	public void setDividendInterestPaymentsAllowed(boolean dividendInterestPaymentsAllowed) {
		this.dividendInterestPaymentsAllowed = dividendInterestPaymentsAllowed;
	}

	/**
	 * Gets the fax.
	 *
	 * @return the fax
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * Sets the fax.
	 *
	 * @param fax
	 *            the new fax
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * Gets the iban.
	 *
	 * @return the iban
	 */
	public String getIban() {
		return iban;
	}

	/**
	 * Sets the iban.
	 *
	 * @param iban
	 *            the new iban
	 */
	public void setIban(String iban) {
		this.iban = iban;
	}

	/**
	 * Checks if is life office debits allowed.
	 *
	 * @return true, if is life office debits allowed
	 */
	public boolean isLifeOfficeDebitsAllowed() {
		return lifeOfficeDebitsAllowed;
	}

	/**
	 * Sets the life office debits allowed.
	 *
	 * @param lifeOfficeDebitsAllowed
	 *            the new life office debits allowed
	 */
	public void setLifeOfficeDebitsAllowed(boolean lifeOfficeDebitsAllowed) {
		this.lifeOfficeDebitsAllowed = lifeOfficeDebitsAllowed;
	}


	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the phone.
	 *
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * Sets the phone.
	 *
	 * @param phone
	 *            the new phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * Gets the post code.
	 *
	 * @return the post code
	 */
	public String getPostCode() {
		return postCode;
	}

	/**
	 * Sets the post code.
	 *
	 * @param postCode
	 *            the new post code
	 */
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	/**
	 * Gets the sort code.
	 *
	 * @return the sort code
	 */
	public String getSortCode() {
		return sortCode;
	}

	/**
	 * Sets the sort code.
	 *
	 * @param sortCode
	 *            the new sort code
	 */
	public void setSortCode(String sortCode) {
		this.sortCode = sortCode;
	}

	/**
	 * Gets the sterling bic bank.
	 *
	 * @return the sterling bic bank
	 */
	public String getSterlingBicBank() {
		return sterlingBicBank;
	}

	/**
	 * Sets the sterling bic bank.
	 *
	 * @param sterlingBicBank
	 *            the new sterling bic bank
	 */
	public void setSterlingBicBank(String sterlingBicBank) {
		this.sterlingBicBank = sterlingBicBank;
	}

	/**
	 * Gets the sterling bic branch.
	 *
	 * @return the sterling bic branch
	 */
	public String getSterlingBicBranch() {
		return sterlingBicBranch;
	}

	/**
	 * Sets the sterling bic branch.
	 *
	 * @param sterlingBicBranch
	 *            the new sterling bic branch
	 */
	public void setSterlingBicBranch(String sterlingBicBranch) {
		this.sterlingBicBranch = sterlingBicBranch;
	}

	/**
	 * Gets the town.
	 *
	 * @return the town
	 */
	public String getTown() {
		return town;
	}

	/**
	 * Sets the town.
	 *
	 * @param town
	 *            the new town
	 */
	public void setTown(String town) {
		this.town = town;
	}

	/**
	 * Checks if is unpaid cheque claims allowed.
	 *
	 * @return true, if is unpaid cheque claims allowed
	 */
	public boolean isUnpaidChequeClaimsAllowed() {
		return unpaidChequeClaimsAllowed;
	}

	/**
	 * Sets the unpaid cheque claims allowed.
	 *
	 * @param unpaidChequeClaimsAllowed
	 *            the new unpaid cheque claims allowed
	 */
	public void setUnpaidChequeClaimsAllowed(boolean unpaidChequeClaimsAllowed) {
		this.unpaidChequeClaimsAllowed = unpaidChequeClaimsAllowed;
	}

	/**
	 * Gets the serialversionuid.
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/** The bank society name. */
	private String bankSocietyName;

	/** The bic bank. */
	private String bicBank;

	/** The bic branch. */
	private String bicBranch;

	/** The branch. */
	private String branch;

	/** The building society credits allowed. */
	private boolean buildingSocietyCreditsAllowed;

	/** The country. */
	private String country;

	/** The county. */
	private String county;

	/** The direct debit allowed. */
	private boolean directDebitAllowed;

	/** The direct debit instructions allowed. */
	private boolean directDebitInstructionsAllowed;

	/** The dividend interest payments allowed. */
	private boolean dividendInterestPaymentsAllowed;

	/** The fax. */
	private String fax;

	/** The iban. */
	private String iban;

	/** The life office debits allowed. */
	private boolean lifeOfficeDebitsAllowed;

	/** The messages. */

	/** The name. */
	private String name;

	/** The phone. */
	private String phone;

	/** The post code. */
	private String postCode;

	/** The sort code. */
	private String sortCode;

	/** The sterling bic bank. */
	private String sterlingBicBank;

	/** The sterling bic branch. */
	private String sterlingBicBranch;

	/** The town. */
	private String town;

	/** The unpaid cheque claims allowed. */
	private boolean unpaidChequeClaimsAllowed;

}
