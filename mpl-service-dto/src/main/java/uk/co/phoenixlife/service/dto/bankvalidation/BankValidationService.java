/*
 * BankValidationService.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.bankvalidation;

import org.springframework.stereotype.Service;

import uk.co.phoenixlife.service.dto.bankvalidation.BankValidationRequest;
import uk.co.phoenixlife.service.dto.bankvalidation.BankValidationResponse;

/**
 * The Interface BankValidationService.
 */
@Service
public interface BankValidationService {

	/**
	 * Method to validate bank details.
	 *
	 * @param vocalinkRequest
	 *            the vocalink request
	 * @param xGUID
	 *            - xGUID
	 * @return - {@link BankValidationResponse}
	 * @throws Exception
	 *             - {@link Exception}
	 */
	
	public BankValidationResponse validateBankDetails(final BankValidationRequest vocalinkRequest, final String xGUID)
			throws Exception;
}
