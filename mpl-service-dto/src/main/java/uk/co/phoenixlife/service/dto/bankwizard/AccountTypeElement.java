
package uk.co.phoenixlife.service.dto.bankwizard;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for AccountTypeElement.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="AccountTypeElement">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="C"/>
 *     &lt;enumeration value="A"/>
 *     &lt;enumeration value="M"/>
 *     &lt;enumeration value="B"/>
 *     &lt;enumeration value="S"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 *
 */
@XmlType(name = "AccountTypeElement")
@XmlEnum
public enum AccountTypeElement {

    C, A, M, B, S;

    public String value() {
        return name();
    }

    public static AccountTypeElement fromValue(final String v) {
        return valueOf(v);
    }

}
