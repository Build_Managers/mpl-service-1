
package uk.co.phoenixlife.service.dto.bankwizard;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for Address complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="Address">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AddressLine" type="{urn:experian/BANKWIZARD/soapservice/types}AddressElement" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PostorZipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Address", propOrder = {
        "addressLine",
        "postorZipCode"
})
public class Address {

    @XmlElement(name = "AddressLine", nillable = true)
    protected List<AddressElement> addressLine;
    @XmlElementRef(name = "PostorZipCode", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<String> postorZipCode;

    /**
     * Gets the value of the addressLine property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the addressLine property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getAddressLine().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AddressElement }
     *
     *
     */
    public List<AddressElement> getAddressLine() {
        if (addressLine == null) {
            addressLine = new ArrayList<AddressElement>();
        }
        return addressLine;
    }

    /**
     * Gets the value of the postorZipCode property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link String
     *         }{@code >}
     * 
     */
    public JAXBElement<String> getPostorZipCode() {
        return postorZipCode;
    }

    /**
     * Sets the value of the postorZipCode property.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String
     *            }{@code >}
     * 
     */
    public void setPostorZipCode(final JAXBElement<String> value) {
        postorZipCode = value;
    }

}
