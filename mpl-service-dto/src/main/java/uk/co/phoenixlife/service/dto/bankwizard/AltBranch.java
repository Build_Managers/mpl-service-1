
package uk.co.phoenixlife.service.dto.bankwizard;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for AltBranch complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="AltBranch">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="details" type="{urn:experian/BANKWIZARD/soapservice/types}Details" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="subbranch" type="{urn:experian/BANKWIZARD/soapservice/types}BranchData" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AltBranch", propOrder = {
        "details",
        "subbranch"
})
public class AltBranch {

    protected List<Details> details;
    @XmlElement(nillable = true)
    protected List<BranchData> subbranch;

    /**
     * Gets the value of the details property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the details property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getDetails().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Details }
     *
     *
     */
    public List<Details> getDetails() {
        if (details == null) {
            details = new ArrayList<Details>();
        }
        return details;
    }

    /**
     * Gets the value of the subbranch property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the subbranch property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getSubbranch().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BranchData }
     *
     *
     */
    public List<BranchData> getSubbranch() {
        if (subbranch == null) {
            subbranch = new ArrayList<BranchData>();
        }
        return subbranch;
    }

}
