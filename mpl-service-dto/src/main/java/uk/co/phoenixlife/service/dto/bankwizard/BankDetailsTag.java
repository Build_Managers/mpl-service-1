
package uk.co.phoenixlife.service.dto.bankwizard;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for BankDetailsTag complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="BankDetailsTag">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BBANparam1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BBANparam2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BBANparam3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BBANparam4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BBANparam5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IBAN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BIC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Additonalinfo" type="{urn:experian/BANKWIZARD/soapservice/types}OptionalItem" minOccurs="0"/>
 *         &lt;element name="SubBranches" type="{urn:experian/BANKWIZARD/soapservice/types}OptionalItem" minOccurs="0"/>
 *         &lt;element name="SWIFTData" type="{urn:experian/BANKWIZARD/soapservice/types}OptionalItem" minOccurs="0"/>
 *         &lt;element name="BulkMode" type="{urn:experian/BANKWIZARD/soapservice/types}OptionalItem" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BankDetailsTag", propOrder = {
        "bbaNparam1",
        "bbaNparam2",
        "bbaNparam3",
        "bbaNparam4",
        "bbaNparam5",
        "iban",
        "bic",
        "additonalinfo",
        "subBranches",
        "swiftData",
        "bulkMode"
})
public class BankDetailsTag {

    @XmlElementRef(name = "BBANparam1", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<String> bbaNparam1;
    @XmlElementRef(name = "BBANparam2", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<String> bbaNparam2;
    @XmlElementRef(name = "BBANparam3", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<String> bbaNparam3;
    @XmlElementRef(name = "BBANparam4", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<String> bbaNparam4;
    @XmlElementRef(name = "BBANparam5", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<String> bbaNparam5;
    @XmlElementRef(name = "IBAN", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<String> iban;
    @XmlElementRef(name = "BIC", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<String> bic;
    @XmlElementRef(name = "Additonalinfo", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<OptionalItem> additonalinfo;
    @XmlElementRef(name = "SubBranches", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<OptionalItem> subBranches;
    @XmlElementRef(name = "SWIFTData", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<OptionalItem> swiftData;
    @XmlElementRef(name = "BulkMode", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<OptionalItem> bulkMode;

    /**
     * Gets the value of the bbaNparam1 property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link String
     *         }{@code >}
     * 
     */
    public JAXBElement<String> getBBANparam1() {
        return bbaNparam1;
    }

    /**
     * Sets the value of the bbaNparam1 property.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String
     *            }{@code >}
     * 
     */
    public void setBBANparam1(final JAXBElement<String> value) {
        bbaNparam1 = value;
    }

    /**
     * Gets the value of the bbaNparam2 property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link String
     *         }{@code >}
     * 
     */
    public JAXBElement<String> getBBANparam2() {
        return bbaNparam2;
    }

    /**
     * Sets the value of the bbaNparam2 property.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String
     *            }{@code >}
     * 
     */
    public void setBBANparam2(final JAXBElement<String> value) {
        bbaNparam2 = value;
    }

    /**
     * Gets the value of the bbaNparam3 property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link String
     *         }{@code >}
     * 
     */
    public JAXBElement<String> getBBANparam3() {
        return bbaNparam3;
    }

    /**
     * Sets the value of the bbaNparam3 property.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String
     *            }{@code >}
     * 
     */
    public void setBBANparam3(final JAXBElement<String> value) {
        bbaNparam3 = value;
    }

    /**
     * Gets the value of the bbaNparam4 property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link String
     *         }{@code >}
     * 
     */
    public JAXBElement<String> getBBANparam4() {
        return bbaNparam4;
    }

    /**
     * Sets the value of the bbaNparam4 property.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String
     *            }{@code >}
     * 
     */
    public void setBBANparam4(final JAXBElement<String> value) {
        bbaNparam4 = value;
    }

    /**
     * Gets the value of the bbaNparam5 property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link String
     *         }{@code >}
     * 
     */
    public JAXBElement<String> getBBANparam5() {
        return bbaNparam5;
    }

    /**
     * Sets the value of the bbaNparam5 property.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String
     *            }{@code >}
     * 
     */
    public void setBBANparam5(final JAXBElement<String> value) {
        bbaNparam5 = value;
    }

    /**
     * Gets the value of the iban property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link String
     *         }{@code >}
     * 
     */
    public JAXBElement<String> getIBAN() {
        return iban;
    }

    /**
     * Sets the value of the iban property.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String
     *            }{@code >}
     * 
     */
    public void setIBAN(final JAXBElement<String> value) {
        iban = value;
    }

    /**
     * Gets the value of the bic property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link String
     *         }{@code >}
     * 
     */
    public JAXBElement<String> getBIC() {
        return bic;
    }

    /**
     * Sets the value of the bic property.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String
     *            }{@code >}
     * 
     */
    public void setBIC(final JAXBElement<String> value) {
        bic = value;
    }

    /**
     * Gets the value of the additonalinfo property.
     *
     * @return possible object is {@link JAXBElement
     *         }{@code <}{@link OptionalItem }{@code >}
     * 
     */
    public JAXBElement<OptionalItem> getAdditonalinfo() {
        return additonalinfo;
    }

    /**
     * Sets the value of the additonalinfo property.
     *
     * @param value
     *            allowed object is {@link JAXBElement
     *            }{@code <}{@link OptionalItem }{@code >}
     * 
     */
    public void setAdditonalinfo(final JAXBElement<OptionalItem> value) {
        additonalinfo = value;
    }

    /**
     * Gets the value of the subBranches property.
     *
     * @return possible object is {@link JAXBElement
     *         }{@code <}{@link OptionalItem }{@code >}
     * 
     */
    public JAXBElement<OptionalItem> getSubBranches() {
        return subBranches;
    }

    /**
     * Sets the value of the subBranches property.
     *
     * @param value
     *            allowed object is {@link JAXBElement
     *            }{@code <}{@link OptionalItem }{@code >}
     * 
     */
    public void setSubBranches(final JAXBElement<OptionalItem> value) {
        subBranches = value;
    }

    /**
     * Gets the value of the swiftData property.
     *
     * @return possible object is {@link JAXBElement
     *         }{@code <}{@link OptionalItem }{@code >}
     * 
     */
    public JAXBElement<OptionalItem> getSWIFTData() {
        return swiftData;
    }

    /**
     * Sets the value of the swiftData property.
     *
     * @param value
     *            allowed object is {@link JAXBElement
     *            }{@code <}{@link OptionalItem }{@code >}
     * 
     */
    public void setSWIFTData(final JAXBElement<OptionalItem> value) {
        swiftData = value;
    }

    /**
     * Gets the value of the bulkMode property.
     *
     * @return possible object is {@link JAXBElement
     *         }{@code <}{@link OptionalItem }{@code >}
     * 
     */
    public JAXBElement<OptionalItem> getBulkMode() {
        return bulkMode;
    }

    /**
     * Sets the value of the bulkMode property.
     *
     * @param value
     *            allowed object is {@link JAXBElement
     *            }{@code <}{@link OptionalItem }{@code >}
     * 
     */
    public void setBulkMode(final JAXBElement<OptionalItem> value) {
        bulkMode = value;
    }

}
