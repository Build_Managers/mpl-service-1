
package uk.co.phoenixlife.service.dto.bankwizard;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for BranchData complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="BranchData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="details" type="{urn:experian/BANKWIZARD/soapservice/types}Details" maxOccurs="unbounded"/>
 *         &lt;element name="swift" type="{urn:experian/BANKWIZARD/soapservice/types}Details" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BranchData", propOrder = {
        "details",
        "swift"
})
public class BranchData {

    @XmlElement(required = true)
    protected List<Details> details;
    @XmlElement(nillable = true)
    protected List<Details> swift;

    /**
     * Gets the value of the details property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the details property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getDetails().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Details }
     *
     *
     */
    public List<Details> getDetails() {
        if (details == null) {
            details = new ArrayList<Details>();
        }
        return details;
    }

    /**
     * Gets the value of the swift property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the swift property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getSwift().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Details }
     *
     *
     */
    public List<Details> getSwift() {
        if (swift == null) {
            swift = new ArrayList<Details>();
        }
        return swift;
    }

}
