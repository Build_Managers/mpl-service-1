
package uk.co.phoenixlife.service.dto.bankwizard;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CheckTypeElement.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="CheckTypeElement">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="VALIDATE_VERIFY"/>
 *     &lt;enumeration value="VERIFY"/>
 *     &lt;enumeration value="FORCE_VERIFY"/>
 *     &lt;enumeration value="VALIDATE_ONLY"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 *
 */
@XmlType(name = "CheckTypeElement")
@XmlEnum
public enum CheckTypeElement {

    VALIDATE_VERIFY, VERIFY, FORCE_VERIFY, VALIDATE_ONLY;

    public String value() {
        return name();
    }

    public static CheckTypeElement fromValue(final String v) {
        return valueOf(v);
    }

}
