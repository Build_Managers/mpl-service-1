
package uk.co.phoenixlife.service.dto.bankwizard;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ClientElement complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="ClientElement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BranchNumber" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ClientIPAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClientMACAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AdditionalItem" type="{urn:experian/BANKWIZARD/soapservice/types}DataElement" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClientElement", propOrder = {
        "branchNumber",
        "clientIPAddress",
        "clientMACAddress",
        "additionalItem"
})
public class ClientElement {

    @XmlElementRef(name = "BranchNumber", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<BigInteger> branchNumber;
    @XmlElementRef(name = "ClientIPAddress", namespace = "urn:experian/BANKWIZARD/soapservice/types",
            type = JAXBElement.class, required = false)
    protected JAXBElement<String> clientIPAddress;
    @XmlElementRef(name = "ClientMACAddress", namespace = "urn:experian/BANKWIZARD/soapservice/types",
            type = JAXBElement.class, required = false)
    protected JAXBElement<String> clientMACAddress;
    @XmlElement(name = "AdditionalItem", nillable = true)
    protected List<DataElement> additionalItem;

    /**
     * Gets the value of the branchNumber property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link BigInteger
     *         }{@code >}
     * 
     */
    public JAXBElement<BigInteger> getBranchNumber() {
        return branchNumber;
    }

    /**
     * Sets the value of the branchNumber property.
     *
     * @param value
     *            allowed object is {@link JAXBElement
     *            }{@code <}{@link BigInteger }{@code >}
     * 
     */
    public void setBranchNumber(final JAXBElement<BigInteger> value) {
        branchNumber = value;
    }

    /**
     * Gets the value of the clientIPAddress property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link String
     *         }{@code >}
     * 
     */
    public JAXBElement<String> getClientIPAddress() {
        return clientIPAddress;
    }

    /**
     * Sets the value of the clientIPAddress property.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String
     *            }{@code >}
     * 
     */
    public void setClientIPAddress(final JAXBElement<String> value) {
        clientIPAddress = value;
    }

    /**
     * Gets the value of the clientMACAddress property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link String
     *         }{@code >}
     * 
     */
    public JAXBElement<String> getClientMACAddress() {
        return clientMACAddress;
    }

    /**
     * Sets the value of the clientMACAddress property.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String
     *            }{@code >}
     * 
     */
    public void setClientMACAddress(final JAXBElement<String> value) {
        clientMACAddress = value;
    }

    /**
     * Gets the value of the additionalItem property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the additionalItem property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getAdditionalItem().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataElement }
     *
     *
     */
    public List<DataElement> getAdditionalItem() {
        if (additionalItem == null) {
            additionalItem = new ArrayList<DataElement>();
        }
        return additionalItem;
    }

}
