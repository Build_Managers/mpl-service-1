
package uk.co.phoenixlife.service.dto.bankwizard;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for Conditions complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="Conditions">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FatalErrors" type="{urn:experian/BANKWIZARD/soapservice/types}Details" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Error" type="{urn:experian/BANKWIZARD/soapservice/types}Details" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Warnings" type="{urn:experian/BANKWIZARD/soapservice/types}Details" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Info" type="{urn:experian/BANKWIZARD/soapservice/types}Details" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DataIntegrityErrors" type="{urn:experian/BANKWIZARD/soapservice/types}Details" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AbsoluteErrors" type="{urn:experian/BANKWIZARD/soapservice/types}Details" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Conditions", propOrder = {
        "fatalErrors",
        "error",
        "warnings",
        "info",
        "dataIntegrityErrors",
        "absoluteErrors"
})
public class Conditions {

    @XmlElement(name = "FatalErrors", nillable = true)
    protected List<Details> fatalErrors;
    @XmlElement(name = "Error", nillable = true)
    protected List<Details> error;
    @XmlElement(name = "Warnings", nillable = true)
    protected List<Details> warnings;
    @XmlElement(name = "Info", nillable = true)
    protected List<Details> info;
    @XmlElement(name = "DataIntegrityErrors", nillable = true)
    protected List<Details> dataIntegrityErrors;
    @XmlElement(name = "AbsoluteErrors", nillable = true)
    protected List<Details> absoluteErrors;

    /**
     * Gets the value of the fatalErrors property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the fatalErrors property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getFatalErrors().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Details }
     *
     *
     */
    public List<Details> getFatalErrors() {
        if (fatalErrors == null) {
            fatalErrors = new ArrayList<Details>();
        }
        return fatalErrors;
    }

    /**
     * Gets the value of the error property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the error property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getError().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Details }
     *
     *
     */
    public List<Details> getError() {
        if (error == null) {
            error = new ArrayList<Details>();
        }
        return error;
    }

    /**
     * Gets the value of the warnings property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the warnings property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getWarnings().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Details }
     *
     *
     */
    public List<Details> getWarnings() {
        if (warnings == null) {
            warnings = new ArrayList<Details>();
        }
        return warnings;
    }

    /**
     * Gets the value of the info property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the info property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getInfo().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Details }
     *
     *
     */
    public List<Details> getInfo() {
        if (info == null) {
            info = new ArrayList<Details>();
        }
        return info;
    }

    /**
     * Gets the value of the dataIntegrityErrors property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the dataIntegrityErrors property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getDataIntegrityErrors().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Details }
     *
     *
     */
    public List<Details> getDataIntegrityErrors() {
        if (dataIntegrityErrors == null) {
            dataIntegrityErrors = new ArrayList<Details>();
        }
        return dataIntegrityErrors;
    }

    /**
     * Gets the value of the absoluteErrors property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the absoluteErrors property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getAbsoluteErrors().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Details }
     *
     *
     */
    public List<Details> getAbsoluteErrors() {
        if (absoluteErrors == null) {
            absoluteErrors = new ArrayList<Details>();
        }
        return absoluteErrors;
    }

}
