
package uk.co.phoenixlife.service.dto.bankwizard;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ContextCheckElement.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="ContextCheckElement">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="DD"/>
 *     &lt;enumeration value="DC"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 *
 */
@XmlType(name = "ContextCheckElement")
@XmlEnum
public enum ContextCheckElement {

    DD, DC;

    public String value() {
        return name();
    }

    public static ContextCheckElement fromValue(final String v) {
        return valueOf(v);
    }

}
