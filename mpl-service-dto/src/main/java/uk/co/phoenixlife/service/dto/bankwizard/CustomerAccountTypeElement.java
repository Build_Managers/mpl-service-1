
package uk.co.phoenixlife.service.dto.bankwizard;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CustomerAccountTypeElement.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="CustomerAccountTypeElement">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="I"/>
 *     &lt;enumeration value="C"/>
 *     &lt;enumeration value="W"/>
 *     &lt;enumeration value="X"/>
 *     &lt;enumeration value="P"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 *
 */
@XmlType(name = "CustomerAccountTypeElement")
@XmlEnum
public enum CustomerAccountTypeElement {

    I, C, W, X, P;

    public String value() {
        return name();
    }

    public static CustomerAccountTypeElement fromValue(final String v) {
        return valueOf(v);
    }

}
