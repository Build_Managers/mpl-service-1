
package uk.co.phoenixlife.service.dto.bankwizard;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for DataValidationRequest complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="DataValidationRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClientInformation" type="{urn:experian/BANKWIZARD/soapservice/types}GenericHeader"/>
 *         &lt;element name="ClientDetails" type="{urn:experian/BANKWIZARD/soapservice/types}ClientElement"/>
 *         &lt;element name="ServerDetails" type="{urn:experian/BANKWIZARD/soapservice/types}ServiceDetails"/>
 *         &lt;element name="BankDetails" type="{urn:experian/BANKWIZARD/soapservice/types}BankDetailsTag"/>
 *         &lt;element name="PersonalDetails" type="{urn:experian/BANKWIZARD/soapservice/types}PersonalDetailsTag" minOccurs="0"/>
 *         &lt;element name="AdditionalItem" type="{urn:experian/BANKWIZARD/soapservice/types}DataElement" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataValidationRequest", propOrder = {
        "clientInformation",
        "clientDetails",
        "serverDetails",
        "bankDetails",
        "personalDetails",
        "additionalItem"
})
@XmlRootElement(name = "DataValidationRequest")
public class DataValidationRequest {

    @XmlElement(name = "ClientInformation", required = true, nillable = true)
    protected GenericHeader clientInformation;
    @XmlElement(name = "ClientDetails", required = true, nillable = true)
    protected ClientElement clientDetails;
    @XmlElement(name = "ServerDetails", required = true, nillable = true)
    protected ServiceDetails serverDetails;
    @XmlElement(name = "BankDetails", required = true, nillable = true)
    protected BankDetailsTag bankDetails;
    @XmlElementRef(name = "PersonalDetails", namespace = "urn:experian/BANKWIZARD/soapservice/types",
            type = JAXBElement.class, required = false)
    protected JAXBElement<PersonalDetailsTag> personalDetails;
    @XmlElement(name = "AdditionalItem", nillable = true)
    protected List<DataElement> additionalItem;

    /**
     * Gets the value of the clientInformation property.
     *
     * @return possible object is {@link GenericHeader }
     *
     */
    public GenericHeader getClientInformation() {
        return clientInformation;
    }

    /**
     * Sets the value of the clientInformation property.
     *
     * @param value
     *            allowed object is {@link GenericHeader }
     *
     */
    public void setClientInformation(final GenericHeader value) {
        clientInformation = value;
    }

    /**
     * Gets the value of the clientDetails property.
     *
     * @return possible object is {@link ClientElement }
     *
     */
    public ClientElement getClientDetails() {
        return clientDetails;
    }

    /**
     * Sets the value of the clientDetails property.
     *
     * @param value
     *            allowed object is {@link ClientElement }
     *
     */
    public void setClientDetails(final ClientElement value) {
        clientDetails = value;
    }

    /**
     * Gets the value of the serverDetails property.
     *
     * @return possible object is {@link ServiceDetails }
     *
     */
    public ServiceDetails getServerDetails() {
        return serverDetails;
    }

    /**
     * Sets the value of the serverDetails property.
     *
     * @param value
     *            allowed object is {@link ServiceDetails }
     *
     */
    public void setServerDetails(final ServiceDetails value) {
        serverDetails = value;
    }

    /**
     * Gets the value of the bankDetails property.
     *
     * @return possible object is {@link BankDetailsTag }
     *
     */
    public BankDetailsTag getBankDetails() {
        return bankDetails;
    }

    /**
     * Sets the value of the bankDetails property.
     *
     * @param value
     *            allowed object is {@link BankDetailsTag }
     *
     */
    public void setBankDetails(final BankDetailsTag value) {
        bankDetails = value;
    }

    /**
     * Gets the value of the personalDetails property.
     *
     * @return possible object is {@link JAXBElement
     *         }{@code <}{@link PersonalDetailsTag }{@code >}
     *
     */
    public JAXBElement<PersonalDetailsTag> getPersonalDetails() {
        return personalDetails;
    }

    /**
     * Sets the value of the personalDetails property.
     *
     * @param value
     *            allowed object is {@link JAXBElement
     *            }{@code <}{@link PersonalDetailsTag }{@code >}
     *
     */
    public void setPersonalDetails(final JAXBElement<PersonalDetailsTag> value) {
        personalDetails = value;
    }

    /**
     * Gets the value of the additionalItem property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the additionalItem property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getAdditionalItem().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataElement }
     *
     *
     */
    public List<DataElement> getAdditionalItem() {
        if (additionalItem == null) {
            additionalItem = new ArrayList<DataElement>();
        }
        return additionalItem;
    }

}
