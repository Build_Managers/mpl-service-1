
package uk.co.phoenixlife.service.dto.bankwizard;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for DataValidationResponse complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="DataValidationResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InputBankDetails" type="{urn:experian/BANKWIZARD/soapservice/types}BankDetailsTag" minOccurs="0"/>
 *         &lt;element name="ProcessedBankDetails" type="{urn:experian/BANKWIZARD/soapservice/types}BankDetailsTag" minOccurs="0"/>
 *         &lt;element name="branchData" type="{urn:experian/BANKWIZARD/soapservice/types}BranchData" minOccurs="0"/>
 *         &lt;element name="alternateBranch" type="{urn:experian/BANKWIZARD/soapservice/types}AltBranch" minOccurs="0"/>
 *         &lt;element name="additionalInfo" type="{urn:experian/BANKWIZARD/soapservice/types}Details" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="conditions" type="{urn:experian/BANKWIZARD/soapservice/types}Conditions" minOccurs="0"/>
 *         &lt;element name="ValidatedBankDetails" type="{urn:experian/BANKWIZARD/soapservice/types}DataElement" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PersonalDetails" type="{urn:experian/BANKWIZARD/soapservice/types}PersonalDetailsResponseElement" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AdditionalItem" type="{urn:experian/BANKWIZARD/soapservice/types}DataElement" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataValidationResponse", propOrder = {
        "inputBankDetails",
        "processedBankDetails",
        "branchData",
        "alternateBranch",
        "additionalInfo",
        "conditions",
        "validatedBankDetails",
        "personalDetails",
        "additionalItem"
})
@XmlRootElement(name = "DataValidationResponse")
public class DataValidationResponse {

    @XmlElementRef(name = "InputBankDetails", namespace = "urn:experian/BANKWIZARD/soapservice/types",
            type = JAXBElement.class, required = false)
    protected JAXBElement<BankDetailsTag> inputBankDetails;
    @XmlElementRef(name = "ProcessedBankDetails", namespace = "urn:experian/BANKWIZARD/soapservice/types",
            type = JAXBElement.class, required = false)
    protected JAXBElement<BankDetailsTag> processedBankDetails;
    @XmlElementRef(name = "branchData", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<BranchData> branchData;
    @XmlElementRef(name = "alternateBranch", namespace = "urn:experian/BANKWIZARD/soapservice/types",
            type = JAXBElement.class, required = false)
    protected JAXBElement<AltBranch> alternateBranch;
    @XmlElement(nillable = true)
    protected List<Details> additionalInfo;
    @XmlElementRef(name = "conditions", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<Conditions> conditions;
    @XmlElement(name = "ValidatedBankDetails", nillable = true)
    protected List<DataElement> validatedBankDetails;
    @XmlElement(name = "PersonalDetails", nillable = true)
    protected List<PersonalDetailsResponseElement> personalDetails;
    @XmlElement(name = "AdditionalItem", nillable = true)
    protected List<DataElement> additionalItem;

    /**
     * Gets the value of the inputBankDetails property.
     *
     * @return possible object is {@link JAXBElement
     *         }{@code <}{@link BankDetailsTag }{@code >}
     *
     */
    public JAXBElement<BankDetailsTag> getInputBankDetails() {
        return inputBankDetails;
    }

    /**
     * Sets the value of the inputBankDetails property.
     *
     * @param value
     *            allowed object is {@link JAXBElement
     *            }{@code <}{@link BankDetailsTag }{@code >}
     *
     */
    public void setInputBankDetails(final JAXBElement<BankDetailsTag> value) {
        inputBankDetails = value;
    }

    /**
     * Gets the value of the processedBankDetails property.
     *
     * @return possible object is {@link JAXBElement
     *         }{@code <}{@link BankDetailsTag }{@code >}
     *
     */
    public JAXBElement<BankDetailsTag> getProcessedBankDetails() {
        return processedBankDetails;
    }

    /**
     * Sets the value of the processedBankDetails property.
     *
     * @param value
     *            allowed object is {@link JAXBElement
     *            }{@code <}{@link BankDetailsTag }{@code >}
     *
     */
    public void setProcessedBankDetails(final JAXBElement<BankDetailsTag> value) {
        processedBankDetails = value;
    }

    /**
     * Gets the value of the branchData property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link BranchData
     *         }{@code >}
     *
     */
    public JAXBElement<BranchData> getBranchData() {
        return branchData;
    }

    /**
     * Sets the value of the branchData property.
     *
     * @param value
     *            allowed object is {@link JAXBElement
     *            }{@code <}{@link BranchData }{@code >}
     *
     */
    public void setBranchData(final JAXBElement<BranchData> value) {
        branchData = value;
    }

    /**
     * Gets the value of the alternateBranch property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link AltBranch
     *         }{@code >}
     *
     */
    public JAXBElement<AltBranch> getAlternateBranch() {
        return alternateBranch;
    }

    /**
     * Sets the value of the alternateBranch property.
     *
     * @param value
     *            allowed object is {@link JAXBElement
     *            }{@code <}{@link AltBranch }{@code >}
     *
     */
    public void setAlternateBranch(final JAXBElement<AltBranch> value) {
        alternateBranch = value;
    }

    /**
     * Gets the value of the additionalInfo property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the additionalInfo property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getAdditionalInfo().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Details }
     *
     *
     */
    public List<Details> getAdditionalInfo() {
        if (additionalInfo == null) {
            additionalInfo = new ArrayList<Details>();
        }
        return additionalInfo;
    }

    /**
     * Gets the value of the conditions property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link Conditions
     *         }{@code >}
     *
     */
    public JAXBElement<Conditions> getConditions() {
        return conditions;
    }

    /**
     * Sets the value of the conditions property.
     *
     * @param value
     *            allowed object is {@link JAXBElement
     *            }{@code <}{@link Conditions }{@code >}
     *
     */
    public void setConditions(final JAXBElement<Conditions> value) {
        conditions = value;
    }

    /**
     * Gets the value of the validatedBankDetails property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the validatedBankDetails property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getValidatedBankDetails().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataElement }
     *
     *
     */
    public List<DataElement> getValidatedBankDetails() {
        if (validatedBankDetails == null) {
            validatedBankDetails = new ArrayList<DataElement>();
        }
        return validatedBankDetails;
    }

    /**
     * Gets the value of the personalDetails property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the personalDetails property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getPersonalDetails().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PersonalDetailsResponseElement }
     *
     *
     */
    public List<PersonalDetailsResponseElement> getPersonalDetails() {
        if (personalDetails == null) {
            personalDetails = new ArrayList<PersonalDetailsResponseElement>();
        }
        return personalDetails;
    }

    /**
     * Gets the value of the additionalItem property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the additionalItem property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getAdditionalItem().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataElement }
     *
     *
     */
    public List<DataElement> getAdditionalItem() {
        if (additionalItem == null) {
            additionalItem = new ArrayList<DataElement>();
        }
        return additionalItem;
    }

}
