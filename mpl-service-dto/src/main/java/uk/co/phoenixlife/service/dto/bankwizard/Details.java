
package uk.co.phoenixlife.service.dto.bankwizard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for Details complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="Details">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fieldname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fieldvalue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Details", propOrder = {
        "fieldname",
        "fieldvalue"
})
public class Details {

    @XmlElement(required = true)
    protected String fieldname;
    @XmlElement(required = true)
    protected String fieldvalue;

    /**
     * Gets the value of the fieldname property.
     *
     * @return possible object is {@link String }
     * 
     */
    public String getFieldname() {
        return fieldname;
    }

    /**
     * Sets the value of the fieldname property.
     *
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setFieldname(final String value) {
        fieldname = value;
    }

    /**
     * Gets the value of the fieldvalue property.
     *
     * @return possible object is {@link String }
     * 
     */
    public String getFieldvalue() {
        return fieldvalue;
    }

    /**
     * Sets the value of the fieldvalue property.
     *
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setFieldvalue(final String value) {
        fieldvalue = value;
    }

}
