
package uk.co.phoenixlife.service.dto.bankwizard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for GenericHeader complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="GenericHeader">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UserID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WSDLVersion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenericHeader", propOrder = {
        "userID",
        "wsdlVersion"
})
public class GenericHeader {

    @XmlElement(name = "UserID", required = true, nillable = true)
    protected String userID;
    @XmlElement(name = "WSDLVersion", required = true)
    protected String wsdlVersion;

    /**
     * Gets the value of the userID property.
     *
     * @return possible object is {@link String }
     * 
     */
    public String getUserID() {
        return userID;
    }

    /**
     * Sets the value of the userID property.
     *
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setUserID(final String value) {
        userID = value;
    }

    /**
     * Gets the value of the wsdlVersion property.
     *
     * @return possible object is {@link String }
     * 
     */
    public String getWSDLVersion() {
        return wsdlVersion;
    }

    /**
     * Sets the value of the wsdlVersion property.
     *
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setWSDLVersion(final String value) {
        wsdlVersion = value;
    }

}
