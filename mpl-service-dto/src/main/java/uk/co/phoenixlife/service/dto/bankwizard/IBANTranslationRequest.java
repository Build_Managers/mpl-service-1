
package uk.co.phoenixlife.service.dto.bankwizard;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for IBANTranslationRequest complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="IBANTranslationRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClientInformation" type="{urn:experian/BANKWIZARD/soapservice/types}GenericHeader"/>
 *         &lt;element name="IBAN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AdditionalItem" type="{urn:experian/BANKWIZARD/soapservice/types}DataElement" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IBANTranslationRequest", propOrder = {
        "clientInformation",
        "iban",
        "additionalItem"
})
public class IBANTranslationRequest {

    @XmlElement(name = "ClientInformation", required = true, nillable = true)
    protected GenericHeader clientInformation;
    @XmlElement(name = "IBAN", required = true)
    protected String iban;
    @XmlElement(name = "AdditionalItem", nillable = true)
    protected List<DataElement> additionalItem;

    /**
     * Gets the value of the clientInformation property.
     *
     * @return possible object is {@link GenericHeader }
     * 
     */
    public GenericHeader getClientInformation() {
        return clientInformation;
    }

    /**
     * Sets the value of the clientInformation property.
     *
     * @param value
     *            allowed object is {@link GenericHeader }
     * 
     */
    public void setClientInformation(final GenericHeader value) {
        clientInformation = value;
    }

    /**
     * Gets the value of the iban property.
     *
     * @return possible object is {@link String }
     * 
     */
    public String getIBAN() {
        return iban;
    }

    /**
     * Sets the value of the iban property.
     *
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setIBAN(final String value) {
        iban = value;
    }

    /**
     * Gets the value of the additionalItem property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the additionalItem property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getAdditionalItem().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataElement }
     *
     *
     */
    public List<DataElement> getAdditionalItem() {
        if (additionalItem == null) {
            additionalItem = new ArrayList<DataElement>();
        }
        return additionalItem;
    }

}
