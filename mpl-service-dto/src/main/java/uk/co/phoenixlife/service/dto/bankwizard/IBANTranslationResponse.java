
package uk.co.phoenixlife.service.dto.bankwizard;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for IBANTranslationResponse complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="IBANTranslationResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BBAN" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="conditions" type="{urn:experian/BANKWIZARD/soapservice/types}Conditions" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IBANTranslationResponse", propOrder = {
        "bban",
        "conditions"
})
public class IBANTranslationResponse {

    @XmlElement(name = "BBAN", nillable = true)
    protected List<String> bban;
    @XmlElementRef(name = "conditions", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<Conditions> conditions;

    /**
     * Gets the value of the bban property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the bban property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getBBAN().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link String }
     *
     *
     */
    public List<String> getBBAN() {
        if (bban == null) {
            bban = new ArrayList<String>();
        }
        return bban;
    }

    /**
     * Gets the value of the conditions property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link Conditions
     *         }{@code >}
     * 
     */
    public JAXBElement<Conditions> getConditions() {
        return conditions;
    }

    /**
     * Sets the value of the conditions property.
     *
     * @param value
     *            allowed object is {@link JAXBElement
     *            }{@code <}{@link Conditions }{@code >}
     * 
     */
    public void setConditions(final JAXBElement<Conditions> value) {
        conditions = value;
    }

}
