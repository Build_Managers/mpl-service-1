
package uk.co.phoenixlife.service.dto.bankwizard;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for MetaDataGroup complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="MetaDataGroup">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GroupID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Size" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="metaItem" type="{urn:experian/BANKWIZARD/soapservice/types}Details" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetaDataGroup", propOrder = {
        "groupID",
        "groupName",
        "name",
        "size",
        "id",
        "metaItem"
})
public class MetaDataGroup {

    @XmlElementRef(name = "GroupID", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<Integer> groupID;
    @XmlElementRef(name = "GroupName", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<String> groupName;
    @XmlElementRef(name = "Name", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<String> name;
    @XmlElementRef(name = "Size", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<Integer> size;
    @XmlElementRef(name = "ID", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<String> id;
    @XmlElement(nillable = true)
    protected List<Details> metaItem;

    /**
     * Gets the value of the groupID property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link Integer
     *         }{@code >}
     * 
     */
    public JAXBElement<Integer> getGroupID() {
        return groupID;
    }

    /**
     * Sets the value of the groupID property.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link Integer
     *            }{@code >}
     * 
     */
    public void setGroupID(final JAXBElement<Integer> value) {
        groupID = value;
    }

    /**
     * Gets the value of the groupName property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link String
     *         }{@code >}
     * 
     */
    public JAXBElement<String> getGroupName() {
        return groupName;
    }

    /**
     * Sets the value of the groupName property.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String
     *            }{@code >}
     * 
     */
    public void setGroupName(final JAXBElement<String> value) {
        groupName = value;
    }

    /**
     * Gets the value of the name property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link String
     *         }{@code >}
     * 
     */
    public JAXBElement<String> getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String
     *            }{@code >}
     * 
     */
    public void setName(final JAXBElement<String> value) {
        name = value;
    }

    /**
     * Gets the value of the size property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link Integer
     *         }{@code >}
     * 
     */
    public JAXBElement<Integer> getSize() {
        return size;
    }

    /**
     * Sets the value of the size property.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link Integer
     *            }{@code >}
     * 
     */
    public void setSize(final JAXBElement<Integer> value) {
        size = value;
    }

    /**
     * Gets the value of the id property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link String
     *         }{@code >}
     * 
     */
    public JAXBElement<String> getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String
     *            }{@code >}
     * 
     */
    public void setID(final JAXBElement<String> value) {
        id = value;
    }

    /**
     * Gets the value of the metaItem property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the metaItem property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getMetaItem().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Details }
     *
     *
     */
    public List<Details> getMetaItem() {
        if (metaItem == null) {
            metaItem = new ArrayList<Details>();
        }
        return metaItem;
    }

}
