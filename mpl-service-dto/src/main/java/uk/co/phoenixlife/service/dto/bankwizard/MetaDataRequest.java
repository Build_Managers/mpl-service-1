
package uk.co.phoenixlife.service.dto.bankwizard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for MetaDataRequest complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="MetaDataRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClientInformation" type="{urn:experian/BANKWIZARD/soapservice/types}GenericHeader"/>
 *         &lt;element name="ISOCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetaDataRequest", propOrder = {
        "clientInformation",
        "isoCountryCode"
})
public class MetaDataRequest {

    @XmlElement(name = "ClientInformation", required = true, nillable = true)
    protected GenericHeader clientInformation;
    @XmlElement(name = "ISOCountryCode", required = true)
    protected String isoCountryCode;

    /**
     * Gets the value of the clientInformation property.
     *
     * @return possible object is {@link GenericHeader }
     * 
     */
    public GenericHeader getClientInformation() {
        return clientInformation;
    }

    /**
     * Sets the value of the clientInformation property.
     *
     * @param value
     *            allowed object is {@link GenericHeader }
     * 
     */
    public void setClientInformation(final GenericHeader value) {
        clientInformation = value;
    }

    /**
     * Gets the value of the isoCountryCode property.
     *
     * @return possible object is {@link String }
     * 
     */
    public String getISOCountryCode() {
        return isoCountryCode;
    }

    /**
     * Sets the value of the isoCountryCode property.
     *
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setISOCountryCode(final String value) {
        isoCountryCode = value;
    }

}
