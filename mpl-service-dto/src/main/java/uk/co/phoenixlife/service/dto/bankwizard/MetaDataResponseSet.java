
package uk.co.phoenixlife.service.dto.bankwizard;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for MetaDataResponseSet complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="MetaDataResponseSet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="metaDataDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="metaDataID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ISOCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InputData" type="{urn:experian/BANKWIZARD/soapservice/types}MetaDataGroup" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="OutputData" type="{urn:experian/BANKWIZARD/soapservice/types}MetaDataGroup" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="conditions" type="{urn:experian/BANKWIZARD/soapservice/types}Conditions" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetaDataResponseSet", propOrder = {
        "metaDataDescription",
        "metaDataID",
        "isoCountryCode",
        "inputData",
        "outputData",
        "conditions"
})
public class MetaDataResponseSet {

    @XmlElementRef(name = "metaDataDescription", namespace = "urn:experian/BANKWIZARD/soapservice/types",
            type = JAXBElement.class, required = false)
    protected JAXBElement<String> metaDataDescription;
    @XmlElementRef(name = "metaDataID", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<Integer> metaDataID;
    @XmlElementRef(name = "ISOCountryCode", namespace = "urn:experian/BANKWIZARD/soapservice/types",
            type = JAXBElement.class, required = false)
    protected JAXBElement<String> isoCountryCode;
    @XmlElement(name = "InputData", nillable = true)
    protected List<MetaDataGroup> inputData;
    @XmlElement(name = "OutputData", nillable = true)
    protected List<MetaDataGroup> outputData;
    @XmlElementRef(name = "conditions", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<Conditions> conditions;
    @XmlAttribute(name = "id")
    protected BigInteger id;

    /**
     * Gets the value of the metaDataDescription property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link String
     *         }{@code >}
     * 
     */
    public JAXBElement<String> getMetaDataDescription() {
        return metaDataDescription;
    }

    /**
     * Sets the value of the metaDataDescription property.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String
     *            }{@code >}
     * 
     */
    public void setMetaDataDescription(final JAXBElement<String> value) {
        metaDataDescription = value;
    }

    /**
     * Gets the value of the metaDataID property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link Integer
     *         }{@code >}
     * 
     */
    public JAXBElement<Integer> getMetaDataID() {
        return metaDataID;
    }

    /**
     * Sets the value of the metaDataID property.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link Integer
     *            }{@code >}
     * 
     */
    public void setMetaDataID(final JAXBElement<Integer> value) {
        metaDataID = value;
    }

    /**
     * Gets the value of the isoCountryCode property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link String
     *         }{@code >}
     * 
     */
    public JAXBElement<String> getISOCountryCode() {
        return isoCountryCode;
    }

    /**
     * Sets the value of the isoCountryCode property.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String
     *            }{@code >}
     * 
     */
    public void setISOCountryCode(final JAXBElement<String> value) {
        isoCountryCode = value;
    }

    /**
     * Gets the value of the inputData property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the inputData property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getInputData().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MetaDataGroup }
     *
     *
     */
    public List<MetaDataGroup> getInputData() {
        if (inputData == null) {
            inputData = new ArrayList<MetaDataGroup>();
        }
        return inputData;
    }

    /**
     * Gets the value of the outputData property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the outputData property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getOutputData().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MetaDataGroup }
     *
     *
     */
    public List<MetaDataGroup> getOutputData() {
        if (outputData == null) {
            outputData = new ArrayList<MetaDataGroup>();
        }
        return outputData;
    }

    /**
     * Gets the value of the conditions property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link Conditions
     *         }{@code >}
     * 
     */
    public JAXBElement<Conditions> getConditions() {
        return conditions;
    }

    /**
     * Sets the value of the conditions property.
     *
     * @param value
     *            allowed object is {@link JAXBElement
     *            }{@code <}{@link Conditions }{@code >}
     * 
     */
    public void setConditions(final JAXBElement<Conditions> value) {
        conditions = value;
    }

    /**
     * Gets the value of the id property.
     *
     * @return possible object is {@link BigInteger }
     * 
     */
    public BigInteger getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value
     *            allowed object is {@link BigInteger }
     * 
     */
    public void setId(final BigInteger value) {
        id = value;
    }

}
