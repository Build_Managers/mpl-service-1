
package uk.co.phoenixlife.service.dto.bankwizard;

import java.math.BigInteger;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the experian.bankwizard.soapservice.types
 * package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SearchRequest_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "SearchRequest");
    private final static QName _VersionResponse_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "VersionResponse");
    private final static QName _DataValidationRequest_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "DataValidationRequest");
    private final static QName _MetaDataRequest_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "MetaDataRequest");
    private final static QName _IBANTranslationResponse_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "IBANTranslationResponse");
    private final static QName _DataValidationResponse_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "DataValidationResponse");
    private final static QName _SearchResponse_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "SearchResponse");
    private final static QName _VersionRequest_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "VersionRequest");
    private final static QName _IBANTranslationRequest_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "IBANTranslationRequest");
    private final static QName _ServiceDetailsContextCheck_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "ContextCheck");
    private final static QName _ServiceDetailsLanguageCountryCode_QNAME = new QName(
            "urn:experian/BANKWIZARD/soapservice/types", "LanguageCountryCode");
    private final static QName _VersionResponseConditions_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "conditions");
    private final static QName _PersonalDetailsResponseElementEnumeratedResponse_QNAME = new QName(
            "urn:experian/BANKWIZARD/soapservice/types", "EnumeratedResponse");
    private final static QName _PersonalDetailsResponseElementScoredResponse_QNAME = new QName(
            "urn:experian/BANKWIZARD/soapservice/types", "ScoredResponse");
    private final static QName _PersonalDetailsTagAddress_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "Address");
    private final static QName _PersonalDetailsTagDOB_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types", "DOB");
    private final static QName _PersonalDetailsTagTypeofAccount_QNAME = new QName(
            "urn:experian/BANKWIZARD/soapservice/types", "TypeofAccount");
    private final static QName _PersonalDetailsTagOwnerType_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "OwnerType");
    private final static QName _PersonalDetailsTagCustomerAccountType_QNAME = new QName(
            "urn:experian/BANKWIZARD/soapservice/types", "CustomerAccountType");
    private final static QName _PersonalDetailsTagAccountSetupDate_QNAME = new QName(
            "urn:experian/BANKWIZARD/soapservice/types", "AccountSetupDate");
    private final static QName _SearchRequestSearch2_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "Search2");
    private final static QName _SearchRequestResultsLimit_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "ResultsLimit");
    private final static QName _SearchRequestSearch3_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "Search3");
    private final static QName _ClientElementBranchNumber_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "BranchNumber");
    private final static QName _ClientElementClientMACAddress_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "ClientMACAddress");
    private final static QName _ClientElementClientIPAddress_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "ClientIPAddress");
    private final static QName _AddressPostorZipCode_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "PostorZipCode");
    private final static QName _DataValidationResponseInputBankDetails_QNAME = new QName(
            "urn:experian/BANKWIZARD/soapservice/types", "InputBankDetails");
    private final static QName _DataValidationResponseProcessedBankDetails_QNAME = new QName(
            "urn:experian/BANKWIZARD/soapservice/types", "ProcessedBankDetails");
    private final static QName _DataValidationResponseBranchData_QNAME = new QName(
            "urn:experian/BANKWIZARD/soapservice/types", "branchData");
    private final static QName _DataValidationResponseAlternateBranch_QNAME = new QName(
            "urn:experian/BANKWIZARD/soapservice/types", "alternateBranch");
    private final static QName _DataValidationRequestPersonalDetails_QNAME = new QName(
            "urn:experian/BANKWIZARD/soapservice/types", "PersonalDetails");
    private final static QName _PersonalDetailsResponseTagDescription_QNAME = new QName(
            "urn:experian/BANKWIZARD/soapservice/types", "Description");
    private final static QName _BankDetailsTagBBANparam5_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "BBANparam5");
    private final static QName _BankDetailsTagIBAN_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types", "IBAN");
    private final static QName _BankDetailsTagBBANparam4_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "BBANparam4");
    private final static QName _BankDetailsTagBBANparam1_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "BBANparam1");
    private final static QName _BankDetailsTagBBANparam3_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "BBANparam3");
    private final static QName _BankDetailsTagBBANparam2_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "BBANparam2");
    private final static QName _BankDetailsTagBIC_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types", "BIC");
    private final static QName _BankDetailsTagSWIFTData_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "SWIFTData");
    private final static QName _BankDetailsTagAdditonalinfo_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "Additonalinfo");
    private final static QName _BankDetailsTagSubBranches_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "SubBranches");
    private final static QName _BankDetailsTagBulkMode_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "BulkMode");
    private final static QName _MetaDataResponseSetISOCountryCode_QNAME = new QName(
            "urn:experian/BANKWIZARD/soapservice/types", "ISOCountryCode");
    private final static QName _MetaDataResponseSetMetaDataID_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "metaDataID");
    private final static QName _MetaDataResponseSetMetaDataDescription_QNAME = new QName(
            "urn:experian/BANKWIZARD/soapservice/types", "metaDataDescription");
    private final static QName _DateElementMonth_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types", "Month");
    private final static QName _DateElementDay_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types", "Day");
    private final static QName _MetaDataGroupSize_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types", "Size");
    private final static QName _MetaDataGroupGroupName_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "GroupName");
    private final static QName _MetaDataGroupGroupID_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types",
            "GroupID");
    private final static QName _MetaDataGroupName_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types", "Name");
    private final static QName _MetaDataGroupID_QNAME = new QName("urn:experian/BANKWIZARD/soapservice/types", "ID");

    /**
     * Create a new ObjectFactory that can be used to create new instances of
     * schema derived classes for package: experian.bankwizard.soapservice.types
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SearchRequest }
     *
     */
    public SearchRequest createSearchRequest() {
        return new SearchRequest();
    }

    /**
     * Create an instance of {@link MetaDataResponse }
     *
     */
    public MetaDataResponse createMetaDataResponse() {
        return new MetaDataResponse();
    }

    /**
     * Create an instance of {@link MetaDataResponseSet }
     *
     */
    public MetaDataResponseSet createMetaDataResponseSet() {
        return new MetaDataResponseSet();
    }

    /**
     * Create an instance of {@link VersionResponse }
     *
     */
    public VersionResponse createVersionResponse() {
        return new VersionResponse();
    }

    /**
     * Create an instance of {@link VersionRequest }
     *
     */
    public VersionRequest createVersionRequest() {
        return new VersionRequest();
    }

    /**
     * Create an instance of {@link IBANTranslationRequest }
     *
     */
    public IBANTranslationRequest createIBANTranslationRequest() {
        return new IBANTranslationRequest();
    }

    /**
     * Create an instance of {@link DataValidationRequest }
     *
     */
    public DataValidationRequest createDataValidationRequest() {
        return new DataValidationRequest();
    }

    /**
     * Create an instance of {@link MetaDataRequest }
     *
     */
    public MetaDataRequest createMetaDataRequest() {
        return new MetaDataRequest();
    }

    /**
     * Create an instance of {@link IBANTranslationResponse }
     *
     */
    public IBANTranslationResponse createIBANTranslationResponse() {
        return new IBANTranslationResponse();
    }

    /**
     * Create an instance of {@link DataValidationResponse }
     *
     */
    public DataValidationResponse createDataValidationResponse() {
        return new DataValidationResponse();
    }

    /**
     * Create an instance of {@link SearchResponse }
     *
     */
    public SearchResponse createSearchResponse() {
        return new SearchResponse();
    }

    /**
     * Create an instance of {@link Address }
     *
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link DateElement }
     *
     */
    public DateElement createDateElement() {
        return new DateElement();
    }

    /**
     * Create an instance of {@link PersonalDetailsTag }
     *
     */
    public PersonalDetailsTag createPersonalDetailsTag() {
        return new PersonalDetailsTag();
    }

    /**
     * Create an instance of {@link GenericHeader }
     *
     */
    public GenericHeader createGenericHeader() {
        return new GenericHeader();
    }

    /**
     * Create an instance of {@link AltBranch }
     *
     */
    public AltBranch createAltBranch() {
        return new AltBranch();
    }

    /**
     * Create an instance of {@link Conditions }
     *
     */
    public Conditions createConditions() {
        return new Conditions();
    }

    /**
     * Create an instance of {@link DataElement }
     *
     */
    public DataElement createDataElement() {
        return new DataElement();
    }

    /**
     * Create an instance of {@link PersonalDetailsResponseElement }
     *
     */
    public PersonalDetailsResponseElement createPersonalDetailsResponseElement() {
        return new PersonalDetailsResponseElement();
    }

    /**
     * Create an instance of {@link PersonalDetailsResponseTag }
     *
     */
    public PersonalDetailsResponseTag createPersonalDetailsResponseTag() {
        return new PersonalDetailsResponseTag();
    }

    /**
     * Create an instance of {@link ClientElement }
     *
     */
    public ClientElement createClientElement() {
        return new ClientElement();
    }

    /**
     * Create an instance of {@link SearchResult }
     *
     */
    public SearchResult createSearchResult() {
        return new SearchResult();
    }

    /**
     * Create an instance of {@link BankDetailsTag }
     *
     */
    public BankDetailsTag createBankDetailsTag() {
        return new BankDetailsTag();
    }

    /**
     * Create an instance of {@link ServiceDetails }
     *
     */
    public ServiceDetails createServiceDetails() {
        return new ServiceDetails();
    }

    /**
     * Create an instance of {@link Details }
     *
     */
    public Details createDetails() {
        return new Details();
    }

    /**
     * Create an instance of {@link PersonalDetailsEnumResponseTag }
     *
     */
    public PersonalDetailsEnumResponseTag createPersonalDetailsEnumResponseTag() {
        return new PersonalDetailsEnumResponseTag();
    }

    /**
     * Create an instance of {@link MetaDataGroup }
     *
     */
    public MetaDataGroup createMetaDataGroup() {
        return new MetaDataGroup();
    }

    /**
     * Create an instance of {@link AddressElement }
     *
     */
    public AddressElement createAddressElement() {
        return new AddressElement();
    }

    /**
     * Create an instance of {@link BranchData }
     *
     */
    public BranchData createBranchData() {
        return new BranchData();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchRequest
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "SearchRequest")
    public JAXBElement<SearchRequest> createSearchRequest(final SearchRequest value) {
        return new JAXBElement<SearchRequest>(_SearchRequest_QNAME, SearchRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionResponse
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "VersionResponse")
    public JAXBElement<VersionResponse> createVersionResponse(final VersionResponse value) {
        return new JAXBElement<VersionResponse>(_VersionResponse_QNAME, VersionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement
     * }{@code <}{@link DataValidationRequest }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "DataValidationRequest")
    public JAXBElement<DataValidationRequest> createDataValidationRequest(final DataValidationRequest value) {
        return new JAXBElement<DataValidationRequest>(_DataValidationRequest_QNAME, DataValidationRequest.class, null,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetaDataRequest
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "MetaDataRequest")
    public JAXBElement<MetaDataRequest> createMetaDataRequest(final MetaDataRequest value) {
        return new JAXBElement<MetaDataRequest>(_MetaDataRequest_QNAME, MetaDataRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement
     * }{@code <}{@link IBANTranslationResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "IBANTranslationResponse")
    public JAXBElement<IBANTranslationResponse> createIBANTranslationResponse(final IBANTranslationResponse value) {
        return new JAXBElement<IBANTranslationResponse>(_IBANTranslationResponse_QNAME, IBANTranslationResponse.class, null,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement
     * }{@code <}{@link DataValidationResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "DataValidationResponse")
    public JAXBElement<DataValidationResponse> createDataValidationResponse(final DataValidationResponse value) {
        return new JAXBElement<DataValidationResponse>(_DataValidationResponse_QNAME, DataValidationResponse.class, null,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchResponse
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "SearchResponse")
    public JAXBElement<SearchResponse> createSearchResponse(final SearchResponse value) {
        return new JAXBElement<SearchResponse>(_SearchResponse_QNAME, SearchResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionRequest
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "VersionRequest")
    public JAXBElement<VersionRequest> createVersionRequest(final VersionRequest value) {
        return new JAXBElement<VersionRequest>(_VersionRequest_QNAME, VersionRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement
     * }{@code <}{@link IBANTranslationRequest }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "IBANTranslationRequest")
    public JAXBElement<IBANTranslationRequest> createIBANTranslationRequest(final IBANTranslationRequest value) {
        return new JAXBElement<IBANTranslationRequest>(_IBANTranslationRequest_QNAME, IBANTranslationRequest.class, null,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement
     * }{@code <}{@link ContextCheckElement }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "ContextCheck",
            scope = ServiceDetails.class)
    public JAXBElement<ContextCheckElement> createServiceDetailsContextCheck(final ContextCheckElement value) {
        return new JAXBElement<ContextCheckElement>(_ServiceDetailsContextCheck_QNAME, ContextCheckElement.class,
                ServiceDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "LanguageCountryCode",
            scope = ServiceDetails.class)
    public JAXBElement<String> createServiceDetailsLanguageCountryCode(final String value) {
        return new JAXBElement<String>(_ServiceDetailsLanguageCountryCode_QNAME, String.class, ServiceDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Conditions
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "conditions",
            scope = VersionResponse.class)
    public JAXBElement<Conditions> createVersionResponseConditions(final Conditions value) {
        return new JAXBElement<Conditions>(_VersionResponseConditions_QNAME, Conditions.class, VersionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement
     * }{@code <}{@link PersonalDetailsEnumResponseTag }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "EnumeratedResponse",
            scope = PersonalDetailsResponseElement.class)
    public JAXBElement<PersonalDetailsEnumResponseTag>
           createPersonalDetailsResponseElementEnumeratedResponse(final PersonalDetailsEnumResponseTag value) {
        return new JAXBElement<PersonalDetailsEnumResponseTag>(_PersonalDetailsResponseElementEnumeratedResponse_QNAME,
                PersonalDetailsEnumResponseTag.class, PersonalDetailsResponseElement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement
     * }{@code <}{@link PersonalDetailsResponseTag }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "ScoredResponse",
            scope = PersonalDetailsResponseElement.class)
    public JAXBElement<PersonalDetailsResponseTag>
           createPersonalDetailsResponseElementScoredResponse(final PersonalDetailsResponseTag value) {
        return new JAXBElement<PersonalDetailsResponseTag>(_PersonalDetailsResponseElementScoredResponse_QNAME,
                PersonalDetailsResponseTag.class, PersonalDetailsResponseElement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Address
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "Address",
            scope = PersonalDetailsTag.class)
    public JAXBElement<Address> createPersonalDetailsTagAddress(final Address value) {
        return new JAXBElement<Address>(_PersonalDetailsTagAddress_QNAME, Address.class, PersonalDetailsTag.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateElement
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "DOB", scope = PersonalDetailsTag.class)
    public JAXBElement<DateElement> createPersonalDetailsTagDOB(final DateElement value) {
        return new JAXBElement<DateElement>(_PersonalDetailsTagDOB_QNAME, DateElement.class, PersonalDetailsTag.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement
     * }{@code <}{@link AccountTypeElement }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "TypeofAccount",
            scope = PersonalDetailsTag.class)
    public JAXBElement<AccountTypeElement> createPersonalDetailsTagTypeofAccount(final AccountTypeElement value) {
        return new JAXBElement<AccountTypeElement>(_PersonalDetailsTagTypeofAccount_QNAME, AccountTypeElement.class,
                PersonalDetailsTag.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement
     * }{@code <}{@link OwnerTypeElement }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "OwnerType",
            scope = PersonalDetailsTag.class)
    public JAXBElement<OwnerTypeElement> createPersonalDetailsTagOwnerType(final OwnerTypeElement value) {
        return new JAXBElement<OwnerTypeElement>(_PersonalDetailsTagOwnerType_QNAME, OwnerTypeElement.class,
                PersonalDetailsTag.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement
     * }{@code <}{@link CustomerAccountTypeElement }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "CustomerAccountType",
            scope = PersonalDetailsTag.class)
    public JAXBElement<CustomerAccountTypeElement>
           createPersonalDetailsTagCustomerAccountType(final CustomerAccountTypeElement value) {
        return new JAXBElement<CustomerAccountTypeElement>(_PersonalDetailsTagCustomerAccountType_QNAME,
                CustomerAccountTypeElement.class, PersonalDetailsTag.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateElement
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "AccountSetupDate",
            scope = PersonalDetailsTag.class)
    public JAXBElement<DateElement> createPersonalDetailsTagAccountSetupDate(final DateElement value) {
        return new JAXBElement<DateElement>(_PersonalDetailsTagAccountSetupDate_QNAME, DateElement.class,
                PersonalDetailsTag.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "Search2", scope = SearchRequest.class)
    public JAXBElement<String> createSearchRequestSearch2(final String value) {
        return new JAXBElement<String>(_SearchRequestSearch2_QNAME, String.class, SearchRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "ResultsLimit",
            scope = SearchRequest.class)
    public JAXBElement<BigInteger> createSearchRequestResultsLimit(final BigInteger value) {
        return new JAXBElement<BigInteger>(_SearchRequestResultsLimit_QNAME, BigInteger.class, SearchRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "Search3", scope = SearchRequest.class)
    public JAXBElement<String> createSearchRequestSearch3(final String value) {
        return new JAXBElement<String>(_SearchRequestSearch3_QNAME, String.class, SearchRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "LanguageCountryCode",
            scope = SearchRequest.class)
    public JAXBElement<String> createSearchRequestLanguageCountryCode(final String value) {
        return new JAXBElement<String>(_ServiceDetailsLanguageCountryCode_QNAME, String.class, SearchRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "BranchNumber",
            scope = ClientElement.class)
    public JAXBElement<BigInteger> createClientElementBranchNumber(final BigInteger value) {
        return new JAXBElement<BigInteger>(_ClientElementBranchNumber_QNAME, BigInteger.class, ClientElement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "ClientMACAddress",
            scope = ClientElement.class)
    public JAXBElement<String> createClientElementClientMACAddress(final String value) {
        return new JAXBElement<String>(_ClientElementClientMACAddress_QNAME, String.class, ClientElement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "ClientIPAddress",
            scope = ClientElement.class)
    public JAXBElement<String> createClientElementClientIPAddress(final String value) {
        return new JAXBElement<String>(_ClientElementClientIPAddress_QNAME, String.class, ClientElement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "PostorZipCode", scope = Address.class)
    public JAXBElement<String> createAddressPostorZipCode(final String value) {
        return new JAXBElement<String>(_AddressPostorZipCode_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankDetailsTag
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "InputBankDetails",
            scope = DataValidationResponse.class)
    public JAXBElement<BankDetailsTag> createDataValidationResponseInputBankDetails(final BankDetailsTag value) {
        return new JAXBElement<BankDetailsTag>(_DataValidationResponseInputBankDetails_QNAME, BankDetailsTag.class,
                DataValidationResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankDetailsTag
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "ProcessedBankDetails",
            scope = DataValidationResponse.class)
    public JAXBElement<BankDetailsTag> createDataValidationResponseProcessedBankDetails(final BankDetailsTag value) {
        return new JAXBElement<BankDetailsTag>(_DataValidationResponseProcessedBankDetails_QNAME, BankDetailsTag.class,
                DataValidationResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BranchData
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "branchData",
            scope = DataValidationResponse.class)
    public JAXBElement<BranchData> createDataValidationResponseBranchData(final BranchData value) {
        return new JAXBElement<BranchData>(_DataValidationResponseBranchData_QNAME, BranchData.class,
                DataValidationResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AltBranch
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "alternateBranch",
            scope = DataValidationResponse.class)
    public JAXBElement<AltBranch> createDataValidationResponseAlternateBranch(final AltBranch value) {
        return new JAXBElement<AltBranch>(_DataValidationResponseAlternateBranch_QNAME, AltBranch.class,
                DataValidationResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Conditions
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "conditions",
            scope = DataValidationResponse.class)
    public JAXBElement<Conditions> createDataValidationResponseConditions(final Conditions value) {
        return new JAXBElement<Conditions>(_VersionResponseConditions_QNAME, Conditions.class, DataValidationResponse.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement
     * }{@code <}{@link PersonalDetailsTag }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "PersonalDetails",
            scope = DataValidationRequest.class)
    public JAXBElement<PersonalDetailsTag> createDataValidationRequestPersonalDetails(final PersonalDetailsTag value) {
        return new JAXBElement<PersonalDetailsTag>(_DataValidationRequestPersonalDetails_QNAME, PersonalDetailsTag.class,
                DataValidationRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Conditions
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "conditions",
            scope = SearchResponse.class)
    public JAXBElement<Conditions> createSearchResponseConditions(final Conditions value) {
        return new JAXBElement<Conditions>(_VersionResponseConditions_QNAME, Conditions.class, SearchResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "Description",
            scope = PersonalDetailsResponseTag.class)
    public JAXBElement<String> createPersonalDetailsResponseTagDescription(final String value) {
        return new JAXBElement<String>(_PersonalDetailsResponseTagDescription_QNAME, String.class,
                PersonalDetailsResponseTag.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "Description",
            scope = PersonalDetailsEnumResponseTag.class)
    public JAXBElement<String> createPersonalDetailsEnumResponseTagDescription(final String value) {
        return new JAXBElement<String>(_PersonalDetailsResponseTagDescription_QNAME, String.class,
                PersonalDetailsEnumResponseTag.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "Description", scope = DataElement.class)
    public JAXBElement<String> createDataElementDescription(final String value) {
        return new JAXBElement<String>(_PersonalDetailsResponseTagDescription_QNAME, String.class, DataElement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "BBANparam5",
            scope = BankDetailsTag.class)
    public JAXBElement<String> createBankDetailsTagBBANparam5(final String value) {
        return new JAXBElement<String>(_BankDetailsTagBBANparam5_QNAME, String.class, BankDetailsTag.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "IBAN", scope = BankDetailsTag.class)
    public JAXBElement<String> createBankDetailsTagIBAN(final String value) {
        return new JAXBElement<String>(_BankDetailsTagIBAN_QNAME, String.class, BankDetailsTag.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "BBANparam4",
            scope = BankDetailsTag.class)
    public JAXBElement<String> createBankDetailsTagBBANparam4(final String value) {
        return new JAXBElement<String>(_BankDetailsTagBBANparam4_QNAME, String.class, BankDetailsTag.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "BBANparam1",
            scope = BankDetailsTag.class)
    public JAXBElement<String> createBankDetailsTagBBANparam1(final String value) {
        return new JAXBElement<String>(_BankDetailsTagBBANparam1_QNAME, String.class, BankDetailsTag.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "BBANparam3",
            scope = BankDetailsTag.class)
    public JAXBElement<String> createBankDetailsTagBBANparam3(final String value) {
        return new JAXBElement<String>(_BankDetailsTagBBANparam3_QNAME, String.class, BankDetailsTag.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "BBANparam2",
            scope = BankDetailsTag.class)
    public JAXBElement<String> createBankDetailsTagBBANparam2(final String value) {
        return new JAXBElement<String>(_BankDetailsTagBBANparam2_QNAME, String.class, BankDetailsTag.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "BIC", scope = BankDetailsTag.class)
    public JAXBElement<String> createBankDetailsTagBIC(final String value) {
        return new JAXBElement<String>(_BankDetailsTagBIC_QNAME, String.class, BankDetailsTag.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OptionalItem
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "SWIFTData",
            scope = BankDetailsTag.class)
    public JAXBElement<OptionalItem> createBankDetailsTagSWIFTData(final OptionalItem value) {
        return new JAXBElement<OptionalItem>(_BankDetailsTagSWIFTData_QNAME, OptionalItem.class, BankDetailsTag.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OptionalItem
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "Additonalinfo",
            scope = BankDetailsTag.class)
    public JAXBElement<OptionalItem> createBankDetailsTagAdditonalinfo(final OptionalItem value) {
        return new JAXBElement<OptionalItem>(_BankDetailsTagAdditonalinfo_QNAME, OptionalItem.class, BankDetailsTag.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OptionalItem
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "SubBranches",
            scope = BankDetailsTag.class)
    public JAXBElement<OptionalItem> createBankDetailsTagSubBranches(final OptionalItem value) {
        return new JAXBElement<OptionalItem>(_BankDetailsTagSubBranches_QNAME, OptionalItem.class, BankDetailsTag.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OptionalItem
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "BulkMode", scope = BankDetailsTag.class)
    public JAXBElement<OptionalItem> createBankDetailsTagBulkMode(final OptionalItem value) {
        return new JAXBElement<OptionalItem>(_BankDetailsTagBulkMode_QNAME, OptionalItem.class, BankDetailsTag.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "ISOCountryCode",
            scope = MetaDataResponseSet.class)
    public JAXBElement<String> createMetaDataResponseSetISOCountryCode(final String value) {
        return new JAXBElement<String>(_MetaDataResponseSetISOCountryCode_QNAME, String.class, MetaDataResponseSet.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Conditions
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "conditions",
            scope = MetaDataResponseSet.class)
    public JAXBElement<Conditions> createMetaDataResponseSetConditions(final Conditions value) {
        return new JAXBElement<Conditions>(_VersionResponseConditions_QNAME, Conditions.class, MetaDataResponseSet.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "metaDataID",
            scope = MetaDataResponseSet.class)
    public JAXBElement<Integer> createMetaDataResponseSetMetaDataID(final Integer value) {
        return new JAXBElement<Integer>(_MetaDataResponseSetMetaDataID_QNAME, Integer.class, MetaDataResponseSet.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "metaDataDescription",
            scope = MetaDataResponseSet.class)
    public JAXBElement<String> createMetaDataResponseSetMetaDataDescription(final String value) {
        return new JAXBElement<String>(_MetaDataResponseSetMetaDataDescription_QNAME, String.class,
                MetaDataResponseSet.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Conditions
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "conditions",
            scope = IBANTranslationResponse.class)
    public JAXBElement<Conditions> createIBANTranslationResponseConditions(final Conditions value) {
        return new JAXBElement<Conditions>(_VersionResponseConditions_QNAME, Conditions.class, IBANTranslationResponse.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "Month", scope = DateElement.class)
    public JAXBElement<String> createDateElementMonth(final String value) {
        return new JAXBElement<String>(_DateElementMonth_QNAME, String.class, DateElement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "Day", scope = DateElement.class)
    public JAXBElement<String> createDateElementDay(final String value) {
        return new JAXBElement<String>(_DateElementDay_QNAME, String.class, DateElement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "Size", scope = MetaDataGroup.class)
    public JAXBElement<Integer> createMetaDataGroupSize(final Integer value) {
        return new JAXBElement<Integer>(_MetaDataGroupSize_QNAME, Integer.class, MetaDataGroup.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "GroupName", scope = MetaDataGroup.class)
    public JAXBElement<String> createMetaDataGroupGroupName(final String value) {
        return new JAXBElement<String>(_MetaDataGroupGroupName_QNAME, String.class, MetaDataGroup.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "GroupID", scope = MetaDataGroup.class)
    public JAXBElement<Integer> createMetaDataGroupGroupID(final Integer value) {
        return new JAXBElement<Integer>(_MetaDataGroupGroupID_QNAME, Integer.class, MetaDataGroup.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "Name", scope = MetaDataGroup.class)
    public JAXBElement<String> createMetaDataGroupName(final String value) {
        return new JAXBElement<String>(_MetaDataGroupName_QNAME, String.class, MetaDataGroup.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String
     * }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:experian/BANKWIZARD/soapservice/types", name = "ID", scope = MetaDataGroup.class)
    public JAXBElement<String> createMetaDataGroupID(final String value) {
        return new JAXBElement<String>(_MetaDataGroupID_QNAME, String.class, MetaDataGroup.class, value);
    }

}
