
package uk.co.phoenixlife.service.dto.bankwizard;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for OwnerTypeElement.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="OwnerTypeElement">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="JOINT"/>
 *     &lt;enumeration value="SINGLE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 *
 */
@XmlType(name = "OwnerTypeElement")
@XmlEnum
public enum OwnerTypeElement {

    JOINT, SINGLE;

    public String value() {
        return name();
    }

    public static OwnerTypeElement fromValue(final String v) {
        return valueOf(v);
    }

}
