
package uk.co.phoenixlife.service.dto.bankwizard;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for PersonalDetailsResponseElement complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="PersonalDetailsResponseElement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ScoredResponse" type="{urn:experian/BANKWIZARD/soapservice/types}PersonalDetailsResponseTag" minOccurs="0"/>
 *         &lt;element name="EnumeratedResponse" type="{urn:experian/BANKWIZARD/soapservice/types}PersonalDetailsEnumResponseTag" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonalDetailsResponseElement", propOrder = {
        "scoredResponse",
        "enumeratedResponse"
})
public class PersonalDetailsResponseElement {

    @XmlElementRef(name = "ScoredResponse", namespace = "urn:experian/BANKWIZARD/soapservice/types",
            type = JAXBElement.class, required = false)
    protected JAXBElement<PersonalDetailsResponseTag> scoredResponse;
    @XmlElementRef(name = "EnumeratedResponse", namespace = "urn:experian/BANKWIZARD/soapservice/types",
            type = JAXBElement.class, required = false)
    protected JAXBElement<PersonalDetailsEnumResponseTag> enumeratedResponse;

    /**
     * Gets the value of the scoredResponse property.
     *
     * @return possible object is {@link JAXBElement
     *         }{@code <}{@link PersonalDetailsResponseTag }{@code >}
     * 
     */
    public JAXBElement<PersonalDetailsResponseTag> getScoredResponse() {
        return scoredResponse;
    }

    /**
     * Sets the value of the scoredResponse property.
     *
     * @param value
     *            allowed object is {@link JAXBElement
     *            }{@code <}{@link PersonalDetailsResponseTag }{@code >}
     * 
     */
    public void setScoredResponse(final JAXBElement<PersonalDetailsResponseTag> value) {
        scoredResponse = value;
    }

    /**
     * Gets the value of the enumeratedResponse property.
     *
     * @return possible object is {@link JAXBElement
     *         }{@code <}{@link PersonalDetailsEnumResponseTag }{@code >}
     * 
     */
    public JAXBElement<PersonalDetailsEnumResponseTag> getEnumeratedResponse() {
        return enumeratedResponse;
    }

    /**
     * Sets the value of the enumeratedResponse property.
     *
     * @param value
     *            allowed object is {@link JAXBElement
     *            }{@code <}{@link PersonalDetailsEnumResponseTag }{@code >}
     * 
     */
    public void setEnumeratedResponse(final JAXBElement<PersonalDetailsEnumResponseTag> value) {
        enumeratedResponse = value;
    }

}
