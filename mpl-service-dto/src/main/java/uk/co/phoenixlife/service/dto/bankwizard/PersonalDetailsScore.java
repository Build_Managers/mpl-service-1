
package uk.co.phoenixlife.service.dto.bankwizard;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for PersonalDetailsScore.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="PersonalDetailsScore">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="MATCH"/>
 *     &lt;enumeration value="NO_MATCH"/>
 *     &lt;enumeration value="UNKNOWN"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 *
 */
@XmlType(name = "PersonalDetailsScore")
@XmlEnum
public enum PersonalDetailsScore {

    MATCH, NO_MATCH, UNKNOWN;

    public String value() {
        return name();
    }

    public static PersonalDetailsScore fromValue(final String v) {
        return valueOf(v);
    }

}
