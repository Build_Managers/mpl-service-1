
package uk.co.phoenixlife.service.dto.bankwizard;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for PersonalDetailsTag complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="PersonalDetailsTag">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FullFirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FullSurname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DOB" type="{urn:experian/BANKWIZARD/soapservice/types}DateElement" minOccurs="0"/>
 *         &lt;element name="Address" type="{urn:experian/BANKWIZARD/soapservice/types}Address" minOccurs="0"/>
 *         &lt;element name="AccountSetupDate" type="{urn:experian/BANKWIZARD/soapservice/types}DateElement" minOccurs="0"/>
 *         &lt;element name="TypeofAccount" type="{urn:experian/BANKWIZARD/soapservice/types}AccountTypeElement" minOccurs="0"/>
 *         &lt;element name="CustomerAccountType" type="{urn:experian/BANKWIZARD/soapservice/types}CustomerAccountTypeElement" minOccurs="0"/>
 *         &lt;element name="OwnerType" type="{urn:experian/BANKWIZARD/soapservice/types}OwnerTypeElement" minOccurs="0"/>
 *         &lt;element name="AdditionalPersonalItems" type="{urn:experian/BANKWIZARD/soapservice/types}DataElement" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonalDetailsTag", propOrder = {
        "fullFirstName",
        "fullSurname",
        "dob",
        "address",
        "accountSetupDate",
        "typeofAccount",
        "customerAccountType",
        "ownerType",
        "additionalPersonalItems"
})
public class PersonalDetailsTag {

    @XmlElement(name = "FullFirstName", required = true, nillable = true)
    protected String fullFirstName;
    @XmlElement(name = "FullSurname", required = true, nillable = true)
    protected String fullSurname;
    @XmlElementRef(name = "DOB", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<DateElement> dob;
    @XmlElementRef(name = "Address", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<Address> address;
    @XmlElementRef(name = "AccountSetupDate", namespace = "urn:experian/BANKWIZARD/soapservice/types",
            type = JAXBElement.class, required = false)
    protected JAXBElement<DateElement> accountSetupDate;
    @XmlElementRef(name = "TypeofAccount", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<AccountTypeElement> typeofAccount;
    @XmlElementRef(name = "CustomerAccountType", namespace = "urn:experian/BANKWIZARD/soapservice/types",
            type = JAXBElement.class, required = false)
    protected JAXBElement<CustomerAccountTypeElement> customerAccountType;
    @XmlElementRef(name = "OwnerType", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<OwnerTypeElement> ownerType;
    @XmlElement(name = "AdditionalPersonalItems", nillable = true)
    protected List<DataElement> additionalPersonalItems;

    /**
     * Gets the value of the fullFirstName property.
     *
     * @return possible object is {@link String }
     * 
     */
    public String getFullFirstName() {
        return fullFirstName;
    }

    /**
     * Sets the value of the fullFirstName property.
     *
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setFullFirstName(final String value) {
        fullFirstName = value;
    }

    /**
     * Gets the value of the fullSurname property.
     *
     * @return possible object is {@link String }
     * 
     */
    public String getFullSurname() {
        return fullSurname;
    }

    /**
     * Sets the value of the fullSurname property.
     *
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setFullSurname(final String value) {
        fullSurname = value;
    }

    /**
     * Gets the value of the dob property.
     *
     * @return possible object is {@link JAXBElement
     *         }{@code <}{@link DateElement }{@code >}
     * 
     */
    public JAXBElement<DateElement> getDOB() {
        return dob;
    }

    /**
     * Sets the value of the dob property.
     *
     * @param value
     *            allowed object is {@link JAXBElement
     *            }{@code <}{@link DateElement }{@code >}
     * 
     */
    public void setDOB(final JAXBElement<DateElement> value) {
        dob = value;
    }

    /**
     * Gets the value of the address property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link Address
     *         }{@code >}
     * 
     */
    public JAXBElement<Address> getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link Address
     *            }{@code >}
     * 
     */
    public void setAddress(final JAXBElement<Address> value) {
        address = value;
    }

    /**
     * Gets the value of the accountSetupDate property.
     *
     * @return possible object is {@link JAXBElement
     *         }{@code <}{@link DateElement }{@code >}
     * 
     */
    public JAXBElement<DateElement> getAccountSetupDate() {
        return accountSetupDate;
    }

    /**
     * Sets the value of the accountSetupDate property.
     *
     * @param value
     *            allowed object is {@link JAXBElement
     *            }{@code <}{@link DateElement }{@code >}
     * 
     */
    public void setAccountSetupDate(final JAXBElement<DateElement> value) {
        accountSetupDate = value;
    }

    /**
     * Gets the value of the typeofAccount property.
     *
     * @return possible object is {@link JAXBElement
     *         }{@code <}{@link AccountTypeElement }{@code >}
     * 
     */
    public JAXBElement<AccountTypeElement> getTypeofAccount() {
        return typeofAccount;
    }

    /**
     * Sets the value of the typeofAccount property.
     *
     * @param value
     *            allowed object is {@link JAXBElement
     *            }{@code <}{@link AccountTypeElement }{@code >}
     * 
     */
    public void setTypeofAccount(final JAXBElement<AccountTypeElement> value) {
        typeofAccount = value;
    }

    /**
     * Gets the value of the customerAccountType property.
     *
     * @return possible object is {@link JAXBElement
     *         }{@code <}{@link CustomerAccountTypeElement }{@code >}
     * 
     */
    public JAXBElement<CustomerAccountTypeElement> getCustomerAccountType() {
        return customerAccountType;
    }

    /**
     * Sets the value of the customerAccountType property.
     *
     * @param value
     *            allowed object is {@link JAXBElement
     *            }{@code <}{@link CustomerAccountTypeElement }{@code >}
     * 
     */
    public void setCustomerAccountType(final JAXBElement<CustomerAccountTypeElement> value) {
        customerAccountType = value;
    }

    /**
     * Gets the value of the ownerType property.
     *
     * @return possible object is {@link JAXBElement
     *         }{@code <}{@link OwnerTypeElement }{@code >}
     * 
     */
    public JAXBElement<OwnerTypeElement> getOwnerType() {
        return ownerType;
    }

    /**
     * Sets the value of the ownerType property.
     *
     * @param value
     *            allowed object is {@link JAXBElement
     *            }{@code <}{@link OwnerTypeElement }{@code >}
     * 
     */
    public void setOwnerType(final JAXBElement<OwnerTypeElement> value) {
        ownerType = value;
    }

    /**
     * Gets the value of the additionalPersonalItems property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the additionalPersonalItems property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getAdditionalPersonalItems().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataElement }
     *
     *
     */
    public List<DataElement> getAdditionalPersonalItems() {
        if (additionalPersonalItems == null) {
            additionalPersonalItems = new ArrayList<DataElement>();
        }
        return additionalPersonalItems;
    }

}
