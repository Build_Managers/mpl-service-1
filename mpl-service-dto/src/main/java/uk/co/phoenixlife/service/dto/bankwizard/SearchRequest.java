
package uk.co.phoenixlife.service.dto.bankwizard;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for SearchRequest complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="SearchRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClientInformation" type="{urn:experian/BANKWIZARD/soapservice/types}GenericHeader"/>
 *         &lt;element name="Search1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Search2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Search3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ISOCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LanguageCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResultsLimit" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="AdditionalItem" type="{urn:experian/BANKWIZARD/soapservice/types}DataElement" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchRequest", propOrder = {
        "clientInformation",
        "search1",
        "search2",
        "search3",
        "isoCountryCode",
        "languageCountryCode",
        "resultsLimit",
        "additionalItem"
})
public class SearchRequest {

    @XmlElement(name = "ClientInformation", required = true, nillable = true)
    protected GenericHeader clientInformation;
    @XmlElement(name = "Search1", required = true, nillable = true)
    protected String search1;
    @XmlElementRef(name = "Search2", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<String> search2;
    @XmlElementRef(name = "Search3", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<String> search3;
    @XmlElement(name = "ISOCountryCode", required = true)
    protected String isoCountryCode;
    @XmlElementRef(name = "LanguageCountryCode", namespace = "urn:experian/BANKWIZARD/soapservice/types",
            type = JAXBElement.class, required = false)
    protected JAXBElement<String> languageCountryCode;
    @XmlElementRef(name = "ResultsLimit", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<BigInteger> resultsLimit;
    @XmlElement(name = "AdditionalItem", nillable = true)
    protected List<DataElement> additionalItem;

    /**
     * Gets the value of the clientInformation property.
     *
     * @return possible object is {@link GenericHeader }
     * 
     */
    public GenericHeader getClientInformation() {
        return clientInformation;
    }

    /**
     * Sets the value of the clientInformation property.
     *
     * @param value
     *            allowed object is {@link GenericHeader }
     * 
     */
    public void setClientInformation(final GenericHeader value) {
        clientInformation = value;
    }

    /**
     * Gets the value of the search1 property.
     *
     * @return possible object is {@link String }
     * 
     */
    public String getSearch1() {
        return search1;
    }

    /**
     * Sets the value of the search1 property.
     *
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setSearch1(final String value) {
        search1 = value;
    }

    /**
     * Gets the value of the search2 property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link String
     *         }{@code >}
     * 
     */
    public JAXBElement<String> getSearch2() {
        return search2;
    }

    /**
     * Sets the value of the search2 property.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String
     *            }{@code >}
     * 
     */
    public void setSearch2(final JAXBElement<String> value) {
        search2 = value;
    }

    /**
     * Gets the value of the search3 property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link String
     *         }{@code >}
     * 
     */
    public JAXBElement<String> getSearch3() {
        return search3;
    }

    /**
     * Sets the value of the search3 property.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String
     *            }{@code >}
     * 
     */
    public void setSearch3(final JAXBElement<String> value) {
        search3 = value;
    }

    /**
     * Gets the value of the isoCountryCode property.
     *
     * @return possible object is {@link String }
     * 
     */
    public String getISOCountryCode() {
        return isoCountryCode;
    }

    /**
     * Sets the value of the isoCountryCode property.
     *
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setISOCountryCode(final String value) {
        isoCountryCode = value;
    }

    /**
     * Gets the value of the languageCountryCode property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link String
     *         }{@code >}
     * 
     */
    public JAXBElement<String> getLanguageCountryCode() {
        return languageCountryCode;
    }

    /**
     * Sets the value of the languageCountryCode property.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String
     *            }{@code >}
     * 
     */
    public void setLanguageCountryCode(final JAXBElement<String> value) {
        languageCountryCode = value;
    }

    /**
     * Gets the value of the resultsLimit property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link BigInteger
     *         }{@code >}
     * 
     */
    public JAXBElement<BigInteger> getResultsLimit() {
        return resultsLimit;
    }

    /**
     * Sets the value of the resultsLimit property.
     *
     * @param value
     *            allowed object is {@link JAXBElement
     *            }{@code <}{@link BigInteger }{@code >}
     * 
     */
    public void setResultsLimit(final JAXBElement<BigInteger> value) {
        resultsLimit = value;
    }

    /**
     * Gets the value of the additionalItem property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the additionalItem property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getAdditionalItem().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataElement }
     *
     *
     */
    public List<DataElement> getAdditionalItem() {
        if (additionalItem == null) {
            additionalItem = new ArrayList<DataElement>();
        }
        return additionalItem;
    }

}
