
package uk.co.phoenixlife.service.dto.bankwizard;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for SearchResponse complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="SearchResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="matches" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="resultSet" type="{urn:experian/BANKWIZARD/soapservice/types}SearchResult" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="conditions" type="{urn:experian/BANKWIZARD/soapservice/types}Conditions" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchResponse", propOrder = {
        "matches",
        "resultSet",
        "conditions"
})
public class SearchResponse {

    @XmlElement(required = true)
    protected BigInteger matches;
    @XmlElement(nillable = true)
    protected List<SearchResult> resultSet;
    @XmlElementRef(name = "conditions", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<Conditions> conditions;

    /**
     * Gets the value of the matches property.
     *
     * @return possible object is {@link BigInteger }
     * 
     */
    public BigInteger getMatches() {
        return matches;
    }

    /**
     * Sets the value of the matches property.
     *
     * @param value
     *            allowed object is {@link BigInteger }
     * 
     */
    public void setMatches(final BigInteger value) {
        matches = value;
    }

    /**
     * Gets the value of the resultSet property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the resultSet property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getResultSet().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchResult }
     *
     *
     */
    public List<SearchResult> getResultSet() {
        if (resultSet == null) {
            resultSet = new ArrayList<SearchResult>();
        }
        return resultSet;
    }

    /**
     * Gets the value of the conditions property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link Conditions
     *         }{@code >}
     * 
     */
    public JAXBElement<Conditions> getConditions() {
        return conditions;
    }

    /**
     * Sets the value of the conditions property.
     *
     * @param value
     *            allowed object is {@link JAXBElement
     *            }{@code <}{@link Conditions }{@code >}
     * 
     */
    public void setConditions(final JAXBElement<Conditions> value) {
        conditions = value;
    }

}
