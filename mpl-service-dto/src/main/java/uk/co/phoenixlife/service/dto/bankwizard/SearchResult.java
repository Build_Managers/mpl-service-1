
package uk.co.phoenixlife.service.dto.bankwizard;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for SearchResult complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="SearchResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Data" type="{urn:experian/BANKWIZARD/soapservice/types}DataElement" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="MetaData" type="{urn:experian/BANKWIZARD/soapservice/types}DataElement" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchResult", propOrder = {
        "id",
        "data",
        "metaData"
})
public class SearchResult {

    @XmlElement(name = "ID", required = true)
    protected String id;
    @XmlElement(name = "Data", nillable = true)
    protected List<DataElement> data;
    @XmlElement(name = "MetaData", nillable = true)
    protected List<DataElement> metaData;

    /**
     * Gets the value of the id property.
     *
     * @return possible object is {@link String }
     * 
     */
    public String getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setID(final String value) {
        id = value;
    }

    /**
     * Gets the value of the data property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the data property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getData().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataElement }
     *
     *
     */
    public List<DataElement> getData() {
        if (data == null) {
            data = new ArrayList<DataElement>();
        }
        return data;
    }

    /**
     * Gets the value of the metaData property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the metaData property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getMetaData().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataElement }
     *
     *
     */
    public List<DataElement> getMetaData() {
        if (metaData == null) {
            metaData = new ArrayList<DataElement>();
        }
        return metaData;
    }

}
