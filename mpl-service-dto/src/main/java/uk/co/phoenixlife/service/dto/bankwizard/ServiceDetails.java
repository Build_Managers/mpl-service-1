
package uk.co.phoenixlife.service.dto.bankwizard;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ServiceDetails complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="ServiceDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CheckType" type="{urn:experian/BANKWIZARD/soapservice/types}CheckTypeElement"/>
 *         &lt;element name="ContextCheck" type="{urn:experian/BANKWIZARD/soapservice/types}ContextCheckElement" minOccurs="0"/>
 *         &lt;element name="ISOCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LanguageCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AdditionalItem" type="{urn:experian/BANKWIZARD/soapservice/types}DataElement" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceDetails", propOrder = {
        "checkType",
        "contextCheck",
        "isoCountryCode",
        "languageCountryCode",
        "additionalItem"
})
public class ServiceDetails {

    @XmlElement(name = "CheckType", required = true, nillable = true)
    @XmlSchemaType(name = "token")
    protected CheckTypeElement checkType;
    @XmlElementRef(name = "ContextCheck", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<ContextCheckElement> contextCheck;
    @XmlElement(name = "ISOCountryCode", required = true)
    protected String isoCountryCode;
    @XmlElementRef(name = "LanguageCountryCode", namespace = "urn:experian/BANKWIZARD/soapservice/types",
            type = JAXBElement.class, required = false)
    protected JAXBElement<String> languageCountryCode;
    @XmlElement(name = "AdditionalItem", nillable = true)
    protected List<DataElement> additionalItem;

    /**
     * Gets the value of the checkType property.
     *
     * @return possible object is {@link CheckTypeElement }
     * 
     */
    public CheckTypeElement getCheckType() {
        return checkType;
    }

    /**
     * Sets the value of the checkType property.
     *
     * @param value
     *            allowed object is {@link CheckTypeElement }
     * 
     */
    public void setCheckType(final CheckTypeElement value) {
        checkType = value;
    }

    /**
     * Gets the value of the contextCheck property.
     *
     * @return possible object is {@link JAXBElement
     *         }{@code <}{@link ContextCheckElement }{@code >}
     * 
     */
    public JAXBElement<ContextCheckElement> getContextCheck() {
        return contextCheck;
    }

    /**
     * Sets the value of the contextCheck property.
     *
     * @param value
     *            allowed object is {@link JAXBElement
     *            }{@code <}{@link ContextCheckElement }{@code >}
     * 
     */
    public void setContextCheck(final JAXBElement<ContextCheckElement> value) {
        contextCheck = value;
    }

    /**
     * Gets the value of the isoCountryCode property.
     *
     * @return possible object is {@link String }
     * 
     */
    public String getISOCountryCode() {
        return isoCountryCode;
    }

    /**
     * Sets the value of the isoCountryCode property.
     *
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setISOCountryCode(final String value) {
        isoCountryCode = value;
    }

    /**
     * Gets the value of the languageCountryCode property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link String
     *         }{@code >}
     * 
     */
    public JAXBElement<String> getLanguageCountryCode() {
        return languageCountryCode;
    }

    /**
     * Sets the value of the languageCountryCode property.
     *
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String
     *            }{@code >}
     * 
     */
    public void setLanguageCountryCode(final JAXBElement<String> value) {
        languageCountryCode = value;
    }

    /**
     * Gets the value of the additionalItem property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the additionalItem property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getAdditionalItem().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataElement }
     *
     *
     */
    public List<DataElement> getAdditionalItem() {
        if (additionalItem == null) {
            additionalItem = new ArrayList<DataElement>();
        }
        return additionalItem;
    }

}
