
package uk.co.phoenixlife.service.dto.bankwizard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for VersionRequest complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="VersionRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClientInformation" type="{urn:experian/BANKWIZARD/soapservice/types}GenericHeader"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VersionRequest", propOrder = {
        "clientInformation"
})
public class VersionRequest {

    @XmlElement(name = "ClientInformation", required = true, nillable = true)
    protected GenericHeader clientInformation;

    /**
     * Gets the value of the clientInformation property.
     *
     * @return possible object is {@link GenericHeader }
     * 
     */
    public GenericHeader getClientInformation() {
        return clientInformation;
    }

    /**
     * Sets the value of the clientInformation property.
     *
     * @param value
     *            allowed object is {@link GenericHeader }
     * 
     */
    public void setClientInformation(final GenericHeader value) {
        clientInformation = value;
    }

}
