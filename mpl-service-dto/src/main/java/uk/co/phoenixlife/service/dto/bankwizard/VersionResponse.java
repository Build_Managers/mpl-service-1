
package uk.co.phoenixlife.service.dto.bankwizard;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for VersionResponse complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="VersionResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DllVersionData" type="{urn:experian/BANKWIZARD/soapservice/types}Details" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DtfVersionData" type="{urn:experian/BANKWIZARD/soapservice/types}Details" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="conditions" type="{urn:experian/BANKWIZARD/soapservice/types}Conditions" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VersionResponse", propOrder = {
        "dllVersionData",
        "dtfVersionData",
        "conditions"
})
public class VersionResponse {

    @XmlElement(name = "DllVersionData", nillable = true)
    protected List<Details> dllVersionData;
    @XmlElement(name = "DtfVersionData", nillable = true)
    protected List<Details> dtfVersionData;
    @XmlElementRef(name = "conditions", namespace = "urn:experian/BANKWIZARD/soapservice/types", type = JAXBElement.class,
            required = false)
    protected JAXBElement<Conditions> conditions;

    /**
     * Gets the value of the dllVersionData property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the dllVersionData property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getDllVersionData().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Details }
     *
     *
     */
    public List<Details> getDllVersionData() {
        if (dllVersionData == null) {
            dllVersionData = new ArrayList<Details>();
        }
        return dllVersionData;
    }

    /**
     * Gets the value of the dtfVersionData property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the dtfVersionData property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getDtfVersionData().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Details }
     *
     *
     */
    public List<Details> getDtfVersionData() {
        if (dtfVersionData == null) {
            dtfVersionData = new ArrayList<Details>();
        }
        return dtfVersionData;
    }

    /**
     * Gets the value of the conditions property.
     *
     * @return possible object is {@link JAXBElement }{@code <}{@link Conditions
     *         }{@code >}
     * 
     */
    public JAXBElement<Conditions> getConditions() {
        return conditions;
    }

    /**
     * Sets the value of the conditions property.
     *
     * @param value
     *            allowed object is {@link JAXBElement
     *            }{@code <}{@link Conditions }{@code >}
     * 
     */
    public void setConditions(final JAXBElement<Conditions> value) {
        conditions = value;
    }

}
