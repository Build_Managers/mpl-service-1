
package uk.co.phoenixlife.service.dto.bankwizard;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for >AddressElement>id.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name=">AddressElement>id">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="name"/>
 *     &lt;enumeration value="number"/>
 *     &lt;enumeration value="flat"/>
 *     &lt;enumeration value="street"/>
 *     &lt;enumeration value="block"/>
 *     &lt;enumeration value="floor"/>
 *     &lt;enumeration value="appartment"/>
 *     &lt;enumeration value="village"/>
 *     &lt;enumeration value="town"/>
 *     &lt;enumeration value="city"/>
 *     &lt;enumeration value="province"/>
 *     &lt;enumeration value="county"/>
 *     &lt;enumeration value="state"/>
 *     &lt;enumeration value="POBox"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 *
 */
@XmlType(name = ">AddressElement>id")
@XmlEnum
public enum _003eAddressElement_003eId {

    @XmlEnumValue("name")
    NAME("name"), @XmlEnumValue("number")
    NUMBER("number"), @XmlEnumValue("flat")
    FLAT("flat"), @XmlEnumValue("street")
    STREET("street"), @XmlEnumValue("block")
    BLOCK("block"), @XmlEnumValue("floor")
    FLOOR("floor"), @XmlEnumValue("appartment")
    APPARTMENT("appartment"), @XmlEnumValue("village")
    VILLAGE("village"), @XmlEnumValue("town")
    TOWN("town"), @XmlEnumValue("city")
    CITY("city"), @XmlEnumValue("province")
    PROVINCE("province"), @XmlEnumValue("county")
    COUNTY("county"), @XmlEnumValue("state")
    STATE("state"), @XmlEnumValue("POBox")
    PO_BOX("POBox");
    private final String value;

    _003eAddressElement_003eId(final String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static _003eAddressElement_003eId fromValue(final String v) {
        for (_003eAddressElement_003eId c : _003eAddressElement_003eId.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
