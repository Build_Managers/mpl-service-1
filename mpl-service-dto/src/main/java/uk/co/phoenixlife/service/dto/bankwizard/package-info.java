@javax.xml.bind.annotation.XmlSchema(namespace = "urn:experian/BANKWIZARD/soapservice/types",
        elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package uk.co.phoenixlife.service.dto.bankwizard;
