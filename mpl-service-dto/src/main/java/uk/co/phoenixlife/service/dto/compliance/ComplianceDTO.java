/*
 * ComplianceDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.compliance;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * ComplianceDTO.java
 */
public class ComplianceDTO implements Serializable {
    /** long */
    private static final long serialVersionUID = 1L;
    private String sessionId;
    private String eventCode;
    private String eventVal;
    private Timestamp eventTs;

    /**
     * Gets the sessionId
     *
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Sets the sessionId
     *
     * @param psessionId
     *            the sessionId to set
     */
    public void setSessionId(final String psessionId) {
        sessionId = psessionId;
    }

    /**
     * Gets the eventCode
     *
     * @return the eventCode
     */
    public String getEventCode() {
        return eventCode;
    }

    /**
     * Sets the eventCode
     *
     * @param peventCode
     *            the eventCode to set
     */
    public void setEventCode(final String peventCode) {
        eventCode = peventCode;
    }

    /**
     * Gets the eventVal
     *
     * @return the eventVal
     */
    public String getEventVal() {
        return eventVal;
    }

    /**
     * Sets the eventVal
     *
     * @param peventVal
     *            the eventVal to set
     */
    public void setEventVal(final String peventVal) {
        eventVal = peventVal;
    }

    /**
     * Gets the eventTs
     *
     * @return the eventTs
     */
    public Timestamp getEventTs() {
        return eventTs;
    }

    /**
     * Sets the eventTs
     *
     * @param peventTs
     *            the eventTs to set
     */
    public void setEventTs(final Timestamp peventTs) {
        eventTs = peventTs;
    }

}
