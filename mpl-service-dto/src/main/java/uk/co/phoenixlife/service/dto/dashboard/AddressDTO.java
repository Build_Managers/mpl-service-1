package uk.co.phoenixlife.service.dto.dashboard;

public class AddressDTO {

	private String houseNum;
    private String houseName;
    private String address1;
    private String address2;
    private String cityTown;
    private String county;
    private String country;
    private String countryCd;
    private String postCode;
    private String addressStatus;
    private String addressStatusDesc;
    private String preferredFlg;
    
    
    
	public String getHouseNum() {
		return houseNum;
	}
	public void setHouseNum(String houseNum) {
		this.houseNum = houseNum;
	}
	public String getHouseName() {
		return houseName;
	}
	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCityTown() {
		return cityTown;
	}
	public void setCityTown(String cityTown) {
		this.cityTown = cityTown;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCountryCd() {
		return countryCd;
	}
	public void setCountryCd(String countryCd) {
		this.countryCd = countryCd;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public String getAddressStatus() {
		return addressStatus;
	}
	public void setAddressStatus(String addressStatus) {
		this.addressStatus = addressStatus;
	}
	public String getAddressStatusDesc() {
		return addressStatusDesc;
	}
	public void setAddressStatusDesc(String addressStatusDesc) {
		this.addressStatusDesc = addressStatusDesc;
	}
	public String getPreferredFlg() {
		return preferredFlg;
	}
	public void setPreferredFlg(String preferredFlg) {
		this.preferredFlg = preferredFlg;
	}
    
    
    
    
    
}
