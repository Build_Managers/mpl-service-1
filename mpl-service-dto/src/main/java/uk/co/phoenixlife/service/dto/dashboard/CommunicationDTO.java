package uk.co.phoenixlife.service.dto.dashboard;

public class CommunicationDTO {

	
	private String commChnlCd;
	private String commChnlSubType;
	private String commChnlSubTypeCd;
	private String commChnlDesc;
	private String commChnlPreferredFlg;
	private String commChnlVal;
	private String countryCd;
	private String commChnlStartDate;
    
    
	public String getCommChnlDesc() {
		return commChnlDesc;
	}
	public void setCommChnlDesc(String commChnlDesc) {
		this.commChnlDesc = commChnlDesc;
	}
	public String getCommChnlPreferredFlg() {
		return commChnlPreferredFlg;
	}
	public void setCommChnlPreferredFlg(String commChnlPreferredFlg) {
		this.commChnlPreferredFlg = commChnlPreferredFlg;
	}
	public String getCommChnlVal() {
		return commChnlVal;
	}
	public void setCommChnlVal(String commChnlVal) {
		this.commChnlVal = commChnlVal;
	}
	public String getCountryCd() {
		return countryCd;
	}
	public void setCountryCd(String countryCd) {
		this.countryCd = countryCd;
	}
	/**
	 * @return the commChnlCd
	 */
	public String getCommChnlCd() {
		return commChnlCd;
	}
	/**
	 * @param commChnlCd the commChnlCd to set
	 */
	public void setCommChnlCd(String commChnlCd) {
		this.commChnlCd = commChnlCd;
	}
	/**
	 * @return the commChnlSubType
	 */
	public String getCommChnlSubType() {
		return commChnlSubType;
	}
	/**
	 * @param commChnlSubType the commChnlSubType to set
	 */
	public void setCommChnlSubType(String commChnlSubType) {
		this.commChnlSubType = commChnlSubType;
	}
	/**
	 * @return the commChnlSubTypeCd
	 */
	public String getCommChnlSubTypeCd() {
		return commChnlSubTypeCd;
	}
	/**
	 * @param commChnlSubTypeCd the commChnlSubTypeCd to set
	 */
	public void setCommChnlSubTypeCd(String commChnlSubTypeCd) {
		this.commChnlSubTypeCd = commChnlSubTypeCd;
	}
	public String getCommChnlStartDate() {
		return commChnlStartDate;
	}
	public void setCommChnlStartDate(String commChnlStartDate) {
		this.commChnlStartDate = commChnlStartDate;
	}
    
    
    
    

}
