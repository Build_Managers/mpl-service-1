package uk.co.phoenixlife.service.dto.dashboard;

import uk.co.phoenixlife.service.dto.dashboard.PoliciesOwnedByAPartyDTO;

public class DashBoardPolicyDTO {
    private String bancsPolNum;
    private PoliciesOwnedByAPartyDTO policiesOwnedByAPartyDTO;

    /**
     * Gets the bancsPolNum
     * 
     * @return the bancsPolNum
     */
    public String getBancsPolNum() {
        return bancsPolNum;
    }

    /**
     * Sets the bancsPolNum
     * 
     * @param bancsPolNum
     *            the bancsPolNum to set
     */
    public void setBancsPolNum(final String bancsPolNum) {
        this.bancsPolNum = bancsPolNum;
    }

    /**
     * Gets the policiesOwnedByAPartyDTO
     * 
     * @return the policiesOwnedByAPartyDTO
     */
    public PoliciesOwnedByAPartyDTO getPoliciesOwnedByAPartyDTO() {
        return policiesOwnedByAPartyDTO;
    }

    /**
     * Sets the policiesOwnedByAPartyDTO
     * 
     * @param policiesOwnedByAPartyDTO
     *            the policiesOwnedByAPartyDTO to set
     */
    public void setPoliciesOwnedByAPartyDTO(final PoliciesOwnedByAPartyDTO policiesOwnedByAPartyDTO) {
        this.policiesOwnedByAPartyDTO = policiesOwnedByAPartyDTO;
    }

}
