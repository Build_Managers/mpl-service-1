/**
 * 
 */
package uk.co.phoenixlife.service.dto.dashboard;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * @author 1012746
 *
 */
public class DataQualityWarning
{
    private String dataQualityWarning;

    public String getDataQualityWarning ()
    {
        return dataQualityWarning;
    }

    public void setDataQualityWarning (String dataQualityWarning)
    {
        this.dataQualityWarning = dataQualityWarning;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
