package uk.co.phoenixlife.service.dto.dashboard;

import java.sql.Timestamp;
import java.util.Date;

public class PartyBasicDetailByPartyNoDTO {

	private String partyType;
	private String title;
	private String forename;
	private String surname;
	private Date dateOfBirth;
	private String lifeStatusLit;
	private String lifeStatusCd;
	private String myPhoenixUserName;
	private String maritalStatus;
	private String gender;
	private String niNo;
	private String bancsLegacyCustNo;
	private String bancsUniqueCustNo;
	private Timestamp busTimeStamp;
	
	
	public String getPartyType() {
		return partyType;
	}
	public void setPartyType(String partyType) {
		this.partyType = partyType;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getForename() {
		return forename;
	}
	public void setForename(String forename) {
		this.forename = forename;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getLifeStatusLit() {
		return lifeStatusLit;
	}
	public void setLifeStatusLit(String lifeStatusLit) {
		this.lifeStatusLit = lifeStatusLit;
	}
	public String getLifeStatusCd() {
		return lifeStatusCd;
	}
	public void setLifeStatusCd(String lifeStatusCd) {
		this.lifeStatusCd = lifeStatusCd;
	}
	public String getMyPhoenixUserName() {
		return myPhoenixUserName;
	}
	public void setMyPhoenixUserName(String myPhoenixUserName) {
		this.myPhoenixUserName = myPhoenixUserName;
	}
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getNiNo() {
		return niNo;
	}
	public void setNiNo(String niNo) {
		this.niNo = niNo;
	}
	public String getBancsLegacyCustNo() {
		return bancsLegacyCustNo;
	}
	public void setBancsLegacyCustNo(String bancsLegacyCustNo) {
		this.bancsLegacyCustNo = bancsLegacyCustNo;
	}
	public String getBancsUniqueCustNo() {
		return bancsUniqueCustNo;
	}
	public void setBancsUniqueCustNo(String bancsUniqueCustNo) {
		this.bancsUniqueCustNo = bancsUniqueCustNo;
	}
	public Timestamp getBusTimeStamp() {
		return busTimeStamp;
	}
	public void setBusTimeStamp(Timestamp busTimeStamp) {
		this.busTimeStamp = busTimeStamp;
	}
	
	
	
	
}
