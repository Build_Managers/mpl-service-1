package uk.co.phoenixlife.service.dto.dashboard;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

public class PoliciesDetailsDTO {

	
	
	private String policyNumber;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'hh:mm:ss")
	private Date busTimeStamp;
	private String productName;
	private String productTypeLit;
	private String productTypeCd;
	private Date planStartDate;
	private String statusCd;
	private String statusLit;
	private Date retirementDate;
	private String gmpTranche;
	private String penRevStatusLit;
	private String penRevStatusCd;
	private List<DataQualityWarning> dataQualityWarning;
	private Double fundValue;
	private Double deathValue;
	private Double transferValue;
	private String bancsUniqueCustNo;
	
	public String getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	public Date getBusTimeStamp() {
		return busTimeStamp;
	}
	public void setBusTimeStamp(Date busTimeStamp) {
		this.busTimeStamp = busTimeStamp;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductTypeLit() {
		return productTypeLit;
	}
	public void setProductTypeLit(String productTypeLit) {
		this.productTypeLit = productTypeLit;
	}
	public String getProductTypeCd() {
		return productTypeCd;
	}
	public void setProductTypeCd(String productTypeCd) {
		this.productTypeCd = productTypeCd;
	}
	public Date getPlanStartDate() {
		return planStartDate;
	}
	public void setPlanStartDate(Date planStartDate) {
		this.planStartDate = planStartDate;
	}
	public String getStatusCd() {
		return statusCd;
	}
	public void setStatusCd(String statusCd) {
		this.statusCd = statusCd;
	}
	public String getStatusLit() {
		return statusLit;
	}
	public void setStatusLit(String statusLit) {
		this.statusLit = statusLit;
	}
	
	public String getGmpTranche() {
		return gmpTranche;
	}
	public void setGmpTranche(String gmpTranche) {
		this.gmpTranche = gmpTranche;
	}
	public String getPenRevStatusLit() {
		return penRevStatusLit;
	}
	public void setPenRevStatusLit(String penRevStatusLit) {
		this.penRevStatusLit = penRevStatusLit;
	}
	public String getPenRevStatusCd() {
		return penRevStatusCd;
	}
	public void setPenRevStatusCd(String penRevStatusCd) {
		this.penRevStatusCd = penRevStatusCd;
	}
	public List<DataQualityWarning> getDataQualityWarning() {
		return dataQualityWarning;
	}
	public void setDataQualityWarning(List<DataQualityWarning> dataQualityWarning) {
		this.dataQualityWarning = dataQualityWarning;
	}
	public Double getFundValue() {
		return fundValue;
	}
	public void setFundValue(Double fundValue) {
		this.fundValue = fundValue;
	}
	public Double getDeathValue() {
		return deathValue;
	}
	public void setDeathValue(Double deathValue) {
		this.deathValue = deathValue;
	}
	public Double getTransferValue() {
		return transferValue;
	}
	public void setTransferValue(Double transferValue) {
		this.transferValue = transferValue;
	}
	public String getBancsUniqueCustNo() {
		return bancsUniqueCustNo;
	}
	public void setBancsUniqueCustNo(String bancsUniqueCustNo) {
		this.bancsUniqueCustNo = bancsUniqueCustNo;
	}
	public Date getRetirementDate() {
		return retirementDate;
	}
	public void setRetirementDate(Date retirementDate) {
		this.retirementDate = retirementDate;
	}
}
