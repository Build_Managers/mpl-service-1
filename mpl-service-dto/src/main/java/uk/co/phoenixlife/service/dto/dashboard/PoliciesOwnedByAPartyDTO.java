/*
 * PoliciesOwnedByAParty.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.dashboard;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * PoliciesOwnedByAParty.java
 */
public class PoliciesOwnedByAPartyDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'hh:mm:ss")
    private Timestamp busTimeStamp;
    private List<PolicyDTO> listOfPolicies;
    private String bancsUniqueCustNo;

    /**
     * Gets the busTimeStamp
     *
     * @return the busTimeStamp
     */
    public Timestamp getBusTimeStamp() {
        return busTimeStamp;
    }

    /**
     * Sets the busTimeStamp
     *
     * @param pBusTimeStamp
     *            the busTimeStamp to set
     */
    public void setBusTimeStamp(final Timestamp pBusTimeStamp) {
        busTimeStamp = pBusTimeStamp;
    }

    /**
     * Method to getListOfPolicies
     *
     * @return the listOfPolicies
     */
    public List<PolicyDTO> getListOfPolicies() {
        return listOfPolicies;
    }

    /**
     * Method to setListOfPolicies
     *
     * @param plistOfPolicies
     *            the listOfPolicies to set
     */
    public void setListOfPolicies(final List<PolicyDTO> plistOfPolicies) {
        this.listOfPolicies = plistOfPolicies;
    }

    /**
     * Gets the bancsUniqueCustNo
     *
     * @return the bancsUniqueCustNo
     */
    public String getBancsUniqueCustNo() {
        return bancsUniqueCustNo;
    }

    /**
     * Sets the bancsUniqueCustNo
     *
     * @param pBancsUniqueCustNo
     *            the bancsUniqueCustNo to set
     */
    public void setBancsUniqueCustNo(final String pBancsUniqueCustNo) {
        bancsUniqueCustNo = pBancsUniqueCustNo;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
