/*
 * PolicyDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.dashboard;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * PolicyDTO.java
 */
public class PolicyDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private Date polStDate;
    private String prodName;
    private String bancsPolNum;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private Date polNRD;
    private String brand;
    private String polNum;
    private String polStatusCd;
    private String polStatusLit;

    private String polSusp;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private Date polIRD;

    /**
     * Gets the polStatusLit
     *
     * @return the polStatusLit
     */
    public String getPolStatusLit() {
        return polStatusLit;
    }

    /**
     * Sets the polStatusLit
     *
     * @param pPolStatusLit
     *            the polStatusLit to set
     *
     */
    public void setPolStatusLit(final String pPolStatusLit) {
        this.polStatusLit = pPolStatusLit;
    }

    /**
     * Gets the polStDate
     *
     * @return the polStDate
     */
    public Date getPolStDate() {
        return polStDate;
    }

    /**
     * Sets the polStDate
     *
     * @param pPolStDate
     *            the polStDate to set
     */
    public void setPolStDate(final Date pPolStDate) {
        polStDate = pPolStDate;
    }

    /**
     * Gets the prodName
     *
     * @return the prodName
     */
    public String getProdName() {
        return prodName;
    }

    /**
     * Sets the prodName
     *
     * @param pProdName
     *            the prodName to set
     */
    public void setProdName(final String pProdName) {
        prodName = pProdName;
    }

    /**
     * Gets the bancsPolNum
     *
     * @return the bancsPolNum
     */
    public String getBancsPolNum() {
        return bancsPolNum;
    }

    /**
     * Sets the bancsPolNum
     *
     * @param pBancsPolNum
     *            the bancsPolNum to set
     */
    public void setBancsPolNum(final String pBancsPolNum) {
        bancsPolNum = pBancsPolNum;
    }

    /**
     * Gets the polNRD
     *
     * @return the polNRD
     */
    public Date getPolNRD() {
        return polNRD;
    }

    /**
     * Sets the polNRD
     *
     * @param pPolNRD
     *            the polNRD to set
     */
    public void setPolNRD(final Date pPolNRD) {
        polNRD = pPolNRD;
    }

    /**
     * Gets the brand
     *
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * Sets the brand
     *
     * @param pBrand
     *            the brand to set
     */
    public void setBrand(final String pBrand) {
        brand = pBrand;
    }

    /**
     * Gets the polNum
     *
     * @return the polNum
     */
    public String getPolNum() {
        return polNum;
    }

    /**
     * Sets the polNum
     *
     * @param pPolNum
     *            the polNum to set
     */
    public void setPolNum(final String pPolNum) {
        polNum = pPolNum;
    }

    /**
     * Gets the polStatus
     *
     * @return the polStatus
     */
    public String getPolStatusCd() {
        return polStatusCd;
    }

    /**
     * Sets the polStatus
     *
     * @param pPolStatusCd
     *            the polStatus to set
     */
    public void setPolStatusCd(final String pPolStatusCd) {
        polStatusCd = pPolStatusCd;
    }

    /**
     * Gets the polSusp
     *
     * @return the polSusp
     */
    public String getPolSusp() {
        return polSusp;
    }

    /**
     * Sets the polSusp
     *
     * @param pPolSusp
     *            the polSusp to set
     */
    public void setPolSusp(final String pPolSusp) {
        polSusp = pPolSusp;
    }

    /**
     * Gets the polIRD
     *
     * @return the polIRD
     */
    public Date getPolIRD() {
        return polIRD;
    }

    /**
     * Sets the polIRD
     *
     * @param pPolIRD
     *            the polIRD to set
     */
    public void setPolIRD(final Date pPolIRD) {
        polIRD = pPolIRD;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
