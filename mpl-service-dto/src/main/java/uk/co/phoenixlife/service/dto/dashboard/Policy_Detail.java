/**
 * 
 */
package uk.co.phoenixlife.service.dto.dashboard;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * @author 1012746
 *
 */
public class Policy_Detail
{
    private String productTypeCd;

    private String dateTime;

    private String productTypeLit;

    private String planStartDate;

    private String guaranteedAnnuityRateIndicator;

    private String statusLit;

    private String penRevStatusCd;

    private String statusCd;

    private String bancsPolicyNo;

    private String penRevStatusLit;

    private String policyNumber;

    private String gmpTranche;

    private List<DataQualityWarning> dataQualityWarning;

    private String productName;

    private String retirementDate;

    public String getProductTypeCd ()
    {
        return productTypeCd;
    }

    public void setProductTypeCd (String productTypeCd)
    {
        this.productTypeCd = productTypeCd;
    }

    public String getDateTime ()
    {
        return dateTime;
    }

    public void setDateTime (String dateTime)
    {
        this.dateTime = dateTime;
    }

    public String getProductTypeLit ()
    {
        return productTypeLit;
    }

    public void setProductTypeLit (String productTypeLit)
    {
        this.productTypeLit = productTypeLit;
    }

    public String getPlanStartDate ()
    {
        return planStartDate;
    }

    public void setPlanStartDate (String planStartDate)
    {
        this.planStartDate = planStartDate;
    }

    public String getGuaranteedAnnuityRateIndicator ()
    {
        return guaranteedAnnuityRateIndicator;
    }

    public void setGuaranteedAnnuityRateIndicator (String guaranteedAnnuityRateIndicator)
    {
        this.guaranteedAnnuityRateIndicator = guaranteedAnnuityRateIndicator;
    }

    public String getStatusLit ()
    {
        return statusLit;
    }

    public void setStatusLit (String statusLit)
    {
        this.statusLit = statusLit;
    }

    public String getPenRevStatusCd ()
    {
        return penRevStatusCd;
    }

    public void setPenRevStatusCd (String penRevStatusCd)
    {
        this.penRevStatusCd = penRevStatusCd;
    }

    public String getStatusCd ()
    {
        return statusCd;
    }

    public void setStatusCd (String statusCd)
    {
        this.statusCd = statusCd;
    }

    public String getBancsPolicyNo ()
    {
        return bancsPolicyNo;
    }

    public void setBancsPolicyNo (String bancsPolicyNo)
    {
        this.bancsPolicyNo = bancsPolicyNo;
    }

    public String getPenRevStatusLit ()
    {
        return penRevStatusLit;
    }

    public void setPenRevStatusLit (String penRevStatusLit)
    {
        this.penRevStatusLit = penRevStatusLit;
    }

    public String getPolicyNumber ()
    {
        return policyNumber;
    }

    public void setPolicyNumber (String policyNumber)
    {
        this.policyNumber = policyNumber;
    }

    public String getGmpTranche ()
    {
        return gmpTranche;
    }

    public void setGmpTranche (String gmpTranche)
    {
        this.gmpTranche = gmpTranche;
    }

    public List<DataQualityWarning> getDataQualityWarning ()
    {
        return dataQualityWarning;
    }

    public void setDataQualityWarning (List<DataQualityWarning> dataQualityWarning)
    {
        this.dataQualityWarning = dataQualityWarning;
    }

    public String getProductName ()
    {
        return productName;
    }

    public void setProductName (String productName)
    {
        this.productName = productName;
    }

    public String getRetirementDate ()
    {
        return retirementDate;
    }

    public void setRetirementDate (String retirementDate)
    {
        this.retirementDate = retirementDate;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
