package uk.co.phoenixlife.service.dto.dashboard;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class PremiumSummariesResponse
{
    private Premium_Summaries premium_Summaries;

    public Premium_Summaries getPremium_Summaries ()
    {
        return premium_Summaries;
    }

    public void setPremium_Summaries (Premium_Summaries premium_Summaries)
    {
        this.premium_Summaries = premium_Summaries;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
	