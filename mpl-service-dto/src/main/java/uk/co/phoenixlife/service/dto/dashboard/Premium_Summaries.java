/**
 * 
 */
package uk.co.phoenixlife.service.dto.dashboard;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * @author 1012746
 *
 */
public class Premium_Summaries
{
    private String dateTime;

    private String bancsPolicyNo;

    private String policyNumber;

    private List<Premium_Summary> premium_Summary;

    public String getDateTime ()
    {
        return dateTime;
    }

    public void setDateTime (String dateTime)
    {
        this.dateTime = dateTime;
    }

    public String getBancsPolicyNo ()
    {
        return bancsPolicyNo;
    }

    public void setBancsPolicyNo (String bancsPolicyNo)
    {
        this.bancsPolicyNo = bancsPolicyNo;
    }

    public String getPolicyNumber ()
    {
        return policyNumber;
    }

    public void setPolicyNumber (String policyNumber)
    {
        this.policyNumber = policyNumber;
    }

    public List<Premium_Summary> getPremium_Summary ()
    {
        return premium_Summary;
    }

    public void setPremium_Summary (List<Premium_Summary> premium_Summary)
    {
        this.premium_Summary = premium_Summary;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}