/**
 * 
 */
package uk.co.phoenixlife.service.dto.dashboard;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * @author 1012746
 *
 */
public class Premium_Summary
{
    private String frequencyLit;

    private String contributorTypeCd;

    private String lastContributionPaidDate;

    private String grossAmount;

    private String contributorTypeLit;

    private String netAmount;

    private String frequencyCd;

    public String getFrequencyLit ()
    {
        return frequencyLit;
    }

    public void setFrequencyLit (String frequencyLit)
    {
        this.frequencyLit = frequencyLit;
    }

    public String getContributorTypeCd ()
    {
        return contributorTypeCd;
    }

    public void setContributorTypeCd (String contributorTypeCd)
    {
        this.contributorTypeCd = contributorTypeCd;
    }

    public String getLastContributionPaidDate ()
    {
        return lastContributionPaidDate;
    }

    public void setLastContributionPaidDate (String lastContributionPaidDate)
    {
        this.lastContributionPaidDate = lastContributionPaidDate;
    }

    public String getGrossAmount ()
    {
        return grossAmount;
    }

    public void setGrossAmount (String grossAmount)
    {
        this.grossAmount = grossAmount;
    }

    public String getContributorTypeLit ()
    {
        return contributorTypeLit;
    }

    public void setContributorTypeLit (String contributorTypeLit)
    {
        this.contributorTypeLit = contributorTypeLit;
    }

    public String getNetAmount ()
    {
        return netAmount;
    }

    public void setNetAmount (String netAmount)
    {
        this.netAmount = netAmount;
    }

    public String getFrequencyCd ()
    {
        return frequencyCd;
    }

    public void setFrequencyCd (String frequencyCd)
    {
        this.frequencyCd = frequencyCd;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
