package uk.co.phoenixlife.service.dto.dashboard;

import java.util.List;
import java.util.Set;

import uk.co.phoenixlife.service.dto.policy.BancsRetQuoteValDTO;
import uk.co.phoenixlife.service.dto.policy.ClaimListDTO;
import uk.co.phoenixlife.service.dto.policy.PolDetailsDTO;

/**
 * SyncDashboardServiceDTO.java
 */
public class SyncDashboardServiceDTO {

    private List<PolDetailsDTO> validPolicy;
    private List<ClaimListDTO> validClaimList;
    private List<BancsRetQuoteValDTO> validRetQuoteVal;
    private Set<String> polDetailsBancsPolNumSet;
    private Set<String> claimListBancsPolNumSet;
    private Set<String> retQuoteValBancsPolNumSet;
    private Boolean flag;

    /**
     * Gets the validRetQuoteVal
     *
     * @return the validRetQuoteVal
     */
    public List<BancsRetQuoteValDTO> getValidRetQuoteVal() {
        return validRetQuoteVal;
    }

    /**
     * Sets the validRetQuoteVal
     *
     * @param pValidRetQuoteVal
     *            the validRetQuoteVal to set
     */
    public void setValidRetQuoteVal(final List<BancsRetQuoteValDTO> pValidRetQuoteVal) {
        validRetQuoteVal = pValidRetQuoteVal;
    }

    /**
     * Gets the retQuoteValBancsPolNumSet
     *
     * @return the retQuoteValBancsPolNumSet
     */
    public Set<String> getRetQuoteValBancsPolNumSet() {
        return retQuoteValBancsPolNumSet;
    }

    /**
     * Sets the retQuoteValBancsPolNumSet
     *
     * @param pRetQuoteValBancsPolNumSet
     *            the retQuoteValBancsPolNumSet to set
     */
    public void setRetQuoteValBancsPolNumSet(final Set<String> pRetQuoteValBancsPolNumSet) {
        retQuoteValBancsPolNumSet = pRetQuoteValBancsPolNumSet;
    }

    /**
     * Gets the validPolicy
     *
     * @return the validPolicy
     */
    public List<PolDetailsDTO> getValidPolicy() {
        return validPolicy;
    }

    /**
     * Sets the validPolicy
     *
     * @param pValidPolicy
     *            the validPolicy to set
     */
    public void setValidPolicy(final List<PolDetailsDTO> pValidPolicy) {
        validPolicy = pValidPolicy;
    }

    /**
     * Gets the validClaimList
     *
     * @return the validClaimList
     */
    public List<ClaimListDTO> getValidClaimList() {
        return validClaimList;
    }

    /**
     * Sets the validClaimList
     *
     * @param pValidClaimList
     *            the validClaimList to set
     */
    public void setValidClaimList(final List<ClaimListDTO> pValidClaimList) {
        validClaimList = pValidClaimList;
    }

    /**
     * Gets the polDetailsBancsPolNumSet
     *
     * @return the polDetailsBancsPolNumSet
     */
    public Set<String> getPolDetailsBancsPolNumSet() {
        return polDetailsBancsPolNumSet;
    }

    /**
     * Sets the polDetailsBancsPolNumSet
     *
     * @param pPolDetailsBancsPolNumSet
     *            the polDetailsBancsPolNumSet to set
     */
    public void setPolDetailsBancsPolNumSet(final Set<String> pPolDetailsBancsPolNumSet) {
        polDetailsBancsPolNumSet = pPolDetailsBancsPolNumSet;
    }

    /**
     * Gets the claimListBancsPolNumSet
     *
     * @return the claimListBancsPolNumSet
     */
    public Set<String> getClaimListBancsPolNumSet() {
        return claimListBancsPolNumSet;
    }

    /**
     * Sets the claimListBancsPolNumSet
     *
     * @param pClaimListBancsPolNumSet
     *            the claimListBancsPolNumSet to set
     */
    public void setClaimListBancsPolNumSet(final Set<String> pClaimListBancsPolNumSet) {
        claimListBancsPolNumSet = pClaimListBancsPolNumSet;
    }

    /**
     * Gets the flag
     *
     * @return the flag
     */
    public Boolean getFlag() {
        return flag;
    }

    /**
     * Sets the flag
     *
     * @param pFlag
     *            the flag to set
     */
    public void setFlag(final Boolean pFlag) {
        flag = pFlag;
    }

}
