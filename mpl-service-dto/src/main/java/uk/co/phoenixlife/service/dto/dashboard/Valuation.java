/**
 *
 */
package uk.co.phoenixlife.service.dto.dashboard;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * @author 1012746
 *
 */
public class Valuation {
    private String marketValueReduction;

    private String earlyExitCharge;

    private String valuationAmount;

    private String rightsType;

    private String valuationType;

    private String valuationAmountGross;

    private Boolean display;

    /**
     * Gets the display
     * 
     * @return the display
     */
    public Boolean getDisplay() {
        return display;
    }

    /**
     * Sets the display
     * 
     * @param display
     *            the display to set
     */
    public void setDisplay(final Boolean display) {
        this.display = display;
    }

    public String getMarketValueReduction() {
        return marketValueReduction;
    }

    public void setMarketValueReduction(final String marketValueReduction) {
        this.marketValueReduction = marketValueReduction;
    }

    public String getEarlyExitCharge() {
        return earlyExitCharge;
    }

    public void setEarlyExitCharge(final String earlyExitCharge) {
        this.earlyExitCharge = earlyExitCharge;
    }

    public String getValuationAmount() {
        return valuationAmount;
    }

    public void setValuationAmount(final String valuationAmount) {
        this.valuationAmount = valuationAmount;
    }

    public String getRightsType() {
        return rightsType;
    }

    public void setRightsType(final String rightsType) {
        this.rightsType = rightsType;
    }

    public String getValuationType() {
        return valuationType;
    }

    public void setValuationType(final String valuationType) {
        this.valuationType = valuationType;
    }

    public String getValuationAmountGross() {
        return valuationAmountGross;
    }

    public void setValuationAmountGross(final String valuationAmountGross) {
        this.valuationAmountGross = valuationAmountGross;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}