package uk.co.phoenixlife.service.dto.dashboard;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Valuations
{
    private String valuationDateTime;

    private List<Valuation> valuation;

    private String bancsPolicyNo;

    private String policyNumber;

    public String getValuationDateTime ()
    {
        return valuationDateTime;
    }

    public void setValuationDateTime (String valuationDateTime)
    {
        this.valuationDateTime = valuationDateTime;
    }

    public List<Valuation> getValuation ()
    {
        return valuation;
    }

    public void setValuation (List<Valuation> valuation)
    {
        this.valuation = valuation;
    }

    public String getBancsPolicyNo ()
    {
        return bancsPolicyNo;
    }

    public void setBancsPolicyNo (String bancsPolicyNo)
    {
        this.bancsPolicyNo = bancsPolicyNo;
    }

    public String getPolicyNumber ()
    {
        return policyNumber;
    }

    public void setPolicyNumber (String policyNumber)
    {
        this.policyNumber = policyNumber;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}