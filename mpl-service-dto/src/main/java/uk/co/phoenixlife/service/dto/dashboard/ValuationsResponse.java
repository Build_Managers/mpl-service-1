package uk.co.phoenixlife.service.dto.dashboard;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class ValuationsResponse
{
    private Valuations valuations;

    public Valuations getValuations ()
    {
        return valuations;
    }

    public void setValuations (Valuations valuations)
    {
        this.valuations = valuations;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
