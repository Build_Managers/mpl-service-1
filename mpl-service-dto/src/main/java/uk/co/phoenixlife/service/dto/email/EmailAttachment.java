package uk.co.phoenixlife.service.dto.email;

import java.io.Serializable;

import javax.activation.DataHandler;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.module.jaxb.deser.DataHandlerJsonDeserializer;
import com.fasterxml.jackson.module.jaxb.ser.DataHandlerJsonSerializer;

/**
 * EmailAttachment.java
 */
public class EmailAttachment implements Serializable {

    /** long */
    private static final long serialVersionUID = 3175206482612015460L;

    private boolean inlineImageFlag;
    private String fileName;
    @JsonSerialize(using = DataHandlerJsonSerializer.class)
    @JsonDeserialize(using = DataHandlerJsonDeserializer.class)
    private DataHandler dataHandler;

    /**
     * Gets the inlineImageFlag
     *
     * @return the inlineImageFlag
     */
    public boolean isInlineImageFlag() {
        return inlineImageFlag;
    }

    /**
     * Sets the inlineImageFlag
     *
     * @param pInlineImageFlag
     *            the inlineImageFlag to set
     */
    public void setInlineImageFlag(final boolean pInlineImageFlag) {
        inlineImageFlag = pInlineImageFlag;
    }

    /**
     * Gets the fileName
     *
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Sets the fileName
     *
     * @param pFileName
     *            the fileName to set
     */
    public void setFileName(final String pFileName) {
        fileName = pFileName;
    }

    /**
     * Gets the dataHandler
     *
     * @return the dataHandler
     */
    public DataHandler getDataHandler() {
        return dataHandler;
    }

    /**
     * Sets the dataHandler
     *
     * @param pDataHandler
     *            the dataHandler to set
     */
    public void setDataHandler(final DataHandler pDataHandler) {
        dataHandler = pDataHandler;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
