package uk.co.phoenixlife.service.dto.email;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * EmailRequestDTO.java
 */
public class EmailRequestDTO implements Serializable {

    /** long. */
    private static final long serialVersionUID = 1L;

    private String from;
    private String to;
    private String senderName;
    private String subject;
    private String body;
    private List<EmailAttachment> attachments;

    /**
     * Gets the from.
     *
     * @return the from
     */
    public String getFrom() {
        return from;
    }

    /**
     * Sets the from.
     *
     * @param pFrom
     *            the new from
     */
    public void setFrom(final String pFrom) {
        from = pFrom;
    }

    /**
     * Gets the to.
     *
     * @return the to
     */
    public String getTo() {
        return to;
    }

    /**
     * Sets the to.
     *
     * @param pTo
     *            the new to
     */
    public void setTo(final String pTo) {
        to = pTo;
    }

    /**
     * Gets the sender name.
     *
     * @return the sender name
     */
    public String getSenderName() {
        return senderName;
    }

    /**
     * Sets the sender name.
     *
     * @param pSenderName
     *            the new sender name
     */
    public void setSenderName(final String pSenderName) {
        senderName = pSenderName;
    }

    /**
     * Gets the subject.
     *
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Sets the subject.
     *
     * @param pSubject
     *            the new subject
     */
    public void setSubject(final String pSubject) {
        subject = pSubject;
    }

    /**
     * Gets the body.
     *
     * @return the body
     */
    public String getBody() {
        return body;
    }

    /**
     * Sets the body.
     *
     * @param pBody
     *            the new body
     */
    public void setBody(final String pBody) {
        body = pBody;
    }

    /**
     * Gets the attachments
     *
     * @return the attachments
     */
    public List<EmailAttachment> getAttachments() {
        return attachments;
    }

    /**
     * Sets the attachments
     *
     * @param pAttachments
     *            the attachments to set
     */
    public void setAttachments(final List<EmailAttachment> pAttachments) {
        attachments = pAttachments;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
