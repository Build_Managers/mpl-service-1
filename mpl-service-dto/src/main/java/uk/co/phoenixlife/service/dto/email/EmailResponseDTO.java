package uk.co.phoenixlife.service.dto.email;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * EmailResponseDTO.java
 */
public class EmailResponseDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private int statusCode;
    private boolean errorFlag;
    private String message;

    /**
     * Gets the statusCode
     *
     * @return the statusCode
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * Sets the statusCode
     *
     * @param pStatusCode
     *            the statusCode to set
     */
    public void setStatusCode(final int pStatusCode) {
        statusCode = pStatusCode;
    }

    /**
     * Gets the errorFlag
     *
     * @return the errorFlag
     */
    public boolean isErrorFlag() {
        return errorFlag;
    }

    /**
     * Sets the errorFlag
     *
     * @param pErrorFlag
     *            the errorFlag to set
     */
    public void setErrorFlag(final boolean pErrorFlag) {
        errorFlag = pErrorFlag;
    }

    /**
     * Gets the message
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the message
     *
     * @param pMessage
     *            the message to set
     */
    public void setMessage(final String pMessage) {
        message = pMessage;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
