/*
 * MPLServiceErrorDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.error;

import java.io.Serializable;

/**
 * MPLServiceErrorDTO.java
 */
public class MPLServiceErrorDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String errorCode;
    private String errorDescription;
    private String requestUrl;

    /**
     * Gets the errorCode
     *
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Sets the errorCode
     *
     * @param pErrorCode
     *            the errorCode to set
     */
    public void setErrorCode(final String pErrorCode) {
        errorCode = pErrorCode;
    }

    /**
     * Gets the errorDescription
     *
     * @return the errorDescription
     */
    public String getErrorDescription() {
        return errorDescription;
    }

    /**
     * Sets the errorDescription
     *
     * @param pErrorDescription
     *            the errorDescription to set
     */
    public void setErrorDescription(final String pErrorDescription) {
        errorDescription = pErrorDescription;
    }

    /**
     * Gets the requestUrl
     *
     * @return the requestUrl
     */
    public String getRequestUrl() {
        return requestUrl;
    }

    /**
     * Sets the requestUrl
     *
     * @param pRequestUrl
     *            the requestUrl to set
     */
    public void setRequestUrl(final String pRequestUrl) {
        requestUrl = pRequestUrl;
    }

}
