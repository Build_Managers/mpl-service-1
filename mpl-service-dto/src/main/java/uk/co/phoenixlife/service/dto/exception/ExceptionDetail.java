package uk.co.phoenixlife.service.dto.exception;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

/**
 * The Class ExceptionDetail.
 */
public class ExceptionDetail implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3423643488129664692L;

    /** The Constant NEW_LINE. */
    private static final String NEW_LINE = "\n";

    /** The exception code. */
    private String exceptionCode;

    /** The service name. */
    private String serviceName;

    /**
     * This field indicates that the exception details are recorded and just
     * need to re-throw the same exception.
     */
    private boolean exceptionDetailRecorded;

    /** The exception class name. */
    private String exceptionClassName;

    /** The exception source class name. */
    private String exceptionSourceClassName;

    /** The method name. */
    private String methodName;

    /** The line number. */
    private Integer lineNumber;

    /** The cause. */
    private String cause;

    /** The stack trace. */
    private String stackTrace;

    /** The message. */
    private String shortMessage;

    /** The message. */
    private String detailMessage;

    /**
     * Gets the exception class name.
     *
     * @return the exception class name
     */
    public String getExceptionClassName() {
        return exceptionClassName;
    }

    /**
     * Sets the exception class name.
     *
     * @param mExceptionClassName
     *            the new exception class name
     */
    public void setExceptionClassName(final String mExceptionClassName) {
        exceptionClassName = mExceptionClassName;
    }

    /**
     * Gets the exception source class name.
     *
     * @return the exception source class name
     */
    public String getExceptionSourceClassName() {
        return exceptionSourceClassName;
    }

    /**
     * Sets the exception source class name.
     *
     * @param mExceptionSourceClassName
     *            the new exception source class name
     */
    public void setExceptionSourceClassName(
            final String mExceptionSourceClassName) {
        exceptionSourceClassName = mExceptionSourceClassName;
    }

    /**
     * Gets the method name.
     *
     * @return the method name
     */
    public String getMethodName() {
        return methodName;
    }

    /**
     * Sets the method name.
     *
     * @param mMethodName
     *            the new method name
     */
    public void setMethodName(final String mMethodName) {
        methodName = mMethodName;
    }

    /**
     * Gets the line number.
     *
     * @return the line number
     */
    public Integer getLineNumber() {
        return lineNumber;
    }

    /**
     * Sets the line number.
     *
     * @param mLineNumber
     *            the new line number
     */
    public void setLineNumber(final Integer mLineNumber) {
        lineNumber = mLineNumber;
    }

    /**
     * Gets the cause.
     *
     * @return the cause
     */
    public String getCause() {
        return cause;
    }

    /**
     * Sets the cause.
     *
     * @param mCause
     *            the new cause
     */
    public void setCause(final String mCause) {
        cause = mCause;
    }

    /**
     * Gets the stack trace.
     *
     * @return the stack trace
     */
    public String getStackTrace() {
        return stackTrace;
    }

    /**
     * Sets the stack trace.
     *
     * @param throwable
     *            the throwable instance
     */

    public void setStackTraceAsString(final Throwable throwable) {
        boolean lineNumberFound = false;
        for (final StackTraceElement stackTraceElement : throwable.getStackTrace()) {
            if (!lineNumberFound
                    && stackTraceElement.getClassName().equals(
                            exceptionSourceClassName)
                    && stackTraceElement.getMethodName().contains(
                            methodName)) {
                setLineNumber(stackTraceElement.getLineNumber());
                lineNumberFound = true;
            }
        }
    }

    /**
     * Sets the stack trace.
     *
     * @param mStackTrace
     *            the new stack trace
     */
    public void setStackTrace(final String mStackTrace) {
        stackTrace = mStackTrace;
    }

    /**
     * Method to return message string with stack trace.
     *
     * @return the string
     */
    public String getMessage() {
        detailMessage = getMessage(true);
        return detailMessage;
    }

    /**
     * Method to return message string with or without stack trace.
     *
     * @param includeStackTrace
     *            the include stack trace
     * @return the string
     */
    public String getMessage(final boolean includeStackTrace) {
        StringBuilder stringBuilder;
        stringBuilder = new StringBuilder();
        stringBuilder.append("Exception Code   : ").append(getExceptionCode())
                .append(NEW_LINE);
        stringBuilder.append("Service          : ").append(getServiceName())
                .append(NEW_LINE);
        stringBuilder.append("Class            : ").append(getExceptionSourceClassName())
                .append(NEW_LINE);
        stringBuilder.append("Method           : ").append(getMethodName())
                .append(NEW_LINE);
        stringBuilder.append("Line Number      : ").append(getLineNumber());
        if (StringUtils.isNotBlank(getShortMessage())) {
            stringBuilder.append(NEW_LINE);
            stringBuilder.append("Message          : ").append(getShortMessage());
        }
        if (StringUtils.isNotBlank(getCause())) {
            stringBuilder.append(NEW_LINE);
            stringBuilder.append("Cause            : ").append(getCause());
        }
        if (includeStackTrace) {
            stringBuilder.append(NEW_LINE);
            stringBuilder.append("StackTrace       : ").append(getStackTrace());
        }
        return stringBuilder.toString();
    }

    /**
     * Sets the message.
     *
     * @param mMessage
     *            the new message
     */
    public void setMessage(final String mMessage) {
        detailMessage = mMessage;
    }

    /**
     * Checks if is this field indicates that the exception details are recorded
     * and just need to re-throw the same exception.
     *
     * @return the this field indicates that the exception details are recorded
     *         and just need to re-throw the same exception
     */
    public boolean isExceptionDetailRecorded() {
        return exceptionDetailRecorded;
    }

    /**
     * Sets the this field indicates that the exception details are recorded and
     * just need to re-throw the same exception.
     *
     * @param mExceptionDetailRecorded
     *            the new this field indicates that the exception details are
     *            recorded and just need to re-throw the same exception
     */
    public void setExceptionDetailRecorded(
            final boolean mExceptionDetailRecorded) {
        exceptionDetailRecorded = mExceptionDetailRecorded;
    }

    /**
     * Gets the message.
     *
     * @return the message
     */
    public String getShortMessage() {
        return shortMessage;
    }

    /**
     * Sets the message.
     *
     * @param mShortMessage
     *            the new message
     */
    public void setShortMessage(final String mShortMessage) {
        shortMessage = mShortMessage;
    }

    /**
     * Gets the service name.
     *
     * @return the service name
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * Sets the service name.
     *
     * @param mServiceName
     *            the new service name
     */
    public void setServiceName(final String mServiceName) {
        serviceName = mServiceName;
    }

    /**
     * Gets the exception code.
     *
     * @return the exception code
     */
    public String getExceptionCode() {
        return exceptionCode;
    }

    /**
     * Sets the exception code.
     *
     * @param mExceptionCode
     *            the new exception code
     */
    public void setExceptionCode(final String mExceptionCode) {
        exceptionCode = mExceptionCode;
    }
}
