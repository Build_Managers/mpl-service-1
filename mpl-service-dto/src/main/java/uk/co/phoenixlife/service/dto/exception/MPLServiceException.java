package uk.co.phoenixlife.service.dto.exception;

import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;

/**
 * This class serves as a base class for all the web exception.
 */
public class MPLServiceException extends Exception {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 4244370526936282141L;

    /** The exception detail. */
    private final ExceptionDetail exceptionDetail = new ExceptionDetail();

    /**
     * Instantiates a new MPL exception.
     */
    public MPLServiceException() {
        super();
    }

    /**
     * Instantiates a new MPL exception.
     *
     * @param message
     *            - exception message
     */
    public MPLServiceException(final String message) {
        super(message);
    }

    /**
     * Instantiates a new MPL exception.
     *
     * @param message
     *            the message
     * @param throwable
     *            the throwable
     */
    public MPLServiceException(final String message, final Throwable throwable) {
        super(message, throwable);
    }

    /**
     * Instantiates a new MPL exception.
     *
     * @param mExceptionDetail
     *            the exception detail
     */
    public MPLServiceException(final ExceptionDetail mExceptionDetail) {
        super();
        exceptionDetail.setExceptionCode(mExceptionDetail.getExceptionCode());
        exceptionDetail.setServiceName(mExceptionDetail.getServiceName());
        exceptionDetail.setCause(mExceptionDetail.getCause());
        exceptionDetail.setExceptionClassName(mExceptionDetail
                .getExceptionClassName());
        exceptionDetail.setExceptionSourceClassName(mExceptionDetail
                .getExceptionSourceClassName());
        exceptionDetail.setExceptionDetailRecorded(mExceptionDetail
                .isExceptionDetailRecorded());
        exceptionDetail.setLineNumber(mExceptionDetail.getLineNumber());
        exceptionDetail.setMethodName(mExceptionDetail.getMethodName());
        exceptionDetail.setStackTrace(mExceptionDetail.getStackTrace());
        exceptionDetail.setShortMessage(mExceptionDetail.getShortMessage());
    }

    /**
     * Instantiates a new MPL exception.
     *
     * @param joinPoint
     *            the join point
     * @param throwable
     *            the throwable
     */
    public MPLServiceException(final JoinPoint joinPoint,
            final Throwable throwable) {
        super(throwable);
        exceptionDetail.setServiceName("MPL Service");
        exceptionDetail.setExceptionSourceClassName(joinPoint.getSignature()
                .getDeclaringTypeName());
        exceptionDetail.setMethodName(joinPoint.getSignature().getName());
        if (throwable.getCause() != null) {
            exceptionDetail.setCause(throwable.getCause().toString());
        }
        exceptionDetail.setShortMessage(throwable.getMessage());
        exceptionDetail.setStackTraceAsString(throwable);
        exceptionDetail.setExceptionClassName(this.getClass().getName());
        exceptionDetail.setExceptionDetailRecorded(true);
    }

    /**
     * Gets the exception detail.
     *
     * @return the exception detail
     */
    public ExceptionDetail getExceptionDetail() {
        return exceptionDetail;
    }

    /**
     * Generate random positive exception code to uniquely identify the
     * exception if error code is not provided. Exception is prefixed with type
     * of exception. i.e. MPLServiceException is prefixed with "RIWE".
     *
     * @param errorCode
     *            the error code
     * @return exception code
     */
    protected String generateExceptionCode(final String errorCode) {
        String exceptionCode = null;
        if (StringUtils.isBlank(errorCode)) {
            Random random;
            Integer randomExceptionCode;

            random = new Random();
            randomExceptionCode = Math.abs(random.nextInt(Integer
                    .valueOf("99999999")));
            exceptionCode = new StringBuilder()
                    .append(getExceptionCodePrefix())
                    .append(randomExceptionCode).toString();
        } else {
            exceptionCode = new StringBuilder()
                    .append(getExceptionCodePrefix()).append(errorCode)
                    .toString();
        }
        return exceptionCode;
    }

    /**
     * Gets the exception code prefix for MPL Web Exception.
     *
     * @return the exception code prefix
     */
    protected String getExceptionCodePrefix() {
        return "MPL-";
    }
}
