package uk.co.phoenixlife.service.dto.exception;

public class PolicyNotFoundException extends Exception {

    /** long */
    private static final long serialVersionUID = -2186593850524941949L;

    public PolicyNotFoundException() {
        super();
    }

    public PolicyNotFoundException(final String message) {
        super(message);
    }

}
