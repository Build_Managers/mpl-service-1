
package uk.co.phoenixlife.service.dto.pca;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Address complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Address">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrganisationName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartmentName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Line1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Line2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Line3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Line4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Line5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PostTown" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="County" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Postcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Mailsort" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Barcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsResidential" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsSmallOrganisation" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsLargeOrganisation" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="RawData" type="{PostcodeAnywhere2}AddressRawData"/>
 *         &lt;element name="GeographicData" type="{PostcodeAnywhere2}AddressGeographicData"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Address", propOrder = {
    "id",
    "organisationName",
    "departmentName",
    "line1",
    "line2",
    "line3",
    "line4",
    "line5",
    "postTown",
    "county",
    "postcode",
    "mailsort",
    "barcode",
    "isResidential",
    "isSmallOrganisation",
    "isLargeOrganisation",
    "rawData",
    "geographicData"
})
public class Address {

    @XmlElement(name = "Id")
    protected String id;
    @XmlElement(name = "OrganisationName")
    protected String organisationName;
    @XmlElement(name = "DepartmentName")
    protected String departmentName;
    @XmlElement(name = "Line1")
    protected String line1;
    @XmlElement(name = "Line2")
    protected String line2;
    @XmlElement(name = "Line3")
    protected String line3;
    @XmlElement(name = "Line4")
    protected String line4;
    @XmlElement(name = "Line5")
    protected String line5;
    @XmlElement(name = "PostTown")
    protected String postTown;
    @XmlElement(name = "County")
    protected String county;
    @XmlElement(name = "Postcode")
    protected String postcode;
    @XmlElement(name = "Mailsort")
    protected String mailsort;
    @XmlElement(name = "Barcode")
    protected String barcode;
    @XmlElement(name = "IsResidential")
    protected boolean isResidential;
    @XmlElement(name = "IsSmallOrganisation")
    protected boolean isSmallOrganisation;
    @XmlElement(name = "IsLargeOrganisation")
    protected boolean isLargeOrganisation;
    @XmlElement(name = "RawData", required = true)
    protected AddressRawData rawData;
    @XmlElement(name = "GeographicData", required = true)
    protected AddressGeographicData geographicData;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the organisationName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrganisationName() {
        return organisationName;
    }

    /**
     * Sets the value of the organisationName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrganisationName(String value) {
        this.organisationName = value;
    }

    /**
     * Gets the value of the departmentName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Sets the value of the departmentName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartmentName(String value) {
        this.departmentName = value;
    }

    /**
     * Gets the value of the line1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLine1() {
        return line1;
    }

    /**
     * Sets the value of the line1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLine1(String value) {
        this.line1 = value;
    }

    /**
     * Gets the value of the line2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLine2() {
        return line2;
    }

    /**
     * Sets the value of the line2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLine2(String value) {
        this.line2 = value;
    }

    /**
     * Gets the value of the line3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLine3() {
        return line3;
    }

    /**
     * Sets the value of the line3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLine3(String value) {
        this.line3 = value;
    }

    /**
     * Gets the value of the line4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLine4() {
        return line4;
    }

    /**
     * Sets the value of the line4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLine4(String value) {
        this.line4 = value;
    }

    /**
     * Gets the value of the line5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLine5() {
        return line5;
    }

    /**
     * Sets the value of the line5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLine5(String value) {
        this.line5 = value;
    }

    /**
     * Gets the value of the postTown property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostTown() {
        return postTown;
    }

    /**
     * Sets the value of the postTown property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostTown(String value) {
        this.postTown = value;
    }

    /**
     * Gets the value of the county property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCounty() {
        return county;
    }

    /**
     * Sets the value of the county property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCounty(String value) {
        this.county = value;
    }

    /**
     * Gets the value of the postcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * Sets the value of the postcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostcode(String value) {
        this.postcode = value;
    }

    /**
     * Gets the value of the mailsort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMailsort() {
        return mailsort;
    }

    /**
     * Sets the value of the mailsort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMailsort(String value) {
        this.mailsort = value;
    }

    /**
     * Gets the value of the barcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBarcode() {
        return barcode;
    }

    /**
     * Sets the value of the barcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBarcode(String value) {
        this.barcode = value;
    }

    /**
     * Gets the value of the isResidential property.
     * 
     */
    public boolean isIsResidential() {
        return isResidential;
    }

    /**
     * Sets the value of the isResidential property.
     * 
     */
    public void setIsResidential(boolean value) {
        this.isResidential = value;
    }

    /**
     * Gets the value of the isSmallOrganisation property.
     * 
     */
    public boolean isIsSmallOrganisation() {
        return isSmallOrganisation;
    }

    /**
     * Sets the value of the isSmallOrganisation property.
     * 
     */
    public void setIsSmallOrganisation(boolean value) {
        this.isSmallOrganisation = value;
    }

    /**
     * Gets the value of the isLargeOrganisation property.
     * 
     */
    public boolean isIsLargeOrganisation() {
        return isLargeOrganisation;
    }

    /**
     * Sets the value of the isLargeOrganisation property.
     * 
     */
    public void setIsLargeOrganisation(boolean value) {
        this.isLargeOrganisation = value;
    }

    /**
     * Gets the value of the rawData property.
     * 
     * @return
     *     possible object is
     *     {@link AddressRawData }
     *     
     */
    public AddressRawData getRawData() {
        return rawData;
    }

    /**
     * Sets the value of the rawData property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressRawData }
     *     
     */
    public void setRawData(AddressRawData value) {
        this.rawData = value;
    }

    /**
     * Gets the value of the geographicData property.
     * 
     * @return
     *     possible object is
     *     {@link AddressGeographicData }
     *     
     */
    public AddressGeographicData getGeographicData() {
        return geographicData;
    }

    /**
     * Sets the value of the geographicData property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressGeographicData }
     *     
     */
    public void setGeographicData(AddressGeographicData value) {
        this.geographicData = value;
    }

}
