
package uk.co.phoenixlife.service.dto.pca;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddressGeographicData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddressGeographicData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GridEastM" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="GridNorthM" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DistrictCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WardCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NHSCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NHSRegionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CountyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WardStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WardName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DistrictName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Objective2" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Objective2Region" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Transitional" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Longitude" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="Latitude" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="OSReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WGS84Longitude" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="WGS84Latitude" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressGeographicData", propOrder = {
    "gridEastM",
    "gridNorthM",
    "districtCode",
    "wardCode",
    "nhsCode",
    "nhsRegionCode",
    "countyCode",
    "countryCode",
    "wardStatus",
    "wardName",
    "districtName",
    "objective2",
    "objective2Region",
    "transitional",
    "longitude",
    "latitude",
    "osReference",
    "wgs84Longitude",
    "wgs84Latitude"
})
public class AddressGeographicData {

    @XmlElement(name = "GridEastM")
    protected int gridEastM;
    @XmlElement(name = "GridNorthM")
    protected int gridNorthM;
    @XmlElement(name = "DistrictCode")
    protected String districtCode;
    @XmlElement(name = "WardCode")
    protected String wardCode;
    @XmlElement(name = "NHSCode")
    protected String nhsCode;
    @XmlElement(name = "NHSRegionCode")
    protected String nhsRegionCode;
    @XmlElement(name = "CountyCode")
    protected String countyCode;
    @XmlElement(name = "CountryCode")
    protected String countryCode;
    @XmlElement(name = "WardStatus")
    protected String wardStatus;
    @XmlElement(name = "WardName")
    protected String wardName;
    @XmlElement(name = "DistrictName")
    protected String districtName;
    @XmlElement(name = "Objective2")
    protected boolean objective2;
    @XmlElement(name = "Objective2Region")
    protected String objective2Region;
    @XmlElement(name = "Transitional")
    protected boolean transitional;
    @XmlElement(name = "Longitude")
    protected double longitude;
    @XmlElement(name = "Latitude")
    protected double latitude;
    @XmlElement(name = "OSReference")
    protected String osReference;
    @XmlElement(name = "WGS84Longitude")
    protected double wgs84Longitude;
    @XmlElement(name = "WGS84Latitude")
    protected double wgs84Latitude;

    /**
     * Gets the value of the gridEastM property.
     * 
     */
    public int getGridEastM() {
        return gridEastM;
    }

    /**
     * Sets the value of the gridEastM property.
     * 
     */
    public void setGridEastM(int value) {
        this.gridEastM = value;
    }

    /**
     * Gets the value of the gridNorthM property.
     * 
     */
    public int getGridNorthM() {
        return gridNorthM;
    }

    /**
     * Sets the value of the gridNorthM property.
     * 
     */
    public void setGridNorthM(int value) {
        this.gridNorthM = value;
    }

    /**
     * Gets the value of the districtCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistrictCode() {
        return districtCode;
    }

    /**
     * Sets the value of the districtCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistrictCode(String value) {
        this.districtCode = value;
    }

    /**
     * Gets the value of the wardCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWardCode() {
        return wardCode;
    }

    /**
     * Sets the value of the wardCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWardCode(String value) {
        this.wardCode = value;
    }

    /**
     * Gets the value of the nhsCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNHSCode() {
        return nhsCode;
    }

    /**
     * Sets the value of the nhsCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNHSCode(String value) {
        this.nhsCode = value;
    }

    /**
     * Gets the value of the nhsRegionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNHSRegionCode() {
        return nhsRegionCode;
    }

    /**
     * Sets the value of the nhsRegionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNHSRegionCode(String value) {
        this.nhsRegionCode = value;
    }

    /**
     * Gets the value of the countyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountyCode() {
        return countyCode;
    }

    /**
     * Sets the value of the countyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountyCode(String value) {
        this.countyCode = value;
    }

    /**
     * Gets the value of the countryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Sets the value of the countryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    /**
     * Gets the value of the wardStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWardStatus() {
        return wardStatus;
    }

    /**
     * Sets the value of the wardStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWardStatus(String value) {
        this.wardStatus = value;
    }

    /**
     * Gets the value of the wardName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWardName() {
        return wardName;
    }

    /**
     * Sets the value of the wardName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWardName(String value) {
        this.wardName = value;
    }

    /**
     * Gets the value of the districtName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistrictName() {
        return districtName;
    }

    /**
     * Sets the value of the districtName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistrictName(String value) {
        this.districtName = value;
    }

    /**
     * Gets the value of the objective2 property.
     * 
     */
    public boolean isObjective2() {
        return objective2;
    }

    /**
     * Sets the value of the objective2 property.
     * 
     */
    public void setObjective2(boolean value) {
        this.objective2 = value;
    }

    /**
     * Gets the value of the objective2Region property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjective2Region() {
        return objective2Region;
    }

    /**
     * Sets the value of the objective2Region property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjective2Region(String value) {
        this.objective2Region = value;
    }

    /**
     * Gets the value of the transitional property.
     * 
     */
    public boolean isTransitional() {
        return transitional;
    }

    /**
     * Sets the value of the transitional property.
     * 
     */
    public void setTransitional(boolean value) {
        this.transitional = value;
    }

    /**
     * Gets the value of the longitude property.
     * 
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * Sets the value of the longitude property.
     * 
     */
    public void setLongitude(double value) {
        this.longitude = value;
    }

    /**
     * Gets the value of the latitude property.
     * 
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * Sets the value of the latitude property.
     * 
     */
    public void setLatitude(double value) {
        this.latitude = value;
    }

    /**
     * Gets the value of the osReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOSReference() {
        return osReference;
    }

    /**
     * Sets the value of the osReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOSReference(String value) {
        this.osReference = value;
    }

    /**
     * Gets the value of the wgs84Longitude property.
     * 
     */
    public double getWGS84Longitude() {
        return wgs84Longitude;
    }

    /**
     * Sets the value of the wgs84Longitude property.
     * 
     */
    public void setWGS84Longitude(double value) {
        this.wgs84Longitude = value;
    }

    /**
     * Gets the value of the wgs84Latitude property.
     * 
     */
    public double getWGS84Latitude() {
        return wgs84Latitude;
    }

    /**
     * Sets the value of the wgs84Latitude property.
     * 
     */
    public void setWGS84Latitude(double value) {
        this.wgs84Latitude = value;
    }

}
