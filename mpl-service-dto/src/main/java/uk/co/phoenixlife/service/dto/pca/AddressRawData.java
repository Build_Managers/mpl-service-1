
package uk.co.phoenixlife.service.dto.pca;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddressRawData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddressRawData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DeliveryPointSuffix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Checksum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NameOrNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubBuildingName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BuildingName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BuildingNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ThoroughfareName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ThoroughfareDescriptor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DependentThoroughfareName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DependentThoroughfareDescriptor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DoubleDependentLocality" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DependentLocality" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="POBoxNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumberOfHouseholds" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ConcatenationOperator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BuildingNameOrNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BuildingFlat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressRawData", propOrder = {
    "deliveryPointSuffix",
    "checksum",
    "nameOrNumber",
    "subBuildingName",
    "buildingName",
    "buildingNumber",
    "thoroughfareName",
    "thoroughfareDescriptor",
    "dependentThoroughfareName",
    "dependentThoroughfareDescriptor",
    "doubleDependentLocality",
    "dependentLocality",
    "poBoxNumber",
    "numberOfHouseholds",
    "concatenationOperator",
    "buildingNameOrNumber",
    "buildingFlat"
})
public class AddressRawData {

    @XmlElement(name = "DeliveryPointSuffix")
    protected String deliveryPointSuffix;
    @XmlElement(name = "Checksum")
    protected String checksum;
    @XmlElement(name = "NameOrNumber")
    protected String nameOrNumber;
    @XmlElement(name = "SubBuildingName")
    protected String subBuildingName;
    @XmlElement(name = "BuildingName")
    protected String buildingName;
    @XmlElement(name = "BuildingNumber")
    protected String buildingNumber;
    @XmlElement(name = "ThoroughfareName")
    protected String thoroughfareName;
    @XmlElement(name = "ThoroughfareDescriptor")
    protected String thoroughfareDescriptor;
    @XmlElement(name = "DependentThoroughfareName")
    protected String dependentThoroughfareName;
    @XmlElement(name = "DependentThoroughfareDescriptor")
    protected String dependentThoroughfareDescriptor;
    @XmlElement(name = "DoubleDependentLocality")
    protected String doubleDependentLocality;
    @XmlElement(name = "DependentLocality")
    protected String dependentLocality;
    @XmlElement(name = "POBoxNumber")
    protected String poBoxNumber;
    @XmlElement(name = "NumberOfHouseholds")
    protected String numberOfHouseholds;
    @XmlElement(name = "ConcatenationOperator")
    protected String concatenationOperator;
    @XmlElement(name = "BuildingNameOrNumber")
    protected String buildingNameOrNumber;
    @XmlElement(name = "BuildingFlat")
    protected String buildingFlat;

    /**
     * Gets the value of the deliveryPointSuffix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryPointSuffix() {
        return deliveryPointSuffix;
    }

    /**
     * Sets the value of the deliveryPointSuffix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryPointSuffix(String value) {
        this.deliveryPointSuffix = value;
    }

    /**
     * Gets the value of the checksum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChecksum() {
        return checksum;
    }

    /**
     * Sets the value of the checksum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChecksum(String value) {
        this.checksum = value;
    }

    /**
     * Gets the value of the nameOrNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameOrNumber() {
        return nameOrNumber;
    }

    /**
     * Sets the value of the nameOrNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameOrNumber(String value) {
        this.nameOrNumber = value;
    }

    /**
     * Gets the value of the subBuildingName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubBuildingName() {
        return subBuildingName;
    }

    /**
     * Sets the value of the subBuildingName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubBuildingName(String value) {
        this.subBuildingName = value;
    }

    /**
     * Gets the value of the buildingName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuildingName() {
        return buildingName;
    }

    /**
     * Sets the value of the buildingName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuildingName(String value) {
        this.buildingName = value;
    }

    /**
     * Gets the value of the buildingNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuildingNumber() {
        return buildingNumber;
    }

    /**
     * Sets the value of the buildingNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuildingNumber(String value) {
        this.buildingNumber = value;
    }

    /**
     * Gets the value of the thoroughfareName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThoroughfareName() {
        return thoroughfareName;
    }

    /**
     * Sets the value of the thoroughfareName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThoroughfareName(String value) {
        this.thoroughfareName = value;
    }

    /**
     * Gets the value of the thoroughfareDescriptor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThoroughfareDescriptor() {
        return thoroughfareDescriptor;
    }

    /**
     * Sets the value of the thoroughfareDescriptor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThoroughfareDescriptor(String value) {
        this.thoroughfareDescriptor = value;
    }

    /**
     * Gets the value of the dependentThoroughfareName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDependentThoroughfareName() {
        return dependentThoroughfareName;
    }

    /**
     * Sets the value of the dependentThoroughfareName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDependentThoroughfareName(String value) {
        this.dependentThoroughfareName = value;
    }

    /**
     * Gets the value of the dependentThoroughfareDescriptor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDependentThoroughfareDescriptor() {
        return dependentThoroughfareDescriptor;
    }

    /**
     * Sets the value of the dependentThoroughfareDescriptor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDependentThoroughfareDescriptor(String value) {
        this.dependentThoroughfareDescriptor = value;
    }

    /**
     * Gets the value of the doubleDependentLocality property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDoubleDependentLocality() {
        return doubleDependentLocality;
    }

    /**
     * Sets the value of the doubleDependentLocality property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDoubleDependentLocality(String value) {
        this.doubleDependentLocality = value;
    }

    /**
     * Gets the value of the dependentLocality property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDependentLocality() {
        return dependentLocality;
    }

    /**
     * Sets the value of the dependentLocality property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDependentLocality(String value) {
        this.dependentLocality = value;
    }

    /**
     * Gets the value of the poBoxNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOBoxNumber() {
        return poBoxNumber;
    }

    /**
     * Sets the value of the poBoxNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOBoxNumber(String value) {
        this.poBoxNumber = value;
    }

    /**
     * Gets the value of the numberOfHouseholds property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumberOfHouseholds() {
        return numberOfHouseholds;
    }

    /**
     * Sets the value of the numberOfHouseholds property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumberOfHouseholds(String value) {
        this.numberOfHouseholds = value;
    }

    /**
     * Gets the value of the concatenationOperator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConcatenationOperator() {
        return concatenationOperator;
    }

    /**
     * Sets the value of the concatenationOperator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConcatenationOperator(String value) {
        this.concatenationOperator = value;
    }

    /**
     * Gets the value of the buildingNameOrNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuildingNameOrNumber() {
        return buildingNameOrNumber;
    }

    /**
     * Sets the value of the buildingNameOrNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuildingNameOrNumber(String value) {
        this.buildingNameOrNumber = value;
    }

    /**
     * Gets the value of the buildingFlat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuildingFlat() {
        return buildingFlat;
    }

    /**
     * Sets the value of the buildingFlat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuildingFlat(String value) {
        this.buildingFlat = value;
    }

}
