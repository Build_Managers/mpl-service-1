
package uk.co.phoenixlife.service.dto.pca;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ByPostcodeResult" type="{PostcodeAnywhere2}InterimResults"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "byPostcodeResult"
})
@XmlRootElement(name = "ByPostcodeResponse")
public class ByPostcodeResponse {

    @XmlElement(name = "ByPostcodeResult", required = true)
    protected InterimResults byPostcodeResult;

    /**
     * Gets the value of the byPostcodeResult property.
     * 
     * @return
     *     possible object is
     *     {@link InterimResults }
     *     
     */
    public InterimResults getByPostcodeResult() {
        return byPostcodeResult;
    }

    /**
     * Sets the value of the byPostcodeResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link InterimResults }
     *     
     */
    public void setByPostcodeResult(InterimResults value) {
        this.byPostcodeResult = value;
    }

}
