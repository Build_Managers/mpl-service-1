
package uk.co.phoenixlife.service.dto.pca;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enContentType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="enContentType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="enContentStandardAddress"/>
 *     &lt;enumeration value="enContentExpandedAddress"/>
 *     &lt;enumeration value="enContentGeographicAddress"/>
 *     &lt;enumeration value="enContentGeographicOnly"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "enContentType")
@XmlEnum
public enum EnContentType {

    @XmlEnumValue("enContentStandardAddress")
    EN_CONTENT_STANDARD_ADDRESS("enContentStandardAddress"),
    @XmlEnumValue("enContentExpandedAddress")
    EN_CONTENT_EXPANDED_ADDRESS("enContentExpandedAddress"),
    @XmlEnumValue("enContentGeographicAddress")
    EN_CONTENT_GEOGRAPHIC_ADDRESS("enContentGeographicAddress"),
    @XmlEnumValue("enContentGeographicOnly")
    EN_CONTENT_GEOGRAPHIC_ONLY("enContentGeographicOnly");
    private final String value;

    EnContentType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EnContentType fromValue(String v) {
        for (EnContentType c: EnContentType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
