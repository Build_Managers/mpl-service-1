
package uk.co.phoenixlife.service.dto.pca;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enLanguage.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="enLanguage">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="enLanguageEnglish"/>
 *     &lt;enumeration value="enLanguageWelsh"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "enLanguage")
@XmlEnum
public enum EnLanguage {

    @XmlEnumValue("enLanguageEnglish")
    EN_LANGUAGE_ENGLISH("enLanguageEnglish"),
    @XmlEnumValue("enLanguageWelsh")
    EN_LANGUAGE_WELSH("enLanguageWelsh");
    private final String value;

    EnLanguage(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EnLanguage fromValue(String v) {
        for (EnLanguage c: EnLanguage.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
