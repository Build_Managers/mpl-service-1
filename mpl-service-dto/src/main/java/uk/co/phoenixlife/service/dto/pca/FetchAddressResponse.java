
package uk.co.phoenixlife.service.dto.pca;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FetchAddressResult" type="{PostcodeAnywhere2}AddressResults"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fetchAddressResult"
})
@XmlRootElement(name = "FetchAddressResponse")
public class FetchAddressResponse {

    @XmlElement(name = "FetchAddressResult", required = true)
    protected AddressResults fetchAddressResult;

    /**
     * Gets the value of the fetchAddressResult property.
     * 
     * @return
     *     possible object is
     *     {@link AddressResults }
     *     
     */
    public AddressResults getFetchAddressResult() {
        return fetchAddressResult;
    }

    /**
     * Sets the value of the fetchAddressResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressResults }
     *     
     */
    public void setFetchAddressResult(AddressResults value) {
        this.fetchAddressResult = value;
    }

}
