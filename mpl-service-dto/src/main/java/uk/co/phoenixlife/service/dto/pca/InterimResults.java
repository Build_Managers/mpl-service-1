
package uk.co.phoenixlife.service.dto.pca;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for InterimResults complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="InterimResults">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IsError" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ErrorNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ErrorMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Results" type="{PostcodeAnywhere2}ArrayOfInterimResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InterimResults", propOrder = {
        "isError",
        "errorNumber",
        "errorMessage",
        "results"
})
public class InterimResults {

    @XmlElement(name = "IsError")
    protected boolean isError;
    @XmlElement(name = "ErrorNumber")
    protected int errorNumber;
    @XmlElement(name = "ErrorMessage")
    protected String errorMessage;
    @XmlElement(name = "Results")
    protected ArrayOfInterimResult results;

    /**
     * Gets the value of the isError property.
     *
     */
    public boolean isIsError() {
        return isError;
    }

    /**
     * Sets the value of the isError property.
     *
     */
    public void setIsError(final boolean value) {
        isError = value;
    }

    /**
     * Gets the value of the errorNumber property.
     *
     */
    public int getErrorNumber() {
        return errorNumber;
    }

    /**
     * Sets the value of the errorNumber property.
     *
     */
    public void setErrorNumber(final int value) {
        errorNumber = value;
    }

    /**
     * Gets the value of the errorMessage property.
     *
     * @return possible object is {@link String }
     * 
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sets the value of the errorMessage property.
     *
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setErrorMessage(final String value) {
        errorMessage = value;
    }

    /**
     * Gets the value of the results property.
     *
     * @return possible object is {@link ArrayOfInterimResult }
     * 
     */
    public ArrayOfInterimResult getResults() {
        return results;
    }

    /**
     * Sets the value of the results property.
     *
     * @param value
     *            allowed object is {@link ArrayOfInterimResult }
     * 
     */
    public void setResults(final ArrayOfInterimResult value) {
        results = value;
    }

}
