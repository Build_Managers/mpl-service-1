
package uk.co.phoenixlife.service.dto.pca;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the postcodeanywhere2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: postcodeanywhere2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ByPostcode }
     * 
     */
    public ByPostcode createByPostcode() {
        return new ByPostcode();
    }

    /**
     * Create an instance of {@link ByPostcodeResponse }
     * 
     */
    public ByPostcodeResponse createByPostcodeResponse() {
        return new ByPostcodeResponse();
    }

    /**
     * Create an instance of {@link InterimResults }
     * 
     */
    public InterimResults createInterimResults() {
        return new InterimResults();
    }

    /**
     * Create an instance of {@link FetchAddressResponse }
     * 
     */
    public FetchAddressResponse createFetchAddressResponse() {
        return new FetchAddressResponse();
    }

    /**
     * Create an instance of {@link AddressResults }
     * 
     */
    public AddressResults createAddressResults() {
        return new AddressResults();
    }

    /**
     * Create an instance of {@link FetchAddress }
     * 
     */
    public FetchAddress createFetchAddress() {
        return new FetchAddress();
    }

    /**
     * Create an instance of {@link ArrayOfInterimResult }
     * 
     */
    public ArrayOfInterimResult createArrayOfInterimResult() {
        return new ArrayOfInterimResult();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link AddressRawData }
     * 
     */
    public AddressRawData createAddressRawData() {
        return new AddressRawData();
    }

    /**
     * Create an instance of {@link InterimResult }
     * 
     */
    public InterimResult createInterimResult() {
        return new InterimResult();
    }

    /**
     * Create an instance of {@link AddressGeographicData }
     * 
     */
    public AddressGeographicData createAddressGeographicData() {
        return new AddressGeographicData();
    }

    /**
     * Create an instance of {@link ArrayOfAddress }
     * 
     */
    public ArrayOfAddress createArrayOfAddress() {
        return new ArrayOfAddress();
    }

}
