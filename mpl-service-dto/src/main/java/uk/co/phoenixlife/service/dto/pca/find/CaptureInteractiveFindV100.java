
package uk.co.phoenixlife.service.dto.pca.find;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Container" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Origin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Countries" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Limit" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Language" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "key",
        "text",
        "container",
        "origin",
        "countries",
        "limit",
        "language"
})
@XmlRootElement(name = "Capture_Interactive_Find_v1_00")
public class CaptureInteractiveFindV100 {

    @XmlElement(name = "Key")
    protected String key;
    @XmlElement(name = "Text")
    protected String text;
    @XmlElement(name = "Container")
    protected String container;
    @XmlElement(name = "Origin")
    protected String origin;
    @XmlElement(name = "Countries")
    protected String countries;
    @XmlElement(name = "Limit")
    protected int limit;
    @XmlElement(name = "Language")
    protected String language;

    /**
     * Gets the value of the key property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets the value of the key property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Gets the value of the text property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the value of the text property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setText(String value) {
        this.text = value;
    }

    /**
     * Gets the value of the container property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getContainer() {
        return container;
    }

    /**
     * Sets the value of the container property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setContainer(String value) {
        this.container = value;
    }

    /**
     * Gets the value of the origin property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * Sets the value of the origin property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setOrigin(String value) {
        this.origin = value;
    }

    /**
     * Gets the value of the countries property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getCountries() {
        return countries;
    }

    /**
     * Sets the value of the countries property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCountries(String value) {
        this.countries = value;
    }

    /**
     * Gets the value of the limit property.
     * 
     */
    public int getLimit() {
        return limit;
    }

    /**
     * Sets the value of the limit property.
     * 
     */
    public void setLimit(int value) {
        this.limit = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setLanguage(String value) {
        this.language = value;
    }

}
