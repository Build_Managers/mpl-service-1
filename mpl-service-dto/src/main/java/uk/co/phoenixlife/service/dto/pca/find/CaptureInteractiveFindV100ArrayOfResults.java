
package uk.co.phoenixlife.service.dto.pca.find;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

// TODO: Auto-generated Javadoc
/**
 * <p>
 * Java class for Capture_Interactive_Find_v1_00_ArrayOfResults complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="Capture_Interactive_Find_v1_00_ArrayOfResults">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Capture_Interactive_Find_v1_00_Results" type="{http://services.postcodeanywhere.co.uk/}Capture_Interactive_Find_v1_00_Results" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Capture_Interactive_Find_v1_00_ArrayOfResults", propOrder = {
        "captureInteractiveFindV100Results"
})
public class CaptureInteractiveFindV100ArrayOfResults {

    /** The capture interactive find V 100 results. */
    @XmlElement(name = "Capture_Interactive_Find_v1_00_Results")
    protected List<CaptureInteractiveFindV100Results> captureInteractiveFindV100Results;

    /**
     * Gets the value of the captureInteractiveFindV100Results property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the captureInteractiveFindV100Results
     * property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getCaptureInteractiveFindV100Results().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CaptureInteractiveFindV100Results }
     *
     * @return the capture interactive find V 100 results
     */
    public List<CaptureInteractiveFindV100Results> getCaptureInteractiveFindV100Results() {
        if (captureInteractiveFindV100Results == null) {
            captureInteractiveFindV100Results = new ArrayList<CaptureInteractiveFindV100Results>();
        }
        return this.captureInteractiveFindV100Results;
    }
}
