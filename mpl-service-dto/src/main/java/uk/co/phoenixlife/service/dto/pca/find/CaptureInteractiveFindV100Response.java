
package uk.co.phoenixlife.service.dto.pca.find;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Capture_Interactive_Find_v1_00_Result" type="{http://services.postcodeanywhere.co.uk/}Capture_Interactive_Find_v1_00_ArrayOfResults" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "captureInteractiveFindV100Result"
})
@XmlRootElement(name = "Capture_Interactive_Find_v1_00_Response")
public class CaptureInteractiveFindV100Response {

    @XmlElement(name = "Capture_Interactive_Find_v1_00_Result")
    protected CaptureInteractiveFindV100ArrayOfResults captureInteractiveFindV100Result;

    /**
     * Gets the value of the captureInteractiveFindV100Result property.
     * 
     * @return possible object is
     *         {@link CaptureInteractiveFindV100ArrayOfResults }
     * 
     */
    public CaptureInteractiveFindV100ArrayOfResults getCaptureInteractiveFindV100Result() {
        return captureInteractiveFindV100Result;
    }

    /**
     * Sets the value of the captureInteractiveFindV100Result property.
     * 
     * @param value
     *            allowed object is
     *            {@link CaptureInteractiveFindV100ArrayOfResults }
     * 
     */
    public void setCaptureInteractiveFindV100Result(CaptureInteractiveFindV100ArrayOfResults value) {
        this.captureInteractiveFindV100Result = value;
    }

}
