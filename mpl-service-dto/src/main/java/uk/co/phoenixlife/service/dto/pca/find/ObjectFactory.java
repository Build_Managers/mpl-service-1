
package uk.co.phoenixlife.service.dto.pca.find;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the pcapredict.retrieve package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    /**
     * Create a new ObjectFactory that can be used to create new instances of
     * schema derived classes for package: pcapredict.retrieve
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CaptureInteractiveFindV100 }
     * 
     */
    public CaptureInteractiveFindV100 createCaptureInteractiveFindV100() {
        return new CaptureInteractiveFindV100();
    }

    /**
     * Create an instance of {@link CaptureInteractiveFindV100Response }
     * 
     */
    public CaptureInteractiveFindV100Response createCaptureInteractiveFindV100Response() {
        return new CaptureInteractiveFindV100Response();
    }

    /**
     * Create an instance of {@link CaptureInteractiveFindV100Results }
     * 
     */
    public CaptureInteractiveFindV100Results createCaptureInteractiveFindV100Results() {
        return new CaptureInteractiveFindV100Results();
    }

    /**
     * Create an instance of {@link CaptureInteractiveFindV100ArrayOfResults }
     * 
     */
    public CaptureInteractiveFindV100ArrayOfResults createCaptureInteractiveFindV100ArrayOfResults() {
        return new CaptureInteractiveFindV100ArrayOfResults();
    }

}
