
package uk.co.phoenixlife.service.dto.pca.find;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by the JAX-WS RI. JAX-WS RI 2.1.7-b01- Generated
 * source version: 2.1
 * 
 */
@WebService(name = "PostcodeAnywhere_Soap", targetNamespace = "http://services.postcodeanywhere.co.uk/")
@XmlSeeAlso({ ObjectFactory.class })
public interface PostcodeAnywhereSoap {

	/**
	 * 
	 * @param container
	 * @param origin
	 * @param limit
	 * @param language
	 * @param text
	 * @param countries
	 * @param key
	 * @return returns
	 *         pcapredict.retrieve.CaptureInteractiveFindV100ArrayOfResults
	 */
	@WebMethod(operationName = "Capture_Interactive_Find_v1_00", action = "http://services.postcodeanywhere.co.uk/Capture_Interactive_Find_v1_00")
	@WebResult(name = "Capture_Interactive_Find_v1_00_Result", targetNamespace = "http://services.postcodeanywhere.co.uk/")
	@RequestWrapper(localName = "Capture_Interactive_Find_v1_00", targetNamespace = "http://services.postcodeanywhere.co.uk/", className = "uk.co.phoenixlife.service.dto.pca.find.CaptureInteractiveFindV100")
	@ResponseWrapper(localName = "Capture_Interactive_Find_v1_00_Response", targetNamespace = "http://services.postcodeanywhere.co.uk/", className = "uk.co.phoenixlife.service.dto.pca.find.CaptureInteractiveFindV100Response")
	public CaptureInteractiveFindV100ArrayOfResults captureInteractiveFindV100(
			@WebParam(name = "Key", targetNamespace = "http://services.postcodeanywhere.co.uk/") String key,
			@WebParam(name = "Text", targetNamespace = "http://services.postcodeanywhere.co.uk/") String text,
			@WebParam(name = "Container", targetNamespace = "http://services.postcodeanywhere.co.uk/") String container,
			@WebParam(name = "Origin", targetNamespace = "http://services.postcodeanywhere.co.uk/") String origin,
			@WebParam(name = "Countries", targetNamespace = "http://services.postcodeanywhere.co.uk/") String countries,
			@WebParam(name = "Limit", targetNamespace = "http://services.postcodeanywhere.co.uk/") int limit,
			@WebParam(name = "Language", targetNamespace = "http://services.postcodeanywhere.co.uk/") String language);

}
