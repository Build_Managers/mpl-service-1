/**
 * This web service
 * 		provides access to all 26 million UK addresses (check out
 * 		services.postcodeanywhere.co.uk/us/lookup.asmx for US addresses).
 * 		Before using this service, you should register at
 * 		www.postcodeanywhere.co.uk for an account code and license key. The
 * 		MachineId parameter should be blank unless you are using the web
 * 		service with a user license key (rather than a click / website license
 * 		key). If you have any queries, call us on +44 (0) 870 241 3310 or
 * 		email on developers@postcodeanywhere.co.uk
 * 
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "PostcodeAnywhere2", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package uk.co.phoenixlife.service.dto.pca;
