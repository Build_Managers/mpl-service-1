
package uk.co.phoenixlife.service.dto.pca.retrieve;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field1Format" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field2Format" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field3Format" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field4Format" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field5Format" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field6Format" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field7Format" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field8Format" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field9Format" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field10Format" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field11Format" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field12Format" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field13Format" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field14Format" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field15Format" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field16Format" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field17Format" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field18Format" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field19Format" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field20Format" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "key",
    "id",
    "field1Format",
    "field2Format",
    "field3Format",
    "field4Format",
    "field5Format",
    "field6Format",
    "field7Format",
    "field8Format",
    "field9Format",
    "field10Format",
    "field11Format",
    "field12Format",
    "field13Format",
    "field14Format",
    "field15Format",
    "field16Format",
    "field17Format",
    "field18Format",
    "field19Format",
    "field20Format"
})
@XmlRootElement(name = "Capture_Interactive_Retrieve_v1_00")
public class CaptureInteractiveRetrieveV100 {

    @XmlElement(name = "Key")
    protected String key;
    @XmlElement(name = "Id")
    protected String id;
    @XmlElement(name = "Field1Format")
    protected String field1Format;
    @XmlElement(name = "Field2Format")
    protected String field2Format;
    @XmlElement(name = "Field3Format")
    protected String field3Format;
    @XmlElement(name = "Field4Format")
    protected String field4Format;
    @XmlElement(name = "Field5Format")
    protected String field5Format;
    @XmlElement(name = "Field6Format")
    protected String field6Format;
    @XmlElement(name = "Field7Format")
    protected String field7Format;
    @XmlElement(name = "Field8Format")
    protected String field8Format;
    @XmlElement(name = "Field9Format")
    protected String field9Format;
    @XmlElement(name = "Field10Format")
    protected String field10Format;
    @XmlElement(name = "Field11Format")
    protected String field11Format;
    @XmlElement(name = "Field12Format")
    protected String field12Format;
    @XmlElement(name = "Field13Format")
    protected String field13Format;
    @XmlElement(name = "Field14Format")
    protected String field14Format;
    @XmlElement(name = "Field15Format")
    protected String field15Format;
    @XmlElement(name = "Field16Format")
    protected String field16Format;
    @XmlElement(name = "Field17Format")
    protected String field17Format;
    @XmlElement(name = "Field18Format")
    protected String field18Format;
    @XmlElement(name = "Field19Format")
    protected String field19Format;
    @XmlElement(name = "Field20Format")
    protected String field20Format;

    /**
     * Gets the value of the key property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets the value of the key property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the field1Format property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getField1Format() {
        return field1Format;
    }

    /**
     * Sets the value of the field1Format property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setField1Format(String value) {
        this.field1Format = value;
    }

    /**
     * Gets the value of the field2Format property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getField2Format() {
        return field2Format;
    }

    /**
     * Sets the value of the field2Format property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setField2Format(String value) {
        this.field2Format = value;
    }

    /**
     * Gets the value of the field3Format property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getField3Format() {
        return field3Format;
    }

    /**
     * Sets the value of the field3Format property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setField3Format(String value) {
        this.field3Format = value;
    }

    /**
     * Gets the value of the field4Format property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getField4Format() {
        return field4Format;
    }

    /**
     * Sets the value of the field4Format property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setField4Format(String value) {
        this.field4Format = value;
    }

    /**
     * Gets the value of the field5Format property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getField5Format() {
        return field5Format;
    }

    /**
     * Sets the value of the field5Format property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setField5Format(String value) {
        this.field5Format = value;
    }

    /**
     * Gets the value of the field6Format property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getField6Format() {
        return field6Format;
    }

    /**
     * Sets the value of the field6Format property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setField6Format(String value) {
        this.field6Format = value;
    }

    /**
     * Gets the value of the field7Format property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getField7Format() {
        return field7Format;
    }

    /**
     * Sets the value of the field7Format property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setField7Format(String value) {
        this.field7Format = value;
    }

    /**
     * Gets the value of the field8Format property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getField8Format() {
        return field8Format;
    }

    /**
     * Sets the value of the field8Format property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setField8Format(String value) {
        this.field8Format = value;
    }

    /**
     * Gets the value of the field9Format property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getField9Format() {
        return field9Format;
    }

    /**
     * Sets the value of the field9Format property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setField9Format(String value) {
        this.field9Format = value;
    }

    /**
     * Gets the value of the field10Format property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getField10Format() {
        return field10Format;
    }

    /**
     * Sets the value of the field10Format property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setField10Format(String value) {
        this.field10Format = value;
    }

    /**
     * Gets the value of the field11Format property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getField11Format() {
        return field11Format;
    }

    /**
     * Sets the value of the field11Format property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setField11Format(String value) {
        this.field11Format = value;
    }

    /**
     * Gets the value of the field12Format property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getField12Format() {
        return field12Format;
    }

    /**
     * Sets the value of the field12Format property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setField12Format(String value) {
        this.field12Format = value;
    }

    /**
     * Gets the value of the field13Format property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getField13Format() {
        return field13Format;
    }

    /**
     * Sets the value of the field13Format property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setField13Format(String value) {
        this.field13Format = value;
    }

    /**
     * Gets the value of the field14Format property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getField14Format() {
        return field14Format;
    }

    /**
     * Sets the value of the field14Format property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setField14Format(String value) {
        this.field14Format = value;
    }

    /**
     * Gets the value of the field15Format property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getField15Format() {
        return field15Format;
    }

    /**
     * Sets the value of the field15Format property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setField15Format(String value) {
        this.field15Format = value;
    }

    /**
     * Gets the value of the field16Format property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getField16Format() {
        return field16Format;
    }

    /**
     * Sets the value of the field16Format property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setField16Format(String value) {
        this.field16Format = value;
    }

    /**
     * Gets the value of the field17Format property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getField17Format() {
        return field17Format;
    }

    /**
     * Sets the value of the field17Format property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setField17Format(String value) {
        this.field17Format = value;
    }

    /**
     * Gets the value of the field18Format property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getField18Format() {
        return field18Format;
    }

    /**
     * Sets the value of the field18Format property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setField18Format(String value) {
        this.field18Format = value;
    }

    /**
     * Gets the value of the field19Format property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getField19Format() {
        return field19Format;
    }

    /**
     * Sets the value of the field19Format property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setField19Format(String value) {
        this.field19Format = value;
    }

    /**
     * Gets the value of the field20Format property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getField20Format() {
        return field20Format;
    }

    /**
     * Sets the value of the field20Format property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setField20Format(String value) {
        this.field20Format = value;
    }

}
