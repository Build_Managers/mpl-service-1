
package uk.co.phoenixlife.service.dto.pca.retrieve;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Capture_Interactive_Retrieve_v1_00_ArrayOfResults complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Capture_Interactive_Retrieve_v1_00_ArrayOfResults">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Capture_Interactive_Retrieve_v1_00_Results" type="{http://services.postcodeanywhere.co.uk/}Capture_Interactive_Retrieve_v1_00_Results" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Capture_Interactive_Retrieve_v1_00_ArrayOfResults", propOrder = {
    "captureInteractiveRetrieveV100Results"
})
public class CaptureInteractiveRetrieveV100ArrayOfResults {

    @XmlElement(name = "Capture_Interactive_Retrieve_v1_00_Results")
    protected List<CaptureInteractiveRetrieveV100Results> captureInteractiveRetrieveV100Results;

    /**
     * Gets the value of the captureInteractiveRetrieveV100Results property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the captureInteractiveRetrieveV100Results property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCaptureInteractiveRetrieveV100Results().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CaptureInteractiveRetrieveV100Results }
     * 
     * 
     */
    public List<CaptureInteractiveRetrieveV100Results> getCaptureInteractiveRetrieveV100Results() {
        if (captureInteractiveRetrieveV100Results == null) {
            captureInteractiveRetrieveV100Results = new ArrayList<CaptureInteractiveRetrieveV100Results>();
        }
        return this.captureInteractiveRetrieveV100Results;
    }

}
