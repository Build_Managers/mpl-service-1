
package uk.co.phoenixlife.service.dto.pca.retrieve;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Capture_Interactive_Retrieve_v1_00_Result" type="{http://services.postcodeanywhere.co.uk/}Capture_Interactive_Retrieve_v1_00_ArrayOfResults" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "captureInteractiveRetrieveV100Result"
})
@XmlRootElement(name = "Capture_Interactive_Retrieve_v1_00_Response")
public class CaptureInteractiveRetrieveV100Response {

    @XmlElement(name = "Capture_Interactive_Retrieve_v1_00_Result")
    protected CaptureInteractiveRetrieveV100ArrayOfResults captureInteractiveRetrieveV100Result;

    /**
     * Gets the value of the captureInteractiveRetrieveV100Result property.
     * 
     * @return
     *     possible object is
     *     {@link CaptureInteractiveRetrieveV100ArrayOfResults }
     *     
     */
    public CaptureInteractiveRetrieveV100ArrayOfResults getCaptureInteractiveRetrieveV100Result() {
        return captureInteractiveRetrieveV100Result;
    }

    /**
     * Sets the value of the captureInteractiveRetrieveV100Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link CaptureInteractiveRetrieveV100ArrayOfResults }
     *     
     */
    public void setCaptureInteractiveRetrieveV100Result(CaptureInteractiveRetrieveV100ArrayOfResults value) {
        this.captureInteractiveRetrieveV100Result = value;
    }

}
