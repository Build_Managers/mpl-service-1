
package uk.co.phoenixlife.service.dto.pca.retrieve;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for Capture_Interactive_Retrieve_v1_00_Results complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="Capture_Interactive_Retrieve_v1_00_Results">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DomesticId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Language" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LanguageAlternatives" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Department" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Company" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubBuilding" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BuildingNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BuildingName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SecondaryStreet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Street" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Block" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Neighbourhood" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="District" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Line1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Line2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Line3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Line4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Line5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AdminAreaName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AdminAreaCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Province" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProvinceName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProvinceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CountryName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CountryIso2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CountryIso3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CountryIsoNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="SortingNumber1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SortingNumber2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Barcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="POBoxNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Label" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Field20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Capture_Interactive_Retrieve_v1_00_Results", propOrder = {
        "id",
        "domesticId",
        "language",
        "languageAlternatives",
        "department",
        "company",
        "subBuilding",
        "buildingNumber",
        "buildingName",
        "secondaryStreet",
        "street",
        "block",
        "neighbourhood",
        "district",
        "city",
        "line1",
        "line2",
        "line3",
        "line4",
        "line5",
        "adminAreaName",
        "adminAreaCode",
        "province",
        "provinceName",
        "provinceCode",
        "postalCode",
        "countryName",
        "countryIso2",
        "countryIso3",
        "countryIsoNumber",
        "sortingNumber1",
        "sortingNumber2",
        "barcode",
        "poBoxNumber",
        "label",
        "type",
        "dataLevel",
        "field1",
        "field2",
        "field3",
        "field4",
        "field5",
        "field6",
        "field7",
        "field8",
        "field9",
        "field10",
        "field11",
        "field12",
        "field13",
        "field14",
        "field15",
        "field16",
        "field17",
        "field18",
        "field19",
        "field20"
})
public class CaptureInteractiveRetrieveV100Results {

    @XmlElement(name = "Id")
    protected String id;
    @XmlElement(name = "DomesticId")
    protected String domesticId;
    @XmlElement(name = "Language")
    protected String language;
    @XmlElement(name = "LanguageAlternatives")
    protected String languageAlternatives;
    @XmlElement(name = "Department")
    protected String department;
    @XmlElement(name = "Company")
    protected String company;
    @XmlElement(name = "SubBuilding")
    protected String subBuilding;
    @XmlElement(name = "BuildingNumber")
    protected String buildingNumber;
    @XmlElement(name = "BuildingName")
    protected String buildingName;
    @XmlElement(name = "SecondaryStreet")
    protected String secondaryStreet;
    @XmlElement(name = "Street")
    protected String street;
    @XmlElement(name = "Block")
    protected String block;
    @XmlElement(name = "Neighbourhood")
    protected String neighbourhood;
    @XmlElement(name = "District")
    protected String district;
    @XmlElement(name = "City")
    protected String city;
    @XmlElement(name = "Line1")
    protected String line1;
    @XmlElement(name = "Line2")
    protected String line2;
    @XmlElement(name = "Line3")
    protected String line3;
    @XmlElement(name = "Line4")
    protected String line4;
    @XmlElement(name = "Line5")
    protected String line5;
    @XmlElement(name = "AdminAreaName")
    protected String adminAreaName;
    @XmlElement(name = "AdminAreaCode")
    protected String adminAreaCode;
    @XmlElement(name = "Province")
    protected String province;
    @XmlElement(name = "ProvinceName")
    protected String provinceName;
    @XmlElement(name = "ProvinceCode")
    protected String provinceCode;
    @XmlElement(name = "PostalCode")
    protected String postalCode;
    @XmlElement(name = "CountryName")
    protected String countryName;
    @XmlElement(name = "CountryIso2")
    protected String countryIso2;
    @XmlElement(name = "CountryIso3")
    protected String countryIso3;
    @XmlElement(name = "CountryIsoNumber")
    protected int countryIsoNumber;
    @XmlElement(name = "SortingNumber1")
    protected String sortingNumber1;
    @XmlElement(name = "SortingNumber2")
    protected String sortingNumber2;
    @XmlElement(name = "Barcode")
    protected String barcode;
    @XmlElement(name = "POBoxNumber")
    protected String poBoxNumber;
    @XmlElement(name = "Label")
    protected String label;
    @XmlElement(name = "Type")
    protected String type;
    @XmlElement(name = "DataLevel")
    protected String dataLevel;
    @XmlElement(name = "Field1")
    protected String field1;
    @XmlElement(name = "Field2")
    protected String field2;
    @XmlElement(name = "Field3")
    protected String field3;
    @XmlElement(name = "Field4")
    protected String field4;
    @XmlElement(name = "Field5")
    protected String field5;
    @XmlElement(name = "Field6")
    protected String field6;
    @XmlElement(name = "Field7")
    protected String field7;
    @XmlElement(name = "Field8")
    protected String field8;
    @XmlElement(name = "Field9")
    protected String field9;
    @XmlElement(name = "Field10")
    protected String field10;
    @XmlElement(name = "Field11")
    protected String field11;
    @XmlElement(name = "Field12")
    protected String field12;
    @XmlElement(name = "Field13")
    protected String field13;
    @XmlElement(name = "Field14")
    protected String field14;
    @XmlElement(name = "Field15")
    protected String field15;
    @XmlElement(name = "Field16")
    protected String field16;
    @XmlElement(name = "Field17")
    protected String field17;
    @XmlElement(name = "Field18")
    protected String field18;
    @XmlElement(name = "Field19")
    protected String field19;
    @XmlElement(name = "Field20")
    protected String field20;

    /**
     * Gets the value of the id property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the domesticId property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getDomesticId() {
        return domesticId;
    }

    /**
     * Sets the value of the domesticId property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setDomesticId(String value) {
        this.domesticId = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the languageAlternatives property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getLanguageAlternatives() {
        return languageAlternatives;
    }

    /**
     * Sets the value of the languageAlternatives property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setLanguageAlternatives(String value) {
        this.languageAlternatives = value;
    }

    /**
     * Gets the value of the department property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getDepartment() {
        return department;
    }

    /**
     * Sets the value of the department property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setDepartment(String value) {
        this.department = value;
    }

    /**
     * Gets the value of the company property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getCompany() {
        return company;
    }

    /**
     * Sets the value of the company property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCompany(String value) {
        this.company = value;
    }

    /**
     * Gets the value of the subBuilding property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getSubBuilding() {
        return subBuilding;
    }

    /**
     * Sets the value of the subBuilding property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setSubBuilding(String value) {
        this.subBuilding = value;
    }

    /**
     * Gets the value of the buildingNumber property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getBuildingNumber() {
        return buildingNumber;
    }

    /**
     * Sets the value of the buildingNumber property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setBuildingNumber(String value) {
        this.buildingNumber = value;
    }

    /**
     * Gets the value of the buildingName property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getBuildingName() {
        return buildingName;
    }

    /**
     * Sets the value of the buildingName property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setBuildingName(String value) {
        this.buildingName = value;
    }

    /**
     * Gets the value of the secondaryStreet property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getSecondaryStreet() {
        return secondaryStreet;
    }

    /**
     * Sets the value of the secondaryStreet property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setSecondaryStreet(String value) {
        this.secondaryStreet = value;
    }

    /**
     * Gets the value of the street property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getStreet() {
        return street;
    }

    /**
     * Sets the value of the street property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setStreet(String value) {
        this.street = value;
    }

    /**
     * Gets the value of the block property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getBlock() {
        return block;
    }

    /**
     * Sets the value of the block property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setBlock(String value) {
        this.block = value;
    }

    /**
     * Gets the value of the neighbourhood property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getNeighbourhood() {
        return neighbourhood;
    }

    /**
     * Sets the value of the neighbourhood property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setNeighbourhood(String value) {
        this.neighbourhood = value;
    }

    /**
     * Gets the value of the district property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getDistrict() {
        return district;
    }

    /**
     * Sets the value of the district property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setDistrict(String value) {
        this.district = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Gets the value of the line1 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getLine1() {
        return line1;
    }

    /**
     * Sets the value of the line1 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setLine1(String value) {
        this.line1 = value;
    }

    /**
     * Gets the value of the line2 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getLine2() {
        return line2;
    }

    /**
     * Sets the value of the line2 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setLine2(String value) {
        this.line2 = value;
    }

    /**
     * Gets the value of the line3 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getLine3() {
        return line3;
    }

    /**
     * Sets the value of the line3 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setLine3(String value) {
        this.line3 = value;
    }

    /**
     * Gets the value of the line4 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getLine4() {
        return line4;
    }

    /**
     * Sets the value of the line4 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setLine4(String value) {
        this.line4 = value;
    }

    /**
     * Gets the value of the line5 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getLine5() {
        return line5;
    }

    /**
     * Sets the value of the line5 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setLine5(String value) {
        this.line5 = value;
    }

    /**
     * Gets the value of the adminAreaName property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getAdminAreaName() {
        return adminAreaName;
    }

    /**
     * Sets the value of the adminAreaName property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setAdminAreaName(String value) {
        this.adminAreaName = value;
    }

    /**
     * Gets the value of the adminAreaCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getAdminAreaCode() {
        return adminAreaCode;
    }

    /**
     * Sets the value of the adminAreaCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setAdminAreaCode(String value) {
        this.adminAreaCode = value;
    }

    /**
     * Gets the value of the province property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getProvince() {
        return province;
    }

    /**
     * Sets the value of the province property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setProvince(String value) {
        this.province = value;
    }

    /**
     * Gets the value of the provinceName property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getProvinceName() {
        return provinceName;
    }

    /**
     * Sets the value of the provinceName property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setProvinceName(String value) {
        this.provinceName = value;
    }

    /**
     * Gets the value of the provinceCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getProvinceCode() {
        return provinceCode;
    }

    /**
     * Sets the value of the provinceCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setProvinceCode(String value) {
        this.provinceCode = value;
    }

    /**
     * Gets the value of the postalCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Sets the value of the postalCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setPostalCode(String value) {
        this.postalCode = value;
    }

    /**
     * Gets the value of the countryName property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getCountryName() {
        return countryName;
    }

    /**
     * Sets the value of the countryName property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCountryName(String value) {
        this.countryName = value;
    }

    /**
     * Gets the value of the countryIso2 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getCountryIso2() {
        return countryIso2;
    }

    /**
     * Sets the value of the countryIso2 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCountryIso2(String value) {
        this.countryIso2 = value;
    }

    /**
     * Gets the value of the countryIso3 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getCountryIso3() {
        return countryIso3;
    }

    /**
     * Sets the value of the countryIso3 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCountryIso3(String value) {
        this.countryIso3 = value;
    }

    /**
     * Gets the value of the countryIsoNumber property.
     * 
     */
    public int getCountryIsoNumber() {
        return countryIsoNumber;
    }

    /**
     * Sets the value of the countryIsoNumber property.
     * 
     */
    public void setCountryIsoNumber(int value) {
        this.countryIsoNumber = value;
    }

    /**
     * Gets the value of the sortingNumber1 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getSortingNumber1() {
        return sortingNumber1;
    }

    /**
     * Sets the value of the sortingNumber1 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setSortingNumber1(String value) {
        this.sortingNumber1 = value;
    }

    /**
     * Gets the value of the sortingNumber2 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getSortingNumber2() {
        return sortingNumber2;
    }

    /**
     * Sets the value of the sortingNumber2 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setSortingNumber2(String value) {
        this.sortingNumber2 = value;
    }

    /**
     * Gets the value of the barcode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getBarcode() {
        return barcode;
    }

    /**
     * Sets the value of the barcode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setBarcode(String value) {
        this.barcode = value;
    }

    /**
     * Gets the value of the poBoxNumber property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getPOBoxNumber() {
        return poBoxNumber;
    }

    /**
     * Sets the value of the poBoxNumber property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setPOBoxNumber(String value) {
        this.poBoxNumber = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the dataLevel property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getDataLevel() {
        return dataLevel;
    }

    /**
     * Sets the value of the dataLevel property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setDataLevel(String value) {
        this.dataLevel = value;
    }

    /**
     * Gets the value of the field1 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getField1() {
        return field1;
    }

    /**
     * Sets the value of the field1 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setField1(String value) {
        this.field1 = value;
    }

    /**
     * Gets the value of the field2 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getField2() {
        return field2;
    }

    /**
     * Sets the value of the field2 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setField2(String value) {
        this.field2 = value;
    }

    /**
     * Gets the value of the field3 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getField3() {
        return field3;
    }

    /**
     * Sets the value of the field3 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setField3(String value) {
        this.field3 = value;
    }

    /**
     * Gets the value of the field4 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getField4() {
        return field4;
    }

    /**
     * Sets the value of the field4 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setField4(String value) {
        this.field4 = value;
    }

    /**
     * Gets the value of the field5 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getField5() {
        return field5;
    }

    /**
     * Sets the value of the field5 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setField5(String value) {
        this.field5 = value;
    }

    /**
     * Gets the value of the field6 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getField6() {
        return field6;
    }

    /**
     * Sets the value of the field6 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setField6(String value) {
        this.field6 = value;
    }

    /**
     * Gets the value of the field7 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getField7() {
        return field7;
    }

    /**
     * Sets the value of the field7 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setField7(String value) {
        this.field7 = value;
    }

    /**
     * Gets the value of the field8 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getField8() {
        return field8;
    }

    /**
     * Sets the value of the field8 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setField8(String value) {
        this.field8 = value;
    }

    /**
     * Gets the value of the field9 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getField9() {
        return field9;
    }

    /**
     * Sets the value of the field9 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setField9(String value) {
        this.field9 = value;
    }

    /**
     * Gets the value of the field10 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getField10() {
        return field10;
    }

    /**
     * Sets the value of the field10 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setField10(String value) {
        this.field10 = value;
    }

    /**
     * Gets the value of the field11 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getField11() {
        return field11;
    }

    /**
     * Sets the value of the field11 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setField11(String value) {
        this.field11 = value;
    }

    /**
     * Gets the value of the field12 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getField12() {
        return field12;
    }

    /**
     * Sets the value of the field12 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setField12(String value) {
        this.field12 = value;
    }

    /**
     * Gets the value of the field13 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getField13() {
        return field13;
    }

    /**
     * Sets the value of the field13 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setField13(String value) {
        this.field13 = value;
    }

    /**
     * Gets the value of the field14 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getField14() {
        return field14;
    }

    /**
     * Sets the value of the field14 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setField14(String value) {
        this.field14 = value;
    }

    /**
     * Gets the value of the field15 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getField15() {
        return field15;
    }

    /**
     * Sets the value of the field15 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setField15(String value) {
        this.field15 = value;
    }

    /**
     * Gets the value of the field16 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getField16() {
        return field16;
    }

    /**
     * Sets the value of the field16 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setField16(String value) {
        this.field16 = value;
    }

    /**
     * Gets the value of the field17 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getField17() {
        return field17;
    }

    /**
     * Sets the value of the field17 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setField17(String value) {
        this.field17 = value;
    }

    /**
     * Gets the value of the field18 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getField18() {
        return field18;
    }

    /**
     * Sets the value of the field18 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setField18(String value) {
        this.field18 = value;
    }

    /**
     * Gets the value of the field19 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getField19() {
        return field19;
    }

    /**
     * Sets the value of the field19 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setField19(String value) {
        this.field19 = value;
    }

    /**
     * Gets the value of the field20 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getField20() {
        return field20;
    }

    /**
     * Sets the value of the field20 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setField20(String value) {
        this.field20 = value;
    }

    @Override
    public String toString() {
        return "CaptureInteractiveRetrieveV100Results [id=" + id + ", domesticId=" + domesticId + ", language=" + language
                + ", languageAlternatives=" + languageAlternatives + ", department=" + department + ", company=" + company
                + ", subBuilding=" + subBuilding + ", buildingNumber=" + buildingNumber + ", buildingName=" + buildingName
                + ", secondaryStreet=" + secondaryStreet + ", street=" + street + ", block=" + block + ", neighbourhood="
                + neighbourhood + ", district=" + district + ", city=" + city + ", line1=" + line1 + ", line2=" + line2
                + ", line3=" + line3 + ", line4=" + line4 + ", line5=" + line5 + ", adminAreaName=" + adminAreaName
                + ", adminAreaCode=" + adminAreaCode + ", province=" + province + ", provinceName=" + provinceName
                + ", provinceCode=" + provinceCode + ", postalCode=" + postalCode + ", countryName=" + countryName
                + ", countryIso2=" + countryIso2 + ", countryIso3=" + countryIso3 + ", countryIsoNumber=" + countryIsoNumber
                + ", sortingNumber1=" + sortingNumber1 + ", sortingNumber2=" + sortingNumber2 + ", barcode=" + barcode
                + ", poBoxNumber=" + poBoxNumber + ", label=" + label + ", type=" + type + "]";
    }

}
