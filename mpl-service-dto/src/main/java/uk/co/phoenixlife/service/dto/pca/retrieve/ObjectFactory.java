
package uk.co.phoenixlife.service.dto.pca.retrieve;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the pcapredict.retrieve package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: pcapredict.retrieve
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CaptureInteractiveRetrieveV100ArrayOfResults }
     * 
     */
    public CaptureInteractiveRetrieveV100ArrayOfResults createCaptureInteractiveRetrieveV100ArrayOfResults() {
        return new CaptureInteractiveRetrieveV100ArrayOfResults();
    }

    /**
     * Create an instance of {@link CaptureInteractiveRetrieveV100Response }
     * 
     */
    public CaptureInteractiveRetrieveV100Response createCaptureInteractiveRetrieveV100Response() {
        return new CaptureInteractiveRetrieveV100Response();
    }

    /**
     * Create an instance of {@link CaptureInteractiveRetrieveV100 }
     * 
     */
    public CaptureInteractiveRetrieveV100 createCaptureInteractiveRetrieveV100() {
        return new CaptureInteractiveRetrieveV100();
    }

    /**
     * Create an instance of {@link CaptureInteractiveRetrieveV100Results }
     * 
     */
    public CaptureInteractiveRetrieveV100Results createCaptureInteractiveRetrieveV100Results() {
        return new CaptureInteractiveRetrieveV100Results();
    }

}
