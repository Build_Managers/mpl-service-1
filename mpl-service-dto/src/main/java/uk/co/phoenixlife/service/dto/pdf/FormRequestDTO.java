/*
 * FormRequestDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.pdf;

import java.io.Serializable;

import uk.co.phoenixlife.service.dto.policy.BankDetailsNinoDTO;
import uk.co.phoenixlife.service.dto.policy.FinalDashboardDTO;
import uk.co.phoenixlife.service.dto.policy.PartyPolicyDTO;

/**
 * FormRequestDTO.java
 */
public class FormRequestDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String documentName;
    private Long policyId;
    private String polNum;
    private String docType;
    private FinalDashboardDTO finalDashboard;
    private Boolean isSmallPot;
    private Boolean contactCustomer;
    private Boolean pensionGuidance;
    private Boolean pensionAdvise;
    private BankDetailsNinoDTO bankDetailsNinoDTO;
    private String pCLS;
    private String tax;
    private Double totValue;
    private Double amountPayable;
    private Double mvaValue;
    private Double penaltyValue;
    private Double earlyExitCharge;
    private Double estimatedPensionFund;
    private String mvrValue;
    private StringBuffer dobWithSuffix;
    private String primaryConactNum;
    private String secConactNum;
    private String additonalConactNum;
    private String eecValue;
    private PartyPolicyDTO partyPolicyDTO;

    /**
     * Gets the partyPolicyDTO
     *
     * @return the partyPolicyDTO
     */
    public PartyPolicyDTO getPartyPolicyDTO() {
        return partyPolicyDTO;
    }

    /**
     * Sets the partyPolicyDTO
     *
     * @param pPartyPolicyDTO
     *            the partyPolicyDTO to set
     */
    public void setPartyPolicyDTO(final PartyPolicyDTO pPartyPolicyDTO) {
        partyPolicyDTO = pPartyPolicyDTO;
    }

    /**
     * Gets the documentName
     *
     * @return the documentName
     */
    public String getDocumentName() {
        return documentName;
    }

    /**
     * Sets the documentName
     *
     * @param pDocumentName
     *            the documentName to set
     */
    public void setDocumentName(final String pDocumentName) {
        documentName = pDocumentName;
    }

    /**
     * Gets the policyId
     *
     * @return the policyId
     */
    public Long getPolicyId() {
        return policyId;
    }

    /**
     * Sets the policyId
     *
     * @param pPolicyId
     *            the policyId to set
     */
    public void setPolicyId(final Long pPolicyId) {
        policyId = pPolicyId;
    }

    /**
     * Gets the polNum
     *
     * @return the polNum
     */
    public String getPolNum() {
        return polNum;
    }

    /**
     * Sets the polNum
     *
     * @param pPolNum
     *            the polNum to set
     */
    public void setPolNum(final String pPolNum) {
        polNum = pPolNum;
    }

    /**
     * Gets the docType
     *
     * @return the docType
     */
    public String getDocType() {
        return docType;
    }

    /**
     * Sets the docType
     *
     * @param pDocType
     *            the docType to set
     */
    public void setDocType(final String pDocType) {
        docType = pDocType;
    }

    /**
     * Gets the finalDashboard
     *
     * @return the finalDashboard
     */
    public FinalDashboardDTO getFinalDashboard() {
        return finalDashboard;
    }

    /**
     * Sets the finalDashboard
     *
     * @param pFinalDashboard
     *            the finalDashboard to set
     */
    public void setFinalDashboard(final FinalDashboardDTO pFinalDashboard) {
        finalDashboard = pFinalDashboard;
    }

    /**
     * Gets the isSmallPot
     *
     * @return the isSmallPot
     */
    public Boolean getIsSmallPot() {
        return isSmallPot;
    }

    /**
     * Sets the isSmallPot
     *
     * @param pIsSmallPot
     *            the isSmallPot to set
     */
    public void setIsSmallPot(final Boolean pIsSmallPot) {
        isSmallPot = pIsSmallPot;
    }

    /**
     * Gets the contactCustomer
     *
     * @return the contactCustomer
     */
    public Boolean getContactCustomer() {
        return contactCustomer;
    }

    /**
     * Sets the contactCustomer
     *
     * @param pContactCustomer
     *            the contactCustomer to set
     */
    public void setContactCustomer(final Boolean pContactCustomer) {
        contactCustomer = pContactCustomer;
    }

    /**
     * Gets the pensioGuidance
     *
     * @return the pensioGuidance
     */
    public Boolean getPensionGuidance() {
        return pensionGuidance;
    }

    /**
     * Sets the pensioGuidance
     *
     * @param pPensionGuidance
     *            the pensioGuidance to set
     */
    public void setPensionGuidance(final Boolean pPensionGuidance) {
        pensionGuidance = pPensionGuidance;
    }

    /**
     * Gets the pensionAdvise
     *
     * @return the pensionAdvise
     */
    public Boolean getPensionAdvise() {
        return pensionAdvise;
    }

    /**
     * Sets the pensionAdvise
     *
     * @param pPensionAdvise
     *            the pensionAdvise to set
     */
    public void setPensionAdvise(final Boolean pPensionAdvise) {
        pensionAdvise = pPensionAdvise;
    }

    /**
     * Gets the serialversionuid
     *
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * Gets the bankDetailsNinoDTO
     *
     * @return the bankDetailsNinoDTO
     */
    public BankDetailsNinoDTO getBankDetailsNinoDTO() {
        return bankDetailsNinoDTO;
    }

    /**
     * Sets the bankDetailsNinoDTO
     *
     * @param pBankDetailsNinoDTO
     *            the bankDetailsNinoDTO to set
     */
    public void setBankDetailsNinoDTO(final BankDetailsNinoDTO pBankDetailsNinoDTO) {
        bankDetailsNinoDTO = pBankDetailsNinoDTO;
    }

    /**
     * Gets the pCLS
     *
     * @return the pCLS
     */
    public String getpCLS() {
        return pCLS;
    }

    /**
     * Sets the pCLS
     *
     * @param pPCLS
     *            the pCLS to set
     */
    public void setpCLS(final String pPCLS) {
        pCLS = pPCLS;
    }

    /**
     * Gets the tax
     *
     * @return the tax
     */
    public String getTax() {
        return tax;
    }

    /**
     * Sets the tax
     *
     * @param pTax
     *            the tax to set
     */
    public void setTax(final String pTax) {
        tax = pTax;
    }

    /**
     * Gets the totValue
     *
     * @return the totValue
     */
    public Double getTotValue() {
        return totValue;
    }

    /**
     * Sets the totValue
     *
     * @param pTotValue
     *            the totValue to set
     */
    public void setTotValue(final Double pTotValue) {
        totValue = pTotValue;
    }

    /**
     * Gets the amountPayable
     *
     * @return the amountPayable
     */
    public Double getAmountPayable() {
        return amountPayable;
    }

    /**
     * Sets the amountPayable
     *
     * @param pAmountPayable
     *            the amountPayable to set
     */
    public void setAmountPayable(final Double pAmountPayable) {
        amountPayable = pAmountPayable;
    }

    /**
     * Gets the mvaValue
     *
     * @return the mvaValue
     */
    public Double getMvaValue() {
        return mvaValue;
    }

    /**
     * Sets the mvaValue
     *
     * @param pMvaValue
     *            the amountPayable to set
     */
    public void setMvaValue(final Double pMvaValue) {
        mvaValue = pMvaValue;
    }

    /**
     * Gets the penaltyValue
     *
     * @return the penaltyValue
     */
    public Double getPenaltyValue() {
        return penaltyValue;
    }

    /**
     * Sets the penaltyValue
     *
     * @param pPenaltyValue
     *            the penaltyValue to set
     */
    public void setPenaltyValue(final Double pPenaltyValue) {
        penaltyValue = pPenaltyValue;
    }

    /**
     * Gets the earlyExitCharge
     *
     * @return the earlyExitCharge
     */
    public Double getEarlyExitCharge() {
        return earlyExitCharge;
    }

    /**
     * Sets the earlyExitCharge
     *
     * @param pEarlyExitCharge
     *            the earlyExitCharge to set
     */
    public void setEarlyExitCharge(final Double pEarlyExitCharge) {
        earlyExitCharge = pEarlyExitCharge;
    }

    /**
     * Gets the estimatedPensionFund
     *
     * @return the estimatedPensionFund
     */
    public Double getEstimatedPensionFund() {
        return estimatedPensionFund;
    }

    /**
     * Sets the estimatedPensionFund
     *
     * @param pEstimatedPensionFund
     *            the estimatedPensionFund to set
     */
    public void setEstimatedPensionFund(final Double pEstimatedPensionFund) {
        estimatedPensionFund = pEstimatedPensionFund;
    }

    /**
     * Gets the mvrValue
     *
     * @return the mvrValue
     */
    public String getMvrValue() {
        return mvrValue;
    }

    /**
     * Sets the mvrValue
     *
     * @param pMvrValue
     *            the mvrValue to set
     */
    public void setMvrValue(final String pMvrValue) {
        mvrValue = pMvrValue;
    }

    /**
     * Gets the dobWithSuffix
     *
     * @return the dobWithSuffix
     */
    public StringBuffer getDobWithSuffix() {
        return dobWithSuffix;
    }

    /**
     * Sets the dobWithSuffix
     *
     * @param pDobWithSuffix
     *            the dobWithSuffix to set
     */
    public void setDobWithSuffix(final StringBuffer pDobWithSuffix) {
        dobWithSuffix = pDobWithSuffix;
    }

    /**
     * Get PrimaryConactNum
     *
     * @return the primaryConactNum
     */
    public String getPrimaryConactNum() {
        return primaryConactNum;
    }

    /**
     * set primaryConactNum
     *
     * @param pPrimaryConactNum
     *            the primaryConactNum to set
     */
    public void setPrimaryConactNum(final String pPrimaryConactNum) {
        primaryConactNum = pPrimaryConactNum;
    }

    /**
     * Gets the secConactNum
     *
     * @return the secConactNum
     */
    public String getSecConactNum() {
        return secConactNum;
    }

    /**
     * Get secConactNum
     *
     * @param pSecConactNum
     *            the secConactNum to set
     */
    public void setSecConactNum(final String pSecConactNum) {
        secConactNum = pSecConactNum;
    }

    /**
     * Get additonalConactNum
     *
     * @return the additonalConactNum
     */
    public String getAdditonalConactNum() {
        return additonalConactNum;
    }

    /**
     * Set additonalConactNum
     *
     * @param pAdditonalConactNum
     *            the additonalConactNum to set
     */
    public void setAdditonalConactNum(final String pAdditonalConactNum) {
        additonalConactNum = pAdditonalConactNum;
    }

    /**
     * Gets the eecValue
     *
     * @return the eecValue
     */
    public String getEecValue() {
        return eecValue;
    }

    /**
     * Sets the eecValue
     *
     * @param pEecValue
     *            the eecValue to set
     */
    public void setEecValue(final String pEecValue) {
        eecValue = pEecValue;
    }

}
