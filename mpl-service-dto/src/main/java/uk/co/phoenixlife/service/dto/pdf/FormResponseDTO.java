/*
 * FormResponseDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.pdf;

import java.io.Serializable;

/**
 * FormResponseDTO.java
 */
public class FormResponseDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String pdfAsHtml;
    private boolean statusFlag;
    private int docCount;

    /**
     * Gets the pdfAsHtml
     *
     * @return the pdfAsHtml
     */
    public String getPdfAsHtml() {
        return pdfAsHtml;
    }

    /**
     * Sets the pdfAsHtml
     *
     * @param pPdfAsHtml
     *            the pdfAsHtml to set
     */
    public void setPdfAsHtml(final String pPdfAsHtml) {
        pdfAsHtml = pPdfAsHtml;
    }

    /**
     * Gets the statusFlag
     *
     * @return the statusFlag
     */
    public boolean isStatusFlag() {
        return statusFlag;
    }

    /**
     * Sets the statusFlag
     *
     * @param pStatusFlag
     *            the statusFlag to set
     */
    public void setStatusFlag(final boolean pStatusFlag) {
        statusFlag = pStatusFlag;
    }

    /**
     * Gets the docCount
     *
     * @return the docCount
     */
    public int getDocCount() {
        return docCount;
    }

    /**
     * Sets the docCount
     *
     * @param pDocCount
     *            the docCount to set
     */
    public void setDocCount(final int pDocCount) {
        docCount = pDocCount;
    }

}
