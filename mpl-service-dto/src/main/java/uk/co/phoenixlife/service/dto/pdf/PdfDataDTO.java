package uk.co.phoenixlife.service.dto.pdf;

public class PdfDataDTO {

	/**
	 * pdfByte
	 */
	private byte[] pdfByte;
	/**
	 * policyId
	 */
	private long policyId;
	
	
	/**
	 * 
	 * @return @byte
	 */
	public byte[] getPdfByte() {
		return pdfByte;
	}
	/**
	 * 
	 * @param pdfByte
	 */
	public void setPdfByte(byte[] pdfByte) {
		this.pdfByte = pdfByte;
	}
	/**
	 * 
	 * @return
	 */
	public long getPolicyId() {
		return policyId;
	}
	/**
	 * 
	 * @param policyId policyId
	 */
	public void setPolicyId(long policyId) {
		this.policyId = policyId;
	}
}
