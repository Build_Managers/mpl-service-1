package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;
import java.util.List;

/**
 * Wrapper class for CashInType BancsCashInTypeWrapper.java
 */
public class BancsCashInTypeWrapper implements Serializable {

    private static final long serialVersionUID = 1L;
    private List<CashInTypeDTO> listOfCashInTypes;

    /**
     * method to get ListofCashInTypes
     *
     * @return List<CashInType>
     */
    public List<CashInTypeDTO> getListOfCashInTypes() {
        return listOfCashInTypes;
    }

    /**
     * Method to set ListOfCashInTypes
     *
     * @param plistOfCashInTypes
     *            plistOfCashInTypes
     */
    public void setListOfCashInTypes(final List<CashInTypeDTO> plistOfCashInTypes) {
        this.listOfCashInTypes = plistOfCashInTypes;
    }

}
