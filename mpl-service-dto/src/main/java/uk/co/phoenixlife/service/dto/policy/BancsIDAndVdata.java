/*
 * BancsIDAndVdata.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * BancsIDAndVdata.java
 */
public class BancsIDAndVdata implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String polNum;
    private String partyType;
    private String title;
    private String forename;
    private String surname;
    private Date dateOfBirth;
    private String houseNo;
    private String houseName;
    private String address1;
    private String address2;
    private String cityTown;
    private String county;
    private String postcode;
    private Integer bancsUniqueCustNo;
    private Boolean poaFlag;
    private Boolean fcuFlag;
    private String bankruptcyRefNo;
    private Date bankruptcyDischargeDate;
    private Date bankruptcyDate;
    private String courtName;
    private Date courtOrderDate;
    private String addressStatus;
    private String bancsPolNum;
    private String polStatus;
    private Date polStDate;
    private String premFreq;
    private String paymentMethod;
    private String benefitName;
    private Double netInitPrem;
    private Integer netModPrem;
    private Double lastPremPaid;
    private Integer sortCode;
    private Integer bankAcct;
    private Date premiumDueDate;
    private String niNo;
    private String fundname;
    private Timestamp busTimeStamp;
    private List<String> lst;
    private String first;
    private String second;
    private String third;
    private String fourth;
    private String fifth;
    private String sixth;

    /**
     * Gets the polNum
     *
     * @return the polNum
     */
    public String getPolNum() {
        return polNum;
    }

    /**
     * Sets the polNum
     *
     * @param pPolNum
     *            the polNum to set
     */
    public void setPolNum(final String pPolNum) {
        polNum = pPolNum;
    }

    /**
     * Gets the partyType
     *
     * @return the partyType
     */
    public String getPartyType() {
        return partyType;
    }

    /**
     * Sets the partyType
     *
     * @param pPartyType
     *            the partyType to set
     */
    public void setPartyType(final String pPartyType) {
        partyType = pPartyType;
    }

    /**
     * Gets the title
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title
     *
     * @param pTitle
     *            the title to set
     */
    public void setTitle(final String pTitle) {
        title = pTitle;
    }

    /**
     * Gets the forename
     *
     * @return the forename
     */
    public String getForename() {
        return forename;
    }

    /**
     * Sets the forename
     *
     * @param pForename
     *            the forename to set
     */
    public void setForename(final String pForename) {
        forename = pForename;
    }

    /**
     * Gets the surname
     *
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets the surname
     *
     * @param pSurname
     *            the surname to set
     */
    public void setSurname(final String pSurname) {
        surname = pSurname;
    }

    /**
     * Gets the dateOfBirth
     *
     * @return the dateOfBirth
     */
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the dateOfBirth
     *
     * @param pDateOfBirth
     *            the dateOfBirth to set
     */
    public void setDateOfBirth(final Date pDateOfBirth) {
        dateOfBirth = pDateOfBirth;
    }

    /**
     * Gets the houseNo
     *
     * @return the houseNo
     */
    public String getHouseNo() {
        return houseNo;
    }

    /**
     * Sets the houseNo
     *
     * @param pHouseNo
     *            the houseNo to set
     */
    public void setHouseNo(final String pHouseNo) {
        houseNo = pHouseNo;
    }

    /**
     * Gets the houseName
     *
     * @return the houseName
     */
    public String getHouseName() {
        return houseName;
    }

    /**
     * Sets the houseName
     *
     * @param pHouseName
     *            the houseName to set
     */
    public void setHouseName(final String pHouseName) {
        houseName = pHouseName;
    }

    /**
     * Gets the address1
     *
     * @return the address1
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * Sets the address1
     *
     * @param pAddress1
     *            the address1 to set
     */
    public void setAddress1(final String pAddress1) {
        address1 = pAddress1;
    }

    /**
     * Gets the address2
     *
     * @return the address2
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * Sets the address2
     *
     * @param pAddress2
     *            the address2 to set
     */
    public void setAddress2(final String pAddress2) {
        address2 = pAddress2;
    }

    /**
     * Gets the cityTown
     *
     * @return the cityTown
     */
    public String getCityTown() {
        return cityTown;
    }

    /**
     * Sets the cityTown
     *
     * @param pCityTown
     *            the cityTown to set
     */
    public void setCityTown(final String pCityTown) {
        cityTown = pCityTown;
    }

    /**
     * Gets the county
     *
     * @return the county
     */
    public String getCounty() {
        return county;
    }

    /**
     * Sets the county
     *
     * @param pCounty
     *            the county to set
     */
    public void setCounty(final String pCounty) {
        county = pCounty;
    }

    /**
     * Gets the postcode
     *
     * @return the postcode
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * Sets the postcode
     *
     * @param pPostcode
     *            the postcode to set
     */
    public void setPostcode(final String pPostcode) {
        postcode = pPostcode;
    }

    /**
     * Gets the bancsUniqueCustNo
     *
     * @return the bancsUniqueCustNo
     */
    public Integer getBancsUniqueCustNo() {
        return bancsUniqueCustNo;
    }

    /**
     * Sets the bancsUniqueCustNo
     *
     * @param pBancsUniqueCustNo
     *            the bancsUniqueCustNo to set
     */
    public void setBancsUniqueCustNo(final Integer pBancsUniqueCustNo) {
        bancsUniqueCustNo = pBancsUniqueCustNo;
    }

    /**
     * Gets the poaFlag
     *
     * @return the poaFlag
     */
    public Boolean getPoaFlag() {
        return poaFlag;
    }

    /**
     * Sets the poaFlag
     *
     * @param pPoaFlag
     *            the poaFlag to set
     */
    public void setPoaFlag(final Boolean pPoaFlag) {
        poaFlag = pPoaFlag;
    }

    /**
     * Gets the fcuFlag
     *
     * @return the fcuFlag
     */
    public Boolean getFcuFlag() {
        return fcuFlag;
    }

    /**
     * Sets the fcuFlag
     *
     * @param pFcuFlag
     *            the fcuFlag to set
     */
    public void setFcuFlag(final Boolean pFcuFlag) {
        fcuFlag = pFcuFlag;
    }

    /**
     * Gets the bankruptcyRefNo
     *
     * @return the bankruptcyRefNo
     */
    public String getBankruptcyRefNo() {
        return bankruptcyRefNo;
    }

    /**
     * Sets the bankruptcyRefNo
     *
     * @param pBankruptcyRefNo
     *            the bankruptcyRefNo to set
     */
    public void setBankruptcyRefNo(final String pBankruptcyRefNo) {
        bankruptcyRefNo = pBankruptcyRefNo;
    }

    /**
     * Gets the bankruptcyDischargeDate
     *
     * @return the bankruptcyDischargeDate
     */
    public Date getBankruptcyDischargeDate() {
        return bankruptcyDischargeDate;
    }

    /**
     * Sets the bankruptcyDischargeDate
     *
     * @param pBankruptcyDischargeDate
     *            the bankruptcyDischargeDate to set
     */
    public void setBankruptcyDischargeDate(final Date pBankruptcyDischargeDate) {
        bankruptcyDischargeDate = pBankruptcyDischargeDate;
    }

    /**
     * Gets the bankruptcyDate
     *
     * @return the bankruptcyDate
     */
    public Date getBankruptcyDate() {
        return bankruptcyDate;
    }

    /**
     * Sets the bankruptcyDate
     *
     * @param pBankruptcyDate
     *            the bankruptcyDate to set
     */
    public void setBankruptcyDate(final Date pBankruptcyDate) {
        bankruptcyDate = pBankruptcyDate;
    }

    /**
     * Gets the courtName
     *
     * @return the courtName
     */
    public String getCourtName() {
        return courtName;
    }

    /**
     * Sets the courtName
     *
     * @param pCourtName
     *            the courtName to set
     */
    public void setCourtName(final String pCourtName) {
        courtName = pCourtName;
    }

    /**
     * Gets the courtOrderDate
     *
     * @return the courtOrderDate
     */
    public Date getCourtOrderDate() {
        return courtOrderDate;
    }

    /**
     * Sets the courtOrderDate
     *
     * @param pCourtOrderDate
     *            the courtOrderDate to set
     */
    public void setCourtOrderDate(final Date pCourtOrderDate) {
        courtOrderDate = pCourtOrderDate;
    }

    /**
     * Gets the addressStatus
     *
     * @return the addressStatus
     */
    public String getAddressStatus() {
        return addressStatus;
    }

    /**
     * Sets the addressStatus
     *
     * @param pAddressStatus
     *            the addressStatus to set
     */
    public void setAddressStatus(final String pAddressStatus) {
        addressStatus = pAddressStatus;
    }

    /**
     * Gets the bancsPolNum
     *
     * @return the bancsPolNum
     */
    public String getBancsPolNum() {
        return bancsPolNum;
    }

    /**
     * Sets the bancsPolNum
     *
     * @param pBancsPolNum
     *            the bancsPolNum to set
     */
    public void setBancsPolNum(final String pBancsPolNum) {
        bancsPolNum = pBancsPolNum;
    }

    /**
     * Gets the polStatus
     *
     * @return the polStatus
     */
    public String getPolStatus() {
        return polStatus;
    }

    /**
     * Sets the polStatus
     *
     * @param pPolStatus
     *            the polStatus to set
     */
    public void setPolStatus(final String pPolStatus) {
        polStatus = pPolStatus;
    }

    /**
     * Gets the polStDate
     *
     * @return the polStDate
     */
    public Date getPolStDate() {
        return polStDate;
    }

    /**
     * Sets the polStDate
     *
     * @param pPolStDate
     *            the polStDate to set
     */
    public void setPolStDate(final Date pPolStDate) {
        polStDate = pPolStDate;
    }

    /**
     * Gets the premFreq
     *
     * @return the premFreq
     */
    public String getPremFreq() {
        return premFreq;
    }

    /**
     * Sets the premFreq
     *
     * @param pPremFreq
     *            the premFreq to set
     */
    public void setPremFreq(final String pPremFreq) {
        premFreq = pPremFreq;
    }

    /**
     * Gets the paymentMethod
     *
     * @return the paymentMethod
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets the paymentMethod
     *
     * @param pPaymentMethod
     *            the paymentMethod to set
     */
    public void setPaymentMethod(final String pPaymentMethod) {
        paymentMethod = pPaymentMethod;
    }

    /**
     * Gets the benefitName
     *
     * @return the benefitName
     */
    public String getBenefitName() {
        return benefitName;
    }

    /**
     * Sets the benefitName
     *
     * @param pBenefitName
     *            the benefitName to set
     */
    public void setBenefitName(final String pBenefitName) {
        benefitName = pBenefitName;
    }

    /**
     * Gets the netInitPrem
     *
     * @return the netInitPrem
     */
    public Double getNetInitPrem() {
        return netInitPrem;
    }

    /**
     * Sets the netInitPrem
     *
     * @param pNetInitPrem
     *            the netInitPrem to set
     */
    public void setNetInitPrem(final Double pNetInitPrem) {
        netInitPrem = pNetInitPrem;
    }

    /**
     * Gets the netModPrem
     *
     * @return the netModPrem
     */
    public Integer getNetModPrem() {
        return netModPrem;
    }

    /**
     * Sets the netModPrem
     *
     * @param pNetModPrem
     *            the netModPrem to set
     */
    public void setNetModPrem(final Integer pNetModPrem) {
        netModPrem = pNetModPrem;
    }

    /**
     * Gets the lastPremPaid
     *
     * @return the lastPremPaid
     */
    public Double getLastPremPaid() {
        return lastPremPaid;
    }

    /**
     * Sets the lastPremPaid
     *
     * @param pLastPremPaid
     *            the lastPremPaid to set
     */
    public void setLastPremPaid(final Double pLastPremPaid) {
        lastPremPaid = pLastPremPaid;
    }

    /**
     * Gets the sortCode
     *
     * @return the sortCode
     */
    public Integer getSortCode() {
        return sortCode;
    }

    /**
     * Sets the sortCode
     *
     * @param pSortCode
     *            the sortCode to set
     */
    public void setSortCode(final Integer pSortCode) {
        sortCode = pSortCode;
    }

    /**
     * Gets the bankAcct
     *
     * @return the bankAcct
     */
    public Integer getBankAcct() {
        return bankAcct;
    }

    /**
     * Sets the bankAcct
     *
     * @param pBankAcct
     *            the bankAcct to set
     */
    public void setBankAcct(final Integer pBankAcct) {
        bankAcct = pBankAcct;
    }

    /**
     * Gets the premiumDueDate
     *
     * @return the premiumDueDate
     */
    public Date getPremiumDueDate() {
        return premiumDueDate;
    }

    /**
     * Sets the premiumDueDate
     *
     * @param pPremiumDueDate
     *            the premiumDueDate to set
     */
    public void setPremiumDueDate(final Date pPremiumDueDate) {
        premiumDueDate = pPremiumDueDate;
    }

    /**
     * Gets the niNo
     *
     * @return the niNo
     */
    public String getNiNo() {
        return niNo;
    }

    /**
     * Sets the niNo
     *
     * @param pNiNo
     *            the niNo to set
     */
    public void setNiNo(final String pNiNo) {
        niNo = pNiNo;
    }

    /**
     * Gets the fundname
     *
     * @return the fundname
     */
    public String getFundname() {
        return fundname;
    }

    /**
     * Sets the fundname
     *
     * @param pFundname
     *            the fundname to set
     */
    public void setFundname(final String pFundname) {
        fundname = pFundname;
    }

    /**
     * Gets the busTimeStamp
     *
     * @return the busTimeStamp
     */
    public Timestamp getBusTimeStamp() {
        return busTimeStamp;
    }

    /**
     * Sets the busTimeStamp
     *
     * @param pBusTimeStamp
     *            the busTimeStamp to set
     */
    public void setBusTimeStamp(final Timestamp pBusTimeStamp) {
        busTimeStamp = pBusTimeStamp;
    }

    /**
     * Gets the lst
     *
     * @return the lst
     */
    public List<String> getLst() {
        return lst;
    }

    /**
     * Sets the lst
     *
     * @param pLst
     *            the lst to set
     */
    public void setLst(final List<String> pLst) {
        lst = pLst;
    }

    /**
     * Gets the first
     *
     * @return the first
     */
    public String getFirst() {
        return first;
    }

    /**
     * Sets the first
     *
     * @param pFirst
     *            the first to set
     */
    public void setFirst(final String pFirst) {
        first = pFirst;
    }

    /**
     * Gets the second
     *
     * @return the second
     */
    public String getSecond() {
        return second;
    }

    /**
     * Sets the second
     *
     * @param pSecond
     *            the second to set
     */
    public void setSecond(final String pSecond) {
        second = pSecond;
    }

    /**
     * Gets the third
     *
     * @return the third
     */
    public String getThird() {
        return third;
    }

    /**
     * Sets the third
     *
     * @param pThird
     *            the third to set
     */
    public void setThird(final String pThird) {
        third = pThird;
    }

    /**
     * Gets the fourth
     *
     * @return the fourth
     */
    public String getFourth() {
        return fourth;
    }

    /**
     * Sets the fourth
     *
     * @param pFourth
     *            the fourth to set
     */
    public void setFourth(final String pFourth) {
        fourth = pFourth;
    }

    /**
     * Gets the fifth
     *
     * @return the fifth
     */
    public String getFifth() {
        return fifth;
    }

    /**
     * Sets the fifth
     *
     * @param pFifth
     *            the fifth to set
     */
    public void setFifth(final String pFifth) {
        fifth = pFifth;
    }

    /**
     * Gets the sixth
     *
     * @return the sixth
     */
    public String getSixth() {
        return sixth;
    }

    /**
     * Sets the sixth
     *
     * @param pSixth
     *            the sixth to set
     */
    public void setSixth(final String pSixth) {
        sixth = pSixth;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
