package uk.co.phoenixlife.service.dto.policy;

import java.sql.Timestamp;
import java.util.List;

/**
 * BancsPolicyExists.java
 */
public class BancsPolicyExists {

    private List<BancsPolicyExistsCheck> listOfPolicies;
    private Timestamp busTimeStamp;
    /**
     * Gets the listOfPolicies
     * @return the listOfPolicies
     */
    public List<BancsPolicyExistsCheck> getListOfPolicies() {
        return listOfPolicies;
    }
    /**
    * Sets the listOfPolicies
    * @param plistOfPolicies the listOfPolicies to set
    */
    public void setListOfPolicies(final List<BancsPolicyExistsCheck> plistOfPolicies) {
        this.listOfPolicies = plistOfPolicies;
    }
    /**
    * Gets the busTimeStamp
    * @return the busTimeStamp
    */
    public Timestamp getBusTimeStamp() {
        return busTimeStamp;
    }
    /**
    * Sets the busTimeStamp
    * @param pbusTimeStamp the busTimeStamp to set
    */
    public void setBusTimeStamp(final Timestamp pbusTimeStamp) {
        this.busTimeStamp = pbusTimeStamp;
    }
}
