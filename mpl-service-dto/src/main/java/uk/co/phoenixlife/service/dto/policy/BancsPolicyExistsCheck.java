package uk.co.phoenixlife.service.dto.policy;

/**
 * BancsPolicyExistsCheck.java
 */
public class BancsPolicyExistsCheck {

    private String bancsPolNum;
    private String polNum;
    private String polStatusLit;
    private String polStatusCd;
    private String polStDate;
    private String bancsProductCode;

    /**
     * Gets the polStatusLit
     *
     * @return the polStatusLit
     */
    public String getPolStatusLit() {
        return polStatusLit;
    }

    /**
     * Sets the polStatusLit
     *
     * @param pPolStatusLit
     *            the polStatusLit to set
     */
    public void setPolStatusLit(final String pPolStatusLit) {
        this.polStatusLit = pPolStatusLit;
    }

    /**
     * Gets the polStatusCd
     *
     * @return the polStatusCd
     */
    public String getPolStatusCd() {
        return polStatusCd;
    }

    /**
     * Sets the polStatusCd
     *
     * @param pPolStatusCd
     *            the polStatusCd to set
     */
    public void setPolStatusCd(final String pPolStatusCd) {
        this.polStatusCd = pPolStatusCd;
    }

    /**
     * Gets the bancsPolNum
     *
     * @return the bancsPolNum
     */
    public String getBancsPolNum() {
        return bancsPolNum;
    }

    /**
     * Sets the bancsPolNum
     *
     * @param pbancsPolNum
     *            the bancsPolNum to set
     */
    public void setBancsPolNum(final String pbancsPolNum) {
        this.bancsPolNum = pbancsPolNum;
    }

    /**
     * Gets the polNum
     *
     * @return the polNum
     */
    public String getPolNum() {
        return polNum;
    }

    /**
     * Sets the polNum
     *
     * @param ppolNum
     *            the polNum to set
     */
    public void setPolNum(final String ppolNum) {
        this.polNum = ppolNum;
    }

    /**
     * Gets the polStDate
     *
     * @return the polStDate
     */
    public String getPolStDate() {
        return polStDate;
    }

    /**
     * Sets the polStDate
     *
     * @param ppolStDate
     *            the polStDate to set
     */
    public void setPolStDate(final String ppolStDate) {
        this.polStDate = ppolStDate;
    }

    /**
     * Gets the bancsProductCode
     *
     * @return the bancsProductCode
     */
    public String getBancsProductCode() {
        return bancsProductCode;
    }

    /**
     * Sets the bancsProductCode
     *
     * @param pbancsProductCode
     *            the bancsProductCode to set
     */
    public void setBancsProductCode(final String pbancsProductCode) {
        this.bancsProductCode = pbancsProductCode;
    }
}
