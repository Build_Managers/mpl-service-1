/*
 * BancsRetQuoteValDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;
import java.sql.Timestamp;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * CalcULPolValDTO.java
 */
public class BancsRetQuoteValDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'hh:mm:ss")
    private Timestamp busTimeStamp;
    private Double cUCharge;
    private Double penValue;
    private String bancsPolNum;
    private Double mvaValue;
    private Double totValue;

    /**
     * Gets the busTimeStamp
     *
     * @return the busTimeStamp
     */
    public Timestamp getBusTimeStamp() {
        return busTimeStamp;
    }

    /**
     * Sets the busTimeStamp
     *
     * @param pBusTimeStamp
     *            the busTimeStamp to set
     */
    public void setBusTimeStamp(final Timestamp pBusTimeStamp) {
        busTimeStamp = pBusTimeStamp;
    }

    /**
     * Gets the cUCharge
     *
     * @return the cUCharge
     */
    public Double getcUCharge() {
        return cUCharge;
    }

    /**
     * Sets the cUCharge
     *
     * @param pCUCharge
     *            the cUCharge to set
     */
    public void setcUCharge(final Double pCUCharge) {
        cUCharge = pCUCharge;
    }

    /**
     * Gets the penValue
     *
     * @return the penValue
     */
    public Double getPenValue() {
        return penValue;
    }

    /**
     * Sets the penValue
     *
     * @param pPenValue
     *            the penValue to set
     */
    public void setPenValue(final Double pPenValue) {
        penValue = pPenValue;
    }

    /**
     * Gets the bancsPolNum
     *
     * @return the bancsPolNum
     */
    public String getBancsPolNum() {
        return bancsPolNum;
    }

    /**
     * Sets the bancsPolNum
     *
     * @param pBancsPolNum
     *            the bancsPolNum to set
     */
    public void setBancsPolNum(final String pBancsPolNum) {
        bancsPolNum = pBancsPolNum;
    }

    /**
     * Gets the mvaValue
     *
     * @return the mvaValue
     */
    public Double getMvaValue() {
        return mvaValue;
    }

    /**
     * Sets the mvaValue
     *
     * @param pMvaValue
     *            the mvaValue to set
     */
    public void setMvaValue(final Double pMvaValue) {
        mvaValue = pMvaValue;
    }

    /**
     * Gets the totValue
     *
     * @return the totValue
     */
    public Double getTotValue() {
        return totValue;
    }

    /**
     * Sets the totValue
     *
     * @param pTotValue
     *            the totValue to set
     */
    public void setTotValue(final Double pTotValue) {
        totValue = pTotValue;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
