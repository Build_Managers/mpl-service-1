/*
 * BankDetailsDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * BankDetailsDTO.java
 */
public class BankDetailsNinoDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String bankOrBuildingSocietyName;
    private String accountHolderName;
    private String accountNumber;
    private String sortCode;
    private String rollNumber;
    private String nino;

    /**
     * Gets the bankOrBuildingSocietyName
     *
     * @return the bankOrBuildingSocietyName
     */
    public String getBankOrBuildingSocietyName() {
        return bankOrBuildingSocietyName;
    }

    /**
     * Sets the bankOrBuildingSocietyName
     *
     * @param pBankOrBuildingSocietyName
     *            the bankOrBuildingSocietyName to set
     */
    public void setBankOrBuildingSocietyName(final String pBankOrBuildingSocietyName) {
        bankOrBuildingSocietyName = pBankOrBuildingSocietyName;
    }

    /**
     * Gets the accountHolderName
     *
     * @return the accountHolderName
     */
    public String getAccountHolderName() {
        return accountHolderName;
    }

    /**
     * Sets the accountHolderName
     *
     * @param pAccountHolderName
     *            the accountHolderName to set
     */
    public void setAccountHolderName(final String pAccountHolderName) {
        accountHolderName = pAccountHolderName;
    }

    /**
     * Gets the accountNumber
     *
     * @return the accountNumber
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the accountNumber
     *
     * @param pAccountNumber
     *            the accountNumber to set
     */
    public void setAccountNumber(final String pAccountNumber) {
        accountNumber = pAccountNumber;
    }

    /**
     * Gets the sortCode
     *
     * @return the sortCode
     */
    public String getSortCode() {
        return sortCode;
    }

    /**
     * Sets the sortCode
     *
     * @param pSortCode
     *            the sortCode to set
     */
    public void setSortCode(final String pSortCode) {
        sortCode = pSortCode;
    }

    /**
     * Gets the rollNumber
     *
     * @return the rollNumber
     */
    public String getRollNumber() {
        return rollNumber;
    }

    /**
     * Sets the rollNumber
     *
     * @param pRollNumber
     *            the rollNumber to set
     */
    public void setRollNumber(final String pRollNumber) {
        rollNumber = pRollNumber;
    }

    /**
     * Gets the nino
     *
     * @return the nino
     */
    public String getNino() {
        return nino;
    }

    /**
     * Sets the nino
     *
     * @param pnino
     *            pnino
     */
    public void setNino(final String pnino) {
        this.nino = pnino;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
