/*
 * BenefitDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * BenefitDTO.java
 */
public class BenefitDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private Double netInitPrem;
    private String benefitName;

    /**
     * Gets the netInitPrem
     *
     * @return the netInitPrem
     */
    public Double getNetInitPrem() {
        return netInitPrem;
    }

    /**
     * Sets the netInitPrem
     *
     * @param pNetInitPrem
     *            the netInitPrem to set
     */
    public void setNetInitPrem(final Double pNetInitPrem) {
        netInitPrem = pNetInitPrem;
    }

    /**
     * Gets the benefitName
     *
     * @return the benefitName
     */
    public String getBenefitName() {
        return benefitName;
    }

    /**
     * Sets the benefitName
     *
     * @param pBenefitName
     *            the benefitName to set
     */
    public void setBenefitName(final String pBenefitName) {
        benefitName = pBenefitName;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}