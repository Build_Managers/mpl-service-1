package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * CashInTypeDTO DTO CashInTypeDTO.java
 */
public class CashInTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    private String bancsPolNum;
    private String bancsUniqueCustNo;
    private String cashInType;
    private String pCLS;
    private String tax;
    private Timestamp busTimeStamp;
    private String source;

    /**
     * Method to get BancsPolNum
     *
     * @return bancsPolNum bancsPolNum
     */
    public String getBancsPolNum() {
        return bancsPolNum;
    }

    /**
     * Method to setBancsPolNum
     *
     * @param pbancsPolNum
     *            bancsPolNum
     */
    public void setBancsPolNum(final String pbancsPolNum) {
        this.bancsPolNum = pbancsPolNum;
    }

    /**
     * Method to getBancsUniqueCustNumber
     *
     * @return bancsUniqueCustNo
     */
    public String getBancsUniqueCustNo() {
        return bancsUniqueCustNo;
    }

    /**
     * Method to setBancsUniqueCustNo
     *
     * @param pbancsUniqueCustNo
     *            bancsUniqueCustNo
     */
    public void setBancsUniqueCustNo(final String pbancsUniqueCustNo) {
        this.bancsUniqueCustNo = pbancsUniqueCustNo;
    }

    /**
     * Method to getCashInType
     *
     * @return cashInType
     */
    public String getCashInType() {
        return cashInType;
    }

    /**
     * Method to setCashInType
     *
     * @param pcashInType
     *            cashInType
     */
    public void setCashInType(final String pcashInType) {
        this.cashInType = pcashInType;
    }

    /**
     * Method to getpCLS
     *
     * @return pCLS
     */
    public String getpCLS() {
        return pCLS;
    }

    /**
     * Method to setpCLS
     *
     * @param pPCLS
     *            pCLS
     */
    public void setpCLS(final String pPCLS) {
        this.pCLS = pPCLS;
    }

    /**
     * Method to getTax
     *
     * @return tax
     */
    public String getTax() {
        return tax;
    }

    /**
     * Method to setTax
     *
     * @param ptax
     *            tax
     */
    public void setTax(final String ptax) {
        this.tax = ptax;
    }

    /**
     * Method to getBusTimeStamp
     *
     * @return busTimeStamp
     */
    public Timestamp getBusTimeStamp() {
        return busTimeStamp;
    }

    /**
     * Method to setBusTimeStamp
     *
     * @param pbusTimeStamp
     *            busTimeStamp
     */
    public void setBusTimeStamp(final Timestamp pbusTimeStamp) {
        this.busTimeStamp = pbusTimeStamp;
    }

    @Override
    public String toString() {
        return "CashInTypeRepeatingGroup [bancsPolNum=" + bancsPolNum + ", bancsUniqueCustNo=" + bancsUniqueCustNo
                + ", cashInType=" + cashInType + ", pCLS=" + pCLS + ", tax=" + tax + ", busTimeStamp=" + busTimeStamp
                + "]";
    }

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

}
