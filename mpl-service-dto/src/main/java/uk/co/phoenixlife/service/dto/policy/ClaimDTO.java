/*
 * ClaimDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * ClaimDTO.java
 */
public class ClaimDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String clmRef;
    private String clmTypeLit;
    private String clmTypeCd;
    private String clmStatusLit;
    private String clmStatusCd;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private Date clmDate;

    /**
     * Gets the clmTypeLit
     *
     * @return the clmTypeLit
     */
    public String getClmTypeLit() {
        return clmTypeLit;
    }

    /**
     * Sets the clmTypeLit
     *
     * @param pclmTypeLit
     *            the clmTypeLit to set
     */
    public void setClmTypeLit(final String pclmTypeLit) {
        this.clmTypeLit = pclmTypeLit;
    }

    /**
     * Gets the clmTypeCd
     *
     * @return the clmTypeCd
     */
    public String getClmTypeCd() {
        return clmTypeCd;
    }

    /**
     * Sets the clmTypeCd
     *
     * @param pclmTypeCd
     *            the clmTypeCd to set
     */
    public void setClmTypeCd(final String pclmTypeCd) {
        this.clmTypeCd = pclmTypeCd;
    }

    /**
     * Gets the clmStatusLit
     *
     * @return the clmStatusLit
     */
    public String getClmStatusLit() {
        return clmStatusLit;
    }

    /**
     * Sets the clmStatusLit
     *
     * @param pclmStatusLit
     *            the clmStatusLit to set
     */
    public void setClmStatusLit(final String pclmStatusLit) {
        this.clmStatusLit = pclmStatusLit;
    }

    /**
     * Gets the clmStatusCd
     *
     * @return the clmStatusCd
     */
    public String getClmStatusCd() {
        return clmStatusCd;
    }

    /**
     * Sets the clmStatusCd
     *
     * @param pclmStatusCd
     *            the clmStatusCd to set
     */
    public void setClmStatusCd(final String pclmStatusCd) {
        this.clmStatusCd = pclmStatusCd;
    }

    /**
     * Gets the clmRef
     *
     * @return the clmRef
     */
    public String getClmRef() {
        return clmRef;
    }

    /**
     * Sets the clmRef
     *
     * @param pClmRef
     *            the clmRef to set
     */
    public void setClmRef(final String pClmRef) {
        clmRef = pClmRef;
    }

    /**
     * Gets the clmDate
     *
     * @return the clmDate
     */
    public Date getClmDate() {
        return clmDate;
    }

    /**
     * Sets the clmDate
     *
     * @param pClmDate
     *            the clmDate to set
     */
    public void setClmDate(final Date pClmDate) {
        clmDate = pClmDate;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
