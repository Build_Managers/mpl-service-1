/*
 * ClaimListDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * ClaimListDTO.java
 */
public class ClaimListDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private List<QuoteDTO> listOfQuotes;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'hh:mm:ss")
    private Timestamp busTimeStamp;
    private String bancsPolNum;
    private List<ClaimDTO> listOfClaims;

    /**
     * Method to getListOfQuotes
     *
     * @return the listOfQuotes
     */
    public List<QuoteDTO> getListOfQuotes() {
        return listOfQuotes;
    }

    /**
     * Method to setListOfQuotes
     *
     * @param plistOfQuotes
     *            the listOfQuotes to set
     */
    public void setListOfQuotes(final List<QuoteDTO> plistOfQuotes) {
        this.listOfQuotes = plistOfQuotes;
    }

    /**
     * Method to getBusTimeStamp
     *
     * @return the busTimeStamp
     */
    public Timestamp getBusTimeStamp() {
        return busTimeStamp;
    }

    /**
     * Method to setBusTimeStamp
     *
     * @param pbusTimeStamp
     *            the busTimeStamp to set
     */
    public void setBusTimeStamp(final Timestamp pbusTimeStamp) {
        this.busTimeStamp = pbusTimeStamp;
    }

    /**
     * Method to setBancsPolNum
     *
     * @return the bancsPolNum
     */
    public String getBancsPolNum() {
        return bancsPolNum;
    }

    /**
     * Method to setBancsPolNum
     *
     * @param pbancsPolNum
     *            the bancsPolNum to set
     */
    public void setBancsPolNum(final String pbancsPolNum) {
        this.bancsPolNum = pbancsPolNum;
    }

    /**
     * Method to getListOfClaims
     *
     * @return the listOfClaims
     */
    public List<ClaimDTO> getListOfClaims() {
        return listOfClaims;
    }

    /**
     * Method to setListOfClaims
     *
     * @param plistOfClaims
     *            the listOfClaims to set
     */
    public void setListOfClaims(final List<ClaimDTO> plistOfClaims) {
        this.listOfClaims = plistOfClaims;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
