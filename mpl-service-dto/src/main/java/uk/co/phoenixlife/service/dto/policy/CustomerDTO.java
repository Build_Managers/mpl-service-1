package uk.co.phoenixlife.service.dto.policy;

import java.util.Date;
import java.util.List;

import uk.co.phoenixlife.security.service.dto.ForgotPasswordDto;
import uk.co.phoenixlife.service.dto.bancspost.BancsCalcCashInTaxOutput;
import uk.co.phoenixlife.service.dto.pdf.FormRequestDTO;

public class CustomerDTO {

    private String username;
    private Long customerId;
    private Long policyId;
    private String policyNumber;
    private String title;
    private String firstName;
    private String lastName;
    private String prefPhoneNo;
    private String nino;
    private String email;
    private StringBuilder address;
    private String houseNum;
    private String houseName;
    private String address1;
    private String address2;
    private String postalTownCity;
    private String postCode;
    private String county;
    private String country;
    private String bancsUniqueCustNo;
    private Date dateOfBirth;
    private List<QuestionsDTO> questListPension;
    private List<QuestionsDTO> questListCalculateTax;
    private BankDetailsNinoDTO bankDetailsNinoDTO;
    private QuoteDetails quoteDetails;
    private FormRequestDTO formRequest;
    private String bancsPolNum;
    private String generated;
    private String primaryPhonenumber;
    private BancsCalcCashInTaxOutput bancsCalcCashInTaxOutput;
    private ForgotPasswordDto forgotPasswordDto;
    private List<String> xGUIDBancsPolNumCacheKeyList;

    /**
     * Gets the xGUIDBancsPolNumCacheKeyList
     * 
     * @return the xGUIDBancsPolNumCacheKeyList
     */
    public List<String> getxGUIDBancsPolNumCacheKeyList() {
        return xGUIDBancsPolNumCacheKeyList;
    }

    /**
     * Sets the xGUIDBancsPolNumCacheKeyList
     * 
     * @param xGUIDBancsPolNumCacheKeyList
     *            the xGUIDBancsPolNumCacheKeyList to set
     */
    public void setxGUIDBancsPolNumCacheKeyList(final List<String> xGUIDBancsPolNumCacheKeyList) {
        this.xGUIDBancsPolNumCacheKeyList = xGUIDBancsPolNumCacheKeyList;
    }

    /**
     * Gets the forgotPasswordDto
     *
     * @return the forgotPasswordDto
     */
    public ForgotPasswordDto getForgotPasswordDto() {
        return forgotPasswordDto;
    }

    /**
     * Sets the forgotPasswordDto
     *
     * @param forgotPasswordDto
     *            the forgotPasswordDto to set
     */
    public void setForgotPasswordDto(final ForgotPasswordDto forgotPasswordDto) {
        this.forgotPasswordDto = forgotPasswordDto;
    }

    /**
     * Gets the primaryPhonenumber
     *
     * @return the primaryPhonenumber
     */
    public String getPrimaryPhonenumber() {
        return primaryPhonenumber;
    }

    /**
     * Sets the primaryPhonenumber
     *
     * @param primaryPhonenumber
     *            the primaryPhonenumber to set
     */
    public void setPrimaryPhonenumber(final String primaryPhonenumber) {
        this.primaryPhonenumber = primaryPhonenumber;
    }

    /**
     * Gets the bancsPolNum
     *
     * @return the bancsPolNum
     */
    public String getBancsPolNum() {
        return bancsPolNum;
    }

    public String getGenerated() {
        return generated;
    }

    public void setGenerated(final String generated) {
        this.generated = generated;
    }

    /**
     * Sets the bancsPolNum
     *
     * @param bancsPolNum
     *            the bancsPolNum to set
     */
    public void setBancsPolNum(final String bancsPolNum) {
        this.bancsPolNum = bancsPolNum;
    }

    /**
     * Gets the formRequest
     *
     * @return the formRequest
     */
    public FormRequestDTO getFormRequest() {
        return formRequest;
    }

    /**
     * Sets the formRequest
     *
     * @param formRequest
     *            the formRequest to set
     */
    public void setFormRequest(final FormRequestDTO formRequest) {
        this.formRequest = formRequest;
    }

    /**
     * Gets the quoteDetails
     *
     * @return the quoteDetails
     */
    public QuoteDetails getQuoteDetails() {
        return quoteDetails;
    }

    /**
     * Sets the quoteDetails
     *
     * @param quoteDetails
     *            the quoteDetails to set
     */
    public void setQuoteDetails(final QuoteDetails quoteDetails) {
        this.quoteDetails = quoteDetails;
    }

    /**
     * Gets the username
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username
     *
     * @param username
     *            the username to set
     */
    public void setUsername(final String username) {
        this.username = username;
    }

    /**
     * Gets the customerId
     *
     * @return the customerId
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * Sets the customerId
     *
     * @param customerId
     *            the customerId to set
     */
    public void setCustomerId(final Long customerId) {
        this.customerId = customerId;
    }

    /**
     * Gets the policyId
     *
     * @return the policyId
     */
    public Long getPolicyId() {
        return policyId;
    }

    /**
     * Sets the policyId
     *
     * @param policyId
     *            the policyId to set
     */
    public void setPolicyId(final Long policyId) {
        this.policyId = policyId;
    }

    /**
     * Gets the policyNumber
     *
     * @return the policyNumber
     */
    public String getPolicyNumber() {
        return policyNumber;
    }

    /**
     * Sets the policyNumber
     *
     * @param policyNumber
     *            the policyNumber to set
     */
    public void setPolicyNumber(final String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getNino() {
        return nino;
    }

    public void setNino(final String nino) {
        this.nino = nino;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * @return the bancsUniqueCustNo
     */
    public String getBancsUniqueCustNo() {
        return bancsUniqueCustNo;
    }

    /**
     * @param bancsUniqueCustNo
     *            the bancsUniqueCustNo to set
     */
    public void setBancsUniqueCustNo(final String bancsUniqueCustNo) {
        this.bancsUniqueCustNo = bancsUniqueCustNo;
    }

    /**
     * Gets the dateOfBirth
     *
     * @return the dateOfBirth
     */
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the dateOfBirth
     *
     * @param dateOfBirth
     *            the dateOfBirth to set
     */
    public void setDateOfBirth(final Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * Gets the prefPhoneNo
     *
     * @return the prefPhoneNo
     */
    public String getPrefPhoneNo() {
        return prefPhoneNo;
    }

    /**
     * Sets the prefPhoneNo
     *
     * @param prefPhoneNo
     *            the prefPhoneNo to set
     */
    public void setPrefPhoneNo(final String prefPhoneNo) {
        this.prefPhoneNo = prefPhoneNo;
    }

    /**
     * Gets the houseNum
     *
     * @return the houseNum
     */
    public String getHouseNum() {
        return houseNum;
    }

    /**
     * Sets the houseNum
     *
     * @param houseNum
     *            the houseNum to set
     */
    public void setHouseNum(final String houseNum) {
        this.houseNum = houseNum;
    }

    /**
     * Gets the houseName
     *
     * @return the houseName
     */
    public String getHouseName() {
        return houseName;
    }

    /**
     * Sets the houseName
     *
     * @param houseName
     *            the houseName to set
     */
    public void setHouseName(final String houseName) {
        this.houseName = houseName;
    }

    /**
     * Gets the address1
     *
     * @return the address1
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * Sets the address1
     *
     * @param address1
     *            the address1 to set
     */
    public void setAddress1(final String address1) {
        this.address1 = address1;
    }

    /**
     * Gets the address2
     *
     * @return the address2
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * Sets the address2
     *
     * @param address2
     *            the address2 to set
     */
    public void setAddress2(final String address2) {
        this.address2 = address2;
    }

    /**
     * Gets the postalTownCity
     *
     * @return the postalTownCity
     */
    public String getPostalTownCity() {
        return postalTownCity;
    }

    /**
     * Sets the postalTownCity
     *
     * @param postalTownCity
     *            the postalTownCity to set
     */
    public void setPostalTownCity(final String postalTownCity) {
        this.postalTownCity = postalTownCity;
    }

    /**
     * Gets the postCode
     *
     * @return the postCode
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * Sets the postCode
     *
     * @param postCode
     *            the postCode to set
     */
    public void setPostCode(final String postCode) {
        this.postCode = postCode;
    }

    /**
     * Gets the county
     *
     * @return the county
     */
    public String getCounty() {
        return county;
    }

    /**
     * Sets the county
     *
     * @param county
     *            the county to set
     */
    public void setCounty(final String county) {
        this.county = county;
    }

    /**
     * Gets the country
     *
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the country
     *
     * @param country
     *            the country to set
     */
    public void setCountry(final String country) {
        this.country = country;
    }

    /**
     *
     * @param address
     */
    public void setAddress(final StringBuilder address) {
        // TODO Auto-generated method stub
        this.address = address;
    }

    /**
     *
     * @return
     */
    public StringBuilder getAddress() {
        return address;
    }

    /**
     *
     * @return
     */
    public List<QuestionsDTO> getQuestListPension() {
        return questListPension;
    }

    public BankDetailsNinoDTO getBankDetailsNinoDTO() {
        return bankDetailsNinoDTO;
    }

    public void setBankDetailsNinoDTO(final BankDetailsNinoDTO bankDetailsNinoDTO) {
        this.bankDetailsNinoDTO = bankDetailsNinoDTO;
    }

    /**
     *
     * @param questListPension
     */
    public void setQuestListPension(final List<QuestionsDTO> questListPension) {
        this.questListPension = questListPension;
    }

    /**
     *
     * @return
     */
    public List<QuestionsDTO> getQuestListCalculateTax() {
        return questListCalculateTax;
    }

    /**
     *
     * @param questListCalculateTax
     */
    public void setQuestListCalculateTax(final List<QuestionsDTO> questListCalculateTax) {
        this.questListCalculateTax = questListCalculateTax;
    }

    public BancsCalcCashInTaxOutput getBancsCalcCashInTaxOutput() {
        return bancsCalcCashInTaxOutput;
    }

    public void setBancsCalcCashInTaxOutput(final BancsCalcCashInTaxOutput bancsCalcCashInTaxOutput) {
        this.bancsCalcCashInTaxOutput = bancsCalcCashInTaxOutput;
    }

}
