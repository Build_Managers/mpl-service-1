package uk.co.phoenixlife.service.dto.policy;

import java.util.List;
import uk.co.phoenixlife.service.dto.response.ContactDetailsResponse;

/**
 * CustomerTableValues.java
 */
public class CustomerTableValues {

    private Long customerId;
    private String emailId;
    private String policyNumber;
    private ContactDetailsResponse contactDetailsResponse;
    private PartyBasicOwnerDTO partyBasicOwnerDTO;
    private List<Long> customerIdList;
    private String bancsPolNum;

     /**
     * Gets the bancsPolNum
     * @return the bancsPolNum
     */
    public String getBancsPolNum() {
        return bancsPolNum;
    }

    /**
     * Sets the bancsPolNum
    * @param pbancsPolNum the bancsPolNum to set
    */
    public void setBancsPolNum(final String pbancsPolNum) {
        this.bancsPolNum = pbancsPolNum;
    }

     /**
     * Gets the customerId
     * @return the customerId
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * Sets the customerId
     * @param pcustomerId
     *            the customerId to set
     */
    public void setCustomerId(final Long pcustomerId) {
        this.customerId = pcustomerId;
    }

    /**
     * Gets the emailId
     * @return the emailId
     */
    public String getEmailId() {
        return emailId;
    }

    /**
     * Sets the emailId
     * @param pemailId
     *            the emailId to set
     */
    public void setEmailId(final String pemailId) {
        this.emailId = pemailId;
    }

    /**
     * Gets the policyNumber
     * @return the policyNumber
     */
    public String getPolicyNumber() {
        return policyNumber;
    }

    /**
     * Sets the policyNumber
     * @param ppolicyNumber
     *            the policyNumber to set
     */
    public void setPolicyNumber(final String ppolicyNumber) {
        this.policyNumber = ppolicyNumber;
    }

    /**
     * Gets the contactDetailsResponse
     * @return the contactDetailsResponse
     */
    public ContactDetailsResponse getContactDetailsResponse() {
        return contactDetailsResponse;
    }

    /**
     * Sets the contactDetailsResponse
     * @param pcontactDetailsResponse
     *            the contactDetailsResponse to set
     */
    public void setContactDetailsResponse(final ContactDetailsResponse pcontactDetailsResponse) {
        this.contactDetailsResponse = pcontactDetailsResponse;
    }

    /**
     * Gets the customerIdList
     * @return the customerIdList
     */
    public List<Long> getCustomerIdList() {
        return customerIdList;
    }

    /**
     * Sets the customerIdList
     * @param pcustomerIdList
     *            the customerIdList to set
     */
    public void setCustomerIdList(final List<Long> pcustomerIdList) {
        this.customerIdList = pcustomerIdList;
    }

    /**
     * Gets the partyBasicOwnerDTO
     * @return the partyBasicOwnerDTO
     */
    public PartyBasicOwnerDTO getPartyBasicOwnerDTO() {
        return partyBasicOwnerDTO;
    }

    /**
     * Sets the partyBasicOwnerDTO
     * @param ppartyBasicOwnerDTO
     *            the partyBasicOwnerDTO to set
     */
    public void setPartyBasicOwnerDTO(final PartyBasicOwnerDTO ppartyBasicOwnerDTO) {
        this.partyBasicOwnerDTO = ppartyBasicOwnerDTO;
    }

}
