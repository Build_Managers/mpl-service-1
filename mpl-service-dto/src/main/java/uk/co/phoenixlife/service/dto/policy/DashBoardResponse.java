package uk.co.phoenixlife.service.dto.policy;

public class DashBoardResponse {

    private Long customerId;
    private String bancsUniqueCustomerNo;
    private String firstName;
    private String lastName;
    private String primaryPhoneNumber;
    private String title;
    private String email;
    private boolean userDoesNotExist;

    /**
     * Gets the userDoesNotExist
     * 
     * @return the userDoesNotExist
     */
    public boolean isUserDoesNotExist() {
        return userDoesNotExist;
    }

    /**
     * Sets the userDoesNotExist
     * 
     * @param userDoesNotExist
     *            the userDoesNotExist to set
     */
    public void setUserDoesNotExist(final boolean userDoesNotExist) {
        this.userDoesNotExist = userDoesNotExist;
    }

    /**
     * Gets the email
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email
     *
     * @param pemail
     *            the email to set
     */
    public void setEmail(final String pemail) {
        email = pemail;
    }

    /**
     * Gets the title
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title
     *
     * @param title
     *            the title to set
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * Gets the customerId
     *
     * @return the customerId
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * Sets the customerId
     *
     * @param customerId
     *            the customerId to set
     */
    public void setCustomerId(final Long customerId) {
        this.customerId = customerId;
    }

    /**
     * Gets the bancsUniqueCustomerNo
     *
     * @return the bancsUniqueCustomerNo
     */
    public String getBancsUniqueCustomerNo() {
        return bancsUniqueCustomerNo;
    }

    /**
     * Sets the bancsUniqueCustomerNo
     *
     * @param bancsUniqueCustomerNo
     *            the bancsUniqueCustomerNo to set
     */
    public void setBancsUniqueCustomerNo(final String bancsUniqueCustomerNo) {
        this.bancsUniqueCustomerNo = bancsUniqueCustomerNo;
    }

    /**
     * Gets the firstName
     *
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the firstName
     *
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the lastName
     *
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the lastName
     *
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the primaryPhoneNumber
     *
     * @return the primaryPhoneNumber
     */
    public String getPrimaryPhoneNumber() {
        return primaryPhoneNumber;
    }

    /**
     * Sets the primaryPhoneNumber
     *
     * @param primaryPhoneNumber
     *            the primaryPhoneNumber to set
     */
    public void setPrimaryPhoneNumber(final String primaryPhoneNumber) {
        this.primaryPhoneNumber = primaryPhoneNumber;
    }

}
