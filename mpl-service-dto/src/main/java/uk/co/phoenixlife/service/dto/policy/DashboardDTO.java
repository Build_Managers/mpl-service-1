package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * This DTO is to store data for Dashboard Policy List that will be iterated in
 * Dashboard Page DashboardDTO.java
 */
public class DashboardDTO implements Serializable {
    /** long */
    private static final long serialVersionUID = 1L;
    private String polNum;
    private List<DataCorrectionFlagDTO> listOfFlags;
    private Double totValue;
    private String gARorGAO;
    private Double mvaValue;
    private Double penValue;
    private Double cUCharge;
    private String mvrValue;
    private double estimatedValue;
    private String earlyExitCharge;
    private String partyProt;
    private String polSusp;
    private String polProt;
    private String penRevStatus;
    private String taxRegime;
    private String cashInInProgress;
    private List<FundDTO> listOfFunds;
    private String bancsPolNum;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private Date polIRD;
    private boolean eligible;
    private List<QuoteDTO> listOfQuotes;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'hh:mm:ss")
    private Timestamp busTimeStamp;

    /**
     * Gets the listOfFunds
     *
     * @return the listOfFunds
     */
    public List<FundDTO> getListOfFunds() {
        return listOfFunds;
    }

    /**
     * Sets the listOfFunds
     *
     * @param plistOfFunds
     *            the listOfFunds to set
     */
    public void setListOfFunds(final List<FundDTO> plistOfFunds) {
        this.listOfFunds = plistOfFunds;
    }

    /**
     * Method ot getPolNum
     *
     * @return the polNum
     */
    public String getPolNum() {
        return polNum;
    }

    /**
     * Method to setPolNum
     *
     * @param pPolNum
     *            the polNum to set
     */
    public void setPolNum(final String pPolNum) {
        this.polNum = pPolNum;
    }

    /**
     * Method to getListOfFlags
     *
     * @return the listOfFlags
     */
    public List<DataCorrectionFlagDTO> getListOfFlags() {
        return listOfFlags;
    }

    /**
     * Method to setListOfFlags
     *
     * @param plistOfFlags
     *            the listOfFlags to set
     */
    public void setListOfFlags(final List<DataCorrectionFlagDTO> plistOfFlags) {
        this.listOfFlags = plistOfFlags;
    }

    /**
     * Method to getTotValue
     *
     * @return the totValue
     */
    public Double getTotValue() {
        return totValue;
    }

    /**
     * Method to setTotValue
     *
     * @param ptotValue
     *            the totValue to set
     */
    public void setTotValue(final Double ptotValue) {
        this.totValue = ptotValue;
    }

    /**
     * Method to GetgARorGAO
     *
     * @return the gARorGAO
     */
    public String getgARorGAO() {
        return gARorGAO;
    }

    /**
     * Method to setgARorGAO
     *
     * @param pgARorGAO
     *            the gARorGAO to set
     */
    public void setgARorGAO(final String pgARorGAO) {
        this.gARorGAO = pgARorGAO;
    }

    /**
     * Method to getMvaValue
     *
     * @return the mvaValue
     */
    public Double getMvaValue() {
        return mvaValue;
    }

    /**
     * Method to setMvaValue
     *
     * @param pmvaValue
     *            the mvaValue to set
     */
    public void setMvaValue(final Double pmvaValue) {
        this.mvaValue = pmvaValue;
    }

    /**
     * Method to getPenValue
     *
     * @return the penValue
     */
    public Double getPenValue() {
        return penValue;
    }

    /**
     * Method to setPenValue
     *
     * @param pPenValue
     *            the penValue to set
     */
    public void setPenValue(final Double pPenValue) {
        this.penValue = pPenValue;
    }

    /**
     * Method to getcUCharge
     *
     * @return the cUCharge
     */
    public Double getcUCharge() {
        return cUCharge;
    }

    /**
     * Method to setcUCharge
     *
     * @param pcUCharge
     *            the cUCharge to set
     */
    public void setcUCharge(final Double pcUCharge) {
        this.cUCharge = pcUCharge;
    }

    /**
     * Method to setMvrValue
     *
     * @return the mvrValue
     */
    public String getMvrValue() {
        return mvrValue;
    }

    /**
     * Method to setMvrValue
     *
     * @param pmvrValue
     *            the mvrValue to set
     */
    public void setMvrValue(final String pmvrValue) {
        this.mvrValue = pmvrValue;
    }

    /**
     * Method to getEstimatedValue
     *
     * @return the estimatedValue
     */
    public double getEstimatedValue() {
        return estimatedValue;
    }

    /**
     * Method to setEstimatedValue
     *
     * @param pestimatedValue
     *            the estimatedValue to set
     */
    public void setEstimatedValue(final double pestimatedValue) {
        this.estimatedValue = pestimatedValue;
    }

    /**
     * Method to setEarlyExitCharge
     *
     * @return the earlyExitCharge
     */
    public String getEarlyExitCharge() {
        return earlyExitCharge;
    }

    /**
     * Method to setEarlyExitCharge
     *
     * @param pearlyExitCharge
     *            the earlyExitCharge to set
     */
    public void setEarlyExitCharge(final String pearlyExitCharge) {
        this.earlyExitCharge = pearlyExitCharge;
    }

    /**
     * Method to getPartyProt
     *
     * @return the partyProt
     */
    public String getPartyProt() {
        return partyProt;
    }

    /**
     * Method to setPartyProt
     *
     * @param pPartyProt
     *            the partyProt to set
     */
    public void setPartyProt(final String pPartyProt) {
        this.partyProt = pPartyProt;
    }

    /**
     * Method to getPolSusp
     *
     * @return the polSusp
     */
    public String getPolSusp() {
        return polSusp;
    }

    /**
     * Method to setPolSusp
     *
     * @param pPolSusp
     *            the polSusp to set
     */
    public void setPolSusp(final String pPolSusp) {
        this.polSusp = pPolSusp;
    }

    /**
     * Method to getPolProt
     *
     * @return the polProt
     */
    public String getPolProt() {
        return polProt;
    }

    /**
     * Method to setPolProt
     *
     * @param pPolProt
     *            the polProt to set
     */
    public void setPolProt(final String pPolProt) {
        this.polProt = pPolProt;
    }

    /**
     * Method to getPenRevStatus
     *
     * @return the penRevStatus
     */
    public String getPenRevStatus() {
        return penRevStatus;
    }

    /**
     * Method to setPenRevStatus
     *
     * @param pPenRevStatus
     *            the penRevStatus to set
     */
    public void setPenRevStatus(final String pPenRevStatus) {
        this.penRevStatus = pPenRevStatus;
    }

    /**
     * Method to getTaxRegime
     *
     * @return the taxRegime
     */
    public String getTaxRegime() {
        return taxRegime;
    }

    /**
     * Method to setTaxRegime
     *
     * @param ptaxRegime
     *            the taxRegime to set
     */
    public void setTaxRegime(final String ptaxRegime) {
        this.taxRegime = ptaxRegime;
    }

    /**
     * Method to getCashInInProgress
     *
     * @return the cashInInProgress
     */
    public String getCashInInProgress() {
        return cashInInProgress;
    }

    /**
     * Method to setCashInInProgress
     *
     * @param pcashInInProgress
     *            the cashInInProgress to set
     */
    public void setCashInInProgress(final String pcashInInProgress) {
        this.cashInInProgress = pcashInInProgress;
    }

    /**
     * Method to getBancsPolNum
     *
     * @return the bancsPolNum
     */
    public String getBancsPolNum() {
        return bancsPolNum;
    }

    /**
     * Method to setBancsPolNum
     *
     * @param pbancsPolNum
     *            the bancsPolNum to set
     */
    public void setBancsPolNum(final String pbancsPolNum) {
        this.bancsPolNum = pbancsPolNum;
    }

    /**
     * Method to getPolIRD
     *
     * @return the polIRD
     */
    public Date getPolIRD() {
        return polIRD;
    }

    /**
     * Method to set PolIRD
     *
     * @param pPolIRD
     *            the polIRD to set
     */
    public void setPolIRD(final Date pPolIRD) {
        this.polIRD = pPolIRD;
    }

    /**
     * Method to check if the policy is eligible or not
     *
     * @return the eligible
     */
    public boolean isEligible() {
        return eligible;
    }

    /**
     * Method to set eligible status
     *
     * @param peligible
     *            the eligible to set
     */
    public void setEligible(final boolean peligible) {
        this.eligible = peligible;
    }

    /**
     * Method to getListOfQuotes
     *
     * @return the listOfQuotes
     */
    public List<QuoteDTO> getListOfQuotes() {
        return listOfQuotes;
    }

    /**
     * Method to setListOfQuotes
     *
     * @param plistOfQuotes
     *            the listOfQuotes to set
     */
    public void setListOfQuotes(final List<QuoteDTO> plistOfQuotes) {
        this.listOfQuotes = plistOfQuotes;
    }

    /**
     * Method to getBusTimeStamp
     *
     * @return the busTimeStamp
     */
    public Timestamp getBusTimeStamp() {
        return busTimeStamp;
    }

    /**
     * Method to setBusTimeStamp
     *
     * @param pbusTimeStamp
     *            the busTimeStamp to set
     */
    public void setBusTimeStamp(final Timestamp pbusTimeStamp) {
        this.busTimeStamp = pbusTimeStamp;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
