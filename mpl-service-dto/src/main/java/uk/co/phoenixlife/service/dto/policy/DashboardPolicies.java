/*
 * DashboardPolicies.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * DashboardPolicies.java
 */
public class DashboardPolicies implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String polNum;
    private Double mvaValue;
    private Double totValue;
    private char gARorGAO;
    private char polProt;
    private List<DataCorrectionFlagDTO> dataCorrectionFlagList;
    private Double penValue;
    private Double cUCharge;
    private char partyProt;
    private char polSusp;
    private String penRevStatus;
    private String taxRegime;

    /**
     * Gets the polNum
     *
     * @return the polNum
     */
    public String getPolNum() {
        return polNum;
    }

    /**
     * Sets the polNum
     *
     * @param pPolNum
     *            the polNum to set
     */
    public void setPolNum(final String pPolNum) {
        polNum = pPolNum;
    }

    /**
     * Gets the mvaValue
     *
     * @return the mvaValue
     */
    public Double getMvaValue() {
        return mvaValue;
    }

    /**
     * Sets the mvaValue
     *
     * @param pMvaValue
     *            the mvaValue to set
     */
    public void setMvaValue(final Double pMvaValue) {
        mvaValue = pMvaValue;
    }

    /**
     * Gets the totValue
     *
     * @return the totValue
     */
    public Double getTotValue() {
        return totValue;
    }

    /**
     * Sets the totValue
     *
     * @param pTotValue
     *            the totValue to set
     */
    public void setTotValue(final Double pTotValue) {
        totValue = pTotValue;
    }

    /**
     * Gets the gARorGAO
     *
     * @return the gARorGAO
     */
    public char getgARorGAO() {
        return gARorGAO;
    }

    /**
     * Sets the gARorGAO
     *
     * @param pGARorGAO
     *            the gARorGAO to set
     */
    public void setgARorGAO(final char pGARorGAO) {
        gARorGAO = pGARorGAO;
    }

    /**
     * Gets the polProt
     *
     * @return the polProt
     */
    public char getPolProt() {
        return polProt;
    }

    /**
     * Sets the polProt
     *
     * @param pPolProt
     *            the polProt to set
     */
    public void setPolProt(final char pPolProt) {
        polProt = pPolProt;
    }

    /**
     * Gets the dataCorrectionFlagList
     *
     * @return the dataCorrectionFlagList
     */
    public List<DataCorrectionFlagDTO> getDataCorrectionFlagList() {
        return dataCorrectionFlagList;
    }

    /**
     * Sets the dataCorrectionFlagList
     *
     * @param pDataCorrectionFlagList
     *            the dataCorrectionFlagList to set
     */
    public void setDataCorrectionFlagList(final List<DataCorrectionFlagDTO> pDataCorrectionFlagList) {
        dataCorrectionFlagList = pDataCorrectionFlagList;
    }

    /**
     * Gets the penValue
     *
     * @return the penValue
     */
    public Double getPenValue() {
        return penValue;
    }

    /**
     * Sets the penValue
     *
     * @param pPenValue
     *            the penValue to set
     */
    public void setPenValue(final Double pPenValue) {
        penValue = pPenValue;
    }

    /**
     * Gets the cUCharge
     *
     * @return the cUCharge
     */
    public Double getcUCharge() {
        return cUCharge;
    }

    /**
     * Sets the cUCharge
     *
     * @param pCUCharge
     *            the cUCharge to set
     */
    public void setcUCharge(final Double pCUCharge) {
        cUCharge = pCUCharge;
    }

    /**
     * Gets the partyProt
     *
     * @return the partyProt
     */
    public char getPartyProt() {
        return partyProt;
    }

    /**
     * Sets the partyProt
     *
     * @param pPartyProt
     *            the partyProt to set
     */
    public void setPartyProt(final char pPartyProt) {
        partyProt = pPartyProt;
    }

    /**
     * Gets the polSusp
     *
     * @return the polSusp
     */
    public char getPolSusp() {
        return polSusp;
    }

    /**
     * Sets the polSusp
     *
     * @param pPolSusp
     *            the polSusp to set
     */
    public void setPolSusp(final char pPolSusp) {
        polSusp = pPolSusp;
    }

    /**
     * Gets the penRevStatus
     *
     * @return the penRevStatus
     */
    public String getPenRevStatus() {
        return penRevStatus;
    }

    /**
     * Sets the penRevStatus
     *
     * @param pPenRevStatus
     *            the penRevStatus to set
     */
    public void setPenRevStatus(final String pPenRevStatus) {
        penRevStatus = pPenRevStatus;
    }

    /**
     * Gets the taxRegime
     *
     * @return the taxRegime
     */
    public String getTaxRegime() {
        return taxRegime;
    }

    /**
     * Sets the taxRegime
     *
     * @param pTaxRegime
     *            the taxRegime to set
     */
    public void setTaxRegime(final String pTaxRegime) {
        taxRegime = pTaxRegime;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
