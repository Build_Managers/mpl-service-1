/*
 * DataCorrectionFlagDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * DataCorrectionFlagDTO.java
 */
public class DataCorrectionFlagDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String dataCorrFlag;

    /**
     * Gets the dataCorrFlag
     *
     * @return the dataCorrFlag
     */
    public String getDataCorrFlag() {
        return dataCorrFlag;
    }

    /**
     * Sets the dataCorrFlag
     *
     * @param pDataCorrFlag
     *            the dataCorrFlag to set
     */
    public void setDataCorrFlag(final String pDataCorrFlag) {
        dataCorrFlag = pDataCorrFlag;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}