package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * This DTO is Final DTO to be maintained in session for further processing
 * after DASHBOARD PAGE FinalDashboardDTO.java
 */
public class FinalDashboardDTO implements Serializable {
    /** long */
    private static final long serialVersionUID = 1L;
    private List<DashboardDTO> dashboardPolicyList;
    private String email;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private Date dateOfBirth;
    private String forename;
    private String surname;
    private String title;
    private String bancslegacyCustNo;
    private String address1;
    private String houseNum;
    private String houseName;
    private String address2;
    private String cityTown;
    private String county;
    private String mobPhoneNum;
    private String primaryPhoneNum;
    private String secPhoneNum;
    private String additionalPhoneNum;
    private String partyProt;
    private String postCode;
    private String bancsUniqueCustNo;
    private String niNo;
    private String polNum;
    private String prodName;
    private String country;

    /**
     * Method to getCountry
     *
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Method to setCountry
     *
     * @param pcountry
     *            the country to set
     */
    public void setCountry(final String pcountry) {
        country = pcountry;
    }

    /**
     * Gets the dashboardPolicyList
     *
     * @return the dashboardPolicyList
     */
    public List<DashboardDTO> getDashboardPolicyList() {
        return dashboardPolicyList;
    }

    /**
     * Sets the dashboardPolicyList
     *
     * @param pDashboardPolicyList
     *            the dashboardPolicyList to set
     */
    public void setDashboardPolicyList(final List<DashboardDTO> pDashboardPolicyList) {
        dashboardPolicyList = pDashboardPolicyList;
    }

    /**
     * Gets the polNum
     *
     * @return the polNum
     */
    public String getPolNum() {
        return polNum;
    }

    /**
     * Sets the polNum
     *
     * @param pPolNum
     *            the polNum to set
     */
    public void setPolNum(final String pPolNum) {
        polNum = pPolNum;
    }

    /**
     * Gets the partyProt
     *
     * @return the partyProt
     */
    public String getPartyProt() {
        return partyProt;
    }

    /**
     * Sets the partyProt
     *
     * @param pPartyProt
     *            the partyProt to set
     */
    public void setPartyProt(final String pPartyProt) {
        partyProt = pPartyProt;
    }

    /**
     * Gets the title
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title
     *
     * @param pTitle
     *            the title to set
     */
    public void setTitle(final String pTitle) {
        title = pTitle;
    }

    /**
     * Gets the forename
     *
     * @return the forename
     */
    public String getForename() {
        return forename;
    }

    /**
     * Sets the forename
     *
     * @param pForename
     *            the forename to set
     */
    public void setForename(final String pForename) {
        forename = pForename;
    }

    /**
     * Gets the bancslegacyCustNo
     *
     * @return the bancslegacyCustNo
     */
    public String getBancslegacyCustNo() {
        return bancslegacyCustNo;
    }

    /**
     * Sets the bancslegacyCustNo
     *
     * @param pBancslegacyCustNo
     *            the bancslegacyCustNo to set
     */
    public void setBancslegacyCustNo(final String pBancslegacyCustNo) {
        bancslegacyCustNo = pBancslegacyCustNo;
    }

    /**
     * Gets the niNo
     *
     * @return the niNo
     */
    public String getNiNo() {
        return niNo;
    }

    /**
     * Sets the niNo
     *
     * @param pNiNo
     *            the niNo to set
     */
    public void setNiNo(final String pNiNo) {
        niNo = pNiNo;
    }

    /**
     * Gets the surname
     *
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets the surname
     *
     * @param pSurname
     *            the surname to set
     */
    public void setSurname(final String pSurname) {
        surname = pSurname;
    }

    /**
     * Gets the bancsUniqueCustNo
     *
     * @return the bancsUniqueCustNo
     */
    public String getBancsUniqueCustNo() {
        return bancsUniqueCustNo;
    }

    /**
     * Sets the bancsUniqueCustNo
     *
     * @param pBancsUniqueCustNo
     *            the bancsUniqueCustNo to set
     */
    public void setBancsUniqueCustNo(final String pBancsUniqueCustNo) {
        bancsUniqueCustNo = pBancsUniqueCustNo;
    }

    /**
     * Gets the dateOfBirth
     *
     * @return the dateOfBirth
     */
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the dateOfBirth
     *
     * @param pDateOfBirth
     *            the dateOfBirth to set
     */
    public void setDateOfBirth(final Date pDateOfBirth) {
        dateOfBirth = pDateOfBirth;
    }

    /**
     * Gets the houseNum
     *
     * @return the houseNum
     */
    public String getHouseNum() {
        return houseNum;
    }

    /**
     * Sets the houseNum
     *
     * @param pHouseNum
     *            the houseNum to set
     */
    public void setHouseNum(final String pHouseNum) {
        houseNum = pHouseNum;
    }

    /**
     * Gets the houseName
     *
     * @return the houseName
     */
    public String getHouseName() {
        return houseName;
    }

    /**
     * Sets the houseName
     *
     * @param pHouseName
     *            the houseName to set
     */
    public void setHouseName(final String pHouseName) {
        houseName = pHouseName;
    }

    /**
     * Gets the address1
     *
     * @return the address1
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * Sets the address1
     *
     * @param pAddress1
     *            the address1 to set
     */
    public void setAddress1(final String pAddress1) {
        address1 = pAddress1;
    }

    /**
     * Gets the address2
     *
     * @return the address2
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * Sets the address2
     *
     * @param pAddress2
     *            the address2 to set
     */
    public void setAddress2(final String pAddress2) {
        address2 = pAddress2;
    }

    /**
     * Gets the cityTown
     *
     * @return the cityTown
     */
    public String getCityTown() {
        return cityTown;
    }

    /**
     * Sets the cityTown
     *
     * @param pCityTown
     *            the cityTown to set
     */
    public void setCityTown(final String pCityTown) {
        cityTown = pCityTown;
    }

    /**
     * Gets the county
     *
     * @return the county
     */
    public String getCounty() {
        return county;
    }

    /**
     * Sets the county
     *
     * @param pCounty
     *            the county to set
     */
    public void setCounty(final String pCounty) {
        county = pCounty;
    }

    /**
     * Gets the mobPhoneNum
     *
     * @return the mobPhoneNum
     */
    public String getMobPhoneNum() {
        return mobPhoneNum;
    }

    /**
     * Sets the mobPhoneNum
     *
     * @param pMobPhoneNum
     *            the mobPhoneNum to set
     */
    public void setMobPhoneNum(final String pMobPhoneNum) {
        mobPhoneNum = pMobPhoneNum;
    }

    /**
     * Gets the postCode
     *
     * @return the postCode
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * Sets the postCode
     *
     * @param pPostCode
     *            the postCode to set
     */
    public void setPostCode(final String pPostCode) {
        postCode = pPostCode;
    }

    /**
     * Gets the email
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email
     *
     * @param pEmail
     *            the email to set
     */
    public void setEmail(final String pEmail) {
        email = pEmail;
    }

    /**
     * Gets the prodName
     *
     * @return the prodName
     */
    public String getProdName() {
        return prodName;
    }

    /**
     * Sets the prodName
     *
     * @param pProdName
     *            the prodName to set
     */
    public void setProdName(final String pProdName) {
        prodName = pProdName;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    /**
     * Gets the SecPhoneNum
     *
     * @return the secPhoneNum
     */
    public String getSecPhoneNum() {
        return secPhoneNum;
    }

    /**
     * sets the SecPhoneNum
     *
     * @param psecPhoneNum
     *            the secPhoneNum to set
     */
    public void setSecPhoneNum(final String psecPhoneNum) {
        secPhoneNum = psecPhoneNum;
    }

    /**
     * Gets the additionalPhoneNum
     *
     * @return the additionalPhoneNum
     */
    public String getAdditionalPhoneNum() {
        return additionalPhoneNum;
    }

    /**
     * sets the additionalPhoneNum
     *
     * @param padditionalPhoneNum
     *            the additionalPhoneNum to set
     */
    public void setAdditionalPhoneNum(final String padditionalPhoneNum) {
        additionalPhoneNum = padditionalPhoneNum;
    }

    /**
     * Gets the primaryPhoneNum
     *
     * @return the primaryPhoneNum
     */
    public String getPrimaryPhoneNum() {
        return primaryPhoneNum;
    }

    /**
     * Sets the primaryPhoneNum
     *
     * @param pPrimaryPhoneNum
     *            the primaryPhoneNum to set
     */
    public void setPrimaryPhoneNum(final String pPrimaryPhoneNum) {
        primaryPhoneNum = pPrimaryPhoneNum;
    }

}
