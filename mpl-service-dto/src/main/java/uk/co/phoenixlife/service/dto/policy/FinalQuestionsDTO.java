package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;
import java.util.List;

/**
 * FinalQuestionsDTO FinalQuestionsDTO.java
 */
public class FinalQuestionsDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    private List<QuestionsDTO> lisofQues;

    /**
     * Method to getListOfQues
     *
     * @return lisOfQues
     */
    public List<QuestionsDTO> getLisofQues() {
        return lisofQues;
    }

    /**
     * Method to setLisOfQues
     *
     * @param plisofQues
     *            lisofQues
     */
    public void setLisofQues(final List<QuestionsDTO> plisofQues) {
        this.lisofQues = plisofQues;
    }

}
