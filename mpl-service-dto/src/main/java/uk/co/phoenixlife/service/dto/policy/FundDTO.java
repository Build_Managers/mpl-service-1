/*
 * FundDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * FundDTO.java
 */
public class FundDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String fundName;

    /**
     * Gets the fundName
     *
     * @return the fundName
     */
    public String getFundName() {
        return fundName;
    }

    /**
     * Sets the fundName
     *
     * @param pFundName
     *            the fundName to set
     */
    public void setFundName(final String pFundName) {
        fundName = pFundName;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}