/*
 * OwnerDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * OwnerDTO.java
 */
public class OwnerDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private Date dateOfBirth;
    private String partyType;
    private char fcuFlag;
    private String forename;
    private char poaFlag;
    private Date bankruptcyDate;
    private String bankruptcyRefNo;
    private String addressStatus;
    private String postCode;
    private String surname;
    private String address1;
    private String address2;
    private Date courtOrderDate;
    private Date bankruptcyDischargeDate;
    private String title;
    private String houseNum;
    private String houseName;
    private String courtName;
    private String county;
    private String cityTown;
    private long bancsUniqueCustNo;

    /**
     * Accessor for DOB
     *
     * @return dateOfBirth
     */
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Mutator for dateOfBirth variable
     *
     * @param dateOfBirthParam
     *            for mutator
     */
    public void setDateOfBirth(final Date dateOfBirthParam) {
        dateOfBirth = dateOfBirthParam;
    }

    /**
     * Accessor for PartyType
     *
     * @return partyType accessor
     */
    public String getPartyType() {
        return partyType;
    }

    /**
     * Mutator for partyType
     *
     * @param partyTypeParam
     *            mutator
     */
    public void setPartyType(final String partyTypeParam) {
        partyType = partyTypeParam;
    }

    /**
     * Accessor for FCUFlag variable
     *
     * @return fcuFlag variable
     */
    public char getFcuFlag() {
        return fcuFlag;
    }

    /**
     * Mutator for fcuFlag Variable
     *
     * @param fcuFlagParam
     *            a mutator
     */
    public void setFcuFlag(final char fcuFlagParam) {
        fcuFlag = fcuFlagParam;
    }

    /**
     * Accessor for forename
     *
     * @return forename variable
     */
    public String getForename() {
        return forename;
    }

    /**
     * Mutator for forename variable
     *
     * @param forenameParam
     *            mutator variable
     */
    public void setForename(final String forenameParam) {
        forename = forenameParam;
    }

    /**
     * Accessor
     *
     * @return poaFlag variable
     */
    public char getPoaFlag() {
        return poaFlag;
    }

    /**
     * Mutator
     *
     * @param poaFlagParam
     *            mutator
     */
    public void setPoaFlag(final char poaFlagParam) {
        poaFlag = poaFlagParam;
    }

    /**
     * Accessor
     *
     * @return bankruptcyDate variable
     */
    public Date getBankruptcyDate() {
        return bankruptcyDate;
    }

    /**
     * Mutator
     *
     * @param bankruptcyDateParam
     *            mutator
     */
    public void setBankruptcyDate(final Date bankruptcyDateParam) {
        bankruptcyDate = bankruptcyDateParam;
    }

    /**
     * Accessor
     *
     * @return bankruptcyRefNo variable
     */
    public String getBankruptcyRefNo() {
        return bankruptcyRefNo;
    }

    /**
     * Mutator
     *
     * @param bankruptcyRefNoParam
     *            variable
     */
    public void setBankruptcyRefNo(final String bankruptcyRefNoParam) {
        bankruptcyRefNo = bankruptcyRefNoParam;
    }

    /**
     * Accessor
     *
     * @return addressStatus variable
     */
    public String getAddressStatus() {
        return addressStatus;
    }

    /**
     * Mutator
     *
     * @param addressStatusParam
     *            variable
     */
    public void setAddressStatus(final String addressStatusParam) {
        addressStatus = addressStatusParam;
    }

    /**
     * Accessor
     *
     * @return postCode variable
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * Mutator
     *
     * @param postCodeParam
     *            variable
     */
    public void setPostCode(final String postCodeParam) {
        postCode = postCodeParam;
    }

    /**
     * Accessor
     *
     * @return surname variable
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Mutator
     *
     * @param surnameParam
     *            variable
     */
    public void setSurname(final String surnameParam) {
        surname = surnameParam;
    }

    /**
     * Accessor
     *
     * @return address1 variable
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * Mutator
     *
     * @param address1Param
     *            variable
     */
    public void setAddress1(final String address1Param) {
        address1 = address1Param;
    }

    /**
     * Accessor
     *
     * @return address2 variable
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * Mutator
     *
     * @param address2Param
     *            variable
     */
    public void setAddress2(final String address2Param) {
        address2 = address2Param;
    }

    /**
     * Accessor
     *
     * @return courtOrderDate variable
     */
    public Date getCourtOrderDate() {
        return courtOrderDate;
    }

    /**
     * Mutator
     *
     * @param courtOrderDateParam
     *            variable
     */
    public void setCourtOrderDate(final Date courtOrderDateParam) {
        courtOrderDate = courtOrderDateParam;
    }

    /**
     * Accessor
     *
     * @return bankruptcyDischargeDate variable
     */
    public Date getBankruptcyDischargeDate() {
        return bankruptcyDischargeDate;
    }

    /**
     * Mutator
     *
     * @param bankruptcyDischargeDateParam
     *            variable
     */
    public void setBankruptcyDischargeDate(final Date bankruptcyDischargeDateParam) {
        bankruptcyDischargeDate = bankruptcyDischargeDateParam;
    }

    /**
     * Accessor
     *
     * @return title variable
     */
    public String getTitle() {
        return title;
    }

    /**
     * Mutator
     *
     * @param titleParam
     *            variable
     */
    public void setTitle(final String titleParam) {
        title = titleParam;
    }

    /**
     * Accessor
     *
     * @return houseNum variable
     */
    public String getHouseNum() {
        return houseNum;
    }

    /**
     * Mutator
     *
     * @param houseNumParam
     *            variable
     */
    public void setHouseNum(final String houseNumParam) {
        houseNum = houseNumParam;
    }

    /**
     * Accessor
     *
     * @return houseName variable
     */
    public String getHouseName() {
        return houseName;
    }

    /**
     * Mutator
     *
     * @param houseNameParam
     *            variable
     */
    public void setHouseName(final String houseNameParam) {
        houseName = houseNameParam;
    }

    /**
     * Accessor
     *
     * @return courtName variable
     */
    public String getCourtName() {
        return courtName;
    }

    /**
     * Mutator
     *
     * @param courtNameParam
     *            variable
     */
    public void setCourtName(final String courtNameParam) {
        courtName = courtNameParam;
    }

    /**
     * Accessor
     *
     * @return county variable
     */
    public String getCounty() {
        return county;
    }

    /**
     * Mutator
     *
     * @param countyParam
     *            variable
     */
    public void setCounty(final String countyParam) {
        county = countyParam;
    }

    /**
     * Accessor
     *
     * @return cityTown variable
     */
    public String getCityTown() {
        return cityTown;
    }

    /**
     * Mutator
     *
     * @param cityTownParam
     *            varaible
     */
    public void setCityTown(final String cityTownParam) {
        cityTown = cityTownParam;
    }

    /**
     * Accessor
     *
     * @return bancsUniqueCustNo variable
     */
    public long getBancsUniqueCustNo() {
        return bancsUniqueCustNo;
    }

    /**
     * Mutator
     *
     * @param bancsUniqueCustNoParam
     *            variable
     */
    public void setBancsUniqueCustNo(final long bancsUniqueCustNoParam) {
        bancsUniqueCustNo = bancsUniqueCustNoParam;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}