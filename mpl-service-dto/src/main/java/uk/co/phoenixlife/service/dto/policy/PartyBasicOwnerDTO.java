/*
 * PartyBasicOwnerDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * PartyBasicOwnerDTO.java
 */
public class PartyBasicOwnerDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private Date dateOfBirth;
    private String title;
    private String partyType;
    private String forename;
    private String bancsLegacyCustNo;
    private String niNo;
    private String surname;
    private String bancsUniqueCustNo;
    private String myPhoenixUserName;

    /**
     * Gets the myPhoenixUserName
     * 
     * @return the myPhoenixUserName
     */
    public String getMyPhoenixUserName() {
        return myPhoenixUserName;
    }

    /**
     * Sets the myPhoenixUserName
     * 
     * @param myPhoenixUserName
     *            the myPhoenixUserName to set
     */
    public void setMyPhoenixUserName(final String myPhoenixUserName) {
        this.myPhoenixUserName = myPhoenixUserName;
    }

    /**
     * Gets the dateOfBirth
     *
     * @return the dateOfBirth
     */
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the dateOfBirth
     *
     * @param pDateOfBirth
     *            the dateOfBirth to set
     */
    public void setDateOfBirth(final Date pDateOfBirth) {
        dateOfBirth = pDateOfBirth;
    }

    /**
     * Gets the title
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title
     *
     * @param pTitle
     *            the title to set
     */
    public void setTitle(final String pTitle) {
        title = pTitle;
    }

    /**
     * Gets the partyType
     *
     * @return the partyType
     */
    public String getPartyType() {
        return partyType;
    }

    /**
     * Sets the partyType
     *
     * @param pPartyType
     *            the partyType to set
     */
    public void setPartyType(final String pPartyType) {
        partyType = pPartyType;
    }

    /**
     * Gets the forename
     *
     * @return the forename
     */
    public String getForename() {
        return forename;
    }

    /**
     * Sets the forename
     *
     * @param pForename
     *            the forename to set
     */
    public void setForename(final String pForename) {
        forename = pForename;
    }

    /**
     * Gets the bancslegacyCustNo
     *
     * @return the bancslegacyCustNo
     */
    public String getBancsLegacyCustNo() {
        return bancsLegacyCustNo;
    }

    /**
     * Sets the bancslegacyCustNo
     *
     * @param pBancslegacyCustNo
     *            the bancslegacyCustNo to set
     */
    public void setBancsLegacyCustNo(final String pBancslegacyCustNo) {
        bancsLegacyCustNo = pBancslegacyCustNo;
    }

    /**
     * Gets the niNo
     *
     * @return the niNo
     */
    public String getNiNo() {
        return niNo;
    }

    /**
     * Sets the niNo
     *
     * @param pNiNo
     *            the niNo to set
     */
    public void setNiNo(final String pNiNo) {
        niNo = pNiNo;
    }

    /**
     * Gets the surname
     *
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets the surname
     *
     * @param pSurname
     *            the surname to set
     */
    public void setSurname(final String pSurname) {
        surname = pSurname;
    }

    /**
     * Gets the bancsUniqueCustNo
     *
     * @return the bancsUniqueCustNo
     */
    public String getBancsUniqueCustNo() {
        return bancsUniqueCustNo;
    }

    /**
     * Sets the bancsUniqueCustNo
     *
     * @param pBancsUniqueCustNo
     *            the bancsUniqueCustNo to set
     */
    public void setBancsUniqueCustNo(final String pBancsUniqueCustNo) {
        bancsUniqueCustNo = pBancsUniqueCustNo;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}