/*
 * PartyBasicOwnerListDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * PartyBasicOwnerListDTO.java
 */
public class PartyBasicOwnerListDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;
    private String polNum;
    private List<PartyBasicOwnerDTO> listOfOwners;

    /**
     * Method to getPolNum
     *
     * @return the polNum
     */
    public String getPolNum() {
        return polNum;
    }

    /**
     * Method to setPolNum
     *
     * @param pPolNum
     *            the polNum to set
     */
    public void setPolNum(final String pPolNum) {
        this.polNum = pPolNum;
    }

    /**
     * Method to getListOfOwners
     *
     * @return the listOfOwners
     */
    public List<PartyBasicOwnerDTO> getListOfOwners() {
        return listOfOwners;
    }

    /**
     * Method to setListOfOwners
     *
     * @param plistOfOwners
     *            the listOfOwners to set
     */
    public void setListOfOwners(final List<PartyBasicOwnerDTO> plistOfOwners) {
        this.listOfOwners = plistOfOwners;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
