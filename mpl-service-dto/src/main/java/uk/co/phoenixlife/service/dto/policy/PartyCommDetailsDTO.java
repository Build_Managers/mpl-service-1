/*
 * PartyCommDetailsDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * PartyCommDetailsDTO.java
 */
public class PartyCommDetailsDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String bancsUniqueCustNo;
    private String houseNum;
    private String houseName;
    private String address1;
    private String address2;
    private String cityTown;
    private String county;
    private String country;
    private String postCode;
    private String partyProt;
    private String mobPhoneNum;
    private String poaFlag;
    private String fcuFlag;
    private String bankruptcyRefNo;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private Date bankruptcyDischargeDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private Date bankruptcyDate;
    private String courtName;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private Date courtOrderDate;
    private String addressStatus;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'hh:mm:ss")
    private Timestamp busTimeStamp;

    /**
     * Gets the bancsUniqueCustNo
     *
     * @return the bancsUniqueCustNo
     */
    public String getBancsUniqueCustNo() {
        return bancsUniqueCustNo;
    }

    /**
     * Sets the bancsUniqueCustNo
     *
     * @param pBancsUniqueCustNo
     *            the bancsUniqueCustNo to set
     */
    public void setBancsUniqueCustNo(final String pBancsUniqueCustNo) {
        bancsUniqueCustNo = pBancsUniqueCustNo;
    }

    /**
     * Gets the houseNum
     *
     * @return the houseNum
     */
    public String getHouseNum() {
        return houseNum;
    }

    /**
     * Sets the houseNum
     *
     * @param pHouseNum
     *            the houseNum to set
     */
    public void setHouseNum(final String pHouseNum) {
        houseNum = pHouseNum;
    }

    /**
     * Gets the houseName
     *
     * @return the houseName
     */
    public String getHouseName() {
        return houseName;
    }

    /**
     * Sets the houseName
     *
     * @param pHouseName
     *            the houseName to set
     */
    public void setHouseName(final String pHouseName) {
        houseName = pHouseName;
    }

    /**
     * Gets the address1
     *
     * @return the address1
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * Sets the address1
     *
     * @param pAddress1
     *            the address1 to set
     */
    public void setAddress1(final String pAddress1) {
        address1 = pAddress1;
    }

    /**
     * Gets the address2
     *
     * @return the address2
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * Sets the address2
     *
     * @param pAddress2
     *            the address2 to set
     */
    public void setAddress2(final String pAddress2) {
        address2 = pAddress2;
    }

    /**
     * Gets the cityTown
     *
     * @return the cityTown
     */
    public String getCityTown() {
        return cityTown;
    }

    /**
     * Sets the cityTown
     *
     * @param pCityTown
     *            the cityTown to set
     */
    public void setCityTown(final String pCityTown) {
        cityTown = pCityTown;
    }

    /**
     * Gets the county
     *
     * @return the county
     */
    public String getCounty() {
        return county;
    }

    /**
     * Sets the county
     *
     * @param pCounty
     *            the county to set
     */
    public void setCounty(final String pCounty) {
        county = pCounty;
    }

    /**
     * Gets the country
     *
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the country
     *
     * @param pCountry
     *            the country to set
     */
    public void setCountry(final String pCountry) {
        country = pCountry;
    }

    /**
     * Gets the postCode
     *
     * @return the postCode
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * Sets the postCode
     *
     * @param pPostCode
     *            the postCode to set
     */
    public void setPostCode(final String pPostCode) {
        postCode = pPostCode;
    }

    /**
     * Gets the partyProt
     *
     * @return the partyProt
     */
    public String getPartyProt() {
        return partyProt;
    }

    /**
     * Sets the partyProt
     *
     * @param pPartyProt
     *            the partyProt to set
     */
    public void setPartyProt(final String pPartyProt) {
        partyProt = pPartyProt;
    }

    /**
     * Gets the mobPhoneNum
     *
     * @return the mobPhoneNum
     */
    public String getMobPhoneNum() {
        return mobPhoneNum;
    }

    /**
     * Sets the mobPhoneNum
     *
     * @param pMobPhoneNum
     *            the mobPhoneNum to set
     */
    public void setMobPhoneNum(final String pMobPhoneNum) {
        mobPhoneNum = pMobPhoneNum;
    }

    /**
     * Gets the poaFlag
     *
     * @return the poaFlag
     */
    public String getPoaFlag() {
        return poaFlag;
    }

    /**
     * Sets the poaFlag
     *
     * @param pPoaFlag
     *            the poaFlag to set
     */
    public void setPoaFlag(final String pPoaFlag) {
        poaFlag = pPoaFlag;
    }

    /**
     * Gets the fcuFlag
     *
     * @return the fcuFlag
     */
    public String getFcuFlag() {
        return fcuFlag;
    }

    /**
     * Sets the fcuFlag
     *
     * @param pFcuFlag
     *            the fcuFlag to set
     */
    public void setFcuFlag(final String pFcuFlag) {
        fcuFlag = pFcuFlag;
    }

    /**
     * Gets the bankruptcyRefNo
     *
     * @return the bankruptcyRefNo
     */
    public String getBankruptcyRefNo() {
        return bankruptcyRefNo;
    }

    /**
     * Sets the bankruptcyRefNo
     *
     * @param pBankruptcyRefNo
     *            the bankruptcyRefNo to set
     */
    public void setBankruptcyRefNo(final String pBankruptcyRefNo) {
        bankruptcyRefNo = pBankruptcyRefNo;
    }

    /**
     * Gets the bankruptcyDischargeDate
     *
     * @return the bankruptcyDischargeDate
     */
    public Date getBankruptcyDischargeDate() {
        return bankruptcyDischargeDate;
    }

    /**
     * Sets the bankruptcyDischargeDate
     *
     * @param pBankruptcyDischargeDate
     *            the bankruptcyDischargeDate to set
     */
    public void setBankruptcyDischargeDate(final Date pBankruptcyDischargeDate) {
        bankruptcyDischargeDate = pBankruptcyDischargeDate;
    }

    /**
     * Gets the bankruptcyDate
     *
     * @return the bankruptcyDate
     */
    public Date getBankruptcyDate() {
        return bankruptcyDate;
    }

    /**
     * Sets the bankruptcyDate
     *
     * @param pBankruptcyDate
     *            the bankruptcyDate to set
     */
    public void setBankruptcyDate(final Date pBankruptcyDate) {
        bankruptcyDate = pBankruptcyDate;
    }

    /**
     * Gets the courtName
     *
     * @return the courtName
     */
    public String getCourtName() {
        return courtName;
    }

    /**
     * Sets the courtName
     *
     * @param pCourtName
     *            the courtName to set
     */
    public void setCourtName(final String pCourtName) {
        courtName = pCourtName;
    }

    /**
     * Gets the courtOrderDate
     *
     * @return the courtOrderDate
     */
    public Date getCourtOrderDate() {
        return courtOrderDate;
    }

    /**
     * Sets the courtOrderDate
     *
     * @param pCourtOrderDate
     *            the courtOrderDate to set
     */
    public void setCourtOrderDate(final Date pCourtOrderDate) {
        courtOrderDate = pCourtOrderDate;
    }

    /**
     * Gets the addressStatus
     *
     * @return the addressStatus
     */
    public String getAddressStatus() {
        return addressStatus;
    }

    /**
     * Sets the addressStatus
     *
     * @param pAddressStatus
     *            the addressStatus to set
     */
    public void setAddressStatus(final String pAddressStatus) {
        addressStatus = pAddressStatus;
    }

    /**
     * Gets the busTimeStamp
     *
     * @return the busTimeStamp
     */
    public Timestamp getBusTimeStamp() {
        return busTimeStamp;
    }

    /**
     * Sets the busTimeStamp
     *
     * @param pBusTimeStamp
     *            the busTimeStamp to set
     */
    public void setBusTimeStamp(final Timestamp pBusTimeStamp) {
        busTimeStamp = pBusTimeStamp;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
