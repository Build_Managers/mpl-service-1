/*
 * PartyPolicyDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * PartyPolicyDTO.java
 */
public class PartyPolicyDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private List<BenefitDTO> listOfBenefits;
    private List<FundDTO> listOfFunds;
    private String polNum;
    private String bancsPolNum;
    private String sortCode;
    private String premFreq;
    private String bankAcct;
    private String polStatusLit;
    private String polStatusCd;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private Date polStDate;
    private Double netModPrem;
    private Double lastPremPaid;
    // @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private String premiumDueDate;
    private String paymentMethodLit;
    private String paymentMethodCd;

    /**
     * Gets the paymentMethodLit
     *
     * @return the paymentMethodLit
     */
    public String getPaymentMethodLit() {
        return paymentMethodLit;
    }

    /**
     * Sets the paymentMethodLit
     *
     * @param pPaymentMethodLit
     *            the paymentMethodLit to set
     */
    public void setPaymentMethodLit(final String pPaymentMethodLit) {
        this.paymentMethodLit = pPaymentMethodLit;
    }

    /**
     * Gets the paymentMethodCd
     *
     * @return the paymentMethodCd
     */
    public String getPaymentMethodCd() {
        return paymentMethodCd;
    }

    /**
     * Sets the paymentMethodCd
     *
     * @param pPaymentMethodCd
     *            the paymentMethodCd to set
     */
    public void setPaymentMethodCd(final String pPaymentMethodCd) {
        this.paymentMethodCd = pPaymentMethodCd;
    }

    /**
     * Gets the polStatusLit
     *
     * @return the polStatusLit
     */
    public String getPolStatusLit() {
        return polStatusLit;
    }

    /**
     * Sets the polStatusLit
     *
     * @param pPolStatusLit
     *            the polStatusLit to set
     */
    public void setPolStatusLit(final String pPolStatusLit) {
        this.polStatusLit = pPolStatusLit;
    }

    /**
     * Gets the polStatusCd
     *
     * @return the polStatusCd
     */
    public String getPolStatusCd() {
        return polStatusCd;
    }

    /**
     * Sets the polStatusCd
     *
     * @param pPolStatusCd
     *            the polStatusCd to set
     */
    public void setPolStatusCd(final String pPolStatusCd) {
        this.polStatusCd = pPolStatusCd;
    }

    /**
     * Method to getListOfBenefits
     *
     * @return the listOfBenefits
     */
    public List<BenefitDTO> getListOfBenefits() {
        return listOfBenefits;
    }

    /**
     * Method to setListOfBenefits
     *
     * @param plistOfBenefits
     *            the listOfBenefits to set
     */
    public void setListOfBenefits(final List<BenefitDTO> plistOfBenefits) {
        this.listOfBenefits = plistOfBenefits;
    }

    /**
     * Method to getListOfFunds
     *
     * @return the listOfFunds
     */
    public List<FundDTO> getListOfFunds() {
        return listOfFunds;
    }

    /**
     * Method to setListOfFunds
     *
     * @param plistOfFunds
     *            the listOfFunds to set
     */
    public void setListOfFunds(final List<FundDTO> plistOfFunds) {
        this.listOfFunds = plistOfFunds;
    }

    /**
     * Method to getPolNum
     *
     * @return the polNum
     */
    public String getPolNum() {
        return polNum;
    }

    /**
     * Method to setPolNum
     *
     * @param pPolNum
     *            the polNum to set
     */
    public void setPolNum(final String pPolNum) {
        this.polNum = pPolNum;
    }

    /**
     * Gets the bancsPolNum
     *
     * @return the bancsPolNum
     */
    public String getBancsPolNum() {
        return bancsPolNum;
    }

    /**
     * Sets the bancsPolNum
     *
     * @param bancsPolNumParam
     *            the bancsPolNum to set
     */
    public void setBancsPolNum(final String bancsPolNumParam) {
        bancsPolNum = bancsPolNumParam;
    }

    /**
     * Gets the sortCode
     *
     * @return the sortCode
     */
    public String getSortCode() {
        return sortCode;
    }

    /**
     * Sets the sortCode
     *
     * @param sortCodeParam
     *            the sortCode to set
     */
    public void setSortCode(final String sortCodeParam) {
        sortCode = sortCodeParam;
    }

    /**
     * Gets the premFreq
     *
     * @return the premFreq
     */
    public String getPremFreq() {
        return premFreq;
    }

    /**
     * Sets the premFreq
     *
     * @param premFreqParam
     *            the premFreq to set
     */
    public void setPremFreq(final String premFreqParam) {
        premFreq = premFreqParam;
    }

    /**
     * Gets the bankAcct
     *
     * @return the bankAcct
     */
    public String getBankAcct() {
        return bankAcct;
    }

    /**
     * Sets the bankAcct
     *
     * @param bankAcctParam
     *            the bankAcct to set
     */
    public void setBankAcct(final String bankAcctParam) {
        bankAcct = bankAcctParam;
    }

    /**
     * Gets the polStDate
     *
     * @return the polStDate
     */
    public Date getPolStDate() {
        return polStDate;
    }

    /**
     * Sets the polStDate
     *
     * @param polStDateParam
     *            the polStDate to set
     */
    public void setPolStDate(final Date polStDateParam) {
        polStDate = polStDateParam;
    }

    /**
     * Gets the netModPrem
     *
     * @return the netModPrem
     */
    public Double getNetModPrem() {
        return netModPrem;
    }

    /**
     * Sets the netModPrem
     *
     * @param netModPremParam
     *            the netModPrem to set
     */
    public void setNetModPrem(final Double netModPremParam) {
        netModPrem = netModPremParam;
    }

    /**
     * Gets the lastPremPaid
     *
     * @return the lastPremPaid
     */
    public Double getLastPremPaid() {
        return lastPremPaid;
    }

    /**
     * Sets the lastPremPaid
     *
     * @param lastPremPaidParam
     *            the lastPremPaid to set
     */
    public void setLastPremPaid(final Double lastPremPaidParam) {
        lastPremPaid = lastPremPaidParam;
    }

    /**
     * Gets the premiumDueDate
     *
     * @return the premiumDueDate
     */
    public String getPremiumDueDate() {
        return premiumDueDate;
    }

    /**
     * Sets the premiumDueDate
     *
     * @param premiumDueDateParam
     *            the premiumDueDate to set
     */
    public void setPremiumDueDate(final String premiumDueDateParam) {
        premiumDueDate = premiumDueDateParam;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}