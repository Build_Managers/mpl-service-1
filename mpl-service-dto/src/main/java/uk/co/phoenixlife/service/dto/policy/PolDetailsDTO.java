/*
 * PolDetailsDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * PolDetailsDTO.java
 */
public class PolDetailsDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String polProt;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'hh:mm:ss")
    private Timestamp busTimeStamp;
    private String gARorGAO;
    private List<PolicyRoleDTO> listOfRoles;
    private String bancsPolNum;
    private List<DataCorrectionFlagDTO> listOfFlags;
    private String cashInInProgress;
    private String trustBasedApplic;
    private String accrualBasisCd;
    private String accrualBasisLit;
    private String unitisedPol;
    private String taxRegimeLit;
    private String penRevStatusLit;
    private String penRevStatusCd;

    /**
     * Gets the penRevStatusLit
     *
     * @return the penRevStatusLit
     */
    public String getPenRevStatusLit() {
        return penRevStatusLit;
    }

    /**
     * Sets the penRevStatusLit
     *
     * @param pPenRevStatusLit
     *            the penRevStatusLit to set
     */
    public void setPenRevStatusLit(final String pPenRevStatusLit) {
        this.penRevStatusLit = pPenRevStatusLit;
    }

    /**
     * Gets the penRevStatusCd
     *
     * @return the penRevStatusCd
     */
    public String getPenRevStatusCd() {
        return penRevStatusCd;
    }

    /**
     * Sets the penRevStatusCd
     *
     * @param pPenRevStatusCd
     *            the penRevStatusCd to set
     */
    public void setPenRevStatusCd(final String pPenRevStatusCd) {
        this.penRevStatusCd = pPenRevStatusCd;
    }

    /**
     * Gets the unitisedPol
     *
     * @return the unitisedPol
     */
    public String getUnitisedPol() {
        return unitisedPol;
    }

    /**
     * Sets the unitisedPol
     *
     * @param punitisedPol
     *            the unitisedPol to set
     */
    public void setUnitisedPol(final String punitisedPol) {
        this.unitisedPol = punitisedPol;
    }

    /**
     * Method to getAccrualBasis
     *
     * @return the accrualBasisCd
     */
    public String getAccrualBasisCd() {
        return accrualBasisCd;
    }

    /**
     * Method to setAccrualbasis
     *
     * @param paccrualBasisCd
     *            the accrualBasisCd to set
     */
    public void setAccrualBasisCd(final String paccrualBasisCd) {
        this.accrualBasisCd = paccrualBasisCd;
    }

    /**
     * Method to getAccrualBasisLit
     *
     * @return the accrualBasisLit
     */
    public String getAccrualBasisLit() {
        return accrualBasisLit;
    }

    /**
     * Method to setAccrualBasisLit
     *
     * @param paccrualBasisLit
     *            the accrualBasisLit to set
     */
    public void setAccrualBasisLit(final String paccrualBasisLit) {
        this.accrualBasisLit = paccrualBasisLit;
    }

    /**
     * Gets the polProt
     *
     * @return the polProt
     */
    public String getPolProt() {
        return polProt;
    }

    /**
     * Sets the polProt
     *
     * @param pPolProt
     *            the polProt to set
     */
    public void setPolProt(final String pPolProt) {
        polProt = pPolProt;
    }

    /**
     * Gets the busTimeStamp
     *
     * @return the busTimeStamp
     */
    public Timestamp getBusTimeStamp() {
        return busTimeStamp;
    }

    /**
     * Sets the busTimeStamp
     *
     * @param pBusTimeStamp
     *            the busTimeStamp to set
     */
    public void setBusTimeStamp(final Timestamp pBusTimeStamp) {
        busTimeStamp = pBusTimeStamp;
    }

    /**
     * Gets the gARorGAO
     *
     * @return the gARorGAO
     */
    public String getgARorGAO() {
        return gARorGAO;
    }

    /**
     * Sets the gARorGAO
     *
     * @param pGARorGAO
     *            the gARorGAO to set
     */
    public void setgARorGAO(final String pGARorGAO) {
        gARorGAO = pGARorGAO;
    }

    /**
     * Gets the bancsPolNum
     *
     * @return the bancsPolNum
     */
    public String getBancsPolNum() {
        return bancsPolNum;
    }

    /**
     * Sets the bancsPolNum
     *
     * @param pBancsPolNum
     *            the bancsPolNum to set
     */
    public void setBancsPolNum(final String pBancsPolNum) {
        bancsPolNum = pBancsPolNum;
    }

    /**
     * Method to getListOfRoles
     *
     * @return the listOfRoles
     */
    public List<PolicyRoleDTO> getListOfRoles() {
        return listOfRoles;
    }

    /**
     * Method to setListOfRoles
     *
     * @param plistOfRoles
     *            the listOfRoles to set
     */
    public void setListOfRoles(final List<PolicyRoleDTO> plistOfRoles) {
        this.listOfRoles = plistOfRoles;
    }

    /**
     * Method to getListofFlgs
     *
     * @return the listOfFlags
     */
    public List<DataCorrectionFlagDTO> getListOfFlags() {
        return listOfFlags;
    }

    /**
     * Method to setListofFlags
     *
     * @param plistOfFlags
     *            the listOfFlags to set
     */
    public void setListOfFlags(final List<DataCorrectionFlagDTO> plistOfFlags) {
        this.listOfFlags = plistOfFlags;
    }

    /**
     * Gets the cashInInProgress
     *
     * @return the cashInInProgress
     */
    public String getCashInInProgress() {
        return cashInInProgress;
    }

    /**
     * Sets the cashInInProgress
     *
     * @param pCashInInProgress
     *            the cashInInProgress to set
     */
    public void setCashInInProgress(final String pCashInInProgress) {
        cashInInProgress = pCashInInProgress;
    }

    /**
     * Gets the trustBasedApplic
     *
     * @return the trustBasedApplic
     */
    public String getTrustBasedApplic() {
        return trustBasedApplic;
    }

    /**
     * Sets the trustBasedApplic
     *
     * @param pTrustBasedApplic
     *            the trustBasedApplic to set
     */
    public void setTrustBasedApplic(final String pTrustBasedApplic) {
        trustBasedApplic = pTrustBasedApplic;
    }

    /**
     * Gets the taxRegime
     *
     * @return the taxRegime
     */
    public String getTaxRegimeLit() {
        return taxRegimeLit;
    }

    /**
     * Sets the taxRegime
     *
     * @param pTaxRegimeLit
     *            the taxRegime to set
     */
    public void setTaxRegimeLit(final String pTaxRegimeLit) {
        taxRegimeLit = pTaxRegimeLit;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
