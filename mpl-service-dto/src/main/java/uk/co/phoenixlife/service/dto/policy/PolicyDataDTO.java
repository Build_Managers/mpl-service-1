package uk.co.phoenixlife.service.dto.policy;

import java.util.ArrayList;

import uk.co.phoenixlife.service.dto.dashboard.PoliciesOwnedByAPartyDTO;

/**
 * PolicyDataDTO.java
 */
public class PolicyDataDTO {

    private String policyNumber;
    private String emailId;
    private PartyBasicDTO partyBasicDTO;
    private PoliciesOwnedByAPartyDTO policiesOwnedByAParty;
    private ArrayList<Long> customerIdList;
    private String primaryContactNum;
    private String secContactNum;
    private String additionalContactNum;
    private String contactType1;
    private String contactType2;
    private String contactType3;

    /**
     * Method to getPolicyNumber
     *
     * @return the policyNumber
     */
    public String getPolicyNumber() {
        return policyNumber;
    }

    /**
     * Method to setPolicyNumber
     *
     * @param pPolicyNumber
     *            the policyNumber to set
     */
    public void setPolicyNumber(final String pPolicyNumber) {
        this.policyNumber = pPolicyNumber;
    }

    /**
     * Method to getEmailId
     *
     * @return the emailId
     */
    public String getEmailId() {
        return emailId;
    }

    /**
     * Method to setEmailId
     *
     * @param pemailId
     *            the emailId to set
     */
    public void setEmailId(final String pemailId) {
        this.emailId = pemailId;
    }

    /**
     * Method to getPartyBasicDTO
     *
     * @return the partyBasicDTO
     */
    public PartyBasicDTO getPartyBasicDTO() {
        return partyBasicDTO;
    }

    /**
     * Method to setPartyBasicDTO
     *
     * @param pPartyBasicDTO
     *            the partyBasicDTO to set
     */
    public void setPartyBasicDTO(final PartyBasicDTO pPartyBasicDTO) {
        this.partyBasicDTO = pPartyBasicDTO;
    }

    /**
     * Method to getPoliciesOwnedByAParty
     *
     * @return the getPoliciesOwnedByAParty
     */
    public PoliciesOwnedByAPartyDTO getPoliciesOwnedByAParty() {
        return policiesOwnedByAParty;
    }

    /**
     * Method to setPoliciesOwnedByAPartyDTO
     *
     * @param pPoliciesOwnedByAParty
     *            the policiesOwnedByAParty to set
     */
    public void setPoliciesOwnedByAParty(final PoliciesOwnedByAPartyDTO pPoliciesOwnedByAParty) {
        this.policiesOwnedByAParty = pPoliciesOwnedByAParty;
    }

    /**
     * Method to getCustIdList
     *
     * @return the customerIdList
     */
    public ArrayList<Long> getCustomerIdList() {
        return customerIdList;
    }

    /**
     * Method to setCustIdList
     *
     * @param pcustomerIdList
     *            the customerIdList to set
     */
    public void setCustomerIdList(final ArrayList<Long> pcustomerIdList) {
        this.customerIdList = pcustomerIdList;
    }

    /**
     * Method to getPrimaryContactNum
     *
     * @return the primaryContactNum
     */
    public String getPrimaryContactNum() {
        return primaryContactNum;
    }

    /**
     * Method to setPrimaryContactNum
     *
     * @param pprimaryContactNum
     *            the primaryContactNum to set
     */
    public void setPrimaryContactNum(final String pprimaryContactNum) {
        this.primaryContactNum = pprimaryContactNum;
    }

    /**
     * Method to getSecContactNum
     *
     * @return the secContactNum
     */
    public String getSecContactNum() {
        return secContactNum;
    }

    /**
     * Method to setSecContactNum
     *
     * @param psecContactNum
     *            the secContactNum to set
     */
    public void setSecContactNum(final String psecContactNum) {
        this.secContactNum = psecContactNum;
    }

    /**
     * Method to getAdditionalContactNum
     *
     * @return the additionalContactNum
     */
    public String getAdditionalContactNum() {
        return additionalContactNum;
    }

    /**
     * Method to setAdditionalContactNum
     *
     * @param padditionalContactNum
     *            the additionalContactNum to set
     */
    public void setAdditionalContactNum(final String padditionalContactNum) {
        this.additionalContactNum = padditionalContactNum;
    }

    /**
     * Method to getContactType1
     *
     * @return the contactType1
     */
    public String getContactType1() {
        return contactType1;
    }

    /**
     * Method to setContactType1
     *
     * @param pcontactType1
     *            the contactType1 to set
     */
    public void setContactType1(final String pcontactType1) {
        this.contactType1 = pcontactType1;
    }

    /**
     * Method to getcontacttype2
     *
     * @return the contactType2
     */
    public String getContactType2() {
        return contactType2;
    }

    /**
     * Method to setContactType2
     *
     * @param pcontactType2
     *            the contactType2 to set
     */
    public void setContactType2(final String pcontactType2) {
        this.contactType2 = pcontactType2;
    }

    /**
     * Method to getContactType3
     *
     * @return the contactType3
     */
    public String getContactType3() {
        return contactType3;
    }

    /**
     * Method to setContactType3
     *
     * @param pcontactType3
     *            the contactType3 to set
     */
    public void setContactType3(final String pcontactType3) {
        this.contactType3 = pcontactType3;
    }

}
