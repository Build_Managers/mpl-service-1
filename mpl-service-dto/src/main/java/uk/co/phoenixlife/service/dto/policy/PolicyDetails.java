/**
 *
 */
package uk.co.phoenixlife.service.dto.policy;

/**
 * PolicyDetails
 *
 * @author 01238762
 *
 */

public class PolicyDetails {

    private String policyRef;
    private String policyBakUniqueNumber;
    private long estimatedVal;
    private Boolean garanteedBenifit;
    private Boolean marketValReduction;
    private Boolean earlyExitCharge;
    private Boolean isEligible;

    /**
     * Getter
     *
     * @return policyRef
     */
    public String getPolicyRef() {
        return policyRef;
    }

    /**
     * Setter
     *
     * @param pPolicyRef
     *            policyRef
     */
    public void setPolicyRef(final String pPolicyRef) {
        this.policyRef = pPolicyRef;
    }

    /**
     * Getter
     *
     * @return policyBakUniqueNumber
     */
    public String getPolicyBakUniqueNumber() {
        return policyBakUniqueNumber;
    }

    /**
     * Setter
     *
     * @param pPolicyBakUniqueNumber
     *            policyBakUniqueNumber
     */
    public void setPolicyBakUniqueNumber(final String pPolicyBakUniqueNumber) {
        this.policyBakUniqueNumber = pPolicyBakUniqueNumber;
    }

    /**
     * Gets the estimatedVal
     *
     * @return the estimatedVal
     */
    public long getEstimatedVal() {
        return estimatedVal;
    }

    /**
     * Sets the estimatedVal
     *
     * @param pestimatedVal
     *            the estimatedVal to set
     */
    public void setEstimatedVal(final long pestimatedVal) {
        this.estimatedVal = pestimatedVal;
    }

    /**
     * Gets the garanteedBenifit
     *
     * @return the garanteedBenifit
     */
    public Boolean getGaranteedBenifit() {
        return garanteedBenifit;
    }

    /**
     * Sets the garanteedBenifit
     *
     * @param pgaranteedBenifit
     *            the garanteedBenifit to set
     */
    public void setGaranteedBenifit(final Boolean pgaranteedBenifit) {
        this.garanteedBenifit = pgaranteedBenifit;
    }

    /**
     * Gets the marketValReduction
     *
     * @return the marketValReduction
     */
    public Boolean getMarketValReduction() {
        return marketValReduction;
    }

    /**
     * Sets the marketValReduction
     *
     * @param pmarketValReduction
     *            the marketValReduction to set
     */
    public void setMarketValReduction(final Boolean pmarketValReduction) {
        this.marketValReduction = pmarketValReduction;
    }

    /**
     * Gets the earlyExitCharge
     *
     * @return the earlyExitCharge
     */
    public Boolean getEarlyExitCharge() {
        return earlyExitCharge;
    }

    /**
     * Sets the earlyExitCharge
     *
     * @param pearlyExitCharge
     *            the earlyExitCharge to set
     */
    public void setEarlyExitCharge(final Boolean pearlyExitCharge) {
        this.earlyExitCharge = pearlyExitCharge;
    }

    /**
     * Gets the isEligible
     *
     * @return the isEligible
     */
    public Boolean getIsEligible() {
        return isEligible;
    }

    /**
     * Sets the isEligible
     *
     * @param pisEligible
     *            the isEligible to set
     */
    public void setIsEligible(final Boolean pisEligible) {
        this.isEligible = pisEligible;
    }

}
