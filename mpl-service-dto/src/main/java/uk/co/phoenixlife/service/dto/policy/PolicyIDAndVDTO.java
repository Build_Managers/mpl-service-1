/*
 * PolicyIDAndVDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * PolicyIDAndVDTO.java
 */
public class PolicyIDAndVDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private List<PartyPolicyDTO> partyPolicy;
    private String polnum;

    /**
     * Gets the List of PartyPolicy
     *
     * @return partyPolicy
     */
    public List<PartyPolicyDTO> getPartyPolicy() {
        return partyPolicy;
    }

    /**
     * Sets the List of PartyPolicy
     *
     * @param partyPolicyParam
     *            Variable
     *
     */
    public void setPartyPolicy(final List<PartyPolicyDTO> partyPolicyParam) {
        partyPolicy = partyPolicyParam;
    }

    /**
     * Gets the PolicyNumber
     *
     * @return polnum
     */
    public String getPolnum() {
        return polnum;
    }

    /**
     * Sets the PolicyNumber
     *
     * @param polnumParam
     *            variable
     */
    public void setPolnum(final String polnumParam) {
        polnum = polnumParam;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
