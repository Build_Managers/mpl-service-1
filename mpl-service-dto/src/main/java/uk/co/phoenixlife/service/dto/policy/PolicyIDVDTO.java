/*
 * PolicyIDVDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * PolicyIDVDTO.java
 */
public class PolicyIDVDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String polNum;
    private String bancsPolNum;
    private String bancsUniqueCustNo;
    private String polStatus;
    private LocalDate polStDate;
    private String premFreq;
    private String paymentMethod;
    private String benefitName;
    private Double netInitPrem;
    private Double netModPrem;
    private Double lastPremPaid;
    private LocalDate premCeaseDate;
    private Integer sortCode;
    private Integer bankAcct;
    private Integer premiumDueDate;
    private String niNo;
    private String mLStatus;
    private List<String> fundName;

    /**
     * Gets the polNum
     *
     * @return the polNum
     */
    public String getPolNum() {
        return polNum;
    }

    /**
     * Sets the polNum
     *
     * @param pPolNum
     *            the polNum to set
     */
    public void setPolNum(final String pPolNum) {
        polNum = pPolNum;
    }

    /**
     * Gets the bancsPolNum
     *
     * @return the bancsPolNum
     */
    public String getBancsPolNum() {
        return bancsPolNum;
    }

    /**
     * Sets the bancsPolNum
     *
     * @param pBancsPolNum
     *            the bancsPolNum to set
     */
    public void setBancsPolNum(final String pBancsPolNum) {
        bancsPolNum = pBancsPolNum;
    }

    /**
     * Gets the bancsUniqueCustNo
     *
     * @return the bancsUniqueCustNo
     */
    public String getBancsUniqueCustNo() {
        return bancsUniqueCustNo;
    }

    /**
     * Sets the bancsUniqueCustNo
     *
     * @param pBancsUniqueCustNo
     *            the bancsUniqueCustNo to set
     */
    public void setBancsUniqueCustNo(final String pBancsUniqueCustNo) {
        bancsUniqueCustNo = pBancsUniqueCustNo;
    }

    /**
     * Gets the polStatus
     *
     * @return the polStatus
     */
    public String getPolStatus() {
        return polStatus;
    }

    /**
     * Sets the polStatus
     *
     * @param pPolStatus
     *            the polStatus to set
     */
    public void setPolStatus(final String pPolStatus) {
        polStatus = pPolStatus;
    }

    /**
     * Gets the polStDate
     *
     * @return the polStDate
     */
    public LocalDate getPolStDate() {
        return polStDate;
    }

    /**
     * Sets the polStDate
     *
     * @param pPolStDate
     *            the polStDate to set
     */
    public void setPolStDate(final LocalDate pPolStDate) {
        polStDate = pPolStDate;
    }

    /**
     * Gets the premFreq
     *
     * @return the premFreq
     */
    public String getPremFreq() {
        return premFreq;
    }

    /**
     * Sets the premFreq
     *
     * @param pPremFreq
     *            the premFreq to set
     */
    public void setPremFreq(final String pPremFreq) {
        premFreq = pPremFreq;
    }

    /**
     * Gets the paymentMethod
     *
     * @return the paymentMethod
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets the paymentMethod
     *
     * @param pPaymentMethod
     *            the paymentMethod to set
     */
    public void setPaymentMethod(final String pPaymentMethod) {
        paymentMethod = pPaymentMethod;
    }

    /**
     * Gets the benefitName
     *
     * @return the benefitName
     */
    public String getBenefitName() {
        return benefitName;
    }

    /**
     * Sets the benefitName
     *
     * @param pBenefitName
     *            the benefitName to set
     */
    public void setBenefitName(final String pBenefitName) {
        benefitName = pBenefitName;
    }

    /**
     * Gets the netInitPrem
     *
     * @return the netInitPrem
     */
    public Double getNetInitPrem() {
        return netInitPrem;
    }

    /**
     * Sets the netInitPrem
     *
     * @param pNetInitPrem
     *            the netInitPrem to set
     */
    public void setNetInitPrem(final Double pNetInitPrem) {
        netInitPrem = pNetInitPrem;
    }

    /**
     * Gets the netModPrem
     *
     * @return the netModPrem
     */
    public Double getNetModPrem() {
        return netModPrem;
    }

    /**
     * Sets the netModPrem
     *
     * @param pNetModPrem
     *            the netModPrem to set
     */
    public void setNetModPrem(final Double pNetModPrem) {
        netModPrem = pNetModPrem;
    }

    /**
     * Gets the lastPremPaid
     *
     * @return the lastPremPaid
     */
    public Double getLastPremPaid() {
        return lastPremPaid;
    }

    /**
     * Sets the lastPremPaid
     *
     * @param pLastPremPaid
     *            the lastPremPaid to set
     */
    public void setLastPremPaid(final Double pLastPremPaid) {
        lastPremPaid = pLastPremPaid;
    }

    /**
     * Gets the premCeaseDate
     *
     * @return the premCeaseDate
     */
    public LocalDate getPremCeaseDate() {
        return premCeaseDate;
    }

    /**
     * Sets the premCeaseDate
     *
     * @param pPremCeaseDate
     *            the premCeaseDate to set
     */
    public void setPremCeaseDate(final LocalDate pPremCeaseDate) {
        premCeaseDate = pPremCeaseDate;
    }

    /**
     * Gets the sortCode
     *
     * @return the sortCode
     */
    public Integer getSortCode() {
        return sortCode;
    }

    /**
     * Sets the sortCode
     *
     * @param pSortCode
     *            the sortCode to set
     */
    public void setSortCode(final Integer pSortCode) {
        sortCode = pSortCode;
    }

    /**
     * Gets the bankAcct
     *
     * @return the bankAcct
     */
    public Integer getBankAcct() {
        return bankAcct;
    }

    /**
     * Sets the bankAcct
     *
     * @param pBankAcct
     *            the bankAcct to set
     */
    public void setBankAcct(final Integer pBankAcct) {
        bankAcct = pBankAcct;
    }

    /**
     * Gets the premiumDueDate
     *
     * @return the premiumDueDate
     */
    public Integer getPremiumDueDate() {
        return premiumDueDate;
    }

    /**
     * Sets the premiumDueDate
     *
     * @param pPremiumDueDate
     *            the premiumDueDate to set
     */
    public void setPremiumDueDate(final Integer pPremiumDueDate) {
        premiumDueDate = pPremiumDueDate;
    }

    /**
     * Gets the niNo
     *
     * @return the niNo
     */
    public String getNiNo() {
        return niNo;
    }

    /**
     * Sets the niNo
     *
     * @param pNiNo
     *            the niNo to set
     */
    public void setNiNo(final String pNiNo) {
        niNo = pNiNo;
    }

    /**
     * Gets the mLStatus
     *
     * @return the mLStatus
     */
    public String getmLStatus() {
        return mLStatus;
    }

    /**
     * Sets the mLStatus
     *
     * @param pMLStatus
     *            the mLStatus to set
     */
    public void setmLStatus(final String pMLStatus) {
        mLStatus = pMLStatus;
    }

    /**
     * Gets the fundName
     *
     * @return the fundName
     */
    public List<String> getFundName() {
        return fundName;
    }

    /**
     * Sets the fundName
     *
     * @param pFundName
     *            the fundName to set
     */
    public void setFundName(final List<String> pFundName) {
        fundName = pFundName;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
