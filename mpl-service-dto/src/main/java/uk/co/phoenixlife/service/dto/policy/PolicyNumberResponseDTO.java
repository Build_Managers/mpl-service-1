/*
 * PolicyNumberResponseDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * PolicyNumberResponseDTO.java
 */
public class PolicyNumberResponseDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String policyStatus;
    private String policyNumber;
    private String errMsg;

    /**
     * Gets the errMsg
     *
     * @return the errMsg
     */
    public String getErrMsg() {
        return errMsg;
    }

    /**
     * Sets the errMsg
     *
     * @param perrMsg
     *            the errMsg to set
     */
    public void setErrMsg(final String perrMsg) {
        this.errMsg = perrMsg;
    }

    /**
     * Gets the policyStatus
     *
     * @return the policyStatus
     */
    public String getPolicyStatus() {
        return policyStatus;
    }

    /**
     * Sets the policyStatus
     *
     * @param pPolicyStatus
     *            the policyStatus to set
     */
    public void setPolicyStatus(final String pPolicyStatus) {
        policyStatus = pPolicyStatus;
    }

    /**
     * Gets the policyNumber
     *
     * @return the policyNumber
     */
    public String getPolicyNumber() {
        return policyNumber;
    }

    /**
     * Sets the policyNumber
     *
     * @param ppolicyNumber
     *            the policyNumber to set
     */
    public void setPolicyNumber(final String ppolicyNumber) {
        this.policyNumber = ppolicyNumber;
    }

    /**
     * Gets the serialversionuid
     *
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
