/*
 * PolicyOwnedByPartyDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * PolicyOwnedByPartyDTO.java
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "bancsUniqueCustNo", "country", "title", "forename", "surname", "dateOfBirth", "partyProt", "niNo",
        "houseNum", "houseName", "address1", "address2", "cityTown", "county", "postCode", "mobPhoneNum",
        "listOfPolicies", "busTimeStamp" })
public class PolicyOwnedByPartyDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    @JsonProperty("bancsUniqueCustNo")
    private String bancsUniqueCustNo;
    @JsonProperty("country")
    private String country;
    @JsonProperty("title")
    private String title;
    @JsonProperty("forename")
    private String forename;
    @JsonProperty("surname")
    private String surname;
    @JsonProperty("dateOfBirth")
    private String dateOfBirth;
    @JsonProperty("partyProt")
    private String partyProt;
    @JsonProperty("niNo")
    private String niNo;
    @JsonProperty("houseNum")
    private String houseNum;
    @JsonProperty("houseName")
    private String houseName;
    @JsonProperty("address1")
    private String address1;
    @JsonProperty("address2")
    private String address2;
    @JsonProperty("cityTown")
    private String cityTown;
    @JsonProperty("county")
    private String county;
    @JsonProperty("postCode")
    private String postCode;
    @JsonProperty("mobPhoneNum")
    private String mobPhoneNum;
    @JsonProperty("busTimeStamp")
    private String busTimeStamp;

    /**
     * Gets the bancsUniqueCustNo
     *
     * @return the bancsUniqueCustNo
     */
    public String getBancsUniqueCustNo() {
        return bancsUniqueCustNo;
    }

    /**
     * Sets the bancsUniqueCustNo
     *
     * @param pBancsUniqueCustNo
     *            the bancsUniqueCustNo to set
     */
    public void setBancsUniqueCustNo(final String pBancsUniqueCustNo) {
        bancsUniqueCustNo = pBancsUniqueCustNo;
    }

    /**
     * Gets the country
     *
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the country
     *
     * @param pCountry
     *            the country to set
     */
    public void setCountry(final String pCountry) {
        country = pCountry;
    }

    /**
     * Gets the title
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title
     *
     * @param pTitle
     *            the title to set
     */
    public void setTitle(final String pTitle) {
        title = pTitle;
    }

    /**
     * Gets the forename
     *
     * @return the forename
     */
    public String getForename() {
        return forename;
    }

    /**
     * Sets the forename
     *
     * @param pForename
     *            the forename to set
     */
    public void setForename(final String pForename) {
        forename = pForename;
    }

    /**
     * Gets the surname
     *
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets the surname
     *
     * @param pSurname
     *            the surname to set
     */
    public void setSurname(final String pSurname) {
        surname = pSurname;
    }

    /**
     * Gets the dateOfBirth
     *
     * @return the dateOfBirth
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the dateOfBirth
     *
     * @param pDateOfBirth
     *            the dateOfBirth to set
     */
    public void setDateOfBirth(final String pDateOfBirth) {
        dateOfBirth = pDateOfBirth;
    }

    /**
     * Gets the partyProt
     *
     * @return the partyProt
     */
    public String getPartyProt() {
        return partyProt;
    }

    /**
     * Sets the partyProt
     *
     * @param pPartyProt
     *            the partyProt to set
     */
    public void setPartyProt(final String pPartyProt) {
        partyProt = pPartyProt;
    }

    /**
     * Gets the niNo
     *
     * @return the niNo
     */
    public String getNiNo() {
        return niNo;
    }

    /**
     * Sets the niNo
     *
     * @param pNiNo
     *            the niNo to set
     */
    public void setNiNo(final String pNiNo) {
        niNo = pNiNo;
    }

    /**
     * Gets the houseNum
     *
     * @return the houseNum
     */
    public String getHouseNum() {
        return houseNum;
    }

    /**
     * Sets the houseNum
     *
     * @param pHouseNum
     *            the houseNum to set
     */
    public void setHouseNum(final String pHouseNum) {
        houseNum = pHouseNum;
    }

    /**
     * Gets the houseName
     *
     * @return the houseName
     */
    public String getHouseName() {
        return houseName;
    }

    /**
     * Sets the houseName
     *
     * @param phouseName
     *            the houseName to set
     */
    public void setHouseName(final String phouseName) {
        houseName = phouseName;
    }

    /**
     * Gets the address1
     *
     * @return the address1
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * Sets the address1
     *
     * @param pAddress1
     *            the address1 to set
     */
    public void setAddress1(final String pAddress1) {
        address1 = pAddress1;
    }

    /**
     * Gets the address2
     *
     * @return the address2
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * Sets the address2
     *
     * @param pAddress2
     *            the address2 to set
     */
    public void setAddress2(final String pAddress2) {
        address2 = pAddress2;
    }

    /**
     * Gets the cityTown
     *
     * @return the cityTown
     */
    public String getCityTown() {
        return cityTown;
    }

    /**
     * Sets the cityTown
     *
     * @param pCityTown
     *            the cityTown to set
     */
    public void setCityTown(final String pCityTown) {
        cityTown = pCityTown;
    }

    /**
     * Gets the county
     *
     * @return the county
     */
    public String getCounty() {
        return county;
    }

    /**
     * Sets the county
     *
     * @param pCounty
     *            the county to set
     */
    public void setCounty(final String pCounty) {
        county = pCounty;
    }

    /**
     * Gets the postCode
     *
     * @return the postCode
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * Sets the postCode
     *
     * @param pPostCode
     *            the postCode to set
     */
    public void setPostCode(final String pPostCode) {
        postCode = pPostCode;
    }

    /**
     * Gets the mobPhoneNum
     *
     * @return the mobPhoneNum
     */
    public String getMobPhoneNum() {
        return mobPhoneNum;
    }

    /**
     * Sets the mobPhoneNum
     *
     * @param pMobPhoneNum
     *            the mobPhoneNum to set
     */
    public void setMobPhoneNum(final String pMobPhoneNum) {
        mobPhoneNum = pMobPhoneNum;
    }

    /**
     * Gets the busTimeStamp
     *
     * @return the busTimeStamp
     */
    public String getBusTimeStamp() {
        return busTimeStamp;
    }

    /**
     * Sets the busTimeStamp
     *
     * @param pBusTimeStamp
     *            the busTimeStamp to set
     */
    public void setBusTimeStamp(final String pBusTimeStamp) {
        busTimeStamp = pBusTimeStamp;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
