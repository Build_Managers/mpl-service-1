/*
 * PolicyRequestDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;

import uk.co.phoenixlife.service.dto.pdf.FormRequestDTO;

/**
 * PolicyRequestDTO.java
 */
public class PolicyRequestDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private long policyId;
    private String policyNumber;
    private String questionKey;
    private int identifySource;
    private long customerId;
    private String nino;
    private FormRequestDTO formData;
    private QuoteDetails quoteDetails;
    private int elligiblePolicyCount;
    private String xGUID;

    /**
     * Gets the policyId
     *
     * @return the policyId
     */
    public long getPolicyId() {
        return policyId;
    }

    /**
     * Sets the policyId
     *
     * @param pPolicyId
     *            the policyId to set
     */
    public void setPolicyId(final long pPolicyId) {
        policyId = pPolicyId;
    }

    /**
     * Gets the policyNumber
     *
     * @return the policyNumber
     */
    public String getPolicyNumber() {
        return policyNumber;
    }

    /**
     * Sets the policyNumber
     *
     * @param pPolicyNumber
     *            the policyNumber to set
     */
    public void setPolicyNumber(final String pPolicyNumber) {
        policyNumber = pPolicyNumber;
    }

    /**
     * Gets the questionKey
     *
     * @return the questionKey
     */
    public String getQuestionKey() {
        return questionKey;
    }

    /**
     * Sets the questionKey
     *
     * @param pQuestionKey
     *            the questionKey to set
     */
    public void setQuestionKey(final String pQuestionKey) {
        questionKey = pQuestionKey;
    }

    /**
     * Gets the identifySource
     *
     * @return the identifySource
     */
    public int getIdentifySource() {
        return identifySource;
    }

    /**
     * Sets the identifySource
     *
     * @param pIdentifySource
     *            the identifySource to set
     */
    public void setIdentifySource(final int pIdentifySource) {
        identifySource = pIdentifySource;
    }

    /**
     * Gets the customerId
     *
     * @return the customerId
     */
    public long getCustomerId() {
        return customerId;
    }

    /**
     * Sets the customerId
     *
     * @param pCustomerId
     *            the customerId to set
     */
    public void setCustomerId(final long pCustomerId) {
        customerId = pCustomerId;
    }

    /**
     * Gets the nino
     *
     * @return the nino
     */
    public String getNino() {
        return nino;
    }

    /**
     * Sets the nino
     *
     * @param pNino
     *            the nino to set
     */
    public void setNino(final String pNino) {
        nino = pNino;
    }

    /**
     * Gets the formData
     *
     * @return the formData
     */
    public FormRequestDTO getFormData() {
        return formData;
    }

    /**
     * Sets the formData
     *
     * @param pFormData
     *            the formData to set
     */
    public void setFormData(final FormRequestDTO pFormData) {
        formData = pFormData;
    }

    /**
     * Gets the quoteDetails
     *
     * @return the quoteDetails
     */
    public QuoteDetails getQuoteDetails() {
        return quoteDetails;
    }

    /**
     * Sets the quoteDetails
     *
     * @param pQuoteDetails
     *            the quoteDetails to set
     */
    public void setQuoteDetails(final QuoteDetails pQuoteDetails) {
        quoteDetails = pQuoteDetails;
    }

    /**
     * Gets elligiblePolicyCount
     *
     * @return the elligiblePolicyCount
     */
    public int getElligiblePolicyCount() {
        return elligiblePolicyCount;
    }

    /**
     * Sets elligiblePolicyCount
     *
     * @param mElligiblePolicyCount
     *            the elligiblePolicyCount to set
     */
    public void setEligiblePolicyCount(final int mElligiblePolicyCount) {
        elligiblePolicyCount = mElligiblePolicyCount;
    }

    /**
     * Gets the xGUID
     *
     * @return the xGUID
     */
    public String getxGUID() {
        return xGUID;
    }

    /**
     * Sets the xGUID
     *
     * @param pXGUID
     *            the xGUID to set
     */
    public void setxGUID(final String pXGUID) {
        xGUID = pXGUID;
    }

}
