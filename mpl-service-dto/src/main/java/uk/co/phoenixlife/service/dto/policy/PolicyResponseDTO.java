/*
 * PolicyResponseDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;
import java.util.List;

/**
 * PolicyResponseDTO.java
 */
public class PolicyResponseDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private boolean isPolicyLocked;
    private List<Long> customerIdList;
    private List<String> lockedQuestionKeyList;

    /**
     * Gets the isPolicyLocked
     *
     * @return the isPolicyLocked
     */
    public boolean isPolicyLocked() {
        return isPolicyLocked;
    }

    /**
     * Sets the isPolicyLocked
     *
     * @param pIsPolicyLocked
     *            the isPolicyLocked to set
     */
    public void setPolicyLocked(final boolean pIsPolicyLocked) {
        isPolicyLocked = pIsPolicyLocked;
    }

    /**
     * Gets the customerIdList
     *
     * @return the customerIdList
     */
    public List<Long> getCustomerIdList() {
        return customerIdList;
    }

    /**
     * Sets the customerIdList
     *
     * @param pCustomerIdList
     *            the customerIdList to set
     */
    public void setCustomerIdList(final List<Long> pCustomerIdList) {
        customerIdList = pCustomerIdList;
    }

    /**
     * Gets the lockedQuestionKeyList
     *
     * @return the lockedQuestionKeyList
     */
    public List<String> getLockedQuestionKeyList() {
        return lockedQuestionKeyList;
    }

    /**
     * Sets the lockedQuestionKeyList
     *
     * @param pLockedQuestionKeyList
     *            the lockedQuestionKeyList to set
     */
    public void setLockedQuestionKeyList(final List<String> pLockedQuestionKeyList) {
        lockedQuestionKeyList = pLockedQuestionKeyList;
    }

}
