/*
 * PolicyRoleDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * PolicyRoleDTO.java
 */
public class PolicyRoleDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String polRoleCd;
    private String polRoleLit;

    private String bancsUniqueCustNo;
    private String legacyCustNo;

    /**
     * Method to getPolRoleCd
     *
     * @return the polRoleCd
     */
    public String getPolRoleCd() {
        return polRoleCd;
    }

    /**
     * Method to setPolRoleCd
     *
     * @param pPolRoleCd
     *            the polRoleCd to set
     */
    public void setPolRoleCd(final String pPolRoleCd) {
        this.polRoleCd = pPolRoleCd;
    }

    /**
     * Method to getPolRoleLit
     *
     * @return the polRoleLit
     */
    public String getPolRoleLit() {
        return polRoleLit;
    }

    /**
     * Method to setPolRoleLit
     *
     * @param pPolRoleLit
     *            the polRoleLit to set
     */
    public void setPolRoleLit(final String pPolRoleLit) {
        this.polRoleLit = pPolRoleLit;
    }

    /**
     * Gets the bancsUniqueCustNo
     *
     * @return the bancsUniqueCustNo
     */
    public String getBancsUniqueCustNo() {
        return bancsUniqueCustNo;
    }

    /**
     * Sets the bancsUniqueCustNo
     *
     * @param pBancsUniqueCustNo
     *            the bancsUniqueCustNo to set
     */
    public void setBancsUniqueCustNo(final String pBancsUniqueCustNo) {
        bancsUniqueCustNo = pBancsUniqueCustNo;
    }

    /**
     * Gets the legacyCustNo
     *
     * @return the legacyCustNo
     */
    public String getLegacyCustNo() {
        return legacyCustNo;
    }

    /**
     * Sets the legacyCustNo
     *
     * @param pLegacyCustNo
     *            the legacyCustNo to set
     */
    public void setLegacyCustNo(final String pLegacyCustNo) {
        legacyCustNo = pLegacyCustNo;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
