/*
 * QuestionsDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;

/**
 * QuestionsDTO.java
 */
public class QuestionsDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    private String queId;
    private boolean result;

    /**
     * Gets the queId
     *
     * @return the queId
     */
    public String getQueId() {
        return queId;
    }

    /**
     * Sets the queId
     *
     * @param pqueId
     *            the queId to set
     */
    public void setQueId(final String pqueId) {
        this.queId = pqueId;
    }

    /**
     * Gets the result
     *
     * @return the result
     */
    public boolean isResult() {
        return result;
    }

    /**
     * Sets the result
     *
     * @param presult
     *            the result to set
     */
    public void setResult(final boolean presult) {
        this.result = presult;
    }

}
