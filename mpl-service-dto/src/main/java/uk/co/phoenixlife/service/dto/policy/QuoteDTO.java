/*
 * QuoteDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * QuoteDTO.java
 */
public class QuoteDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String qteRef;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private Date qteDate;
    private String qteTypeLit;
    private String qteTypeCd;
    private String qteStatusLit;
    private String qteStatusCd;

    /**
     * Gets the qteTypeLit
     *
     * @return the qteTypeLit
     */
    public String getQteTypeLit() {
        return qteTypeLit;
    }

    /**
     * Sets the qteTypeLit
     *
     * @param pqteTypeLit
     *            the qteTypeLit to set
     */
    public void setQteTypeLit(final String pqteTypeLit) {
        this.qteTypeLit = pqteTypeLit;
    }

    /**
     * Gets the qteTypeCd
     *
     * @return the qteTypeCd
     */
    public String getQteTypeCd() {
        return qteTypeCd;
    }

    /**
     * Sets the qteTypeCd
     *
     * @param pqteTypeCd
     *            the qteTypeCd to set
     */
    public void setQteTypeCd(final String pqteTypeCd) {
        this.qteTypeCd = pqteTypeCd;
    }

    /**
     * Gets the qteStatusLit
     *
     * @return the qteStatusLit
     */
    public String getQteStatusLit() {
        return qteStatusLit;
    }

    /**
     * Sets the qteStatusLit
     *
     * @param pqteStatusLit
     *            the qteStatusLit to set
     */
    public void setQteStatusLit(final String pqteStatusLit) {
        this.qteStatusLit = pqteStatusLit;
    }

    /**
     * Gets the qteStatusCd
     *
     * @return the qteStatusCd
     */
    public String getQteStatusCd() {
        return qteStatusCd;
    }

    /**
     * Sets the qteStatusCd
     *
     * @param pqteStatusCd
     *            the qteStatusCd to set
     */
    public void setQteStatusCd(final String pqteStatusCd) {
        this.qteStatusCd = pqteStatusCd;
    }

    /**
     * gets the qtedate
     *
     * @return the qteDate
     */
    public Date getQteDate() {
        return qteDate;
    }

    /**
     * sets the qtedate
     *
     * @param pqteDate
     *            the qteDate to set
     */
    public void setQteDate(final Date pqteDate) {
        this.qteDate = pqteDate;
    }

    /**
     * Gets the qteRef
     *
     * @return the qteRef
     */
    public String getQteRef() {
        return qteRef;
    }

    /**
     * Sets the qteRef
     *
     * @param pQteRef
     *            the qteRef to set
     */
    public void setQteRef(final String pQteRef) {
        qteRef = pQteRef;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
