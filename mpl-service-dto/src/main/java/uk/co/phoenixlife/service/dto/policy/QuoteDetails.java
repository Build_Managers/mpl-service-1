/*
 * QuoteDetails.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.policy;

import java.io.Serializable;
import java.sql.Timestamp;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * QuoteDetails.java
 */
public class QuoteDetails implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String paymentMethod;
    private String sortCode;
    private String bankAcctNo;
    private String rollNo;
    private Boolean smallPotFlag;
    private Boolean contactCustomerFlag;
    private Timestamp sentTimestamp;
    private Timestamp ackTimestamp;
    private String errorDesc;
    private int quoteAPIResp;
    private String encashRefNo;

    /**
     * Gets the encashRefNo
     * 
     * @return the encashRefNo
     */
    public String getEncashRefNo() {
        return encashRefNo;
    }

    /**
     * Sets the encashRefNo
     * 
     * @param encashRefNo
     *            the encashRefNo to set
     */
    public void setEncashRefNo(final String encashRefNo) {
        this.encashRefNo = encashRefNo;
    }

    /**
     * Gets the quoteAPIResp
     *
     * @return the quoteAPIResp
     */
    public int getQuoteAPIResp() {
        return quoteAPIResp;
    }

    /**
     * Sets the quoteAPIResp
     *
     * @param quoteAPIResp
     *            the quoteAPIResp to set
     */
    public void setQuoteAPIResp(final int quoteAPIResp) {
        this.quoteAPIResp = quoteAPIResp;
    }

    /**
     * Gets the sentTimestamp
     *
     * @return the sentTimestamp
     */
    public Timestamp getSentTimestamp() {
        return sentTimestamp;
    }

    /**
     * Sets the sentTimestamp
     *
     * @param sentTimestamp
     *            the sentTimestamp to set
     */
    public void setSentTimestamp(final Timestamp sentTimestamp) {
        this.sentTimestamp = sentTimestamp;
    }

    /**
     * Gets the ackTimestamp
     *
     * @return the ackTimestamp
     */
    public Timestamp getAckTimestamp() {
        return ackTimestamp;
    }

    /**
     * Sets the ackTimestamp
     *
     * @param ackTimestamp
     *            the ackTimestamp to set
     */
    public void setAckTimestamp(final Timestamp ackTimestamp) {
        this.ackTimestamp = ackTimestamp;
    }

    /**
     * Gets the errorDesc
     *
     * @return the errorDesc
     */
    public String getErrorDesc() {
        return errorDesc;
    }

    /**
     * Sets the errorDesc
     *
     * @param errorDesc
     *            the errorDesc to set
     */
    public void setErrorDesc(final String errorDesc) {
        this.errorDesc = errorDesc;
    }

    /**
     * Gets the paymentMethod
     *
     * @return the paymentMethod
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets the paymentMethod
     *
     * @param pPaymentMethod
     *            the paymentMethod to set
     */
    public void setPaymentMethod(final String pPaymentMethod) {
        paymentMethod = pPaymentMethod;
    }

    /**
     * Gets the sortCode
     *
     * @return the sortCode
     */
    public String getSortCode() {
        return sortCode;
    }

    /**
     * Sets the sortCode
     *
     * @param pSortCode
     *            the sortCode to set
     */
    public void setSortCode(final String pSortCode) {
        sortCode = pSortCode;
    }

    /**
     * Gets the bankAcctNo
     *
     * @return the bankAcctNo
     */
    public String getBankAcctNo() {
        return bankAcctNo;
    }

    /**
     * Sets the bankAcctNo
     *
     * @param pBankAcctNo
     *            the bankAcctNo to set
     */
    public void setBankAcctNo(final String pBankAcctNo) {
        bankAcctNo = pBankAcctNo;
    }

    /**
     * Gets the rollNo
     *
     * @return the rollNo
     */
    public String getRollNo() {
        return rollNo;
    }

    /**
     * Sets the rollNo
     *
     * @param pRollNo
     *            the rollNo to set
     */
    public void setRollNo(final String pRollNo) {
        rollNo = pRollNo;
    }

    /**
     * Gets the smallPotFlag
     *
     * @return the smallPotFlag
     */
    public Boolean getSmallPotFlag() {
        return smallPotFlag;
    }

    /**
     * Sets the smallPotFlag
     *
     * @param pSmallPotFlag
     *            the smallPotFlag to set
     */
    public void setSmallPotFlag(final Boolean pSmallPotFlag) {
        smallPotFlag = pSmallPotFlag;
    }

    /**
     * Gets the contactCustomerFlag
     *
     * @return the contactCustomerFlag
     */
    public Boolean getContactCustomerFlag() {
        return contactCustomerFlag;
    }

    /**
     * Sets the contactCustomerFlag
     *
     * @param pContactCustomerFlag
     *            the contactCustomerFlag to set
     */
    public void setContactCustomerFlag(final Boolean pContactCustomerFlag) {
        contactCustomerFlag = pContactCustomerFlag;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
