package uk.co.phoenixlife.service.dto.response;

import java.io.Serializable;

import uk.co.phoenixlife.service.dto.policy.BancsCashInTypeWrapper;

/**
 * BancsCashInTypeResponse.java
 */
public class BancsCashInTypeResponse implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private BancsCashInTypeWrapper bancsCalcCashInTax;

    /**
     * Gets the bancsCalcCashInTax
     *
     * @return the bancsCalcCashInTax
     */
    public BancsCashInTypeWrapper getBancsCalcCashInTax() {
        return bancsCalcCashInTax;
    }

    /**
     * Sets the bancsCalcCashInTax
     *
     * @param pBancsCalcCashInTax
     *            the bancsCalcCashInTax to set
     */
    public void setBancsCalcCashInTax(final BancsCashInTypeWrapper pBancsCalcCashInTax) {
        bancsCalcCashInTax = pBancsCalcCashInTax;
    }

}
