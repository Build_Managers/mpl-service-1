/*
 * BancsClaimListResponse.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.response;

import uk.co.phoenixlife.service.dto.policy.ClaimListDTO;

/**
 * BancsClaimListResponse.java
 */
public class BancsClaimListResponse {
    private ClaimListDTO bancsClaimList;

    /**
     * Gets the bancsClaimList
     *
     * @return the bancsClaimList
     */
    public ClaimListDTO getBancsClaimList() {
        return bancsClaimList;
    }

    /**
     * Sets the bancsClaimList
     *
     * @param pBancsClaimList
     *            the bancsClaimList to set
     */
    public void setBancsClaimList(final ClaimListDTO pBancsClaimList) {
        bancsClaimList = pBancsClaimList;
    }

}
