/*
 * BancsIDAndVdataResponse.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.response;

import uk.co.phoenixlife.service.dto.policy.IdAndVDataDTO;

/**
 * BancsIDAndVdataResponse.java
 */
public class BancsIDAndVDataResponse {
    private IdAndVDataDTO bancsIDAndVData;

    /**
     * Gets the bancsIDAndVdata
     *
     * @return the bancsIDAndVdata
     */
    public IdAndVDataDTO getBancsIDAndVData() {
        return bancsIDAndVData;
    }

    /**
     * Sets the bancsIDAndVdata
     *
     * @param pBancsIDAndVdata
     *            the bancsIDAndVdata to set
     */
    public void setBancsIDAndVData(final IdAndVDataDTO pBancsIDAndVdata) {
        bancsIDAndVData = pBancsIDAndVdata;
    }

}
