/*
 * BancsPartyBasicDetailResponse.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.response;

import uk.co.phoenixlife.service.dto.policy.PartyBasicDTO;

/**
 * BancsPartyBasicDetailResponse.java
 */
public class BancsPartyBasicDetailResponse {
    private PartyBasicDTO bancsPartyBasicDetail;

    /**
     * Gets the bancsPartyBasicDetail
     *
     * @return the bancsPartyBasicDetail
     */
    public PartyBasicDTO getBancsPartyBasicDetail() {
        return bancsPartyBasicDetail;
    }

    /**
     * Sets the bancsPartyBasicDetail
     *
     * @param pBancsPartyBasicDetail
     *            the bancsPartyBasicDetail to set
     */
    public void setBancsPartyBasicDetail(final PartyBasicDTO pBancsPartyBasicDetail) {
        bancsPartyBasicDetail = pBancsPartyBasicDetail;
    }

}
