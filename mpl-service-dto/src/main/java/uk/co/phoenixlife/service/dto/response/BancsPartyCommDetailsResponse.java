/*
 * BancsPartyCommDetailsResponse.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.response;

import uk.co.phoenixlife.service.dto.policy.PartyCommDetailsDTO;

/**
 * BancsPartyCommDetailsResponse.java
 */
public class BancsPartyCommDetailsResponse {
    private PartyCommDetailsDTO bancsPartyCommDetails;

    /**
     * Gets the bancsPartyCommDetails
     *
     * @return the bancsPartyCommDetails
     */
    public PartyCommDetailsDTO getBancsPartyCommDetails() {
        return bancsPartyCommDetails;
    }

    /**
     * Sets the bancsPartyCommDetails
     *
     * @param pBancsPartyCommDetails
     *            the bancsPartyCommDetails to set
     */
    public void setBancsPartyCommDetails(final PartyCommDetailsDTO pBancsPartyCommDetails) {
        bancsPartyCommDetails = pBancsPartyCommDetails;
    }

}
