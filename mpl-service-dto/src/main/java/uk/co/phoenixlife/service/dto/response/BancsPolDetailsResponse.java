/*
 * BancsPolDetailsResponse.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.response;

import uk.co.phoenixlife.service.dto.policy.PolDetailsDTO;

/**
 * BancsPolDetailsResponse.java
 */
public class BancsPolDetailsResponse {
    private PolDetailsDTO bancsPolDetails;

    /**
     * Gets the bancsPolDetails
     *
     * @return the bancsPolDetails
     */
    public PolDetailsDTO getBancsPolDetails() {
        return bancsPolDetails;
    }

    /**
     * Sets the bancsPolDetails
     *
     * @param pBancsPolDetails
     *            the bancsPolDetails to set
     */
    public void setBancsPolDetails(final PolDetailsDTO pBancsPolDetails) {
        bancsPolDetails = pBancsPolDetails;
    }

}
