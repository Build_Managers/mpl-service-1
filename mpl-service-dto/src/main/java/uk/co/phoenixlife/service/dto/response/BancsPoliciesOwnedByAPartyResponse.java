/*
 * BancsPoliciesOwnedByAPartyResponse.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.response;

import uk.co.phoenixlife.service.dto.dashboard.PoliciesOwnedByAPartyDTO;

/**
 * BancsPoliciesOwnedByAPartyResponse.java
 */
public class BancsPoliciesOwnedByAPartyResponse {
    private PoliciesOwnedByAPartyDTO bancsPoliciesOwnedByAParty;

    /**
     * Gets the bancsPoliciesOwnedByAParty
     *
     * @return the bancsPoliciesOwnedByAParty
     */
    public PoliciesOwnedByAPartyDTO getBancsPoliciesOwnedByAParty() {
        return bancsPoliciesOwnedByAParty;
    }

    /**
     * Sets the bancsPoliciesOwnedByAParty
     *
     * @param pBancsPoliciesOwnedByAParty
     *            the bancsPoliciesOwnedByAParty to set
     */
    public void setBancsPoliciesOwnedByAParty(final PoliciesOwnedByAPartyDTO pBancsPoliciesOwnedByAParty) {
        bancsPoliciesOwnedByAParty = pBancsPoliciesOwnedByAParty;
    }

}
