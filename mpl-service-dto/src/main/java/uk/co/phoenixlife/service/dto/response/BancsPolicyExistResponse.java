package uk.co.phoenixlife.service.dto.response;

import uk.co.phoenixlife.service.dto.policy.BancsPolicyExists;

/**
 * BancsPolicyExistResponse.java
 */
public class BancsPolicyExistResponse {

    private BancsPolicyExists bancsPolicyExistsCheck;

    /**
     * Gets the bancsPolicyExistCheckList
     *
     * @return the bancsPolicyExistCheckList
     */
    public BancsPolicyExists getBancsPolicyExistsCheck() {
        return bancsPolicyExistsCheck;
    }

    /**
     * Sets the bancsPolicyExistCheckList
     *
     * @param pBancsPolicyExistsCheck
     *            the pbancsPolicyExistCheckList to set
     */
    public void setBancsPolicyExistsCheck(final BancsPolicyExists pBancsPolicyExistsCheck) {
        bancsPolicyExistsCheck = pBancsPolicyExistsCheck;
    }

}
