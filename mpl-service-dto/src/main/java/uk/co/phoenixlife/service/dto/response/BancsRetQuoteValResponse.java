/*
 * BancsCalcULPolValResponse.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.response;

import uk.co.phoenixlife.service.dto.policy.BancsRetQuoteValDTO;

/**
 * BancsCalcULPolValResponse.java
 */
public class BancsRetQuoteValResponse {
    private BancsRetQuoteValDTO bancsRetQuoteVal;

    /**
     * Gets the bancsRetQuoteVal
     *
     * @return the bancsRetQuoteVal
     */
    public BancsRetQuoteValDTO getBancsRetQuoteVal() {
        return bancsRetQuoteVal;
    }

    /**
     * Sets the bancsRetQuoteVal
     *
     * @param pBancsRetQuoteVal
     *            the bancsRetQuoteVal to set
     */
    public void setBancsRetQuoteVal(final BancsRetQuoteValDTO pBancsRetQuoteVal) {
        bancsRetQuoteVal = pBancsRetQuoteVal;
    }

}
