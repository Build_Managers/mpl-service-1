package uk.co.phoenixlife.service.dto.response;

/**
 * ContactDetailsResponse.java
 */
public class ContactDetailsResponse {

    private String primaryPhoneType;
    private String primaryPhoneNumber;
    private String secondaryPhoneType;
    private String secondaryPhoneNumber;
    private String additionalPhoneType;
    private String additionalPhoneNumber;

    /**
     * Gets the primaryPhoneType
     *
     * @return the primaryPhoneType
     */
    public String getPrimaryPhoneType() {
        return primaryPhoneType;
    }

    /**
     * Sets the primaryPhoneType
     *
     * @param pPrimaryPhoneType
     *            the primaryPhoneType to set
     */
    public void setPrimaryPhoneType(final String pPrimaryPhoneType) {
        primaryPhoneType = pPrimaryPhoneType;
    }

    /**
     * Gets the primaryPhoneNumber
     *
     * @return the primaryPhoneNumber
     */
    public String getPrimaryPhoneNumber() {
        return primaryPhoneNumber;
    }

    /**
     * Sets the primaryPhoneNumber
     *
     * @param pPrimaryPhoneNumber
     *            the primaryPhoneNumber to set
     */
    public void setPrimaryPhoneNumber(final String pPrimaryPhoneNumber) {
        primaryPhoneNumber = pPrimaryPhoneNumber;
    }

    /**
     * Gets the secondaryPhoneType
     *
     * @return the secondaryPhoneType
     */
    public String getSecondaryPhoneType() {
        return secondaryPhoneType;
    }

    /**
     * Sets the secondaryPhoneType
     *
     * @param pSecondaryPhoneType
     *            the secondaryPhoneType to set
     */
    public void setSecondaryPhoneType(final String pSecondaryPhoneType) {
        secondaryPhoneType = pSecondaryPhoneType;
    }

    /**
     * Gets the secondaryPhoneNumber
     *
     * @return the secondaryPhoneNumber
     */
    public String getSecondaryPhoneNumber() {
        return secondaryPhoneNumber;
    }

    /**
     * Sets the secondaryPhoneNumber
     *
     * @param pSecondaryPhoneNumber
     *            the secondaryPhoneNumber to set
     */
    public void setSecondaryPhoneNumber(final String pSecondaryPhoneNumber) {
        secondaryPhoneNumber = pSecondaryPhoneNumber;
    }

    /**
     * Gets the additionalPhoneType
     *
     * @return the additionalPhoneType
     */
    public String getAdditionalPhoneType() {
        return additionalPhoneType;
    }

    /**
     * Sets the additionalPhoneType
     *
     * @param pAdditionalPhoneType
     *            the additionalPhoneType to set
     */
    public void setAdditionalPhoneType(final String pAdditionalPhoneType) {
        additionalPhoneType = pAdditionalPhoneType;
    }

    /**
     * Gets the additionalPhoneNumber
     *
     * @return the additionalPhoneNumber
     */
    public String getAdditionalPhoneNumber() {
        return additionalPhoneNumber;
    }

    /**
     * Sets the additionalPhoneNumber
     *
     * @param pAdditionalPhoneNumber
     *            the additionalPhoneNumber to set
     */
    public void setAdditionalPhoneNumber(final String pAdditionalPhoneNumber) {
        additionalPhoneNumber = pAdditionalPhoneNumber;
    }

}
