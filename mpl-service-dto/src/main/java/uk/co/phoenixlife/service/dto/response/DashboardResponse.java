/*
 * DashboardResponse.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.response;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * DashboardResponse.java
 */
public class DashboardResponse implements Serializable {

    /** long */
    private static final long serialVersionUID = 1326653404658708275L;

    private int polStatus;
    private String detail;

    /**
     * Gets the polStatus
     *
     * @return the polStatus
     */
    public int getPolStatus() {
        return polStatus;
    }

    /**
     * Sets the polStatus
     *
     * @param pPolStatus
     *            the polStatus to set
     */
    public void setPolStatus(final int pPolStatus) {
        polStatus = pPolStatus;
    }

    /**
     * Gets the detail
     *
     * @return the detail
     */
    public String getDetail() {
        return detail;
    }

    /**
     * Sets the detail
     *
     * @param pDetail
     *            the detail to set
     */
    public void setDetail(final String pDetail) {
        detail = pDetail;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
