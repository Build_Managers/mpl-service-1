package uk.co.phoenixlife.service.dto.response;
/*

 * ErrorDetailDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */


import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * ErrorDetailDTO.java
 */
public class ErrorDetailDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String title;
    @JsonProperty("internal_Error_Code")
    private String internalErrorCode;
    @JsonProperty("error_Desc")
    private String errorDesciption;

    /**
     * Gets the title
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title
     *
     * @param pTitle
     *            the title to set
     */
    public void setTitle(final String pTitle) {
        title = pTitle;
    }

    /**
     * Gets the internalErrorCode
     *
     * @return the internalErrorCode
     */
    public String getInternalErrorCode() {
        return internalErrorCode;
    }

    /**
     * Sets the internalErrorCode
     *
     * @param pInternalErrorCode
     *            the internalErrorCode to set
     */
    public void setInternalErrorCode(final String pInternalErrorCode) {
        internalErrorCode = pInternalErrorCode;
    }

    /**
     * Gets the errorDesciption
     *
     * @return the errorDesciption
     */
    public String getErrorDesciption() {
        return errorDesciption;
    }

    /**
     * Sets the errorDesciption
     *
     * @param pErrorDesciption
     *            the errorDesciption to set
     */
    public void setErrorDesciption(final String pErrorDesciption) {
        errorDesciption = pErrorDesciption;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}