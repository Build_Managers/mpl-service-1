package uk.co.phoenixlife.service.dto.response;

/*
 * ErrorResponseDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * ErrorResponseDTO.java
 */
public class ErrorResponseDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    @JsonProperty("error_Detail")
    private ErrorDetailDTO errorDetail;

    /**
     * Gets the errorDetail
     *
     * @return the errorDetail
     */
    public ErrorDetailDTO getErrorDetail() {
        return errorDetail;
    }

    /**
     * Sets the errorDetail
     *
     * @param pErrorDetail
     *            the errorDetail to set
     */
    public void setErrorDetail(final ErrorDetailDTO pErrorDetail) {
        errorDetail = pErrorDetail;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
