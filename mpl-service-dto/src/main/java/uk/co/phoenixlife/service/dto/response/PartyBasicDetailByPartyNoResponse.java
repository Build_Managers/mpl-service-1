package uk.co.phoenixlife.service.dto.response;

import java.time.LocalDate;

import uk.co.phoenixlife.service.dto.dashboard.PartyBasicDetailByPartyNoDTO;

public class PartyBasicDetailByPartyNoResponse {

	private PartyBasicDetailByPartyNoDTO partyBasicDetailByPartyNo;

	public PartyBasicDetailByPartyNoDTO getPartyBasicDetailByPartyNo() {
		return partyBasicDetailByPartyNo;
	}

	public void setPartyBasicDetailByPartyNo( PartyBasicDetailByPartyNoDTO partyBasicDetailByPartyNo) {
		this.partyBasicDetailByPartyNo = partyBasicDetailByPartyNo;
	}

	
	
	
	
}
