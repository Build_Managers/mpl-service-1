/*
 * BancsPoliciesOwnedByAPartyResponse.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.response;

import uk.co.phoenixlife.service.dto.dashboard.PoliciesOwnedByAPartyDTO;

/**
 * BancsPoliciesOwnedByAPartyResponse.java
 */
public class PoliciesOwnedByAPartyResponse {
    private PoliciesOwnedByAPartyDTO policiesOwnedByAParty;

    /**
     * Gets the policiesOwnedByAParty
     *
     * @return the policiesOwnedByAParty
     */
    public PoliciesOwnedByAPartyDTO getBancsPoliciesOwnedByAParty() {
        return policiesOwnedByAParty;
    }

    /**
     * Sets the policiesOwnedByAParty
     *
     * @param ppoliciesOwnedByAParty
     *            the policiesOwnedByAParty to set
     */
    public void setBancsPoliciesOwnedByAParty(final PoliciesOwnedByAPartyDTO ppoliciesOwnedByAParty) {
    	policiesOwnedByAParty = ppoliciesOwnedByAParty;
    }

}
