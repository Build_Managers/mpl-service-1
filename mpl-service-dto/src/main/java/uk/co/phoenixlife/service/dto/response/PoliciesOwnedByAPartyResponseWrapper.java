package uk.co.phoenixlife.service.dto.response;

import java.util.List;

public class PoliciesOwnedByAPartyResponseWrapper {

    private PoliciesOwnedByAPartyResponse policiesOwnedByAPartyResponse;
    private List<String> xGUIDBancsPolNumCacheKeyList;

    /**
     * Gets the policiesOwnedByAPartyResponse
     * 
     * @return the policiesOwnedByAPartyResponse
     */
    public PoliciesOwnedByAPartyResponse getPoliciesOwnedByAPartyResponse() {
        return policiesOwnedByAPartyResponse;
    }

    /**
     * Sets the policiesOwnedByAPartyResponse
     * 
     * @param policiesOwnedByAPartyResponse
     *            the policiesOwnedByAPartyResponse to set
     */
    public void setPoliciesOwnedByAPartyResponse(final PoliciesOwnedByAPartyResponse policiesOwnedByAPartyResponse) {
        this.policiesOwnedByAPartyResponse = policiesOwnedByAPartyResponse;
    }

    /**
     * Gets the xGUIDBancsPolNumCacheKeyList
     * 
     * @return the xGUIDBancsPolNumCacheKeyList
     */
    public List<String> getxGUIDBancsPolNumCacheKeyList() {
        return xGUIDBancsPolNumCacheKeyList;
    }

    /**
     * Sets the xGUIDBancsPolNumCacheKeyList
     * 
     * @param xGUIDBancsPolNumCacheKeyList
     *            the xGUIDBancsPolNumCacheKeyList to set
     */
    public void setxGUIDBancsPolNumCacheKeyList(final List<String> xGUIDBancsPolNumCacheKeyList) {
        this.xGUIDBancsPolNumCacheKeyList = xGUIDBancsPolNumCacheKeyList;
    }

}
