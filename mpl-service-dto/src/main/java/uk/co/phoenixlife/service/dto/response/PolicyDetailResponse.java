/**
 * 
 */
package uk.co.phoenixlife.service.dto.response;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import uk.co.phoenixlife.service.dto.dashboard.Policy_Detail;

/**
 * @author 1012746
 *
 */
public class PolicyDetailResponse
{
    private Policy_Detail policy_Detail;

    public Policy_Detail getPolicy_Detail ()
    {
        return policy_Detail;
    }

    public void setPolicy_Detail (Policy_Detail policy_Detail)
    {
        this.policy_Detail = policy_Detail;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
