package uk.co.phoenixlife.service.dto.response;

import java.util.List;

/**
 * PolicyLockedResponse.java
 */
public class PolicyLockedResponse {

    private boolean isPolicyLocked;
    private List<Long> customerIdList;

    /**
     * Gets the isPolicyLocked
     *
     * @return the isPolicyLocked
     */
    public boolean isPolicyLocked() {
        return isPolicyLocked;
    }

    /**
     * Sets the isPolicyLocked
     *
     * @param pIsPolicyLocked
     *            the isPolicyLocked to set
     */
    public void setPolicyLocked(final boolean pIsPolicyLocked) {
        isPolicyLocked = pIsPolicyLocked;
    }

    /**
     * Gets the customerIdList
     *
     * @return the customerIdList
     */
    public List<Long> getCustomerIdList() {
        return customerIdList;
    }

    /**
     * Sets the customerIdList
     *
     * @param pCustomerIdList
     *            the customerIdList to set
     */
    public void setCustomerIdList(final List<Long> pCustomerIdList) {
        customerIdList = pCustomerIdList;
    }

}
