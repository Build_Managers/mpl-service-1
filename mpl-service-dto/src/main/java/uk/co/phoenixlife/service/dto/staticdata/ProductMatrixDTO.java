/*
 * ProductMatrixDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.staticdata;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * ProductMatrixDTO.java
 */
public class ProductMatrixDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = -218685233403685424L;

    private char displayFundValueFlag;
    private char displayTransferValueFlag;
    private char displayDeathValueFlag;
    private char displayCurrentPremiumFlag;
    private char garFlag;
    private char lifestyleOptionFlag;
    private char gmpValueFlag;
    private char displayFundHoldingInfoFlag;

    /**
     * Constructor
     *
     * @param pDisplayFundValueFlag
     *            - FundValueFlag
     * @param pDisplayTransferValueFlag
     *            - TransferValueFlag
     * @param pDisplayDeathValueFlag
     *            - DeathValueFlag
     * @param pDisplayCurrentPremiumFlag
     *            - CurrentPremiumFlag
     * @param pGarFlag
     *            - garFlag
     * @param pLifestyleOptionFlag
     *            - lifestyleOptionFlag
     * @param pGmpValueFlag
     *            - gmpValueFlag
     * @param pDisplayFundHoldingInfoFlag
     *            - FundHoldingInfoFlag
     */
    public ProductMatrixDTO(final char pDisplayFundValueFlag, final char pDisplayTransferValueFlag,
            final char pDisplayDeathValueFlag, final char pDisplayCurrentPremiumFlag, final char pGarFlag,
            final char pLifestyleOptionFlag, final char pGmpValueFlag, final char pDisplayFundHoldingInfoFlag) {
        super();
        displayFundValueFlag = pDisplayFundValueFlag;
        displayTransferValueFlag = pDisplayTransferValueFlag;
        displayDeathValueFlag = pDisplayDeathValueFlag;
        displayCurrentPremiumFlag = pDisplayCurrentPremiumFlag;
        garFlag = pGarFlag;
        lifestyleOptionFlag = pLifestyleOptionFlag;
        gmpValueFlag = pGmpValueFlag;
        displayFundHoldingInfoFlag = pDisplayFundHoldingInfoFlag;
    }

    /**
     * Gets the displayFundValueFlag
     *
     * @return the displayFundValueFlag
     */
    public char getDisplayFundValueFlag() {
        return displayFundValueFlag;
    }

    /**
     * Sets the displayFundValueFlag
     *
     * @param pDisplayFundValueFlag
     *            the displayFundValueFlag to set
     */
    public void setDisplayFundValueFlag(final char pDisplayFundValueFlag) {
        displayFundValueFlag = pDisplayFundValueFlag;
    }

    /**
     * Gets the displayTransferValueFlag
     *
     * @return the displayTransferValueFlag
     */
    public char getDisplayTransferValueFlag() {
        return displayTransferValueFlag;
    }

    /**
     * Sets the displayTransferValueFlag
     *
     * @param pDisplayTransferValueFlag
     *            the displayTransferValueFlag to set
     */
    public void setDisplayTransferValueFlag(final char pDisplayTransferValueFlag) {
        displayTransferValueFlag = pDisplayTransferValueFlag;
    }

    /**
     * Gets the displayDeathValueFlag
     *
     * @return the displayDeathValueFlag
     */
    public char getDisplayDeathValueFlag() {
        return displayDeathValueFlag;
    }

    /**
     * Sets the displayDeathValueFlag
     *
     * @param pDisplayDeathValueFlag
     *            the displayDeathValueFlag to set
     */
    public void setDisplayDeathValueFlag(final char pDisplayDeathValueFlag) {
        displayDeathValueFlag = pDisplayDeathValueFlag;
    }

    /**
     * Gets the displayCurrentPremiumFlag
     *
     * @return the displayCurrentPremiumFlag
     */
    public char getDisplayCurrentPremiumFlag() {
        return displayCurrentPremiumFlag;
    }

    /**
     * Sets the displayCurrentPremiumFlag
     *
     * @param pDisplayCurrentPremiumFlag
     *            the displayCurrentPremiumFlag to set
     */
    public void setDisplayCurrentPremiumFlag(final char pDisplayCurrentPremiumFlag) {
        displayCurrentPremiumFlag = pDisplayCurrentPremiumFlag;
    }

    /**
     * Gets the garFlag
     *
     * @return the garFlag
     */
    public char getGarFlag() {
        return garFlag;
    }

    /**
     * Sets the garFlag
     *
     * @param pGarFlag
     *            the garFlag to set
     */
    public void setGarFlag(final char pGarFlag) {
        garFlag = pGarFlag;
    }

    /**
     * Gets the lifestyleOptionFlag
     *
     * @return the lifestyleOptionFlag
     */
    public char getLifestyleOptionFlag() {
        return lifestyleOptionFlag;
    }

    /**
     * Sets the lifestyleOptionFlag
     *
     * @param pLifestyleOptionFlag
     *            the lifestyleOptionFlag to set
     */
    public void setLifestyleOptionFlag(final char pLifestyleOptionFlag) {
        lifestyleOptionFlag = pLifestyleOptionFlag;
    }

    /**
     * Gets the gmpValueFlag
     *
     * @return the gmpValueFlag
     */
    public char getGmpValueFlag() {
        return gmpValueFlag;
    }

    /**
     * Sets the gmpValueFlag
     *
     * @param pGmpValueFlag
     *            the gmpValueFlag to set
     */
    public void setGmpValueFlag(final char pGmpValueFlag) {
        gmpValueFlag = pGmpValueFlag;
    }

    /**
     * Gets the displayFundHoldingInfoFlag
     *
     * @return the displayFundHoldingInfoFlag
     */
    public char getDisplayFundHoldingInfoFlag() {
        return displayFundHoldingInfoFlag;
    }

    /**
     * Sets the displayFundHoldingInfoFlag
     *
     * @param pDisplayFundHoldingInfoFlag
     *            the displayFundHoldingInfoFlag to set
     */
    public void setDisplayFundHoldingInfoFlag(final char pDisplayFundHoldingInfoFlag) {
        displayFundHoldingInfoFlag = pDisplayFundHoldingInfoFlag;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
