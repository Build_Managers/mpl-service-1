/*
 * StaticDataRequestDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.staticdata;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * StaticDataRequestDTO.java
 */
public class StaticDataRequestDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private Boolean startFlag;
    private String groupId;
    private String keyValue;

    /**
     * Gets the startFlag
     *
     * @return the startFlag
     */
    public Boolean getStartFlag() {
        return startFlag;
    }

    /**
     * Sets the startFlag
     *
     * @param pStartFlag
     *            the startFlag to set
     */
    public void setStartFlag(final Boolean pStartFlag) {
        startFlag = pStartFlag;
    }

    /**
     * Gets the groupId
     *
     * @return the groupId
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Sets the groupId
     *
     * @param pGroupId
     *            the groupId to set
     */
    public void setGroupId(final String pGroupId) {
        groupId = pGroupId;
    }

    /**
     * Gets the keyValue
     *
     * @return the keyValue
     */
    public String getKeyValue() {
        return keyValue;
    }

    /**
     * Sets the keyValue
     *
     * @param pKeyValue
     *            the keyValue to set
     */
    public void setKeyValue(final String pKeyValue) {
        keyValue = pKeyValue;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
