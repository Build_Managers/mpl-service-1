/*
 * StaticDataResponseDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.staticdata;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * StaticDataResponseDTO.java
 */
public class StaticDataResponseDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private List<StaticIDVDataDTO> staticIDVDataList;
    private Map<String, String> staticDataMap;
    private String staticValue;

    /**
     * Gets the staticIDVDataList
     *
     * @return the staticIDVDataList
     */
    public List<StaticIDVDataDTO> getStaticIDVDataList() {
        return staticIDVDataList;
    }

    /**
     * Sets the staticIDVDataList
     *
     * @param pStaticIDVDataList
     *            the staticIDVDataList to set
     */
    public void setStaticIDVDataList(final List<StaticIDVDataDTO> pStaticIDVDataList) {
        staticIDVDataList = pStaticIDVDataList;
    }

    /**
     * Gets the staticDataMap
     *
     * @return the staticDataMap
     */
    public Map<String, String> getStaticDataMap() {
        return staticDataMap;
    }

    /**
     * Sets the staticDataMap
     *
     * @param pStaticDataMap
     *            the staticDataMap to set
     */
    public void setStaticDataMap(final Map<String, String> pStaticDataMap) {
        staticDataMap = pStaticDataMap;
    }

    /**
     * Gets the staticValue
     *
     * @return the staticValue
     */
    public String getStaticValue() {
        return staticValue;
    }

    /**
     * Sets the staticValue
     *
     * @param pStaticValue
     *            the staticValue to set
     */
    public void setStaticValue(final String pStaticValue) {
        staticValue = pStaticValue;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
