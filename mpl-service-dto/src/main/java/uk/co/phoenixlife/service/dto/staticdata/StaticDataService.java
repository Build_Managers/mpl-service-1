/*
 * StaticDataService.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.staticdata;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

/**
 * StaticDataService.java
 */
@Service
public interface StaticDataService {

    /**
     * Method to get static data map for a group
     *
     * @param groupId
     *            - groupId
     * @return -{@link Map}
     * @throws Exception
     *             - {@link Exception}
     */
    Map<String, String> getStaticDataMap(String groupId) throws Exception;

    /**
     * Method to get value of selected groupId key
     *
     * @param groupId
     *            - groupId
     * @param key
     *            - key
     * @return - String value
     * @throws Exception
     *             - {@link Exception}
     */
    String getStaticDataValue(String groupId, String key) throws Exception;

    /**
     * Method to get IDV data
     *
     * @return - {@link List}
     * @throws Exception
     *             - {@link Exception}
     */
    List<StaticIDVDataDTO> getIDVQuestionsData() throws Exception;
}
