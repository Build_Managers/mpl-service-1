/*
 * StaticIDVDataDTO.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.staticdata;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * StaticIDVDataDTO.java
 */
public class StaticIDVDataDTO implements Serializable {

    /** long */
    private static final long serialVersionUID = 1L;

    private String questionKey;
    private String questionValue;
    private String questionLabel;

    /**
     * Default Constructor
     */
    public StaticIDVDataDTO() {
        super();
    }

    /**
     * Constructor
     *
     * @param pQuestionKey
     *            - key
     * @param pQuestionValue
     *            - value
     * @param pQuestionLabel
     *            - label
     */
    public StaticIDVDataDTO(final String pQuestionKey, final String pQuestionValue, final String pQuestionLabel) {
        super();
        questionKey = pQuestionKey;
        questionValue = pQuestionValue;
        questionLabel = pQuestionLabel;
    }

    /**
     * Gets the questionKey
     *
     * @return the questionKey
     */
    public String getQuestionKey() {
        return questionKey;
    }

    /**
     * Sets the questionKey
     *
     * @param pQuestionKey
     *            the questionKey to set
     */
    public void setQuestionKey(final String pQuestionKey) {
        questionKey = pQuestionKey;
    }

    /**
     * Gets the questionValue
     *
     * @return the questionValue
     */
    public String getQuestionValue() {
        return questionValue;
    }

    /**
     * Sets the questionValue
     *
     * @param pQuestionValue
     *            the questionValue to set
     */
    public void setQuestionValue(final String pQuestionValue) {
        questionValue = pQuestionValue;
    }

    /**
     * Gets the questionLabel
     *
     * @return the questionLabel
     */
    public String getQuestionLabel() {
        return questionLabel;
    }

    /**
     * Sets the questionLabel
     *
     * @param pQuestionLabel
     *            the questionLabel to set
     */
    public void setQuestionLabel(final String pQuestionLabel) {
        questionLabel = pQuestionLabel;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
