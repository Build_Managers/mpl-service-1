/*
 * ServiceStub.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.dto.stubs;

/**
 * ServiceStub.java
 */
public final class ServiceStub {

    private ServiceStub() {
        super();
    }

    /**
     * Method to get stub response for bank wizard validation
     *
     * @param sortCode
     *            - sortCode
     * @param accountNumber
     *            - accountNumber
     * @return - true/false
     */
    public static boolean stubBankWizardValidation(final String sortCode, final String accountNumber) {
        boolean validFlag = false;

        if ("12345678".equalsIgnoreCase(accountNumber) && "938611".equalsIgnoreCase(sortCode)) {
            validFlag = true;
        }
        return validFlag;
    }
}
