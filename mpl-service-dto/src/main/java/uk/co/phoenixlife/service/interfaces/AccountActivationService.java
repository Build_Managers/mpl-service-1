package uk.co.phoenixlife.service.interfaces;

import uk.co.phoenixlife.service.dto.activation.CustomerActivationDTO;
import uk.co.phoenixlife.service.dto.activation.CustomerDetails;
import uk.co.phoenixlife.service.dto.activation.CustomerPersonalDetailStatusResponse;
import uk.co.phoenixlife.service.dto.activation.PolicyExistLockStatus;
import uk.co.phoenixlife.service.dto.activation.VerificationCheckAnswerWrapperDTO;
import uk.co.phoenixlife.service.dto.activation.VerificationDisplayEligibleQuestionWrapperDTO;
import uk.co.phoenixlife.service.dto.activation.VerificationMapAndLockedCountDTO;
import uk.co.phoenixlife.service.dto.activation.VerificationReturnValuesDTO;

/**
 * Service for AccountActivation
 */
public interface AccountActivationService {
    /**
     * Method to check policy existence in baNCs
     *
     * @param policyNumber
     *            policy number entered by user
     * @param xGUID
     *            xGUID
     * @return PolicyExistResponse or Error
     * @throws Exception
     *             any Exception caught
     */
    PolicyExistLockStatus getPolicyExist(final CustomerActivationDTO customerActivationDTO, final String xGUID)
            throws Exception;

    /**
     * Method to validate a policy via personal details
     *
     * @param customerActivationDTO
     *            customer entered personal detail
     * @param xGUID
     *            xGUID of the user
     * @return List of possible user matching those details
     * @throws Exception
     *             exception
     */
    CustomerPersonalDetailStatusResponse validatePersonalDetail(final CustomerActivationDTO customerActivationDTO,
            final String xGUID) throws Exception;

    CustomerDetails validatePostCodeGoneAwayAndLifeStatusFlag(final CustomerDetails customerDetails, final String xGUID)
            throws Exception;

    VerificationReturnValuesDTO getQuestionAnswer(final CustomerActivationDTO customerActivationDTO, final String xGUID)
            throws Exception;

    VerificationMapAndLockedCountDTO checkAnswer(final VerificationCheckAnswerWrapperDTO verificationCheckAnswerWrapperDTO,
            final String xGUID)
            throws Exception;

    VerificationReturnValuesDTO getNextQuestionDetails(
            final VerificationDisplayEligibleQuestionWrapperDTO VerificationDisplayEligibleQuestionWrapperDTO,
            final String xGUID) throws Exception;

    CustomerActivationDTO getCustomerAndPolicyId(final CustomerActivationDTO customerActivationDTO, final String xGUID)
            throws Exception;

    Boolean checkUsernameExistForDifferentUniqueCustomerNo(final CustomerActivationDTO customerActivationDTO,
            final String xGUID);
}
