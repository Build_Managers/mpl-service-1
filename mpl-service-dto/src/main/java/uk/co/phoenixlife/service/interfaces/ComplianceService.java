package uk.co.phoenixlife.service.interfaces;

import java.util.List;

import uk.co.phoenixlife.service.dto.compliance.ComplianceDTO;

/**
 * This service is used to hold the logic i.e. related to Compliance
 * ComplianceService.java
 */
public interface ComplianceService {

    /**
     * Method to save the compliance data into DB
     *
     * @param listOfCompliance
     *            List of compliance object
     * @return flag (true/ false) about DB
     */
    boolean saveComplianceData(final List<ComplianceDTO> listOfCompliance, final String xGUID);

}
