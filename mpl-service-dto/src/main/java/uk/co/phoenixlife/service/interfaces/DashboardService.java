/*
 * DashboardService.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.interfaces;

import org.springframework.stereotype.Service;

import uk.co.phoenixlife.service.dto.activation.CustomerIdentifierDTO;
import uk.co.phoenixlife.service.dto.activation.CustomerPolicyDetail;
import uk.co.phoenixlife.service.dto.bancspost.BancsCalcCashInTaxOutput;
import uk.co.phoenixlife.service.dto.policy.CashInTypeDTO;
import uk.co.phoenixlife.service.dto.policy.DashBoardResponse;
import uk.co.phoenixlife.service.dto.policy.FinalDashboardDTO;
import uk.co.phoenixlife.service.dto.response.BancsPartyCommDetailsResponse;
import uk.co.phoenixlife.service.dto.response.PoliciesOwnedByAPartyResponseWrapper;

/**
 * DashboardService.java
 */
@Service
public interface DashboardService {

    /**
     * Method to PoliciesOwnedByParty
     *
     * @param uniqueCustomerNumber
     *            - uniqueCustomerNumber
     * @param xGUID
     *            - xGUID
     * @return - PoliciesOwnedByPartyResponse object
     * @throws Exception
     *             - {@link Exception}
     */
    PoliciesOwnedByAPartyResponseWrapper policiesOwnedByParty(String uniqueCustomerNumber, String xGUID) throws Exception;

    /**
     * Method to policyDetail
     *
     * @param policyNumber
     *            - policyNumber
     * @param xGUID
     *            - xGUID
     * @return - CustomerPolicyDetail object
     * @throws Exception
     *             - {@link Exception}
     */
    CustomerPolicyDetail getpolicyDetailValuationPremiumSum(String policyNumber, String xGUID);

    /**
     *
     * @param bancsCustNumber
     * @param bancsPolicyNumber
     * @param xGUID
     * @return
     * @throws Exception
     */
    BancsCalcCashInTaxOutput getBancsCalcCashInTaxOutput(final CashInTypeDTO cashInType, final String xGUID)
            throws Exception;

    /**
     *
     * @param bancsCustNumber
     * @param bancsPolicyNumber
     * @param xGUID
     * @return
     * @throws Exception
     */
    BancsCalcCashInTaxOutput getSyncBancsCalcCashInTaxOutput(final CashInTypeDTO cashInType, final String xGUID)
            throws Exception;

    /**
     * Method to get Encashment Dashboard
     *
     * @param customerData
     *            Customer Data - bancsUniqueCustNo,polNum
     * @param xGUID
     *            xGUID
     * @return Dashboard of the user
     * @throws Exception
     */
    FinalDashboardDTO getEncashmentDashboard(final CustomerIdentifierDTO customerData, final String xGUID) throws Exception;

    DashBoardResponse getUniqueCustomerNo(final String userName, final String xGUID) throws Exception;

    /**
     * Method to get PartCommDetails Dashboard
     *
     * @param customerData
     *            Customer Data - bancsUniqueCustNo,polNum
     * @param xGUID
     *            xGUID
     * @return Dashboard of the user
     * @throws Exception
     */
    BancsPartyCommDetailsResponse getPartyCommDetailsResponse(final String bancsUniqueCustNo, final String xGUID)
            throws Exception;
}
