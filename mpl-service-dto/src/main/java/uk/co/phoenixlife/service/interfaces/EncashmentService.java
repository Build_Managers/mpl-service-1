/*
 * EncashmentService.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.interfaces;

import uk.co.phoenixlife.service.dto.activation.CustomerIdentifierDTO;
import uk.co.phoenixlife.service.dto.policy.CustomerDTO;
import uk.co.phoenixlife.service.dto.policy.FinalDashboardDTO;

/**
 * EncashmentService.java
 */
public interface EncashmentService {

    /**
     * This method return encashment Dashboard of a customer
     *
     * @param polNum
     *            policy number
     * @param bancsUniqueCustNo
     *            bancsUniqueCustNo
     * @param listOfBancPolNum
     *            listOfBancPolNum
     * @param emailId
     *            emailId
     * @param xGUID
     *            xGUID
     * @return Dashboard Object
     */
    FinalDashboardDTO getEncashmentDashboard(final String bancsUniqueCustNo, final CustomerIdentifierDTO customerData, final String xGUID) throws Exception;

    /**
     * This method returns flag status about bancs Post Calls
     *
     * @param customerDTO
     *            CustomerInformation
     * @param xGUID
     *            xguid
     * @return flag
     * @throws Exception
     */
    void submitEncashmentPolicyDetailsAndBaNCSCalls(final CustomerDTO customerDTO, final String xGUID) throws Exception;
}
