package uk.co.phoenixlife.service.interfaces;

import java.util.List;

public interface LogoutService {

    void clearCache(final String xGUID, final List<String> xGUIDBancsPolNumCacheKeyList);
}
