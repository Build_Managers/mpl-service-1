package uk.co.phoenixlife.service.interfaces;

import java.util.List;

import org.springframework.stereotype.Service;

import uk.co.phoenixlife.service.dto.ErrorListDTO;
import uk.co.phoenixlife.service.dto.compliance.ComplianceDTO;


public interface MIComplianceService {

	
	 void submitErrorList(final List<ErrorListDTO> errorList, final String xGUID, final String emailAddress, List<ComplianceDTO> list);

	 
	

	
}
