package uk.co.phoenixlife.service.interfaces;

import org.springframework.stereotype.Service;

import uk.co.phoenixlife.service.dto.MiErrorRequestDTO;
@Service
public interface MIComplianceServiceInterface {
	 void submitErrorList(MiErrorRequestDTO miRequestDTO);
}
