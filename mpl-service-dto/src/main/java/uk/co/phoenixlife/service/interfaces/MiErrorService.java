package uk.co.phoenixlife.service.interfaces;

import java.util.List;

import org.springframework.stereotype.Service;

import uk.co.phoenixlife.service.dto.ErrorListDTO;
import uk.co.phoenixlife.service.dto.MiErrorRequestDTO;

/**
 *
 * MiErrorService.java
 */
@Service
public interface MiErrorService {

    /**
     * Method to save ErrorListDTO in DB
     *
     * @param errorListDTO
     *            - errorListDTO
     * @return {@link- Boolean}
     */
    Boolean saveErrorList(List<ErrorListDTO> errorListDTO);

}
