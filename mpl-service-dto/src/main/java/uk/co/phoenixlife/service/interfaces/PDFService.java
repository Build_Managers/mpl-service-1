/**
 *
 */
package uk.co.phoenixlife.service.interfaces;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;


import org.springframework.stereotype.Service;

import uk.co.phoenixlife.service.dto.pdf.FormRequestDTO;
import uk.co.phoenixlife.service.dto.pdf.PdfDataDTO;
import uk.co.phoenixlife.service.dto.policy.DashboardDTO;
import uk.co.phoenixlife.service.dto.policy.IdAndVDataDTO;
import uk.co.phoenixlife.service.dto.policy.PartyPolicyDTO;

/**
 * PDFCreationService
 * @author 1240916
 *
 */
@Service
public interface PDFService {

    

    /**
     * getStaticPdf
     * @return byte array
     * @throws IOException - IOException
     */
    byte[] getStaticPdf(String pdfType) throws Exception;  
    /**
     * 
     * @param customerId
     * @param eligible
     * @param xGUID
     * @return
     * @throws Exception
     */
    public Long insertEligiblePolicyData(final Long customerId, final DashboardDTO eligible, final String xGUID) throws Exception;
    /**
     * Method is used for retirement pack
     * @param dashboardDTO - dashboardDTO
     * @param xGUID - xGUID
     * @return boolean value
     */
    
    Boolean showRetirementPack(DashboardDTO dashboardDTO, final String xGUID);
    
    /**
     * Method is used for sendPdfContent
     * @param dashboardDTO - dashboardDTO
     * @param xGUID - xGUID
     * @return boolean value
     */
    
    Boolean savePdfContentToDB(FormRequestDTO formRequestDTO, final String xGUID) throws Exception ;
    
    
    
    /**
     * Method to get pdf as Byte Array
     *
     * @param policyId
     *            - policy id
     * @param docType
     *            - doc type (AF/RP)
     * @param xGUID - xGUID
     * @throws Exception - Exception
     * @return - Encoded Byte Array
     */
    String generatePdf(long policyId, String docType, String xGUID) throws Exception;
	/**
     * merge
     * @param inputPdfList - inputPdfList
     * @param baos1 - baos1
     * @return baos
     * @throws Exception - Exception
     */
    byte[] merge(List<ByteArrayInputStream> inputPdfList, ByteArrayOutputStream baos1) throws Exception;
    /**
     * Method is used for merging two pdf
     * @return byte array
     * @throws IOException - IOException
     */
    byte[] doMerge() throws IOException;

    /**
     * mergeSign
     * @param string - string
     * @return baos2
     * @throws Exception - Exception
     */
    byte[] mergeSign(String string) throws Exception;
    
    
    /*
     * 
     * @param pdfData
     * @param xGUID
     * @return
     * @throws Exception
     */
    byte[] generateRetirementPackPdf(final PdfDataDTO pdfData, final String xGUID) throws Exception;
    /**
     * 
     * @param decodedPDF
     * @param xGUID
     * @return
     * @throws Exception
     */
	byte[] generateApplicationPdf(byte[] decodedPDF, String xGUID) throws Exception;
    
	/**
	 * 
	 * @param bancsPolNum
	 * @param xGUID
	 * @return
	 * @throws Exception
	 */
	PartyPolicyDTO getEligiblePolicyIdvData(final DashboardDTO eligible, final String xGUID) throws Exception;
	
	/**
	 * 
	 * @param customerId
	 * @param eligible
	 * @param xGUID
	 * @return
	 * @throws Exception
	 */
	Long checkCustomerPolicyExistance(final Long customerId, final DashboardDTO eligible, final String xGUID) throws Exception;

}
