package uk.co.phoenixlife.service.interfaces;

import org.springframework.stereotype.Service;

import uk.co.phoenixlife.service.dto.pca.find.CaptureInteractiveFindV100ArrayOfResults;

@Service
public  interface PcaFindClientService  {

	CaptureInteractiveFindV100ArrayOfResults find(final String postcode);
	
	
}
