package uk.co.phoenixlife.service.interfaces;


import org.springframework.stereotype.Service;

import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100;
import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100ArrayOfResults;

@Service
public interface PcaRetriveClientService {
	
	   CaptureInteractiveRetrieveV100ArrayOfResults retrieve( final CaptureInteractiveRetrieveV100 retrieveRequest);


	   
	  

	

}
