/*
 * PolicyHolderService.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.interfaces;

import java.io.IOException;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import uk.co.phoenixlife.service.dto.policy.CustomerDTO;
import uk.co.phoenixlife.service.dto.response.BancsPartyCommDetailsResponse;
import uk.co.phoenixlife.service.dto.response.PartyBasicDetailByPartyNoResponse;

/**
 * This service for getting personal details of the Policy Holder.
 * PolicyHolderService.java
 */
@Service
public interface PolicyHolderService {
   
	
	/*public BancsPolicyHoldersResponse getPolicyHoldersDetails(final String bancsUniqueCustNo, final CustomerDTO customerDTO)
            throws JsonParseException, JsonMappingException, IOException;
*/
    public Boolean updateEmail(final CustomerDTO customerDTO);
    
    
    public PartyBasicDetailByPartyNoResponse getPartyBasicDetailByPartyNo(final String bancsUniqueCustNo, final String xGUID) 
    throws Exception;

	//public BancsPartyCommDetailsResponse getBancsPartyCommDetails(final String bancsUniqueCustNo);

	//PartyBasicDetailByPartyNoResponse getPartyBasicDetailByPartyNo1(String bancsUniqueCustNo);
}
