/*
 * PolicyService.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.interfaces;

import java.text.ParseException;
import java.util.List;
import uk.co.phoenixlife.service.dto.ErrorListDTO;
import uk.co.phoenixlife.service.dto.pdf.FormRequestDTO;
import uk.co.phoenixlife.service.dto.policy.PartyBasicDTO;
import uk.co.phoenixlife.service.dto.policy.PartyCommDetailsDTO;
import uk.co.phoenixlife.service.dto.policy.PolicyDataDTO;
import uk.co.phoenixlife.service.dto.policy.QuoteDetails;
import uk.co.phoenixlife.service.dto.response.PolicyLockedResponse;

/**
 * PolicyService.java
 */
public interface PolicyService {

    /**
     * Method to check if policy exists
     *
     * @param policynumber
     *            - policynumber
     * @param xGUID - xGUID
     * @throws Exception exception
     * @return - {@link Integer}
     */
    int isPolicyExist(String policynumber, String xGUID) throws Exception;

    /**
     * Method to check policy locked status
     *
     * @param policynumber
     *            - policynumber
     * @param xGUID - xGUID
     * @throws Exception exception
     * @return - {@link Boolean}
     */
    PolicyLockedResponse isPolicyLocked(final String policynumber, final String xGUID) throws Exception;

    /**
     * Method to check personal detail
     *
     * @param title
     *            - title
     * @param firstName
     *            - firstName
     * @param lastName
     *            - lastName
     * @param dateofbirth
     *            - dateofbirth
     * @param partyBasicDTO - partyBasicDTO
     * @param xGUID - xGUID
     * @return - {@link Boolean}
     * @throws ParseException
     *             - {@link ParseException}
     */
    PartyBasicDTO isPersonalDetailCheck(final String title, final String firstName, final String lastName,
            final String dateofbirth, final PartyBasicDTO partyBasicDTO, final String xGUID) throws ParseException;

    /**
     * Method to insert values in customer table
     * @param policyDataSubmit - policyDataSubmit
     * @param xGUID - xGUID
     * @return {@link List}
     */
    List<String> insertIntoCustomerTable(final PolicyDataDTO policyDataSubmit, final String xGUID);

    /**
     * Method to check if postcode entered by user matches with the response
     * given by Bancs
     *
     * @param postCode
     *            PostCode entered by user
     * @param partyCommDetailsDTO
     *            PartyCommDetailsDTO for session
     * @return result flag
     * @throws InterruptedException
     *             Exception in case of Thread.sleep
     */
    Boolean isPostCodeValid(final String postCode, final PartyCommDetailsDTO partyCommDetailsDTO) throws InterruptedException;

    /**
     * Method to submit policy encashment
     *
     * @param customerId
     *            - customerId
     * @param nino
     *            - nino
     * @param formData
     *            - FormRequestDTO object
     * @param quote
     *            - QuoteDetails object
     * @param xGUID
     *            - xGUID
     * @throws Exception
     *             -{@link Exception}
     */
    void submitPolicyEncashment(final long customerId, final String nino, final FormRequestDTO formData,
            final QuoteDetails quote, String xGUID) throws Exception;

    /**
     *Method to store ErrorListDTO in DB
     * @param errorListDTO - list of errors
     * @param xGUID - xGUID
     * @param emailAddress - emailAddress
     */
    void submitErrorList(final List<ErrorListDTO> errorListDTO, final String xGUID, final String emailAddress);
    /**
     * Method to update Elligible Policy count
     * @param count
     *            - count
     * @param customerId
     *            - customerId
     * @param xGUID - xGUID
     * @return boolean value
     */
    Boolean updateEligiblePolicyCount(final int count, final long customerId, final String xGUID);
}
