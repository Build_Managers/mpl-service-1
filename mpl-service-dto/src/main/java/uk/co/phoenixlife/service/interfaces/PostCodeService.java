/*
 * PostCodeService.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.interfaces;

import org.springframework.stereotype.Service;

import uk.co.phoenixlife.service.dto.pca.find.CaptureInteractiveFindV100ArrayOfResults;
import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100;
import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100ArrayOfResults;

/**
 * PostCodeService.java
 */
@Service
public interface PostCodeService {

    /**
     *
     * @param postcode
     * @param xGuid
     * @return
     * @throws Exception
     */
    CaptureInteractiveFindV100ArrayOfResults findAddress(final String postcode) throws Exception;

    /**
     *
     * @param retrieveRequest
     * @param xGuid
     * @return
     * @throws Exception
     */
    CaptureInteractiveRetrieveV100ArrayOfResults retrieveAddress(final CaptureInteractiveRetrieveV100 retrieveRequest)
            throws Exception;
}
