package uk.co.phoenixlife.service.interfaces;

import org.springframework.stereotype.Service;

import uk.co.phoenixlife.service.dto.pdf.FormRequestDTO;
import uk.co.phoenixlife.service.dto.policy.DashboardDTO;

/**
 * RetirementPackService
 * @author 1250641 This is interface who includes all methos for RetirementPack
 *         RetirementPackService.java
 */
@Service
public interface RetirementPackService {

    /**
     * Method to get pdf as Byte Array
     *
     * @param policyId
     *            - policy id
     * @param docType
     *            - doc type (AF/RP)
     * @param xGUID - xGUID
     * @throws Exception - Exception
     * @return - Encoded Byte Array
     */
    String generatePdf(long policyId, String docType, final String xGUID) throws Exception;

    /**
     * checkDocumentExists
     * @param policyId - policyId
     * @param docType - docType
     * @return integer value
     * @throws Exception - Exception
     */
    int checkDocumentExists(long policyId, String docType) throws Exception;

    /**
     * insertEligiblePolicyData
     * @param customerId - customerId
     * @param eligible - eligible
     * @param xGUID - xGUID
     * @return long value
     * @throws Exception - Exception
     */
    Long insertEligiblePolicyData(final Long customerId, final DashboardDTO eligible, final String xGUID) throws Exception;

    /**
     * savePdfContent
     * @param request - request
     * @return boolean
     * @throws Exception 
     */
	boolean savePdfContent(final FormRequestDTO request) throws Exception;
}
