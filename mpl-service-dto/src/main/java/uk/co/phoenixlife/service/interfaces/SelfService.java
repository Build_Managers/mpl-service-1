/*
 * SelfService.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.interfaces;

import java.util.List;

import org.springframework.stereotype.Service;

import uk.co.phoenixlife.service.dto.SecurityQuestionDTO;
import uk.co.phoenixlife.service.dto.SecurityQuestionsForAUserDTO;

/**
 * This service for getting security questions for a user based on  username.
 * SelfService.java
 */
@Service
public interface SelfService {


    public SecurityQuestionDTO getSecurityQuestions(String username);
    public boolean checkAnswerOfSecurityQuestion(SecurityQuestionsForAUserDTO  dtoObj);
}
