/*
 * PolicyHolderService.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.interfaces;

import java.util.Set;

import org.springframework.stereotype.Service;

/**
 * This service for getting personal details of the Policy Holder.
 * PolicyHolderService.java
 */
@Service
public interface UserRegistrationService {
    public boolean checkUsernameAvailabilty(Set<String> alreadyTakenUserNameSet);

    public String getTempUsername(boolean userNameAvailability);

    public String getSuggestedUsername(String tempUsername);
}
