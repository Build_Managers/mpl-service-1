/*
 * CommonUtils.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.dto.dashboard.AddressDTO;
import uk.co.phoenixlife.service.dto.dashboard.CommunicationDTO;
import uk.co.phoenixlife.service.dto.policy.PartyCommDetailsDTO;

/**
 * CommonUtils.java
 */
public final class CommonUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommonUtils.class);
    private static final String EMAIL_PATTERN = "[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";
    private static final int ONE = 1;
    private static final int TWENTYONE = 21;
    private static final int ELEVEN = 11;
    private static final int THIRTEEN = 13;
    private static final int FOURTEEN = 14;
    private static final int THRTYONE = 31;
    private static final int TWO = 2;
    private static final int THREE = 3;
    private static final int TWENTYTWO = 22;
    private static final int TWENTYTHREE = 23;
    private static final String BLANK = " ";
    private static final String ZERO = "0";
    private static final String INVALID = "INVALID_DOB";
    private static final String MOBILE_PATTERN = "^[0]{1}?[7]{1}?\\d{9}$|^[+]{1}?[4]{1}?[4]{1}?[7]{1}?\\d{9}$|^[0]{2}?[4]{2}?[7]{1}?\\d{9}$";
    private static final String HOMEWORK_PATTERN_01 = "^[0]{1}?[1]{1}?\\d{8,9}$";
    private static final String HOMEWORK_PATTERN_02 = "^[0]{1}?[2]{1}?\\d{9}$";
    private static final String TYPE = "Select";

    private CommonUtils() {
    }

    /**
     * Method to get a null object * @return - null
     */
    public static Object getNullObject() {
        return null;
    }

    /**
     * Check if object is null
     *
     * @param object
     *            - object
     * @return - true/false
     */

    /**
     * Method to check if object is not null
     *
     * @param obj
     *            - object
     * @return - {@link Boolean}
     */
    public static Boolean isObjectNotNull(final Object obj) {
        return null != obj;
    }

    public static Boolean isobjectNull(final Object object) {

        Boolean flag = false;
        if (null == object) {
            flag = true;
        }
        return flag;
    }

    /**
     * Check if object is not null
     *
     * @param object
     *            - object
     * @return - true/false true if object is not null
     */
    public static Boolean isobjectNotNull(final Object object) {

        Boolean flag = true;
        if (null == object) {
            flag = false;
        }
        return flag;
    }

    /**
     * Method to check Empty Fields
     *
     * @param input
     *            - input
     * @return - Return Boolean
     */
    public static boolean isEmpty(final String input) {
        boolean isEmpty = false;
        if (input == null || input.equals("")) {
            isEmpty = true;
        }
        return isEmpty;
    }

    /**
     * Method o check whitespaces
     *
     * @param input
     *            - input
     * @return - return boolean
     */
    public static boolean isWhitespace(final String input) {
        return input.matches("^\\s*$");
    }

    /**
     * Method to check date format
     *
     * @param date
     *            - input date
     * @return - {@link String}
     * @throws ParseException
     *             - {@link ParseException}
     */
    public static String checkDateFormat(final Date date) throws ParseException {

        SimpleDateFormat dateFormat;
        String curDate;

        dateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
        curDate = dateFormat.format(date);

        return curDate;
    }

    /**
     * Method to check date format for verification question
     *
     * @param date
     *            - input date
     * @return - {@link String}
     * @throws ParseException
     *             - {@link ParseException}
     */
    public static String checkDateFormatVerification(final Date date) throws ParseException {

        SimpleDateFormat dateFormat;
        String curDate;

        dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        curDate = dateFormat.format(date);

        return curDate;
    }

    /**
     * Method to check valid email Address
     *
     * @param emailAddress
     *            - emailadress
     * @return Return true
     */
    public static boolean isValidEmailAddress(final String emailAddress) {
        boolean valid = false;
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(emailAddress);
        if (matcher.matches()) {
            valid = true;

        }
        return valid;
    }

    /**
     * Check if string is valid
     *
     * @param str
     *            - input string
     * @return - {@link Boolean}
     */
    public static Boolean isValidString(final String str) {
        Boolean isValid = false;
        if (str != null && !str.equals("")) {
            isValid = true;
        }
        return isValid;

    }

    /**
     * Method to validate int
     *
     * @param input
     *            - input
     * @return Return boolean
     */
    public static boolean isNumber(final String input) {
        boolean isNumber = true;
        try {
            Integer.parseInt(input);
        } catch (final NumberFormatException nfe) {
            isNumber = false;
        }

        return isNumber;
    }

    /**
     * Check if list is valid.
     *
     * @param list
     *            list to check for validity
     * @return Boolean true if valid list false if invalid list
     */
    public static Boolean isValidList(final List<? extends Object> list) {
        Boolean isValid = false;
        if (null != list && !list.isEmpty()) {
            isValid = true;
        }
        return isValid;
    }

    /**
     * Check if set is valid.
     *
     * @param set
     *            list to check for validity
     * @return Boolean true if valid set false if invalid set
     */
    public static Boolean isValidSet(final Set<? extends Object> set) {
        Boolean isValid = false;
        if (null != set && !set.isEmpty()) {
            isValid = true;
        }
        return isValid;
    }

    /*
     * Method to generate the unique ID value. This unique ID can be used across
     * multiple functions of application such as to get a unique transaction ID
     * for BaNCS API calls. Unique ID is generated using java.util.UUID API.
     * Length is restricted to 36 characters.
     */
    /**
     * Method to generate the unique ID value. This unique ID can be used across
     * multiple functions of application such as to get a unique transaction ID
     * for BaNCS API calls. Unique ID is generated using java.util.UUID API.
     * Length is restricted to 36 characters.
     *
     * @return 36 char Unique String
     */
    public static String generateUniqueID() {

        return UUID.randomUUID().toString().substring(0, 36);

    }

    /**
     * to check a valid Premium
     *
     * @param answer
     *            answer
     * @return boolean result
     */
    public static boolean isValidPremium(final String answer) {
        boolean patternMatched = false;
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile("^\\d{1,8}(\\.\\d{1,2})?$");
        matcher = pattern.matcher(answer);
        patternMatched = matcher.matches();
        return patternMatched;
    }

    /**
     * method to validate a policy number
     *
     * @param policyNumber
     *            policyNumber
     * @return boolean result
     */
    public static boolean isValidPolicyNumber(final String policyNumber) {
        boolean patternMatched = false;
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile("^[ A-Za-z0-9/]*$");
        matcher = pattern.matcher(policyNumber);
        patternMatched = matcher.matches();
        return patternMatched;
    }

    /**
     * Util method to get empty String
     *
     * @return - empty String ""
     */
    public static String getEmptyString() {
        return StringUtils.EMPTY;
    }
    
    /**
     * Util method to get empty StringBuiler
     *
     * @return - empty String ""
     */
    public static StringBuilder getEmptyStringBuilder() {
        return new StringBuilder("");
    }

    /**
     * Util method to check if string is not empty
     *
     * @param str
     *            - input string
     * @return - true if string not empty
     */
    public static boolean stringNotEmptyCheck(final String str) {
        return StringUtils.isNotEmpty(str);
    }

    /**
     * Util method to check if string is not blank
     *
     * @param str
     *            - input string
     * @return - true if string not blank
     */
    public static boolean stringNotBlankCheck(final String str) {
        return StringUtils.isNotBlank(str);
    }

    /**
     * Method to append suffix to date for Personal Details page
     *
     * @param validSuffix
     *            - validSuffix received after checking day
     * @param formattedDate
     *            - Date of birth in yyyymmdd format
     * @return - Date of birth with suffix e.g 21st
     */
    public static StringBuffer suffixValueInDOB(final String validSuffix, final String formattedDate) {
        final StringBuffer dobWithSuffix = new StringBuffer();
        try {
            final String[] split = formattedDate.split(BLANK);
            if (split.length >= THREE) {
                dobWithSuffix.append(split[0].concat(validSuffix)).append(BLANK).append(split[1]).append(BLANK)
                        .append(split[2]);
            } else {
                dobWithSuffix.append(INVALID);
            }
        } catch (final IndexOutOfBoundsException index) {
            dobWithSuffix.append(INVALID);
        } catch (final Exception e) {
            dobWithSuffix.append(INVALID);
        }
        return dobWithSuffix;
    }

    /**
     * Check if object is null
     *
     * @param inputDate
     *            is validk
     * @return isValidDate
     */
    public static boolean isValidDate(final String inputDate) {
        SimpleDateFormat dateFormat;
        dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        dateFormat.setLenient(false);
        boolean isValidDate = false;
        try {
            dateFormat.parse(inputDate.trim());
            isValidDate = true;
        } catch (final ParseException pe) {
            isValidDate = false;
        }
        return isValidDate;
    }

    /**
     * Check if contact type is selected or not
     *
     * @param str
     *            - selected contact type
     * @return - isSelected - If contactType = 'Select' or contactType = empty
     *         then return false else return true
     */
    public static Boolean isContactTypeSelected(final String str) {
        Boolean isSelected = false;
        String type;
        type = TYPE;
        if (!str.trim().isEmpty() && !str.equalsIgnoreCase(type)) {
            isSelected = true;
        }
        return isSelected;
    }

    /**
     * Check if phone number is valid or not
     *
     * @param telephoneNumber
     *            - telephoneNumber
     * @param contactType
     *            - contactType
     * @return - patternMatched -If pattern is matched for selected contact type
     *         returns true else returns false.
     */
    public static boolean isValidTelephone(final String telephoneNumber, final String contactType) {
        boolean patternMatched = false;
        Pattern pattern;
        Pattern pattern1;
        Matcher matcher;
        Matcher matcher1;

        if (null != contactType && "MOBILE".equalsIgnoreCase(contactType)) {
            pattern = Pattern.compile(MOBILE_PATTERN);
            matcher = pattern.matcher(telephoneNumber);
            patternMatched = matcher.matches();

        } else if (null != contactType && ("HOME".equalsIgnoreCase(contactType) || "WORK".equalsIgnoreCase(contactType))) {
            pattern = Pattern.compile(HOMEWORK_PATTERN_01);
            pattern1 = Pattern.compile(HOMEWORK_PATTERN_02);
            matcher = pattern.matcher(telephoneNumber);
            matcher1 = pattern1.matcher(telephoneNumber);
            patternMatched = matcher.matches() || matcher1.matches();
        }

        return patternMatched;
    }

    /**
     * Check if contact details are valid - it checks selected contactType and
     * phone pattern
     *
     * @param telephoneNumber
     *            -telephoneNumber
     *
     * @param contactType
     *            - contactType
     * @param xGUID
     *            -xGUID
     * @return -setValidFlag -if ContactType and telephoneNumber combination
     *         validates to true it will return true else false.
     *
     */
    public static boolean isValidContactDetails(final String telephoneNumber, final String contactType, final String xGUID) {
        boolean setValidFlag = false;
        String selectedType;
        selectedType = TYPE;
        if ((contactType.equalsIgnoreCase(selectedType) || contactType.trim().isEmpty())
                && telephoneNumber.trim().isEmpty()) {
            LOGGER.debug("{} Both Contact type and telephoneNumber are empty");
            setValidFlag = true;
        } else if (CommonUtils.isContactTypeSelected(contactType)) {
            LOGGER.debug("{} When contact type is selected");
            setValidFlag = CommonUtils.isValidTelephone(telephoneNumber, contactType);

        } else if (!telephoneNumber.trim().isEmpty()) {
            if (CommonUtils.isContactTypeSelected(contactType)) {
                setValidFlag = CommonUtils.isValidTelephone(telephoneNumber, contactType);
                LOGGER.debug("{} When telephoneNumber is not empty");
            } else {
                setValidFlag = false;
                LOGGER.debug("{} Contact type is not selected");
            }
        } else {
            setValidFlag = false;
            LOGGER.debug("{} phone number is empty");
        }
        return setValidFlag;

    }

    /**
     * To generate unique name for retirement pack pdf
     *
     * @param policyNumber
     *            - policy number of the user
     * @param bancsUniqueCustomerNumber
     *            - Bancs unique customer number
     * @param fileType
     *            - fileType pdf
     * @return String
     */
    public static String generateUniqueFileName(String policyNumber, String bancsUniqueCustomerNumber,
            final String fileType) {
        final StringBuffer fileName = new StringBuffer();
        final Calendar cal = Calendar.getInstance();
        final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmss");
        final String strDate = sdf.format(cal.getTime());
        if (CommonUtils.isobjectNotNull(policyNumber)) {
            policyNumber = policyNumber.replaceAll("\\\\", "-").trim().replaceAll("/", "-").trim();
            bancsUniqueCustomerNumber = bancsUniqueCustomerNumber.replaceAll("\\\\", "-").trim().replaceAll("/", "-").trim();
            fileName.append(fileType).append("_").append(policyNumber).append("_").append(bancsUniqueCustomerNumber)
                    .append("_").append(strDate);
        }
        return fileName.toString();
    }

    /**
     * Method to split mobile phone number in STD code and local phone number
     *
     * @param phoneNumber
     *            -phoneNumber
     * @return String[]
     */
    public static String[] splitMobile(final StringBuffer phoneNumber) {
        String[] phoneArr;
        final int phoneLength = phoneNumber.length();
        String stdCode = CommonUtils.getEmptyString();
        String localNumber = CommonUtils.getEmptyString();
        phoneArr = new String[2];
        switch (phoneLength) {
        case ELEVEN:
            final String char2 = phoneNumber.substring(MPLConstants.ZERO, MPLConstants.TWO);
            if ("07".equals(char2)) {
                stdCode = phoneNumber.substring(MPLConstants.ZERO, MPLConstants.FIVE);
                localNumber = phoneNumber.substring(MPLConstants.FIVE, phoneLength);
            } else {
                LOGGER.debug("Not valid 07 number");
            }
            break;
        case THIRTEEN:
            final String char4 = phoneNumber.substring(MPLConstants.ZERO, MPLConstants.FOUR);
            LOGGER.info("first four ", char4);
            if ("+447".equals(char4)) {
                LOGGER.info("Replace +44 with 0: ", phoneNumber.replace(MPLConstants.ZERO, MPLConstants.THREE, ZERO));
                stdCode = phoneNumber.substring(MPLConstants.ZERO, MPLConstants.FIVE);
                localNumber = phoneNumber.substring(MPLConstants.FIVE, phoneNumber.length());
            } else {
                LOGGER.debug("not valid +447");
            }
            break;
        case FOURTEEN:
            final String char5 = phoneNumber.substring(MPLConstants.ZERO, MPLConstants.FIVE);
            LOGGER.info("first 5: ", char5);
            if ("00447".equals(char5)) {
                LOGGER.info("Replace 0044 with 0: ", phoneNumber.replace(MPLConstants.ZERO, MPLConstants.FOUR, ZERO));
                stdCode = phoneNumber.substring(MPLConstants.ZERO, MPLConstants.FIVE);
                localNumber = phoneNumber.substring(MPLConstants.FIVE, phoneNumber.length());
            } else {
                LOGGER.debug("not valid 00447 number");
            }
            break;
        default:
            LOGGER.info("Error in phone number");
            break;
        }

        phoneArr[0] = stdCode;
        phoneArr[1] = localNumber;

        return phoneArr;
    }

    /**
     * Split home or work number into STD code and local phone number
     *
     * @param phoneNumber
     *            -phoneNumber
     * @return String[]
     */
    public static String[] splitHomeOrWork(final StringBuffer phoneNumber) {

        String[] phoneArr;
        String[] phoneArrWithZeroOne;
        final int phoneLength = phoneNumber.length();
        String stdCode = CommonUtils.getEmptyString();

        String localHomeNumber = CommonUtils.getEmptyString();

        String firstTwo;

        phoneArr = new String[2];
        firstTwo = phoneNumber.substring(0, 2);

        switch (firstTwo) {
        case "02":
            stdCode = phoneNumber.substring(MPLConstants.ZERO, MPLConstants.THREE);
            localHomeNumber = phoneNumber.substring(MPLConstants.THREE, phoneLength);
            break;
        case "01":
            switch (phoneLength) {
            case MPLConstants.TEN:
                stdCode = phoneNumber.substring(MPLConstants.ZERO, MPLConstants.FIVE);
                localHomeNumber = phoneNumber.substring(MPLConstants.FIVE, phoneLength);
                break;
            case ELEVEN:
                phoneArrWithZeroOne = getStdCodeZeroOne(phoneNumber, phoneLength);
                stdCode = phoneArrWithZeroOne[0];
                localHomeNumber = phoneArrWithZeroOne[1];
                break;
            default:
                LOGGER.info("inside else");
                break;
            }
            break;
        default:
            LOGGER.info("Error in phonenumber");
        }

        phoneArr[0] = stdCode;
        phoneArr[1] = localHomeNumber;

        return phoneArr;

    }

    /**
     * get std code when user enter 01XXXXXXXXX - 11 digit number
     *
     * @param phoneNumber
     *            phonenumber
     * @param phoneLength
     *            phone length
     * @return String[]
     */
    public static String[] getStdCodeZeroOne(final StringBuffer phoneNumber, final int phoneLength) {
        String stdCode;
        String localHomeNumber;
        String[] phoneArr;

        final String char3 = phoneNumber.substring(0, 3);
        phoneArr = new String[2];
        LOGGER.info("first three ", char3);

        if (phoneNumber.substring(MPLConstants.ZERO, MPLConstants.THREE).equals("011")) {
            stdCode = phoneNumber.substring(MPLConstants.ZERO, MPLConstants.FOUR);
            localHomeNumber = phoneNumber.substring(MPLConstants.FOUR, phoneLength);

        } else if (phoneNumber.substring(MPLConstants.THREE, MPLConstants.FOUR).equals("1")) {
            stdCode = phoneNumber.substring(MPLConstants.ZERO, MPLConstants.FOUR);
            localHomeNumber = phoneNumber.substring(MPLConstants.FOUR, phoneLength);

        } else {
            stdCode = phoneNumber.substring(MPLConstants.ZERO, MPLConstants.FIVE);
            localHomeNumber = phoneNumber.substring(MPLConstants.FIVE, phoneLength);

        }

        phoneArr[0] = stdCode;
        phoneArr[1] = localHomeNumber;

        return phoneArr;
    }

    /**
     * Added this method to allow & < and > symbol in pdf template process
     *
     * @param toReplace
     *            toReplaceString
     * @return String
     */
    public static String replaceEscapeCharacters(final String toReplace) {
        String replaced = toReplace;
        if (toReplace != null
                && (toReplace.contains(MPLConstants.AMPSIGN) || toReplace.contains(MPLConstants.LTSIGN)
                        || toReplace.contains(MPLConstants.GRTSIGN))) {
            replaced = toReplace.replaceAll(MPLConstants.AMPSIGN, "&amp;").replaceAll(MPLConstants.LTSIGN, "&lt;")
                    .replaceAll(MPLConstants.GRTSIGN, "&gt;");
        }
        return replaced;
    }

    /**
     * Method to get runtime value
     *
     * @param batchFlag
     *            - batchFlag
     * @return - batch or real_time
     */
    public static String batchOrRealTime(final boolean batchFlag) {

        String runtime;

        if (batchFlag) {
            runtime = MPLConstants.BATCH;
        } else {
            runtime = MPLConstants.REAL_TIME;
        }

        return runtime;
    }

    /**
     * Method to check if string is blank
     *
     * @param input
     *            - string
     * @return - {@link Boolean}
     */
    public static Boolean stringNotBlank(final String input) {
        return StringUtils.isNotBlank(input);
    }

    /**
     * Method to append suffix to date for Personal Details page
     *
     * @param day
     *            - day received from bancs
     * @param formattedDate
     *            - Date of birth in yyyymmdd format
     * @return - Date of birth with suffix e.g 21st
     */
    public static StringBuffer suffixDateFormat(final int day, final String formattedDate) {
        String validSuffix;

        switch (day) {
        case ONE:
        case TWENTYONE:
        case THRTYONE:
            validSuffix = "st";
            break;
        case TWO:
        case TWENTYTWO:
            validSuffix = "nd";
            break;
        case THREE:
        case TWENTYTHREE:
            validSuffix = "rd";
            break;
        default:
            validSuffix = "th";
        }
        return suffixValueInDOB(validSuffix, formattedDate);
    }

/*public static AddressDTO getPreferredAddress(final PartyCommDetailsDTO partyCommDetails){
    	
    	AddressDTO addressDTO = null;
		 for(AddressDTO address  : partyCommDetails.getAddressList()){
			 if(address.getPreferredFlg()!=null && address.getPreferredFlg().equalsIgnoreCase("Y")){
				 addressDTO = address;
			 }
		 }
		 return addressDTO;
    }
    
    public static CommunicationDTO getPreferredContact(final PartyCommDetailsDTO partyCommDetails){
    	CommunicationDTO communicationDTO = null;
		 for(CommunicationDTO communication  : partyCommDetails.getCommChnlList()){
			 if(communication.getCommChnlPreferredFlg()!=null && communication.getCommChnlPreferredFlg().equalsIgnoreCase("Y")){
				 communicationDTO = communication;
			 }
		 }
		 return communicationDTO;
    } */
	
	/**
     * Active Flag check list for policies
     * 
     * @return list of active flags
     */
    public static List<String> fetchInActiveFlagList() {
        final List<String> listOfFlag = new ArrayList<>();
        listOfFlag.add("INNPP");
        listOfFlag.add("WVRA");
        listOfFlag.add("RLPU");
        listOfFlag.add("INF");
        listOfFlag.add("PUP");
        listOfFlag.add("CNTRH");
        listOfFlag.add("RESS");
        listOfFlag.add("WVRI");
        listOfFlag.add("RRPPI");
        listOfFlag.add("RRPPA");
        listOfFlag.add("CBR");
        return listOfFlag;
    }
}
