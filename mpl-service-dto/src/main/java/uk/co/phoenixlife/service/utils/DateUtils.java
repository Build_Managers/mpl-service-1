/*
 * DateUtils.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.utils;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * DateUtils.java
 */
public final class DateUtils {

    /** The date format. */
    public static final String DATE_FORMAT = "yyyy-MM-dd";

    /** ONE */
    public static final int ONE = 1;
    private static final Logger LOGGER = LoggerFactory.getLogger(DateUtils.class);

    /**
     * Default Constructor
     */
    private DateUtils() {
        super();
    }

    /**
     * Method to get current LocalDateTime
     *
     * @return - {@link LocalDateTime}
     */
    public static LocalDateTime getCurrentLocalDateTime() {
        return LocalDateTime.now();
    }

    /**
     * Gets the date from string.
     *
     * @param stringDate
     *            the input date
     * @return the date
     */
    public static LocalDate getDateFromString(final String stringDate) {

        return getDateFromString(stringDate, DATE_FORMAT);
    }

    /**
     * Gets the date from string.
     *
     * @param stringDate
     *            the input date
     * @param format
     *            the format of the date
     * @return the date
     */
    public static LocalDate getDateFromString(final String stringDate, final String format) {

        LOGGER.debug("Inside getDateFromString {}", format);
        LocalDate returnDate = null;
        DateTimeFormatter formatter;
        formatter = DateTimeFormatter.ofPattern(format);

        if (StringUtils.isNotBlank(stringDate)) {
            returnDate = LocalDate.parse(stringDate, formatter);
        }
        LOGGER.debug("Return returnDate {}", format);
        return returnDate;
    }

    /**
     * Get 1 year before date from todays date
     *
     * @param date
     *            date
     * @return date
     */
    public static LocalDate calDateForOneYrBefore(final LocalDate date) {
        LocalDate oneYearBefore;
        oneYearBefore = date.minus(ONE, ChronoUnit.YEARS);
        return oneYearBefore;
    }

    /**
     * Get 1 year after date from input date
     *
     * @param date
     *            date
     * @return date
     */
    public static LocalDate calDateForOneYrAfter(final LocalDate date) {
        LocalDate oneYearAfter;
        oneYearAfter = date.plus(ONE, ChronoUnit.YEARS);
        return oneYearAfter;
    }

    public static boolean dobValidYearRange(final LocalDate date) {

        return (date.isBefore(DateUtils.calDateForFiftyFiveYrBefore(LocalDate.now()))
                || date.equals(DateUtils.calDateForFiftyFiveYrBefore(LocalDate.now())))
                && (date.isAfter(DateUtils.calDateForSeventyFiveYrBefore(LocalDate.now()))
                        || date.equals(DateUtils.calDateForSeventyFiveYrBefore(LocalDate.now())));

    }

    public static LocalDate calDateForFiftyFiveYrBefore(final LocalDate date) {
        LocalDate fiftyFiveYearBefore;
        fiftyFiveYearBefore = date.minus(55, ChronoUnit.YEARS);
        return fiftyFiveYearBefore;
    }

    public static LocalDate calDateForSeventyFiveYrBefore(final LocalDate date) {
        LocalDate seventyFiveYearBefore;
        seventyFiveYearBefore = date.minus(75, ChronoUnit.YEARS);
        return seventyFiveYearBefore;
    }

    /**
     * Method to convert LocalDate to given format
     *
     * @param localDate
     *            - {@link LocalDate}
     * @param format
     *            - String format
     * @return - date as String
     */
    public static String formatDate(final LocalDate localDate, final String format) {

        DateTimeFormatter formatter;
        String dateString;

        formatter = DateTimeFormatter.ofPattern(format);
        dateString = localDate.format(formatter);

        return dateString;
    }

    /**
     * Method to convert LocalDate to given format
     *
     * @param localDateTime
     *            - {@link LocalDate}
     * @param format
     *            - String format
     * @return - date as String
     */
    public static String formatDateTime(final LocalDateTime localDateTime, final String format) {

        DateTimeFormatter formatter;
        String dateString;

        formatter = DateTimeFormatter.ofPattern(format);
        dateString = localDateTime.format(formatter);

        return dateString;
    }

    /**
     * Method to convert Timestamp to given format
     *
     * @param timestamp
     *            - {@link Timestamp}
     * @param format
     *            - String format
     * @return - date as String
     */
    public static String formatTimestamp(final Timestamp timestamp, final String format) {

        DateTimeFormatter formatter;
        String dateString;

        formatter = DateTimeFormatter.ofPattern(format);
        dateString = formatDateTime(timestamp).format(formatter);

        return dateString;
    }

    /**
     * Method to convert Date to LocalDate
     *
     * @param inputDate
     *            - {@link Date}
     * @return - {@link LocalDate}
     */
    public static LocalDate convertToLocalDate(final Date inputDate) {
        return inputDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    /**
     * Method to get current timestamp
     *
     * @return - java.sql.Timestamp
     */
    public static Timestamp getCurrentTimestamp() {
        return Timestamp.valueOf(LocalDateTime.now());
    }

    /**
     * Method to get current DateTime
     *
     * @return - {@link LocalDateTime}
     */
    public static LocalDateTime getCurrentDateTime() {
        return LocalDateTime.now();
    }

    /**
     * Method to get current Date
     *
     * @return - {@link LocalDate}
     */
    public static LocalDate getCurrentDate() {
        return LocalDate.now();
    }

    /**
     * Method to convert LocalDate to java.sql.Date
     *
     * @param date
     *            - {@link LocalDate}
     * @return - java.sql.Date
     */
    public static java.sql.Date convertToSqlDate(final LocalDate date) {
        return java.sql.Date.valueOf(date);
    }

    /**
     * Method to convert java.sql.Date to LocalDate
     *
     * @param date
     *            - java.sql.Date
     * @return - {@link LocalDate}
     */
    public static LocalDate convertToLocalDate(final java.sql.Date date) {
        return date.toLocalDate();
    }

    /**
     * Method to convert LocalDateTime to Timestamp
     *
     * @param localDateTime
     *            - {@link LocalDateTime}
     * @return - {@link Timestamp}
     */
    public static Timestamp convertToTimestamp(final LocalDateTime localDateTime) {
        return Timestamp.valueOf(localDateTime);
    }

    /**
     * Method to get current year
     *
     * @return - present year
     */
    public static int getCurrentYear() {
        Calendar cal;
        cal = Calendar.getInstance();
        return cal.get(Calendar.YEAR);
    }

    /**
     * Method to convert date into localdate
     *
     * @param date
     *            inputdate
     * @return localdate
     */
    public static LocalDate convertDate(final Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    /**
     * Method to compare 2 date of type LocalDate
     *
     * @param localDate1
     *            date1
     * @param localDate2
     *            date2
     * @return boolean result
     */
    public static Boolean compareLocalDate(final LocalDate localDate1, final LocalDate localDate2) {
        return localDate1.isEqual(localDate2);
    }

    /**
     * Method to convert LocalDateTime to Timestamp
     *
     * @param localDateTime
     *            - {@link LocalDateTime}
     * @return - {@link Timestamp}
     */
    public static Timestamp formatDateTime(final LocalDateTime localDateTime) {
        return Timestamp.valueOf(localDateTime);
    }

    /**
     * Method to convert Timestamp to LocalDateTime
     *
     * @param timestamp
     *            - {@link Timestamp}
     * @return - {@link LocalDateTime}
     */
    public static LocalDateTime formatDateTime(final Timestamp timestamp) {
        return timestamp.toLocalDateTime();
    }

}
