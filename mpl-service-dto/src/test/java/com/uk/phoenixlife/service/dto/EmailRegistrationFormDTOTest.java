package com.uk.phoenixlife.service.dto;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.EmailRegistrationFormDTO;

public class EmailRegistrationFormDTOTest {
	
	@Test (enabled = true)
	
    public void testEmailRegistrationSuccess(){
		EmailRegistrationFormDTO emailRegistrationFormDTO = new EmailRegistrationFormDTO();
		emailRegistrationFormDTO.setEmail("abc@xyz.com");
		emailRegistrationFormDTO.setConfirmEmail("abc@xyz.com");
		
		Assert.assertEquals("abc@xyz.com", emailRegistrationFormDTO.getEmail());
		Assert.assertEquals("abc@xyz.com", emailRegistrationFormDTO.getConfirmEmail());
	}

}
