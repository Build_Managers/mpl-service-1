package com.uk.phoenixlife.service.dto;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.LoginSecurityQuestionDTO;

public class LoginSecurityQuestionDTOTest {
	
	@Test (enabled = true)
	
	public void testLoginSecurityQuestionSuccess(){
		LoginSecurityQuestionDTO loginSecurityQuestionDTO = new LoginSecurityQuestionDTO();
		 loginSecurityQuestionDTO.setAnswer("ABCD");
		 
		 Assert.assertEquals("ABCD", loginSecurityQuestionDTO.getAnswer());
	}

}
