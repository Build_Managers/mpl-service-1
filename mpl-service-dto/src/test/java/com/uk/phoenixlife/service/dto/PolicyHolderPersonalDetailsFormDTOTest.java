package com.uk.phoenixlife.service.dto;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.PolicyHolderPersonalDetailsFormDTO;

public class PolicyHolderPersonalDetailsFormDTOTest {
	 
	@Test (enabled = true)
	 public void testPolicyHolderPersonalDetailsSuccess(){
		PolicyHolderPersonalDetailsFormDTO policyHolderPersonalDetailsDTO = new PolicyHolderPersonalDetailsFormDTO();
		policyHolderPersonalDetailsDTO.setFullName("Richard Milsum");
		policyHolderPersonalDetailsDTO.setDateOfBirth("19840204");
		policyHolderPersonalDetailsDTO.setNiNo("PQR12345");
		policyHolderPersonalDetailsDTO.setPolicyHolderAddress("United Kingdom");
		policyHolderPersonalDetailsDTO.setOldPassword("PWD123");
		policyHolderPersonalDetailsDTO.setNewPassword("PWD321");
		policyHolderPersonalDetailsDTO.setConfirmNewPassword("PWD321");
		policyHolderPersonalDetailsDTO.setPrimaryPhone("0712345");
		policyHolderPersonalDetailsDTO.setEmail("abc@xyz.com");
		
		Assert.assertEquals("Richard Milsum", policyHolderPersonalDetailsDTO.getFullName());
		Assert.assertEquals("19840204", policyHolderPersonalDetailsDTO.getDateOfBirth());
		Assert.assertEquals("PQR12345", policyHolderPersonalDetailsDTO.getNiNo());
		Assert.assertEquals("United Kingdom", policyHolderPersonalDetailsDTO.getPolicyHolderAddress());
		Assert.assertEquals("PWD123", policyHolderPersonalDetailsDTO.getOldPassword());
		Assert.assertEquals("PWD321", policyHolderPersonalDetailsDTO.getNewPassword());
		Assert.assertEquals("PWD321", policyHolderPersonalDetailsDTO.getConfirmNewPassword());
		Assert.assertEquals("0712345", policyHolderPersonalDetailsDTO.getPrimaryPhone());
		Assert.assertEquals("abc@xyz.com",policyHolderPersonalDetailsDTO.getEmail());
	}

}
