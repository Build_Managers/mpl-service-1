package com.uk.phoenixlife.service.dto;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.UserLoginFormDTO;

public class UserLoginFormDTOTest {

	 @Test (enabled = true)
	 public void testUserLoginFormSuccess(){
		 
		UserLoginFormDTO userLoginFormDTO =  new UserLoginFormDTO();
		userLoginFormDTO.setUsername("RICHMIL");
		userLoginFormDTO.setPassword("PWD1234");
		
		Assert.assertEquals("RICHMIL", userLoginFormDTO.getUsername());
		Assert.assertEquals("PWD1234", userLoginFormDTO.getPassword());
	 }
}
