package com.uk.phoenixlife.service.dto;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.UserRegistrationFormDTO;

public class UserRegistrationFormDTOTest {
	 
	 @Test (enabled = true)
	 public void testUserRegistrationFormSuccess(){
		 UserRegistrationFormDTO userRegistrationFormDTO = new UserRegistrationFormDTO();
		 userRegistrationFormDTO.setUsername("RICHMIL");
		 userRegistrationFormDTO.setPassword("PWD12345");
		 userRegistrationFormDTO.setConfirmPassword("PWD12345");
		 userRegistrationFormDTO.setEmail("abc@xyz.com");
		 
		 
		 Assert.assertEquals("RICHMIL", userRegistrationFormDTO.getUsername());
		 Assert.assertEquals("PWD12345", userRegistrationFormDTO.getPassword());
		 Assert.assertEquals("PWD12345", userRegistrationFormDTO.getConfirmPassword());
		 Assert.assertEquals("abc@xyz.com", userRegistrationFormDTO.getEmail());
	 }

}
