package uk.co.phoenixlife.service.dto.activation;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

public class CustomerDetailsTest {

	@Test(enabled = true)
	public void testCustomerDetailsSuccess(){
		List<CustomerPersonalDetailStatus> customerPersonalDetailsStatus = new ArrayList<CustomerPersonalDetailStatus>();
		CustomerDetails customerDetails = new CustomerDetails();
		customerDetails.setCustomerId(100L);
		customerDetails.setIdentifiedBancsUniqueCustomerNumber("ABC12345");
		customerDetails.setPolicyNumber("12345");
		customerDetails.setPostCodeFlag(false);
		customerDetails.setCustomerPersonalDetailStatusList(customerPersonalDetailsStatus);
		customerDetails.setPostCode("PV1 26G");
		customerDetails.setPolicyId(123L);
		
		Assert.assertNotEquals("customerPersonalDetailsStatus",customerDetails.getCustomerPersonalDetailStatusList());
		Assert.assertEquals("12345",customerDetails.getPolicyNumber());
		Assert.assertEquals("PV1 26G", customerDetails.getPostCode());
		Assert.assertEquals("ABC12345", customerDetails.getIdentifiedBancsUniqueCustomerNumber());
		Assert.assertNotNull(customerDetails.getCustomerId());
		Assert.assertNotNull(customerDetails.getPolicyId());
		Assert.assertFalse(customerDetails.getPostCodeFlag());
		
	}
}
