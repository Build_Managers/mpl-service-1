package uk.co.phoenixlife.service.dto.activation;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

public class CustomerIdentifierDTOTest {
	
	@Test(enabled = true)
	public void testCustomerIdentifierSuccess(){
		List<String> listOfBancsPolNum = new  ArrayList<String>();
		CustomerIdentifierDTO customerIdentifierDTO = new CustomerIdentifierDTO();
		customerIdentifierDTO.setUserName("RICHMIL");
		customerIdentifierDTO.setPolNum("GB19991A");
		customerIdentifierDTO.setBancsUniqueCustNo("19995");
		customerIdentifierDTO.setListOfBancsPolNum(listOfBancsPolNum);
		customerIdentifierDTO.setEmailId("abc@xyz.com");
		customerIdentifierDTO.setxGUID("789Xyz123");
		customerIdentifierDTO.setFirstName("RICHARD");
		customerIdentifierDTO.setLastName("MILSUM");
		customerIdentifierDTO.setTitle("Mr.");
		
		Assert.assertEquals("RICHMIL", customerIdentifierDTO.getUserName());
		Assert.assertEquals("GB19991A", customerIdentifierDTO.getPolNum());
		Assert.assertEquals("19995", customerIdentifierDTO.getBancsUniqueCustNo());
		Assert.assertEquals("abc@xyz.com", customerIdentifierDTO.getEmailId());
		Assert.assertEquals("789Xyz123", customerIdentifierDTO.getxGUID());
		Assert.assertEquals("RICHARD", customerIdentifierDTO.getFirstName());
		Assert.assertEquals("MILSUM", customerIdentifierDTO.getLastName());
		Assert.assertEquals("Mr.", customerIdentifierDTO.getTitle());
		Assert.assertNotNull(customerIdentifierDTO.getListOfBancsPolNum());
		
	}

}
