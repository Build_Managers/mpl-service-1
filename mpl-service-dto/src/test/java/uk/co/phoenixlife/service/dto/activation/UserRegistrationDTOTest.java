package uk.co.phoenixlife.service.dto.activation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.testng.Assert;
import org.testng.annotations.Test;

public class UserRegistrationDTOTest {
	
	 @Test(enabled = true)
	 public void testUserRegistrationSuccess(){
		 Set<String> alreadyTakenUserNameSet = new HashSet<String>();
		 List<String> suggestedUserNameList = new ArrayList<String>();
		 UserRegistrationDTO userRegistrationDTO = new UserRegistrationDTO();
		 userRegistrationDTO.setUserName("RICHMIL");
		 userRegistrationDTO.setUserNameExistFlag(false);
		 userRegistrationDTO.setAlreadyTakenUserNameSet(alreadyTakenUserNameSet);
		 userRegistrationDTO.setSuggestedUserNameList(suggestedUserNameList);
		 
		 Assert.assertEquals("RICHMIL", userRegistrationDTO.getUserName());
		 Assert.assertFalse(userRegistrationDTO.isUserNameExistFlag());
		 Assert.assertNotEquals("alreadyTakenUserNameSet", userRegistrationDTO.getAlreadyTakenUserNameSet());
		 Assert.assertNotEquals("suggestedUserNameList", userRegistrationDTO.getSuggestedUserNameList());
	 }

}
