package uk.co.phoenixlife.service.dto.activation;

import org.testng.Assert;
import org.testng.annotations.Test;

public class VerificationDTOTest {
   @Test (enabled = true)
   public void testVerificationDTOSuccess(){
	  VerificationDTO verificationDTO = new VerificationDTO();
	  verificationDTO.setFundName("ABC");
	  verificationDTO.setNiNo("PQR1234");
	  verificationDTO.setNetinitPremium("25");
	  verificationDTO.setBenefitName("XYZ");
	  verificationDTO.setQuestionKey("123");
	  verificationDTO.setQuestion("Whats your first pet name");
	  verificationDTO.setAnswer("pappy");
	  verificationDTO.setQuestionLabel("PQR");
	  
	  Assert.assertEquals("ABC",verificationDTO.getFundName());
	  Assert.assertEquals("PQR1234", verificationDTO.getNiNo());
	  Assert.assertEquals("25", verificationDTO.getNetinitPremium());
	  Assert.assertEquals("XYZ", verificationDTO.getBenefitName());
      Assert.assertEquals("123", verificationDTO.getQuestionKey());
      Assert.assertEquals("Whats your first pet name", verificationDTO.getQuestion());
      Assert.assertEquals("pappy", verificationDTO.getAnswer());
      Assert.assertEquals("PQR", verificationDTO.getQuestionLabel());
	   
   }
}
