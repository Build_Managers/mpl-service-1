package uk.co.phoenixlife.service.dto.activation;

import org.testng.Assert;
import org.testng.annotations.Test;

public class VerificationFormDTOTest {

	  @Test (enabled = true)
	  public void testVerificationDTOSuccess(){
		  VerificationFormDTO verificationFormDTO = new VerificationFormDTO();
		  verificationFormDTO.setQuestion("ABCD");
		  verificationFormDTO.setAnswer("XYZ");
		  verificationFormDTO.setAnswer1("PQR");
		  verificationFormDTO.setQuestionKey("12345");
		  verificationFormDTO.setQuestionLabel("A12");
		  verificationFormDTO.setQuestionLabelForAccountNumber("B10");
		  
		  Assert.assertEquals("ABCD", verificationFormDTO.getQuestion());
		  Assert.assertEquals("XYZ",verificationFormDTO.getAnswer());
		  Assert.assertEquals("PQR", verificationFormDTO.getAnswer1());
		  Assert.assertEquals("12345", verificationFormDTO.getQuestionKey());
		  Assert.assertEquals("A12",verificationFormDTO.getQuestionLabel());
		  Assert.assertEquals("B10", verificationFormDTO.getQuestionLabelForAccountNumber());
	  }
}
