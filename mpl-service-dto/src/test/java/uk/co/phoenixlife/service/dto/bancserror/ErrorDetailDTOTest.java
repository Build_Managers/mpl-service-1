package uk.co.phoenixlife.service.dto.bancserror;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ErrorDetailDTOTest {
	@Test (enabled = true)
	public void testErrorDetailSuccess(){
		ErrorDetailDTO errorDetailDTO = new ErrorDetailDTO();
		errorDetailDTO.setTitle("Mr.");
		errorDetailDTO.setInternalErrorCode("500");
		errorDetailDTO.setErrorDesciption("APIFailed");
		
		Assert.assertEquals("Mr.", errorDetailDTO.getTitle());
		Assert.assertEquals("500",errorDetailDTO.getInternalErrorCode());
		Assert.assertEquals("APIFailed",errorDetailDTO.getErrorDesciption());
	}

}
