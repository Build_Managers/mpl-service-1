package uk.co.phoenixlife.service.dto.bancserror;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ErrorResponseDTOTest {

	  @Test
	  public void ErrorResponseSuccess(){
		  ErrorResponseDTO errorResponseDTO = new ErrorResponseDTO();
		  ErrorDetailDTO errorDetail = new ErrorDetailDTO();
		  errorResponseDTO.setErrorDetail(errorDetail);
		  errorResponseDTO.toString();
		  
		  Assert.assertNotNull(errorResponseDTO.getErrorDetail());
	  }
}
