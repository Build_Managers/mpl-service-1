package uk.co.phoenixlife.service.dto.error;

import org.testng.Assert;
import org.testng.annotations.Test;


public class MPLServiceErrorDTOTest {
	@Test (enabled=true)
	public void mplServiceErrorDTOSuccess() {
		MPLServiceErrorDTO mplServiceErrorDTO = new MPLServiceErrorDTO();
		
		mplServiceErrorDTO.setErrorCode("EC123");
		mplServiceErrorDTO.setErrorDescription("ED123");
		mplServiceErrorDTO.setRequestUrl("RU123");
		
		Assert.assertEquals("EC123", mplServiceErrorDTO.getErrorCode());
		Assert.assertEquals("ED123", mplServiceErrorDTO.getErrorDescription());
		Assert.assertEquals("RU123", mplServiceErrorDTO.getRequestUrl());
	}
}
