package uk.co.phoenixlife.service.dto.pdf;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.BankDetailsNinoDTO;
import uk.co.phoenixlife.service.dto.policy.FinalDashboardDTO;
import uk.co.phoenixlife.service.dto.policy.PartyPolicyDTO;

public class FormRequestDTOTest {
	@Test (enabled=true)
	public void formRequestDTOSuccess() {
		FormRequestDTO formRequestDTO = new FormRequestDTO();
		FinalDashboardDTO finalDashboard = new FinalDashboardDTO();
		BankDetailsNinoDTO bankDetailsNinoDTO = new BankDetailsNinoDTO();
		StringBuffer dobWithSuffix = new StringBuffer();
		PartyPolicyDTO partyPolicyDTO = new PartyPolicyDTO();
		formRequestDTO.setDocumentName("doc1");
		formRequestDTO.setPolicyId(9876L); 
		formRequestDTO.setPolNum("1234");
		formRequestDTO.setDocType("pdf");
		formRequestDTO.setFinalDashboard(finalDashboard);
		formRequestDTO.setIsSmallPot(false);
		formRequestDTO.setContactCustomer(false);
		formRequestDTO.setPensionGuidance(false);
		formRequestDTO.setPensionAdvise(false);
		formRequestDTO.setBankDetailsNinoDTO(bankDetailsNinoDTO);
		formRequestDTO.setpCLS("xyz");
		formRequestDTO.setTax("tax");
		formRequestDTO.setTotValue(999.0);
		formRequestDTO.setAmountPayable(100.0);
		formRequestDTO.setMvaValue(987.0);
		formRequestDTO.setPenaltyValue(9999.0);
		formRequestDTO.setEarlyExitCharge(10.0);
		formRequestDTO.setEstimatedPensionFund(6789.0);
		formRequestDTO.setMvrValue("456");
		formRequestDTO.setDobWithSuffix(dobWithSuffix);
		formRequestDTO.setPrimaryConactNum("9876");
		formRequestDTO.setSecConactNum("4567");
		formRequestDTO.setAdditonalConactNum("9999");
		formRequestDTO.setEecValue("abcd");
		formRequestDTO.setPartyPolicyDTO(partyPolicyDTO);
		
		Assert.assertEquals("doc1", formRequestDTO.getDocumentName());
		Assert.assertNotNull(formRequestDTO.getPolicyId());
		Assert.assertEquals("1234", formRequestDTO.getPolNum());
		Assert.assertEquals("pdf",formRequestDTO.getDocType());
		Assert.assertNotNull(formRequestDTO.getFinalDashboard());
		Assert.assertNotNull(formRequestDTO.getSerialversionuid());
		Assert.assertFalse(formRequestDTO.getIsSmallPot());
		Assert.assertFalse(formRequestDTO.getContactCustomer());
		Assert.assertFalse(formRequestDTO.getPensionGuidance());
		Assert.assertFalse(formRequestDTO.getPensionAdvise());
		Assert.assertNotNull(formRequestDTO.getBankDetailsNinoDTO());
		Assert.assertEquals("xyz", formRequestDTO.getpCLS());
		Assert.assertEquals("tax", formRequestDTO.getTax());
		Assert.assertEquals(999.0, formRequestDTO.getTotValue());
		Assert.assertEquals(100.0, formRequestDTO.getAmountPayable());
		Assert.assertEquals(987.0, formRequestDTO.getMvaValue());
		Assert.assertEquals(9999.0, formRequestDTO.getPenaltyValue());
		Assert.assertEquals(10.0, formRequestDTO.getEarlyExitCharge());
		Assert.assertEquals(6789.0, formRequestDTO.getEstimatedPensionFund());
		Assert.assertEquals("456", formRequestDTO.getMvrValue());
		Assert.assertNotNull(formRequestDTO.getDobWithSuffix());
		Assert.assertEquals("9876", formRequestDTO.getPrimaryConactNum());
		Assert.assertEquals("4567", formRequestDTO.getSecConactNum());
		Assert.assertEquals("9999", formRequestDTO.getAdditonalConactNum());
		Assert.assertEquals("abcd", formRequestDTO.getEecValue());
		Assert.assertNotNull(formRequestDTO.getPartyPolicyDTO());
	}
}
