package uk.co.phoenixlife.service.dto.pdf;

import org.testng.Assert;
import org.testng.annotations.Test;

public class FormResponseDTOTest {
	@Test (enabled=true)
	public void formResponseDTOTestSuccess() {
		FormResponseDTO formResponseDTO = new FormResponseDTO();
		formResponseDTO.setPdfAsHtml("pdf");
		formResponseDTO.setDocCount(10);
		formResponseDTO.setStatusFlag(false);
		
		Assert.assertEquals("pdf", formResponseDTO.getPdfAsHtml());
		Assert.assertEquals(10, formResponseDTO.getDocCount());
		Assert.assertFalse(formResponseDTO.isStatusFlag());
	}
}
 