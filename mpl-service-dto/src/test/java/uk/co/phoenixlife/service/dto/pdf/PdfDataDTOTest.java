package uk.co.phoenixlife.service.dto.pdf;

import org.testng.Assert;
import org.testng.annotations.Test;


public class PdfDataDTOTest {
	@Test (enabled=true)
	public void pdfDataDTOSuccess() {
		PdfDataDTO pdfDataDTO = new PdfDataDTO();
		pdfDataDTO.setPdfByte(null);
		pdfDataDTO.setPolicyId(123L);
		
		Assert.assertEquals(123L, pdfDataDTO.getPolicyId());
		Assert.assertEquals(null, pdfDataDTO.getPdfByte());
	}

}
