package uk.co.phoenixlife.service.dto.policy;


	import java.util.ArrayList;
	import java.util.List;

	import org.testng.Assert;
	import org.testng.annotations.Test;

	import uk.co.phoenixlife.service.dto.policy.BancsCashInTypeWrapper;
	import uk.co.phoenixlife.service.dto.policy.CashInTypeDTO;
	
	public class BancsCashInTypeWrapperTest {

		@Test (enabled=true)
		public void bancsCashInTypeWrapperSuccess() {
			List<CashInTypeDTO> listOfCashInTypes = new ArrayList<CashInTypeDTO>();
			BancsCashInTypeWrapper bancsCashInTypeWrapper = new BancsCashInTypeWrapper();
			bancsCashInTypeWrapper.setListOfCashInTypes(listOfCashInTypes);
			
			Assert.assertNotNull(bancsCashInTypeWrapper.getListOfCashInTypes());
		}
	}


