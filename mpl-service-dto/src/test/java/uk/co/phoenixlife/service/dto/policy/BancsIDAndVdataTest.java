package uk.co.phoenixlife.service.dto.policy;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.BancsIDAndVdata;


public class BancsIDAndVdataTest
{

	@Test (enabled=true)
	public void bancsIDAndVdataSuccess()
	{
		BancsIDAndVdata bancsIDAndVdata = new BancsIDAndVdata();
		
		bancsIDAndVdata.setPolNum("123");
		bancsIDAndVdata.setPartyType("abc");
		bancsIDAndVdata.setTitle("Mr");
		bancsIDAndVdata.setForename("abc");
		bancsIDAndVdata.setSurname("abc");
		bancsIDAndVdata.setDateOfBirth(new Date());
		bancsIDAndVdata.setHouseNo("123");
		bancsIDAndVdata.setHouseName("abc");
		bancsIDAndVdata.setAddress1("abc");
		bancsIDAndVdata.setAddress2("abc");
		bancsIDAndVdata.setCityTown("abc");
		bancsIDAndVdata.setCounty("abc");
		
		bancsIDAndVdata.setCounty("123");
		bancsIDAndVdata.setPostcode("abc");
		bancsIDAndVdata.setBancsUniqueCustNo(123);
		bancsIDAndVdata.setPoaFlag(true);
		bancsIDAndVdata.setFcuFlag(true);
		bancsIDAndVdata.setBankruptcyRefNo("123");
		bancsIDAndVdata.setBankruptcyDischargeDate(new Date());
		bancsIDAndVdata.setBankruptcyDate(new Date());
		bancsIDAndVdata.setCourtName("abc");
		bancsIDAndVdata.setCourtOrderDate(new Date());
		bancsIDAndVdata.setAddressStatus("abc");
		bancsIDAndVdata.setBancsPolNum("abc");
		bancsIDAndVdata.setPolStatus("123");
		bancsIDAndVdata.setPolStDate(new Date());
		bancsIDAndVdata.setPremFreq("123");
		bancsIDAndVdata.setPaymentMethod("abc");
		bancsIDAndVdata.setBenefitName("abc");
		bancsIDAndVdata.setNetInitPrem(new Double(10.20));
		bancsIDAndVdata.setNetModPrem(123);
		bancsIDAndVdata.setLastPremPaid(new Double(10.20));
		bancsIDAndVdata.setSortCode(123);
		bancsIDAndVdata.setBankAcct(23);
		bancsIDAndVdata.setPremiumDueDate(new Date());
		bancsIDAndVdata.setNiNo("abc");
		bancsIDAndVdata.setFundname("abc");
		bancsIDAndVdata.setBusTimeStamp(new Timestamp(0));
		List<String> pLst = new ArrayList<String>();
		pLst.add("abc");
		bancsIDAndVdata.setLst(pLst );
		bancsIDAndVdata.setSortCode(123);
		bancsIDAndVdata.setBankAcct(23);
		bancsIDAndVdata.setPremiumDueDate(new Date());
		bancsIDAndVdata.setNiNo("abc");
		bancsIDAndVdata.setFirst("abc");
		bancsIDAndVdata.setSecond("abc");
		bancsIDAndVdata.setThird("abc");
		bancsIDAndVdata.setFourth("abc");
		bancsIDAndVdata.setFifth("abc");
		bancsIDAndVdata.setSixth("abc");
		bancsIDAndVdata.toString();
		
		
		Assert.assertEquals("123",bancsIDAndVdata.getPolNum());
		Assert.assertNotNull(bancsIDAndVdata.getPartyType());
		Assert.assertEquals("Mr",bancsIDAndVdata.getTitle());
		Assert.assertNotNull(bancsIDAndVdata.getForename());
		Assert.assertEquals("abc",bancsIDAndVdata.getSurname());
		Assert.assertNotNull(bancsIDAndVdata.getDateOfBirth());
		Assert.assertNotNull(bancsIDAndVdata.getHouseNo());
		Assert.assertEquals("abc",bancsIDAndVdata.getHouseName());
		Assert.assertNotNull(bancsIDAndVdata.getAddress1());
		Assert.assertEquals("abc",bancsIDAndVdata.getAddress2());
		Assert.assertEquals("abc",bancsIDAndVdata.getCityTown());
		Assert.assertNotNull(bancsIDAndVdata.getCounty());
		Assert.assertEquals("abc", bancsIDAndVdata.getNiNo());
		Assert.assertEquals("abc", bancsIDAndVdata.getFundname());
		Assert.assertNotNull(bancsIDAndVdata.getBusTimeStamp());
		Assert.assertNotNull(bancsIDAndVdata.getLst());
		Assert.assertEquals("abc", bancsIDAndVdata.getFirst());
		Assert.assertEquals("abc", bancsIDAndVdata.getSecond());
		Assert.assertEquals("abc", bancsIDAndVdata.getThird());
		Assert.assertEquals("abc", bancsIDAndVdata.getFourth());
		Assert.assertEquals("abc", bancsIDAndVdata.getFifth());
		Assert.assertEquals("abc", bancsIDAndVdata.getSixth());
		
		
		Assert.assertEquals("abc",bancsIDAndVdata.getPostcode());
		Assert.assertNotNull(bancsIDAndVdata.getBancsUniqueCustNo());
		Assert.assertEquals("abc",bancsIDAndVdata.getPostcode());
		Assert.assertNotNull(bancsIDAndVdata.getPoaFlag());
		Assert.assertNotNull(bancsIDAndVdata.getFcuFlag());
		Assert.assertNotNull(bancsIDAndVdata.getBankruptcyRefNo());
		Assert.assertNotNull(bancsIDAndVdata.getBankruptcyDischargeDate());
		Assert.assertNotNull(bancsIDAndVdata.getBankruptcyDate());
		Assert.assertNotNull(bancsIDAndVdata.getCourtName());
		Assert.assertNotNull(bancsIDAndVdata.getCourtOrderDate());
		Assert.assertNotNull(bancsIDAndVdata.getAddressStatus());
		Assert.assertEquals("abc",bancsIDAndVdata.getBancsPolNum());
		Assert.assertNotNull(bancsIDAndVdata.getPolStatus());
		Assert.assertNotNull(bancsIDAndVdata.getPolStDate());
		Assert.assertNotNull(bancsIDAndVdata.getPremFreq());
		Assert.assertEquals("abc",bancsIDAndVdata.getPaymentMethod());
		Assert.assertNotNull(bancsIDAndVdata.getBenefitName());
		Assert.assertNotNull(bancsIDAndVdata.getNetInitPrem());
		Assert.assertNotNull(bancsIDAndVdata.getNetModPrem());
		Assert.assertNotNull(bancsIDAndVdata.getLastPremPaid());
		Assert.assertNotNull(bancsIDAndVdata.getPremFreq());
		Assert.assertNotNull(bancsIDAndVdata.getSortCode());
		Assert.assertNotNull(bancsIDAndVdata.getBankAcct());
		Assert.assertNotNull(bancsIDAndVdata.getPremiumDueDate());

		
	}

}
