package uk.co.phoenixlife.service.dto.policy;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.BancsPolicyExistsCheck;

public class BancsPolicyExistsCheckTest {
	@Test (enabled=true)
	public void bancsPolicyExistsCheckSuccess() {
		BancsPolicyExistsCheck bancsPolicyExistsCheck = new BancsPolicyExistsCheck();
		bancsPolicyExistsCheck.setBancsPolNum("bnum");
		bancsPolicyExistsCheck.setBancsProductCode("prod");
		bancsPolicyExistsCheck.setPolNum("num");
		bancsPolicyExistsCheck.setPolStatusCd("status");
		bancsPolicyExistsCheck.setPolStatusLit("lit");
		bancsPolicyExistsCheck.setPolStDate("date");
		
		Assert.assertEquals("bnum", bancsPolicyExistsCheck.getBancsPolNum());
		Assert.assertEquals("prod", bancsPolicyExistsCheck.getBancsProductCode());
		Assert.assertEquals("num", bancsPolicyExistsCheck.getPolNum());
		Assert.assertEquals("status", bancsPolicyExistsCheck.getPolStatusCd());
		Assert.assertEquals("lit", bancsPolicyExistsCheck.getPolStatusLit());
		Assert.assertEquals("date", bancsPolicyExistsCheck.getPolStDate());
	}

}
