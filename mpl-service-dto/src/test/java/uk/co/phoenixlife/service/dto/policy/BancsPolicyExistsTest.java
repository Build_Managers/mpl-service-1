package uk.co.phoenixlife.service.dto.policy;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.BancsPolicyExists;
import uk.co.phoenixlife.service.dto.policy.BancsPolicyExistsCheck;

public class BancsPolicyExistsTest {

	@Test (enabled=true)
	public void bancsPolicyExistsSuccess() {
		List<BancsPolicyExistsCheck> listOfPolicies = new ArrayList<BancsPolicyExistsCheck>();
		Timestamp timestamp = new Timestamp(12345L);
		BancsPolicyExists bancsPolicyExists = new BancsPolicyExists();
		bancsPolicyExists.setListOfPolicies(listOfPolicies);
		bancsPolicyExists.setBusTimeStamp(timestamp);
		
		Assert.assertNotNull(bancsPolicyExists.getListOfPolicies());
		Assert.assertNotEquals(1234L, bancsPolicyExists.getBusTimeStamp());
		
	}
}
