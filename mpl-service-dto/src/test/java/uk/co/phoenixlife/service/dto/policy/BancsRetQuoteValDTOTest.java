package uk.co.phoenixlife.service.dto.policy;

import java.sql.Timestamp;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.BancsRetQuoteValDTO;

public class BancsRetQuoteValDTOTest {

	@Test (enabled=true)
	public void bancsRetQuoteValDTOSuccess() {
		BancsRetQuoteValDTO bancsRetQuoteValDTO = new BancsRetQuoteValDTO();
		Timestamp pBusTimeStamp = new Timestamp(12345L);
		bancsRetQuoteValDTO.setBancsPolNum("num");
		bancsRetQuoteValDTO.setBusTimeStamp(pBusTimeStamp);
		bancsRetQuoteValDTO.setcUCharge(10.0);
		bancsRetQuoteValDTO.setMvaValue(20.0);
		bancsRetQuoteValDTO.setPenValue(30.0);
		bancsRetQuoteValDTO.setTotValue(40.0);
		bancsRetQuoteValDTO.toString();
		
		Assert.assertEquals("num", bancsRetQuoteValDTO.getBancsPolNum());
		Assert.assertNotEquals(123L, bancsRetQuoteValDTO.getBusTimeStamp());
		Assert.assertEquals(10.0, bancsRetQuoteValDTO.getcUCharge());
		Assert.assertEquals(20.0, bancsRetQuoteValDTO.getMvaValue());
		Assert.assertEquals(30.0, bancsRetQuoteValDTO.getPenValue());
		Assert.assertEquals(40.0, bancsRetQuoteValDTO.getTotValue());
		
	}
}