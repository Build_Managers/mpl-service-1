package uk.co.phoenixlife.service.dto.policy;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.BankDetailsNinoDTO;

public class BankDetailsNinoDTOTest {

	@Test (enabled=true)
	public void bankDetailsNinoDTOSuccess() {
		BankDetailsNinoDTO bankDetailsNinoDTO = new BankDetailsNinoDTO();
		bankDetailsNinoDTO.setAccountHolderName("abc");
		bankDetailsNinoDTO.setAccountNumber("123");
		bankDetailsNinoDTO.setBankOrBuildingSocietyName("xyz");
		bankDetailsNinoDTO.setNino("nino");
		bankDetailsNinoDTO.setRollNumber("roll");
		bankDetailsNinoDTO.setSortCode("sort");
		bankDetailsNinoDTO.toString();
		
		Assert.assertEquals("abc", bankDetailsNinoDTO.getAccountHolderName());
		Assert.assertEquals("123", bankDetailsNinoDTO.getAccountNumber());
		Assert.assertEquals("xyz", bankDetailsNinoDTO.getBankOrBuildingSocietyName());
		Assert.assertEquals("nino", bankDetailsNinoDTO.getNino());
		Assert.assertEquals("roll", bankDetailsNinoDTO.getRollNumber());
		Assert.assertEquals("sort", bankDetailsNinoDTO.getSortCode());
	}
}