package uk.co.phoenixlife.service.dto.policy;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.BenefitDTO;

	public class BenefitDTOTest{


	@Test (enabled=true)
	public void benefitDTOTestSuccess()
	{
		BenefitDTO benefitDTO = new BenefitDTO();
		benefitDTO.setNetInitPrem(new Double(0));
		benefitDTO.setBenefitName("abc");
		benefitDTO.toString();
		
		Assert.assertNotNull(benefitDTO.getNetInitPrem());
		Assert.assertNotNull(benefitDTO.getBenefitName());

		
	}

}