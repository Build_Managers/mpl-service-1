package uk.co.phoenixlife.service.dto.policy;

import java.sql.Timestamp;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.CashInTypeDTO;

public class CashInTypeDTOTest {

	@Test (enabled=true)
	public void cashInTypeDTOSuccess() {
		CashInTypeDTO cashInTypeDTO = new CashInTypeDTO();
		Timestamp busTimeStamp = new Timestamp(123L);
		cashInTypeDTO.setBancsPolNum("123");
		cashInTypeDTO.setBancsUniqueCustNo("456");
		cashInTypeDTO.setBusTimeStamp(busTimeStamp);
		cashInTypeDTO.setCashInType("cash");
		cashInTypeDTO.setpCLS("cls");
		cashInTypeDTO.setTax("tax");
		cashInTypeDTO.setSource("S123");
		cashInTypeDTO.toString();
		
		Assert.assertEquals("123", cashInTypeDTO.getBancsPolNum());
		Assert.assertEquals("456", cashInTypeDTO.getBancsUniqueCustNo());
		Assert.assertNotEquals(12L, cashInTypeDTO.getBusTimeStamp());
		Assert.assertEquals("cash", cashInTypeDTO.getCashInType());
		Assert.assertEquals("cls", cashInTypeDTO.getpCLS());
		Assert.assertEquals("tax", cashInTypeDTO.getTax());
		Assert.assertEquals("S123", cashInTypeDTO.getSource());
	}
}