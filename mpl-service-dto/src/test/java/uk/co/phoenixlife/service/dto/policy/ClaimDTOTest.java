package uk.co.phoenixlife.service.dto.policy;

import java.util.Date;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.ClaimDTO;


public class ClaimDTOTest {

	@Test (enabled=true)
    public void claimDTOSuccess() {
        ClaimDTO claimDTO = new ClaimDTO();

        claimDTO.setClmRef("abc");
        claimDTO.setClmStatusCd("abc");
        claimDTO.setClmTypeCd("abc");
        claimDTO.setClmDate(new Date());
        claimDTO.setClmTypeLit("lit");
        claimDTO.setClmStatusLit("status");
        claimDTO.toString();

        Assert.assertNotNull(claimDTO.getClmRef());
        Assert.assertNotNull(claimDTO.getClmStatusCd());
        Assert.assertNotNull(claimDTO.getClmTypeCd());
        Assert.assertNotNull(claimDTO.getClmDate());
        Assert.assertEquals("lit", claimDTO.getClmTypeLit());
        Assert.assertEquals("status", claimDTO.getClmStatusLit());

    }

}
