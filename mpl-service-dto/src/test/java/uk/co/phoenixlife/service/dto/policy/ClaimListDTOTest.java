package uk.co.phoenixlife.service.dto.policy;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.ClaimDTO;
import uk.co.phoenixlife.service.dto.policy.ClaimListDTO;
import uk.co.phoenixlife.service.dto.policy.QuoteDTO;


public class ClaimListDTOTest
{
	
	@Test (enabled=true)
	public void claimListDTOSuccess()
	{
		ClaimListDTO claimListDTO = new ClaimListDTO();
		
		
		List<QuoteDTO> pQuoteList = new ArrayList<QuoteDTO>();
		claimListDTO.setListOfQuotes(pQuoteList);
		claimListDTO.setBusTimeStamp(new Timestamp(0));
		claimListDTO.setBancsPolNum("abc");
		List<ClaimDTO> pClaimList = new ArrayList<ClaimDTO>();
		claimListDTO.setListOfClaims(pClaimList);
		claimListDTO.toString();
		
		Assert.assertNotNull(claimListDTO.getListOfQuotes());
		Assert.assertNotNull(claimListDTO.getBusTimeStamp());
		Assert.assertNotNull(claimListDTO.getBancsPolNum());
		Assert.assertNotNull(claimListDTO.getListOfClaims());

		
	}

}

