package uk.co.phoenixlife.service.dto.policy;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.CustomerTableValues;
import uk.co.phoenixlife.service.dto.policy.PartyBasicOwnerDTO;
import uk.co.phoenixlife.service.dto.response.ContactDetailsResponse;

public class CustomerTableValuesTest {

	@Test (enabled=true)
	public void customerTableValuesSuccess() {
		CustomerTableValues customerTableValues = new CustomerTableValues();
		ContactDetailsResponse pcontactDetailsResponse = new ContactDetailsResponse();
		ArrayList<Long> pcustomerIdList = new ArrayList<Long>();
		PartyBasicOwnerDTO ppartyBasicOwnerDTO = new PartyBasicOwnerDTO();
		customerTableValues.setBancsPolNum("num");
		customerTableValues.setContactDetailsResponse(pcontactDetailsResponse);
		customerTableValues.setCustomerId(1234L);
		customerTableValues.setCustomerIdList(pcustomerIdList);
		customerTableValues.setEmailId("email");
		customerTableValues.setPartyBasicOwnerDTO(ppartyBasicOwnerDTO);
		customerTableValues.setPolicyNumber("pnum");
		
		Assert.assertEquals("num", customerTableValues.getBancsPolNum());
		Assert.assertNotNull(customerTableValues.getContactDetailsResponse());
		Assert.assertNotNull(customerTableValues.getCustomerId());
		Assert.assertNotNull(customerTableValues.getCustomerIdList());
		Assert.assertEquals("email", customerTableValues.getEmailId());
		Assert.assertNotNull(customerTableValues.getPartyBasicOwnerDTO());
		Assert.assertEquals("pnum", customerTableValues.getPolicyNumber());
		
	}
}
