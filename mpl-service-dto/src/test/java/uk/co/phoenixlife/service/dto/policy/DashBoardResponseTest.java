package uk.co.phoenixlife.service.dto.policy;

import org.testng.Assert;
import org.testng.annotations.Test;

public class DashBoardResponseTest {
	
	@Test (enabled=true)
	public void dashBoardResponseSuccess(){
		
		DashBoardResponse dashBoardResponse = new DashBoardResponse();
		dashBoardResponse.setCustomerId(123456L);
		dashBoardResponse.setBancsUniqueCustomerNo("BUCN1");
		dashBoardResponse.setFirstName("FN");
		dashBoardResponse.setLastName("LN");
		dashBoardResponse.setPrimaryPhoneNumber("PPN123");
		dashBoardResponse.setTitle("T123");
		
		
		Assert.assertNotNull(dashBoardResponse.getCustomerId());
		Assert.assertEquals("BUCN1", dashBoardResponse.getBancsUniqueCustomerNo());
		Assert.assertEquals("FN", dashBoardResponse.getFirstName());
		Assert.assertEquals("LN", dashBoardResponse.getLastName());
		Assert.assertEquals("PPN123", dashBoardResponse.getPrimaryPhoneNumber());
		Assert.assertEquals("T123", dashBoardResponse.getTitle());
	}

}
