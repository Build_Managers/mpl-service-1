package uk.co.phoenixlife.service.dto.policy;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.DashboardDTO;
import uk.co.phoenixlife.service.dto.policy.DataCorrectionFlagDTO;
import uk.co.phoenixlife.service.dto.policy.FundDTO;
import uk.co.phoenixlife.service.dto.policy.QuoteDTO;

public class DashboardDTOTest {

	@Test (enabled=true)
	public void dashboardDTOSuccess() {
		DashboardDTO dashboardDTO = new DashboardDTO();
		Timestamp busTimeStamp = new Timestamp(123L);
		List<DataCorrectionFlagDTO> listOfFlags = new ArrayList<DataCorrectionFlagDTO>();
		List<QuoteDTO> listOfQuotes = new ArrayList<QuoteDTO>();
		Date polIRD = new Date();
		dashboardDTO.setBancsPolNum("num");
		dashboardDTO.setBusTimeStamp(busTimeStamp);
		dashboardDTO.setCashInInProgress("cash");
		dashboardDTO.setcUCharge(10.0);
		dashboardDTO.setEarlyExitCharge("10");
		dashboardDTO.setEligible(false);
		dashboardDTO.setEstimatedValue(20.0);
		dashboardDTO.setgARorGAO("abc");
		dashboardDTO.setListOfFlags(listOfFlags);
		List<FundDTO> listOfFunds = new ArrayList<FundDTO>();
		dashboardDTO.setListOfFunds(listOfFunds );
		dashboardDTO.setListOfQuotes(listOfQuotes);
		dashboardDTO.setMvaValue(30.0);
		dashboardDTO.setMvrValue("20");
		dashboardDTO.setPartyProt("party");
		dashboardDTO.setPenRevStatus("pen");
		dashboardDTO.setPenValue(40.0);
		dashboardDTO.setPolIRD(polIRD);
		dashboardDTO.setPolNum("pnum");
		dashboardDTO.setPolProt("prot");
		dashboardDTO.setPolSusp("sup");
		dashboardDTO.setTaxRegime("tax");
		dashboardDTO.setTotValue(50.0);
		dashboardDTO.toString();
		
		Assert.assertEquals("num", dashboardDTO.getBancsPolNum());
		Assert.assertNotEquals(12L, dashboardDTO.getBusTimeStamp());
		Assert.assertEquals("cash", dashboardDTO.getCashInInProgress());
		Assert.assertEquals(10.0, dashboardDTO.getcUCharge());
		Assert.assertEquals("10", dashboardDTO.getEarlyExitCharge());
		Assert.assertNotNull(dashboardDTO.getListOfFunds());
		Assert.assertFalse(dashboardDTO.isEligible());
		Assert.assertEquals(20.0, dashboardDTO.getEstimatedValue());
		Assert.assertEquals("abc", dashboardDTO.getgARorGAO());
		Assert.assertNotNull(dashboardDTO.getListOfFlags());
		Assert.assertNotNull(dashboardDTO.getListOfQuotes());
		Assert.assertEquals(30.0, dashboardDTO.getMvaValue());
		Assert.assertEquals("20", dashboardDTO.getMvrValue());
		Assert.assertEquals("party", dashboardDTO.getPartyProt());
		Assert.assertEquals("pen", dashboardDTO.getPenRevStatus());
		Assert.assertEquals(40.0, dashboardDTO.getPenValue());
		Assert.assertNotNull(dashboardDTO.getPolIRD());
		Assert.assertEquals("pnum", dashboardDTO.getPolNum());
		Assert.assertEquals("prot", dashboardDTO.getPolProt());
		Assert.assertEquals("sup", dashboardDTO.getPolSusp());
		Assert.assertEquals("tax", dashboardDTO.getTaxRegime());
		Assert.assertEquals(50.0, dashboardDTO.getTotValue());
		
		
		
	}
}
