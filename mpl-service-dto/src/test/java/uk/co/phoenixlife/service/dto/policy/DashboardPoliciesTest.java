package uk.co.phoenixlife.service.dto.policy;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.DashboardPolicies;
import uk.co.phoenixlife.service.dto.policy.DataCorrectionFlagDTO;


public class DashboardPoliciesTest
{
	
	@Test (enabled=true)
	public void dashboardPoliciesSuccess()
	{
		
	    DashboardPolicies dashboardPolicies = new DashboardPolicies();
		dashboardPolicies.setPolNum("abc");
		dashboardPolicies.setMvaValue(new Double(0));
		dashboardPolicies.setTotValue(new Double(0));
		dashboardPolicies.setgARorGAO('a');
		dashboardPolicies.setPolProt('a');
		List<DataCorrectionFlagDTO> pDataCorrectionFlagList = new ArrayList<DataCorrectionFlagDTO>();
		dashboardPolicies.setDataCorrectionFlagList(pDataCorrectionFlagList);
		dashboardPolicies.setPenValue(new Double(0));
		dashboardPolicies.setcUCharge(new Double(0));
		dashboardPolicies.setPartyProt('a');
		dashboardPolicies.setPolSusp('a');
		dashboardPolicies.setPenRevStatus("abc");
		dashboardPolicies.setTaxRegime("abc");
		dashboardPolicies.toString();
		
		Assert.assertNotNull(dashboardPolicies.getPolNum());
		Assert.assertNotNull(dashboardPolicies.getMvaValue());
		Assert.assertNotNull(dashboardPolicies.getTotValue());
		Assert.assertNotNull(dashboardPolicies.getgARorGAO());
		Assert.assertNotNull(dashboardPolicies.getPolNum());
		Assert.assertNotNull(dashboardPolicies.getPolProt());
		Assert.assertNotNull(dashboardPolicies.getDataCorrectionFlagList());
		Assert.assertNotNull(dashboardPolicies.getcUCharge());
		Assert.assertNotNull(dashboardPolicies.getPenValue());
		Assert.assertNotNull(dashboardPolicies.getPartyProt());
		Assert.assertNotNull(dashboardPolicies.getPolSusp());
		Assert.assertNotNull(dashboardPolicies.getPenRevStatus());
		Assert.assertNotNull(dashboardPolicies.getTaxRegime());
		
	}

}

