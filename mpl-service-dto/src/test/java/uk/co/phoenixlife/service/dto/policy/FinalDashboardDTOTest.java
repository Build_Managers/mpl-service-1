package uk.co.phoenixlife.service.dto.policy;



import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.DashboardDTO;
import uk.co.phoenixlife.service.dto.policy.FinalDashboardDTO;

public class FinalDashboardDTOTest {

	@Test (enabled=true)
	public void finalDashboardDTOSuccess() {
		FinalDashboardDTO finalDashboardDTO = new FinalDashboardDTO();
		List<DashboardDTO> pDashboardPolicyList = new ArrayList<DashboardDTO>();
		Date pDateOfBirth = new Date();
		finalDashboardDTO.setAdditionalPhoneNum("123");
		finalDashboardDTO.setAddress1("add1");
		finalDashboardDTO.setAddress2("add2");
		finalDashboardDTO.setBancslegacyCustNo("bancs");
		finalDashboardDTO.setBancsUniqueCustNo("uniq");
		finalDashboardDTO.setCityTown("city");
		finalDashboardDTO.setCountry("country");
		finalDashboardDTO.setCounty("pc");
		finalDashboardDTO.setDashboardPolicyList(pDashboardPolicyList);
		finalDashboardDTO.setDateOfBirth(pDateOfBirth);
		finalDashboardDTO.setEmail("email");
		finalDashboardDTO.setForename("for");
		finalDashboardDTO.setHouseName("hname");
		finalDashboardDTO.setHouseNum("hnum");
		finalDashboardDTO.setMobPhoneNum("456");
		finalDashboardDTO.setNiNo("nino");
		finalDashboardDTO.setPartyProt("party");
		finalDashboardDTO.setPolNum("pol");
		finalDashboardDTO.setPostCode("999");
		finalDashboardDTO.setPrimaryPhoneNum("9876");
		finalDashboardDTO.setProdName("prod");
		finalDashboardDTO.setSecPhoneNum("333");
		finalDashboardDTO.setSurname("sname");
		finalDashboardDTO.setTitle("title");
		finalDashboardDTO.toString();
		
		Assert.assertEquals("123", finalDashboardDTO.getAdditionalPhoneNum());
		Assert.assertEquals("add1", finalDashboardDTO.getAddress1());
		Assert.assertEquals("add2", finalDashboardDTO.getAddress2());
		Assert.assertEquals("bancs", finalDashboardDTO.getBancslegacyCustNo());
		Assert.assertEquals("uniq", finalDashboardDTO.getBancsUniqueCustNo());
		Assert.assertEquals("city", finalDashboardDTO.getCityTown());
		Assert.assertEquals("country", finalDashboardDTO.getCountry());
		Assert.assertEquals("pc", finalDashboardDTO.getCounty());
		Assert.assertNotNull(finalDashboardDTO.getDashboardPolicyList());
		Assert.assertNotNull(finalDashboardDTO.getDateOfBirth());
		Assert.assertEquals("email", finalDashboardDTO.getEmail());
		Assert.assertEquals("for", finalDashboardDTO.getForename());
		Assert.assertEquals("hname", finalDashboardDTO.getHouseName());
		Assert.assertEquals("hnum", finalDashboardDTO.getHouseNum());
		Assert.assertEquals("456", finalDashboardDTO.getMobPhoneNum());
		Assert.assertEquals("nino", finalDashboardDTO.getNiNo());
		Assert.assertEquals("party", finalDashboardDTO.getPartyProt());
		Assert.assertEquals("pol", finalDashboardDTO.getPolNum());
		Assert.assertEquals("999", finalDashboardDTO.getPostCode());
		Assert.assertEquals("9876", finalDashboardDTO.getPrimaryPhoneNum());
		Assert.assertEquals("prod", finalDashboardDTO.getProdName());
		Assert.assertEquals("333", finalDashboardDTO.getSecPhoneNum());
		Assert.assertEquals("sname", finalDashboardDTO.getSurname());
		Assert.assertEquals("title", finalDashboardDTO.getTitle());
	}
}

