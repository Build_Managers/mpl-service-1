package uk.co.phoenixlife.service.dto.policy;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.FinalQuestionsDTO;
import uk.co.phoenixlife.service.dto.policy.QuestionsDTO;

public class FinalQuestionsDTOTest {

	@Test (enabled=true)
	public void finalQuestionsDTOSuccess() {
		FinalQuestionsDTO finalQuestionsDTO = new FinalQuestionsDTO();
		List<QuestionsDTO> lisofQues = new ArrayList<QuestionsDTO>();
		finalQuestionsDTO.setLisofQues(lisofQues);
		
		Assert.assertNotNull(finalQuestionsDTO.getLisofQues());
	}
}