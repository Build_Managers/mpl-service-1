package uk.co.phoenixlife.service.dto.policy;

import org.testng.Assert;
import org.testng.annotations.Test;
import uk.co.phoenixlife.service.dto.policy.FundDTO;

public class FundDTOTest {

	@Test (enabled=true)
	public void fundDTOSuccess() {
		FundDTO fundDTO = new FundDTO();
		fundDTO.setFundName("fname");
		fundDTO.toString();
		
		Assert.assertEquals("fname", fundDTO.getFundName());
	}
	
}