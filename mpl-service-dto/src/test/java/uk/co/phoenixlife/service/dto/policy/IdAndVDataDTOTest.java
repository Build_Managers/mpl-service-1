package uk.co.phoenixlife.service.dto.policy;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.IdAndVDataDTO;
import uk.co.phoenixlife.service.dto.policy.PartyPolicyDTO;

public class IdAndVDataDTOTest {

	@Test (enabled=true)
	public void idAndVDataDTOSuccess() {
		IdAndVDataDTO idAndVDataDTO = new IdAndVDataDTO();
		Timestamp busTimeStampParam = new Timestamp(0);
		List<PartyPolicyDTO> listOfPolicies = new ArrayList<PartyPolicyDTO>();
		idAndVDataDTO.setBusTimeStamp(busTimeStampParam);
		idAndVDataDTO.setListOfPolicies(listOfPolicies);
		idAndVDataDTO.toString();
		
		Assert.assertNotNull(idAndVDataDTO.getBusTimeStamp());
		Assert.assertNotNull(idAndVDataDTO.getListOfPolicies());
	}
}