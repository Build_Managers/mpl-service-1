package uk.co.phoenixlife.service.dto.policy;

import java.util.Date;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.OwnerDTO;

public class OwnerDTOTest {

	@Test (enabled=true)
	public void ownerDTOSuccess() {
		OwnerDTO ownerDTO = new OwnerDTO();
		ownerDTO.setAddress1("add1");
		ownerDTO.setAddress2("add2");
		ownerDTO.setAddressStatus("status");
		ownerDTO.setBancsUniqueCustNo(new Long(0));
		ownerDTO.setBankruptcyDate(new Date());
		ownerDTO.setBankruptcyDischargeDate(new Date());
		ownerDTO.setBankruptcyRefNo("123");
		ownerDTO.setCityTown("city");
		ownerDTO.setCounty("country");
		ownerDTO.setCourtName("cname");
		ownerDTO.setCourtOrderDate(new Date());
		ownerDTO.setDateOfBirth(new Date());
		ownerDTO.setFcuFlag('y');
		ownerDTO.setForename("for");
		ownerDTO.setHouseName("hname");
		ownerDTO.setHouseNum("456");
		ownerDTO.setPartyType("party");
		ownerDTO.setPoaFlag('y');
		ownerDTO.setPostCode("999");
		ownerDTO.setSurname("sname");
		ownerDTO.setTitle("title");
		ownerDTO.toString();
		
		Assert.assertEquals("add1", ownerDTO.getAddress1());
		Assert.assertEquals("add2", ownerDTO.getAddress2());
		Assert.assertEquals("status", ownerDTO.getAddressStatus());
		Assert.assertNotNull(ownerDTO.getBancsUniqueCustNo());
		Assert.assertNotNull(ownerDTO.getBankruptcyDate());
		Assert.assertNotNull(ownerDTO.getBankruptcyDischargeDate());
		Assert.assertEquals("123", ownerDTO.getBankruptcyRefNo());
		Assert.assertEquals("city", ownerDTO.getCityTown());
		Assert.assertEquals("country", ownerDTO.getCounty());
		Assert.assertEquals("cname", ownerDTO.getCourtName());
		Assert.assertNotNull(ownerDTO.getCourtOrderDate());
		Assert.assertNotNull(ownerDTO.getDateOfBirth());
		Assert.assertEquals('y', ownerDTO.getFcuFlag());
		Assert.assertEquals("for", ownerDTO.getForename());
		Assert.assertEquals("hname", ownerDTO.getHouseName());
		Assert.assertEquals("456", ownerDTO.getHouseNum());
		Assert.assertEquals("party", ownerDTO.getPartyType());
		Assert.assertEquals('y', ownerDTO.getPoaFlag());
		Assert.assertEquals("999", ownerDTO.getPostCode());
		Assert.assertEquals("sname", ownerDTO.getSurname());
		Assert.assertEquals("title", ownerDTO.getTitle());
	}
}
