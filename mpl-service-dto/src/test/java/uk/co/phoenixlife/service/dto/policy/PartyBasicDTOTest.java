package uk.co.phoenixlife.service.dto.policy;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.PartyBasicDTO;
import uk.co.phoenixlife.service.dto.policy.PartyBasicOwnerListDTO;

public class PartyBasicDTOTest {

	@Test (enabled=true)
	public void partyBasicDTOSuccess() {
		PartyBasicDTO partyBasicDTO = new PartyBasicDTO();
		List<PartyBasicOwnerListDTO> listOfPolicies = new ArrayList<PartyBasicOwnerListDTO>();
		partyBasicDTO.setBusTimeStamp(new Timestamp(10L));
		partyBasicDTO.setIsValid(false);
		partyBasicDTO.setListOfPolicies(listOfPolicies);
		partyBasicDTO.toString();
		
		Assert.assertNotEquals(1, partyBasicDTO.getBusTimeStamp());
		Assert.assertFalse(partyBasicDTO.getIsValid());
		Assert.assertNotNull(partyBasicDTO.getListOfPolicies());
	}
}
