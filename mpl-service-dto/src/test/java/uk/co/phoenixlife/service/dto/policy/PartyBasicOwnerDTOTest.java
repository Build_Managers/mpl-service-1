package uk.co.phoenixlife.service.dto.policy;

import java.util.Date;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.PartyBasicOwnerDTO;

public class PartyBasicOwnerDTOTest {

	@Test (enabled=true)
	public void partyBasicOwnerDTOSuccess() {
		PartyBasicOwnerDTO partyBasicOwnerDTO = new PartyBasicOwnerDTO();
		partyBasicOwnerDTO.setBancsLegacyCustNo("abc");
		partyBasicOwnerDTO.setBancsUniqueCustNo("xyz");
		partyBasicOwnerDTO.setDateOfBirth(new Date());
		partyBasicOwnerDTO.setForename("fname");
		partyBasicOwnerDTO.setNiNo("nino");
		partyBasicOwnerDTO.setPartyType("type");
		partyBasicOwnerDTO.setSurname("sname");
		partyBasicOwnerDTO.setTitle("title");
		partyBasicOwnerDTO.toString();
		
		Assert.assertEquals("abc", partyBasicOwnerDTO.getBancsLegacyCustNo());
		Assert.assertEquals("xyz", partyBasicOwnerDTO.getBancsUniqueCustNo());
		Assert.assertNotNull(partyBasicOwnerDTO.getDateOfBirth());
		Assert.assertEquals("fname", partyBasicOwnerDTO.getForename());
		Assert.assertEquals("nino", partyBasicOwnerDTO.getNiNo());
		Assert.assertEquals("type", partyBasicOwnerDTO.getPartyType());
		Assert.assertEquals("sname", partyBasicOwnerDTO.getSurname());
		Assert.assertEquals("title", partyBasicOwnerDTO.getTitle());
	}
}

