package uk.co.phoenixlife.service.dto.policy;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.PartyBasicOwnerDTO;
import uk.co.phoenixlife.service.dto.policy.PartyBasicOwnerListDTO;

public class PartyBasicOwnerListDTOTest {
	@Test (enabled=true)
	public void partyBasicOwnerListDTOSuccess() {
		PartyBasicOwnerListDTO partyBasicOwnerListDTO = new PartyBasicOwnerListDTO();
		List<PartyBasicOwnerDTO> listOfOwners = new ArrayList<PartyBasicOwnerDTO>();
		partyBasicOwnerListDTO.setListOfOwners(listOfOwners);
		partyBasicOwnerListDTO.setPolNum("123");
		partyBasicOwnerListDTO.toString();
		
		Assert.assertNotNull(partyBasicOwnerListDTO.getListOfOwners());
		Assert.assertEquals("123", partyBasicOwnerListDTO.getPolNum());
	}
	
}
