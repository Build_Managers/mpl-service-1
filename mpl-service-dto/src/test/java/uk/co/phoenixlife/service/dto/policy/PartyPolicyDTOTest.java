package uk.co.phoenixlife.service.dto.policy;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.BenefitDTO;
import uk.co.phoenixlife.service.dto.policy.FundDTO;
import uk.co.phoenixlife.service.dto.policy.PartyPolicyDTO;

public class PartyPolicyDTOTest {
	@Test (enabled=true)
	public void partyPolicyDTOSuccess() {
		PartyPolicyDTO partyPolicyDTO = new PartyPolicyDTO();
		List<BenefitDTO> listOfBenefits = new ArrayList<BenefitDTO>();
		List<FundDTO> listOfFunds = new ArrayList<FundDTO>();
		partyPolicyDTO.setBancsPolNum("123");
		partyPolicyDTO.setBankAcct("acc");
		partyPolicyDTO.setLastPremPaid(new Double(0));
		partyPolicyDTO.setListOfBenefits(listOfBenefits);
		partyPolicyDTO.setListOfFunds(listOfFunds);
		partyPolicyDTO.setNetModPrem(new Double(0));
		partyPolicyDTO.setPaymentMethodCd("cd");
		partyPolicyDTO.setPaymentMethodLit("lit");
		partyPolicyDTO.setPolNum("456");
		partyPolicyDTO.setPolStatusCd("y");
		partyPolicyDTO.setPolStatusLit("lit");
		partyPolicyDTO.setPolStDate(new Date());
		partyPolicyDTO.setPremFreq("freq");
		partyPolicyDTO.setPremiumDueDate("date");
		partyPolicyDTO.setSortCode("999");
		partyPolicyDTO.toString();
		
		Assert.assertEquals("123", partyPolicyDTO.getBancsPolNum());
		Assert.assertEquals("acc", partyPolicyDTO.getBankAcct());
		Assert.assertNotNull(partyPolicyDTO.getLastPremPaid());
		Assert.assertNotNull(partyPolicyDTO.getListOfBenefits());
		Assert.assertNotNull(partyPolicyDTO.getListOfFunds());
		Assert.assertNotNull(partyPolicyDTO.getNetModPrem());
		Assert.assertEquals("cd", partyPolicyDTO.getPaymentMethodCd());
		Assert.assertEquals("lit", partyPolicyDTO.getPaymentMethodLit());
		Assert.assertEquals("456", partyPolicyDTO.getPolNum());
		Assert.assertEquals("y", partyPolicyDTO.getPolStatusCd());
		Assert.assertEquals("lit", partyPolicyDTO.getPolStatusLit());
		Assert.assertNotNull(partyPolicyDTO.getPolStDate());
		Assert.assertEquals("freq", partyPolicyDTO.getPremFreq());
		Assert.assertEquals("date", partyPolicyDTO.getPremiumDueDate());
		Assert.assertEquals("999", partyPolicyDTO.getSortCode());
	}
}

