package uk.co.phoenixlife.service.dto.policy;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.DataCorrectionFlagDTO;
import uk.co.phoenixlife.service.dto.policy.PolDetailsDTO;
import uk.co.phoenixlife.service.dto.policy.PolicyRoleDTO;

public class PolDetailsDTOTest {
	@Test (enabled=true)
	public void PolDetailsDTOSuccess() {
		PolDetailsDTO polDetailsDTO = new PolDetailsDTO();
		List<DataCorrectionFlagDTO> listOfFlags = new ArrayList<DataCorrectionFlagDTO>();
		List<PolicyRoleDTO> listOfRoles = new ArrayList<PolicyRoleDTO>();
		polDetailsDTO.setAccrualBasisCd("cd");
		polDetailsDTO.setAccrualBasisLit("lit");
		polDetailsDTO.setBancsPolNum("123");
		polDetailsDTO.setBusTimeStamp(new Timestamp(1L));
		polDetailsDTO.setCashInInProgress("y");
		polDetailsDTO.setgARorGAO("gao");
		polDetailsDTO.setListOfFlags(listOfFlags);
		polDetailsDTO.setListOfRoles(listOfRoles);
		polDetailsDTO.setPenRevStatusCd("cd");
		polDetailsDTO.setPenRevStatusLit("lit");
		polDetailsDTO.setPolProt("pol");
		polDetailsDTO.setTaxRegimeLit("rlit");
		polDetailsDTO.setTrustBasedApplic("trust");
		polDetailsDTO.setUnitisedPol("unit");
		polDetailsDTO.toString();
		
		Assert.assertEquals("cd", polDetailsDTO.getAccrualBasisCd());
		Assert.assertEquals("lit", polDetailsDTO.getAccrualBasisLit());
		Assert.assertEquals("123", polDetailsDTO.getBancsPolNum());
		Assert.assertNotEquals(100L, polDetailsDTO.getBusTimeStamp());
		Assert.assertEquals("y", polDetailsDTO.getCashInInProgress());
		Assert.assertEquals("gao", polDetailsDTO.getgARorGAO());
		Assert.assertNotNull(polDetailsDTO.getListOfFlags());
		Assert.assertNotNull(polDetailsDTO.getListOfRoles());
		Assert.assertEquals("cd", polDetailsDTO.getPenRevStatusCd());
		Assert.assertEquals("lit", polDetailsDTO.getPenRevStatusLit());
		Assert.assertEquals("pol", polDetailsDTO.getPolProt());
		Assert.assertEquals("rlit", polDetailsDTO.getTaxRegimeLit());
		Assert.assertEquals("trust", polDetailsDTO.getTrustBasedApplic());
		Assert.assertEquals("unit", polDetailsDTO.getUnitisedPol());
	}
}
