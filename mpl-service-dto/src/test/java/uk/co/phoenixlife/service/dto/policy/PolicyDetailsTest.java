package uk.co.phoenixlife.service.dto.policy;

import org.testng.Assert;
import org.testng.annotations.Test;

public class PolicyDetailsTest {
	
	@Test (enabled=true)
	public void policyDetailsSuccess(){
		
		
		PolicyDetails policyDetails = new PolicyDetails();
		policyDetails.setPolicyRef("PR123");
		policyDetails.setPolicyBakUniqueNumber("PBUN1");
		policyDetails.setEstimatedVal(123L);
		policyDetails.setGaranteedBenifit(true);
		policyDetails.setMarketValReduction(true);
		policyDetails.setEarlyExitCharge(true);
		policyDetails.setIsEligible(true);
		
		Assert.assertEquals("PR123", policyDetails.getPolicyRef());
		Assert.assertEquals("PBUN1", policyDetails.getPolicyBakUniqueNumber());
		Assert.assertEquals(123L, policyDetails.getEstimatedVal());
		Assert.assertNotNull(policyDetails.getGaranteedBenifit());
		Assert.assertNotNull(policyDetails.getMarketValReduction());
		Assert.assertNotNull(policyDetails.getEarlyExitCharge());
		Assert.assertNotNull(policyDetails.getIsEligible());
	}

}

	