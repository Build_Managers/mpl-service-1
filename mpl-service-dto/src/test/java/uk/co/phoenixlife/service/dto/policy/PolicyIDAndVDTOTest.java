package uk.co.phoenixlife.service.dto.policy;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.PartyPolicyDTO;
import uk.co.phoenixlife.service.dto.policy.PolicyIDAndVDTO;

public class PolicyIDAndVDTOTest {
	@Test (enabled=true)
	public void policyIDAndVDTOSuccess() {
		PolicyIDAndVDTO policyIDAndVDTO = new PolicyIDAndVDTO();
		List<PartyPolicyDTO> partyPolicyParam = new ArrayList<PartyPolicyDTO>();
		policyIDAndVDTO.setPartyPolicy(partyPolicyParam);
		policyIDAndVDTO.setPolnum("123");
		policyIDAndVDTO.toString();
		
		Assert.assertNotNull(policyIDAndVDTO.getPartyPolicy());
		Assert.assertEquals("123", policyIDAndVDTO.getPolnum());
	}
}
