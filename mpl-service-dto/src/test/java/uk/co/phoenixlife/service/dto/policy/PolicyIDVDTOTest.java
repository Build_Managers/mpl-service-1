package uk.co.phoenixlife.service.dto.policy;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.format.datetime.joda.LocalDateParser;
import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.PolicyIDVDTO;
/**
 * PolicyIDVDTO.java
 * @author 1200912
 *
 */
public class PolicyIDVDTOTest {

	@Test (enabled=true)
	public void policyIDVDTOSuccess() {
		PolicyIDVDTO policyIDVDTO = new PolicyIDVDTO();
		List<String> pFundName = new ArrayList<String>();
		policyIDVDTO.setBancsPolNum("123");
		policyIDVDTO.setBancsUniqueCustNo("999");
		policyIDVDTO.setBankAcct(1234);
		policyIDVDTO.setBenefitName("bname");
		policyIDVDTO.setFundName(pFundName);
		policyIDVDTO.setLastPremPaid(555.0);
		policyIDVDTO.setmLStatus("y");
		policyIDVDTO.setNetInitPrem(10.0);
		policyIDVDTO.setNetModPrem(20.0);
		policyIDVDTO.setNiNo("nino");
		policyIDVDTO.setPaymentMethod("xyz");
		policyIDVDTO.setPolNum("456");
		policyIDVDTO.setPolStatus("y");
		policyIDVDTO.setPolStDate(null);
		policyIDVDTO.setPremCeaseDate(null);
		policyIDVDTO.setPremFreq("freq");
		policyIDVDTO.setPremiumDueDate(10);
		policyIDVDTO.setSortCode(789);
		policyIDVDTO.toString();
		
		Assert.assertEquals("123", policyIDVDTO.getBancsPolNum());
		Assert.assertEquals("999", policyIDVDTO.getBancsUniqueCustNo());
		Assert.assertNotEquals(123, policyIDVDTO.getBankAcct());
		Assert.assertEquals("bname", policyIDVDTO.getBenefitName());
		Assert.assertNotNull(policyIDVDTO.getFundName());
		Assert.assertEquals(555.0, policyIDVDTO.getLastPremPaid());
		Assert.assertEquals("y", policyIDVDTO.getmLStatus());
		Assert.assertEquals(10.0, policyIDVDTO.getNetInitPrem());
		Assert.assertEquals(20.0, policyIDVDTO.getNetModPrem());
		Assert.assertEquals("nino", policyIDVDTO.getNiNo());
		Assert.assertEquals("xyz", policyIDVDTO.getPaymentMethod());
		Assert.assertEquals("456", policyIDVDTO.getPolNum());
		Assert.assertEquals("y", policyIDVDTO.getPolStatus());
		Assert.assertNull(policyIDVDTO.getPolStDate());
		Assert.assertNull(policyIDVDTO.getPremCeaseDate());
		Assert.assertEquals("freq", policyIDVDTO.getPremFreq());
		Assert.assertNotEquals(12, policyIDVDTO.getPremiumDueDate());
		Assert.assertNotEquals(7899, policyIDVDTO.getSortCode());
		
	}
}

