package uk.co.phoenixlife.service.dto.policy;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.PolicyOwnedByPartyDTO;

public class PolicyOwnedByPartyDTOTest {
	@Test (enabled=true)
	public void policyOwnedByPartyDTOSuccess() {
		PolicyOwnedByPartyDTO policyOwnedByPartyDTO = new PolicyOwnedByPartyDTO();
		policyOwnedByPartyDTO.setAddress1("add1");
		policyOwnedByPartyDTO.setAddress2("add2");
		policyOwnedByPartyDTO.setBancsUniqueCustNo("123");
		policyOwnedByPartyDTO.setBusTimeStamp("999");
		policyOwnedByPartyDTO.setCityTown("city");
		policyOwnedByPartyDTO.setCountry("country");
		policyOwnedByPartyDTO.setCounty("pcountry");
		policyOwnedByPartyDTO.setDateOfBirth("456");
		policyOwnedByPartyDTO.setForename("fname");
		policyOwnedByPartyDTO.setHouseName("hname");
		policyOwnedByPartyDTO.setHouseNum("999");
		policyOwnedByPartyDTO.setMobPhoneNum("111");
		policyOwnedByPartyDTO.setNiNo("nino");
		policyOwnedByPartyDTO.setPartyProt("party");
		policyOwnedByPartyDTO.setPostCode("666");
		policyOwnedByPartyDTO.setSurname("sname");
		policyOwnedByPartyDTO.setTitle("title");
		policyOwnedByPartyDTO.toString();
		
		Assert.assertEquals("add1", policyOwnedByPartyDTO.getAddress1());
		Assert.assertEquals("add2", policyOwnedByPartyDTO.getAddress2());
		Assert.assertEquals("123", policyOwnedByPartyDTO.getBancsUniqueCustNo());
		Assert.assertEquals("999", policyOwnedByPartyDTO.getBusTimeStamp());
		Assert.assertEquals("city", policyOwnedByPartyDTO.getCityTown());
		Assert.assertEquals("country", policyOwnedByPartyDTO.getCountry());
		Assert.assertEquals("pcountry", policyOwnedByPartyDTO.getCounty());
		Assert.assertEquals("456", policyOwnedByPartyDTO.getDateOfBirth());
		Assert.assertEquals("fname", policyOwnedByPartyDTO.getForename());
		Assert.assertEquals("hname", policyOwnedByPartyDTO.getHouseName());
		Assert.assertEquals("999", policyOwnedByPartyDTO.getHouseNum());
		Assert.assertEquals("111", policyOwnedByPartyDTO.getMobPhoneNum());
		Assert.assertEquals("nino", policyOwnedByPartyDTO.getNiNo());
		Assert.assertEquals("party", policyOwnedByPartyDTO.getPartyProt());
		Assert.assertEquals("666", policyOwnedByPartyDTO.getPostCode());
		Assert.assertEquals("sname", policyOwnedByPartyDTO.getSurname());
		Assert.assertEquals("title", policyOwnedByPartyDTO.getTitle());
	}
}
