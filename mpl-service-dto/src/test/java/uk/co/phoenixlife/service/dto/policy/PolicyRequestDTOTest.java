package uk.co.phoenixlife.service.dto.policy;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.pdf.FormRequestDTO;
import uk.co.phoenixlife.service.dto.policy.PolicyRequestDTO;
import uk.co.phoenixlife.service.dto.policy.QuoteDetails;

public class PolicyRequestDTOTest {
	@Test (enabled=true)
	public void policyRequestDTOSuccess() {
		PolicyRequestDTO policyRequestDTO = new PolicyRequestDTO();
		QuoteDetails pQuoteDetails = new QuoteDetails();
		FormRequestDTO pFormData = new FormRequestDTO();
		policyRequestDTO.setCustomerId(100L);
		policyRequestDTO.setEligiblePolicyCount(100);
		policyRequestDTO.setFormData(pFormData);
		policyRequestDTO.setIdentifySource(10);
		policyRequestDTO.setNino("nino");
		policyRequestDTO.setPolicyId(10L);
		policyRequestDTO.setPolicyNumber("123");
		policyRequestDTO.setQuestionKey("qkey");
		policyRequestDTO.setQuoteDetails(pQuoteDetails);
		policyRequestDTO.setxGUID("xguid");
		
		Assert.assertNotNull(policyRequestDTO.getCustomerId());
		Assert.assertEquals(100,policyRequestDTO.getElligiblePolicyCount());
		Assert.assertNotNull(policyRequestDTO.getFormData());
		Assert.assertEquals(10, policyRequestDTO.getIdentifySource());
		Assert.assertEquals("nino", policyRequestDTO.getNino());
		Assert.assertEquals(10L, policyRequestDTO.getPolicyId());
		Assert.assertEquals("123", policyRequestDTO.getPolicyNumber());
		Assert.assertEquals("qkey", policyRequestDTO.getQuestionKey());
		Assert.assertNotNull(policyRequestDTO.getQuoteDetails());
		Assert.assertEquals("xguid", policyRequestDTO.getxGUID());
		
	}
}
