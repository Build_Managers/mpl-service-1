package uk.co.phoenixlife.service.dto.policy;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.PolicyResponseDTO;

public class PolicyResponseDTOTest {

	@Test (enabled=true)
	public void policyResponseDTOSuccess() {
		PolicyResponseDTO policyResponseDTO = new PolicyResponseDTO();
		List<Long> pCustomerIdList = new ArrayList<Long>();
		List<String> pLockedQuestionKeyList = new ArrayList<String>();
		policyResponseDTO.setCustomerIdList(pCustomerIdList);
		policyResponseDTO.setLockedQuestionKeyList(pLockedQuestionKeyList);
		policyResponseDTO.setPolicyLocked(false);
		
		Assert.assertNotNull(policyResponseDTO.getCustomerIdList());
		Assert.assertNotNull(policyResponseDTO.getLockedQuestionKeyList());
		Assert.assertFalse(policyResponseDTO.isPolicyLocked());
	}
}
