package uk.co.phoenixlife.service.dto.policy;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.PolicyRoleDTO;

public class PolicyRoleDTOTest {

	@Test (enabled=true)
	public void policyRoleDTOSuccess() {
		PolicyRoleDTO policyRoleDTO = new PolicyRoleDTO();
		policyRoleDTO.setBancsUniqueCustNo("123");
		policyRoleDTO.setLegacyCustNo("abc");
		policyRoleDTO.setPolRoleCd("cd");
		policyRoleDTO.setPolRoleLit("lit");
		policyRoleDTO.toString();
		
		Assert.assertEquals("123", policyRoleDTO.getBancsUniqueCustNo());
		Assert.assertEquals("abc", policyRoleDTO.getLegacyCustNo());
		Assert.assertEquals("cd", policyRoleDTO.getPolRoleCd());
		Assert.assertEquals("lit", policyRoleDTO.getPolRoleLit());
	}
}
