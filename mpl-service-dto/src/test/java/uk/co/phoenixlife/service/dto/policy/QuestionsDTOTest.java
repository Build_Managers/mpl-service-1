package uk.co.phoenixlife.service.dto.policy;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.QuestionsDTO;

public class QuestionsDTOTest {

	@Test (enabled=true)
	public void questionsDTOSuccess() {
		QuestionsDTO questionsDTO = new QuestionsDTO();
		questionsDTO.setQueId("123");
		questionsDTO.setResult(true);
		
		Assert.assertEquals("123", questionsDTO.getQueId());
		Assert.assertTrue(questionsDTO.isResult());
	}
	
}

