package uk.co.phoenixlife.service.dto.policy;

import java.sql.Timestamp;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.QuoteDetails;

public class QuoteDetailsTest {

	@Test (enabled=true)
	public void quoteDetailsSuccess() {
		QuoteDetails quoteDetails = new QuoteDetails();
		quoteDetails.setPaymentMethod("PM123");
		quoteDetails.setSortCode("789");
		quoteDetails.setBankAcctNo("123");
		quoteDetails.setRollNo("RN");
		quoteDetails.setSmallPotFlag(false);
		quoteDetails.setContactCustomerFlag(false);
		quoteDetails.setSentTimestamp(new Timestamp(0));
		quoteDetails.setAckTimestamp(new Timestamp(0));
		quoteDetails.setErrorDesc("ED");
		quoteDetails.setQuoteAPIResp(1234);
		quoteDetails.setEncashRefNo("ERN");
		quoteDetails.toString();
		
		Assert.assertEquals("PM123", quoteDetails.getPaymentMethod());
		Assert.assertEquals("789", quoteDetails.getSortCode());
		Assert.assertEquals("123", quoteDetails.getBankAcctNo());
		Assert.assertEquals("RN", quoteDetails.getRollNo());
		Assert.assertFalse(quoteDetails.getSmallPotFlag());
		Assert.assertFalse(quoteDetails.getContactCustomerFlag());
		Assert.assertNotNull(quoteDetails.getSentTimestamp());
		Assert.assertNotNull(quoteDetails.getAckTimestamp());
		Assert.assertEquals("ED", quoteDetails.getErrorDesc());
		Assert.assertEquals(1234, quoteDetails.getQuoteAPIResp());
		Assert.assertEquals("ERN", quoteDetails.getEncashRefNo());
	}
}
