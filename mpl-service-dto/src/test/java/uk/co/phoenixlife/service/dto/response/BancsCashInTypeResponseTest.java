package uk.co.phoenixlife.service.dto.response;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.BancsCashInTypeWrapper;
import uk.co.phoenixlife.service.dto.staticdata.StaticDataRequestDTO;

public class BancsCashInTypeResponseTest {
	@Test (enabled=true)
	public void bancsCashInTypeResponseSuccess() {
		BancsCashInTypeResponse bancsCashInTypeResponse = new BancsCashInTypeResponse();
		BancsCashInTypeWrapper bancsCashInTypeWrapper = new BancsCashInTypeWrapper();
		bancsCashInTypeResponse.setBancsCalcCashInTax(bancsCashInTypeWrapper);
       
		Assert.assertNotNull(bancsCashInTypeResponse.getBancsCalcCashInTax());
		}

}
