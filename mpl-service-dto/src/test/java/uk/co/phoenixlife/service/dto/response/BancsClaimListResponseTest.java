package uk.co.phoenixlife.service.dto.response;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.ClaimListDTO;

public class BancsClaimListResponseTest {
	@Test (enabled=true)
	public void bancsCashInTypeResponseSuccess() {
		BancsClaimListResponse bancsClaimListResponse =new BancsClaimListResponse();
		ClaimListDTO claimListDTO = new ClaimListDTO();
		bancsClaimListResponse.setBancsClaimList(claimListDTO);
       
		Assert.assertNotNull(bancsClaimListResponse.getBancsClaimList());
		}

}
