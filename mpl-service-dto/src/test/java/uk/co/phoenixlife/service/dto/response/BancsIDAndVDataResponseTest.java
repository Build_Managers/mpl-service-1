package uk.co.phoenixlife.service.dto.response;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.IdAndVDataDTO;

public class BancsIDAndVDataResponseTest {
	@Test (enabled=true)
	public void bancsIDAndVDataResponseSuccess() {
		BancsIDAndVDataResponse bancsIDAndVDataResponse =new BancsIDAndVDataResponse();
		IdAndVDataDTO idAndVDataDTO = new IdAndVDataDTO();
		bancsIDAndVDataResponse.setBancsIDAndVData(idAndVDataDTO);
       
		Assert.assertNotNull(bancsIDAndVDataResponse.getBancsIDAndVData());
		}

}
