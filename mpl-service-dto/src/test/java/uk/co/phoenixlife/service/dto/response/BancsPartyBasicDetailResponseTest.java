package uk.co.phoenixlife.service.dto.response;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.PartyBasicDTO;

public class BancsPartyBasicDetailResponseTest {
	@Test (enabled=true)
	public void bancsPartyBasicDetailResponseSuccess() {
		BancsPartyBasicDetailResponse bancsPartyBasicDetailResponse =new BancsPartyBasicDetailResponse();
		PartyBasicDTO partyBasicDTO = new PartyBasicDTO();
		bancsPartyBasicDetailResponse.setBancsPartyBasicDetail(partyBasicDTO);
       
		Assert.assertNotNull(bancsPartyBasicDetailResponse.getBancsPartyBasicDetail());
		}

}
