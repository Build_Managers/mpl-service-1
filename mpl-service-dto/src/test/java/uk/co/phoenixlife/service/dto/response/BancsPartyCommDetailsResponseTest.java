package uk.co.phoenixlife.service.dto.response;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.PartyCommDetailsDTO;

public class BancsPartyCommDetailsResponseTest {
	@Test (enabled=true)
	public void bancsPartyCommDetailsResponseSuccess() {
		BancsPartyCommDetailsResponse bancsPartyCommDetailsResponse =new BancsPartyCommDetailsResponse();
		PartyCommDetailsDTO PartyCommDetailsDTO = new PartyCommDetailsDTO();
		bancsPartyCommDetailsResponse.setBancsPartyCommDetails(PartyCommDetailsDTO);
       
		Assert.assertNotNull(bancsPartyCommDetailsResponse.getBancsPartyCommDetails());
		}

}
