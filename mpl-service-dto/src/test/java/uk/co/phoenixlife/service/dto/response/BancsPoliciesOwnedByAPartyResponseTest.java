package uk.co.phoenixlife.service.dto.response;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.dashboard.PoliciesOwnedByAPartyDTO;


public class BancsPoliciesOwnedByAPartyResponseTest {
	@Test (enabled=true)
	public void bancsPoliciesOwnedByAPartyResponseSuccess() {
		BancsPoliciesOwnedByAPartyResponse bancsPoliciesOwnedByAPartyResponse =new BancsPoliciesOwnedByAPartyResponse();
		PoliciesOwnedByAPartyDTO policiesOwnedByAPartyDTO = new PoliciesOwnedByAPartyDTO();
		bancsPoliciesOwnedByAPartyResponse.setBancsPoliciesOwnedByAParty(policiesOwnedByAPartyDTO);
       
		Assert.assertNotNull(bancsPoliciesOwnedByAPartyResponse.getBancsPoliciesOwnedByAParty());
		}

}
