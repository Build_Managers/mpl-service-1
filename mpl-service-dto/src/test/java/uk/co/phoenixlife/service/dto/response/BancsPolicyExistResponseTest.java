package uk.co.phoenixlife.service.dto.response;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.policy.BancsPolicyExists;

public class BancsPolicyExistResponseTest {
	@Test (enabled=true)
	public void bancsPoliciesOwnedByAPartyResponseSuccess() {
		BancsPolicyExistResponse BancsPolicyExistResponse =new BancsPolicyExistResponse();
		BancsPolicyExists bancsPolicyExists = new BancsPolicyExists();
		BancsPolicyExistResponse.setBancsPolicyExistsCheck(bancsPolicyExists);
       
		Assert.assertNotNull(BancsPolicyExistResponse.getBancsPolicyExistsCheck());
		}
	

}
