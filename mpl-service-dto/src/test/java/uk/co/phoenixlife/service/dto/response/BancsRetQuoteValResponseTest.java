package uk.co.phoenixlife.service.dto.response;

import org.testng.Assert;
import org.testng.annotations.Test;


import uk.co.phoenixlife.service.dto.policy.BancsRetQuoteValDTO;

public class BancsRetQuoteValResponseTest {
	@Test (enabled=true)
	public void bancsRetQuoteValResponseSuccess() {
		BancsRetQuoteValResponse bancsRetQuoteValResponse =new BancsRetQuoteValResponse();
		BancsRetQuoteValDTO bancsRetQuoteValDTO = new BancsRetQuoteValDTO();
		bancsRetQuoteValResponse.setBancsRetQuoteVal(bancsRetQuoteValDTO);
		
		Assert.assertNotNull(bancsRetQuoteValResponse.getBancsRetQuoteVal());
		}
	

}
