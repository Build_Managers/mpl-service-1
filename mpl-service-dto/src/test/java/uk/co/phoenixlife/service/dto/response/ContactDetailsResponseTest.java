package uk.co.phoenixlife.service.dto.response;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ContactDetailsResponseTest {
	@Test (enabled=true)
	public void contactDetailsResponseSuccess() {
		ContactDetailsResponse contactDetailsResponse = new ContactDetailsResponse();
		contactDetailsResponse.setAdditionalPhoneNumber("APN");
		contactDetailsResponse.setAdditionalPhoneType("APT");
		contactDetailsResponse.setPrimaryPhoneNumber("PPN");
		contactDetailsResponse.setPrimaryPhoneType("PPT");
		contactDetailsResponse.setSecondaryPhoneNumber("SPN");
		contactDetailsResponse.setSecondaryPhoneType("SPT");
       
		Assert.assertEquals("APN", contactDetailsResponse.getAdditionalPhoneNumber());
		Assert.assertEquals("APT", contactDetailsResponse.getAdditionalPhoneType());
		Assert.assertEquals("PPN", contactDetailsResponse.getPrimaryPhoneNumber());
		Assert.assertEquals("PPT", contactDetailsResponse.getPrimaryPhoneType());
		Assert.assertEquals("SPN", contactDetailsResponse.getSecondaryPhoneNumber());
		Assert.assertEquals("SPT", contactDetailsResponse.getSecondaryPhoneType());
		}
}
