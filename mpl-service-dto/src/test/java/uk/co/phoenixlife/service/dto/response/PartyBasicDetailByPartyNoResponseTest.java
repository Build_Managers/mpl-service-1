package uk.co.phoenixlife.service.dto.response;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.dashboard.PartyBasicDetailByPartyNoDTO;

public class PartyBasicDetailByPartyNoResponseTest {
	@Test (enabled=true)
	public void partyBasicDetailByPartyNoResponseSuccess() {
		PartyBasicDetailByPartyNoResponse partyBasicDetailByPartyNoResponse = new PartyBasicDetailByPartyNoResponse();
		PartyBasicDetailByPartyNoDTO partyBasicDetailByPartyNoDTO = new PartyBasicDetailByPartyNoDTO();
		partyBasicDetailByPartyNoResponse.setPartyBasicDetailByPartyNo(partyBasicDetailByPartyNoDTO);
		
       
		Assert.assertNotNull(partyBasicDetailByPartyNoResponse.getPartyBasicDetailByPartyNo());
		
		}

}
