package uk.co.phoenixlife.service.dto.response;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.dashboard.PoliciesDetailsDTO;

public class PoliciesDetailsResponseTest {
	@Test (enabled=true)
	public void policiesDetailsResponseSuccess() {
		PoliciesDetailsResponse policiesDetailsResponse = new PoliciesDetailsResponse();
		PoliciesDetailsDTO policiesDetailsDTO = new PoliciesDetailsDTO();
		policiesDetailsResponse.setPolicyDetails(policiesDetailsDTO);
		
       
		Assert.assertNotNull(policiesDetailsResponse.getPolicyDetails());
		
		}

}
