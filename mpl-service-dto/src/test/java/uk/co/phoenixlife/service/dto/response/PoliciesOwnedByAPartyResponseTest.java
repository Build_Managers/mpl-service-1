package uk.co.phoenixlife.service.dto.response;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.dashboard.PoliciesOwnedByAPartyDTO;

public class PoliciesOwnedByAPartyResponseTest {
	@Test (enabled=true)
	public void policiesDetailsResponseSuccess() {
		PoliciesOwnedByAPartyResponse policiesOwnedByAPartyResponse = new PoliciesOwnedByAPartyResponse();
		PoliciesOwnedByAPartyDTO policiesOwnedByAPartyDTO = new PoliciesOwnedByAPartyDTO();
		policiesOwnedByAPartyResponse.setBancsPoliciesOwnedByAParty(policiesOwnedByAPartyDTO);
		
       
		Assert.assertNotNull(policiesOwnedByAPartyResponse.getBancsPoliciesOwnedByAParty());
		
		}

}
