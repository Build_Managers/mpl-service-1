package uk.co.phoenixlife.service.dto.response;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.dashboard.Policy_Detail;

public class PolicyDetailResponseTest {
	@Test (enabled=true)
	public void policyDetailResponseSuccess() {
		PolicyDetailResponse policyDetailResponse = new PolicyDetailResponse();
		Policy_Detail policy_Detail = new Policy_Detail();
		policyDetailResponse.setPolicy_Detail(policy_Detail);
		policyDetailResponse.toString();
		
       
		Assert.assertNotNull(policyDetailResponse.getPolicy_Detail());
		
		}

}
