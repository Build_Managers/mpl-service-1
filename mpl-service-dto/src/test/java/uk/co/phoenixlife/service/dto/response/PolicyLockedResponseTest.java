package uk.co.phoenixlife.service.dto.response;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;


public class PolicyLockedResponseTest {
	@Test (enabled=true)
	public void policyLockedResponseSuccess() {
		PolicyLockedResponse policyLockedResponse = new PolicyLockedResponse();
		List<Long> list = new ArrayList<Long>();
		policyLockedResponse.setPolicyLocked(true);
		policyLockedResponse.setCustomerIdList(list);
		
		
	   Assert.assertNotNull(policyLockedResponse.getCustomerIdList());
       Assert.assertNotNull(policyLockedResponse.isPolicyLocked());
		
		
		}
}
