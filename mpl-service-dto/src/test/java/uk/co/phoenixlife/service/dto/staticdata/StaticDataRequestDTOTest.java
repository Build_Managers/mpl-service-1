package uk.co.phoenixlife.service.dto.staticdata;

import org.testng.Assert;
import org.testng.annotations.Test;

public class StaticDataRequestDTOTest {
	
	@Test (enabled=true)
	public void staticDataRequestDTOSuccess() {
		StaticDataRequestDTO staticDataRequestDTO = new StaticDataRequestDTO();
		
		staticDataRequestDTO.setStartFlag(true);
		staticDataRequestDTO.setGroupId("GI123");
		staticDataRequestDTO.setKeyValue("KV123");
		staticDataRequestDTO.toString();
	
		
		Assert.assertEquals("GI123", staticDataRequestDTO.getGroupId());
		Assert.assertEquals("KV123", staticDataRequestDTO.getKeyValue());
		Assert.assertNotNull(staticDataRequestDTO.getStartFlag());
		

	}

}
