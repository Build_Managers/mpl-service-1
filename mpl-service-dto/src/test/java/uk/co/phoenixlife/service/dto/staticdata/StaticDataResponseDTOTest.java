package uk.co.phoenixlife.service.dto.staticdata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;


public class StaticDataResponseDTOTest {
	
	@Test (enabled=true)
	public void staticIDVDataDTOSuccess() {
		StaticDataResponseDTO staticDataResponseDTO = new StaticDataResponseDTO();
		List<StaticIDVDataDTO> list = new ArrayList<StaticIDVDataDTO>();
		staticDataResponseDTO.setStaticIDVDataList(list);
		Map<String, String> map = new HashMap<String, String>();
		staticDataResponseDTO.setStaticDataMap(map);
		staticDataResponseDTO.setStaticValue("SV123");
		staticDataResponseDTO.toString();
		
		
		Assert.assertEquals("SV123", staticDataResponseDTO.getStaticValue());
		Assert.assertNotNull(staticDataResponseDTO.getStaticIDVDataList());
		Assert.assertNotNull(staticDataResponseDTO.getStaticDataMap());


	}

}
