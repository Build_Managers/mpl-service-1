package uk.co.phoenixlife.service.dto.stubs;

import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class ServiceStubTest {
	@InjectMocks
	ServiceStub serviceStub;
	
	@BeforeTest
	public void runBeforeEveryTest()
    {
        MockitoAnnotations.initMocks(this);
        
    }
	
	 @AfterTest
     public void runAfterEveryTest()
     {
    	 serviceStub = null;
     }
	 
	@Test
	public void testStubBankWizardValidationTrue() {
		final String sortCode = "938611";
		final String accountNumber = "12345678";
		ServiceStub.stubBankWizardValidation(sortCode, accountNumber);
	}
	
	@Test
	public void testStubBankWizardValidationFlase() {
		final String sortCode = "1234";
		final String accountNumber = "5678";
		ServiceStub.stubBankWizardValidation(sortCode, accountNumber);
	}
	
	@Test
	public void testStubBankWizardValidationTrueFalse() {
		final String sortCode = "938611";
		final String accountNumber = "5678";
		ServiceStub.stubBankWizardValidation(sortCode, accountNumber);
	}
	
	@Test
	public void testStubBankWizardValidationFalseTrue() {
		final String sortCode = "123";
		final String accountNumber = "12345678";
		ServiceStub.stubBankWizardValidation(sortCode, accountNumber);
	}

}
