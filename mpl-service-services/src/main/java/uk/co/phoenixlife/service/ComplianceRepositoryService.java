/*
 * ComplianceRepositoryService.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import uk.co.phoenixlife.service.dao.entity.MPLComplianceAudit;
import uk.co.phoenixlife.service.dao.repository.ComplianceRepositoryImpl;
import uk.co.phoenixlife.service.dto.compliance.ComplianceDTO;
import uk.co.phoenixlife.service.utils.DateUtils;

/**
 * ComplianceRepositoryService.java
 */
@Component
public class ComplianceRepositoryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ComplianceRepositoryService.class);

    @Autowired
    private ComplianceRepositoryImpl complianceRepositoryImpl;

    /**
     * Method to save the compliance data to DB
     *
     * @param complianceList
     *            List of compliance Object
     * @return Flag (true/ false) about DB entry
     */
    @Transactional
    public Boolean saveComplianceData(final List<ComplianceDTO> complianceList) {
        LOGGER.debug("Inserting compliance data for MPL  --->>>>");
        MPLComplianceAudit entityResp = null;
        Boolean result = true;
        for (final ComplianceDTO compliance : complianceList) {
            MPLComplianceAudit entity;
            entity = getComplianceAuditObj();

            entity.setSessionId(compliance.getSessionId());
            entity.setEventCode(compliance.getEventCode());
            entity.setEventVal(compliance.getEventVal().getBytes());
            entity.setEventTs(DateUtils.getCurrentTimestamp());

            try {
                entityResp = complianceRepositoryImpl.saveComplianceData(entity);
            } catch (Exception exception) {
                LOGGER.error("Insertion of Compliance data failed with exception {}, retrying again for X-GUID {}",
                        exception, compliance.getSessionId());
                try {
                    entityResp = complianceRepositoryImpl.saveComplianceData(entity);
                } catch (Exception exception1) {
                    LOGGER.error("Insertion of Compliance data failed again with exception {}, for X-GUID {}", exception1,
                            compliance.getSessionId());
                }
            }

            if (entityResp == null) {
                result = false;
                LOGGER.error("DB Insert query failed with X-GUID {}", compliance.getSessionId());
            }
        }
        return result;
    }

    private MPLComplianceAudit getComplianceAuditObj() {
        return new MPLComplianceAudit();

    }

}
