/*
 * PolicyRepositoryService.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import uk.co.phoenixlife.service.dao.entity.MPLMiDropOut;
import uk.co.phoenixlife.service.dao.entity.MPLMiErrorMessage;
import uk.co.phoenixlife.service.dao.repository.MiDropOutReportRepository;
import uk.co.phoenixlife.service.dao.repository.MiDropOutRepositoryImpl;
import uk.co.phoenixlife.service.dao.repository.MiErrorMessageReportRepository;
import uk.co.phoenixlife.service.dto.ErrorListDTO;
import uk.co.phoenixlife.service.dto.MPLDropoutDTO;
import uk.co.phoenixlife.service.utils.CommonUtils;

/**
 * PolicyRepositoryService.java
 */
@Component
public class PolicyRepositoryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PolicyRepositoryService.class);

    @Autowired
    private MiDropOutReportRepository miDropOutReportRepository;

    @Autowired
    private MiDropOutRepositoryImpl miDropOutRepositoryImpl;

    @Autowired
    private MiErrorMessageReportRepository miErrorRepository;

    /**
     * Method to submit ErrorListDTO
     *
     * @param errorListDTO
     *            - errorListDTO
     */
    @Transactional
    public void submitErrorList(final List<ErrorListDTO> errorListDTO) {
        LOGGER.debug("Inserting submitErrorList detail for MPL ---->>>>");
        MPLMiErrorMessage mplMiError;
        mplMiError = new MPLMiErrorMessage();
        for (final ErrorListDTO error : errorListDTO) {
            if (error.getMessageCount() == 0) {
                LOGGER.debug("MessageCount is Less than Zero CANNOT PROCCED OBJECT=>{}", error);
            } else {
                if (checkAlreadyExists(error.getPageId(), error.getMessageId(), Date.valueOf(LocalDate.now()))) {

                    LOGGER.debug("Error Message already exists Update query fired Error OBJECT=>{}", error);
                    miErrorRepository.updateErrorMessageCount(Date.valueOf(LocalDate.now()), error.getMessageId(),
                            error.getPageId(), error.getMessageCount(), Timestamp.valueOf(LocalDateTime.now()));
                    LOGGER.info("Error message updated with pageID:{}, errorMessageId:{}, errorCount:{} ", error.getPageId(),
                            error.getMessageId(), error.getMessageCount());
                } else {
                    mplMiError.setPageId(error.getPageId());
                    mplMiError.setMessageId(error.getMessageId());
                    mplMiError.setMessageCount(error.getMessageCount());
                    mplMiError.setErrorDate(Date.valueOf(LocalDate.now()));
                    mplMiError.setCreatedOn(Timestamp.valueOf(LocalDateTime.now()));
                    miErrorRepository.saveAndFlush(mplMiError);
                    LOGGER.info("Error message inserted with pageID:{}, errorMessageId:{}, errorCount:{} ",
                            error.getPageId(), error.getMessageId(), error.getMessageCount());
                }

            }
        }

    }

    /**
     * Method to submit dropOutList
     *
     * @param dropOutDTO
     *            - dropOutList
     */
    @Transactional
    public void submitDropOut(final MPLDropoutDTO dropOutDTO) {
        LOGGER.info("XGUID {} --> Inserting submitDropOut detail for Page Id {}", dropOutDTO.getSessionId(),
                dropOutDTO.getPageId());

        MPLMiDropOut mplMiDropOut;
        mplMiDropOut = new MPLMiDropOut();
        mplMiDropOut.setCreatedOn(Timestamp.valueOf(LocalDateTime.now()));
        mplMiDropOut.setDropOutDate(Date.valueOf(LocalDate.now()));
        mplMiDropOut.setPageId(dropOutDTO.getPageId());
        mplMiDropOut.setTimeSpent(dropOutDTO.getTimeSpent());
        mplMiDropOut.setEmailAddress(dropOutDTO.getEmailAddress().getBytes());
        mplMiDropOut.setSessionId(dropOutDTO.getSessionId());

        MPLMiDropOut mplMiDropOutNextPage;
        mplMiDropOutNextPage = new MPLMiDropOut();
        mplMiDropOutNextPage.setCreatedOn(Timestamp.valueOf(LocalDateTime.now()));
        mplMiDropOutNextPage.setDropOutDate(Date.valueOf(LocalDate.now()));
        mplMiDropOutNextPage.setPageId(dropOutDTO.getNextPageId());
        mplMiDropOutNextPage.setTimeSpent(0);
        mplMiDropOutNextPage.setEmailAddress(dropOutDTO.getEmailAddress().getBytes());
        mplMiDropOutNextPage.setSessionId(dropOutDTO.getSessionId());

        final boolean alreadyExistsflagCurrentPage = checkMiEntryExists(mplMiDropOut.getSessionId(),
                mplMiDropOut.getPageId(), mplMiDropOut.getDropOutDate());
        final boolean alreadyExistsflagNextPage = checkMiEntryExists(mplMiDropOutNextPage.getSessionId(),
                mplMiDropOutNextPage.getPageId(), mplMiDropOutNextPage.getDropOutDate());

        if (CommonUtils.stringNotEmptyCheck(mplMiDropOutNextPage.getPageId()) &&
                CommonUtils.stringNotEmptyCheck(mplMiDropOut.getPageId())) {
            if (alreadyExistsflagCurrentPage) {
                miDropOutRepositoryImpl.updateDropOutTimeSpent(mplMiDropOut.getTimeSpent(), dropOutDTO.getEmailAddress(),
                        mplMiDropOut.getPageId(), mplMiDropOut.getSessionId(), mplMiDropOut.getDropOutDate());
                LOGGER.info("XGUID {} --> Dropout data updated for Page Id: {}", dropOutDTO.getSessionId(),
                        mplMiDropOut.getPageId());
            } else {
                miDropOutRepositoryImpl.saveDropOutData(mplMiDropOut);
                LOGGER.info("XGUID {} --> Dropout data inserted for Page Id: {}", dropOutDTO.getSessionId(),
                        mplMiDropOut.getPageId());
            }

            if (alreadyExistsflagNextPage) {
                miDropOutRepositoryImpl.updateDropOutTimeSpent(mplMiDropOutNextPage.getTimeSpent(),
                        dropOutDTO.getEmailAddress(),
                        mplMiDropOutNextPage.getPageId(), mplMiDropOutNextPage.getSessionId(),
                        mplMiDropOutNextPage.getDropOutDate());
                LOGGER.info("XGUID {} --> Dropout data updtaed for Next Page Id: {}", dropOutDTO.getSessionId(),
                        mplMiDropOutNextPage.getPageId());
            } else {
                miDropOutRepositoryImpl.saveDropOutData(mplMiDropOutNextPage);
                LOGGER.info("XGUID {} --> Dropout data inserted for Next Page Id: {}", dropOutDTO.getSessionId(),
                        mplMiDropOutNextPage.getPageId());
            }
        } else {
            LOGGER.info("Insert Dropout data call skipped because pageID- {} and nextPageID-{}", mplMiDropOut.getPageId(),
                    mplMiDropOutNextPage.getPageId());
        }

        LOGGER.debug("Submit Drop Out detail Inserted----->{} ---->>>>", mplMiDropOut);

    }

    /**
     * Method to check Mi dropout entry is already exists or NOT
     *
     * @param pageId
     *            - pageId
     * @param msgId
     *            - msgId
     * @param errorDate
     *            - errorDate
     * @return @link - boolean
     */
    private boolean checkMiEntryExists(final String sessionId, final String pageId,
            final Date miDate) {

        boolean flag = false;
        MPLMiDropOut.MPLMiDropOutId dropOutCompositeKey;
        dropOutCompositeKey = new MPLMiDropOut.MPLMiDropOutId();
        dropOutCompositeKey.setSessionId(sessionId);
        dropOutCompositeKey.setDropOutDate(miDate);
        dropOutCompositeKey.setPageId(pageId);

        flag = miDropOutReportRepository.exists(dropOutCompositeKey);

        return flag;
    }

    /**
     * Method to check Error Object already exists or NOT
     *
     * @param pageId
     *            - pageId
     * @param msgId
     *            - msgId
     * @param errorDate
     *            - errorDate
     * @return @link - boolean
     */
    private boolean checkAlreadyExists(final String pageId, final String msgId, final Date errorDate) {

        boolean flag;
        MPLMiErrorMessage.MPLMiErrorMessageId errorId;
        errorId = new MPLMiErrorMessage.MPLMiErrorMessageId();
        errorId.setErrorDate(errorDate);
        errorId.setMessageId(msgId);
        errorId.setPageId(pageId);
        flag = miErrorRepository.exists(errorId);
        LOGGER.debug("checkAlreadyExists =>{}", flag);
        return flag;
    }

}
