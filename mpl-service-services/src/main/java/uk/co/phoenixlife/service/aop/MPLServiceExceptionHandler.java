/*
 * MPLServiceExceptionHandler.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.aop;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import uk.co.phoenixlife.service.dto.MPLException;
import uk.co.phoenixlife.service.dto.error.MPLServiceErrorDTO;

/**
 * MPLServiceExceptionHandler.java
 */
@ControllerAdvice
public class MPLServiceExceptionHandler {
    // extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(MPLServiceExceptionHandler.class);

    /**
     * Error handler for generic exception
     *
     * @param exception
     *            - {@link Exception}
     * @param request
     *            - {@link WebRequest}
     * @return - {@link ResponseEntity}
     */
    @ExceptionHandler(MPLException.class)
    public ResponseEntity<MPLServiceErrorDTO> handleMPLException(final Exception exception,
            final WebRequest request) {

        LOGGER.error("Exception occurred : {} ", exception.getMessage(), exception);
        MPLServiceErrorDTO serviceError;

        serviceError = new MPLServiceErrorDTO();

        serviceError.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
        serviceError.setErrorDescription("Internal server error occurred.");
        serviceError.setRequestUrl(request.toString());
        return new ResponseEntity<MPLServiceErrorDTO>(serviceError, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    /**
     * Error handler for generic exception
     *
     * @param exception
     *            - {@link Exception}
     * @param request
     *            - {@link WebRequest}
     * @return - {@link ResponseEntity}
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<MPLServiceErrorDTO> handleGenericException(final Exception exception,
            final WebRequest request) {

        LOGGER.error("Exception occurred : {} ", exception.getMessage(), exception);
        MPLServiceErrorDTO serviceError;

        serviceError = new MPLServiceErrorDTO();

        serviceError.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
        serviceError.setErrorDescription("Internal server error occurred.");
        serviceError.setRequestUrl(request.toString());
        return new ResponseEntity<MPLServiceErrorDTO>(serviceError, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
