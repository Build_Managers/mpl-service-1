package uk.co.phoenixlife.service.async;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import uk.co.phoenixlife.service.client.bancs.BancsServiceClient;
import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dto.policy.PartyBasicOwnerDTO;
import uk.co.phoenixlife.service.dto.policy.PartyBasicOwnerListDTO;
import uk.co.phoenixlife.service.dto.response.BancsPartyBasicDetailResponse;
import uk.co.phoenixlife.service.utils.CommonUtils;

/**
 * AsyncService for PolicyActivation Journey PolicyActivationAsyncService.java
 */
@Component
public class AccountActivationAsyncService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountActivationAsyncService.class);
    @Autowired
    private BancsServiceClient bancsServiceClient;

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Async execution of initial BancsCalls
     *
     * @param policyNumber
     *            policyNumber of the user
     * @param xGUID
     *            xGUID of the user
     * @throws Exception
     *             exception
     */
    @Async
    public void initiateBancsCallForAccountActivation(final String policyNumber, final String xGUID) throws Exception {
    	LOGGER.info("initiateBancsCallForAccountActivation, first intiating PartyBasicDetail call");
        final String response = bancsServiceClient.getPartyBasicDetailResponse(policyNumber, xGUID);
        BancsPartyBasicDetailResponse partyBasicResp;
        if (!response.contains(MPLServiceConstants.ERRORSTRING)) {
        	LOGGER.info("BancsPartyBasicDetailResponse successfull");
            partyBasicResp = objectMapper.readValue(response, BancsPartyBasicDetailResponse.class);

            final List<PartyBasicOwnerListDTO> partyBasicOwnerListList = partyBasicResp.getBancsPartyBasicDetail()
                    .getListOfPolicies();
            final List<PartyBasicOwnerDTO> partyBasicOwnerList;
            if (CommonUtils.isValidList(partyBasicOwnerListList) && partyBasicOwnerListList.size() == 1) {
            	LOGGER.debug("BancsPartyBasicDetailResponse ownerList is valid");
                partyBasicOwnerList = partyBasicOwnerListList.get(0).getListOfOwners();
                if (CommonUtils.isValidList(partyBasicOwnerList) && partyBasicOwnerList.size() == 1) {
                    final String bancsUniqueCustNo = partyBasicOwnerList.get(0).getBancsUniqueCustNo();
                    LOGGER.debug("Initiating PartyComDetails and PoliciesOwnedByParty call with bancsUniqueCustNo {}",bancsUniqueCustNo);
                    bancsServiceClient.getPartyCommDetailsResponse(bancsUniqueCustNo, xGUID);
                    bancsServiceClient.getPartyBasicDetailByPartyNoResponse(bancsUniqueCustNo, xGUID);
                    bancsServiceClient.policiesOwnedByParty(bancsUniqueCustNo, xGUID);
                }

            }
        }
        else{
        	LOGGER.info("BancsPartyBasicDetailResponse failed {}",response);
        }
    }

    /**
     * Async execution of bancsPartyBasicDetail
     *
     * @param policyNumber
     *            policyNumber of user
     * @param xGUID
     *            xGUID of user
     * @return BancsPartyBasicDetailResponse
     * @throws Exception
     *             exception
     *//*
       * @Async public Future<BancsPartyBasicDetailResponse>
       * getPartyBasicDetail(final String policyNumber, final String xGUID)
       * throws Exception { Thread.sleep(10000);
       * LOGGER.info("Calling PartyBasicDetails"); final
       * BancsPartyBasicDetailResponse bancsPartyBasicDetailResp =
       * bancsServiceClient .getPartyBasicDetailResponse(policyNumber, xGUID);
       * if (CommonUtils.isObjectNotNull(bancsPartyBasicDetailResp) &&
       * CommonUtils.isObjectNotNull(bancsPartyBasicDetailResp.
       * getBancsPartyBasicDetail())) { final int policyCount =
       * bancsPartyBasicDetailResp.getBancsPartyBasicDetail().getListOfPolicies(
       * ).size(); LOGGER.info("Received response {} from BaNCS for xGUID: {}",
       * policyCount, xGUID); if (policyCount == 1) {
       * partyCommThreadCall(bancsPartyBasicDetailResp.getBancsPartyBasicDetail(
       * ).getListOfPolicies(), xGUID); } else if (policyCount > 1) {
       * LOGGER.info("Multiple Policy hence cannot call partycomm immediatly");
       * } } else {
       * LOGGER.error("Received NULL Response from BaNCS Client for xGUID: {}",
       * xGUID); } return new
       * AsyncResult<BancsPartyBasicDetailResponse>(bancsPartyBasicDetailResp);
       * }
       */

    /**
     * Async execution of bancsIDAndVData
     *
     * @param policyNumber
     *            policyNumber of user
     * @param xGUID
     *            xGUID of user
     * @throws Exception
     *             exception
     */
    @Async
    public void getIDAndVData(final String policyNumber, final String xGUID)
            throws Exception {
        bancsServiceClient.getIDAndVDataResponse(policyNumber, xGUID);
    }
	
	@Async
    public void getPoliciesOwnedByParty(final String uniqueCustomerNumber, final String xGUID)
            throws Exception {
        bancsServiceClient.policiesOwnedByParty(uniqueCustomerNumber, xGUID);
    }

    /**
     * Async execution of bancsPartyCommDetails
     *
     * @param bancsUniqueCustNo
     *            bancsUniqueCustNo of the customer
     * @param xGUID
     *            xGUID
     * @throws Exception
     *             Exception
     */
    @Async
    public void getPartyCommDetails(final String bancsUniqueCustNo, final String xGUID)
            throws Exception {
    	LOGGER.info("Inside getPartyCommDetails");
        bancsServiceClient.getPartyCommDetailsResponse(bancsUniqueCustNo, xGUID);
    }
    /*
     * private void partyCommThreadCall(final List<PartyBasicOwnerListDTO>
     * listOfPartyBasicOwner, final String xGUID) throws Exception { if
     * (CommonUtils.isValidList(listOfPartyBasicOwner)) { final
     * PartyBasicOwnerListDTO partyBasicOwnerDTOList =
     * listOfPartyBasicOwner.get(0); List<PartyBasicOwnerDTO> partyOwnerList;
     * partyOwnerList = partyBasicOwnerDTOList.getListOfOwners(); if
     * (CommonUtils.isValidList(partyOwnerList) && partyOwnerList.size() == 1) {
     * final PartyBasicOwnerDTO ownerDTO = partyOwnerList.get(0); if
     * (CommonUtils.isObjectNotNull(ownerDTO)) { final String bancsUniqueCustNo
     * = ownerDTO.getBancsUniqueCustNo(); getPartyCommDetails(bancsUniqueCustNo,
     * xGUID); } else { LOGGER.
     * error("{} PartyBasicOwnerDTO is NULL required to get bancsUniquesCustNo for PartComDetails"
     * , xGUID); } } else if (partyOwnerList.size() > 1) {
     * LOGGER.error("SYSTEM EXCEPTION"); } } }
     */

    /**
     * Async execution of PartyCommDetails
     *
     * @param bancsUniqueCustNo
     *            uniqueCustNo
     * @param xGUID
     *            xGUID
     * @return partyCommDetail
     * @throws Exception
     *             exception
     */
    /*
     * @Async public BancsPartyCommDetailsResponse getPartyCommDetails(final
     * String bancsUniqueCustNo, final String xGUID) throws Exception {
     * LOGGER.info("Calling PartyCommDetails"); final
     * BancsPartyCommDetailsResponse bancsPartyCommDetailsResp =
     * bancsServiceClient .getPartyCommDetailsResponse(bancsUniqueCustNo,
     * xGUID); if (CommonUtils.isObjectNotNull(bancsPartyCommDetailsResp) &&
     * CommonUtils.isObjectNotNull(bancsPartyCommDetailsResp.
     * getBancsPartyCommDetails())) {
     * LOGGER.info("Received response {} from BaNCS for xGUID: {}",
     * bancsPartyCommDetailsResp.getBancsPartyCommDetails().getCountry(),
     * xGUID); } else {
     * LOGGER.error("Received NULL Response from BaNCS Client for xGUID: {}",
     * xGUID); } return bancsPartyCommDetailsResp; }
     */

}
