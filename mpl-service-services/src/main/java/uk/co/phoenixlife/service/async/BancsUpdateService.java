/*
 * BancsUpdateService.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.async;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpServerErrorException;

import com.fasterxml.jackson.databind.ObjectMapper;

import uk.co.phoenixlife.service.client.bancs.BancsUpdateServiceClient;
import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dao.entity.MPLCustomer;
import uk.co.phoenixlife.service.dao.entity.MPLCustomerPolicy;
import uk.co.phoenixlife.service.dao.entity.MPLCustomerUpdateStatus;
import uk.co.phoenixlife.service.dao.entity.MPLDocumentAudit;
import uk.co.phoenixlife.service.dao.entity.MPLDocumentUpdateStatus;
import uk.co.phoenixlife.service.dao.entity.MPLPolicyEncashment;
import uk.co.phoenixlife.service.dao.entity.MPLQuoteUpdateStatus;
import uk.co.phoenixlife.service.dao.repository.CustomerRepositoryImpl;
import uk.co.phoenixlife.service.dao.repository.CustomerUpdateStatusRepository;
import uk.co.phoenixlife.service.dao.repository.DocumentUpdateStatusRepository;
import uk.co.phoenixlife.service.dao.repository.EmailStatusRepository;
import uk.co.phoenixlife.service.dao.repository.PolicyEncashmentRepository;
import uk.co.phoenixlife.service.dao.repository.QuoteDetailsRepositoryImpl;
import uk.co.phoenixlife.service.dao.repository.QuoteUpdateStatusRepository;
import uk.co.phoenixlife.service.dto.bancspost.BancsCreateQuoteRequest;
import uk.co.phoenixlife.service.dto.bancspost.BancsCreateQuoteResponse;
import uk.co.phoenixlife.service.dto.bancspost.BancsEmailPhoneUserIdRequest;
import uk.co.phoenixlife.service.dto.bancspost.BancsLogCorrespondenceRequest;
import uk.co.phoenixlife.service.dto.bancspost.BancsUpdateAttemptDTO;
import uk.co.phoenixlife.service.dto.policy.QuoteDetails;
import uk.co.phoenixlife.service.dto.response.ErrorResponseDTO;
import uk.co.phoenixlife.service.mapper.BancsUpdateRequestMapper;
import uk.co.phoenixlife.service.mapper.UpdateAttemptsMapper;
import uk.co.phoenixlife.service.utils.CommonUtils;
import uk.co.phoenixlife.service.utils.DateUtils;

/**
 * BancsUpdateService.java
 */
@Component("bancsUpdateService")
public class BancsUpdateService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BancsUpdateService.class);

    @Value("${batch.attempts}")
    private String attempts;

    @Value("${batch.delayMinutes}")
    private String delayMinutes;

    @Autowired
    private BancsUpdateServiceClient bancsUpdateClient;

    @Autowired
    private CustomerUpdateStatusRepository custUpdateStatusRepo;
    @Autowired
    private QuoteUpdateStatusRepository qteUpdateStatusRepo;
    @Autowired
    private DocumentUpdateStatusRepository docUpdateStatusRepo;

    @Autowired
    private CustomerRepositoryImpl customerRepository;
    @Autowired
    private QuoteDetailsRepositoryImpl qteDetailRepository;

    @Autowired
    private PolicyEncashmentRepository encashmentRepo;
    @Autowired
    private EmailStatusRepository emailStatusRepo;

    @Autowired
    private ObjectMapper objMapper;

    /**
     * Method to send personal detail changes to BANCs
     *
     * @param customerUpdateStatus
     *            - MPLCustomerUpdateStatus object
     * @param updateType
     *            - (encashment | userid)
     * @param xGUID
     *            - unique UUID identifier
     * @throws Exception
     *             - {@link Exception}
     */
    @Async
    @Transactional
    public void sendPersonalDetailUpdate(final MPLCustomerUpdateStatus customerUpdateStatus, final String updateType,
            final String xGUID) throws Exception {
        getCustUpdateDetailsAndSendToBancs(customerUpdateStatus, false, updateType, xGUID);
    }

    /**
     * Method to send quote details to BANCs
     *
     * @param quote
     *            - QuoteDetails object
     * @param mplPolicy
     *            - MPLCustomerPolicy object
     * @param bancsQuoteAttempt
     *            - BancsUpdateAttemptDTO object
     * @param xGUID
     *            - unique UUID identifier
     * @return - (0: validation failure | 1: internal server error| 2: success)
     * @throws Exception
     *             - {@link Exception}
     */
    @Transactional
    public int sendQuoteDetails(final QuoteDetails quote, final MPLCustomerPolicy mplPolicy,
            final BancsUpdateAttemptDTO bancsQuoteAttempt, final String xGUID) throws Exception {

        int status;
        String bancsResponse;

        bancsQuoteAttempt.setUpdateTmst(DateUtils.getCurrentDateTime());
        bancsResponse = bancsUpdateClient.sendQuoteDetails(
                BancsUpdateRequestMapper.mapToBancsCreateQuote(quote),
                mplPolicy.getBancsPolNum(), mplPolicy.getPolicyCustomer().getBancsCustNum(), xGUID);
        bancsQuoteAttempt.setAckTmst(DateUtils.getCurrentDateTime());

        status = validateQuoteDetails(bancsResponse, xGUID, mplPolicy, quote, bancsQuoteAttempt);

        return status;
    }

    private int validateQuoteDetails(final String bancsResponse, final String xGUID, final MPLCustomerPolicy mplPolicy,
            final QuoteDetails quote, final BancsUpdateAttemptDTO bancsQuoteAttempt) throws Exception {

        int status;
        ErrorResponseDTO error;
        BancsCreateQuoteResponse createQuoteResponse;

        if (StringUtils.containsIgnoreCase(bancsResponse, MPLServiceConstants.ERRORSTRING)) {
            error = objMapper.readValue(bancsResponse, ErrorResponseDTO.class);
            if (HttpStatus.INTERNAL_SERVER_ERROR.toString()
                    .equalsIgnoreCase(error.getErrorDetail().getInternalErrorCode())) {
                LOGGER.error("[{}]:|{}|- bancsCreateQuoteAPI server error for mpl_policy -> {} : {}",
                        MPLConstants.REAL_TIME, xGUID, mplPolicy.getPolicyId(), bancsResponse);
                status = 1;
                bancsQuoteAttempt.setUpdateStatus(MPLServiceConstants.FAILURE);
                bancsQuoteAttempt.setAckFlg(MPLServiceConstants.NO);
                bancsQuoteAttempt.setFailureReason(error.getErrorDetail().getErrorDesciption());
                bancsQuoteAttempt.setAckTmst(null);
            } else {
                LOGGER.error("[{}]:|{}|- bancsCreateQuoteAPI validation failure for mpl_policy -> {} : {}",
                        MPLConstants.REAL_TIME, xGUID, mplPolicy.getPolicyId(), bancsResponse);
                status = 0;
            }
        } else {
            LOGGER.info("[{}]:|{}|- bancsCreateQuoteAPI validation successfully for mpl_policy -> {}",
                    MPLConstants.REAL_TIME, xGUID, mplPolicy.getPolicyId());
            status = 2;

            createQuoteResponse = objMapper.readValue(bancsResponse, BancsCreateQuoteResponse.class);

            bancsQuoteAttempt.setUpdateStatus(MPLServiceConstants.SUCCESS);
            bancsQuoteAttempt.setAckFlg(MPLServiceConstants.YES);
            bancsQuoteAttempt.setQuoteRef(createQuoteResponse.getBancsCreateQuote().getQuoteRef());
        }
        return status;
    }

    /**
     * Method to send log correspondence to BANCs
     *
     * @param doc
     *            - MPLDocumentAudit object
     * @param mplPolicy
     *            - MPLCustomerPolicy object
     * @param docType
     *            - (RP : RetirementPack| AF: ApplicationForm)
     * @param xGUID
     *            - unique UUID identifier
     * @throws Exception
     *             - {@link Exception}
     */
    @Async
    @Transactional
    public void sendDocumentUpdate(final MPLDocumentAudit doc, final MPLCustomerPolicy mplPolicy, final String docType,
            final String xGUID) throws Exception {

        LOGGER.info("[{}]:|{}|- bancsLogCorrespondenceAPI for {} of mpl_policy {}", MPLServiceConstants.REAL_TIME,
                xGUID, docType, mplPolicy.getPolicyId());

        MPLCustomer mplCustomer;
        BancsLogCorrespondenceRequest bancsLogCorrespondenceRequest;

        mplCustomer = mplPolicy.getPolicyCustomer();
        bancsLogCorrespondenceRequest = BancsUpdateRequestMapper.mapToBancsLogRequest(mplCustomer.getBancsCustNum(),
                mplCustomer.getLegacyCustNum(), new ArrayList<MPLDocumentAudit>(Collections.singletonList(doc)));

        LOGGER.warn("JSON {}", objMapper.writerWithDefaultPrettyPrinter().writeValueAsString(bancsLogCorrespondenceRequest));

        sendLogCorrespondence(bancsLogCorrespondenceRequest, mplPolicy.getBancsPolNum(),
                false, xGUID, mplPolicy.getPolicyId(),
                new ArrayList<MPLDocumentUpdateStatus>(Collections.singletonList(doc.getDocumentUpdateStatus())));
    }

    /**
     * Method to process failed BANCs update
     *
     * @throws Exception
     *             - {@link Exception}
     */
    @Transactional
    public void bancsUpdateBatch() throws Exception {

        int maxAttempts;
        Timestamp maxTmst;

        List<MPLCustomerUpdateStatus> custFailedUpdates;
        List<MPLCustomerUpdateStatus> custFailedUpdatesForEncashment;
        List<MPLQuoteUpdateStatus> quoteFailedUpdates;
        List<MPLCustomerPolicy> docFailedUpdates;

        maxAttempts = Integer.valueOf(attempts);
        maxTmst = DateUtils.formatDateTime(DateUtils.getCurrentLocalDateTime().minusMinutes(Long.valueOf(delayMinutes)));

        custFailedUpdates = customerRepository.getFailedPersonalDetailsUpdates(maxAttempts, maxTmst);
        custFailedUpdatesForEncashment = customerRepository.getFailedCustUpdates(maxAttempts, maxTmst);
        quoteFailedUpdates = qteDetailRepository.getFailedQuoteUpdates(maxAttempts, maxTmst);
        docFailedUpdates = customerRepository.getFailedDocUpdates(maxAttempts, maxTmst);

        processFailedCustUpdates(custFailedUpdates);
        processFailedCustUpdatesForEncashment(custFailedUpdatesForEncashment);
        processFailedQuoteUpdates(quoteFailedUpdates);
        processFailedDocUpdates(docFailedUpdates);

    }

    private void processFailedCustUpdates(final List<MPLCustomerUpdateStatus> custFailedUpdates) {

        custFailedUpdates.forEach(custUpdt -> {
            try {
                getCustUpdateDetailsAndSendToBancs(custUpdt, true, MPLServiceConstants.USERID,
                        CommonUtils.generateUniqueID());
            } catch (Exception ex) {
                LOGGER.error("Exception occurred while processing failed customer update batch request ->", ex);
            }
        });

    }

    private void processFailedCustUpdatesForEncashment(final List<MPLCustomerUpdateStatus> custFailedUpdates) {

        custFailedUpdates.forEach(custUpdt -> {
            try {
                getCustUpdateDetailsAndSendToBancs(custUpdt, true, MPLServiceConstants.ENCASHMENT,
                        getTransactionId(custUpdt.getPolicyId()));
            } catch (Exception ex) {
                LOGGER.error("Exception occurred while processing failed customer update batch request ->", ex);
            }
        });

    }

    private void processFailedQuoteUpdates(final List<MPLQuoteUpdateStatus> quoteFailedUpdates) {

        quoteFailedUpdates.forEach(qteUpdt -> {

            MPLCustomerPolicy policy;
            String transactionId;
            boolean ackFlag;

            try {
                policy = qteUpdt.getUpdatedQuote().getQuotePolicy();
                transactionId = getTransactionId(policy);
                ackFlag = getQteDetailsAndSendToBancs(qteUpdt, true, transactionId);
                if (ackFlag) {
                    getDocUpdateAndSendToBancs(policy, true, transactionId);
                }
            } catch (Exception ex) {
                LOGGER.error("Exception occurred while processing failed quote update batch request ->", ex);
            }
        });
    }

    private void processFailedDocUpdates(final List<MPLCustomerPolicy> docFailedUpdates) {
        docFailedUpdates.forEach(pol -> {
            try {
                getDocUpdateAndSendToBancs(pol, true, getTransactionId(pol));
            } catch (Exception ex) {
                LOGGER.error("Exception occurred while processing failed document update batch request ->", ex);
            }
        });
    }

    private String getTransactionId(final MPLCustomerPolicy mplPolicy) throws Exception {

        MPLPolicyEncashment polEncash;
        String tranxId;

        polEncash = mplPolicy.getPolicyEncashment();

        if (CommonUtils.isObjectNotNull(polEncash)) {
            tranxId = polEncash.getTransactionId();
        } else {
            tranxId = CommonUtils.generateUniqueID();
        }

        return tranxId;
    }

    private String getTransactionId(final long policyId) throws Exception {

        String tranxId;
        tranxId = encashmentRepo.getEncashmentTransactionId(policyId);

        if (CommonUtils.isEmpty(tranxId)) {
            tranxId = CommonUtils.generateUniqueID();
        }

        return tranxId;
    }

    private void getCustUpdateDetailsAndSendToBancs(final MPLCustomerUpdateStatus customerUpdateStatus,
            final boolean batchFlag, final String updateType, final String xGUID) throws Exception {

        MPLCustomer mplCustomer;
        BancsEmailPhoneUserIdRequest bancsEmailPhoneUserIdRequest;

        mplCustomer = customerUpdateStatus.getUpdatedCustomer();
        bancsEmailPhoneUserIdRequest = BancsUpdateRequestMapper
                .mapToBancsEmailPhoneUserIdRequest(mplCustomer, updateType);

        LOGGER.warn("JSON {}", objMapper.writerWithDefaultPrettyPrinter().writeValueAsString(bancsEmailPhoneUserIdRequest));

        sendEmailPhoneUserIdDetails(bancsEmailPhoneUserIdRequest, mplCustomer.getBancsCustNum(), batchFlag, xGUID,
                mplCustomer.getCustomerId(), customerUpdateStatus, updateType);

    }

    private boolean getQteDetailsAndSendToBancs(final MPLQuoteUpdateStatus quoteFailedUpdate, final boolean batchFlag,
            final String xGUID) throws Exception {

        MPLCustomerPolicy mplPolicy;
        MPLCustomer mplCustomer;
        BancsCreateQuoteRequest bancsCreateQuoteRequest;
        boolean ackFlag;

        mplPolicy = quoteFailedUpdate.getUpdatedQuote().getQuotePolicy();
        mplCustomer = mplPolicy.getPolicyCustomer();
        bancsCreateQuoteRequest = BancsUpdateRequestMapper.mapToBancsCreateQuoteRequest(quoteFailedUpdate.getUpdatedQuote());

        LOGGER.warn("JSON {}", objMapper.writerWithDefaultPrettyPrinter().writeValueAsString(bancsCreateQuoteRequest));

        ackFlag = sendQuoteDetails(bancsCreateQuoteRequest, mplPolicy.getBancsPolNum(), mplCustomer.getBancsCustNum(),
                batchFlag, xGUID, mplPolicy.getPolicyId(), quoteFailedUpdate);

        return ackFlag;
    }

    private void getDocUpdateAndSendToBancs(final MPLCustomerPolicy mplPolicy, final boolean batchFlag,
            final String xGUID) throws Exception {

        MPLCustomer mplCustomer;
        List<MPLDocumentAudit> mplDocuments;
        BancsLogCorrespondenceRequest bancsLogCorrespondenceRequest;
        List<MPLDocumentUpdateStatus> docUpdateStatusList;

        mplCustomer = mplPolicy.getPolicyCustomer();

        mplDocuments = mplPolicy.getPolicyDocuments().stream()
                .filter(doc -> doc.getDocumentUpdateStatus().getAttempts() < Integer.valueOf(attempts)
                        && doc.getDocumentUpdateStatus().getTimestamp()
                                .before(DateUtils.formatDateTime(
                                        DateUtils.getCurrentLocalDateTime().minusMinutes(Long.valueOf(delayMinutes))))
                        && (doc.getDocumentUpdateStatus().getStatus() == 'F'
                                || doc.getDocumentUpdateStatus().getStatus() == ' '))
                .collect(Collectors.toList());

        bancsLogCorrespondenceRequest = BancsUpdateRequestMapper.mapToBancsLogRequest(mplCustomer.getBancsCustNum(),
                mplCustomer.getLegacyCustNum(), mplDocuments);

        LOGGER.warn("JSON {}", objMapper.writerWithDefaultPrettyPrinter().writeValueAsString(bancsLogCorrespondenceRequest));

        docUpdateStatusList = mplDocuments.stream().map(doc -> doc.getDocumentUpdateStatus()).collect(Collectors.toList());

        sendLogCorrespondence(bancsLogCorrespondenceRequest, mplPolicy.getBancsPolNum(), batchFlag, xGUID,
                mplPolicy.getPolicyId(), docUpdateStatusList);
    }

    private Boolean sendEmailPhoneUserIdDetails(final BancsEmailPhoneUserIdRequest bancsEmailPhoneUserIdRequest,
            final String bancsCustomerNumber, final boolean batchFlag, final String xGUID,
            final long customerId, final MPLCustomerUpdateStatus customerUpdateStatus, final String updateType)
            throws Exception {

        LocalDateTime sentTmst;
        LocalDateTime ackTmst;
        String bancsAckResponse;
        Boolean statusFlag;
        Boolean exceptionFlag;

        sentTmst = DateUtils.getCurrentLocalDateTime();
        ackTmst = (LocalDateTime) CommonUtils.getNullObject();
        bancsAckResponse = CommonUtils.getEmptyString();
        exceptionFlag = false;

        try {
            LOGGER.info("[{}]:|{}|- bancsEmailPhoneUseridAPI ({} update) initiated for mpl_customer -> {}",
                    CommonUtils.batchOrRealTime(batchFlag), xGUID, updateType, customerId);

            bancsAckResponse = bancsUpdateClient.sendEmailPhoneUserIdDetails(bancsEmailPhoneUserIdRequest,
                    bancsCustomerNumber, xGUID);

            ackTmst = DateUtils.getCurrentLocalDateTime();
        } catch (HttpServerErrorException ex) {
            exceptionFlag = true;
            bancsAckResponse = new StringBuffer(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())
                    .append(" : ").append(ex.getMessage()).toString();

            LOGGER.error("[{}]:|{}|- bancsEmailPhoneUserIdAPI ({} update) exception for mpl_customer -> {}",
                    CommonUtils.batchOrRealTime(batchFlag), xGUID, updateType, customerId, ex);
        } finally {

            statusFlag = checkBancsEmailPhoneUserIdAck(customerUpdateStatus, sentTmst, ackTmst, batchFlag,
                    xGUID, customerId, bancsAckResponse, exceptionFlag, updateType);
        }

        return statusFlag;
    }

    private Boolean checkBancsEmailPhoneUserIdAck(final MPLCustomerUpdateStatus customerUpdateStatus,
            final LocalDateTime sentTmst, final LocalDateTime ackTmst, final boolean batchFlag, final String xGUID,
            final long customerId, final String bancsResponse, final Boolean exceptionFlag,
            final String updateType) throws Exception {

        boolean ackFlag;
        String failureReason;

        ackFlag = false;

        if (StringUtils.containsIgnoreCase(bancsResponse, MPLServiceConstants.ERRORSTRING)
                || StringUtils.containsIgnoreCase(bancsResponse, HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())) {

            LOGGER.error("[{}]:|{}|- bancsEmailPhoneUserIdAPI ({} update) failure for mpl_customer -> {} : {}",
                    CommonUtils.batchOrRealTime(batchFlag), xGUID, updateType, customerId, bancsResponse);
            if (exceptionFlag) {
                failureReason = bancsResponse;
            } else {
                failureReason = objMapper.readValue(bancsResponse, ErrorResponseDTO.class)
                        .getErrorDetail().getErrorDesciption();
            }

            updateBancsEmailPhoneUserIdAck(customerUpdateStatus, new BancsUpdateAttemptDTO(MPLServiceConstants.FAILURE,
                    sentTmst, MPLServiceConstants.NO, null, failureReason));
        } else {

            LOGGER.info("[{}]:|{}|- bancsEmailPhoneUserIdAPI ({} update) completed successfully for mpl_customer -> {}",
                    CommonUtils.batchOrRealTime(batchFlag), xGUID, updateType, customerId);
            updateBancsEmailPhoneUserIdAck(customerUpdateStatus,
                    new BancsUpdateAttemptDTO(MPLServiceConstants.SUCCESS, sentTmst, MPLServiceConstants.YES, ackTmst,
                            null));
            ackFlag = true;
        }

        return ackFlag;
    }

    private void updateBancsEmailPhoneUserIdAck(final MPLCustomerUpdateStatus customerUpdateStatus,
            final BancsUpdateAttemptDTO updateAck) throws Exception {

        UpdateAttemptsMapper.mapUpdatedCustUpdtStatusAndAttempts(customerUpdateStatus, updateAck);
        custUpdateStatusRepo.saveAndFlush(customerUpdateStatus);

    }

    private Boolean sendQuoteDetails(final BancsCreateQuoteRequest bancsCreateQuoteRequest,
            final String bancsPolicyNumber, final String bancsCustomerNumber, final boolean batchFlag,
            final String xGUID, final long policyId, final MPLQuoteUpdateStatus quoteUpdateStatus) throws Exception {

        LocalDateTime sentTmst;
        LocalDateTime ackTmst;
        String bancsAckResponse;
        Boolean statusFlag;
        Boolean exceptionFlag;

        sentTmst = DateUtils.getCurrentLocalDateTime();
        ackTmst = (LocalDateTime) CommonUtils.getNullObject();
        bancsAckResponse = CommonUtils.getEmptyString();
        exceptionFlag = false;

        try {
            LOGGER.info("[{}]:|{}|- bancsCreateQuoteAPI initiated for mpl_policy -> {}",
                    CommonUtils.batchOrRealTime(batchFlag), xGUID, policyId);
            bancsAckResponse = bancsUpdateClient.sendQuoteDetails(bancsCreateQuoteRequest, bancsPolicyNumber,
                    bancsCustomerNumber, xGUID);
            ackTmst = DateUtils.getCurrentLocalDateTime();
        } catch (HttpServerErrorException ex) {
            exceptionFlag = true;
            bancsAckResponse = new StringBuffer(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())
                    .append(" : ").append(ex.getMessage()).toString();
            LOGGER.error("[{}]:|{}|- bancsCreateQuoteAPI exception for mpl_policy -> {}",
                    CommonUtils.batchOrRealTime(batchFlag), xGUID, policyId, ex);
        } finally {
            statusFlag = checkBancsCreateQuoteAck(quoteUpdateStatus, sentTmst, ackTmst, batchFlag,
                    xGUID, policyId, bancsAckResponse, exceptionFlag);
        }

        return statusFlag;
    }

    private Boolean checkBancsCreateQuoteAck(final MPLQuoteUpdateStatus quoteUpdateStatus, final LocalDateTime sentTmst,
            final LocalDateTime ackTmst, final boolean batchFlag, final String xGUID, final long policyId,
            final String bancsResponse, final Boolean exceptionFlag) throws Exception {

        boolean ackFlag;
        String failureReason;

        ackFlag = false;

        if (StringUtils.containsIgnoreCase(bancsResponse, MPLServiceConstants.ERRORSTRING)
                || StringUtils.containsIgnoreCase(bancsResponse, HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())) {

            if (exceptionFlag) {
                failureReason = bancsResponse;
            } else {
                failureReason = objMapper.readValue(bancsResponse, ErrorResponseDTO.class)
                        .getErrorDetail().getErrorDesciption();
            }

            LOGGER.error("[{}]:|{}|- bancsCreateQuoteAPI failure for mpl_policy -> {} : {}",
                    CommonUtils.batchOrRealTime(batchFlag), xGUID, policyId, bancsResponse);
            updateBancsCreateQuoteAck(quoteUpdateStatus, new BancsUpdateAttemptDTO(MPLServiceConstants.FAILURE, sentTmst,
                    MPLServiceConstants.NO, null, failureReason));

        } else {
            LOGGER.info("[{}]:|{}|- bancsCreateQuoteAPI completed successfully for mpl_policy -> {}",
                    CommonUtils.batchOrRealTime(batchFlag), xGUID, policyId);
            updateQuoteReferenceAndEmailStatus(objMapper.readValue(bancsResponse, BancsCreateQuoteResponse.class)
                    .getBancsCreateQuote().getQuoteRef(), quoteUpdateStatus);
            updateBancsCreateQuoteAck(quoteUpdateStatus, new BancsUpdateAttemptDTO(MPLServiceConstants.SUCCESS, sentTmst,
                    MPLServiceConstants.YES, ackTmst, null));
            ackFlag = true;
        }

        return ackFlag;
    }

    private void updateQuoteReferenceAndEmailStatus(final String qteReference,
            final MPLQuoteUpdateStatus quoteUpdateStatus) {
        MPLPolicyEncashment mplPolicyEncashment;

        mplPolicyEncashment = quoteUpdateStatus.getUpdatedQuote().getQuotePolicy().getPolicyEncashment();
        encashmentRepo.updateQuoteReference(qteReference, mplPolicyEncashment.getEncashmentId());
        emailStatusRepo.updateEmailStatuse(mplPolicyEncashment.getEncashmentId());
    }

    private void updateBancsCreateQuoteAck(final MPLQuoteUpdateStatus quoteUpdateStatus,
            final BancsUpdateAttemptDTO updateAck) throws Exception {

        UpdateAttemptsMapper.mapUpdatedQteUpdtStatusAndAttempts(quoteUpdateStatus, updateAck);
        qteUpdateStatusRepo.saveAndFlush(quoteUpdateStatus);

    }

    private Boolean sendLogCorrespondence(final BancsLogCorrespondenceRequest bancsLogCorrespondenceReq,
            final String bancsPolicyNumber, final boolean batchFlag, final String xGUID,
            final long policyId, final List<MPLDocumentUpdateStatus> docUpdateStatusList) throws Exception {

        LocalDateTime sentTmst;
        LocalDateTime ackTmst;
        String bancsAckResponse;
        Boolean statusFlag;
        Boolean exceptionFlag;

        sentTmst = DateUtils.getCurrentLocalDateTime();
        ackTmst = (LocalDateTime) CommonUtils.getNullObject();
        bancsAckResponse = CommonUtils.getEmptyString();
        exceptionFlag = false;

        try {
            LOGGER.info("[{}]:|{}|- bancsLogCorrespondenceAPI initiated for mpl_policy -> {}",
                    CommonUtils.batchOrRealTime(batchFlag), xGUID, policyId);

            bancsAckResponse = bancsUpdateClient.sendLogCorrespondence(bancsLogCorrespondenceReq, bancsPolicyNumber, xGUID);
            ackTmst = DateUtils.getCurrentLocalDateTime();
        } catch (HttpServerErrorException ex) {
            exceptionFlag = true;
            bancsAckResponse = new StringBuffer(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())
                    .append(" : ").append(ex.getMessage()).toString();
            LOGGER.error("[{}]:|{}|- bancsLogCorrespondenceAPI exception for mpl_policy -> {}",
                    CommonUtils.batchOrRealTime(batchFlag), xGUID, policyId, ex);
        } finally {
            statusFlag = checkBancsLogCorrespondenceAck(docUpdateStatusList, sentTmst, ackTmst, batchFlag,
                    xGUID, policyId, bancsAckResponse, exceptionFlag);
        }

        return statusFlag;
    }

    private Boolean checkBancsLogCorrespondenceAck(final List<MPLDocumentUpdateStatus> docUpdateStatusList,
            final LocalDateTime sentTmst, final LocalDateTime ackTmst, final boolean batchFlag, final String xGUID,
            final long policyId, final String bancsResponse, final Boolean exceptionFlag) throws Exception {

        boolean ackFlag;
        String failureReason;

        ackFlag = false;

        if (StringUtils.containsIgnoreCase(bancsResponse, MPLServiceConstants.ERRORSTRING)
                || StringUtils.containsIgnoreCase(bancsResponse, HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())) {

            if (exceptionFlag) {
                failureReason = bancsResponse;
            } else {
                failureReason = objMapper.readValue(bancsResponse, ErrorResponseDTO.class)
                        .getErrorDetail().getErrorDesciption();
            }

            LOGGER.error("[{}]:|{}|- bancsLogCorrespondenceAPI failure for mpl_policy -> {} : {}",
                    CommonUtils.batchOrRealTime(batchFlag), xGUID, policyId, bancsResponse);
            updateBancsLogCorrespondenceAck(docUpdateStatusList, new BancsUpdateAttemptDTO(MPLServiceConstants.FAILURE,
                    sentTmst, MPLServiceConstants.NO, null, failureReason));
        } else {

            LOGGER.info("[{}]:|{}|- bancsLogCorrespondenceAPI completed successfully for mpl_policy -> {}",
                    CommonUtils.batchOrRealTime(batchFlag), xGUID, policyId);
            updateBancsLogCorrespondenceAck(docUpdateStatusList, new BancsUpdateAttemptDTO(MPLServiceConstants.SUCCESS,
                    sentTmst, MPLServiceConstants.YES, ackTmst, null));
            ackFlag = true;
        }

        return ackFlag;
    }

    private void updateBancsLogCorrespondenceAck(final List<MPLDocumentUpdateStatus> docUpdateStatusList,
            final BancsUpdateAttemptDTO updateAck) throws Exception {

        docUpdateStatusList.forEach(updt -> {
            UpdateAttemptsMapper.mapUpdatedDocUpdtStatusAndAttempts(updt, updateAck);
            docUpdateStatusRepo.saveAndFlush(updt);
        });
    }

}
