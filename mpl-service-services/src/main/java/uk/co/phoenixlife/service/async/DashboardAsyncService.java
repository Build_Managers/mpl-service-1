/*
 * DashboardAsyncService.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.async;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import uk.co.phoenixlife.service.client.bancs.BancsServiceClient;

/**
 * DashboardAsyncService.java
 */
@Component
public class DashboardAsyncService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DashboardAsyncService.class);

    @Autowired
    private BancsServiceClient bancsServiceClient;

    /**
     * Async execution of bancsPartyCommDetails
     *
     * @param bancsUniqueCustNo
     *            bancsUniqueCustNo of the customer
     * @param xGUID
     *            xGUID
     * @throws Exception
     *             Exception
     */
    @Async
    public void getPartyCommDetails(final String bancsUniqueCustNo, final String xGUID)
            throws Exception {
        bancsServiceClient.getPartyCommDetailsResponse(bancsUniqueCustNo, xGUID);
    }

    /**
     * Async execution of bancsIDAndVData
     *
     * @param policyNumber
     *            policyNumber of user
     * @param xGUID
     *            xGUID of user
     * @throws Exception
     *             exception
     */
    @Async
    public void getIDAndVData(final String policyNumber, final String xGUID)
            throws Exception {
        bancsServiceClient.getIDAndVDataResponse(policyNumber, xGUID);
    }

    /**
     * Async execution of bancsPolDetails
     *
     * @param bancsPolNum
     *            bancsPolNum of user
     * @param xGUID
     *            xGUID of user
     * @throws Exception
     *             exception
     */
    @Async
    public void getPolDetails(final String bancsPolNum, final String xGUID, final String xGUIDBancsPolNumCacheKey)
            throws Exception {
        bancsServiceClient.getPolDetailsResponse(bancsPolNum, xGUID, xGUIDBancsPolNumCacheKey);
    }

    /**
     * Async execution of bancsClaimList
     *
     * @param bancsPolNum
     *            bancsPolNum of user
     * @param xGUID
     *            xGUID of user
     * @throws Exception
     *             exception
     */
    @Async
    public void getClaimList(final String bancsPolNum, final String xGUID, final String xGUIDBancsPolNumCacheKey)
            throws Exception {
        bancsServiceClient.getClaimListResponse(bancsPolNum, xGUID, xGUIDBancsPolNumCacheKey);
    }

    /**
     * Async execution of bancsClaimList
     *
     * @param bancsPolNum
     *            bancsPolNum of user
     * @param bancsUniqueCustNo
     *            bancsUniqueCustNo of user
     * @param xGUID
     *            xGUID of user
     * @throws Exception
     *             exception
     */
    @Async
    public void getRetQuote(final String bancsPolNum, final String bancsUniqueCustNo, final String xGUID,
            final String xGUIDBancsPolNumCacheKey)
            throws Exception {
        bancsServiceClient.getRetQuoteResponse(bancsPolNum, bancsUniqueCustNo, xGUID, xGUIDBancsPolNumCacheKey);
    }

}
