/*
 * EmailService.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.async;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import freemarker.template.Configuration;
import uk.co.phoenixlife.service.client.email.EmailServiceClient;
import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dao.entity.MPLEmailStatus;
import uk.co.phoenixlife.service.dao.repository.CustomerRepositoryImpl;
import uk.co.phoenixlife.service.dao.repository.EmailStatusRepository;
import uk.co.phoenixlife.service.dao.repository.PolicyEncashmentRepository;
import uk.co.phoenixlife.service.dto.email.EmailResponseDTO;
import uk.co.phoenixlife.service.mapper.EmailMapper;
import uk.co.phoenixlife.service.utils.CommonUtils;
import uk.co.phoenixlife.service.utils.DateUtils;

/**
 * EmailService.java
 */
@Component
public class EmailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailService.class);

    @Autowired
    private Configuration freeMarkerConfiguration;

    @Autowired
    private EmailStatusRepository emailStatusRepository;
    @Autowired
    private CustomerRepositoryImpl customerRepository;
    @Autowired
    private PolicyEncashmentRepository policyEncashmentRepo;

    @Autowired
    private EmailServiceClient emailClient;

    @Value("${batch.attempts}")
    private String attempts;

    @Value("${batch.delayMinutes}")
    private String delayMinutes;

    @Value("${email.from}")
    private String from;

    @Value("${email.subject}")
    private String subject;

    @Value("${email.suppress}")
    private String emailSuppress;

    /**
     * Batch for emails
     *
     * @throws Exception
     *             - {@link Exception}
     */
    @Transactional
    public void emailBatch() throws Exception {

        int maxAttempts;
        Timestamp maxTmst;

        List<MPLEmailStatus> emailStatusList;

        maxAttempts = Integer.valueOf(attempts);
        maxTmst = DateUtils.formatDateTime(DateUtils.getCurrentLocalDateTime().minusMinutes(Long.valueOf(delayMinutes)));

        emailStatusList = customerRepository.getEmailsForBatch(maxAttempts, maxTmst);

        if (CommonUtils.stringNotBlankCheck(emailSuppress) && "Y".equalsIgnoreCase(emailSuppress)) {
            LOGGER.info("Suppressing EmailAPI ---->>>>");
        } else {
            processEmails(emailStatusList);
        }

    }

    private void processEmails(final List<MPLEmailStatus> emailStatusList) throws Exception {

        emailStatusList.forEach(emailStatus -> {
            try {
                sendMail(emailStatus, true);
            } catch (Exception ex) {
                LOGGER.info("[{}]:|{}|- emailAPI ({}) exception for mpl_customer -> {}",
                        CommonUtils.batchOrRealTime(true), getTransactionId(emailStatus.getEncashmentId()),
                        emailStatus.getEmailType(), emailStatus.getEmailCustomer().getCustomerId(), ex);
            }
        });
    }

    private void sendMail(final MPLEmailStatus mplEmailStatus, final boolean batchFlag) throws Exception {

        LocalDateTime sentTmst;
        LocalDateTime ackTmst;
        String transactionId;
        EmailResponseDTO emailResponse;

        sentTmst = DateUtils.getCurrentLocalDateTime();
        ackTmst = (LocalDateTime) CommonUtils.getNullObject();
        transactionId = getTransactionId(mplEmailStatus.getEncashmentId());

        LOGGER.info("[{}]:|{}|- emailAPI ({}) initiated for mpl_customer -> {}",
                CommonUtils.batchOrRealTime(batchFlag), transactionId, mplEmailStatus.getEmailType(),
                mplEmailStatus.getEmailCustomer().getCustomerId());

        emailResponse = emailClient
                .sendMail(EmailMapper.mapEmailRequest(freeMarkerConfiguration, from, subject,
                        new String(mplEmailStatus.getEmailCustomer().getEmailAddr())));

        ackTmst = DateUtils.getCurrentLocalDateTime();
        updateEmailStatusAndAttempt(emailResponse, mplEmailStatus, sentTmst, ackTmst, batchFlag, transactionId);

    }

    private String getTransactionId(final Long encashmentId) {

        String transactionId;

        if (CommonUtils.isObjectNotNull(encashmentId)) {
            transactionId = policyEncashmentRepo.findOne(encashmentId).getTransactionId();
        } else {
            transactionId = CommonUtils.generateUniqueID();
        }

        return transactionId;
    }

    private void updateEmailStatusAndAttempt(final EmailResponseDTO emailResponse, final MPLEmailStatus emailStatus,
            final LocalDateTime sentTmst, final LocalDateTime failedTmst, final boolean batchFlag,
            final String transactionId) throws Exception {

        if (emailResponse.getStatusCode() == HttpStatus.INTERNAL_SERVER_ERROR.value()) {

            LOGGER.error("[{}]:|{}|- emailAPI ({}) failed for mpl_customer -> {}",
                    CommonUtils.batchOrRealTime(batchFlag), transactionId, emailStatus.getEmailType(),
                    emailStatus.getEmailCustomer().getCustomerId());
            EmailMapper.mapUpdatedEmailStatusAndAttempts(emailStatus, MPLServiceConstants.FAILURE, MPLServiceConstants.NO,
                    sentTmst, failedTmst, MPLServiceConstants.EMAIL_ERROR);

        } else {

            LOGGER.error("[{}]:|{}|- emailAPI ({}) success for mpl_customer -> {}",
                    CommonUtils.batchOrRealTime(batchFlag), transactionId, emailStatus.getEmailType(),
                    emailStatus.getEmailCustomer().getCustomerId());
            EmailMapper.mapUpdatedEmailStatusAndAttempts(emailStatus, MPLServiceConstants.SUCCESS, MPLServiceConstants.YES,
                    sentTmst, null, null);
        }

        emailStatusRepository.saveAndFlush(emailStatus);

    }

}
