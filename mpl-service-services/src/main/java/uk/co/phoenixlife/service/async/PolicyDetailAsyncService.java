/*
 * PolicyDetailAsyncService.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.async;

import org.springframework.stereotype.Component;

/**
 * PolicyDetailAsyncService.java
 */

@Component
public class PolicyDetailAsyncService {

}
