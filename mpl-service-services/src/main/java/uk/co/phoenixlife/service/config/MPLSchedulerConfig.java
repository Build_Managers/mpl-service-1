/*
 * MPLSchedulerConfig.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.config;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import net.javacrumbs.shedlock.core.LockProvider;
import net.javacrumbs.shedlock.provider.jdbctemplate.JdbcTemplateLockProvider;
import net.javacrumbs.shedlock.spring.SpringLockableTaskSchedulerFactory;

/**
 * MPLSchedulerConfig.java
 */
@Configuration
@EnableScheduling
@ImportResource({ "classpath:META-INF/spring/applicationContext-dao.xml" })
public class MPLSchedulerConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(MPLSchedulerConfig.class);

    private static final int POOL_SIZE = 10;

    @Autowired
    private DataSource dataSource;

    /**
     * Bean configuration for ThreadPoolTaskScheduler
     *
     * @param lockProvider
     *            - {@link LockProvider}
     * @return - {@link ThreadPoolTaskScheduler}
     */
    @Bean
    public TaskScheduler poolScheduler(final LockProvider lockProvider) {

        TaskScheduler taskScheduler;
        final int poolSize = POOL_SIZE;

        taskScheduler = SpringLockableTaskSchedulerFactory.newLockableTaskScheduler(poolSize,
                lockProvider);
        LOGGER.debug("TaskScheduler bean configured with lock provider for clustered env...");
        return taskScheduler;
    }

    /**
     * Bean configuration for LockProvider
     *
     * @return - {@link LockProvider}
     */
    @Bean
    public LockProvider lockProvider() {
        LOGGER.debug("LockProvider bean configured...");
        return new JdbcTemplateLockProvider(dataSource);
    }

}
