/*
 * MPLServiceConfig.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Controller;

import uk.co.phoenixlife.dao.config.CacheConfig;
import uk.co.phoenixlife.service.client.config.MPLServiceClientConfig;
import uk.co.phoenixlife.service.client.config.SoapClientConfiguration;

/**
 * MPLServiceConfig.java
 */
@Configuration
@EnableAsync
@PropertySource(value = { "file:/app-wildfly/mpl_config/mpl-service.properties",
        "file:/app-wildfly/mpl_config/bancs-service.properties", "file:/app-wildfly/mpl_config/mpl-batch.properties" })
@ImportResource({ "classpath:META-INF/spring/applicationContext-pdf.xml" })
@Import({ MPLServiceClientConfig.class, MPLSchedulerConfig.class, SoapClientConfiguration.class, CacheConfig.class })
@ComponentScan(basePackages = { "uk.co.phoenixlife.service" }, excludeFilters = {
        @ComponentScan.Filter(value = Configuration.class, type = FilterType.ANNOTATION),
        @ComponentScan.Filter(value = Controller.class, type = FilterType.ANNOTATION) })
public class MPLServiceConfig {

    /**
     * Bean configuration for PropertySourcesPlaceholderConfigurer
     *
     * @return - {@link PropertySourcesPlaceholderConfigurer}
     */
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfig() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
