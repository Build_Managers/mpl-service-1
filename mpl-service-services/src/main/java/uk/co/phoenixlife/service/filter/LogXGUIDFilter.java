/*
 * LogXGUIDFilter.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.MDC;
import org.springframework.web.filter.OncePerRequestFilter;

import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.utils.CommonUtils;

/**
 * LogXGUIDFilter.java
 */
public class LogXGUIDFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
            final FilterChain chain) throws ServletException, IOException {

        String xGUID;
        xGUID = request.getHeader(MPLServiceConstants.XGUID_HEADER);

        if (CommonUtils.stringNotBlank(xGUID)) {
            MDC.put(MPLServiceConstants.XGUID_LOWERCASE, xGUID);
        }

        try {
            chain.doFilter(request, response);
        } finally {
            MDC.remove(MPLServiceConstants.XGUID_LOWERCASE);
        }
    }
}
