/*
 * PolicyActivationHelper.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.phoenixlife.service.dto.MPLException;
import uk.co.phoenixlife.service.dto.activation.CustomerActivationDTO;
import uk.co.phoenixlife.service.dto.activation.CustomerPersonalDetailStatus;
import uk.co.phoenixlife.service.dto.policy.BancsPolicyExists;
import uk.co.phoenixlife.service.dto.policy.BancsPolicyExistsCheck;
import uk.co.phoenixlife.service.dto.policy.PartyBasicDTO;
import uk.co.phoenixlife.service.dto.policy.PartyBasicOwnerDTO;
import uk.co.phoenixlife.service.dto.policy.PartyBasicOwnerListDTO;
import uk.co.phoenixlife.service.utils.CommonUtils;
import uk.co.phoenixlife.service.utils.DateUtils;

/**
 * This is an helper class for PolicyActivation related context
 * PolicyActivationHelper.java
 */
public final class AccountActivationHelper {
    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountActivationHelper.class);

    /**
     * method to checkPolicy status as per Business Requirements
     *
     * @param policyExists
     *            Response from BaNCS
     * @param xGUID
     *            xGUID
     * @param policyNumber
     *            current Policy Number
     * @return Result of the check
     */
    public static Integer checkPolicy(final BancsPolicyExists policyExists, final String xGUID, final String policyNumber) {
        Integer result = 0;
        final List<String> validPolStatusList = getValidList();
        if (CommonUtils.isObjectNotNull(policyExists)) {
            for (final BancsPolicyExistsCheck bancsPolicyExistsCheck : policyExists.getListOfPolicies()) {
                if (validPolStatusList.contains(bancsPolicyExistsCheck.getPolStatusLit())
                        && bancsPolicyExistsCheck.getPolNum().equalsIgnoreCase(policyNumber)) {
                    LOGGER.info("Inside checkPolicy, Policy is in Valid Policy List result is 1 :: {}", policyNumber);
                    result = 1;
                    break;
                } else {
                    result = 2;
                    LOGGER.debug("Policy is not in Valid Policy List result is:  {}", result);
                }
            }
        }
        return result;
    }

    private static List<String> getValidList() {
        final List<String> validList = new ArrayList<String>();
        validList.add("In Force Non Premium Paying");
        validList.add("Waiver admitted");
        validList.add("Revivable Paid Up");
        validList.add("Inforce");
        validList.add("Reissued");
        validList.add("Paid up");
        validList.add("Contribution Holiday");
        validList.add("Waiver inforce");
        validList.add("Regular Repayment Protection Inforce");
        validList.add("Regular Repayment Protection Admitted");
        return validList;
    }

    /**
     * Method to checkPersonalDetails
     *
     * @param partyBasic
     *            PartyBasicDetails from BaNCS
     * @param customerPersonalDetail
     *            Details entered by user
     * @return Object array having result set of the checks
     * @throws Exception
     *             Mplexception
     */
    public static List<CustomerPersonalDetailStatus> checkPersonalDetails(final PartyBasicDTO partyBasic,
            final CustomerActivationDTO customerActivationDTO) throws Exception {
        LOGGER.info("Inside AccountActiovationHelper checkPersonalDetails");
        Map<String, String> bancsCustWithMultipleOwner;
        Map<String, String> bancsCustWithSingleOwner;
        Map<String, String> bancsCustMap;
        List<CustomerPersonalDetailStatus> customerPersonalDetailStatusList;

        customerPersonalDetailStatusList = new ArrayList<CustomerPersonalDetailStatus>();
        bancsCustWithMultipleOwner = new HashMap<String, String>();
        bancsCustWithSingleOwner = new HashMap<String, String>();
        bancsCustMap = new HashMap<String, String>();

        boolean multipleOwnerFlag;

        final List<PartyBasicOwnerListDTO> listOfPolicies = partyBasic.getListOfPolicies();

        for (final PartyBasicOwnerListDTO partyBasicOwnerList : listOfPolicies) {

            multipleOwnerFlag = partyBasicOwnerList.getListOfOwners().size() > 1;
            for (final PartyBasicOwnerDTO owner : partyBasicOwnerList.getListOfOwners()) {
                if (multipleOwnerFlag) {
                    if (validatePersonalDetails(owner, customerActivationDTO)) {
                        bancsCustWithMultipleOwner.put(owner.getBancsUniqueCustNo(), owner.getMyPhoenixUserName());

                        bancsCustMap.put(owner.getBancsUniqueCustNo(), owner.getBancsLegacyCustNo());
                        LOGGER.debug(
                                "----------------------------" + owner.getMyPhoenixUserName() + "------------------------");
                        LOGGER.debug("Added uniqueCustomerNumber {} in bancsCustWithMultipleOwner ",
                                owner.getBancsUniqueCustNo());
                    }
                } else {
                    if (validatePersonalDetails(owner, customerActivationDTO)) {
                        bancsCustWithSingleOwner.put(owner.getBancsUniqueCustNo(), owner.getMyPhoenixUserName());
                        bancsCustMap.put(owner.getBancsUniqueCustNo(), owner.getBancsLegacyCustNo());
                        LOGGER.debug(
                                "----------------------------" + owner.getMyPhoenixUserName() + "------------------------");
                        LOGGER.debug("Added uniqueCustomerNumber {} in bancsCustWithSingleOwner ",
                                owner.getBancsUniqueCustNo());
                    }
                }

            }
        }
        if (!bancsCustWithMultipleOwner.isEmpty()
                && bancsCustWithSingleOwner.isEmpty()) {
            LOGGER.error("ERROR in checkPersonalDetails method");
            throw new MPLException();
        } else {
            bancsCustWithSingleOwner.entrySet().stream().forEach(cust -> customerPersonalDetailStatusList
                    .add(new CustomerPersonalDetailStatus(cust.getKey(), false, cust.getValue(),
                            bancsCustMap.get(cust.getKey()))));
            bancsCustWithMultipleOwner.entrySet().stream().forEach(cust -> customerPersonalDetailStatusList
                    .add(new CustomerPersonalDetailStatus(cust.getKey(), true, cust.getValue(),
                            bancsCustMap.get(cust.getKey()))));
        }
        LOGGER.debug("checkPersonalDetails method successful");
        return customerPersonalDetailStatusList;
    }

    private static boolean validatePersonalDetails(final PartyBasicOwnerDTO owner,
            final CustomerActivationDTO customerActivationDTO) {

        boolean matchFlag = false;
        if (owner.getForename().equalsIgnoreCase(customerActivationDTO.getFirstName())
                && owner.getSurname().equalsIgnoreCase(customerActivationDTO.getSurName())
                && DateUtils.compareLocalDate(DateUtils.convertDate(owner.getDateOfBirth()),
                        customerActivationDTO.getDateOfBirth())) {
            matchFlag = true;
        }

        return matchFlag;
    }

}
