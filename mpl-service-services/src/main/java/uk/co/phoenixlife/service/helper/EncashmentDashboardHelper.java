/*
 * EncashmentDashboardHelper.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.helper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.dao.repository.CustomerPolicyRepository;
import uk.co.phoenixlife.service.dto.MPLException;
import uk.co.phoenixlife.service.dto.bancserror.ErrorResponseDTO;
import uk.co.phoenixlife.service.dto.dashboard.PartyBasicDetailByPartyNoDTO;
import uk.co.phoenixlife.service.dto.dashboard.PoliciesOwnedByAPartyDTO;
import uk.co.phoenixlife.service.dto.dashboard.PolicyDTO;
import uk.co.phoenixlife.service.dto.policy.BancsRetQuoteValDTO;
import uk.co.phoenixlife.service.dto.policy.ClaimDTO;
import uk.co.phoenixlife.service.dto.policy.ClaimListDTO;
import uk.co.phoenixlife.service.dto.policy.DashboardDTO;
import uk.co.phoenixlife.service.dto.policy.DataCorrectionFlagDTO;
import uk.co.phoenixlife.service.dto.policy.FinalDashboardDTO;
import uk.co.phoenixlife.service.dto.policy.FundDTO;
import uk.co.phoenixlife.service.dto.policy.IdAndVDataDTO;
import uk.co.phoenixlife.service.dto.policy.PartyBasicDTO;
import uk.co.phoenixlife.service.dto.policy.PartyBasicOwnerDTO;
import uk.co.phoenixlife.service.dto.policy.PartyBasicOwnerListDTO;
import uk.co.phoenixlife.service.dto.policy.PartyCommDetailsDTO;
import uk.co.phoenixlife.service.dto.policy.PartyPolicyDTO;
import uk.co.phoenixlife.service.dto.policy.PolDetailsDTO;
import uk.co.phoenixlife.service.dto.policy.PolicyRoleDTO;
import uk.co.phoenixlife.service.dto.response.BancsClaimListResponse;
import uk.co.phoenixlife.service.dto.response.BancsIDAndVDataResponse;
import uk.co.phoenixlife.service.dto.response.BancsPartyBasicDetailResponse;
import uk.co.phoenixlife.service.dto.response.BancsPartyCommDetailsResponse;
import uk.co.phoenixlife.service.dto.response.BancsPolDetailsResponse;
import uk.co.phoenixlife.service.dto.response.BancsPoliciesOwnedByAPartyResponse;
import uk.co.phoenixlife.service.dto.response.BancsRetQuoteValResponse;
import uk.co.phoenixlife.service.dto.response.PartyBasicDetailByPartyNoResponse;
import uk.co.phoenixlife.service.utils.CommonUtils;

/**
 * EncashmentDashboardHelper.java
 */
@Component
public class EncashmentDashboardHelper {
    private static ObjectMapper mapper = new ObjectMapper();

    private static final Logger LOGGER = LoggerFactory.getLogger(EncashmentDashboardHelper.class);

    @Autowired
    private CustomerPolicyRepository customerPolicyRepository;

    /**
     * Method to check IdAndVData API Response
     *
     * @param idAndVDataResponse
     *            idAndVData
     * @param xGUID
     *            xGUID
     * @return IdAndVDataDTO
     * @throws JsonParseException
     *             exception
     * @throws JsonMappingException
     *             exception
     * @throws IOException
     *             exception
     * @throws MPLException
     */
    public static IdAndVDataDTO getIdAndVDataCheck(final String idAndVDataResponse, final String xGUID)
            throws JsonParseException, JsonMappingException, IOException, MPLException {
        IdAndVDataDTO idAndVData = null;
        if (CommonUtils.stringNotBlank(idAndVDataResponse)) {
            if (idAndVDataResponse.contains(MPLConstants.ERROR_DETAIL)) {
                final ErrorResponseDTO error = mapper.readValue(idAndVDataResponse, ErrorResponseDTO.class);

                LOGGER.error("IDAndVDataAPI received ERROR Response {} from BaNCS for xGUID :: {}", error);
                throw new MPLException();
            } else {
                final BancsIDAndVDataResponse bancsIDAndVDataResponse = mapper.readValue(idAndVDataResponse,
                        BancsIDAndVDataResponse.class);
                if (CommonUtils.isobjectNotNull(bancsIDAndVDataResponse)
                        && CommonUtils.isobjectNotNull(bancsIDAndVDataResponse.getBancsIDAndVData())) {
                    idAndVData = bancsIDAndVDataResponse.getBancsIDAndVData();
                } else {
                    LOGGER.error("IDAndVDataResponse is null for xGUID :: {}");
                    throw new MPLException();

                }
            }
        } else {
            LOGGER.error("IDAndVData is blank for xGUID :: {}");
            throw new MPLException();

        }
        return idAndVData;
    }

    /**
     * Method to check partycommdetails check
     *
     * @param partyCommDetailResponse
     *            response
     * @param xGUID
     *            xguid
     * @return PartyCommDetail
     * @throws Exception
     *             exception
     * @throws JsonMappingException
     *             exception
     * @throws IOException
     *             exception
     */
    public static PartyCommDetailsDTO partyCommFlagLifeStatusCheck(final String partyCommDetailResponse,
            final String partyBasicPartyNoResponse,
            final String xGUID)
            throws Exception, JsonMappingException, IOException {
        PartyCommDetailsDTO partyCommDetail = null;

        if (CommonUtils.stringNotBlank(partyCommDetailResponse)) {
            if (partyCommDetailResponse.contains(MPLConstants.ERROR_DETAIL)
                    || partyBasicPartyNoResponse.contains(MPLConstants.ERROR_DETAIL)) {
                final ErrorResponseDTO error = mapper.readValue(partyCommDetailResponse, ErrorResponseDTO.class);
                LOGGER.error(
                        "PartyCommDetails or PartyBasicPartyNoReposne received ERROR Response {} from BaNCS for xGUID :: {}",
                        error);
                throw new MPLException();

            } else {
                final BancsPartyCommDetailsResponse bancsPartyCommDetailsResponse = mapper.readValue(partyCommDetailResponse,
                        BancsPartyCommDetailsResponse.class);
                final PartyBasicDetailByPartyNoResponse partyBasicByPartyNoResponse = mapper.readValue(
                        partyBasicPartyNoResponse,
                        PartyBasicDetailByPartyNoResponse.class);
                final PartyBasicDetailByPartyNoDTO partyBasicDetailByPartyNoDTO = partyBasicByPartyNoResponse
                        .getPartyBasicDetailByPartyNo();
                if (CommonUtils.isobjectNotNull(bancsPartyCommDetailsResponse)
                        && CommonUtils.isobjectNotNull(bancsPartyCommDetailsResponse.getBancsPartyCommDetails())
                        && partyCommFlagLifeStatusCheck(bancsPartyCommDetailsResponse.getBancsPartyCommDetails(),
                                partyBasicDetailByPartyNoDTO.getLifeStatusLit(), xGUID)) {
                    partyCommDetail = bancsPartyCommDetailsResponse.getBancsPartyCommDetails();
                } else {
                    LOGGER.error("PartyCommDetails failed with Business Validation or Null Values for xGUID :: {}");
                    throw new MPLException();

                }
            }
        } else {
            LOGGER.error("PartyCommDetails is blank for xGUID :: {}");
            throw new MPLException();

        }
        return partyCommDetail;
    }

    private static boolean partyCommFlagLifeStatusCheck(final PartyCommDetailsDTO partyCommDetailsDTO,
            final String lifeStatusLit,
            final String xGUID) throws Exception {
        boolean result = true;
        if (MPLConstants.YLITERAL.equalsIgnoreCase(partyCommDetailsDTO.getFcuFlag())
                || MPLConstants.YLITERAL.equalsIgnoreCase(partyCommDetailsDTO.getPoaFlag())
                || partyCommBankCheck(partyCommDetailsDTO, xGUID)
                || partyCommAddressStatusCheck(partyCommDetailsDTO, xGUID)
                || partyCommLifeStatusLitIsValid(lifeStatusLit, xGUID)) {
            result = false;
            LOGGER.error("{} BancsPartyComm FAILED while doing flag check");
            throw new MPLException();
        }
        return result;
    }

    private static Boolean partyCommLifeStatusLitIsValid(final String lifeStatusLit, final String xGUID) {

        boolean result = false;

        if (lifeStatusLit.equalsIgnoreCase(MPLConstants.NOTIFIED_DEATH)
                || lifeStatusLit.equalsIgnoreCase(MPLConstants.CERTIFIED_DEATH)) {
            result = true;
            LOGGER.error("{} LifeStatus is either Notified death or Certified  death, throwing SYSTEM ERROR ", xGUID);
        }
        return result;
    }

    private static boolean partyCommBankCheck(final PartyCommDetailsDTO partyCommDetailsDTO, final String xGUID) {
        boolean result = true;
        if (bankruptcyCheck(partyCommDetailsDTO, xGUID) && partyCommDetailsDTO.getCourtName() == null
                && partyCommDetailsDTO.getCourtOrderDate() == null) {
            result = false;
            LOGGER.debug("{} PartyCommBankCheck passed as per business validation ");
        } else {
            LOGGER.error("{} partyCommBankCheck FAILED while doing BankCheck");
        }
        return result;
    }

    private static boolean bankruptcyCheck(final PartyCommDetailsDTO partyCommDetailsDTO, final String xGUID) {
        boolean result = false;
        if (partyCommDetailsDTO.getBankruptcyRefNo() == null && partyCommDetailsDTO.getBankruptcyDischargeDate() == null
                && partyCommDetailsDTO.getBankruptcyDate() == null) {
            result = true;
        } else {
            LOGGER.error("{} bankruptcyCheck failed");
        }

        return result;
    }

    private static boolean partyCommAddressStatusCheck(final PartyCommDetailsDTO partyCommDetailsDTO, final String xGUID) {
        Boolean isGoneAway = false;
        // for(AddressDTO address : partyCommDetailsDTO.getAddressList()){
        if (partyCommDetailsDTO.getAddressStatus() != null
                && (partyCommDetailsDTO.getAddressStatus().equals(MPLConstants.GAVLITERAL)
                        || partyCommDetailsDTO.getAddressStatus().equals(MPLConstants.GAULITERAL))) {
            isGoneAway = true;
        }
        // }
        return isGoneAway;
    }

    /**
     * Method to check IdAndVData API Response
     *
     * @param idAndVDataResponse
     *            idAndVData
     * @param xGUID
     *            xGUID
     * @return IdAndVDataDTO
     * @throws JsonParseException
     *             exception
     * @throws JsonMappingException
     *             exception
     * @throws IOException
     *             exception
     * @throws MPLException
     */
    public static PoliciesOwnedByAPartyDTO getPoliciesOwnedByAPartyCheck(final String policiesOwnedByAPartyResponse,
            final String xGUID)
            throws JsonParseException, JsonMappingException, IOException, MPLException {
        PoliciesOwnedByAPartyDTO policiesOwnedByAPartyDTO = null;
        if (CommonUtils.stringNotBlank(policiesOwnedByAPartyResponse)) {
            if (policiesOwnedByAPartyResponse.contains(MPLConstants.ERROR_DETAIL)) {
                final ErrorResponseDTO error = mapper.readValue(policiesOwnedByAPartyResponse, ErrorResponseDTO.class);

                LOGGER.error("PoliciesOwnedByAParty received ERROR Response {} from BaNCS for xGUID :: {}", error);
                throw new MPLException();
            } else {
                final BancsPoliciesOwnedByAPartyResponse bancsPoliciesOwnedByAPartyResponse = mapper.readValue(
                        policiesOwnedByAPartyResponse,
                        BancsPoliciesOwnedByAPartyResponse.class);
                if (CommonUtils.isobjectNotNull(bancsPoliciesOwnedByAPartyResponse)
                        && CommonUtils.isobjectNotNull(bancsPoliciesOwnedByAPartyResponse.getBancsPoliciesOwnedByAParty())) {
                    policiesOwnedByAPartyDTO = bancsPoliciesOwnedByAPartyResponse.getBancsPoliciesOwnedByAParty();
                } else {
                    LOGGER.error("PoliciesOwnedByAParty is null for xGUID :: {}");
                    throw new MPLException();

                }
            }
        } else {
            LOGGER.error("PoliciesOwnedByAParty is blank for xGUID :: {}");
            throw new MPLException();

        }
        return policiesOwnedByAPartyDTO;
    }

    /**
     * Method to check PolDetails API Response
     *
     * @param polDetailsResponse
     *            polDetails
     * @param xGUID
     *            xGUID
     * @return poldetailsdto
     * @throws JsonParseException
     *             exception
     * @throws JsonMappingException
     *             exception
     * @throws IOException
     *             exception
     * @throws MPLException
     */
    public static PolDetailsDTO getPolDetailsCheck(final String polDetailsResponse, final String xGUID)
            throws JsonParseException, JsonMappingException, IOException, MPLException {
        PolDetailsDTO polDetails = null;
        if (CommonUtils.stringNotBlank(polDetailsResponse)) {
            if (polDetailsResponse.contains(MPLConstants.ERROR_DETAIL)) {
                final ErrorResponseDTO error = mapper.readValue(polDetailsResponse, ErrorResponseDTO.class);
                LOGGER.error("PolDetailsAPI received ERROR Response {} from BaNCS for xGUID :: {}", error);
                throw new MPLException();

            } else {
                final BancsPolDetailsResponse bancsPolDetailsResponse = mapper.readValue(polDetailsResponse,
                        BancsPolDetailsResponse.class);
                if (CommonUtils.isobjectNotNull(bancsPolDetailsResponse)
                        && CommonUtils.isobjectNotNull(bancsPolDetailsResponse.getBancsPolDetails())) {
                    if (polDetailsCheck(bancsPolDetailsResponse.getBancsPolDetails(), xGUID)) {
                        polDetails = bancsPolDetailsResponse.getBancsPolDetails();
                    }
                } else {
                    LOGGER.error("PolDetails failed with Business Validation or Null Values for xGUID :: {}");
                    throw new MPLException();
                }
            }
        } else {
            LOGGER.error("PolDetailsResponse is blank for xGUID :: {}");
            throw new MPLException();
        }
        return polDetails;
    }

    private static Boolean polDetailsCheck(final PolDetailsDTO polDetails, final String xGUID) {
        Boolean result = false;
        if (CommonUtils.isobjectNotNull(polDetails.getAccrualBasisCd())
                && CommonUtils.isobjectNotNull(polDetails.getUnitisedPol())) {
            if (MPLConstants.DCLITERAL.equalsIgnoreCase(polDetails.getAccrualBasisCd())
                    && !MPLConstants.CAPITALYLITERAL.equalsIgnoreCase(polDetails.getTrustBasedApplic())
                    && checkRoleCount(polDetails) <= 1
                    && !MPLConstants.UNITISEDLITERAL.equalsIgnoreCase(polDetails.getUnitisedPol())) {
                result = true;
            } else {
                LOGGER.info("{} Accural Basis / InvType / TrustBasedApplic / RoleCount is invalid for bancsPolNum {}",
                        polDetails.getBancsPolNum());
            }
        } else {
            LOGGER.info("{} Accural Basis or InvType is NULL for bancsPolNum {}", polDetails.getBancsPolNum());
        }
        return result;
    }

    private static int checkRoleCount(final PolDetailsDTO polDetails) {
        List<PolicyRoleDTO> roleList;
        int countOfRole = 0;
        if (CommonUtils.isValidList(polDetails.getListOfRoles())) {
            roleList = polDetails.getListOfRoles();
            for (final PolicyRoleDTO roleObj : roleList) {
                if (roleObj.getPolRoleCd() != null && MPLConstants.OWRLITERAL.equalsIgnoreCase(roleObj.getPolRoleCd())) {
                    countOfRole++;
                }
            }
        }
        return countOfRole;
    }

    /**
     * Method to check ClaimList API Response
     *
     * @param claimListResponse
     *            claimList
     * @param xGUID
     *            xGUID
     * @return claimListResponse
     * @throws JsonParseException
     *             exception
     * @throws JsonMappingException
     *             exception
     * @throws IOException
     *             exception
     * @throws MPLException
     */
    public static ClaimListDTO getClaimListCheck(final String claimListResponse, final String xGUID)
            throws JsonParseException, JsonMappingException, IOException, MPLException {
        ClaimListDTO claimList = null;
        if (CommonUtils.stringNotBlank(claimListResponse)) {
            if (claimListResponse.contains(MPLConstants.ERROR_DETAIL)) {
                final ErrorResponseDTO error = mapper.readValue(claimListResponse, ErrorResponseDTO.class);
                LOGGER.error("ClaimListAPI received ERROR Response {} from BaNCS for xGUID :: {}", error);
                throw new MPLException();
            } else {
                final BancsClaimListResponse bancsClaimListResponse = mapper.readValue(claimListResponse,
                        BancsClaimListResponse.class);
                if (CommonUtils.isobjectNotNull(bancsClaimListResponse)
                        && CommonUtils.isobjectNotNull(bancsClaimListResponse.getBancsClaimList())) {
                    if (claimListCheck(bancsClaimListResponse.getBancsClaimList(), xGUID)) {
                        claimList = bancsClaimListResponse.getBancsClaimList();
                    }
                } else {
                    LOGGER.error("ClaimListAPI failed with Business Validation or Null Values for xGUID :: {}");
                    throw new MPLException();
                }
            }
        } else {
            LOGGER.error("ClaimListAPI is blank for xGUID :: {}");
            throw new MPLException();
        }
        return claimList;
    }

    private static boolean claimListCheck(final ClaimListDTO claimListDTO, final String xGUID) {
        boolean result = false;
        if (CommonUtils.isobjectNotNull(claimListDTO.getListOfClaims())) {
            final List<ClaimDTO> claims = claimListDTO.getListOfClaims();
            for (final ClaimDTO claim : claims) {
                if (CommonUtils.isobjectNotNull(claim.getClmTypeLit())
                        && !getInvalidClaimType().contains(claim.getClmTypeLit())) {
                    if (claimListCheck(claimListDTO, claim, xGUID)) {
                        result = true;
                    }
                } else {
                    LOGGER.debug("ClaimType is null or invalid for XGUID {}");
                }
            }

        } else {
            result = true;
            LOGGER.debug("ListOfClaims is NULL for XGUID {}");
        }

        return result;

    }

    private static List<String> getInvalidClaimType() {
        ArrayList<String> invalidClmType;
        invalidClmType = new ArrayList<String>();
        invalidClmType.add(MPLConstants.DEATHLITERAL);
        invalidClmType.add(MPLConstants.DEPENDANTDRAWDOWNLITERAL);
        return invalidClmType;
    }

    private static Boolean claimListCheck(final ClaimListDTO claimListObj, final ClaimDTO claim, final String xGUID) {
        Boolean result;
        if (MPLConstants.FULLENCASHMENTCD.equalsIgnoreCase(claim.getClmTypeCd())
                || MPLConstants.FULLTRANSFERCD.equalsIgnoreCase(claim.getClmTypeCd())
                || MPLConstants.RETIREMENTCD.equalsIgnoreCase(claim.getClmTypeCd())) {
            if (claimStatusCheck(claim)) {
                LOGGER.debug(" {} Inside claimListCheck validation failure for Policy {}",
                        claimListObj.getBancsPolNum());
                result = false;
            } else {
                LOGGER.debug(" {} ClaimTypeCD is successfull, but claimStatusCD is failed ");
                result = true;
            }
        } else {
            LOGGER.debug(" {} ClaimTypeCD is failed ");
            result = true;
        }
        return result;
    }

    private static Boolean claimStatusCheck(final ClaimDTO claim) {
        Boolean result = false;
        if (claim.getClmStatusCd() != null && (MPLConstants.ADMITTERCD.equalsIgnoreCase(claim.getClmStatusCd())
                || MPLConstants.SETTLEDCD.equalsIgnoreCase(claim.getClmStatusCd())
                || MPLConstants.DRAFTCD.equalsIgnoreCase(claim.getClmStatusCd()))) {
            result = true;
        }
        return result;
    }

    /**
     * Method to check RetQuoteVal API Response
     *
     * @param retQuoteResponse
     *            retQuoteResponse
     * @param xGUID
     *            xGUID
     * @return claimListResponse
     * @throws JsonParseException
     *             exception
     * @throws JsonMappingException
     *             exception
     * @throws IOException
     *             exception
     * @throws MPLException
     */
    public static BancsRetQuoteValDTO getRetQuoteValCheck(final String retQuoteResponse, final String xGUID)
            throws JsonParseException, JsonMappingException, IOException, MPLException {
        BancsRetQuoteValDTO bancsRetQuoteValDTO = null;
        if (CommonUtils.stringNotBlank(retQuoteResponse)) {
            if (retQuoteResponse.contains(MPLConstants.ERROR_DETAIL)) {
                final ErrorResponseDTO error = mapper.readValue(retQuoteResponse, ErrorResponseDTO.class);
                LOGGER.error("retQuoteValAPI received ERROR Response {} from BaNCS for xGUID :: {}", error);
                throw new MPLException();
            } else {
                final BancsRetQuoteValResponse bancsRetQuoteValResponse = mapper.readValue(retQuoteResponse,
                        BancsRetQuoteValResponse.class);
                if (CommonUtils.isobjectNotNull(bancsRetQuoteValResponse)
                        && CommonUtils.isobjectNotNull(bancsRetQuoteValResponse.getBancsRetQuoteVal())) {
                    bancsRetQuoteValDTO = bancsRetQuoteValResponse.getBancsRetQuoteVal();
                } else {
                    LOGGER.error("retQuoteValAPI failed with Business Validation or Null Values for xGUID :: {}");
                    throw new MPLException();
                }
            }
        } else {
            LOGGER.error("retQuoteValAPI is blank for xGUID :: {}", xGUID);
            throw new MPLException();
        }
        return bancsRetQuoteValDTO;
    }

    public FinalDashboardDTO finalDashboardSetter(final String email, final PartyBasicOwnerDTO partyBasicOwner,
            final IdAndVDataDTO iDAndVData,
            final PartyCommDetailsDTO partyCommDetails, final List<PolicyDTO> policies,
            final List<PolDetailsDTO> validPolicy, final List<BancsRetQuoteValDTO> validRetQuoteVal,
            final List<ClaimListDTO> validClaimList, final String xGUID) {
        boolean corrFlagInvalid = false;
        FinalDashboardDTO result = null;
        FinalDashboardDTO finalDashboard;
        finalDashboard = getFinalDashboardWithBasicAttr(email, partyBasicOwner, partyCommDetails, xGUID);
        List<DashboardDTO> dashboardPolicyList;
        dashboardPolicyList = new ArrayList<DashboardDTO>();
        final ArrayList<String> listOfFlag = (ArrayList<String>) CommonUtils.fetchInActiveFlagList();
        for (final PolicyDTO policy : policies) {
            if (listOfFlag.contains(policy.getPolStatusCd())) {
                final Boolean policyEncashedFlag = checkPolicyAlreadyEncashed(policy.getBancsPolNum(), policy.getPolNum());
                if (!policyEncashedFlag) {
                    LOGGER.info("{} Inside Policies validation set");
                    corrFlagInvalid = false;
                    DashboardDTO dashboard = getNewDashboard();
                    dashboard.setBancsPolNum(policy.getBancsPolNum());
                    dashboard.setPolNum(policy.getPolNum());
                    if (policy.getPolIRD() != null) {
                        dashboard.setPolIRD(policy.getPolIRD());
                    }
                    LOGGER.debug("{} Validating PolDetails, RetQuote and ClaimList data to set in dashboard");
                    dashboard.setPolSusp(policy.getPolSusp());
                    dashboard = getUpdatedDashboardWithPolDetailsCheck(dashboard, policy, validPolicy, xGUID);
                    dashboard = getUpdatedDashboardWithRetQuoteValCheck(dashboard, policy, validRetQuoteVal, xGUID);
                    dashboard = getUpdatedDashboardWithClaimListCheck(dashboard, policy, validClaimList, xGUID);
                    dashboard = getUpdatedDashboardWithIDAndVData(dashboard, policy, iDAndVData, xGUID);
                    dashboard.setPartyProt(partyCommDetails.getPartyProt());
                    dashboardPolicyList = getDashboardListWithCheck(dashboard, corrFlagInvalid, dashboardPolicyList, xGUID);
                    LOGGER.info("{} Validation of PolDetails, RetQuote and ClaimList is succesfull for dashboard creation");
                }
            }
        }
        finalDashboard.setDashboardPolicyList(dashboardPolicyList);
        LOGGER.debug("{} Final validation succesfull");
        if (finalDashboard.getDashboardPolicyList() != null
                && finalDashboard.getDashboardPolicyList().size() >= 1) {
            LOGGER.info("{} Final Validation Set Completed");
            result = finalDashboard;
        } else {
            LOGGER.error("{} FinalDashboard Set Not Completed because of size of list is 0");
        }
        return result;
    }

    private Boolean checkPolicyAlreadyEncashed(final String bancsPolNum, final String legacyPolNum) {
        Boolean policyEncashedFlag = false;
        List<Object[]> iDList;
        iDList = customerPolicyRepository.fetchPolicyAndEncashmentId(bancsPolNum, legacyPolNum);
        if (CommonUtils.isValidList(iDList)) {
            if (CommonUtils.isobjectNotNull(iDList.get(0)[0])) {
                LOGGER.debug("Policy entry is already present in DB policy number: {}, legacyPolNum : {}", legacyPolNum,
                        bancsPolNum);
                if (CommonUtils.isobjectNotNull(iDList.get(0)[1])) {
                    LOGGER.debug("Policy is already encashed policy number: {}, legacyPolNum : {}", legacyPolNum,
                            bancsPolNum);
                    policyEncashedFlag = true;
                } else {
                    LOGGER.debug(
                            "Policy entry is already present in DB but not encashed policy number: {}, legacyPolNum : {}",
                            legacyPolNum, bancsPolNum);
                }
            } else {
                LOGGER.debug("Policy entry is not present in DB policy number: {}, legacyPolNum : {}", legacyPolNum,
                        bancsPolNum);
            }
        }
        return policyEncashedFlag;
    }

    private static FinalDashboardDTO getFinalDashboardWithBasicAttr(final String email,
            final PartyBasicOwnerDTO partyBasicOwner,
            final PartyCommDetailsDTO partyCommDetails, final String xGUID) {
        LOGGER.info(" {} Inside getFinalDashboardWithBasicAttr Setting data in FinalDashboard");
        FinalDashboardDTO finalDashboard;
        finalDashboard = new FinalDashboardDTO();
        finalDashboard.setEmail(email);
        finalDashboard.setDateOfBirth(partyBasicOwner.getDateOfBirth());
        finalDashboard.setForename(partyBasicOwner.getForename());
        // finalDashboard.setCountry(CommonUtils.getPreferredAddress(partyCommDetails).getCountry());
        finalDashboard.setCountry(partyCommDetails.getCountry());
        if (CommonUtils.isobjectNotNull(partyBasicOwner.getNiNo())) {
            finalDashboard.setNiNo(partyBasicOwner.getNiNo());
        }
        finalDashboard.setSurname(partyBasicOwner.getSurname());
        finalDashboard.setTitle(partyBasicOwner.getTitle());
        finalDashboard.setBancslegacyCustNo(partyBasicOwner.getBancsLegacyCustNo());
        LOGGER.debug(" After PartyBasicOwner validation set");
        finalDashboard = setAddr1Addr2CityCounty(finalDashboard, partyCommDetails);
        finalDashboard = setHouseNameNumMobPost(finalDashboard, partyCommDetails);
        finalDashboard.setPartyProt(partyCommDetails.getPartyProt());

        finalDashboard.setBancsUniqueCustNo(partyCommDetails.getBancsUniqueCustNo());
        LOGGER.debug(" {} Details are set in FinalDashboard");
        return finalDashboard;
    }

    private static FinalDashboardDTO setAddr1Addr2CityCounty(final FinalDashboardDTO finalDashboard,
            final PartyCommDetailsDTO partyCommDetails) {

        if (CommonUtils.isobjectNotNull(partyCommDetails.getAddress1())) {
            finalDashboard.setAddress1(partyCommDetails.getAddress1());
        }
        if (CommonUtils.isobjectNotNull(partyCommDetails.getAddress2())) {
            finalDashboard.setAddress2(partyCommDetails.getAddress2());
        }
        if (CommonUtils.isobjectNotNull(partyCommDetails.getCityTown())) {
            finalDashboard.setCityTown(partyCommDetails.getCityTown());
        }
        if (CommonUtils.isobjectNotNull(partyCommDetails.getCounty())) {
            finalDashboard.setCounty(partyCommDetails.getCounty());
        }
        return finalDashboard;

    }

    private static FinalDashboardDTO setHouseNameNumMobPost(final FinalDashboardDTO finalDashboard,
            final PartyCommDetailsDTO partyCommDetails) {

        if (CommonUtils.isobjectNotNull(partyCommDetails.getHouseName())) {
            finalDashboard.setHouseName(partyCommDetails.getHouseName());
        }
        if (CommonUtils.isobjectNotNull(partyCommDetails.getHouseNum())) {
            finalDashboard.setHouseNum(partyCommDetails.getHouseNum());
        }
        if (CommonUtils.isobjectNotNull(partyCommDetails.getMobPhoneNum())) {
            finalDashboard.setMobPhoneNum(partyCommDetails.getMobPhoneNum());
        }
        if (CommonUtils.isobjectNotNull(partyCommDetails.getPostCode())) {
            finalDashboard.setPostCode(partyCommDetails.getPostCode());
        }
        return finalDashboard;

    }

    private static DashboardDTO getNewDashboard() {
        return new DashboardDTO();
    }

    private static DashboardDTO getUpdatedDashboardWithPolDetailsCheck(final DashboardDTO dashboard, final PolicyDTO policy,
            final List<PolDetailsDTO> validPolicy, final String xGUID) {
        for (final PolDetailsDTO polDetails : validPolicy) {
            if (polDetails.getBancsPolNum().equals(policy.getBancsPolNum())) {
                LOGGER.debug("{} Inside getUpdatedDashboardWithPolDetailsCheck method ");

                dashboard.setCashInInProgress(polDetails.getCashInInProgress());
                if (CommonUtils.isobjectNotNull(polDetails.getListOfFlags())) {
                    dashboard.setListOfFlags(
                            polDetails.getListOfFlags());
                } else {
                    LOGGER.debug("{} List of CorrFlag is NULL for bancsPolNum {}", polDetails.getBancsPolNum());
                }
                dashboard.setgARorGAO(polDetails.getgARorGAO());
                if (CommonUtils.isobjectNotNull(polDetails.getPenRevStatusLit())) {
                    dashboard.setPenRevStatus(polDetails.getPenRevStatusLit());
                } else {
                    LOGGER.debug("{} PenRevStatus is NULL for bancsPolNum {}", polDetails.getBancsPolNum());

                }
                dashboard.setPolProt(polDetails.getPolProt());
                if (CommonUtils.isobjectNotNull(polDetails.getTaxRegimeLit())) {
                    dashboard.setTaxRegime(polDetails.getTaxRegimeLit());
                } else {
                    LOGGER.debug("{} Tax Regime is NULL for bancsPolNum {}", polDetails.getBancsPolNum());
                }
                break;
            }
        }
        LOGGER.info(" {} getUpdatedDashboardWithPolDetailsCheck is successfull:: ");
        return dashboard;
    }

    private static DashboardDTO getUpdatedDashboardWithRetQuoteValCheck(final DashboardDTO dashboard, final PolicyDTO policy,
            final List<BancsRetQuoteValDTO> validRetQuoteVal, final String xGUID) {
        for (final BancsRetQuoteValDTO polVal : validRetQuoteVal) {
            if (polVal.getBancsPolNum().equals(policy.getBancsPolNum())) {
                LOGGER.debug(" {} Inside getUpdatedDashboardWithRetQuoteValCheck validation set");
                dashboard.setcUCharge(polVal.getcUCharge());
                dashboard.setMvaValue(polVal.getMvaValue());
                if (CommonUtils.isobjectNotNull(polVal.getMvaValue()) && polVal.getMvaValue() > 0) {
                    dashboard.setMvrValue(MPLConstants.CAPITALYLITERAL);
                } else {
                    dashboard.setMvrValue(MPLConstants.NLITERAL);
                }
                dashboard.setBusTimeStamp(polVal.getBusTimeStamp());
                dashboard.setPenValue(polVal.getPenValue());
                dashboard.setTotValue(polVal.getTotValue());
                if (getEarlyExitChargeCheck(polVal)) {
                    dashboard.setEarlyExitCharge(MPLConstants.CAPITALYLITERAL);
                } else {
                    dashboard.setEarlyExitCharge(MPLConstants.NLITERAL);
                }
                break;
            }
        }
        LOGGER.info(" {} getUpdatedDashboardWithRetQuoteValCheck is successfull:: ");
        return dashboard;
    }

    private static Boolean getEarlyExitChargeCheck(final BancsRetQuoteValDTO polVal) {
        Boolean result = false;
        if (CommonUtils.isobjectNotNull(polVal.getPenValue()) && polVal.getPenValue() > 0
                || CommonUtils.isobjectNotNull(polVal.getcUCharge()) && polVal.getcUCharge() > 0) {
            result = true;
        }
        return result;
    }

    private static DashboardDTO getUpdatedDashboardWithClaimListCheck(final DashboardDTO dashboard, final PolicyDTO policy,
            final List<ClaimListDTO> validClaimList, final String xGUID) {
        for (final ClaimListDTO claim : validClaimList) {
            if (claim.getBancsPolNum().equals(policy.getBancsPolNum())) {
                LOGGER.debug(" {} Inside getUpdatedDashboardWithClaimListCheck validation set");
                if (CommonUtils.isobjectNotNull(claim.getListOfQuotes())) {
                    dashboard.setListOfQuotes(claim.getListOfQuotes());
                } else {
                    LOGGER.debug(" {} Quote List for {} is null", policy.getBancsPolNum());
                }
            }
        }
        LOGGER.info(" {} getUpdatedDashboardWithClaimListCheck is successfull:: ");
        return dashboard;
    }

    private static DashboardDTO getUpdatedDashboardWithIDAndVData(final DashboardDTO dashboard, final PolicyDTO policy,
            final IdAndVDataDTO iDAndVData, final String xGUID) {
        for (final PartyPolicyDTO partyPolicy : iDAndVData.getListOfPolicies()) {
            if (partyPolicy.getBancsPolNum().equals(policy.getBancsPolNum())) {
                LOGGER.info("{} Inside IDAndVData validation set");
                if (CommonUtils.isobjectNotNull(partyPolicy.getListOfFunds())) {
                    dashboard.setListOfFunds(partyPolicy.getListOfFunds());
                } else {
                    LOGGER.info("{} Fund List is null");
                }
            }
        }
        return dashboard;
    }

    private static List<DashboardDTO> getDashboardListWithCheck(final DashboardDTO dashboard, final boolean corrFlagInvalid,
            final List<DashboardDTO> dashboardPolicyList, final String xGUID) {
        boolean corrFlagStatusInvalid = corrFlagInvalid;
        final DashboardDTO dashboardUpdated = dashboard;
        if (dashboard.getListOfFlags() != null) {
            LOGGER.debug(" {} Inside Data Correction validation set");
            final List<DataCorrectionFlagDTO> corrFlagList = dashboardUpdated.getListOfFlags();
            for (final DataCorrectionFlagDTO corrFlag : corrFlagList) {
                if (corrFlagCheck(corrFlag, xGUID)) {
                    corrFlagStatusInvalid = true;
                    dashboardUpdated.setEstimatedValue(MPLConstants.DEFAULTDOUBLEBANCS);
                    break;
                }
            }
        }

        getUpdatedDashboard(dashboard, dashboardUpdated, xGUID, corrFlagStatusInvalid, dashboardPolicyList);

        return dashboardPolicyList;
    }

    private static boolean corrFlagCheck(final DataCorrectionFlagDTO corrFlag, final String xGUID) {
        LOGGER.debug(" {} Inside corrFlagCheck ");
        boolean result = false;
        if (MPLConstants.BLITERAL.equalsIgnoreCase(corrFlag.getDataCorrFlag())
                || MPLConstants.ULITERAL.equalsIgnoreCase(corrFlag.getDataCorrFlag())
                || MPLConstants.MLITERAL.equalsIgnoreCase(corrFlag.getDataCorrFlag())
                || MPLConstants.TLITERAL.equalsIgnoreCase(corrFlag.getDataCorrFlag())) {
            result = true;
        }
        return result;
    }

    private static void getUpdatedDashboard(final DashboardDTO dashboard, final DashboardDTO dashboardUpdated,
            final String xGUID,
            final boolean corrFlagStatusInvalid, final List<DashboardDTO> dashboardPolicyList) {

        if (CommonUtils.isobjectNotNull(dashboard.getTotValue())) {
            getDashboardUpdatedWithEstVal(dashboardUpdated, xGUID);

            if (corrFlagStatusInvalid || dashboardInEligibleCheck(dashboardUpdated, xGUID)) {
                dashboardUpdated.setEligible(false);
                LOGGER.debug("{} Inside Non-Eligible Policy validation set for policy {}",
                        dashboardUpdated.getBancsPolNum());
            } else {
                dashboardUpdated.setEligible(true);
                LOGGER.debug(" {} Inside Eligible Policy validation set for policy {}",
                        dashboardUpdated.getBancsPolNum());
            }
            dashboardPolicyList.add(dashboardUpdated);
        } else if (dashboard.getTotValue() == null && corrFlagStatusInvalid) {

            dashboardUpdated.setEligible(false);
            LOGGER.debug(" {} Inside Non-Eligible Policy validation set for policy {}",
                    dashboardUpdated.getBancsPolNum());
            dashboardPolicyList.add(dashboardUpdated);

        }
    }

    private static DashboardDTO getDashboardUpdatedWithEstVal(final DashboardDTO dashboard, final String xGUID) {
        LOGGER.debug(" {} Inside getDashboardUpdatedWithEstVal ");
        if (dashboard.getEstimatedValue() != MPLConstants.DEFAULTDOUBLEBANCS) {
            if (dashboard.getTotValue() > 0) {
                dashboard.setEstimatedValue(dashboard.getTotValue());
            } else if (dashboard.getTotValue() == 0) {
                dashboard.setEstimatedValue(MPLConstants.DEFAULTDOUBLEBANCS);
            }
        }
        return dashboard;
    }

    private static boolean dashboardInEligibleCheck(final DashboardDTO dashboard, final String xGUID) {
        LOGGER.debug(" {} Inside dashboardInEligibleCheck ");
        boolean result = false;
        if (MPLConstants.CAPITALYLITERAL.equalsIgnoreCase(dashboard.getPartyProt())
                || MPLConstants.CAPITALYLITERAL.equalsIgnoreCase(dashboard.getPolSusp())
                || getPenRevStatusTaxRegimeAndFundNameCheck(dashboard, xGUID)
                || getgARPolProtCashInTotValCheck(dashboard, xGUID)) {
            result = true;
        }
        return result;
    }

    private static boolean getgARPolProtCashInTotValCheck(final DashboardDTO dashboard, final String xGUID) {
        LOGGER.debug(" {} Inside getgARPolProtCashInTotValCheck ");
        boolean result = false;
        if (MPLConstants.CAPITALYLITERAL.equalsIgnoreCase(dashboard.getgARorGAO())
                // ||
                // MPLConstants.CAPITALYLITERAL.equalsIgnoreCase(dashboard.getPolProt())
                // ---------------- Implemented as part of CR DCH - 028
                || MPLConstants.CAPITALYLITERAL.equalsIgnoreCase(dashboard.getCashInInProgress())
                || getTotValueCheck(dashboard)) {
            result = true;
        }
        return result;
    }

    private static Boolean getTotValueCheck(final DashboardDTO dashboard) {
        Boolean result = false;
        if (dashboard.getTotValue() > MPLConstants.BANCSTOTALVALFLAG || dashboard.getTotValue() == 0) {
            result = true;
        }
        return result;
    }

    private static boolean getPenRevStatusTaxRegimeAndFundNameCheck(final DashboardDTO dashboard, final String xGUID) {
        LOGGER.debug(" {} Inside getPenRevStatusAndTaxRegimeCheck ");
        boolean result = false;
        if (dashboard.getPenRevStatus() != null
                && MPLConstants.NOTSTARTEDLITERAL.equalsIgnoreCase(dashboard.getPenRevStatus())
                || fundNameCheck(dashboard.getListOfFunds())
                || MPLConstants.PENDINGLITERAL.equalsIgnoreCase(dashboard.getPenRevStatus())) {
            result = true;
        }
        return result;
    }

    private static boolean fundNameCheck(final List<FundDTO> fundDTOList) {
        boolean result = false;
        if (CommonUtils.isobjectNotNull(fundDTOList)) {
            for (final FundDTO fundDTO : fundDTOList) {
                if (MPLConstants.FUNDTRUSTEEKEY.equalsIgnoreCase(fundDTO.getFundName())
                        || MPLConstants.FUNDUNITISEDPROFITSKEY.equalsIgnoreCase(fundDTO.getFundName())
                        || MPLConstants.FUNDUNITISEDPROFITKEY.equalsIgnoreCase(fundDTO.getFundName())) {
                    result = true;
                    LOGGER.info("FUND NAME Validation failed");
                    break;
                }
            }
        }

        return result;
    }

    public static PartyBasicOwnerDTO getPartyBasicOwner(final String bancsUniqueCustNo, final String partyBasicResponse,
            final String xGUID)
            throws Exception {
        PartyBasicOwnerDTO partyBasicOwner = null;
        if (CommonUtils.stringNotBlank(partyBasicResponse)) {
            if (partyBasicResponse.contains(MPLConstants.ERROR_DETAIL)) {
                final ErrorResponseDTO error = mapper.readValue(partyBasicResponse, ErrorResponseDTO.class);
                LOGGER.error("PartyBasicAPI received ERROR Response {} from BaNCS for xGUID :: {}", error);
                throw new MPLException();
            } else {
                LOGGER.debug("Encashment PartyBasicDetailReponse received");
                final BancsPartyBasicDetailResponse bancsPartyBasicDetailResp = mapper.readValue(partyBasicResponse,
                        BancsPartyBasicDetailResponse.class);
                final PartyBasicDTO partyBasic = bancsPartyBasicDetailResp.getBancsPartyBasicDetail();
                final List<PartyBasicOwnerListDTO> listOfPolicies = partyBasic.getListOfPolicies();
                for (final PartyBasicOwnerListDTO partyBasicOwnerList : listOfPolicies) {
                    if (partyBasicOwnerList.getListOfOwners().size() > 1) {
                        LOGGER.debug("Multiple owners found checking if bancs unique customer number matched");
                        for (final PartyBasicOwnerDTO owner : partyBasicOwnerList.getListOfOwners()) {
                            if (owner.getBancsUniqueCustNo().equals(bancsUniqueCustNo)) {
                                LOGGER.debug(
                                        "bancs unique customer number matched for multiple owner throwing MPLException");
                                throw new MPLException();
                            }
                        }
                    } else {
                        LOGGER.debug("Single owner found checking if bancs unique customer number matched");
                        if (partyBasicOwnerList.getListOfOwners().get(0).getBancsUniqueCustNo().equals(bancsUniqueCustNo)) {
                            LOGGER.debug(
                                    "bancs unique customer number matched for single owner returning party basic owner dto");
                            partyBasicOwner = partyBasicOwnerList.getListOfOwners().get(0);
                            break;
                        }
                    }

                }
            }
        } else {
            throw new MPLException();
        }
        return partyBasicOwner;
    }
}
