/*
 * VerificationClientHelper.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.dto.activation.VerificationDTO;
import uk.co.phoenixlife.service.dto.policy.FundDTO;
import uk.co.phoenixlife.service.dto.policy.IdAndVDataDTO;
import uk.co.phoenixlife.service.dto.policy.PartyBasicDTO;
import uk.co.phoenixlife.service.dto.policy.PartyBasicOwnerDTO;
import uk.co.phoenixlife.service.dto.policy.PartyPolicyDTO;
import uk.co.phoenixlife.service.dto.staticdata.StaticIDVDataDTO;
import uk.co.phoenixlife.service.utils.CommonUtils;

/**
 * VerificationClientHelper.java
 */
@Component
public class VerificationClientHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(VerificationClientHelper.class);

    /**
     * Method to find partyPolicyDTOList
     *
     * @param idAndVDataDTO
     *            - idAndVDataDTO
     * @param xGUID
     *            - xGUID
     * @return partyPolicyDTOList
     */
    public List<PartyPolicyDTO> findPartyPolicyDTOList(final IdAndVDataDTO idAndVDataDTO, final String xGUID) {
        LOGGER.info("{} Inside findPartyPolicyDTOList method");
        List<PartyPolicyDTO> partyPolicyDTOList = null;
        if (CommonUtils.isobjectNotNull(idAndVDataDTO)) {
            partyPolicyDTOList = idAndVDataDTO.getListOfPolicies();
        } else {
            LOGGER.error("{} Inside findPartyPolicyDTOList idAndVDataDTO is NULL");
        }
        LOGGER.debug("{} Returning partyPolicyDTOList");
        return partyPolicyDTOList;
    }

    /**
     * Method to find partyBasicOwnerDTO
     *
     * @param partyBasicDTO
     *            - partyBasicDTO
     * @param xGUID
     *            - xGUID
     * @return partyBasicOwnerDTO
     */
    public PartyBasicOwnerDTO findPartyBasicOwner(final PartyBasicDTO partyBasicDTO, final String xGUID) {
        LOGGER.info("{} Inside findPartyBasicOwner method ");
        PartyBasicOwnerDTO partyBasicOwnerDTO = new PartyBasicOwnerDTO();
        if (null != partyBasicDTO && CommonUtils.isValidList(partyBasicDTO.getListOfPolicies())
                && CommonUtils.isValidList(partyBasicDTO.getListOfPolicies().get(0).getListOfOwners())) {
            partyBasicOwnerDTO = partyBasicDTO.getListOfPolicies().get(0).getListOfOwners()
                    .get(0);
        } else {
            LOGGER.error("{} failed in findPartyBasicOwner check");
        }
        LOGGER.debug("{} Returning partyBasicOwnerDTO");
        return partyBasicOwnerDTO;
    }

    /**
     * Method to find fundName
     *
     * @param partyPolicyDTO
     *            - partyPolicyDTO
     * @param xGUID
     *            - xGUID
     * @return fundName
     */
    public String findFundName(final PartyPolicyDTO partyPolicyDTO, final String xGUID) {
    	LOGGER.debug("inside Fund Name");
        StringBuffer fundName = new StringBuffer();
        LOGGER.debug("list Of funds:: {} ",partyPolicyDTO.getListOfFunds());
        LOGGER.debug("Size Of funds:: {} ",partyPolicyDTO.getListOfFunds().size());
        if (CommonUtils.isValidList(partyPolicyDTO.getListOfFunds())) {
        	LOGGER.debug("Iterating Funds now::");
        	for (final FundDTO fundDTO : partyPolicyDTO.getListOfFunds()) {
        		LOGGER.debug("Fund Name is :: {} ", fundDTO.getFundName());
                if (StringUtils.isNotBlank(fundDTO.getFundName())) {
                    fundName = fundName.append(fundDTO.getFundName()).append(MPLConstants.ATTHERATE);
                }
            }
        }
        LOGGER.debug(" {} succesfully returning from findFundName method");
        return fundName.toString();
    }

    /**
     * Method to find partyPolicyDTO
     *
     * @param partyPolicyDTOList
     *            - partyPolicyDTOList
     * @param bancsPolNum
     *            - bancs policy number
     * @return partyPolicyDTO
     */
    public List<PartyPolicyDTO> findPartyPolicyDTOValue(final List<PartyPolicyDTO> partyPolicyDTOList, final List<String> bancsPolNumList) {
        List<PartyPolicyDTO> partyPolicyDTOListIntern = new ArrayList<PartyPolicyDTO>();
        if (CommonUtils.isValidList(partyPolicyDTOList)) {
            for (final PartyPolicyDTO partyPolicy : partyPolicyDTOList) {
                if (bancsPolNumList.size()>0 && StringUtils.isNotBlank(partyPolicy.getBancsPolNum())
                        && bancsPolNumList.contains(partyPolicy.getBancsPolNum())) {
                	partyPolicyDTOListIntern.add(partyPolicy);
                }
            }
        }
        return partyPolicyDTOListIntern;
    }

    /**
     * Method for checking null condition while checking answer
     *
     * @param sessionCheckAnswerMap
     *            - map stored in session
     * @param verificationDTO
     *            - verificationDTO
     * @return True or false
     */
    public Boolean checkNullConditionWhileCheckingAnswer(
            final ConcurrentHashMap<String, ConcurrentHashMap<String, String>> sessionCheckAnswerMap,
            final VerificationDTO verificationDTO) {
    	LOGGER.debug("Inside checkNullConditionWhileCheckingAnswer");
        return sessionCheckAnswerMap.size() > 0 && StringUtils.isNotBlank(verificationDTO.getAnswer())
                && StringUtils.isNotBlank(verificationDTO.getQuestion())
                && StringUtils.isNotBlank(verificationDTO.getQuestionLabel());
    }

    /**
     * Method for checking static data null condition before inserting values in
     * map
     *
     * @param staticQuestionData
     *            - object containing static data
     * @return true or false
     */
    public Boolean checkNullConditionWhileInsertingValuesInMap(final StaticIDVDataDTO staticQuestionData) {

        return null != staticQuestionData && StringUtils.isNotBlank(staticQuestionData.getQuestionKey())
                && StringUtils.isNotBlank(staticQuestionData.getQuestionLabel())
                && StringUtils.isNotBlank(staticQuestionData.getQuestionValue());
    }

}
