package uk.co.phoenixlife.service.helper;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import uk.co.phoenixlife.service.client.security.MPLSecurityClient;
import uk.co.phoenixlife.service.dao.entity.MPLAccountLockOut;
import uk.co.phoenixlife.service.dao.entity.MPLCustomerPolicy;
import uk.co.phoenixlife.service.dao.entity.MPLPolicyLockOut;
import uk.co.phoenixlife.service.dao.entity.MPLVerificationAttempts;
import uk.co.phoenixlife.service.dao.repository.CustomerAccountLockOutRepository;
import uk.co.phoenixlife.service.dao.repository.CustomerPolicyLockOutRepository;
import uk.co.phoenixlife.service.dao.repository.CustomerPolicyRepository;
import uk.co.phoenixlife.service.dao.repository.VerificationAttemptsRepository;
import uk.co.phoenixlife.service.dto.ErrorListDTO;
import uk.co.phoenixlife.service.dto.MiErrorRequestDTO;
import uk.co.phoenixlife.service.dto.activation.CustomerActivationDTO;
import uk.co.phoenixlife.service.dto.activation.VerificationDTO;
import uk.co.phoenixlife.service.dto.activation.VerificationMapAndLockedCountDTO;
import uk.co.phoenixlife.service.dto.activation.VerificationSuccessDTO;
import uk.co.phoenixlife.service.interfaces.MIComplianceServiceInterface;
import uk.co.phoenixlife.service.utils.DateUtils;

@Component
public class VerificationQuestionHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(VerificationQuestionHelper.class);

    @Autowired
    private VerificationClientHelper verificationClientHelper;

    @Autowired
    private VerificationQuestionServiceClientHelper verificationQuestionServiceClientHelper;

    @Autowired
    private MPLSecurityClient mPLSecurityClient;

    @Autowired
    private VerificationAttemptsRepository verificationAttemptsRepository;

    @Autowired
    private CustomerPolicyLockOutRepository customerPolicyLockOutRepository;

    @Autowired
    private CustomerAccountLockOutRepository customerAccountLockOutRepository;

    @Autowired
    private CustomerPolicyRepository customerPolicyRepository;
    
    @Autowired
	 private MIComplianceServiceInterface miComplianceServiceInterface;

    @Value("${policylock.policyverification.limit}")
    private int policyVerificationThresholdLimit;

    @Value("${accountlock.verification.limit}")
    private int verificationThresholdLimit;

    @Transactional
    public String setResult(final int successFlag, final String result,
            final CustomerActivationDTO customerActivationDTO, final Integer lockedCount, final String xGUID)
            throws Exception {
        LOGGER.debug("{} inside setResult method {}, {}", successFlag, customerActivationDTO.getPolicyId());
        String updatedResult = result;
        if (1 != successFlag) {
            updatedResult = checkLastAttempt(customerActivationDTO.getUserName(), customerActivationDTO.getPolicyId(),
                    customerActivationDTO.getCheckAnswerMap(), lockedCount, xGUID, successFlag);
        }
        LOGGER.info("{} Returning updatedResult ");
        return updatedResult;
    }

    private String checkLastAttempt(final String userName, final long policyId,
            final ConcurrentHashMap<String, ConcurrentHashMap<String, String>> sessionCheckAnswerMap,
            final Integer lockedCount, final String xGUID, final int successFlag) throws Exception {
        LOGGER.debug("{} inside checkLastAttempt method {}", policyId);
        String result = StringUtils.EMPTY;
        final Integer checkAnswerMapSize = sessionCheckAnswerMap.size();
        if (checkAnswerMapSize.equals(0) && lockedCount.equals(verificationThresholdLimit + 1)) {
            LOGGER.debug("{} If condition satisfied for last attempt");
            lockAccountWithRespectToVerificationAttempt(userName);
            // uncomment this for production
            mPLSecurityClient.lockAccount(userName, true);
            
            MiErrorRequestDTO miErrorRequestDTO = new MiErrorRequestDTO();
        	ErrorListDTO  errorListDTO = new ErrorListDTO();
        	errorListDTO.setPageId("PG111");
        	errorListDTO.setNextPageId("PG202");
        	errorListDTO.setMessageCount(1);
        	errorListDTO.setTimeSpent(1);
        	errorListDTO.setMessageId("E024");
        	List<ErrorListDTO> errorDTOList = new ArrayList<ErrorListDTO>();
        	errorDTOList.add(errorListDTO);
        	miErrorRequestDTO.setErrorListDTO(errorDTOList);
        	miComplianceServiceInterface.submitErrorList(miErrorRequestDTO);
        	
            result = "fail";
            
        } else if (checkAnswerMapSize.equals(0) && lockedCount < verificationThresholdLimit + 1) {
            LOGGER.debug("{} If condition satisfied for question exhaust.");
            lockAccountWithRespectToVerificationAttempt(userName);
            // uncomment this for production
            mPLSecurityClient.lockAccount(userName, true);
            
            MiErrorRequestDTO miErrorRequestDTO = new MiErrorRequestDTO();
        	ErrorListDTO  errorListDTO = new ErrorListDTO();
        	errorListDTO.setPageId("PG111");
        	errorListDTO.setNextPageId("PG202");
        	errorListDTO.setMessageCount(1);
        	errorListDTO.setTimeSpent(1);
        	errorListDTO.setMessageId("E036");
        	List<ErrorListDTO> errorDTOList = new ArrayList<ErrorListDTO>();
        	errorDTOList.add(errorListDTO);
        	miErrorRequestDTO.setErrorListDTO(errorDTOList);
        	miComplianceServiceInterface.submitErrorList(miErrorRequestDTO);
        	
            result = "exhaust";
            
        } else if (2 == successFlag) {
        	
        	MiErrorRequestDTO miErrorRequestDTO = new MiErrorRequestDTO();
        	ErrorListDTO  errorListDTO = new ErrorListDTO();
        	errorListDTO.setPageId("PG111");
        	errorListDTO.setNextPageId("PG202");
        	errorListDTO.setMessageCount(1);
        	errorListDTO.setTimeSpent(1);
        	errorListDTO.setMessageId("E022");
        	List<ErrorListDTO> errorDTOList = new ArrayList<ErrorListDTO>();
        	errorDTOList.add(errorListDTO);
        	miErrorRequestDTO.setErrorListDTO(errorDTOList);
        	miComplianceServiceInterface.submitErrorList(miErrorRequestDTO);
        	
            result = "policyLocked";
            
        } else {
            LOGGER.info("{} If condition satisfied to ask next question");
            lockAccountWithRespectToVerificationAttempt(userName);
            result = "shuffleAndShow";
        }
        LOGGER.info("{} Returning result checkLastAttempt");
        return result;
    }

    @Transactional
    public void lockAccountWithRespectToVerificationAttempt(final String userName) {

        final Long malId = customerAccountLockOutRepository.getMalIdOfAccountLockOut(userName);
        MPLAccountLockOut savedResult = customerAccountLockOutRepository.findOne(malId);
        if (savedResult.getVerificationFailCount() == verificationThresholdLimit) {
            savedResult = setLockDetailsForMPLAccountLockOutTable(savedResult);
            savedResult.setVerificationFailCount(savedResult.getVerificationFailCount() + 1);
        } else {
            savedResult = setIncrementDetailsForMPLAccountlockOuttable(savedResult);
            savedResult.setVerificationFailCount(savedResult.getVerificationFailCount() + 1);
        }
        customerAccountLockOutRepository.saveAndFlush(savedResult);
    }

    public VerificationSuccessDTO checkLastPremiumPaidAnswer(final VerificationDTO verificationDTO,
            final CustomerActivationDTO customerActivationDTO, final String xGUID,
            final VerificationMapAndLockedCountDTO verificationMapAndLockedCountDTOResponse) throws Exception {
        LOGGER.debug("{} inside checkLastPremiumPaidAnswer method ");
        final VerificationSuccessDTO successDTO = new VerificationSuccessDTO();
        int successFlag = 0;
        int identifySource;
        identifySource = 2;
        verificationMapAndLockedCountDTOResponse.setCheckAnswerMap(customerActivationDTO.getCheckAnswerMap());
        verificationMapAndLockedCountDTOResponse.setLockedCount(customerActivationDTO.getLockedCount());
        if (verificationClientHelper.checkNullConditionWhileCheckingAnswer(customerActivationDTO.getCheckAnswerMap(),
                verificationDTO)) {
            successFlag = verificationQuestionServiceClientHelper.findSuccessFlagForLastPrmPd(verificationDTO,
                    customerActivationDTO, xGUID);
        }
        if (0 == successFlag) {
            LOGGER.info("---------Wrong answer-----------checkLastPremiumPaidAnswer method");
            final VerificationMapAndLockedCountDTO response = verificationQuestionServiceClientHelper.removeFromMap(
                    verificationDTO.getQuestionKey(), customerActivationDTO, verificationMapAndLockedCountDTOResponse);
            successDTO.setLockedListCount(response.getLockedCount());
            successFlag = updateVerificationAttemptsTable(customerActivationDTO, verificationDTO.getQuestionKey(),
                    identifySource, xGUID);
            sendErrorMsgCount();
        }
        successDTO.setSuccessFlag(successFlag);
        successDTO.setCustomerId(customerActivationDTO.getCustomerId());
        successDTO.setPolicyId(customerActivationDTO.getPolicyId());
        LOGGER.debug("{} Returning from checkLastPremiumPaidAnswer with successFlag {}", successFlag);
        return successDTO;
    }

    public VerificationSuccessDTO checkOtherAnswer(final VerificationDTO verificationDTO,
            final CustomerActivationDTO customerActivationDTO, final String xGUID,
            final VerificationMapAndLockedCountDTO verificationMapAndLockedCountDTOResponse) throws Exception {
        final VerificationSuccessDTO successDTO = new VerificationSuccessDTO();
        LOGGER.debug("{} inside checkOtherAnswer method ");
        int successFlag = 0;
        int identifySource;
        identifySource = 2;
        verificationMapAndLockedCountDTOResponse.setCheckAnswerMap(customerActivationDTO.getCheckAnswerMap());
        verificationMapAndLockedCountDTOResponse.setLockedCount(customerActivationDTO.getLockedCount());
        if (customerActivationDTO.getCheckAnswerMap().size() > 0 && StringUtils.isNotBlank(verificationDTO.getAnswer())) {
            StringBuffer storedAnswer = new StringBuffer();
            storedAnswer = storedAnswer
                    .append(customerActivationDTO.getCheckAnswerMap().get(verificationDTO.getQuestionKey())
                            .get(verificationDTO.getQuestionLabel() + "/" + verificationDTO.getQuestion()));
            if (storedAnswer.toString().trim().equalsIgnoreCase(verificationDTO.getAnswer())) {
                LOGGER.info("{} ---------Correct answer-----------checkOtherAnswer method");
                successFlag = 1;
            }
        }
        if (0 == successFlag) {
            LOGGER.info("{} ---------Wrong answer-----------checkOtherAnswer method");
            final VerificationMapAndLockedCountDTO response = verificationQuestionServiceClientHelper.removeFromMap(
                    verificationDTO.getQuestionKey(), customerActivationDTO, verificationMapAndLockedCountDTOResponse);
            LOGGER.debug("{} Removed from map in checkOtherAnswer method ");
            successFlag = updateVerificationAttemptsTable(customerActivationDTO, verificationDTO.getQuestionKey(),
                    identifySource, xGUID);
            successDTO.setLockedListCount(response.getLockedCount());
            sendErrorMsgCount();
        }
        successDTO.setSuccessFlag(successFlag);
        successDTO.setCustomerId(customerActivationDTO.getCustomerId());
        successDTO.setPolicyId(customerActivationDTO.getPolicyId());
        LOGGER.debug("{} Returning from checkOtherAnswer method ");
        return successDTO;
    }

    public VerificationSuccessDTO checkPolicyStartDateAnswer(final VerificationDTO verificationDTO,
            final CustomerActivationDTO customerActivationDTO, final String xGUID,
            final VerificationMapAndLockedCountDTO verificationMapAndLockedCountDTOResponse) throws Exception {
        LOGGER.debug("{} inside checkPolicyStartDateAnswer method ");
        VerificationSuccessDTO successDTO = null;
        int identifySource;
        identifySource = 2;
        LocalDate inputDate = null;
        LocalDate storedDate = null;
        StringBuffer questionKeyStr = new StringBuffer();
        verificationMapAndLockedCountDTOResponse.setCheckAnswerMap(customerActivationDTO.getCheckAnswerMap());
        verificationMapAndLockedCountDTOResponse.setLockedCount(customerActivationDTO.getLockedCount());
        if (verificationClientHelper.checkNullConditionWhileCheckingAnswer(customerActivationDTO.getCheckAnswerMap(),
                verificationDTO)) {
            questionKeyStr = questionKeyStr.append(verificationDTO.getQuestionLabel()).append("/")
                    .append(verificationDTO.getQuestion());
            inputDate = DateUtils.getDateFromString(verificationDTO.getAnswer(), "dd/MM/yyyy");
            storedDate = DateUtils.getDateFromString(
                    customerActivationDTO.getCheckAnswerMap().get(verificationDTO.getQuestionKey())
                            .get(questionKeyStr.toString()),
                    "yyyy-MM-dd");
        }
        successDTO = findSuccessFlag(storedDate, inputDate, customerActivationDTO, verificationDTO.getQuestionKey(),
                identifySource, xGUID, verificationMapAndLockedCountDTOResponse);
        successDTO.setCustomerId(customerActivationDTO.getCustomerId());
        successDTO.setPolicyId(customerActivationDTO.getPolicyId());
        LOGGER.debug("{} Returning from checkPolicyStartDateAnswer method ");

        return successDTO;
    }

    private VerificationSuccessDTO findSuccessFlag(final LocalDate storedDate, final LocalDate inputDate,
            final CustomerActivationDTO customerActivationDTO,
            final String questionKey, final int identifySource, final String xGUID,
            final VerificationMapAndLockedCountDTO verificationMapAndLockedCountDTOResponse) throws Exception {
        LOGGER.debug("{} inside findSuccessFlag method ");
        int successFlag = 0;
        final VerificationSuccessDTO successDTO = new VerificationSuccessDTO();
        if (storedDate != null && checkYearCondition(storedDate, inputDate)) {
            LOGGER.info("{} ---------Correct answer-----------findSuccessFlag method");
            successFlag = 1;
        } else {
            LOGGER.info("{}---------Wrong answer-----------findSuccessFlag method");
            final VerificationMapAndLockedCountDTO response = verificationQuestionServiceClientHelper.removeFromMap(
                    questionKey,
                    customerActivationDTO, verificationMapAndLockedCountDTOResponse);
            successFlag = updateVerificationAttemptsTable(customerActivationDTO, questionKey, identifySource, xGUID);
            successDTO.setLockedListCount(response.getLockedCount());
            sendErrorMsgCount();
        }
        successDTO.setSuccessFlag(successFlag);
        LOGGER.debug("{} Returning from findSuccessFlag method ");
        return successDTO;
    }

    private Boolean checkYearCondition(final LocalDate storedDate, final LocalDate inputDate) {
        return (inputDate.isAfter(DateUtils.calDateForOneYrBefore(storedDate))
                || inputDate.equals(DateUtils.calDateForOneYrBefore(storedDate)))
                && (inputDate.isBefore(DateUtils.calDateForOneYrAfter(storedDate))
                        || inputDate.equals(DateUtils.calDateForOneYrAfter(storedDate)));
    }

    public VerificationSuccessDTO checkFundNameAnswer(final VerificationDTO verificationDTO,
            final CustomerActivationDTO customerActivationDTO, final String xGUID,
            final VerificationMapAndLockedCountDTO verificationMapAndLockedCountDTOResponse) throws Exception {
        LOGGER.debug("{} inside checkFundNameAnswer method ");
        final VerificationSuccessDTO successDTO = new VerificationSuccessDTO();
        int successFlag = 0;
        boolean fundNameCorrect = false;
        int identifySource;
        identifySource = 2;
        verificationMapAndLockedCountDTOResponse.setCheckAnswerMap(customerActivationDTO.getCheckAnswerMap());
        verificationMapAndLockedCountDTOResponse.setLockedCount(customerActivationDTO.getLockedCount());
        if (verificationClientHelper.checkNullConditionWhileCheckingAnswer(customerActivationDTO.getCheckAnswerMap(),
                verificationDTO)) {
            StringBuffer storedAnswer = new StringBuffer();
            storedAnswer = storedAnswer
                    .append(customerActivationDTO.getCheckAnswerMap().get(verificationDTO.getQuestionKey())
                            .get(verificationDTO.getQuestionLabel() + "/" + verificationDTO.getQuestion()));
            final String[] fundNameArray = storedAnswer.toString().split("@");
            final ArrayList<String> arrayList = new ArrayList<String>(Arrays.asList(fundNameArray));
            fundNameCorrect = findFundNameCorrect(arrayList, verificationDTO.getAnswer());
            if (fundNameCorrect) {
                LOGGER.info("{}---------Correct answer-----------checkFundNameAnswer method");
                successFlag = 1;
            }
        }
        if (0 == successFlag) {
            LOGGER.info("{} ---------Wrong answer-----------checkFundNameAnswer method");
            final VerificationMapAndLockedCountDTO response = verificationQuestionServiceClientHelper.removeFromMap(
                    verificationDTO.getQuestionKey(), customerActivationDTO, verificationMapAndLockedCountDTOResponse);
            successFlag = updateVerificationAttemptsTable(customerActivationDTO, verificationDTO.getQuestionKey(),
                    identifySource, xGUID);
            successDTO.setLockedListCount(response.getLockedCount());
            sendErrorMsgCount();
        }
        LOGGER.debug("{} Returning from checkFundNameAnswer with success Flag {}", successFlag);
        successDTO.setSuccessFlag(successFlag);
        successDTO.setCustomerId(customerActivationDTO.getCustomerId());
        successDTO.setPolicyId(customerActivationDTO.getPolicyId());
        return successDTO;
    }

    private boolean findFundNameCorrect(final List<String> arrayList, final String answer) {
        boolean fundNameCorrect = false;
        for (final String current : arrayList) {
            if (current.trim().equalsIgnoreCase(answer)) {
                fundNameCorrect = true;
                LOGGER.info("----------Correct Answer--------findFundNameCorrect method");
                break;
            }
        }
        return fundNameCorrect;
    }

    public VerificationSuccessDTO checkBenefitNameAnswer(final VerificationDTO verificationDTO,
            final CustomerActivationDTO customerActivationDTO, final String xGUID,
            final VerificationMapAndLockedCountDTO verificationMapAndLockedCountDTOResponse) throws Exception {
        LOGGER.debug("{} inside checkBenefitNameAnswer method ");
        final VerificationSuccessDTO successDTO = new VerificationSuccessDTO();
        int successFlag = 0;
        int identifySource;
        identifySource = 2;
        verificationMapAndLockedCountDTOResponse.setCheckAnswerMap(customerActivationDTO.getCheckAnswerMap());
        verificationMapAndLockedCountDTOResponse.setLockedCount(customerActivationDTO.getLockedCount());
        if (verificationClientHelper.checkNullConditionWhileCheckingAnswer(customerActivationDTO.getCheckAnswerMap(),
                verificationDTO)) {
            final Double inputAnswer = Double.valueOf(verificationDTO.getAnswer());
            final Double storedAnswer = Double.valueOf(
                    customerActivationDTO.getCheckAnswerMap().get(verificationDTO.getQuestionKey())
                            .get(verificationDTO.getQuestionLabel()
                                    + "/" + verificationDTO.getQuestion()));
            if (inputAnswer.equals(storedAnswer)) {
                LOGGER.info("{}---------Correct answer-----------checkBenefitNameAnswer method");
                successFlag = 1;
            }
        }
        if (0 == successFlag) {
            LOGGER.info("{}---------Wrong answer-----------checkBenefitNameAnswer method");
            final VerificationMapAndLockedCountDTO response = verificationQuestionServiceClientHelper.removeFromMap(
                    verificationDTO.getQuestionKey(), customerActivationDTO, verificationMapAndLockedCountDTOResponse);
            successFlag = updateVerificationAttemptsTable(customerActivationDTO, verificationDTO.getQuestionKey(),
                    identifySource, xGUID);
            successDTO.setLockedListCount(response.getLockedCount());
            sendErrorMsgCount();
        }
        LOGGER.debug("{} Returning success Flag for checkBenefitNameAnswer {}", successFlag);
        successDTO.setSuccessFlag(successFlag);
        successDTO.setCustomerId(customerActivationDTO.getCustomerId());
        successDTO.setPolicyId(customerActivationDTO.getPolicyId());
        return successDTO;
    }

    public int updateVerificationAttemptsTable(final CustomerActivationDTO customerActivationDTO,
            final String questionKey, final int identifySource, final String xGUID)
            throws Exception {
        LOGGER.debug("Inside PolicyVerificationService class updateVerificationAttemptsTable method {}, {}, {}",
                customerActivationDTO.getPolicyId(), questionKey, identifySource);
        final MPLCustomerPolicy policy = customerPolicyRepository.findOne(customerActivationDTO.getPolicyId());
        final MPLVerificationAttempts attempt = new MPLVerificationAttempts();
        attempt.setVerificationPolicy(policy);
        attempt.setQuestionId(questionKey);
        if (identifySource == 1) {
            attempt.setQuestionSkipped('Y');
        } else {
            attempt.setQuestionSkipped('N');
        }
        attempt.setCreatedOn(Timestamp.valueOf(LocalDateTime.now()));
        attempt.setCreatedBy("SYSTEM");
        verificationAttemptsRepository.saveAndFlush(attempt);
        LOGGER.info("Sucessfull Inside PolicyVerificationService class updateVerificationAttemptsTable method {}, {}, {}",
                customerActivationDTO.getPolicyId(), questionKey, identifySource);
        return doWrongVerificationAttemptOperation(customerActivationDTO);
    }

    private int doWrongVerificationAttemptOperation(final CustomerActivationDTO customerActivationDTO) {
        final Long mplId = customerPolicyLockOutRepository.getPolicyLockOutObject(customerActivationDTO.getPolicyNumber());
        int successFlag = 0;
        MPLPolicyLockOut mPLPolicyLockOut = customerPolicyLockOutRepository.findOne(mplId);
        if (mPLPolicyLockOut.getPolicyVerificationCount() == policyVerificationThresholdLimit) {
            mPLPolicyLockOut = setLockDetailsForMPLPolicyLockOutTable(mPLPolicyLockOut);
            successFlag = 2;
        } else {
            mPLPolicyLockOut = setIncrementDetailsForMPLPolicyLockOutTable(mPLPolicyLockOut);
        }
        customerPolicyLockOutRepository.saveAndFlush(mPLPolicyLockOut);
        return successFlag;
    }

    private MPLAccountLockOut setIncrementDetailsForMPLAccountlockOuttable(final MPLAccountLockOut savedResult) {
        savedResult.setLastUpdatedOn(Timestamp.valueOf(LocalDateTime.now()));
        savedResult.setLastUpdatedBy("SYSTEM");
        return savedResult;
    }

    private MPLAccountLockOut setLockDetailsForMPLAccountLockOutTable(final MPLAccountLockOut savedResult) {
        savedResult.setAccountLockStatus('Y');
        savedResult.setLastUpdatedBy("SYSTEM");
        savedResult.setLastUpdatedOn(Timestamp.valueOf(LocalDateTime.now()));
        return savedResult;
    }

    private MPLPolicyLockOut setIncrementDetailsForMPLPolicyLockOutTable(final MPLPolicyLockOut mPLPolicyLockOut) {
        mPLPolicyLockOut.setPolicyVerificationCount(mPLPolicyLockOut.getPolicyVerificationCount() + 1);
        mPLPolicyLockOut.setLastUpdatedOn(Timestamp.valueOf(LocalDateTime.now()));
        mPLPolicyLockOut.setLastUpdatedBy("SYSTEM");
        return mPLPolicyLockOut;
    }

    private MPLPolicyLockOut setLockDetailsForMPLPolicyLockOutTable(final MPLPolicyLockOut mPLPolicyLockOut) {
        mPLPolicyLockOut.setPolicyLockStatus('Y');
        mPLPolicyLockOut.setLastUpdatedBy("SYSTEM");
        mPLPolicyLockOut.setPolicyVerificationCount(mPLPolicyLockOut.getPolicyVerificationCount() + 1);
        mPLPolicyLockOut.setLastUpdatedOn(Timestamp.valueOf(LocalDateTime.now()));
        return mPLPolicyLockOut;
    }
    
    private void sendErrorMsgCount(){
    	
    	MiErrorRequestDTO miErrorRequestDTO = new MiErrorRequestDTO();
    	ErrorListDTO  errorListDTO = new ErrorListDTO();
    	errorListDTO.setPageId("PG111");
    	errorListDTO.setNextPageId("PG202");
    	errorListDTO.setMessageCount(1);
    	errorListDTO.setTimeSpent(1);
    	errorListDTO.setMessageId("E018");
    	List<ErrorListDTO> errorDTOList = new ArrayList<ErrorListDTO>();
    	errorDTOList.add(errorListDTO);
    	miErrorRequestDTO.setErrorListDTO(errorDTOList);
    	miComplianceServiceInterface.submitErrorList(miErrorRequestDTO);
    }
}
