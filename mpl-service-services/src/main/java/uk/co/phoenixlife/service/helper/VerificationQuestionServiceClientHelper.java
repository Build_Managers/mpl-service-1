/*
 * VerificationQuestionServiceClientHelper.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.helper;

import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import uk.co.phoenixlife.service.async.BancsUpdateService;
import uk.co.phoenixlife.service.client.security.MPLSecurityClient;
import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dao.entity.MPLCustomer;
import uk.co.phoenixlife.service.dao.entity.MPLCustomerUpdateStatus;
import uk.co.phoenixlife.service.dao.repository.CustomerRepositoryImpl;
import uk.co.phoenixlife.service.dao.repository.CustomerUpdateStatusRepository;
import uk.co.phoenixlife.service.dto.activation.CustomerActivationDTO;
import uk.co.phoenixlife.service.dto.activation.VerificationDTO;
import uk.co.phoenixlife.service.dto.activation.VerificationMapAndLockedCountDTO;
import uk.co.phoenixlife.service.dto.activation.VerificationReturnValuesDTO;
import uk.co.phoenixlife.service.dto.policy.PartyPolicyDTO;
import uk.co.phoenixlife.service.dto.staticdata.StaticIDVDataDTO;
import uk.co.phoenixlife.service.mapper.UpdateAttemptsMapper;
import uk.co.phoenixlife.service.utils.CommonUtils;

/**
 * VerificationQuestionServiceClientHelper.java
 */
@Component
public class VerificationQuestionServiceClientHelper {

    @Autowired
    private VerificationClientHelper verificationClientHelper;

    @Autowired
    private CustomerRepositoryImpl customerRepositoryImpl;

    @Autowired
    private MPLSecurityClient mPLSecurityClient;

    @Autowired
    CustomerUpdateStatusRepository customerUpdateStatusRepository;

    @Autowired
    BancsUpdateService bancsUpdateService;

    @Autowired
    private VerificationQuestionHelper verificationQuestionHelper;
    private static final Logger LOGGER = LoggerFactory.getLogger(VerificationQuestionServiceClientHelper.class);
    private static final int MAX = 3;

    /**
     * createFinalMapObject to create final main map
     *
     * @param verificationDTO
     *            object containing string values
     * @param staticDataList
     *            list containing question and answer
     * @param lockedQuestionKeylist
     *            list containing locked question key
     * @param partyPolicyDTO
     *            object containing rest answer
     * @throws ParseException
     *             exception
     * @return checkAnswerMap map containing question, question key, answer,
     *         question label
     */
    public ConcurrentHashMap<String, ConcurrentHashMap<String, String>> createFinalMapObject(
            final VerificationDTO verificationDTO,
            final List<StaticIDVDataDTO> staticDataList,
            final List<String> lockedQuestionKeylist, final PartyPolicyDTO partyPolicyDTO) throws ParseException {
        final ConcurrentHashMap<String, ConcurrentHashMap<String, String>> checkAnswerMap = new ConcurrentHashMap<String, ConcurrentHashMap<String, String>>();
        if (CommonUtils.isValidList(staticDataList)) {
            final int lockedQuestionKeylistCount = lockedQuestionKeylist.size();
            for (final StaticIDVDataDTO staticIDVDataDTO : staticDataList) {
                if (null != staticIDVDataDTO
                        && !lockedQuestionKeylist.contains(staticIDVDataDTO.getQuestionKey())
                        && lockedQuestionKeylistCount + checkAnswerMap.size() < MAX) {
                    LOGGER.debug("Question is not asked before and checkAnswerMap size is less than 3");
                    insertQuestionAnswerInMap(staticIDVDataDTO, verificationDTO,
                            partyPolicyDTO, checkAnswerMap);
                }
            }
        }
        return checkAnswerMap;
    }

    private void insertQuestionAnswerInMap(final StaticIDVDataDTO staticIDVDataDTO, final VerificationDTO verificationDTO,
            final PartyPolicyDTO partyPolicyDTO,
            final ConcurrentHashMap<String, ConcurrentHashMap<String, String>> checkAnswerMap)
            throws ParseException {
        final String questionKey = staticIDVDataDTO.getQuestionKey();
        if (StringUtils.isNotBlank(questionKey)) {
            switch (questionKey) {
            case MPLConstants.FUNDNAME:
                if (StringUtils.isNotBlank(verificationDTO.getFundName())) {
                    insertFundNameValuesInMap(staticIDVDataDTO, verificationDTO.getFundName(), checkAnswerMap);
                }
                break;
            case MPLConstants.POLICYSTARTDATE:
                if (null != partyPolicyDTO && null != partyPolicyDTO.getPolStDate()) {
                    insertPolicyStartDateValuesInMap(staticIDVDataDTO, partyPolicyDTO.getPolStDate(), checkAnswerMap);
                }
                break;
            default:
                insertQuestionAnswerInMapBenftNiNo(staticIDVDataDTO, partyPolicyDTO, checkAnswerMap, verificationDTO);
                break;
            }
        }
    }

    /**
     * Helper method to prepare the map
     *
     * @param staticQuestionData
     * @param niNo
     * @param netinitPremium
     * @param partyPolicyDTO
     * @param benefitName
     * @param checkAnswerMap
     * @throws ParseException
     */
    private void insertQuestionAnswerInMapBenftNiNo(final StaticIDVDataDTO staticQuestionData,
            final PartyPolicyDTO partyPolicyDTO,
            final ConcurrentHashMap<String, ConcurrentHashMap<String, String>> checkAnswerMap,
            final VerificationDTO verificationDTO)
            throws ParseException {
        final String questionKey = staticQuestionData.getQuestionKey();
        switch (questionKey) {
        case MPLConstants.NINO:
            if (StringUtils.isNotBlank(verificationDTO.getNiNo())) {
                insertNationalInsuranceNumberValuesInMap(staticQuestionData, verificationDTO.getNiNo(), checkAnswerMap);
            }
            break;
        case MPLConstants.BENEFITNAME:
            if (StringUtils.isNotBlank(verificationDTO.getNetinitPremium())
                    && StringUtils.isNotBlank(verificationDTO.getBenefitName())) {
                insertBenefitNameValuesInMap(staticQuestionData, verificationDTO.getNetinitPremium(),
                        verificationDTO.getBenefitName(), checkAnswerMap);
            }
            break;
        default:
            insertQuestionAnswerInMapLstPrimPd(staticQuestionData, partyPolicyDTO, checkAnswerMap);
            break;
        }
    }

    /**
     * Helper method to prepare the map
     *
     * @param staticQuestionData
     * @param niNo
     * @param netinitPremium
     * @param partyPolicyDTO
     * @param checkAnswerMap
     * @param benefitName
     * @throws ParseException
     */
    private void insertQuestionAnswerInMapLstPrimPd(final StaticIDVDataDTO staticQuestionData,
            final PartyPolicyDTO partyPolicyDTO,
            final ConcurrentHashMap<String, ConcurrentHashMap<String, String>> checkAnswerMap)
            throws ParseException {
        final String questionKey = staticQuestionData.getQuestionKey();
        switch (questionKey) {
        case MPLConstants.LASTPREMIUMPAID:
            if (checkNullConditionForLastPremiumPaid(partyPolicyDTO)) {
                insertLastPremiumPaidValuesInMap(staticQuestionData, partyPolicyDTO.getLastPremPaid().toString(),
                        checkAnswerMap);
            }
            break;
        case MPLConstants.SORTCODEACCOUNTNUMBER:
            if (checkNullConditionForSortCodeAccountNumber(partyPolicyDTO)) {
                insertSortCodeBankAcctValuesInMap(staticQuestionData, partyPolicyDTO.getSortCode().toString(),
                        partyPolicyDTO.getBankAcct().toString(), checkAnswerMap);
            }
            break;
        default:
            if (null != partyPolicyDTO && null != partyPolicyDTO.getPremiumDueDate()) {
                insertPremiumDueDate(staticQuestionData, partyPolicyDTO.getPremiumDueDate(), checkAnswerMap);
            }
            break;
        }
    }

    private void insertPremiumDueDate(final StaticIDVDataDTO staticQuestionData, final String premiumDueDate,
            final ConcurrentHashMap<String, ConcurrentHashMap<String, String>> checkAnswerMap) {
        StringBuffer questionLabelValue = new StringBuffer();
        if (verificationClientHelper.checkNullConditionWhileInsertingValuesInMap(staticQuestionData)) {
            LOGGER.debug("If condition satisfied for premiumDueDate setting values in checkAnswerMap");
            final String questionKey = staticQuestionData.getQuestionKey();
            questionLabelValue = questionLabelValue.append(staticQuestionData.getQuestionLabel())
                    .append(MPLConstants.BACKLSLASH)
                    .append(staticQuestionData.getQuestionValue());
            final ConcurrentHashMap<String, String> valueLabelAnswerMap = new ConcurrentHashMap<String, String>();
            valueLabelAnswerMap.put(questionLabelValue.toString(), premiumDueDate);
            checkAnswerMap.put(questionKey, valueLabelAnswerMap);
        }
    }

    private Boolean checkNullConditionForLastPremiumPaid(final PartyPolicyDTO partyPolicyDTO) {
        return null != partyPolicyDTO && null != partyPolicyDTO.getLastPremPaid()
                && MPLConstants.DIRECTDEBIT.equalsIgnoreCase(partyPolicyDTO.getPaymentMethodLit());
    }

    private Boolean checkNullConditionForSortCodeAccountNumber(final PartyPolicyDTO partyPolicyDTO) {
        return null != partyPolicyDTO && null != partyPolicyDTO.getSortCode()
                && null != partyPolicyDTO.getBankAcct()
                && MPLConstants.DIRECTDEBIT.equalsIgnoreCase(partyPolicyDTO.getPaymentMethodLit());
    }

    private void insertPolicyStartDateValuesInMap(final StaticIDVDataDTO staticIDVDataDTO, final Date policyStartDate,
            final ConcurrentHashMap<String, ConcurrentHashMap<String, String>> checkAnswerMap)
            throws ParseException {
        StringBuffer questionLabelValue = new StringBuffer();
        if (verificationClientHelper.checkNullConditionWhileInsertingValuesInMap(staticIDVDataDTO)) {
            LOGGER.debug("If condition satisfied for policyStartDate setting values in checkAnswerMap");
            final String questionKey = staticIDVDataDTO.getQuestionKey();
            questionLabelValue = questionLabelValue.append(staticIDVDataDTO.getQuestionLabel())
                    .append(MPLConstants.BACKLSLASH)
                    .append(staticIDVDataDTO.getQuestionValue());
            final ConcurrentHashMap<String, String> valueLabelAnswerMap = new ConcurrentHashMap<String, String>();
            valueLabelAnswerMap.put(questionLabelValue.toString(), CommonUtils.checkDateFormatVerification(policyStartDate));
            checkAnswerMap.put(questionKey, valueLabelAnswerMap);
        }
    }

    private void insertSortCodeBankAcctValuesInMap(final StaticIDVDataDTO staticIDVDataDTO, final String sortCode,
            final String accountNumber, final ConcurrentHashMap<String, ConcurrentHashMap<String, String>> checkAnswerMap) {
        StringBuffer questionLabelValue = new StringBuffer();
        if (verificationClientHelper.checkNullConditionWhileInsertingValuesInMap(staticIDVDataDTO)) {
            LOGGER.debug("If condition satisfied for sortCode setting values in checkAnswerMap");
            final String questionKey = staticIDVDataDTO.getQuestionKey();
            questionLabelValue = questionLabelValue.append(staticIDVDataDTO.getQuestionLabel())
                    .append(MPLConstants.BACKLSLASH)
                    .append(staticIDVDataDTO.getQuestionValue());
            final ConcurrentHashMap<String, String> valueLabelAnswerMap = new ConcurrentHashMap<String, String>();
            valueLabelAnswerMap.put(questionLabelValue.toString(), sortCode + accountNumber);
            checkAnswerMap.put(questionKey, valueLabelAnswerMap);
            LOGGER.debug("Check Answer Map insertSortCodeBankAcctValuesInMap");
        }
    }

    private void insertLastPremiumPaidValuesInMap(final StaticIDVDataDTO staticIDVDataDTO, final String lastPremiumPaid,
            final ConcurrentHashMap<String, ConcurrentHashMap<String, String>> checkAnswerMap) {
        StringBuffer questionLabelValue = new StringBuffer();
        if (verificationClientHelper.checkNullConditionWhileInsertingValuesInMap(staticIDVDataDTO)) {
            LOGGER.debug("If condition satisfied for lastPremiumPaid setting values in checkAnswerMap");
            final String questionKey = staticIDVDataDTO.getQuestionKey();
            questionLabelValue = questionLabelValue.append(staticIDVDataDTO.getQuestionLabel())
                    .append(MPLConstants.BACKLSLASH)
                    .append(staticIDVDataDTO.getQuestionValue());
            final ConcurrentHashMap<String, String> valueLabelAnswerMap = new ConcurrentHashMap<String, String>();
            valueLabelAnswerMap.put(questionLabelValue.toString(), lastPremiumPaid);
            checkAnswerMap.put(questionKey, valueLabelAnswerMap);
            LOGGER.debug("Check Answer Map insertLastPremiumPaidValuesInMap ");
        }
    }

    private void insertBenefitNameValuesInMap(final StaticIDVDataDTO staticIDVDataDTO, final String netinitPremium,
            final String benefitName, final ConcurrentHashMap<String, ConcurrentHashMap<String, String>> checkAnswerMap) {
        if (verificationClientHelper.checkNullConditionWhileInsertingValuesInMap(staticIDVDataDTO)) {
            LOGGER.debug("If condition satisfied for benefitName setting values in checkAnswerMap");
            final String benefitNameQuestionValue = findBenefitNameQuestionValue(staticIDVDataDTO, benefitName);
            if (StringUtils.isNotBlank(benefitNameQuestionValue)) {
                final String questionKey = staticIDVDataDTO.getQuestionKey();
                StringBuffer questionLabelValue = new StringBuffer();
                questionLabelValue = questionLabelValue.append(staticIDVDataDTO.getQuestionLabel())
                        .append(MPLConstants.BACKLSLASH).append(benefitNameQuestionValue);
                final ConcurrentHashMap<String, String> valueLabelAnswerMap = new ConcurrentHashMap<String, String>();
                valueLabelAnswerMap.put(questionLabelValue.toString(), netinitPremium);
                checkAnswerMap.put(questionKey, valueLabelAnswerMap);
            }
        }
    }

    private String findBenefitNameQuestionValue(final StaticIDVDataDTO staticIDVDataDTO, final String benefitName) {
        String benefitNameQuestionValue = StringUtils.EMPTY;
        final String[] correctBenefitQuestion = staticIDVDataDTO.getQuestionValue().split(MPLConstants.ATTHERATE);
        if (MPLConstants.TRANSFERNONPROTECTED.equalsIgnoreCase(benefitName)
                || MPLConstants.TRANSFERPROTECTED.equalsIgnoreCase(benefitName)) {
            benefitNameQuestionValue = correctBenefitQuestion[0];
        } else if (MPLConstants.EESINV.equalsIgnoreCase(benefitName)) {
            benefitNameQuestionValue = correctBenefitQuestion[1];
        } else if (MPLConstants.ERSINV.equalsIgnoreCase(benefitName)) {
            benefitNameQuestionValue = correctBenefitQuestion[2];
        }
        return benefitNameQuestionValue;
    }

    private void insertNationalInsuranceNumberValuesInMap(final StaticIDVDataDTO staticIDVDataDTO, final String niNo,
            final ConcurrentHashMap<String, ConcurrentHashMap<String, String>> checkAnswerMap) {
        StringBuffer questionLabelValue = new StringBuffer();
        if (verificationClientHelper.checkNullConditionWhileInsertingValuesInMap(staticIDVDataDTO)) {
            LOGGER.debug("If condition satisfied for niNo setting values in checkAnswerMap");
            final String questionKey = staticIDVDataDTO.getQuestionKey();
            questionLabelValue = questionLabelValue.append(staticIDVDataDTO.getQuestionLabel())
                    .append(MPLConstants.BACKLSLASH)
                    .append(staticIDVDataDTO.getQuestionValue());
            final ConcurrentHashMap<String, String> valueLabelAnswerMap = new ConcurrentHashMap<String, String>();
            valueLabelAnswerMap.put(questionLabelValue.toString(), niNo);
            checkAnswerMap.put(questionKey, valueLabelAnswerMap);
        }
    }

    private void insertFundNameValuesInMap(final StaticIDVDataDTO staticIDVDataDTO, final String fundName,
            final ConcurrentHashMap<String, ConcurrentHashMap<String, String>> checkAnswerMap) {
        StringBuffer questionLabelValue = new StringBuffer();
        if (verificationClientHelper.checkNullConditionWhileInsertingValuesInMap(staticIDVDataDTO)) {
            LOGGER.debug("If condition satisfied for fundname setting values in checkAnswerMap");
            final String questionKey = staticIDVDataDTO.getQuestionKey();
            questionLabelValue = questionLabelValue.append(staticIDVDataDTO.getQuestionLabel())
                    .append(MPLConstants.BACKLSLASH)
                    .append(staticIDVDataDTO.getQuestionValue());
            final ConcurrentHashMap<String, String> valueLabelAnswerMap = new ConcurrentHashMap<String, String>();
            valueLabelAnswerMap.put(questionLabelValue.toString(), fundName);
            checkAnswerMap.put(questionKey, valueLabelAnswerMap);
        }
    }

    /**
     * Method for finding success result
     *
     * @param successFlag
     *            - boolean value
     * @param customerId
     * @return result
     * @throws Exception
     */
    public String successIdentifier(final int successFlag, final Long customerId, final String userName, final String xGUID)
            throws Exception {
        LOGGER.debug("Inside successIdentifier method {}", successFlag);
        String result = StringUtils.EMPTY;
        if (1 == successFlag) {
            LOGGER.info("If condition true is satisfied");
            // Initiate LDAP call to update the status of account to verified.
            mPLSecurityClient.setAccountVerified(userName, "Verified");
            MPLCustomer mPLCustomer = customerRepositoryImpl.findOne(customerId);
            mPLCustomer.setLastUpdatedBy("SYSTEM");
            mPLCustomer.setLastUpdatedOn(Timestamp.valueOf(LocalDateTime.now()));
            mPLCustomer.setAccountCreatedOn(Timestamp.valueOf(LocalDateTime.now()));
            customerRepositoryImpl.saveCustDetail(mPLCustomer);

            MPLCustomerUpdateStatus mplCustomerUpdateStatus;
            mplCustomerUpdateStatus = UpdateAttemptsMapper.mapCustUpdateStatus(mPLCustomer, null,
                    MPLServiceConstants.ALPHABET_U);

            customerUpdateStatusRepository.saveAndFlush(mplCustomerUpdateStatus);

            bancsUpdateService.sendPersonalDetailUpdate(mplCustomerUpdateStatus, MPLServiceConstants.USERID, xGUID);
            /*
             * bancsUpdateService.callBancsEmailPhoneUserID(
             * BancsUpdateRequestMapper.mapToBancsEmailPhoneUserIdRequest(
             * mPLCustomer, MPLServiceConstants.USERID),
             * mPLCustomer.getBancsCustNum(), false, "xGUID",
             * mPLCustomer.getCustomerId(),
             * mplCustomerUpdateStatus.getUpdateId());
             */

            result = MPLConstants.SUCCESS;
        }
        LOGGER.debug("Returning result {}", result);
        return result;
    }

    /**
     * removeFromAllMap to removeFromAllMap
     *
     * @param questionKey
     *            questionKey to store questionKey data
     * @param verificationMapAndLockedCountDTOResponse
     * @param sessionCheckAnswerMap
     *            sessionCheckAnswerMap
     * @param lockedCount
     *            lockedCount
     * @return count
     */
    public VerificationMapAndLockedCountDTO removeFromMap(final String questionKey,
            final CustomerActivationDTO customerActivationDTO,
            final VerificationMapAndLockedCountDTO verificationMapAndLockedCountDTOResponse) {
        Integer count = customerActivationDTO.getLockedCount();
        customerActivationDTO.getCheckAnswerMap().remove(questionKey);
        count++;
        verificationMapAndLockedCountDTOResponse.setCheckAnswerMap(customerActivationDTO.getCheckAnswerMap());
        verificationMapAndLockedCountDTOResponse.setLockedCount(count);
        return verificationMapAndLockedCountDTOResponse;
    }

    /**
     * Method to set VerificationReturnValuesDTO
     *
     * @param randomKey
     *            - random question key
     * @param randomVal
     *            - inner map
     * @param returnedEligibleQuestion
     *            - returnedEligibleQuestion
     */
    public void setReturnObjectValues(final String randomKey, final ConcurrentHashMap<String, String> randomVal,
            final VerificationReturnValuesDTO returnedEligibleQuestion) {
        if (MPLConstants.SORTCODEACCOUNTNUMBER.equalsIgnoreCase(randomKey)) {
            for (final Map.Entry<String, String> entry : randomVal.entrySet()) {
                final String questionLabel = entry.getKey();
                final String[] tokens = questionLabel.split(MPLConstants.BACKLSLASH);
                final String[] labelArray = tokens[0].split(MPLConstants.ATTHERATE);
                returnedEligibleQuestion.setQuestionLabel(labelArray[0]);
                returnedEligibleQuestion.setQuestionLabelForAccountNumber(labelArray[1]);
                returnedEligibleQuestion.setQuestion(tokens[1]);
            }
        } else {
            for (final Map.Entry<String, String> entry : randomVal.entrySet()) {
                final String questionLabel = entry.getKey();
                final String[] tokens = questionLabel.split(MPLConstants.BACKLSLASH);
                returnedEligibleQuestion.setQuestionLabel(tokens[0]);
                returnedEligibleQuestion.setQuestion(tokens[1]);
                returnedEligibleQuestion.setQuestionLabelForAccountNumber("");
            }
        }
    }

    /**
     * Method to find success flag for last premium paid
     *
     * @param verificationDTO
     *            - object containing string values
     * @param sessionCheckAnswerMap
     *            - map fetched from session
     * @param xGUID
     *            - xGUID
     * @return successFlag
     */
    public int findSuccessFlagForLastPrmPd(final VerificationDTO verificationDTO,
            final CustomerActivationDTO customerActivationDTO, final String xGUID) {
        int successFlag = 0;
        if (CommonUtils.isValidPremium(verificationDTO.getAnswer())) {
            final Double inputAnswer = Double.valueOf(verificationDTO.getAnswer());
            final Double storedAnswer = Double.valueOf(
                    customerActivationDTO.getCheckAnswerMap().get(verificationDTO.getQuestionKey())
                            .get(verificationDTO.getQuestionLabel() + MPLConstants.BACKLSLASH
                                    + verificationDTO.getQuestion()));
            if (inputAnswer >= storedAnswer - 1 && inputAnswer <= storedAnswer + 1) {
                LOGGER.info("{} ---------Correct answer-----------findSuccessFlagForLastPrmPd method");
                successFlag = 1;
            }
        }
        return successFlag;
    }

    /**
     * displayEligibleQuestion to displayEligibleQuestion
     *
     * @param sessionCheckAnswerMap
     *            sessionCheckAnswerMap
     * @param identifySource
     *            identifySource
     * @param questionKey
     *            questionKey
     * @param questionDTO
     *            questionDTO
     * @param xGUID
     *            - xGUID
     * @param policyNumber
     * @throws Exception
     *             exception
     * @return returnEligibleQuestion
     */
    public VerificationReturnValuesDTO displayEligibleQuestion(
            final CustomerActivationDTO customerActivationDTO,
            final int identifySource, final String questionKey, final int lockedQuestionKeySize,
            final String xGUID) throws Exception {
        LOGGER.debug(" {} inside displayEligibleQuestion method ");
        ConcurrentHashMap<String, String> randomVal = null;
        final VerificationReturnValuesDTO returnedEligibleQuestion = new VerificationReturnValuesDTO();
        String randomKey = null;
        int policyLockFlag = 0;
        if (identifySource == 0 && questionKey == null) {
            LOGGER.debug(" {} If condition satisfied for verification page load or next button ");
            final ArrayList<String> keys = new ArrayList<String>(customerActivationDTO.getCheckAnswerMap().keySet());
            randomKey = keys.get(0);
            randomVal = customerActivationDTO.getCheckAnswerMap().get(randomKey);
            returnedEligibleQuestion.setLockedListCount(lockedQuestionKeySize);
            returnedEligibleQuestion.setCount(customerActivationDTO.getCheckAnswerMap().size());
        } else if (identifySource == 1 && questionKey != null) {
            LOGGER.debug(" {} If condition satisfied for clicking here ");
            customerActivationDTO.getCheckAnswerMap().remove(questionKey);
            LOGGER.debug(" {} Session Check Answer Map after removing from map ");
            Integer sessionCount = lockedQuestionKeySize;
            sessionCount++;
            LOGGER.debug(" {} Setting count value in session {}", sessionCount);
            returnedEligibleQuestion.setLockedListCount(sessionCount);
            policyLockFlag = verificationQuestionHelper.updateVerificationAttemptsTable(customerActivationDTO, questionKey,
                    identifySource, xGUID);
            verificationQuestionHelper.lockAccountWithRespectToVerificationAttempt(customerActivationDTO.getUserName());
            randomKey = toAskQuestionKey(customerActivationDTO.getCheckAnswerMap());
            returnedEligibleQuestion.setCount(customerActivationDTO.getCheckAnswerMap().size());
            randomVal = customerActivationDTO.getCheckAnswerMap().get(randomKey);
        }
        returnedEligibleQuestion.setQuestionkey(randomKey);
        if (0 == policyLockFlag) {
            setReturnObjectValues(randomKey, randomVal, returnedEligibleQuestion);
            returnedEligibleQuestion.setExhaustMessage("");
        } else {
            returnedEligibleQuestion.setExhaustMessage("policyLocked");
            returnedEligibleQuestion.setQuestionkey("");
            returnedEligibleQuestion.setQuestionkey("");
        }

        returnedEligibleQuestion.setLockedMessage("");
        returnedEligibleQuestion.setCheckAnswerMap(customerActivationDTO.getCheckAnswerMap());
        LOGGER.debug(" {} Returning from returnedEligibleQuestion question ");
        return returnedEligibleQuestion;
    }

    /**
     * toAskQuestionKey to get question key
     *
     * @param mapToShuffle
     *            map in which shuffling is done
     * @return question key
     */
    public String toAskQuestionKey(final ConcurrentHashMap<String, ConcurrentHashMap<String, String>> mapToShuffle) {
        final ArrayList<String> keys = new ArrayList<String>(mapToShuffle.keySet());
        Collections.shuffle(keys);
        return keys.get(0);
    }
}
