/*
 * PolicyActivationServiceImpl.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.impl;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

import uk.co.phoenixlife.service.async.AccountActivationAsyncService;
import uk.co.phoenixlife.service.client.bancs.BancsServiceClient;
import uk.co.phoenixlife.service.client.security.MPLSecurityClient;
import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dao.entity.MPLAccountLockOut;
import uk.co.phoenixlife.service.dao.entity.MPLCustomer;
import uk.co.phoenixlife.service.dao.entity.MPLCustomerPolicy;
import uk.co.phoenixlife.service.dao.entity.MPLPolicyLockOut;
import uk.co.phoenixlife.service.dao.repository.CustomerAccountLockOutRepository;
import uk.co.phoenixlife.service.dao.repository.CustomerPolicyLockOutRepository;
import uk.co.phoenixlife.service.dao.repository.CustomerPolicyRepository;
import uk.co.phoenixlife.service.dao.repository.CustomerRepository;
import uk.co.phoenixlife.service.dao.repository.CustomerRepositoryImpl;
import uk.co.phoenixlife.service.dao.repository.VerificationAttemptsRepository;
import uk.co.phoenixlife.service.dto.ErrorListDTO;
import uk.co.phoenixlife.service.dto.MPLException;
import uk.co.phoenixlife.service.dto.MiErrorRequestDTO;
import uk.co.phoenixlife.service.dto.activation.CustomerActivationDTO;
import uk.co.phoenixlife.service.dto.activation.CustomerDetails;
import uk.co.phoenixlife.service.dto.activation.CustomerPersonalDetailStatus;
import uk.co.phoenixlife.service.dto.activation.CustomerPersonalDetailStatusResponse;
import uk.co.phoenixlife.service.dto.activation.PolicyExistLockStatus;
import uk.co.phoenixlife.service.dto.activation.VerificationCheckAnswerWrapperDTO;
import uk.co.phoenixlife.service.dto.activation.VerificationDTO;
import uk.co.phoenixlife.service.dto.activation.VerificationDisplayEligibleQuestionWrapperDTO;
import uk.co.phoenixlife.service.dto.activation.VerificationMapAndLockedCountDTO;
import uk.co.phoenixlife.service.dto.activation.VerificationReturnValuesDTO;
import uk.co.phoenixlife.service.dto.activation.VerificationSuccessDTO;
import uk.co.phoenixlife.service.dto.dashboard.PoliciesOwnedByAPartyDTO;
import uk.co.phoenixlife.service.dto.dashboard.PolicyDTO;
import uk.co.phoenixlife.service.dto.exception.PolicyNotFoundException;
import uk.co.phoenixlife.service.dto.policy.BenefitDTO;
import uk.co.phoenixlife.service.dto.policy.IdAndVDataDTO;
import uk.co.phoenixlife.service.dto.policy.PartyBasicDTO;
import uk.co.phoenixlife.service.dto.policy.PartyBasicOwnerDTO;
import uk.co.phoenixlife.service.dto.policy.PartyCommDetailsDTO;
import uk.co.phoenixlife.service.dto.policy.PartyPolicyDTO;
import uk.co.phoenixlife.service.dto.response.BancsIDAndVDataResponse;
import uk.co.phoenixlife.service.dto.response.BancsPartyBasicDetailResponse;
import uk.co.phoenixlife.service.dto.response.BancsPartyCommDetailsResponse;
import uk.co.phoenixlife.service.dto.response.BancsPolicyExistResponse;
import uk.co.phoenixlife.service.dto.response.PoliciesOwnedByAPartyResponse;
import uk.co.phoenixlife.service.dto.staticdata.StaticIDVDataDTO;
import uk.co.phoenixlife.service.helper.AccountActivationHelper;
import uk.co.phoenixlife.service.helper.EncashmentDashboardHelper;
import uk.co.phoenixlife.service.helper.VerificationClientHelper;
import uk.co.phoenixlife.service.helper.VerificationQuestionHelper;
import uk.co.phoenixlife.service.helper.VerificationQuestionServiceClientHelper;
import uk.co.phoenixlife.service.interfaces.AccountActivationService;
import uk.co.phoenixlife.service.interfaces.MIComplianceServiceInterface;
import uk.co.phoenixlife.service.mapper.MPLCustomerMapper;
import uk.co.phoenixlife.service.utils.CommonUtils;

/**
 * This class holds Business Logic for Policy Activation and relavent API Calls
 * PolicyActivationServiceImpl.java
 */
@Component
public class AccountActivationServiceImpl implements AccountActivationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountActivationServiceImpl.class);
    @Autowired
    private BancsServiceClient bancsServiceClient;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MPLSecurityClient mPLSecurityClient;

    @Autowired
    private VerificationQuestionHelper verificationQuestionHelper;

    @Autowired
    private MPLCustomerMapper mPLCustomerMapper;

    @Autowired
    private AccountActivationAsyncService bancsAsyncService;

    @Autowired
    private VerificationQuestionServiceClientHelper verificationQuestionServiceClientHelper;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerRepositoryImpl customerRepositoryImpl;

    @Autowired
    private CustomerPolicyRepository customerPolicyRepository;

    @Autowired
    private CustomerPolicyLockOutRepository customerPolicyLockOutRepository;

    @Autowired
    private CustomerAccountLockOutRepository customerAccountLockOutRepository;

    @Autowired
    private VerificationAttemptsRepository verificationAttemptsRepository;

    @Autowired
    private VerificationClientHelper verificationClientHelper;
    
    @Autowired
  	private MIComplianceServiceInterface miComplianceServiceInterface;
    

    @Value("${policylock.policyverification.limit}")
    private int policyVerificationThresholdLimit;

    @Value("${accountlock.policysearch.limit}")
    private int policySearchThresholdLimit;

    @Value("${accountlock.personaldetails.limit}")
    private int personalDetailsThresholdLimit;

    private static final int IDENTIFYSOURCE = 0;

    @Override
    @Transactional
    public PolicyExistLockStatus getPolicyExist(final CustomerActivationDTO customerActivationDTO, final String xGUID)
            throws Exception {

        LOGGER.debug("INSIDE THE AccountActivationServiceImpl getPolicyExists check");
        final String policyNumber = customerActivationDTO.getPolicyNumber();
        final String userName = customerActivationDTO.getUserName();
        PolicyExistLockStatus policyStatus = new PolicyExistLockStatus();
        policyStatus.setPolicyNumber(policyNumber);
        final Long malId = customerAccountLockOutRepository.getMalIdOfAccountLockOut(userName);
        LOGGER.debug("getPolicyExist malId is:: {}", malId);
        final MPLAccountLockOut savedResult;
        if (malId == null) {
            LOGGER.debug("getPolicyExist malId is null, inserting value in MPLAccountLockout table");
            savedResult = insertValuesInMPLAccountLockOutTable(userName);
        } else {
            LOGGER.debug("getPolicyExist malId is NOT null, fetching value from MPLAccountLockout table");
            savedResult = fetchValuesFromMPLAccountLockOutTable(malId);
        }
        LOGGER.debug("getPolicyExist checking policyLocked flag");
        Boolean policyLocked = checkPolicyStatus(policyNumber);
        LOGGER.debug("getPolicyExist policyLocked flag is:: {}", policyLocked);
        if (!policyLocked) {
            LOGGER.debug("getPolicyExist policy not locked intiating BancsExists call");
            policyStatus = checkPolicyExistenceInBancs(policyNumber, xGUID, savedResult, policyStatus, userName);
        } else {
            LOGGER.debug("Policy is locked setting locked description in PolicyExistLockStatus object");
            policyStatus.setFlag(0);
            policyStatus.setDescription("Policy is locked or encashed in database");
            sendPolicyLockedMsg();
        }
        return policyStatus;
    }

    private PolicyExistLockStatus checkPolicyExistenceInBancs(final String policyNumber, final String xGUID,
            final MPLAccountLockOut savedResult, PolicyExistLockStatus policyStatus, final String userName)
            throws Exception {

        BancsPolicyExistResponse bancsPolicyExistResp;
        try {
            bancsPolicyExistResp = bancsServiceClient.getPolicyExistsResponse(policyNumber, xGUID);
            if (CommonUtils.isObjectNotNull(bancsPolicyExistResp)) {
                switch (AccountActivationHelper.checkPolicy(bancsPolicyExistResp.getBancsPolicyExistsCheck(), xGUID,
                        policyNumber)) {
                case 1:
                    policyStatus = doPolicyValidOperation(policyStatus, policyNumber);
                    bancsAsyncService.initiateBancsCallForAccountActivation(policyNumber, xGUID);
                    bancsAsyncService.getIDAndVData(policyNumber, xGUID);
                    break;
                case 2:
                    policyStatus = doPolicyInValidOperation(policyStatus, policyNumber, savedResult);
                    if (policyStatus.getFlag() == 4) {
                        // uncomment this for production
                        mPLSecurityClient.lockAccount(userName, true);
                    }
                    break;
                default:
                    break;
                }
            } else {

            }
        } catch (PolicyNotFoundException excp) {

            LOGGER.error("PolicyNotFoundException : {}", excp);
            policyStatus = doPolicyNotFoundOperation(policyStatus, policyNumber, savedResult);
            if (policyStatus.getFlag() == 4) {
                mPLSecurityClient.lockAccount(userName, true);
            }
        }
        return policyStatus;
    }

    private PolicyExistLockStatus doPolicyNotFoundOperation(final PolicyExistLockStatus policyStatus,
            final String policyNumber, MPLAccountLockOut savedResult) {
        final Long mplId = customerPolicyLockOutRepository.getPolicyLockOutObject(policyNumber);
        MPLPolicyLockOut mPLPolicyLockOut = customerPolicyLockOutRepository.findOne(mplId);
        if (mPLPolicyLockOut.getPolicyVerificationCount() == policyVerificationThresholdLimit) {
            mPLPolicyLockOut = setLockDetailsForMPLPolicyLockOutTable(mPLPolicyLockOut);
            policyStatus.setFlag(0);
            policyStatus.setDescription("Policy does not exist in BANCS and threshold limit crossed policy locked.");
            sendPolicyLockedMsg();
        } else {
            mPLPolicyLockOut = setIncrementDetailsForMPLPolicyLockOutTable(mPLPolicyLockOut);
            policyStatus.setFlag(3);
            policyStatus.setDescription("Policy does not exist in BANCS");
            sendPolicyNotFoundMsg();
        }
        if (savedResult.getPolicySearchCount() == policySearchThresholdLimit) {
            savedResult = setLockDetailsForMPLAccountLockOutTable(savedResult);
            savedResult.setPolicySearchCount(savedResult.getPolicySearchCount() + 1);
            policyStatus.setFlag(4);
            policyStatus.setDescription("Threshold limit for policy search reached account locked");
            sendAccountLockedMsg();
        } else {
            savedResult = setIncrementDetailsForMPLAccountlockOuttable(savedResult);
            savedResult.setPolicySearchCount(savedResult.getPolicySearchCount() + 1);
            if (mPLPolicyLockOut.getPolicyVerificationCount() != policyVerificationThresholdLimit + 1) {
                policyStatus.setFlag(3);
                policyStatus.setDescription("Policy does not exist in BANCS");
            }
        }
        customerPolicyLockOutRepository.saveAndFlush(mPLPolicyLockOut);
        LOGGER.debug("doPolicyNotFoundOperation Saved data in POlicyLockOut Table");
        customerAccountLockOutRepository.saveAndFlush(savedResult);
        LOGGER.debug("doPolicyNotFoundOperation Saved data in AccountLockOut Table");
        return policyStatus;
    }

    private PolicyExistLockStatus doPolicyValidOperation(final PolicyExistLockStatus policyStatus,
            final String policyNumber) {
        final Long mplId = customerPolicyLockOutRepository.getPolicyLockOutObject(policyNumber);
        MPLPolicyLockOut mPLPolicyLockOut = customerPolicyLockOutRepository.findOne(mplId);
        if (mPLPolicyLockOut.getPolicyVerificationCount() == policyVerificationThresholdLimit) {
            mPLPolicyLockOut = setLockDetailsForMPLPolicyLockOutTable(mPLPolicyLockOut);
            policyStatus.setFlag(0);
            policyStatus.setDescription("Policy Exist in BaNCS but threshold limit crossed");
            sendPolicyLockedMsg();
        } else {
            mPLPolicyLockOut = setIncrementDetailsForMPLPolicyLockOutTable(mPLPolicyLockOut);
            policyStatus.setFlag(1);
            policyStatus.setDescription("Policy Exist in BaNCS and is VALID");
        }
        customerPolicyLockOutRepository.saveAndFlush(mPLPolicyLockOut);
        LOGGER.debug("doPolicyValidOperation Saved data in POlicyLockOut Table");
        return policyStatus;
    }

    private PolicyExistLockStatus doPolicyInValidOperation(final PolicyExistLockStatus policyStatus,
            final String policyNumber, MPLAccountLockOut savedResult) {
        final Long mplId = customerPolicyLockOutRepository.getPolicyLockOutObject(policyNumber);
        MPLPolicyLockOut mPLPolicyLockOut = customerPolicyLockOutRepository.findOne(mplId);
        if (mPLPolicyLockOut.getPolicyVerificationCount() == policyVerificationThresholdLimit) {
            mPLPolicyLockOut = setLockDetailsForMPLPolicyLockOutTable(mPLPolicyLockOut);
            policyStatus.setFlag(0);
            LOGGER.debug(
                    "doPolicyInValidOperation Policy Exist in BaNCS but is Invalid and threshhold limit exceeded policy locked");
            policyStatus.setDescription("Policy Exist in BaNCS but is Invalid and threshhold limit exceeded policy locked.");
            sendPolicyLockedMsg();
        } else {
            mPLPolicyLockOut = setIncrementDetailsForMPLPolicyLockOutTable(mPLPolicyLockOut);
            policyStatus.setFlag(2);
            LOGGER.debug("doPolicyInValidOperation Policy Exist in BaNCS but is Invalid");
            policyStatus.setDescription("Policy Exist in BaNCS but is Invalid");
        }
        if (savedResult.getPolicySearchCount() == policySearchThresholdLimit) {
            savedResult = setLockDetailsForMPLAccountLockOutTable(savedResult);
            savedResult.setPolicySearchCount(savedResult.getPolicySearchCount() + 1);
            policyStatus.setFlag(4);
            LOGGER.debug("doPolicyInValidOperation Threshold limit for policy search reached account locked");
            policyStatus.setDescription("Threshold limit for policy search reached account locked");
            sendAccountLockedMsg();
        } else {
            savedResult = setIncrementDetailsForMPLAccountlockOuttable(savedResult);
            savedResult.setPolicySearchCount(savedResult.getPolicySearchCount() + 1);
            if (mPLPolicyLockOut.getPolicyVerificationCount() != policyVerificationThresholdLimit + 1) {
                policyStatus.setFlag(2);
                LOGGER.debug("Policy Exist in BaNCS but is Invalid");
                policyStatus.setDescription("Policy Exist in BaNCS but is Invalid");
            }
            LOGGER.debug("doPolicyInValidOperation Else Operation successfull");
        }
        customerPolicyLockOutRepository.saveAndFlush(mPLPolicyLockOut);
        LOGGER.debug("doPolicyInValidOperation Saved data in POlicyLockOut Table");
        customerAccountLockOutRepository.saveAndFlush(savedResult);
        LOGGER.debug("doPolicyInValidOperation Saved data in POlicyLockOut Table");
        return policyStatus;
    }

    private MPLAccountLockOut setIncrementDetailsForMPLAccountlockOuttable(final MPLAccountLockOut savedResult) {
        savedResult.setLastUpdatedOn(Timestamp.valueOf(LocalDateTime.now()));
        savedResult.setLastUpdatedBy("SYSTEM");
        return savedResult;
    }

    private MPLAccountLockOut setLockDetailsForMPLAccountLockOutTable(final MPLAccountLockOut savedResult) {
        savedResult.setAccountLockStatus('Y');
        savedResult.setLastUpdatedBy("SYSTEM");
        savedResult.setLastUpdatedOn(Timestamp.valueOf(LocalDateTime.now()));
        return savedResult;
    }

    private MPLPolicyLockOut setIncrementDetailsForMPLPolicyLockOutTable(final MPLPolicyLockOut mPLPolicyLockOut) {
        mPLPolicyLockOut.setPolicyVerificationCount(mPLPolicyLockOut.getPolicyVerificationCount() + 1);
        mPLPolicyLockOut.setLastUpdatedOn(Timestamp.valueOf(LocalDateTime.now()));
        mPLPolicyLockOut.setLastUpdatedBy("SYSTEM");
        return mPLPolicyLockOut;
    }

    private MPLPolicyLockOut setLockDetailsForMPLPolicyLockOutTable(final MPLPolicyLockOut mPLPolicyLockOut) {
        mPLPolicyLockOut.setPolicyLockStatus('Y');
        mPLPolicyLockOut.setLastUpdatedBy("SYSTEM");
        mPLPolicyLockOut.setPolicyVerificationCount(mPLPolicyLockOut.getPolicyVerificationCount() + 1);
        mPLPolicyLockOut.setLastUpdatedOn(Timestamp.valueOf(LocalDateTime.now()));
        return mPLPolicyLockOut;
    }

    private MPLAccountLockOut fetchValuesFromMPLAccountLockOutTable(final Long malId) {
        return customerAccountLockOutRepository.findOne(malId);
    }

    private MPLAccountLockOut insertValuesInMPLAccountLockOutTable(final String userName) {
        MPLAccountLockOut mPLAccountLockOut = new MPLAccountLockOut();
        mPLAccountLockOut.setUserName(userName);
        mPLAccountLockOut.setCreatedOn(Timestamp.valueOf(LocalDateTime.now()));
        mPLAccountLockOut.setAccountLockStatus('N');
        return customerAccountLockOutRepository.saveAndFlush(mPLAccountLockOut);
    }

    private Boolean checkPolicyStatus(final String policyNumber) throws Exception {

        LOGGER.debug("inside checkPolicyStatus method ");
        Boolean policyLocked = false;
        Character status = customerPolicyLockOutRepository.policyLockedStatus(policyNumber);
        LOGGER.debug("result --->> {}", status);
        if (status == null) {
            LOGGER.debug("Policy doesnot exist in DB");
            MPLPolicyLockOut mPLPolicyLockOut = new MPLPolicyLockOut();
            mPLPolicyLockOut.setLegacyPolicynumber(policyNumber);
            mPLPolicyLockOut.setPolicyLockStatus('N');
            mPLPolicyLockOut.setPolicyVerificationCount(0);
            mPLPolicyLockOut.setCreatedOn(Timestamp.valueOf(LocalDateTime.now()));
            customerPolicyLockOutRepository.saveAndFlush(mPLPolicyLockOut);
        } else if (status == 'Y') {
            LOGGER.debug("Policy is locked");
            policyLocked = true;
        }
        LOGGER.info("checkPolicyStatus method succesfull locked response is: {}", policyLocked);
        return policyLocked;
    }

    @Override
    public CustomerPersonalDetailStatusResponse validatePersonalDetail(final CustomerActivationDTO customerActivationDTO,
            final String xGUID) throws Exception {
        LOGGER.debug("Reached AccountActivationServiceImpl to validate Personal Details");
        final String policyNumber = customerActivationDTO.getPolicyNumber();
        CustomerPersonalDetailStatusResponse customerPersonalDetailStatusResponse = new CustomerPersonalDetailStatusResponse();
        final String response = bancsServiceClient.getPartyBasicDetailResponse(policyNumber, xGUID);
        List<CustomerPersonalDetailStatus> validationDetails = null;
        int errorCode = 1;
        if (CommonUtils.isobjectNotNull(response)) {
            if (response.contains(MPLServiceConstants.ERRORSTRING)) {
                LOGGER.error("AccountActivationServiceImpl PartyBasicDetailResponse failed {}", response);
                throw new MPLException();
            } else {
                LOGGER.debug("AccountActivationServiceImpl PartyBasicDetailReponse received");
                final BancsPartyBasicDetailResponse bancsPartyBasicDetailResp = objectMapper.readValue(response,
                        BancsPartyBasicDetailResponse.class);
                final PartyBasicDTO partyBasic = bancsPartyBasicDetailResp.getBancsPartyBasicDetail();
                validationDetails = AccountActivationHelper.checkPersonalDetails(partyBasic,
                        customerActivationDTO);
                if (CommonUtils.isValidList(validationDetails)) {
                    for (final CustomerPersonalDetailStatus validationDetail : validationDetails) {
                        LOGGER.debug("Initiating PartyCommDetails for {}", validationDetail.getBancsUniqueCustNo());
                        errorCode = 1;
                        bancsAsyncService.getPartyCommDetails(validationDetail.getBancsUniqueCustNo(), xGUID);
                    }
                } else {
                    LOGGER.debug("Personal Details Not Matched");
                    errorCode = doPersonalDetailsNotFoundOperation(customerActivationDTO);
                    if (errorCode == 2) {
                        // uncomment this for production
                        mPLSecurityClient.lockAccount(customerActivationDTO.getUserName(), true);
                    }
                }

            }
        }
        LOGGER.debug("Returning from validatePersonalDetail");
        customerPersonalDetailStatusResponse.setCustomerPersonalDetailStatus(validationDetails);
        customerPersonalDetailStatusResponse.setErrorCode(errorCode);

        return customerPersonalDetailStatusResponse;
    }

    @Transactional
    private int doPersonalDetailsNotFoundOperation(final CustomerActivationDTO customerActivationDTO) {
        int errorCode = 0;
        final Long mplId = customerPolicyLockOutRepository.getPolicyLockOutObject(customerActivationDTO.getPolicyNumber());
        MPLPolicyLockOut mPLPolicyLockOut = customerPolicyLockOutRepository.findOne(mplId);
        if (mPLPolicyLockOut.getPolicyVerificationCount() == policyVerificationThresholdLimit) {
            mPLPolicyLockOut = setLockDetailsForMPLPolicyLockOutTable(mPLPolicyLockOut);
            errorCode = 3;
        } else {
            mPLPolicyLockOut = setIncrementDetailsForMPLPolicyLockOutTable(mPLPolicyLockOut);
        }
        final Long malId = customerAccountLockOutRepository.getMalIdOfAccountLockOut(customerActivationDTO.getUserName());
        MPLAccountLockOut savedResult = customerAccountLockOutRepository.findOne(malId);
        if (savedResult.getPersonalFailCount() == personalDetailsThresholdLimit) {
            savedResult = setLockDetailsForMPLAccountLockOutTable(savedResult);
            savedResult.setPersonalFailCount(savedResult.getPersonalFailCount() + 1);
            errorCode = 2;
        } else {
            savedResult = setIncrementDetailsForMPLAccountlockOuttable(savedResult);
            savedResult.setPersonalFailCount(savedResult.getPersonalFailCount() + 1);
        }
        customerPolicyLockOutRepository.saveAndFlush(mPLPolicyLockOut);
        LOGGER.debug("doPersonalDetailsNotFoundOperation Saved data in PolicyLockOut Table");
        customerAccountLockOutRepository.saveAndFlush(savedResult);
        LOGGER.debug("doPersonalDetailsNotFoundOperation Saved data in CustomerAccountLockOut Table");
        return errorCode;

    }

    @Override
    public CustomerDetails validatePostCodeGoneAwayAndLifeStatusFlag(final CustomerDetails customerDetails,
            final String xGUID)
            throws Exception {

        Boolean postCodeFlag = false;
        final List<CustomerPersonalDetailStatus> customerPersonalDetailStatus = customerDetails
                .getCustomerPersonalDetailStatusList();
        for (final CustomerPersonalDetailStatus details : customerPersonalDetailStatus) {
            final String response = bancsServiceClient.getPartyCommDetailsResponse(details.getBancsUniqueCustNo(), xGUID);
            final String partyBasicPartyNoResponse = bancsServiceClient
                    .getPartyBasicDetailByPartyNoResponse(details.getBancsUniqueCustNo(), xGUID);
            if (response.contains(MPLServiceConstants.ERRORSTRING)
                    || partyBasicPartyNoResponse.contains(MPLServiceConstants.ERRORSTRING)
                    || EncashmentDashboardHelper.partyCommFlagLifeStatusCheck(response, partyBasicPartyNoResponse,
                            xGUID) == null) {
                throw new MPLException();
            } else {
                final BancsPartyCommDetailsResponse bancsPartyCommDetailsResponse = objectMapper.readValue(response,
                        BancsPartyCommDetailsResponse.class);
                final PartyCommDetailsDTO partyCommDetailsDTO = bancsPartyCommDetailsResponse.getBancsPartyCommDetails();
                final Boolean validPostCode = validatePostCode(customerDetails.getPostCode(), partyCommDetailsDTO);

                if (validPostCode
                        && details.getMultipleOwnerFlag()) {
                    throw new MPLException();
                } else if (validPostCode
                        && !details.getMultipleOwnerFlag()) {
                    LOGGER.debug(
                            "----------------------------" + details.getMyPhoenixUserName() + "------------------------");
                    if (CommonUtils.isobjectNull(details.getMyPhoenixUserName())
                            || CommonUtils.isEmpty(details.getMyPhoenixUserName())) {
                        postCodeFlag = true;
                        customerDetails.setUserNameAlreadyExistFlag(false);
                        customerDetails.setLegacyCustNo(details.getLegacyCustNo());
                        fetchCustomerAndPolicyId(customerDetails, details);
                        bancsAsyncService.getPoliciesOwnedByParty(details.getBancsUniqueCustNo(), xGUID);
                        LOGGER.debug("validation for PostCode succesfull");
                        break;
                    } else {
                        LOGGER.debug("Account already created");
                        customerDetails.setUserNameAlreadyExistFlag(true);
                        break;
                    }
                } else {
                    customerDetails.setUserNameAlreadyExistFlag(false);
                }
            }
        }
        customerDetails.setPostCodeFlag(postCodeFlag);
        return customerDetails;
    }

    private Boolean validatePostCode(final String postCode, final PartyCommDetailsDTO partyCommDetailsDTO) {
        Boolean validPostCode = false;
        // for (final AddressDTO address : partyCommDetailsDTO.getAddressList())
        // {
        if (partyCommDetailsDTO.getPostCode().replaceAll("\\s", "").equalsIgnoreCase(postCode)) {
            validPostCode = true;
        }
        // }

        return validPostCode;

    }

    @Transactional
    private void fetchCustomerAndPolicyId(final CustomerDetails customerDetails,
            final CustomerPersonalDetailStatus details) {
        Long policyId = null;
        final Long customerId = customerRepository.fetchCustomerId(details.getBancsUniqueCustNo());
        if (customerId != null) {
            MPLCustomerPolicy mPLCustomerPolicy = customerPolicyRepository.getPolicies(customerId,
                    customerDetails.getPolicyNumber());
            if (mPLCustomerPolicy != null) {
                policyId = mPLCustomerPolicy.getPolicyId();
            }
        }
        customerDetails.setIdentifiedBancsUniqueCustomerNumber(details.getBancsUniqueCustNo());
        customerDetails.setCustomerId(customerId);
        customerDetails.setPolicyId(policyId);
    }

    @Override
    public VerificationReturnValuesDTO getQuestionAnswer(final CustomerActivationDTO customerActivationDTO,
            final String xGUID)
            throws Exception {

        LOGGER.debug("Inside getQuestionAnswer");
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);

        VerificationReturnValuesDTO eligibleQuestion = new VerificationReturnValuesDTO();
        final List<StaticIDVDataDTO> staticDataList = customerActivationDTO.getStaticDataList();
        String response = bancsServiceClient.getIDAndVDataResponse(customerActivationDTO.getPolicyNumber(), xGUID);
        PoliciesOwnedByAPartyResponse policiesOwnedByAPartyResponse = getPolicyOwnedByPartyResponse(
                customerActivationDTO.getIdentifiedBancsUniqueCustomerNumber(), xGUID);
        PoliciesOwnedByAPartyDTO policiesOwnedByAPartyDTO = policiesOwnedByAPartyResponse.getBancsPoliciesOwnedByAParty();
        List<PolicyDTO> policyDTOList = policiesOwnedByAPartyDTO.getListOfPolicies();
        List<String> bancsPolNumList = findBancsPolicyNumber(policyDTOList, customerActivationDTO.getPolicyNumber());
        final String response2 = bancsServiceClient
                .getPartyBasicDetailResponse(customerActivationDTO.getPolicyNumber(), xGUID);
        if (response.contains(MPLServiceConstants.ERRORSTRING) || response2.contains(MPLServiceConstants.ERRORSTRING)) {
            throw new MPLException();
        } else {
            BancsIDAndVDataResponse bancsIDAndVDataResponse = objectMapper.readValue(response,
                    BancsIDAndVDataResponse.class);
            IdAndVDataDTO idAndVDataDTO = bancsIDAndVDataResponse.getBancsIDAndVData();
            BancsPartyBasicDetailResponse bancsPartyBasicDetailResponse = objectMapper.readValue(response2,
                    BancsPartyBasicDetailResponse.class);
            PartyBasicDTO partyBasicDTO = bancsPartyBasicDetailResponse.getBancsPartyBasicDetail();
            final PartyBasicOwnerDTO partyBasicOwnerDTO = verificationClientHelper.findPartyBasicOwner(partyBasicDTO, xGUID);
            final List<PartyPolicyDTO> partyPolicyDTOList = verificationClientHelper
                    .findPartyPolicyDTOList(idAndVDataDTO, xGUID);
            String niNo = findNiNo(partyBasicOwnerDTO);
            LOGGER.debug("Calling findPartyPolicyDTOValue {} ", niNo);
            final List<PartyPolicyDTO> partyPolicyDTOListInternal = verificationClientHelper.findPartyPolicyDTOValue(
                    partyPolicyDTOList,
                    bancsPolNumList);
            LOGGER.debug("Returned from findPartyPolicyDTOValue list size is {} ", partyPolicyDTOListInternal.size());

            if (partyPolicyDTOListInternal.size() == 1) {
                LOGGER.debug("checking partyPolicyInternal size is one", partyPolicyDTOListInternal.size());
                PartyPolicyDTO partyPolicyDTO = partyPolicyDTOListInternal.get(0);
                LOGGER.debug("IS partyPolicyDTO null {} ", partyPolicyDTO);

                String fundName = verificationClientHelper.findFundName(partyPolicyDTO, xGUID);
                List<String> lockedQuestionKeylist = new ArrayList<String>();
                if (customerActivationDTO.getPolicyId() != null) {
                    lockedQuestionKeylist = verificationAttemptsRepository
                            .findlockedQuestionKey(customerActivationDTO.getPolicyId());
                }
                final ConcurrentHashMap<String, ConcurrentHashMap<String, String>> checkAnswerMap = processFindBenefitNetInit(
                        partyPolicyDTO, fundName, niNo, lockedQuestionKeylist, xGUID, staticDataList);
                if (checkAnswerMap.size() > 0) {
                    customerActivationDTO.setCheckAnswerMap(checkAnswerMap);
                    eligibleQuestion = verificationQuestionServiceClientHelper.displayEligibleQuestion(customerActivationDTO,
                            IDENTIFYSOURCE, null, lockedQuestionKeylist.size(), xGUID);
                    LOGGER.debug(" getQuestionAnswer is successfully completed, returning eligible question");
                }
            } else {
                LOGGER.error("Size is more than 2, Multiple bancsPolNum found in IDV for Policy Number:: {}",
                        customerActivationDTO.getPolicyNumber());
                throw new MPLException();
            }

        }
        return eligibleQuestion;
    }

    private PoliciesOwnedByAPartyResponse getPolicyOwnedByPartyResponse(final String identifiedBancsUniqueCustomerNumber,
            final String xGUID) throws Exception {
        PoliciesOwnedByAPartyResponse policiesOwnedByAPartyResponse;
        String response = bancsServiceClient.policiesOwnedByParty(identifiedBancsUniqueCustomerNumber, xGUID);
        if (response.contains(MPLServiceConstants.ERRORSTRING)) {
            LOGGER.error("ERROR Throwing MPLException from getPolicyOwnedByPartyResponse");
            throw new MPLException();
        } else {
            policiesOwnedByAPartyResponse = objectMapper.readValue(response, PoliciesOwnedByAPartyResponse.class);
        }
        LOGGER.debug("returning from getPolicyOwnedByPartyResponse");
        return policiesOwnedByAPartyResponse;
    }

    private ConcurrentHashMap<String, ConcurrentHashMap<String, String>> processFindBenefitNetInit(
            final PartyPolicyDTO partyPolicyDTO, final String fundName, final String niNo,
            final List<String> lockedQuestionKeylist, final String xGUID, final List<StaticIDVDataDTO> staticDataList)
            throws Exception {
        LOGGER.debug("{} inside processFindBenefitNetInit method");
        String benefitName = StringUtils.EMPTY;
        String netinitPremium = StringUtils.EMPTY;
        final List<String> benefitNetInit = findBenefitNetInit(partyPolicyDTO);
        LOGGER.debug("{} Response recieved benefitNetInit");
        if (CommonUtils.isValidList(benefitNetInit)) {
            benefitName = benefitNetInit.get(0);
            netinitPremium = benefitNetInit.get(1);
        }
        LOGGER.debug("{} Response recieved lockedQuestionKeylist");
        final VerificationDTO verificationDTO = new VerificationDTO();
        verificationDTO.setBenefitName(benefitName);
        verificationDTO.setFundName(fundName);
        verificationDTO.setNiNo(niNo);
        verificationDTO.setNetinitPremium(netinitPremium);
        LOGGER.debug("Successfully returning from processFindBenefitNetInit method ");
        return verificationQuestionServiceClientHelper.createFinalMapObject(verificationDTO, staticDataList,
                lockedQuestionKeylist, partyPolicyDTO);
    }

    private List<String> findBenefitNetInit(final PartyPolicyDTO partyPolicyDTO) {
        final List<String> benefitNetInit = new ArrayList<String>();
        if (CommonUtils.isValidList(partyPolicyDTO.getListOfBenefits())) {
            for (final BenefitDTO benefitDTO : partyPolicyDTO.getListOfBenefits()) {
                if (benefitDTO.getBenefitName() != null
                        && benefitDTO.getNetInitPrem() != null && checkConditionForBenefitName(benefitDTO)) {
                    final String benefitName = benefitDTO.getBenefitName();
                    final String netinitPremium = benefitDTO.getNetInitPrem().toString();
                    benefitNetInit.add(benefitName);
                    benefitNetInit.add(netinitPremium);
                    break;
                }
            }
        }
        return benefitNetInit;
    }

    private Boolean checkConditionForBenefitName(final BenefitDTO benefitDTO) {
        return "TRI_PR".equalsIgnoreCase(benefitDTO.getBenefitName())
                || "TRI_NPR".equalsIgnoreCase(benefitDTO.getBenefitName())
                || "EME_SINV".equalsIgnoreCase(benefitDTO.getBenefitName())
                || "EMR_SINV".equalsIgnoreCase(benefitDTO.getBenefitName());
    }

    private String findNiNo(final PartyBasicOwnerDTO partyBasicOwnerDTO) {
        String niNo = StringUtils.EMPTY;
        if (null != partyBasicOwnerDTO.getNiNo()) {
            niNo = partyBasicOwnerDTO.getNiNo();
        }
        return niNo;
    }

    private List<String> findBancsPolicyNumber(final List<PolicyDTO> policyDTOList, final String legacyPolicyNumber) {
        List<String> bancsPolNumList = new ArrayList<String>();
        if (CommonUtils.isValidList(policyDTOList)) {
            for (final PolicyDTO policyDTO : policyDTOList) {
                if (StringUtils.isNotBlank(legacyPolicyNumber) && StringUtils.isNotBlank(policyDTO.getBancsPolNum())
                        && legacyPolicyNumber.equalsIgnoreCase(policyDTO.getPolNum())) {
                    bancsPolNumList.add(policyDTO.getBancsPolNum());
                    // break;
                } else {
                    LOGGER.info("Policy Number didnt fetched bancsPolNum");
                }
            }
        }
        return bancsPolNumList;
    }

    @Override
    public VerificationMapAndLockedCountDTO checkAnswer(
            final VerificationCheckAnswerWrapperDTO verificationCheckAnswerWrapperDTO,
            final String xGUID) throws Exception {

        LOGGER.debug("{} inside checkAnswer method");
        return transactionStart(verificationCheckAnswerWrapperDTO, xGUID);
    }

    @Transactional
    private VerificationMapAndLockedCountDTO transactionStart(
            final VerificationCheckAnswerWrapperDTO verificationCheckAnswerWrapperDTO,
            final String xGUID) throws Exception {

        LOGGER.debug("{} inside transactionStart method");
        String result = StringUtils.EMPTY;
        VerificationMapAndLockedCountDTO verificationMapAndLockedCountDTOResponse = new VerificationMapAndLockedCountDTO();
        VerificationDTO verificationDTO = verificationCheckAnswerWrapperDTO.getVerificationDTO();
        CustomerActivationDTO customerActivationDTO = verificationCheckAnswerWrapperDTO.getCustomerActivationDTO();
        VerificationSuccessDTO successDTO = null;
        switch (verificationDTO.getQuestionKey()) {
        case "lastPremPaid":
            LOGGER.debug("{} Switch condtion satisfied for LASTPREMIUMPAID");
            successDTO = verificationQuestionHelper.checkLastPremiumPaidAnswer(verificationDTO, customerActivationDTO,
                    xGUID, verificationMapAndLockedCountDTOResponse);
            result = verificationQuestionServiceClientHelper.successIdentifier(successDTO.getSuccessFlag(),
                    customerActivationDTO.getCustomerId(), customerActivationDTO.getUserName(), xGUID);
            break;
        case "benefitName":
            LOGGER.debug("{} Switch condtion satisfied for BENEFITNAME");
            successDTO = verificationQuestionHelper.checkBenefitNameAnswer(verificationDTO, customerActivationDTO, xGUID,
                    verificationMapAndLockedCountDTOResponse);
            result = verificationQuestionServiceClientHelper.successIdentifier(successDTO.getSuccessFlag(),
                    customerActivationDTO.getCustomerId(), customerActivationDTO.getUserName(), xGUID);
            break;
        case "fundName":
            LOGGER.debug("{} Switch condtion satisfied for FUNDNAME");
            successDTO = verificationQuestionHelper.checkFundNameAnswer(verificationDTO, customerActivationDTO, xGUID,
                    verificationMapAndLockedCountDTOResponse);
            result = verificationQuestionServiceClientHelper.successIdentifier(successDTO.getSuccessFlag(),
                    customerActivationDTO.getCustomerId(), customerActivationDTO.getUserName(), xGUID);
            break;
        case "polStDate":
            LOGGER.debug("{} Switch condtion satisfied for POLICYSTARTDATE");
            successDTO = verificationQuestionHelper.checkPolicyStartDateAnswer(verificationDTO, customerActivationDTO,
                    xGUID, verificationMapAndLockedCountDTOResponse);
            result = verificationQuestionServiceClientHelper.successIdentifier(successDTO.getSuccessFlag(),
                    customerActivationDTO.getCustomerId(), customerActivationDTO.getUserName(), xGUID);
            break;
        default:
            LOGGER.debug("{} Switch condtion satisfied for default");
            successDTO = verificationQuestionHelper.checkOtherAnswer(verificationDTO, customerActivationDTO, xGUID,
                    verificationMapAndLockedCountDTOResponse);
            result = verificationQuestionServiceClientHelper.successIdentifier(successDTO.getSuccessFlag(),
                    customerActivationDTO.getCustomerId(), customerActivationDTO.getUserName(), xGUID);
            break;
        }
        LOGGER.info("returning from checkAnswer for key {}", verificationDTO.getQuestionKey());
        if (MPLConstants.SUCCESS.equals(result)) {
            LOGGER.info("Account Succesfully Activated");
            verificationMapAndLockedCountDTOResponse.setAccountActivated(true);
        }
        String response = verificationQuestionHelper.setResult(successDTO.getSuccessFlag(), result, customerActivationDTO,
                successDTO.getLockedListCount(), xGUID);
        verificationMapAndLockedCountDTOResponse.setCheckAnswerResponse(response);
        verificationMapAndLockedCountDTOResponse.setCustomerId(successDTO.getCustomerId());
        verificationMapAndLockedCountDTOResponse.setPolicyId(successDTO.getPolicyId());
        LOGGER.info("Returning from AccountActivationServiceImpl");
        return verificationMapAndLockedCountDTOResponse;
    }

    @Override
    @Transactional
    public VerificationReturnValuesDTO getNextQuestionDetails(
            final VerificationDisplayEligibleQuestionWrapperDTO verificationDisplayEligibleQuestionWrapperDTO,
            final String xGUID) throws Exception {
        VerificationReturnValuesDTO eligibleQuestion = new VerificationReturnValuesDTO();
        if (verificationDisplayEligibleQuestionWrapperDTO.getCustomerActivationDTO().getCustomerId() == null) {
            CustomerActivationDTO customerActivationDTO = insertCustomerTableAndPolicyTableValues(
                    verificationDisplayEligibleQuestionWrapperDTO.getCustomerActivationDTO(),
                    xGUID);
            verificationDisplayEligibleQuestionWrapperDTO.getCustomerActivationDTO()
                    .setPolicyId(customerActivationDTO.getPolicyId());
            eligibleQuestion.setCustomerId(customerActivationDTO.getCustomerId());
            eligibleQuestion.setPolicyId(customerActivationDTO.getPolicyId());
        }
        eligibleQuestion = verificationQuestionServiceClientHelper.displayEligibleQuestion(
                verificationDisplayEligibleQuestionWrapperDTO.getCustomerActivationDTO(),
                verificationDisplayEligibleQuestionWrapperDTO.getIdentifySource(),
                verificationDisplayEligibleQuestionWrapperDTO.getQuestionKey(),
                verificationDisplayEligibleQuestionWrapperDTO.getCustomerActivationDTO().getLockedCount(), xGUID);
        if (verificationDisplayEligibleQuestionWrapperDTO.getCustomerActivationDTO().getCustomerId() != null) {
            eligibleQuestion.setCustomerId(
                    verificationDisplayEligibleQuestionWrapperDTO.getCustomerActivationDTO().getCustomerId());
            eligibleQuestion
                    .setPolicyId(verificationDisplayEligibleQuestionWrapperDTO.getCustomerActivationDTO().getPolicyId());
        }
        return eligibleQuestion;
    }

    @Override
    public CustomerActivationDTO getCustomerAndPolicyId(CustomerActivationDTO customerActivationDTO, final String xGUID)
            throws Exception {

        customerActivationDTO = insertCustomerTableAndPolicyTableValues(customerActivationDTO,
                xGUID);
        LOGGER.debug("returning from getCustomerAndPolicyID method");
        return customerActivationDTO;
    }

    @Override
    public Boolean checkUsernameExistForDifferentUniqueCustomerNo(final CustomerActivationDTO customerActivationDTO,
            final String xGUID) {
        LOGGER.debug("Checking UserName Existence");
        Boolean flag = false;
        Long customerId = customerRepository
                .fetchCustomerId(customerActivationDTO.getIdentifiedBancsUniqueCustomerNumber());
        if (null != customerId) {
            MPLCustomer mPLCustomer = customerRepositoryImpl.findOne(customerId);
            if (mPLCustomer.getAccountCreatedOn() != null) {
                flag = true;
            }
        }
        LOGGER.debug("Returnin result of UserName exist check {}", flag);
        return flag;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private CustomerActivationDTO
            insertCustomerTableAndPolicyTableValues(final CustomerActivationDTO customerActivationDTO, final String xGUID)
                    throws Exception {
        LOGGER.debug("inside insertCustomerTableAndPolicyTableValues");
        final PoliciesOwnedByAPartyResponse policiesOwnedByAPartyResponse = getPolicyOwnedByPartyResponse(
                customerActivationDTO.getIdentifiedBancsUniqueCustomerNumber(), xGUID);

        List<PolicyDTO> identifiedPolicyDTOList = new ArrayList();
        for (PolicyDTO policyDTO : policiesOwnedByAPartyResponse.getBancsPoliciesOwnedByAParty().getListOfPolicies()) {
            if (customerActivationDTO.getPolicyNumber().equalsIgnoreCase(policyDTO.getPolNum())) {
                identifiedPolicyDTOList.add(policyDTO);
                break;
            }
        }
        if (customerActivationDTO.getCustomerId() != null) {
            LOGGER.debug("insertCustomerTableAndPolicyTableValues Finding Customer from DB for CustomerID {}",
                    customerActivationDTO.getCustomerId());
            MPLCustomer mPLCustomer = customerRepositoryImpl.findOne(customerActivationDTO.getCustomerId());
            if (customerActivationDTO.getPolicyId() == null) {
                Set<MPLCustomerPolicy> customerPolicies;
                customerPolicies = new HashSet<MPLCustomerPolicy>();
                identifiedPolicyDTOList
                        .forEach(pol -> customerPolicies.add(mPLCustomerMapper.mapCustomerPolicy(mPLCustomer, pol)));
                final MPLCustomer updatedMplCustomer = mPLCustomerMapper.mapUpatedCustomer(mPLCustomer,
                        customerActivationDTO);
                updatedMplCustomer.setCustomerPolicies(customerPolicies);
                MPLCustomer savedMPLCustomer = customerRepositoryImpl.saveCustDetail(updatedMplCustomer);
                LOGGER.debug("insertCustomerTableAndPolicyTableValues Customer Details updated in DB ");
                Set<MPLCustomerPolicy> customerPolicy = savedMPLCustomer.getCustomerPolicies();
                customerActivationDTO.setPolicyId(customerPolicy.iterator().next().getPolicyId());
            } else {
                final MPLCustomer updatedMplCustomer = mPLCustomerMapper.mapUpatedCustomer(mPLCustomer,
                        customerActivationDTO);
                customerRepositoryImpl.saveCustDetail(updatedMplCustomer);
            }

        } else {
            LOGGER.debug("insertCustomerTableAndPolicyTableValues CustomerID not present ");
            final MPLCustomer mPLCustomer = mPLCustomerMapper.mapCustomer(customerActivationDTO, identifiedPolicyDTOList);
            final MPLCustomer savedResultMPLCustomer = customerRepositoryImpl.saveCustDetail(mPLCustomer);
            LOGGER.debug("insertCustomerTableAndPolicyTableValues Customer Details saved in DB ");
            customerActivationDTO.setCustomerId(savedResultMPLCustomer.getCustomerId());
            Set<MPLCustomerPolicy> customerPolicy = savedResultMPLCustomer.getCustomerPolicies();
            LOGGER.debug("insertCustomerTableAndPolicyTableValues Policy Details saved in DB ");
            customerActivationDTO.setPolicyId(customerPolicy.iterator().next().getPolicyId());
        }
        LOGGER.debug("returning from insertCustomerTableAndPolicyTableValues");
        return customerActivationDTO;
    }
    
    private void sendAccountLockedMsg()
    {
    	MiErrorRequestDTO miErrorRequestDTO = new MiErrorRequestDTO();
    	ErrorListDTO  errorListDTO = new ErrorListDTO();
    	errorListDTO.setPageId("PG107");
    	errorListDTO.setNextPageId("PG108");
    	errorListDTO.setMessageCount(1);
    	errorListDTO.setTimeSpent(1);
    	errorListDTO.setMessageId("E024");
    	List<ErrorListDTO> errorDTOList = new ArrayList<ErrorListDTO>();
    	errorDTOList.add(errorListDTO);
    	miErrorRequestDTO.setErrorListDTO(errorDTOList);
    	miComplianceServiceInterface.submitErrorList(miErrorRequestDTO);
    }
    
    private void sendPolicyLockedMsg()
    {
    	MiErrorRequestDTO miErrorRequestDTO = new MiErrorRequestDTO();
    	ErrorListDTO  errorListDTO = new ErrorListDTO();
    	errorListDTO.setPageId("PG107");
    	errorListDTO.setNextPageId("PG108");
    	errorListDTO.setMessageCount(1);
    	errorListDTO.setTimeSpent(1);
    	errorListDTO.setMessageId("E022");
    	List<ErrorListDTO> errorDTOList = new ArrayList<ErrorListDTO>();
    	errorDTOList.add(errorListDTO);
    	miErrorRequestDTO.setErrorListDTO(errorDTOList);
    	miComplianceServiceInterface.submitErrorList(miErrorRequestDTO);
    }
    
    
    private void sendPolicyNotFoundMsg()
    {
    	MiErrorRequestDTO miErrorRequestDTO = new MiErrorRequestDTO();
    	ErrorListDTO  errorListDTO = new ErrorListDTO();
    	errorListDTO.setPageId("PG107");
    	errorListDTO.setNextPageId("PG108");
    	errorListDTO.setMessageCount(1);
    	errorListDTO.setTimeSpent(1);
    	errorListDTO.setMessageId("E021");
    	List<ErrorListDTO> errorDTOList = new ArrayList<ErrorListDTO>();
    	errorDTOList.add(errorListDTO);
    	miErrorRequestDTO.setErrorListDTO(errorDTOList);
    	miComplianceServiceInterface.submitErrorList(miErrorRequestDTO);
    
    }
    
}
