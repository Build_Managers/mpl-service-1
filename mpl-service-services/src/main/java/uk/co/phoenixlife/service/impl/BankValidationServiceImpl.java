package uk.co.phoenixlife.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import uk.co.phoenixlife.service.client.vocalink.VocalinkClient;
import uk.co.phoenixlife.service.dto.bankvalidation.BankValidationRequest;
import uk.co.phoenixlife.service.dto.bankvalidation.BankValidationResponse;
import uk.co.phoenixlife.service.dto.bankvalidation.BankValidationService;

@Component
public class BankValidationServiceImpl implements BankValidationService{

	  @Autowired
	  private VocalinkClient vocalinkClient;
	  
	  private static final Logger LOGGER = LoggerFactory.getLogger(BankValidationServiceImpl.class);

	  public BankValidationResponse validateBankDetails(final BankValidationRequest vocalinkRequest, final String xGUID)
				throws Exception {
			LOGGER.debug("Inside BankValidationRestController validateBankDetails");
	        return vocalinkClient.validateBankDetails(vocalinkRequest,xGUID);
	   }
  }
