/*
 * DashboardServiceImpl.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.impl;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import uk.co.phoenixlife.service.async.DashboardAsyncService;
import uk.co.phoenixlife.service.client.bancs.BancsServiceClient;
import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dao.entity.MPLCustomer;
import uk.co.phoenixlife.service.dao.repository.CustomerPolicyRepository;
import uk.co.phoenixlife.service.dao.repository.CustomerRepositoryImpl;
import uk.co.phoenixlife.service.dto.MPLException;
import uk.co.phoenixlife.service.dto.activation.CustomerIdentifierDTO;
import uk.co.phoenixlife.service.dto.activation.CustomerPolicyDetail;
import uk.co.phoenixlife.service.dto.bancserror.ErrorResponseDTO;
import uk.co.phoenixlife.service.dto.bancspost.BancsCalcCashInTaxOutput;
import uk.co.phoenixlife.service.dto.bancspost.BancsCalcCashInTaxResponse;
import uk.co.phoenixlife.service.dto.dashboard.DataQualityWarning;
import uk.co.phoenixlife.service.dto.dashboard.PartyBasicDetailByPartyNoDTO;
import uk.co.phoenixlife.service.dto.dashboard.PolicyDTO;
import uk.co.phoenixlife.service.dto.dashboard.Policy_Detail;
import uk.co.phoenixlife.service.dto.dashboard.PremiumSummariesResponse;
import uk.co.phoenixlife.service.dto.dashboard.Valuation;
import uk.co.phoenixlife.service.dto.dashboard.Valuations;
import uk.co.phoenixlife.service.dto.dashboard.ValuationsResponse;
import uk.co.phoenixlife.service.dto.policy.CashInTypeDTO;
import uk.co.phoenixlife.service.dto.policy.DashBoardResponse;
import uk.co.phoenixlife.service.dto.policy.FinalDashboardDTO;
import uk.co.phoenixlife.service.dto.response.BancsPartyCommDetailsResponse;
import uk.co.phoenixlife.service.dto.response.PartyBasicDetailByPartyNoResponse;
import uk.co.phoenixlife.service.dto.response.PoliciesOwnedByAPartyResponse;
import uk.co.phoenixlife.service.dto.response.PoliciesOwnedByAPartyResponseWrapper;
import uk.co.phoenixlife.service.dto.response.PolicyDetailResponse;
import uk.co.phoenixlife.service.dto.staticdata.ProductMatrixDTO;
import uk.co.phoenixlife.service.helper.EncashmentDashboardHelper;
import uk.co.phoenixlife.service.interfaces.DashboardService;
import uk.co.phoenixlife.service.mapper.BancsRequestMapper;
import uk.co.phoenixlife.service.utils.CommonUtils;

/**
 * DashboardServiceImpl.java
 */
@Component
public class DashboardServiceImpl implements DashboardService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DashboardServiceImpl.class);

    @Autowired
    private BancsServiceClient bancsServiceClient;

    @Autowired
    private DashboardAsyncService dashboardAsyncService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CustomerRepositoryImpl customerRepositoryImpl;

    @Autowired
    private CustomerPolicyRepository customerPolicyRepository;

    @Override
    public PoliciesOwnedByAPartyResponseWrapper policiesOwnedByParty(final String uniqueCustomerNumber, final String xGUID)
            throws Exception {
        List<PolicyDTO> listOfPolicies = new ArrayList<PolicyDTO>();
        List<String> xGUIDBancsPolNumCacheKeyList = new ArrayList<String>();
        PoliciesOwnedByAPartyResponse policiesOwnedByAPartyResponse = null;
        String response = bancsServiceClient.policiesOwnedByParty(uniqueCustomerNumber, xGUID);
        if (response.contains(MPLServiceConstants.ERRORSTRING)) {
            throw new MPLException();
        } else {
            policiesOwnedByAPartyResponse = objectMapper.readValue(response, PoliciesOwnedByAPartyResponse.class);
        }
        if (policiesOwnedByAPartyResponse != null) {
            ArrayList<String> listOfFlag = (ArrayList<String>) CommonUtils.fetchInActiveFlagList();
            for (PolicyDTO policyDTO : policiesOwnedByAPartyResponse.getBancsPoliciesOwnedByAParty()
                    .getListOfPolicies()) {
                if (listOfFlag.contains(policyDTO.getPolStatusCd())) {
                    listOfPolicies.add(policyDTO);
                    String xGUIDBancsPolNumCacheKey = xGUID.concat(policyDTO.getBancsPolNum());
                    xGUIDBancsPolNumCacheKeyList.add(xGUIDBancsPolNumCacheKey);
                    dashboardAsyncService.getPolDetails(policyDTO.getBancsPolNum(), xGUID, xGUIDBancsPolNumCacheKey);
                    dashboardAsyncService.getClaimList(policyDTO.getBancsPolNum(), xGUID, xGUIDBancsPolNumCacheKey);
                    dashboardAsyncService.getRetQuote(policyDTO.getBancsPolNum(), uniqueCustomerNumber, xGUID,
                            xGUIDBancsPolNumCacheKey);
                }
            }
            policiesOwnedByAPartyResponse.getBancsPoliciesOwnedByAParty().setListOfPolicies(listOfPolicies);

            if (policiesOwnedByAPartyResponse.getBancsPoliciesOwnedByAParty().getListOfPolicies().size() > 0) {
                dashboardAsyncService.getIDAndVData(
                        policiesOwnedByAPartyResponse.getBancsPoliciesOwnedByAParty().getListOfPolicies().get(0)
                                .getPolNum(),
                        xGUID);
            }
        }
        final PoliciesOwnedByAPartyResponseWrapper policiesOwnedByAPartyResponseWrapper = new PoliciesOwnedByAPartyResponseWrapper();
        policiesOwnedByAPartyResponseWrapper.setPoliciesOwnedByAPartyResponse(policiesOwnedByAPartyResponse);
        policiesOwnedByAPartyResponseWrapper.setxGUIDBancsPolNumCacheKeyList(xGUIDBancsPolNumCacheKeyList);
        return policiesOwnedByAPartyResponseWrapper;
    }

    @Override
    public CustomerPolicyDetail getpolicyDetailValuationPremiumSum(final String policyNumber, final String xGUID) {
        // TODO Auto-generated method stub
        CustomerPolicyDetail customerPolicyDetail = new CustomerPolicyDetail();
        PolicyDetailResponse policiesDetailsResponse;
        ValuationsResponse valuationsResponse;
        PremiumSummariesResponse premiumSummariesResponse;
        ProductMatrixDTO productMatrix;
        policiesDetailsResponse = bancsServiceClient.policyDetail(policyNumber, xGUID);

        if (CommonUtils.isObjectNotNull(policiesDetailsResponse)) {
            Policy_Detail policyDetail = policiesDetailsResponse.getPolicy_Detail();
            customerPolicyDetail.setPolicyDetail(policyDetail);
            productMatrix = getProductMatrixData(policyDetail, xGUID, customerPolicyDetail);
            if (checkProductMatrixPremium(policyDetail, xGUID, productMatrix)) {
                LOGGER.debug("Business Validation failed, Hence cannot call PremiumSummaries API for xGUID -> {}");
            } else {
                premiumSummariesResponse = bancsServiceClient.premiumSummaries(policyNumber, xGUID);
                if (CommonUtils.isObjectNotNull(premiumSummariesResponse)) {
                    customerPolicyDetail.setPremiumSummaries(premiumSummariesResponse.getPremium_Summaries());
                }
            }
            if (checkDQW(policyDetail.getDataQualityWarning(), xGUID) || checkProductMatrixValuation(productMatrix, xGUID)) {
                LOGGER.debug("Business Validation failed, Hence cannot call Valuation API for xGUID -> {}");
            } else {
                valuationsResponse = bancsServiceClient.valuations(policyNumber, xGUID);
                if (CommonUtils.isObjectNotNull(valuationsResponse)) {
                    Valuations valuation = valuationsResponse.getValuations();
                    List<Valuation> valuationList = valuation.getValuation();
                    if (customerPolicyDetail.getCleanPolicy()) {
                        for (final Valuation val : valuationList) {
                            switch (val.getValuationType()) {
                            case "Transfer":
                                if (productMatrix.getDisplayTransferValueFlag() == 'Y') {
                                    val.setDisplay(true);
                                } else {
                                    val.setDisplay(false);
                                }
                                break;
                            case "Retirement":
                                if (productMatrix.getDisplayFundValueFlag() == 'Y') {
                                    val.setDisplay(true);
                                } else {
                                    val.setDisplay(false);
                                }
                                break;
                            case "Death":
                                if (productMatrix.getDisplayDeathValueFlag() == 'Y') {
                                    val.setDisplay(true);
                                } else {
                                    val.setDisplay(false);
                                }
                                break;
                            }
                        }
                    }
                    customerPolicyDetail.setValuations(valuation);
                }
            }
            if (checkPenRevStatus(policyDetail.getPenRevStatusLit(), xGUID)) {
                customerPolicyDetail.setDisplayBenefitGuarantee("True");
            } else {
                customerPolicyDetail.setDisplayBenefitGuarantee("False");
            }

        }
        return customerPolicyDetail;
    }

    private boolean checkPenRevStatus(final String penRevStatusLit, final String xGUID) {
        boolean result = false;
        if (MPLServiceConstants.NOTSTARTEDLITERAL.equalsIgnoreCase(penRevStatusLit)
                || MPLServiceConstants.PENDINGLITERAL.equalsIgnoreCase(penRevStatusLit)) {
            LOGGER.debug(
                    "Benefit Guarantee cannot be displayed as it has penRevStatus as Not Started/ Pending for xGUID {}");
            result = true;
        }

        return result;

    }

    private boolean checkDQW(final List<DataQualityWarning> dataQualityWarnings, final String xGUID) {
        boolean result = false;
        for (final DataQualityWarning dataQualityWarning : dataQualityWarnings) {
            if (MPLServiceConstants.MLETTER_CAPS.equalsIgnoreCase(dataQualityWarning.getDataQualityWarning())
                    || MPLServiceConstants.TLETTER_CAPS.equalsIgnoreCase(dataQualityWarning.getDataQualityWarning())) {
                LOGGER.debug("Policy contains M/T Flag for xGUID -> {}");
                result = true;
            }
        }
        return result;
    }

    private boolean checkDQWForRFlag(final List<DataQualityWarning> dataQualityWarnings, final String xGUID) {
        boolean result = false;
        for (final DataQualityWarning dataQualityWarning : dataQualityWarnings) {
            if (MPLServiceConstants.RLETTER_CAPS.equalsIgnoreCase(dataQualityWarning.getDataQualityWarning())) {
                LOGGER.debug("Policy contains R Flag for xGUID -> {}");
                result = true;
            }
        }
        return result;
    }

    private boolean checkProductMatrixValuation(final ProductMatrixDTO productMatrix, final String xGUID) {
        boolean result = false;
        if (productMatrix.getDisplayFundValueFlag() == MPLServiceConstants.NCHARACTERCAPS
                && productMatrix.getDisplayDeathValueFlag() == MPLServiceConstants.NCHARACTERCAPS
                && productMatrix.getDisplayTransferValueFlag() == MPLServiceConstants.NCHARACTERCAPS) {
            result = true;
        }
        return result;
    }

    private boolean checkProductMatrixPremium(final Policy_Detail policyDetail, final String xGUID,
            final ProductMatrixDTO productMatrix) {
        boolean result = false;
        if (productMatrix.getDisplayCurrentPremiumFlag() == MPLServiceConstants.NCHARACTERCAPS) {
            result = true;
        }
        return result;
    }

    private ProductMatrixDTO getProductMatrixData(final Policy_Detail policyDetail, final String xGUID,
            final CustomerPolicyDetail customerPolicy) {
        ProductMatrixDTO productMatrixdata = null;
        String prodName = policyDetail.getProductName();
        char polFlag;
        if (checkDQW(policyDetail.getDataQualityWarning(), xGUID)
                || checkDQWForRFlag(policyDetail.getDataQualityWarning(), xGUID)) {
            polFlag = 'M';
            customerPolicy.setCleanPolicy(false);
        } else {
            polFlag = ' ';
            customerPolicy.setCleanPolicy(true);
        }
        // productMatrixdata =
        // productMatrixRepo.getProductMatricDetail(prodName, polFlag);
        return productMatrixdata;
    }

    @Async
    @Override
    public BancsCalcCashInTaxOutput getBancsCalcCashInTaxOutput(final CashInTypeDTO cashInTypeDTO,
            final String xGUID) throws Exception {

        return bancsCalcCashInTaxAPI(cashInTypeDTO.getBancsUniqueCustNo(), cashInTypeDTO.getBancsPolNum(), xGUID);

    }

    @Override
    public BancsCalcCashInTaxOutput getSyncBancsCalcCashInTaxOutput(final CashInTypeDTO cashInTypeDTO,
            final String xGUID) throws Exception {

        return bancsCalcCashInTaxAPI(cashInTypeDTO.getBancsUniqueCustNo(), cashInTypeDTO.getBancsPolNum(), xGUID);

    }

    private BancsCalcCashInTaxOutput bancsCalcCashInTaxAPI(final String bancsCustNumber, final String bancsPolicyNumber,
            final String xGUID) throws Exception {
        String bancsResponse;
        BancsCalcCashInTaxResponse bancsCalcCashInTaxResponse;

        bancsResponse = bancsServiceClient.bancsCalcCashInTaxAPI(bancsPolicyNumber,
                BancsRequestMapper.getDummyAnnuityQuote(bancsCustNumber), xGUID);

        if (bancsResponse.contains(MPLServiceConstants.ERRORSTRING)) {
            throw new MPLException();

        } else {
            bancsCalcCashInTaxResponse = objectMapper.readValue(bancsResponse, BancsCalcCashInTaxResponse.class);
        }
        return bancsCalcCashInTaxResponse.getBancsCalcCashInTax();
    }

    @Override
    public FinalDashboardDTO getEncashmentDashboard(final CustomerIdentifierDTO customerData, final String xGUID)
            throws Exception {
        return null;
    }

    @Override
    public DashBoardResponse getUniqueCustomerNo(final String userName, final String xGUID) throws Exception {

        final MPLCustomer mPLCustomer = customerRepositoryImpl.fetchCustomer(userName);
        DashBoardResponse dashBoardResponse = new DashBoardResponse();
        if (CommonUtils.isObjectNotNull(mPLCustomer)) {
            String response;
            PartyBasicDetailByPartyNoResponse partyBasicDetailByPartyNoResponse = null;
            response = bancsServiceClient.getPartyBasicDetailByPartyNoResponse(mPLCustomer.getBancsCustNum(),
                    xGUID);
            final String partyCommDetailsResponse = bancsServiceClient
                    .getPartyCommDetailsResponse(mPLCustomer.getBancsCustNum(), xGUID);
            if (response.contains(MPLServiceConstants.ERRORSTRING)
                    || partyCommDetailsResponse.contains(MPLServiceConstants.ERRORSTRING)
                    || EncashmentDashboardHelper.partyCommFlagLifeStatusCheck(partyCommDetailsResponse, response,
                            xGUID) == null) {
                throw new MPLException();
            } else {
                partyBasicDetailByPartyNoResponse = objectMapper.readValue(response,
                        PartyBasicDetailByPartyNoResponse.class);
            }
            final PartyBasicDetailByPartyNoDTO partyBasicDetailByPartyNoDTO = partyBasicDetailByPartyNoResponse
                    .getPartyBasicDetailByPartyNo();
            checkCustomerDetailsInDB(partyBasicDetailByPartyNoDTO, mPLCustomer);
            dashBoardResponse.setBancsUniqueCustomerNo(mPLCustomer.getBancsCustNum());
            dashBoardResponse.setCustomerId(mPLCustomer.getCustomerId());
            dashBoardResponse.setFirstName(new String(partyBasicDetailByPartyNoDTO.getForename()));
            dashBoardResponse.setLastName(new String(partyBasicDetailByPartyNoDTO.getSurname()));
            dashBoardResponse.setTitle(new String(partyBasicDetailByPartyNoDTO.getTitle()));
            dashBoardResponse.setPrimaryPhoneNumber(new String(mPLCustomer.getPrimaryPhNo()));
            dashBoardResponse.setEmail(new String(mPLCustomer.getEmailAddr()));
        } else {
            dashBoardResponse.setUserDoesNotExist(true);
        }

        return dashBoardResponse;
    }

    @Override
    public BancsPartyCommDetailsResponse getPartyCommDetailsResponse(final String bancsUniqueCustNo, final String xGUID)
            throws Exception {
        // TODO Auto-generated method stub
        String partyCommDetailsResponse;
        BancsPartyCommDetailsResponse partyCommResponse = null;

        partyCommDetailsResponse = bancsServiceClient.getPartyCommDetailsResponse(bancsUniqueCustNo, xGUID);
        if (CommonUtils.stringNotBlank(partyCommDetailsResponse)) {
            if (partyCommDetailsResponse.contains(MPLConstants.ERROR_DETAIL)) {
                final ErrorResponseDTO error = objectMapper.readValue(partyCommDetailsResponse, ErrorResponseDTO.class);

                LOGGER.error("BancsPartyCommDetailsResponse received ERROR Response {} from BaNCS for xGUID :: {}", error);
                throw new MPLException();
            } else {
                partyCommResponse = objectMapper.readValue(partyCommDetailsResponse,
                        BancsPartyCommDetailsResponse.class);
            }
        } else {
            LOGGER.error("IDAndVData is blank for xGUID :: {}");
            throw new MPLException();

        }
        return partyCommResponse;
    }

    private void checkCustomerDetailsInDB(final PartyBasicDetailByPartyNoDTO partyBasicDetailByPartyNoDTO,
            final MPLCustomer customerData) throws Exception {
        if (!partyBasicDetailByPartyNoDTO.getTitle().equalsIgnoreCase(new String(customerData.getTitle()))
                || !partyBasicDetailByPartyNoDTO.getForename().equalsIgnoreCase(new String(customerData.getFirstName()))
                || !partyBasicDetailByPartyNoDTO.getSurname().equalsIgnoreCase(new String(customerData.getLastName()))) {
            MPLCustomer mPLCustomer = customerRepositoryImpl.fetchCustomer(customerData.getUserName());
            mPLCustomer.setTitle(partyBasicDetailByPartyNoDTO.getTitle().getBytes());
            mPLCustomer.setFirstName(partyBasicDetailByPartyNoDTO.getForename().getBytes());
            mPLCustomer.setLastName(partyBasicDetailByPartyNoDTO.getSurname().getBytes());
            mPLCustomer.setLastUpdatedOn(Timestamp.valueOf(LocalDateTime.now()));
            mPLCustomer.setLastUpdatedBy("SYSTEM");
            customerRepositoryImpl.saveCustDetail(mPLCustomer);
        }
    }
}
