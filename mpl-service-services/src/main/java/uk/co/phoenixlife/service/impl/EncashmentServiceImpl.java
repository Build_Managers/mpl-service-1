/*
 * EncashmentServiceImpl.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import freemarker.template.Configuration;
import uk.co.phoenixlife.service.async.BancsUpdateService;
import uk.co.phoenixlife.service.client.bancs.BancsServiceClient;
import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dao.entity.MPLCustomer;
import uk.co.phoenixlife.service.dao.entity.MPLCustomerPolicy;
import uk.co.phoenixlife.service.dao.entity.MPLCustomerUpdateStatus;
import uk.co.phoenixlife.service.dao.entity.MPLDocumentAudit;
import uk.co.phoenixlife.service.dao.entity.MPLPolicyEncashment;
import uk.co.phoenixlife.service.dao.repository.CustomerRepositoryImpl;
import uk.co.phoenixlife.service.dao.repository.CustomerUpdateStatusRepository;
import uk.co.phoenixlife.service.dao.repository.DocumentAuditRepository;
import uk.co.phoenixlife.service.dao.repository.EmailStatusRepository;
import uk.co.phoenixlife.service.dao.repository.PolicyEncashmentRepository;
import uk.co.phoenixlife.service.dao.repository.QuoteDetailsRepositoryImpl;
import uk.co.phoenixlife.service.dto.MPLException;
import uk.co.phoenixlife.service.dto.activation.CustomerIdentifierDTO;
import uk.co.phoenixlife.service.dto.bancspost.BancsUpdateAttemptDTO;
import uk.co.phoenixlife.service.dto.dashboard.PoliciesOwnedByAPartyDTO;
import uk.co.phoenixlife.service.dto.dashboard.PolicyDTO;
import uk.co.phoenixlife.service.dto.dashboard.SyncDashboardServiceDTO;
import uk.co.phoenixlife.service.dto.pdf.FormRequestDTO;
import uk.co.phoenixlife.service.dto.policy.BancsRetQuoteValDTO;
import uk.co.phoenixlife.service.dto.policy.ClaimListDTO;
import uk.co.phoenixlife.service.dto.policy.CustomerDTO;
import uk.co.phoenixlife.service.dto.policy.FinalDashboardDTO;
import uk.co.phoenixlife.service.dto.policy.IdAndVDataDTO;
import uk.co.phoenixlife.service.dto.policy.PartyBasicOwnerDTO;
import uk.co.phoenixlife.service.dto.policy.PartyCommDetailsDTO;
import uk.co.phoenixlife.service.dto.policy.PolDetailsDTO;
import uk.co.phoenixlife.service.dto.policy.QuoteDetails;
import uk.co.phoenixlife.service.helper.EncashmentDashboardHelper;
import uk.co.phoenixlife.service.interfaces.EncashmentService;
import uk.co.phoenixlife.service.mapper.BancsUpdateRequestMapper;
import uk.co.phoenixlife.service.mapper.EmailMapper;
import uk.co.phoenixlife.service.mapper.PdfMapper;
import uk.co.phoenixlife.service.mapper.UpdateAttemptsMapper;
import uk.co.phoenixlife.service.utils.CommonUtils;
import uk.co.phoenixlife.service.utils.DateUtils;

/**
 * EncashmentServiceImpl.java
 */
@Component
public class EncashmentServiceImpl implements EncashmentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EncashmentServiceImpl.class);

    @Autowired
    private BancsServiceClient bancsServiceClient;

    @Autowired
    private CustomerRepositoryImpl customerRepository;
    @Autowired
    private QuoteDetailsRepositoryImpl quoteRepository;

    @Autowired
    private DocumentAuditRepository docRepository;
    @Autowired
    private PolicyEncashmentRepository encashmentRepo;
    @Autowired
    private CustomerUpdateStatusRepository custUpdtRepository;

    @Autowired
    private BancsUpdateService bancsUpdateService;

    @Autowired
    private Configuration freeMarkerConfiguration;
    @Autowired
    private ServletContext context;

    @Autowired
    private EmailStatusRepository emailStatusRepository;

    @Autowired
    private EncashmentDashboardHelper encashmentDashboardHelper;

    @Override
    @Cacheable(value = "finalDashboardCache", key = "#xGUID")
    public FinalDashboardDTO getEncashmentDashboard(final String bancsUniqueCustNo, final CustomerIdentifierDTO customerData,
            final String xGUID)
            throws Exception {
        FinalDashboardDTO finalDashboard = null;
        SyncDashboardServiceDTO matchingPolicyData = new SyncDashboardServiceDTO();
        final List<PolDetailsDTO> listOfPolDetails = new ArrayList<>();
        final List<ClaimListDTO> listOfClaimList = new ArrayList<>();
        final List<BancsRetQuoteValDTO> listOfRetQuoteVal = new ArrayList<>();
        // getting PartyBasicOwnerDTO
        String partyBasicResponse = bancsServiceClient.getPartyBasicDetailResponse(customerData.getPolNum(), xGUID);
        final PartyBasicOwnerDTO partyBasicOwner = EncashmentDashboardHelper.getPartyBasicOwner(
                customerData.getBancsUniqueCustNo(),
                partyBasicResponse, xGUID);
        // PartyBasicDetailByPartyNoResponse partyBasicDetailByPartyNoResponse =
        // null;
        /*
         * String response; response =
         * bancsServiceClient.getPartyBasicDetailByPartyNoResponse(customerData.
         * getBancsUniqueCustNo(), xGUID); if
         * (response.contains(MPLServiceConstants.ERRORSTRING)) { throw new
         * MPLException(); } else { partyBasicDetailByPartyNoResponse =
         * objectMapper.readValue(response,
         * PartyBasicDetailByPartyNoResponse.class); }
         */
        boolean validAge = checkCustomerValidAge(partyBasicOwner, customerData);
        // boolean validAge =
        // checkCustomerValidAge(partyBasicDetailByPartyNoResponse,
        // customerData);
        LOGGER.debug("DOB Check result {}", validAge);
        if (validAge) {
            // checkCustomerDetailsInDB(partyBasicOwner, customerData);
            Set<String> retQuoteValBancsPolNumSet;
            retQuoteValBancsPolNumSet = new HashSet<String>();
            Set<String> polDetailsBancsPolNumSet;
            polDetailsBancsPolNumSet = new HashSet<String>();
            Set<String> claimListBancsPolNumSet;
            claimListBancsPolNumSet = new HashSet<String>();
            // Getting IDAndVData
            final IdAndVDataDTO iDAndVDataDTO = getIDAndVData(customerData.getPolNum(), xGUID);
            // Getting PartyCommDetails after business validation
            final PartyCommDetailsDTO partyCommDetails = getPartyCommDetails(customerData.getBancsUniqueCustNo(), xGUID);
            // Getting PoliciesOwnedByAParty
            final PoliciesOwnedByAPartyDTO policiesOwnedByAParty = getPoliciesOwnedByAParty(
                    customerData.getBancsUniqueCustNo(),
                    xGUID);
            final List<PolicyDTO> policyDTOList = policiesOwnedByAParty.getListOfPolicies();
            final ArrayList<String> listOfFlag = (ArrayList<String>) CommonUtils.fetchInActiveFlagList();
            for (final PolicyDTO policyDTO : policyDTOList) {
                if (listOfFlag.contains(policyDTO.getPolStatusCd())) {
                    // Getting PolDetails ClaimList & RetQuoteVal Data after
                    // Business Validation
                    String xGUIDBancsPolNumCacheKey = xGUID.concat(policyDTO.getBancsPolNum());
                    final PolDetailsDTO polDetails = getPolDetails(policyDTO.getBancsPolNum(), xGUID,
                            xGUIDBancsPolNumCacheKey);
                    final ClaimListDTO claimList = getClaimList(policyDTO.getBancsPolNum(), xGUID, xGUIDBancsPolNumCacheKey);
                    final BancsRetQuoteValDTO retQuoteVal = getRetQuoteVal(policyDTO.getBancsPolNum(),
                            customerData.getBancsUniqueCustNo(), xGUID, xGUIDBancsPolNumCacheKey);
                    if (CommonUtils.isobjectNotNull(polDetails)) {
                        listOfPolDetails.add(polDetails);
                        polDetailsBancsPolNumSet.add(polDetails.getBancsPolNum());
                    } else {
                        LOGGER.info("PolDetails for BancsPolNum {} is skipped because of validation failure xGUID :: {}",
                                policyDTO.getBancsPolNum());
                    }
                    if (CommonUtils.isobjectNotNull(claimList)) {
                        listOfClaimList.add(claimList);
                        claimListBancsPolNumSet.add(claimList.getBancsPolNum());
                    } else {
                        LOGGER.info("claimList for BancsPolNum {} is skipped because of validation failure xGUID :: {}",
                                policyDTO.getBancsPolNum());
                    }
                    if (CommonUtils.isobjectNotNull(retQuoteVal)) {
                        listOfRetQuoteVal.add(retQuoteVal);
                        retQuoteValBancsPolNumSet.add(retQuoteVal.getBancsPolNum());
                    } else {
                        LOGGER.info("retQuoteVal for BancsPolNum {} is skipped because of validation failure xGUID :: {}",
                                policyDTO.getBancsPolNum());
                    }
                }
            }
            if (isValidListOfPolicies(listOfPolDetails, listOfClaimList, listOfRetQuoteVal)) {
                matchingPolicyData.setValidPolicy(listOfPolDetails);
                matchingPolicyData.setValidClaimList(listOfClaimList);
                matchingPolicyData.setValidRetQuoteVal(listOfRetQuoteVal);
                matchingPolicyData.setPolDetailsBancsPolNumSet(polDetailsBancsPolNumSet);
                matchingPolicyData.setClaimListBancsPolNumSet(claimListBancsPolNumSet);
                matchingPolicyData.setRetQuoteValBancsPolNumSet(retQuoteValBancsPolNumSet);
                matchingPolicyData = getMatchingPolicyDetailsList(matchingPolicyData, xGUID);
                if (matchingPolicyData.getFlag()) {
                    finalDashboard = encashmentDashboardHelper.finalDashboardSetter(customerData.getEmailId(),
                            partyBasicOwner,
                            iDAndVDataDTO, partyCommDetails, policyDTOList, matchingPolicyData.getValidPolicy(),
                            matchingPolicyData.getValidRetQuoteVal(), matchingPolicyData.getValidClaimList(), xGUID);
                }
            } else {
                LOGGER.error(
                        "{} PolDetail,ClaimList,RetQuoteVal may have null values or there is no single policy for which this 3 API has response");
            }
        }
        return finalDashboard;
    }

    private boolean checkCustomerValidAge(final PartyBasicOwnerDTO partyBasicOwner,
            final CustomerIdentifierDTO customerData) {
        LocalDate storedDob = DateUtils
                .convertDate(partyBasicOwner.getDateOfBirth());
        return DateUtils.dobValidYearRange(storedDob);
    }

    /*
     * private void checkCustomerDetailsInDB(final PartyBasicOwnerDTO
     * partyBasicOwner, final CustomerIdentifierDTO customerData) { if
     * (!partyBasicOwner.getTitle().equalsIgnoreCase(customerData.getTitle()) ||
     * !partyBasicOwner.getForename().equalsIgnoreCase(customerData.getFirstName
     * ()) ||
     * !partyBasicOwner.getSurname().equalsIgnoreCase(customerData.getLastName()
     * )) { MPLCustomer mPLCustomer =
     * customerRepositoryImpl.fetchCustomer(customerData.getUserName());
     * mPLCustomer.setTitle(partyBasicOwner.getTitle().getBytes());
     * mPLCustomer.setFirstName(partyBasicOwner.getForename().getBytes());
     * mPLCustomer.setLastName(partyBasicOwner.getSurname().getBytes());
     * mPLCustomer.setLastUpdatedOn(Timestamp.valueOf(LocalDateTime.now()));
     * mPLCustomer.setLastUpdatedBy("SYSTEM");
     * customerRepositoryImpl.saveCustDetail(mPLCustomer); }
     *
     * }
     */

    private boolean isValidListOfPolicies(final List<PolDetailsDTO> listOfPolDetails,
            final List<ClaimListDTO> listOfClaimList, final List<BancsRetQuoteValDTO> listOfRetQuoteVal) {
        boolean result = false;
        if (CommonUtils.isValidList(listOfPolDetails) && CommonUtils.isValidList(listOfClaimList)
                && CommonUtils.isValidList(listOfRetQuoteVal)) {
            result = true;
        }
        return result;
    }

    private SyncDashboardServiceDTO getMatchingPolicyDetailsList(final SyncDashboardServiceDTO matchingPolicyData,
            final String xGUID) {
        boolean flag = true;
        matchingPolicyData.getPolDetailsBancsPolNumSet().retainAll(matchingPolicyData.getClaimListBancsPolNumSet());
        matchingPolicyData.getPolDetailsBancsPolNumSet().retainAll(matchingPolicyData.getRetQuoteValBancsPolNumSet());
        LOGGER.info("{} Intersection of Policies yielding result for all this 3 API {}",
                matchingPolicyData.getPolDetailsBancsPolNumSet().size());
        LOGGER.info("{} Size of Valid PolDetails, Valid ClaimLists, Valid RetQuoteVal {}, {}, {}",
                matchingPolicyData.getValidPolicy().size(),
                matchingPolicyData.getValidClaimList().size(), matchingPolicyData.getValidRetQuoteVal().size());
        final List<PolDetailsDTO> updatedValidPolicyDetails = new ArrayList<PolDetailsDTO>();
        final List<ClaimListDTO> updatedValidClaimList = new ArrayList<ClaimListDTO>();
        final List<BancsRetQuoteValDTO> updatedValidRetQuoteVal = new ArrayList<BancsRetQuoteValDTO>();
        for (final String bancsPolNum : matchingPolicyData.getPolDetailsBancsPolNumSet()) {
            for (final PolDetailsDTO polDetail : matchingPolicyData.getValidPolicy()) {
                if (bancsPolNum.equalsIgnoreCase(polDetail.getBancsPolNum())) {
                    updatedValidPolicyDetails.add(polDetail);
                }
            }
            for (final ClaimListDTO claimList : matchingPolicyData.getValidClaimList()) {
                if (bancsPolNum.equalsIgnoreCase(claimList.getBancsPolNum())) {
                    updatedValidClaimList.add(claimList);
                }
            }

            for (final BancsRetQuoteValDTO polVal : matchingPolicyData.getValidRetQuoteVal()) {
                if (bancsPolNum.equalsIgnoreCase(polVal.getBancsPolNum())) {
                    updatedValidRetQuoteVal.add(polVal);
                }
            }

        }
        LOGGER.debug("Updated the PolDetails, ClaimList & RetQuoteVal");
        if (matchingPolicyData.getPolDetailsBancsPolNumSet().isEmpty()) {
            flag = false;
        }
        final SyncDashboardServiceDTO matchingPolicyDataUpdated = new SyncDashboardServiceDTO();
        matchingPolicyDataUpdated.setValidPolicy(updatedValidPolicyDetails);
        matchingPolicyDataUpdated.setValidClaimList(updatedValidClaimList);
        matchingPolicyDataUpdated.setValidRetQuoteVal(updatedValidRetQuoteVal);
        matchingPolicyDataUpdated.setFlag(flag);
        return matchingPolicyDataUpdated;
    }

    private IdAndVDataDTO getIDAndVData(final String polNum, final String xGUID) throws Exception {
        final String idAndVDataResponse = bancsServiceClient.getIDAndVDataResponse(polNum, xGUID);
        return EncashmentDashboardHelper.getIdAndVDataCheck(idAndVDataResponse, xGUID);
    }

    private PartyCommDetailsDTO getPartyCommDetails(final String bancsUniqueCustNo, final String xGUID) throws Exception {
        final String partyCommDetailResponse = bancsServiceClient.getPartyCommDetailsResponse(bancsUniqueCustNo, xGUID);
        final String partyBasicPartyNoResponse = bancsServiceClient
                .getPartyBasicDetailByPartyNoResponse(bancsUniqueCustNo, xGUID);
        return EncashmentDashboardHelper.partyCommFlagLifeStatusCheck(partyCommDetailResponse, partyBasicPartyNoResponse,
                xGUID);
    }

    private PoliciesOwnedByAPartyDTO getPoliciesOwnedByAParty(final String bancsUniqueCustNo, final String xGUID)
            throws Exception {
        final String policiesOwnedByAPartyResponse = bancsServiceClient
                .policiesOwnedByParty(bancsUniqueCustNo, xGUID);
        return EncashmentDashboardHelper
                .getPoliciesOwnedByAPartyCheck(policiesOwnedByAPartyResponse, xGUID);
    }

    private PolDetailsDTO getPolDetails(final String bancsPolNum, final String xGUID, final String xGUIDBancsPolNumCacheKey)
            throws Exception {
        final String polDetailsResponse = bancsServiceClient.getPolDetailsResponse(bancsPolNum, xGUID,
                xGUIDBancsPolNumCacheKey);
        return EncashmentDashboardHelper.getPolDetailsCheck(polDetailsResponse, xGUID);

    }

    private ClaimListDTO getClaimList(final String bancsPolNum, final String xGUID, final String xGUIDBancsPolNumCacheKey)
            throws Exception {
        final String claimListResponse = bancsServiceClient.getClaimListResponse(bancsPolNum, xGUID,
                xGUIDBancsPolNumCacheKey);
        return EncashmentDashboardHelper.getClaimListCheck(claimListResponse, xGUID);

    }

    private BancsRetQuoteValDTO getRetQuoteVal(final String bancsPolNum, final String bancsUniqueCustNo, final String xGUID,
            final String xGUIDBancsPolNumCacheKey)
            throws Exception {
        final String retQuoteResponse = bancsServiceClient.getRetQuoteResponse(bancsPolNum, bancsUniqueCustNo,
                xGUID, xGUIDBancsPolNumCacheKey);
        return EncashmentDashboardHelper.getRetQuoteValCheck(retQuoteResponse, xGUID);

    }

    @Override
    @Transactional
    // public void submitPolicyEncashment(final CustomerDTO customerDTO, final
    public void submitEncashmentPolicyDetailsAndBaNCSCalls(final CustomerDTO customerDTO, final String xGUID)
            throws Exception {
        int count;
        int quoteStatus;

        MPLCustomerPolicy mplPolicy;
        MPLCustomer mplCustomer;
        MPLCustomerUpdateStatus customerUpdateStatus;
        BancsUpdateAttemptDTO bancsQuoteAttempt;
        MPLDocumentAudit docAudit;

        count = encashmentRepo.checkIfAlreadyEncashed(customerDTO.getPolicyId());

        if (count == 0) {
            LOGGER.info("|Encashment|: started for -->> mpl_customer: {} | mpl_policy: {}",
                    customerDTO.getCustomerId(), customerDTO.getPolicyId());

            mplPolicy = customerRepository.getEncashmentPolicy(customerDTO.getPolicyId());
            mplCustomer = mplPolicy.getPolicyCustomer();

            /** update NINO **/
            customerUpdateStatus = updateNinoAndSaveCustUpdt(mplCustomer, customerDTO.getNino(), mplPolicy.getPolicyId());

            LOGGER.info("|Encashment|: verifying quote for mpl_policy: {}", customerDTO.getPolicyId());
            bancsQuoteAttempt = new BancsUpdateAttemptDTO();
            quoteStatus = bancsUpdateService.sendQuoteDetails(customerDTO.getQuoteDetails(), mplPolicy,
                    bancsQuoteAttempt, xGUID);

            if (quoteStatus == 1 || quoteStatus == 2) {

                /** save application-form pdf **/
                docAudit = generateAndSaveApplicationFormPdf(customerDTO.getFormRequest(), mplPolicy, xGUID);

                /** save quote details **/
                saveQuoteDetail(customerDTO.getQuoteDetails(), mplPolicy, bancsQuoteAttempt);

                /** save policy encashment details **/
                savePolicyEncashmentDetails(xGUID, bancsQuoteAttempt.getQuoteRef(), mplPolicy);

                LOGGER.info("|Encashment|: completed for -->> mpl_customer: {} | mpl_policy: {}",
                        customerDTO.getCustomerId(), customerDTO.getPolicyId());

                if (quoteStatus == 2) {
                    /** log correspondence API (asynchronous) **/
                    bancsUpdateService.sendDocumentUpdate(docAudit, mplPolicy, "AF", xGUID);
                }

                /** emailPhoneUserID API (asynchronous) **/
                bancsUpdateService.sendPersonalDetailUpdate(customerUpdateStatus, MPLServiceConstants.ENCASHMENT, xGUID);
                bancsServiceClient.clearFinalDashboardCache(xGUID);

            } else {
                LOGGER.error("|Encashment|: failed for -->> mpl_customer: {} | mpl_policy: {}",
                        customerDTO.getCustomerId(), customerDTO.getPolicyId());
                throw new MPLException("Invalid quote details");
            }

        } else {
            LOGGER.error("|Encashment|: already completed for -->> mpl_customer: {} | mpl_policy: {}",
                    customerDTO.getCustomerId(), customerDTO.getPolicyId());
            throw new MPLException("Policy already encashed");
        }

    }

    private MPLCustomerUpdateStatus updateNinoAndSaveCustUpdt(final MPLCustomer mplCustomer, final String nino,
            final long policyId) throws Exception {

        MPLCustomerUpdateStatus customerUpdateStatus;

        if (CommonUtils.stringNotBlankCheck(nino)) {
            LOGGER.info("|Encashment|: updating NINO for mpl_customer: {}", mplCustomer.getCustomerId());
            mplCustomer.setNino(nino.getBytes());
            mplCustomer.setLastUpdatedBy(MPLServiceConstants.SYSTEM);
            mplCustomer.setLastUpdatedOn(DateUtils.getCurrentTimestamp());
            customerRepository.saveCustDetail(mplCustomer);
        }

        customerUpdateStatus = custUpdtRepository
                .saveAndFlush(UpdateAttemptsMapper
                        .mapCustUpdateStatus(mplCustomer, policyId, MPLServiceConstants.ALPHABET_X));

        return customerUpdateStatus;
    }

    private MPLDocumentAudit generateAndSaveApplicationFormPdf(final FormRequestDTO formRequest,
            final MPLCustomerPolicy mplPolicy, final String xGUID) throws Exception {

        MPLDocumentAudit docAudit;
        LOGGER.info("|Encashment|: generating and saving application-form for mpl_policy: {}", mplPolicy.getPolicyId());
        docAudit = docRepository
                .saveAndFlush(PdfMapper.mapToDocumentEntity(formRequest, mplPolicy, freeMarkerConfiguration, context));

        return docAudit;
    }

    private void saveQuoteDetail(final QuoteDetails quote, final MPLCustomerPolicy mplPolicy,
            final BancsUpdateAttemptDTO bancsQuoteAttempt) throws Exception {

        LOGGER.info("|Encashment|: saving quote details for mpl_policy: {}", mplPolicy.getPolicyId());
        quoteRepository.saveQuoteDetail(BancsUpdateRequestMapper.mapToQuoteEntity(quote, mplPolicy, bancsQuoteAttempt));
    }

    private void savePolicyEncashmentDetails(final String xGUID, final String encashRefNo,
            final MPLCustomerPolicy mplPolicy) throws Exception {

        MPLCustomer customer;
        MPLPolicyEncashment policyEncashment;

        customer = mplPolicy.getPolicyCustomer();

        LOGGER.info("|Encashment|: saving policy encashment details for mpl_policy: {}", mplPolicy.getPolicyId());
        policyEncashment = encashmentRepo
                .saveAndFlush(BancsUpdateRequestMapper.mapToPolicyEncashmentEntity(xGUID, encashRefNo, mplPolicy));

        LOGGER.info("|Encashment|: saving encashment email status for mpl_customer: {} | policy encashment: {}",
                customer.getCustomerId(), policyEncashment.getEncashmentId());
        emailStatusRepository.saveAndFlush(EmailMapper
                .mapToEmailStatusEntity(policyEncashment.getEncashmentId(), mplPolicy.getPolicyCustomer(), encashRefNo));

    }

}
