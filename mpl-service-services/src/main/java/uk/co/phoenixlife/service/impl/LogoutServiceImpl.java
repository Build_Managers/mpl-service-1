package uk.co.phoenixlife.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import uk.co.phoenixlife.service.client.bancs.BancsServiceClient;
import uk.co.phoenixlife.service.interfaces.LogoutService;
import uk.co.phoenixlife.service.utils.CommonUtils;

@Component
public class LogoutServiceImpl implements LogoutService {

    @Autowired
    private BancsServiceClient bancsServiceClient;

    /**
     * clearCache
     * 
     * @param xGUID
     *            - xGUID
     * @param xGUIDBancsPolNumCacheKeyList
     *            (non-Javadoc)
     * @see uk.co.phoenixlife.service.interfaces.LogoutService#clearCache(java.lang.String,
     *      java.util.List)
     */
    @Override
    public void clearCache(final String xGUID, final List<String> xGUIDBancsPolNumCacheKeyList) {
        bancsServiceClient.clearCache(xGUID);
        if (CommonUtils.isObjectNotNull(xGUIDBancsPolNumCacheKeyList)) {
            for (final String xGUIDBancsPolNumCacheKey : xGUIDBancsPolNumCacheKeyList) {
                bancsServiceClient.clearXGUIDBancsPolNumCache(xGUIDBancsPolNumCacheKey);
            }
        }
    }
}
