package uk.co.phoenixlife.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import uk.co.phoenixlife.security.service.constants.MPLSecurityServiceConstant;
import uk.co.phoenixlife.security.service.dto.ForgotPasswordDto;
import uk.co.phoenixlife.security.service.dto.UserRegistrationDTO;
import uk.co.phoenixlife.security.service.dto.UserRegistrationFormDTO;
import uk.co.phoenixlife.security.service.interfaces.MPLSecurityService;
import uk.co.phoenixlife.service.client.security.MPLSecurityClient;
import uk.co.phoenixlife.service.dto.ErrorListDTO;
import uk.co.phoenixlife.service.dto.MPLDropoutDTO;
import uk.co.phoenixlife.service.dto.MiErrorRequestDTO;
import uk.co.phoenixlife.service.interfaces.MIComplianceServiceInterface;

@Component
public class MPLSecurityServiceImpl implements MPLSecurityService {

    @Autowired
    private MPLSecurityClient mPLSecurityClient;
    
    @Autowired
	 private MIComplianceServiceInterface miComplianceServiceInterface;

    @Override
    public void insertUserDetailInLDAP(final UserRegistrationFormDTO userRegistrationFormDTO) throws Exception {

        mPLSecurityClient.insertUserDetailInLDAP(userRegistrationFormDTO);
    }

    @Override
    public void updateUserDetailInLDAP(final UserRegistrationFormDTO userRegistrationFormDTO) throws Exception {

        mPLSecurityClient.updateUserDetailInLDAP(userRegistrationFormDTO);
    }

    @Override
    public UserRegistrationDTO checkUserNameAlreadyExist(final String userName) throws Exception {

        return mPLSecurityClient.checkUserNameAlreadyExist(userName);
    }

    @Override
    public boolean checkEmailAlreadyExist(final UserRegistrationFormDTO userRegistrationFormDTO) throws Exception {
        return mPLSecurityClient.checkEmailAlreadyExist(userRegistrationFormDTO);
    }

    @Override
    public UserRegistrationFormDTO activateAccount(final String token) throws Exception {
    	
        UserRegistrationFormDTO userRegistrationFormDTO = mPLSecurityClient.activateAccount(token);
        if (userRegistrationFormDTO.isInvalidToken() || userRegistrationFormDTO.isTokenExpired() ) {
        	MiErrorRequestDTO miErrorRequestDTO = new MiErrorRequestDTO();
        	ErrorListDTO  errorListDTO = new ErrorListDTO();
        	errorListDTO.setPageId("PG105");
        	errorListDTO.setNextPageId("PG106");
        	errorListDTO.setMessageCount(1);
        	errorListDTO.setTimeSpent(1);
        	errorListDTO.setMessageId("E049");
        	List<ErrorListDTO> errorDTOList = new ArrayList<ErrorListDTO>();
        	errorDTOList.add(errorListDTO);
        	miErrorRequestDTO.setErrorListDTO(errorDTOList);
        	miComplianceServiceInterface.submitErrorList(miErrorRequestDTO);
        }
        return userRegistrationFormDTO;

    }

    @Override
    public String fetchSecurityQuestion(final ForgotPasswordDto forgotPasswordDto) throws Exception {
        // TODO Auto-generated method stub
        return mPLSecurityClient.fetchSecurityQuestion(forgotPasswordDto);
    }

    @Override
    public String submitSecurityAnswer(final ForgotPasswordDto forgotPasswordDto) throws Exception {
        // TODO Auto-generated method stub
    	Gson gson = new Gson();
        String response = mPLSecurityClient.submitSecurityAnswer(forgotPasswordDto);
        ForgotPasswordDto resultObject = gson.fromJson(response, ForgotPasswordDto.class);
        if (MPLSecurityServiceConstant.INVALIDANSWER.equals(resultObject.getReturnMessage())) {
            
            	MiErrorRequestDTO miErrorRequestDTO = new MiErrorRequestDTO();
            	ErrorListDTO  errorListDTO = new ErrorListDTO();
            	errorListDTO.setPageId("PG106");
            	errorListDTO.setNextPageId("PG107");
            	errorListDTO.setMessageCount(1);
            	errorListDTO.setTimeSpent(1);
            	errorListDTO.setMessageId("E018");
            	List<ErrorListDTO> errorDTOList = new ArrayList<ErrorListDTO>();
            	errorDTOList.add(errorListDTO);
            	miErrorRequestDTO.setErrorListDTO(errorDTOList);
            	miComplianceServiceInterface.submitErrorList(miErrorRequestDTO);
             
        }
        else if(MPLSecurityServiceConstant.ACCOUNTLOCKED.equals(resultObject.getReturnMessage())){
        	
        	MiErrorRequestDTO miErrorRequestDTO = new MiErrorRequestDTO();
        	ErrorListDTO  errorListDTO = new ErrorListDTO();
        	errorListDTO.setPageId("PG106");
        	errorListDTO.setNextPageId("PG107");
        	errorListDTO.setMessageCount(1);
        	errorListDTO.setTimeSpent(1);
        	errorListDTO.setMessageId("E019");
        	List<ErrorListDTO> errorDTOList = new ArrayList<ErrorListDTO>();
        	errorDTOList.add(errorListDTO);
        	miErrorRequestDTO.setErrorListDTO(errorDTOList);
        	miComplianceServiceInterface.submitErrorList(miErrorRequestDTO);
        }
        return response;
    }

    @Override
    public String changePassword(final ForgotPasswordDto forgotPasswordDto) throws Exception {
        // TODO Auto-generated method stub
    	 return mPLSecurityClient.changePassword(forgotPasswordDto);
    }


    @Override
    public String changePassUrlValidate(final String token) throws Exception {
        // TODO Auto-generated method stub
    	 return mPLSecurityClient.changePassUrlValidate(token);
    }

	@Override
	public void lockAccount(final String userName, boolean isRegistration) throws Exception {
		// TODO Auto-generated method stub
	}

    @Override
    public String increaseAnswerAttemptCount(final String userName) throws Exception {
        // TODO Auto-generated method stub
    	String output= mPLSecurityClient.increaseAnswerAttemptCount(userName);
    	return output;
    }

    @Override
    public String changePasswordDashboard(final ForgotPasswordDto forgotPasswordDto) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String changeEmailDashboard(final ForgotPasswordDto forgotPasswordDto) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void updateEmailStatusAndLDAPStatus(final String userName, final String status) throws Exception {

	}

	@Override
	public UserRegistrationFormDTO ResetAccount(String token) throws Exception {
	
		  UserRegistrationFormDTO userRegistrationFormDTO = mPLSecurityClient.activateAccount(token);
	        if (userRegistrationFormDTO.isInvalidToken() || userRegistrationFormDTO.isTokenExpired() ) {
	        	MiErrorRequestDTO miErrorRequestDTO = new MiErrorRequestDTO();
	        	ErrorListDTO  errorListDTO = new ErrorListDTO();
	        	errorListDTO.setPageId("PG200");
	        	errorListDTO.setNextPageId("PG201");
	        	errorListDTO.setMessageCount(1);
	        	errorListDTO.setTimeSpent(1);
	        	errorListDTO.setMessageId("E049");
	        	List<ErrorListDTO> errorDTOList = new ArrayList<ErrorListDTO>();
	        	errorDTOList.add(errorListDTO);
	        	miErrorRequestDTO.setErrorListDTO(errorDTOList);
	        	miComplianceServiceInterface.submitErrorList(miErrorRequestDTO);
	        }else if( userRegistrationFormDTO.isLockedStatus())
	        {
	        	MiErrorRequestDTO miErrorRequestDTO = new MiErrorRequestDTO();
	        	ErrorListDTO  errorListDTO = new ErrorListDTO();
	        	errorListDTO.setPageId("PG200");
	        	errorListDTO.setNextPageId("PG201");
	        	errorListDTO.setMessageCount(1);
	        	errorListDTO.setTimeSpent(1);
	        	errorListDTO.setMessageId("E024");
	        	List<ErrorListDTO> errorDTOList = new ArrayList<ErrorListDTO>();
	        	errorDTOList.add(errorListDTO);
	        	miErrorRequestDTO.setErrorListDTO(errorDTOList);
	        	miComplianceServiceInterface.submitErrorList(miErrorRequestDTO);
	        }
	        return userRegistrationFormDTO;

	}

	@Override
	public String updateSecurityQuestionInLDAP(UserRegistrationFormDTO userRegistrationFormDTO) throws Exception {
		// TODO Auto-generated method stub
		return mPLSecurityClient.updateSecurityQuestionInLDAP(userRegistrationFormDTO);
	}

	@Override
	public void sendFailureRegMail(String userName) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public String fetchEmailIdFromLDAP(String userName) throws Exception {
		
		return mPLSecurityClient.fetchEmailIdFromLDAP(userName);
	}

	@Override
	public String fetchDetailsFromLDAP(String userName) throws Exception {
		return mPLSecurityClient.fetchDetailsFromLDAP(userName);
	}

}
