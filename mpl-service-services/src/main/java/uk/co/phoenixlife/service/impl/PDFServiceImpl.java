/*
 * PDFCreationServiceImpl.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itextpdf.text.Document;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfArray;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;

import freemarker.template.Configuration;
import uk.co.phoenixlife.service.async.BancsUpdateService;
import uk.co.phoenixlife.service.client.bancs.BancsServiceClient;
import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dao.entity.MPLCustomer;
import uk.co.phoenixlife.service.dao.entity.MPLCustomerPolicy;
import uk.co.phoenixlife.service.dao.entity.MPLDocumentAudit;
import uk.co.phoenixlife.service.dao.entity.MPLDocumentUpdateStatus;
import uk.co.phoenixlife.service.dao.repository.CustomerPolicyRepository;
import uk.co.phoenixlife.service.dao.repository.CustomerRepository;
import uk.co.phoenixlife.service.dao.repository.CustomerRepositoryImpl;
import uk.co.phoenixlife.service.dao.repository.DocumentAuditRepository;
import uk.co.phoenixlife.service.dao.repository.DocumentUpdateStatusRepository;
import uk.co.phoenixlife.service.dto.pdf.FormRequestDTO;
import uk.co.phoenixlife.service.dto.pdf.PdfDataDTO;
import uk.co.phoenixlife.service.dto.policy.DashboardDTO;
import uk.co.phoenixlife.service.dto.policy.FinalDashboardDTO;
import uk.co.phoenixlife.service.dto.policy.IdAndVDataDTO;
import uk.co.phoenixlife.service.dto.policy.PartyPolicyDTO;
import uk.co.phoenixlife.service.dto.policy.QuoteDTO;
import uk.co.phoenixlife.service.helper.EncashmentDashboardHelper;
import uk.co.phoenixlife.service.interfaces.PDFService;
import uk.co.phoenixlife.service.mapper.PdfMapper;
import uk.co.phoenixlife.service.utils.CommonUtils;
import uk.co.phoenixlife.service.utils.DateUtils;

/**
 * PDFServiceImpl.java
 */
@Component
public class PDFServiceImpl implements PDFService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PDFServiceImpl.class);

    private static final int ARRAYSIZE = 8192;

    /*@Value("${privacyNotice.pdf.path}")
    private String privacyNoticePdfPath;*/

    @Value("${termsOfUse.pdf.path}")
    private String privacyNoticePdfPath;

    @Value("${pensionScamsLeaflet.pdf.path}")
    private String pensionScamsLeaflet;

    @Value("${signposting.pdf.path}")
    private String signpostingfilepath;

    @Value("${static.pdf.path}")
    private String filePath;

    @Autowired
    private BancsUpdateService bancsUpdateService;

    @Autowired
    private DocumentUpdateStatusRepository docUpdateStatusRepo;

    @Autowired
    private CustomerPolicyRepository customerPolicyRepository;

    @Autowired
    private CustomerRepositoryImpl customerRepositoryImpl;

    @Autowired
    private DocumentAuditRepository documentAuditRepository;

    @Autowired
    private Configuration freeMarkerConfiguration;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ServletContext context;

    @Autowired
    private BancsServiceClient bancsServiceClient;

    @Override
    public byte[] getStaticPdf(final String pdfType) throws Exception {

        byte[] pdfContent = getPrivacyNoticePdf();


        /*if ("termsOfUse".equals(pdfType)) {
            pdfContent = getTermsOfUsePdf();
        } else if ("privacyStatement".equals(pdfType)) {
            pdfContent = getPrivacyStatementPdf();
        }*/

        return pdfContent;
    }

    @Override
    public Long insertEligiblePolicyData(final Long customerId, final DashboardDTO eligible, final String xGUID)
            throws Exception {
        LOGGER.debug("inside insertEligiblePolicyData method");
        final MPLCustomer customer = customerRepository.findOne(customerId);
        final MPLCustomerPolicy customerPolicy = new MPLCustomerPolicy();
        customerPolicy.setBancsPolNum(eligible.getBancsPolNum());
        customerPolicy.setLegacyPolNum(eligible.getPolNum());
        customerPolicy.setPolicyLockedStatus('N');
        customerPolicy.setCreatedBy("SYSTEM");
        customerPolicy.setCreatedOn(Timestamp.valueOf(LocalDateTime.now()));
        customerPolicy.setPolicyCustomer(customer);
        final MPLCustomerPolicy savedResult = customerPolicyRepository.saveAndFlush(customerPolicy);
        LOGGER.info("Sucessfull inserted EligiblePolicyData");
        return savedResult.getPolicyId();
    }

    @Transactional
    @Override
    public String generatePdf(final long policyId, final String docType, final String xGUID) throws Exception {

        List<MPLDocumentAudit> documents;
        MPLDocumentAudit document;
        MPLDocumentUpdateStatus docUpdateStatus;
        String htmlBase64 = null;
        byte[] encodedByte = null;

        LOGGER.debug("inside generatePdf method for DocType:: {}", docType);

        htmlBase64 = (String) CommonUtils.getNullObject();
        documents = documentAuditRepository.findDocumentByPolicyIdAndDocType(policyId, docType);
        if (CommonUtils.isValidList(documents)) {
            document = documents.get(0);
            encodedByte = document.getDocumentContents();

            docUpdateStatus = document.getDocumentUpdateStatus();

            htmlBase64 = new String(encodedByte);
            if ("RP".equalsIgnoreCase(docType) && MPLServiceConstants.ALPHABET_X == docUpdateStatus.getStatus()) {
                updateRetirementPackBancsFlagCheck(docUpdateStatus.getUpdateId());
                bancsUpdateService.sendDocumentUpdate(document, customerRepositoryImpl.getEncashmentPolicy(policyId),
                        docType, xGUID);
            }

            LOGGER.info("succesfull  inside generatePdf method for DocType:: {}", docType);

        }

        return htmlBase64;
    }

    private void updateRetirementPackBancsFlagCheck(final long documentUpdateId) throws Exception {
        LOGGER.debug("Updating RetirementPackBancsFlag ", documentUpdateId);
        docUpdateStatusRepo.updateRetirementPackBancsFlag(documentUpdateId);
        LOGGER.info("Sucessfull Updating RetirementPackBancsFlag ", documentUpdateId);
    }

    /**
     * Method is used for retirement pack
     *
     * @param dashboardDTO
     *            - dashboardDTO
     * @return boolean value
     */
    @Override
    public Boolean showRetirementPack(final DashboardDTO dashboardDTO, final String xGUID) {

        LOGGER.debug("isRetirementPackCheck inside Retirement service client executed");
        final List<Boolean> showRetirementList = new ArrayList<Boolean>();
        Boolean value = false;
        if (dashboardDTO.getListOfQuotes() != null && dashboardDTO.getListOfQuotes().size() > 0) {
            final List<QuoteDTO> quoteList = dashboardDTO.getListOfQuotes();
            for (int i = 0; i < quoteList.size(); i++) {
                final QuoteDTO quote = quoteList.get(i);
                if (CommonUtils.isobjectNotNull(quote) && CommonUtils.isobjectNotNull(quote.getQteDate())
                        && CommonUtils.isobjectNotNull(quote.getQteTypeCd())) {
                    final Date input = (Date) quote.getQteDate();
                    final LocalDate localDateBancs = DateUtils.convertToLocalDate(input);
                    final LocalDate now = LocalDate.now();
                    final Period diff = Period.between(localDateBancs, now);
                    if (quote.getQteTypeCd().equalsIgnoreCase(MPLConstants.RET) && diff.getYears() > 0) {
                        LOGGER.debug("isRetirementPackCheck First if satisfied");
                        showRetirementList.add(true);
                    } else if (quote.getQteTypeCd().equalsIgnoreCase(MPLConstants.RET)
                            && diff.getMonths() >= MPLConstants.SIX) {
                        LOGGER.debug("isRetirementPackCheck First elseif satisfied");
                        showRetirementList.add(true);
                    } else if (quote.getQteTypeCd().equalsIgnoreCase(MPLConstants.RET) && diff.getYears() < 1
                            && diff.getMonths() < MPLConstants.SIX) {
                        LOGGER.debug("isRetirementPackCheck second elseif satisfied");
                        showRetirementList.add(false);
                    } else if (quote.getQteTypeCd().equalsIgnoreCase("")
                            || !quote.getQteTypeCd().equalsIgnoreCase(MPLConstants.RET)) {
                        LOGGER.debug("isRetirementPackCheck third elseif satisfied");
                        value = true;
                    }
                } else {
                    LOGGER.debug("isRetirementPackCheck internal else satisfied");
                    value = true;
                }
            }
        } else {
            LOGGER.debug("isRetirementPackCheck external else satisfied");
            value = true;
        }
        if (showRetirementList.contains(false)) {
            LOGGER.debug("isRetirementPackCheck showRetirementList.contains false");
            value = false;
        } else {
            LOGGER.debug("isRetirementPackCheck showRetirementList.contains true");
            value = true;
        }
        LOGGER.debug("Condition to show retirement pack pdf completed");
        return value;
    }

    @Transactional
    public boolean savePdfContent(final FormRequestDTO formData) throws Exception {
        LOGGER.debug("inside savePdfContent method for DocType:: {} ", formData.getDocType());
        MPLDocumentAudit savedDocument;
        MPLCustomerPolicy mplPolicy;
        boolean flag = false;
        mplPolicy = customerPolicyRepository.findOne(formData.getPolicyId());
        savedDocument = documentAuditRepository
                .saveAndFlush(PdfMapper.mapToDocumentEntity(formData, mplPolicy, freeMarkerConfiguration, context));

        if (savedDocument != null) {
            flag = true;
        }
        LOGGER.info("succesfull  inside savePdfContent method for DocType:: {} ", formData.getDocType());
        return flag;
    }

    /**
     * merge
     *
     * @param inputPdfList
     *            - inputPdfList
     * @param baos
     *            - baos
     * @return baos
     * @throws Exception
     *             - Exception
     */
    @Override
    public byte[] merge(final List<ByteArrayInputStream> inputPdfList, final ByteArrayOutputStream baos) throws Exception {
        final Document document = new Document();
        final Rectangle one = new Rectangle(419, 595);
        document.setPageSize(one);
        final List<PdfReader> readers = new ArrayList<PdfReader>();
        int totalPages = 0;
        final Iterator<ByteArrayInputStream> pdfIterator = inputPdfList.iterator();
        while (pdfIterator.hasNext()) {
            final ByteArrayInputStream pdf = pdfIterator.next();
            final PdfReader pdfReader = instantiatingObjectForPdfReader(pdf);
            readers.add(pdfReader);
            totalPages = totalPages + pdfReader.getNumberOfPages();
        }
        final PdfWriter writer = PdfWriter.getInstance(document, baos);
        document.open();
        final PdfContentByte pageContentByte = writer.getDirectContent();
        PdfImportedPage pdfImportedPage;
        int currentPdfReaderPage = 1;
        final Iterator<PdfReader> iteratorPDFReader = readers.iterator();
        while (iteratorPDFReader.hasNext()) {
            final PdfReader pdfReader = iteratorPDFReader.next();
            while (currentPdfReaderPage <= pdfReader.getNumberOfPages()) {
                document.newPage();
                pdfImportedPage = writer.getImportedPage(pdfReader, currentPdfReaderPage);
                pageContentByte.addTemplate(pdfImportedPage, 0, 0);
                currentPdfReaderPage++;
            }
            currentPdfReaderPage = 1;
        }
        baos.flush();
        document.close();
        baos.close();
        return baos.toByteArray();
    }

    /**
     * Method is used for merging two pdf
     *
     * @return byte array
     * @throws IOException
     *             - IOException
     */
    @Override
    public byte[] doMerge() throws IOException {
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(filePath);
            return readFully(inputStream);
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    /**
     * mergeSign
     *
     * @param pdfName
     *            - pdfName
     * @return baos2
     * @throws Exception
     *             - Exception
     */
    @Override
    public byte[] mergeSign(final String pdfName) throws Exception {
        InputStream inputStream = null;
        try {
            if ("SIGN".equalsIgnoreCase(pdfName)) {
                inputStream = new FileInputStream(signpostingfilepath);
            } else {
                inputStream = new FileInputStream(pensionScamsLeaflet);
            }
            final byte[] signPdf = readFully(inputStream);
            final ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
            final PdfReader reader1 = new PdfReader(signPdf);
            final PdfStamper stamper = new PdfStamper(reader1, baos2);
            final int numberOfPages = reader1.getNumberOfPages();
            PdfDictionary page1;
            final PdfArray crop = new PdfArray();
            PdfArray media;
            for (int count = 1; count <= numberOfPages; count++) {
                page1 = reader1.getPageN(count);
                media = page1.getAsArray(PdfName.CROPBOX);
                if (media == null) {
                    media = page1.getAsArray(PdfName.MEDIABOX);
                }
                crop.add(instantiatingObject());
                crop.add(instantiatingObject());
                crop.add(instantiatingObjectWithMedia(media));
                crop.add(instantiatingObjectWithMediaThree(media));
                page1.put(PdfName.MEDIABOX, crop);
                page1.put(PdfName.CROPBOX, crop);
                stamper.getUnderContent(count).setLiteral("\nq 0.69 0 0 0.69 0 0 cm\nq\n");
                stamper.getOverContent(count).setLiteral("\nQ\nQ\n");
            }
            closeAll(stamper, reader1, baos2);
            return baos2.toByteArray();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    @Override
    public byte[] generateApplicationPdf(final byte[] decodedPDF, final String xGUID) throws Exception {
        byte[] finalPdf = null;
        final ByteArrayOutputStream toBeDecoded = new ByteArrayOutputStream();
        final Document document = new Document();
        final Rectangle one = new Rectangle(419, 595);
        document.setPageSize(one);
        final PdfWriter pdfWriter = PdfWriter.getInstance(document, toBeDecoded);
        final PdfReader reader = new PdfReader(decodedPDF);
        document.open();
        final PdfContentByte contentByte = pdfWriter.getDirectContent();
        final int total = reader.getNumberOfPages() + 1;
        for (int i = 1; i < total; i++) {
            final PdfImportedPage page = pdfWriter.getImportedPage(reader, i);
            document.newPage();
            contentByte.addTemplate(page, 0, 0);
        }
        document.close();
        toBeDecoded.close();
        finalPdf = toBeDecoded.toByteArray();
        return finalPdf;
    }

    @Override
    public byte[] generateRetirementPackPdf(final PdfDataDTO pdfData, final String xGUID)
            throws Exception {
        byte[] finalPdf = null;
        final byte[] staticPdf = doMerge();
        final byte[] signPdf = mergeSign("SIGN");
        final byte[] pensionScamsLeafletPdf = mergeSign("PS");
        final ByteArrayInputStream byteArrayDecoded = new ByteArrayInputStream(pdfData.getPdfByte());
        final ByteArrayInputStream byteArrayStatic = new ByteArrayInputStream(staticPdf);
        final ByteArrayInputStream byteArraySign = new ByteArrayInputStream(signPdf);
        final ByteArrayInputStream byteArrayPensionScams = new ByteArrayInputStream(pensionScamsLeafletPdf);
        final List<ByteArrayInputStream> inputPdfList = new ArrayList<ByteArrayInputStream>();
        inputPdfList.add(byteArrayDecoded);
        inputPdfList.add(byteArrayStatic);
        inputPdfList.add(byteArrayPensionScams);
        inputPdfList.add(byteArraySign);
        final ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
        finalPdf = merge(inputPdfList, baos1);

        /*
         * if (session.getAttribute(MPLConstants.SENTTOBANCS) == null) { LOGGER.
         * debug("Real time bancs post call for Retirement Pack of policyId {}",
         * policyId); final String xGUID = (String)
         * session.getAttribute(MPLConstants.XGUIDKEY);
         * asyncService.sendDocument(policyId, MPLConstants.RPLITERAL, xGUID);
         * session.setAttribute(MPLConstants.SENTTOBANCS, "Y"); }
         */
        return finalPdf;
    }

    private byte[] getPrivacyNoticePdf() throws IOException {

        InputStream inputStream = null;

        try {
            inputStream = new FileInputStream(privacyNoticePdfPath);
            return readFully(inputStream);
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

   /* private byte[] getPrivacyStatementPdf() throws IOException {

        InputStream inputStream = null;

        try {
            inputStream = new FileInputStream(privacyStatementPdfPath);
            return readFully(inputStream);
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }*/

    private static byte[] readFully(final InputStream stream) throws IOException {
        final byte[] buffer = new byte[ARRAYSIZE];
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int bytesRead;
        while ((bytesRead = stream.read(buffer)) != -1) {
            baos.write(buffer, 0, bytesRead);
        }
        return baos.toByteArray();
    }

    private static PdfReader instantiatingObjectForPdfReader(final ByteArrayInputStream pdf) throws IOException {
        return new PdfReader(pdf);
    }

    private static PdfNumber instantiatingObject() {
        return new PdfNumber(0);
    }

    private static PdfNumber instantiatingObjectWithMedia(final PdfArray media) {
        return new PdfNumber(media.getAsNumber(2).floatValue() / MPLConstants.ONEPOINTONE);
    }

    private static PdfNumber instantiatingObjectWithMediaThree(final PdfArray media) {
        return new PdfNumber(media.getAsNumber(MPLConstants.THREE).floatValue() / MPLConstants.ONEPOINTONE);
    }

    private void closeAll(final PdfStamper stamper, final PdfReader reader1, final ByteArrayOutputStream baos2)
            throws Exception {
        stamper.close();
        reader1.close();
        baos2.close();
    }

    @Override
    public Boolean savePdfContentToDB(final FormRequestDTO pdfData, final String xGUID) throws Exception {
        DashboardDTO eligible = null;
        FinalDashboardDTO finalDashboard = pdfData.getFinalDashboard();
        for (DashboardDTO dashboardDTO : finalDashboard.getDashboardPolicyList()) {
            if (dashboardDTO.isEligible()) {
                LOGGER.debug("{} Found Eligible Policy");
                eligible = dashboardDTO;
            }
        }
        String documentName = CommonUtils.generateUniqueFileName(eligible.getPolNum(), finalDashboard.getBancsUniqueCustNo(),
                "RetirementPack");
        /*
         * FormRequestDTO pdfData; pdfData = new FormRequestDTO();
         */
        pdfData.setDocType("RP");
        pdfData.setDocumentName(documentName);
        pdfData.setPolNum(eligible.getPolNum());
        pdfData.setTotValue(eligible.getTotValue());
        pdfData.setMvrValue(eligible.getMvrValue());
        pdfData.setMvaValue(eligible.getMvaValue());
        // As per CR 032
        pdfData.setEstimatedPensionFund(
                eligible.getTotValue() + eligible.getPenValue() + eligible.getcUCharge() + eligible.getMvaValue());

        pdfData.setPenaltyValue(eligible.getPenValue() + eligible.getcUCharge());

        LOGGER.debug("{} saving retirement pack ");
        return savePdfContent(pdfData);
    }

    @Override
    public PartyPolicyDTO getEligiblePolicyIdvData(final DashboardDTO eligible, final String xGUID) throws Exception {
        final String idAndVDataResponse = bancsServiceClient.getIDAndVDataResponse(eligible.getPolNum(), xGUID);
        IdAndVDataDTO idvDTO = EncashmentDashboardHelper.getIdAndVDataCheck(idAndVDataResponse, xGUID);
        PartyPolicyDTO partyPolicyDTO = null;
        for (PartyPolicyDTO partyPolicy : idvDTO.getListOfPolicies()) {
            if (partyPolicy.getBancsPolNum().equalsIgnoreCase(eligible.getBancsPolNum())) {
                partyPolicyDTO = partyPolicy;
            }
        }

        return partyPolicyDTO;
    }

    @Override
    public Long checkCustomerPolicyExistance(final Long customerId, final DashboardDTO eligible, final String xGUID)
            throws Exception {
        MPLCustomerPolicy savedResult = null;
        savedResult = customerPolicyRepository.getPolicies(customerId, eligible.getPolNum());
        if (savedResult == null) {
            final MPLCustomer customer = customerRepository.findOne(customerId);
            final MPLCustomerPolicy customerPolicy = new MPLCustomerPolicy();
            customerPolicy.setBancsPolNum(eligible.getBancsPolNum());
            customerPolicy.setLegacyPolNum(eligible.getPolNum());
            customerPolicy.setCreatedBy("SYSTEM");
            customerPolicy.setCreatedOn(Timestamp.valueOf(LocalDateTime.now()));
            customerPolicy.setPolicyCustomer(customer);
            savedResult = customerPolicyRepository.saveAndFlush(customerPolicy);
            LOGGER.info("Sucessfull inserted new Policy data in DB");
        } else {
            LOGGER.info("Policy record already present in DB");
        }

        LOGGER.info("Sucessfull inside insertEligiblePolicyData method");
        return savedResult.getPolicyId();
    }

}
