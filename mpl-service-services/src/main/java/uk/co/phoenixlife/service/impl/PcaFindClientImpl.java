package uk.co.phoenixlife.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import uk.co.phoenixlife.service.client.pca.PcaFindService;
import uk.co.phoenixlife.service.dto.pca.find.CaptureInteractiveFindV100;
import uk.co.phoenixlife.service.dto.pca.find.CaptureInteractiveFindV100ArrayOfResults;
import uk.co.phoenixlife.service.dto.pca.find.CaptureInteractiveFindV100Response;
import uk.co.phoenixlife.service.dto.pca.find.CaptureInteractiveFindV100Results;
import uk.co.phoenixlife.service.interfaces.PcaFindClientService;
import uk.co.phoenixlife.service.utils.CommonUtils;

@Component
public class PcaFindClientImpl extends CaptureInteractiveFindV100ArrayOfResults implements PcaFindClientService {
	
	 private static final Logger LOGGER = LoggerFactory.getLogger(PcaFindClientImpl.class);
	 private String key;
	 
	 public PcaFindClientImpl() {
	     LOGGER.info("in contructor");
	    }
	 	 /**
	     * Instantiates a new PCA predict find client.
	     *
	     * @param pcaKey
	     *            the pca key
	     */
	    public PcaFindClientImpl(final String pcaKey) {
	        this.key = pcaKey;
	    }
	 
	    @Autowired
	    private PcaFindService addressFindService;
	    
	    
	 
	@Override
	public CaptureInteractiveFindV100ArrayOfResults find(String postcode) {
		// TODO Auto-generated method stub
		
		List<CaptureInteractiveFindV100Results> listOfResults;

        listOfResults = new ArrayList<CaptureInteractiveFindV100Results>();

        CaptureInteractiveFindV100 findRequest = new CaptureInteractiveFindV100();

        findRequest.setKey(key);
        findRequest.setText(postcode);
        findRequest.setCountries("GB");
        findRequest.setLanguage("en");

        LOGGER.info(" --->>> Calling findAddress() method");
		
        listOfResults = addressFindService.findAddress(findRequest, listOfResults);
		
		LOGGER.info("Addr. Service Obj : {}", addressFindService.toString());
		
        if (CommonUtils.isobjectNotNull(listOfResults)) {
            super.captureInteractiveFindV100Results = listOfResults;
        }
        return this;
	}

	

}


