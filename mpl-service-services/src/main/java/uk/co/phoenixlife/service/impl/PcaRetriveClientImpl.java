package uk.co.phoenixlife.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.List;
import uk.co.phoenixlife.service.client.pca.PcaRetrieveService;

import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100;
import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100ArrayOfResults;
import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100Results;
import uk.co.phoenixlife.service.interfaces.PcaRetriveClientService;
import uk.co.phoenixlife.service.dto.pca.retrieve.ObjectFactory;


@Component
public class PcaRetriveClientImpl extends CaptureInteractiveRetrieveV100ArrayOfResults implements PcaRetriveClientService {
	
	 

	    /** The retrieve object factory. */
	    private ObjectFactory retrieveObjectFactory = new ObjectFactory();

	    /** The key. */
	    private String key;


	    /**
	     * Instantiates a new PCA predict retrieve client.
	     *
	     * @param pcaKey
	     *            the pca key
	     */
	    public PcaRetriveClientImpl (final String pcaKey) {
	        this.key = pcaKey;
	    }

	    /**
	     * Instantiates a new PCA predict retrieve client.
	     */
	    public PcaRetriveClientImpl() {

	    }
	
	    @Autowired
	    private PcaRetrieveService addressRetrieveService;

	
	
//	private static final Logger LOGGER = LoggerFactory.getLogger(PcaFindClientImpl.class);
	

	@Override
	public CaptureInteractiveRetrieveV100ArrayOfResults retrieve(
            final CaptureInteractiveRetrieveV100 retrieveRequest ) {

        final CaptureInteractiveRetrieveV100ArrayOfResults retrieveArray = retrieveObjectFactory
                .createCaptureInteractiveRetrieveV100ArrayOfResults();
        final List <CaptureInteractiveRetrieveV100Results> listOfRetrieveResults = retrieveArray
                .getCaptureInteractiveRetrieveV100Results();

		//LOGGER.info("Calling retrieveAddress()");
        super.captureInteractiveRetrieveV100Results = addressRetrieveService
                .retrieveAddress(retrieveRequest, listOfRetrieveResults);
				
		//LOGGER.info("Addr. Service Obj : {}", addressRetrieveService.toString());

        return this;
    }
	public CaptureInteractiveRetrieveV100ArrayOfResults captureInteractiveRetrieveV100(
            final String id) {
        CaptureInteractiveRetrieveV100 retrieveRequest = retrieveObjectFactory
                .createCaptureInteractiveRetrieveV100();

        retrieveRequest.setKey(key);
        retrieveRequest.setId(id);

        return retrieve(retrieveRequest);
    }
	
	
	}
	
	

