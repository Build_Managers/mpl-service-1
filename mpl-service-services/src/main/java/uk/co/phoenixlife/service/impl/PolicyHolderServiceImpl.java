/*
 * PolicyHolderServiceImpl.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.impl;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import uk.co.phoenixlife.service.client.bancs.BancsServiceClient;
import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dto.MPLException;
import uk.co.phoenixlife.service.dto.policy.CustomerDTO;
import uk.co.phoenixlife.service.dto.response.BancsPartyCommDetailsResponse;
import uk.co.phoenixlife.service.dto.response.PartyBasicDetailByPartyNoResponse;
import uk.co.phoenixlife.service.dto.response.PoliciesOwnedByAPartyResponse;
import uk.co.phoenixlife.service.interfaces.PolicyHolderService;

/**
 * PolicyHolderServiceImpl.java
 */
@Component
public class PolicyHolderServiceImpl implements PolicyHolderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PolicyHolderServiceImpl.class);

    @Autowired
    private BancsServiceClient bancsServiceClient;  
    
    @Autowired
    private ObjectMapper objectMapper;

    /*@Override
    public BancsPolicyHoldersResponse getPolicyHoldersDetails(final String bancsUniqueCustNo,
            final CustomerDTO customerDTO) throws JsonParseException, JsonMappingException, IOException {
        BancsPolicyHoldersResponse bancsPolicyHoldersResponse;
        bancsPolicyHoldersResponse = bancsServiceClient.getPolicyHoldersDetails(bancsUniqueCustNo);
        if (isCustomerDataDiffer(customerDTO, bancsPolicyHoldersResponse)) {
            LOGGER.debug("Customer Data is different in Bancs and Portal");
            // Update Customer Data
        }
        LOGGER.info("PolicyHolderServiceImpl--> bancsPolicyHoldersResponse : {}", bancsPolicyHoldersResponse);
        return bancsPolicyHoldersResponse;
    }

    private boolean isCustomerDataDiffer(final CustomerDTO customerDTO,
            final BancsPolicyHoldersResponse bancsPolicyHoldersResponse) {
        // TODO Write Business logic to update details

		return false;
	}*/

    /**
     * @param customerDTO
     * @return (non-Javadoc)
     * @see uk.co.phoenixlife.service.interfaces.PolicyHolderService#updateEmail(uk.co.phoenixlife.service.dto.policy.CustomerDTO)
     */
    @Override
    public Boolean updateEmail(final CustomerDTO customerDTO) {
        // TODO Auto-generated method stub
        LOGGER.info("Inside Service IMPL");
        Boolean flag= false;
        flag=true;
        return flag;
    }

	@Override
	public PartyBasicDetailByPartyNoResponse getPartyBasicDetailByPartyNo(final String bancsUniqueCustNo , final String xGUID) 
	throws Exception{
		// TODO Auto-generated method stub
		String response;
		PartyBasicDetailByPartyNoResponse partyBasicDetailByPartyNoResponse = null;
		response = bancsServiceClient.getPartyBasicDetailByPartyNoResponse(bancsUniqueCustNo,xGUID);
        //write Business Logic Here
		if (response.contains(MPLServiceConstants.ERRORSTRING)) {
            throw new MPLException();
        } else {
        	partyBasicDetailByPartyNoResponse = objectMapper.readValue(response, PartyBasicDetailByPartyNoResponse.class);
        }
		LOGGER.info("partyBasicDetailByPartyNoResponse received");
		return partyBasicDetailByPartyNoResponse;
	}

	/*@Override
	public BancsPartyCommDetailsResponse getBancsPartyCommDetails(String bancsUniqueCustNo) {
		// TODO Auto-generated method stub
		BancsPartyCommDetailsResponse bancsPartyCommDetailsResponse;
		bancsPartyCommDetailsResponse = bancsServiceClient.getBancsPartyCommDetailsResponse(bancsUniqueCustNo);
        //write Business Logic Here
		LOGGER.info("partyBasicDetailByPartyNoResponse received");
		return bancsPartyCommDetailsResponse;
	}*/
}
