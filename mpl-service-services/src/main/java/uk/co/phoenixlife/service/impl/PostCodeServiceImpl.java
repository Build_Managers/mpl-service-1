/*
 * PostCodeServiceImpl.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import uk.co.phoenixlife.service.client.pca.PcaFindClient;
import uk.co.phoenixlife.service.client.pca.PcaRetrieveClient;
import uk.co.phoenixlife.service.dto.pca.find.CaptureInteractiveFindV100ArrayOfResults;
import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100;
import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100ArrayOfResults;
import uk.co.phoenixlife.service.interfaces.PostCodeService;

/**
 * PostCodeServiceImpl.java
 */
@Component
public class PostCodeServiceImpl implements PostCodeService {

    @Autowired
    private PcaFindClient addressFindClient;

    @Autowired
    private PcaRetrieveClient addressRetrieveClient;

    @Override
    public CaptureInteractiveFindV100ArrayOfResults findAddress(final String postcode) throws Exception {
        return addressFindClient.find(postcode);
    }

    @Override
    public CaptureInteractiveRetrieveV100ArrayOfResults retrieveAddress(final CaptureInteractiveRetrieveV100 retrieveRequest)
            throws Exception {
        return addressRetrieveClient.retrieve(retrieveRequest);
    }

}
