/*
 * PolicyActivationServiceImpl.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import uk.co.phoenixlife.service.client.selfsevice.SelfServiceClient;
import uk.co.phoenixlife.service.dto.SecurityQuestionDTO;
import uk.co.phoenixlife.service.dto.SecurityQuestionsForAUserDTO;
import uk.co.phoenixlife.service.interfaces.SelfService;

/**
 * This class holds Business Logic for getting security questions for a particular user
 * SelfServiceImpl.java
 */
@Component
public class SelfServiceImpl implements SelfService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SelfServiceImpl.class);
    
  
    @Autowired
    private SelfServiceClient selfServiceClient;
    
	@Override
	public SecurityQuestionDTO getSecurityQuestions(String username) {
		
		
		return selfServiceClient.getQuestions(username);
	}

	@Override
	public boolean checkAnswerOfSecurityQuestion(SecurityQuestionsForAUserDTO dtoObj) {
		
		
		return selfServiceClient.checkAnswerOfSecurityQuestion(dtoObj);
		
	}

   
	
	
	
}
