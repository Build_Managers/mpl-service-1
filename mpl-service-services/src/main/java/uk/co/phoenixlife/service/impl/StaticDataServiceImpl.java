/*
 * StaticDataServiceImpl.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import uk.co.phoenixlife.service.dao.entity.MPLStaticData;
//import uk.co.phoenixlife.service.dao.repository.ProductMatrixRepository;
import uk.co.phoenixlife.service.dao.repository.StaticDataRepository;
import uk.co.phoenixlife.service.dto.staticdata.StaticDataService;
import uk.co.phoenixlife.service.dto.staticdata.StaticIDVDataDTO;
import uk.co.phoenixlife.service.utils.CommonUtils;

/**
 * StaticDataServiceImpl.java
 */
@Component
public class StaticDataServiceImpl implements StaticDataService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StaticDataServiceImpl.class);

    @Autowired
    private StaticDataRepository staticDataRepository;

    /*@Autowired
    private ProductMatrixRepository productMatrixRepo;*/

    /**
     * Post construct method
     *
     * @throws Exception
     *             - {@link Exception}
    
    @PostConstruct
    public void loadStaticDataCache() throws Exception {
        LOGGER.info("Loading static data cache at bean initialization...");
        staticDataRepository.getAllDistinctGroups().forEach(group -> getstaticDataList(group));

        LOGGER.info("Loading product matrix cache at bean initialization...");
        productMatrixRepo.getAllDistinctProductCodeAndFlag()
                .forEach(product -> productMatrixRepo.getProductMatricDetail(String.valueOf(product[0]),
                        String.valueOf(product[1])));

    } */

    @Override
    public Map<String, String> getStaticDataMap(final String groupId) throws Exception {

        //LOGGER.debug("Invoking getStaticDataMap of StaticDataServiceImpl for group : {}", groupId);

        List<MPLStaticData> staticDataList;
        Map<String, String> staticDataMap;

        staticDataMap = new HashMap<String, String>();
        staticDataList = getstaticDataList(groupId);

        staticDataList
                .forEach(staticData -> staticDataMap.put(staticData.getStaticDataKey(), staticData.getStaticDataValue()));

        LOGGER.debug("StaticDataMap records count for group: {} is {}",groupId, staticDataMap.size());

        return staticDataMap;
    }

    @Override
    public String getStaticDataValue(final String groupId, final String key) throws Exception {

        LOGGER.debug("Invoking getStaticDataValue of StaticDataServiceImpl for group : {} | key : {}", groupId, key);

        List<MPLStaticData> staticDataList;
        List<MPLStaticData> filtered;
        String dataValue = CommonUtils.getEmptyString();

        staticDataList = getstaticDataList(groupId);
        filtered = staticDataList.stream()
                .filter(staticData -> key.equalsIgnoreCase(staticData.getStaticDataKey()))
                .collect(Collectors.toList());

        if (CommonUtils.isValidList(filtered)) {
            dataValue = filtered.get(0).getStaticDataValue();
        }

        return dataValue;
    }

    private List<MPLStaticData> getstaticDataList(final String groupId) {

        List<MPLStaticData> staticDataList;

        staticDataList = staticDataRepository.findByStaticDataGroup(groupId);
        return staticDataList;
    }

    @Override
    public List<StaticIDVDataDTO> getIDVQuestionsData() throws Exception {

        LOGGER.info("Invoking getIDVQuestionsData of StaticDataService ---->>>>>");

        List<MPLStaticData> staticDataList;
        List<StaticIDVDataDTO> idvStaticDataList;

        idvStaticDataList = new ArrayList<StaticIDVDataDTO>();

        staticDataList = getstaticDataList("ID&V-Question");

        staticDataList.forEach(staticData -> idvStaticDataList
                .add(new StaticIDVDataDTO(staticData.getStaticDataKey(), staticData.getStaticDataValue(),
                        staticData.getQuestionLabel())));

        LOGGER.debug("ID&V questions count ---->>>>> {}", idvStaticDataList.size());

        return idvStaticDataList;
    }

}
