/*
 * BancsRequestMapper.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.mapper;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.dto.bancspost.BancsCalcAnnuityIllustrationInput;
import uk.co.phoenixlife.service.dto.bancspost.BancsCalcCashInTaxInput;
import uk.co.phoenixlife.service.dto.bancspost.BancsCalcCashInTaxRequest;
import uk.co.phoenixlife.service.dto.bancspost.BancsCalcCashInType;
import uk.co.phoenixlife.service.dto.bancspost.BancsCalcNonAnnuityIllustrationInput;
import uk.co.phoenixlife.service.dto.bancspost.BancsCreateQuote;
import uk.co.phoenixlife.service.dto.policy.QuoteDetails;
import uk.co.phoenixlife.service.utils.CommonUtils;

/**
 * BancsRequestMapper.java
 */
public final class BancsRequestMapper {

    private static final Logger LOGGER = LoggerFactory.getLogger(BancsRequestMapper.class);

    /**
     * Default Constructor
     */
    private BancsRequestMapper() {
        super();
    }

    /**
     * Method to create dummy annuity quote for 1(a)
     *
     * @return - BancsCalcCashInTaxRequest object
     */
    public static BancsCalcCashInTaxRequest getDummyAnnuityQuote(final String bancsCustomerNumber) {

        LOGGER.debug("Creating BancsCalcCashInTaxRequest -->>");

        BancsCalcCashInTaxRequest cashInTaxRequest;
        BancsCalcCashInTaxInput calTax;
        BancsCalcNonAnnuityIllustrationInput nonAnnuityTypes;
        BancsCalcAnnuityIllustrationInput annuityIll;
        List<BancsCalcCashInType> cashInTypes;
        List<BancsCalcAnnuityIllustrationInput> annuityIllList;
        BancsCalcCashInType ufpls;
        BancsCalcCashInType smallPot;

        cashInTaxRequest = new BancsCalcCashInTaxRequest();
        calTax = new BancsCalcCashInTaxInput();
        nonAnnuityTypes = new BancsCalcNonAnnuityIllustrationInput();
        annuityIll = new BancsCalcAnnuityIllustrationInput();
        cashInTypes = new ArrayList<BancsCalcCashInType>();
        annuityIllList = new ArrayList<BancsCalcAnnuityIllustrationInput>();
        ufpls = new BancsCalcCashInType();
        smallPot = new BancsCalcCashInType();

        ufpls.setCashInType(MPLConstants.UFPLS);
        smallPot.setCashInType(MPLConstants.SMALL_POT);

        cashInTypes.add(smallPot);
        cashInTypes.add(ufpls);

        nonAnnuityTypes.setListOfCashInTypes(cashInTypes);

        setDummyAnnuityIllustration(annuityIll);
        annuityIllList.add(annuityIll);

        calTax.setNonAnnuityIllustrationInput(nonAnnuityTypes);
        calTax.setAnnuityIllustrationInput(annuityIllList);
        calTax.setBancsUniqueCustNo(bancsCustomerNumber);

        cashInTaxRequest.setBancsCalcCashInTax(calTax);

        return cashInTaxRequest;
    }

    private static void setDummyAnnuityIllustration(final BancsCalcAnnuityIllustrationInput annuityIll) {

        LOGGER.debug("Creating dummy annuity illustration quote -->>");

        annuityIll.setOptionCode(MPLConstants.ANNUITY_OPTION_CODE);
        annuityIll.setPmtFreq(MPLConstants.ANNUITY_PYMT_FREQ_ANNUAL);
        annuityIll.setAdvArr(MPLConstants.ANNUITY_ARREARS);
        annuityIll.setLifeBasis(MPLConstants.ANNUITY_SINGLE);

        LOGGER.debug("Setting escType to {}", MPLConstants.ANNUITY_ESCALATION_LEVEL);

        annuityIll.setEscType(MPLConstants.ANNUITY_ESCALATION_LEVEL);
        annuityIll.setOverlap(String.valueOf(MPLConstants.NO));
        annuityIll.setProportion(String.valueOf(MPLConstants.NO));
    }

    public static BancsCreateQuote getQuotationForAPI(final QuoteDetails quote) {
        BancsCreateQuote createQuote;
        createQuote = new BancsCreateQuote();

        if (MPLConstants.ALPHABET_C == quote.getPaymentMethod().charAt(0)) {
            createQuote.setPaytMethod(MPLConstants.CHEQUE);
        } else {
            createQuote.setPaytMethod(MPLConstants.DIRECT_CREDIT);
            createQuote.setSortCode(new String(quote.getSortCode()));
            createQuote.setBankAcct(new String(quote.getBankAcctNo()));
            if (CommonUtils.isobjectNotNull(quote.getRollNo())) {
                createQuote.setRollNum(new String(quote.getRollNo()));
            }
        }

        createQuote.setCashInType(MPLConstants.UFPLS);
        if (quote.getSmallPotFlag()) {
            createQuote.setCashInType(MPLConstants.SMALL_POT);
        }
        /* For 1a set this to Y */
        createQuote.setSuppressQte(String.valueOf(MPLConstants.YES));

        createQuote.setContactCustomer(String.valueOf(MPLConstants.NO));

        if (quote.getContactCustomerFlag()) {
            createQuote.setContactCustomer(String.valueOf(MPLConstants.YES));
        }

        return createQuote;

    }
}
