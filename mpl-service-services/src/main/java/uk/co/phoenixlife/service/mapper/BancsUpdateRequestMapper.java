/*
 * BancsUpdateRequestMapper.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.mapper;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.codec.binary.Base64;

import com.itextpdf.text.Document;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;

import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dao.entity.MPLCustomer;
import uk.co.phoenixlife.service.dao.entity.MPLCustomerPolicy;
import uk.co.phoenixlife.service.dao.entity.MPLCustomerUpdateAttempt;
import uk.co.phoenixlife.service.dao.entity.MPLCustomerUpdateStatus;
import uk.co.phoenixlife.service.dao.entity.MPLDocumentAudit;
import uk.co.phoenixlife.service.dao.entity.MPLPolicyEncashment;
import uk.co.phoenixlife.service.dao.entity.MPLQuoteDetails;
import uk.co.phoenixlife.service.dao.entity.MPLQuoteUpdateAttempt;
import uk.co.phoenixlife.service.dao.entity.MPLQuoteUpdateStatus;
import uk.co.phoenixlife.service.dto.bancspost.BancsCreateQuote;
import uk.co.phoenixlife.service.dto.bancspost.BancsCreateQuoteRequest;
import uk.co.phoenixlife.service.dto.bancspost.BancsCustomerPhoneDetails;
import uk.co.phoenixlife.service.dto.bancspost.BancsEmailPhoneUserId;
import uk.co.phoenixlife.service.dto.bancspost.BancsEmailPhoneUserIdRequest;
import uk.co.phoenixlife.service.dto.bancspost.BancsLogCorrespondence;
import uk.co.phoenixlife.service.dto.bancspost.BancsLogCorrespondenceDocument;
import uk.co.phoenixlife.service.dto.bancspost.BancsLogCorrespondenceRequest;
import uk.co.phoenixlife.service.dto.bancspost.BancsUpdateAttemptDTO;
import uk.co.phoenixlife.service.dto.policy.QuoteDetails;
import uk.co.phoenixlife.service.utils.CommonUtils;
import uk.co.phoenixlife.service.utils.DateUtils;

/**
 * BancsUpdateRequestMapper.java
 */
public final class BancsUpdateRequestMapper {

    /**
     * Constructor
     */
    private BancsUpdateRequestMapper() {
        super();
    }

    /**
     * Method to map MPLCustomer to BancsEmailPhoneUserIdRequest
     *
     * @param mplCustomer
     *            - MPLCustomer object
     * @param updateType
     *            - updateType (userId / encashment)
     * @return - BancsEmailPhoneUserIdRequest object
     */
    public static BancsEmailPhoneUserIdRequest mapToBancsEmailPhoneUserIdRequest(final MPLCustomer mplCustomer,
            final String updateType) {

        BancsEmailPhoneUserIdRequest personalDetailUpdateRequest;
        BancsEmailPhoneUserId personalDetailUpdate;

        personalDetailUpdateRequest = new BancsEmailPhoneUserIdRequest();

        personalDetailUpdate = mapPersonalDetailUpdate(mplCustomer);

        if (MPLServiceConstants.ENCASHMENT.equalsIgnoreCase(updateType)
                && CommonUtils.isobjectNotNull(mplCustomer.getNino())) {

            personalDetailUpdate.setUpdateNiNo(MPLServiceConstants.YES);
            personalDetailUpdate.setNiNo(new String(mplCustomer.getNino()));
        }

        personalDetailUpdateRequest.setBancsEmailPhoneUserid(personalDetailUpdate);

        return personalDetailUpdateRequest;
    }

    private static BancsEmailPhoneUserId mapPersonalDetailUpdate(final MPLCustomer mplCustomer) {

        BancsEmailPhoneUserId bancsEmailPhoneUserId;
        bancsEmailPhoneUserId = new BancsEmailPhoneUserId();

        bancsEmailPhoneUserId.setUpdateMail(MPLServiceConstants.YES);
        bancsEmailPhoneUserId.setEmailId(new String(mplCustomer.getEmailAddr()));
        bancsEmailPhoneUserId.setUpdateUserName(MPLServiceConstants.YES);
        bancsEmailPhoneUserId.setUserName(mplCustomer.getUserName());
        bancsEmailPhoneUserId.setUpdateNiNo(MPLServiceConstants.NO);

        bancsEmailPhoneUserId.setListOfPhones(
                new ArrayList<BancsCustomerPhoneDetails>(Collections
                        .singletonList(mapToPhoneDetailsDTO(mplCustomer))));

        return bancsEmailPhoneUserId;
    }

    private static BancsCustomerPhoneDetails mapToPhoneDetailsDTO(final MPLCustomer mplCustomer) {

        BancsCustomerPhoneDetails primary;
        String phoneType;
        String phoneNumber;
        String[] phoneArr;

        primary = new BancsCustomerPhoneDetails();
        phoneType = mplCustomer.getPrimaryPhType();
        phoneNumber = new String(mplCustomer.getPrimaryPhNo());

        primary.setPhoneType(phoneType);
        primary.setPreferred(MPLServiceConstants.YES);
        primary.setCountryCode(MPLServiceConstants.UNITED_KINGDOM);

        if (MPLServiceConstants.MOBILE.equalsIgnoreCase(phoneType)) {
            phoneArr = CommonUtils.splitMobile(new StringBuffer(phoneNumber));
        } else {
            phoneArr = CommonUtils.splitHomeOrWork(new StringBuffer(phoneNumber));
        }

        primary.setStd(phoneArr[0]);
        primary.setNumber(phoneArr[1]);

        return primary;

    }

    /**
     * Method to map MPLQuoteDetails to BancsCreateQuoteRequest
     *
     * @param quoteDetails
     *            - MPLQuoteDetails object
     * @return - BancsCreateQuoteRequest object
     */
    public static BancsCreateQuoteRequest mapToBancsCreateQuoteRequest(final MPLQuoteDetails quoteDetails) {

        BancsCreateQuoteRequest bancsCreateQuoteRequest;
        BancsCreateQuote bancsCreateQuote;

        bancsCreateQuoteRequest = new BancsCreateQuoteRequest();
        bancsCreateQuote = new BancsCreateQuote();

        if (MPLServiceConstants.ALPHABET_C == quoteDetails.getPaymentMethod()) {
            bancsCreateQuote.setPaytMethod(MPLServiceConstants.CHEQUE);
        } else {
            bancsCreateQuote.setPaytMethod(MPLServiceConstants.DIRECT_CREDIT);
            bancsCreateQuote.setSortCode(new String(quoteDetails.getSortCode()));
            bancsCreateQuote.setBankAcct(new String(quoteDetails.getBankAcctNo()));
            if (CommonUtils.isobjectNotNull(quoteDetails.getRollNo())) {
                bancsCreateQuote.setRollNum(new String(quoteDetails.getRollNo()));
            }
        }

        bancsCreateQuote.setCashInType(quoteDetails.getCashInType());
        /* For 1a set this to Y */
        bancsCreateQuote.setSuppressQte(String.valueOf(MPLServiceConstants.YES));
        bancsCreateQuote.setContactCustomer(String.valueOf(quoteDetails.getContactCustomer()));

        bancsCreateQuoteRequest.setBancsCreateQuote(bancsCreateQuote);

        return bancsCreateQuoteRequest;
    }

    public static BancsCreateQuoteRequest mapToBancsCreateQuote(final QuoteDetails quote) {

        BancsCreateQuoteRequest bancsCreateQuoteRequest;
        BancsCreateQuote createQuote;

        bancsCreateQuoteRequest = new BancsCreateQuoteRequest();
        createQuote = new BancsCreateQuote();

        if (MPLConstants.ALPHABET_C == quote.getPaymentMethod().charAt(0)) {
            createQuote.setPaytMethod(MPLConstants.CHEQUE);
        } else {
            createQuote.setPaytMethod(MPLConstants.DIRECT_CREDIT);
            createQuote.setSortCode(quote.getSortCode());
            createQuote.setBankAcct(quote.getBankAcctNo());
            if (CommonUtils.isobjectNotNull(quote.getRollNo())) {
                createQuote.setRollNum(quote.getRollNo());
            }
        }

        createQuote.setCashInType(MPLConstants.UFPLS);
        if (quote.getSmallPotFlag()) {
            createQuote.setCashInType(MPLConstants.SMALL_POT);
        }
        /* For 1a set this to Y */
        createQuote.setSuppressQte(String.valueOf(MPLConstants.YES));
        createQuote.setContactCustomer(String.valueOf(MPLConstants.NO));

        if (quote.getContactCustomerFlag()) {
            createQuote.setContactCustomer(String.valueOf(MPLConstants.YES));
        }

        bancsCreateQuoteRequest.setBancsCreateQuote(createQuote);

        return bancsCreateQuoteRequest;

    }

    /**
     * Method to map mplDocuments to BancsLogCorrespondenceRequest
     *
     * @param bancsUniqueCustNo
     *            - bancsUniqueCustNo
     * @param legacyCustNo
     *            - legacyCustNo
     * @param mplDocuments
     *            - {@link List}
     * @return - BancsLogCorrespondenceDTO object
     * @throws Exception
     *             - {@link Exception]
     */
    public static BancsLogCorrespondenceRequest mapToBancsLogRequest(final String bancsUniqueCustNo,
            final String legacyCustNo, final List<MPLDocumentAudit> mplDocuments) throws Exception {

        BancsLogCorrespondenceRequest bancsLogCorrespondenceRequest;
        BancsLogCorrespondence bancsLogCorrespondence;
        List<BancsLogCorrespondenceDocument> documents;

        bancsLogCorrespondenceRequest = new BancsLogCorrespondenceRequest();
        bancsLogCorrespondence = new BancsLogCorrespondence();
        documents = new ArrayList<BancsLogCorrespondenceDocument>();

        bancsLogCorrespondence.setBancsUniqueCustNo(bancsUniqueCustNo);
        bancsLogCorrespondence.setLegacyCustNo(legacyCustNo);

        for (MPLDocumentAudit mplDocument : mplDocuments) {
            documents.add(mapToDocument(mplDocument));
        }

        bancsLogCorrespondence.setDocumentList(documents);

        bancsLogCorrespondenceRequest.setBancsLogCorrespondence(bancsLogCorrespondence);

        return bancsLogCorrespondenceRequest;
    }

    private static BancsLogCorrespondenceDocument mapToDocument(final MPLDocumentAudit mplDocument) throws Exception {

        BancsLogCorrespondenceDocument document;
        byte[] decodedByte;

        document = new BancsLogCorrespondenceDocument();
        decodedByte = Base64.decodeBase64(mplDocument.getDocumentContents());

        document.setDocName(mplDocument.getDocName());
        document.setDocCode(MPLServiceConstants.OUTBOUND);
        document.setDocContent(Base64.encodeBase64String(generatePdf(decodedByte)));
        document.setDocDate(
                DateUtils.formatTimestamp(mplDocument.getCreatedOn(), MPLServiceConstants.DATE_PATTERN_DDMMYYYY));
        document.setDocMediaType(MPLServiceConstants.MEDIA_TYPE_PDF);

        return document;
    }

    private static byte[] generatePdf(final byte[] decodedPDF) throws Exception {
        byte[] finalPdf = null;
        final ByteArrayOutputStream toBeDecoded = new ByteArrayOutputStream();
        final Document document = new Document();
        final Rectangle one = new Rectangle(419, 595);
        document.setPageSize(one);
        final PdfWriter pdfWriter = PdfWriter.getInstance(document, toBeDecoded);
        final PdfReader reader = new PdfReader(decodedPDF);
        document.open();
        final PdfContentByte contentByte = pdfWriter.getDirectContent();
        final int total = reader.getNumberOfPages() + 1;
        for (int i = 1; i < total; i++) {
            final PdfImportedPage page = pdfWriter.getImportedPage(reader, i);
            document.newPage();
            contentByte.addTemplate(page, 0, 0);
        }
        document.close();
        toBeDecoded.close();
        finalPdf = toBeDecoded.toByteArray();
        return finalPdf;
    }

    /**
     * Method to map FormRequestDTO to MPLPolicyEncashment
     *
     * @param encashRefNo
     *            encashRefNo
     * @param xGUID
     *            - xGUID
     * @param policy
     *            - MPLCustomerPolicy object
     * @return - MPLPolicyEncashment object
     */
    public static MPLPolicyEncashment mapToPolicyEncashmentEntity(final String xGUID, final String encashRefNo,
            final MPLCustomerPolicy policy) {

        MPLPolicyEncashment mplPolicyEncashment;
        mplPolicyEncashment = new MPLPolicyEncashment();
        mplPolicyEncashment.setTransactionId(xGUID);
        mplPolicyEncashment.setEncashmentPolicy(policy);
        mplPolicyEncashment.setEncashRefNum(encashRefNo);
        mplPolicyEncashment.setCreatedBy(MPLConstants.SYSTEM);
        mplPolicyEncashment.setCreatedOn(DateUtils.getCurrentTimestamp());
        return mplPolicyEncashment;
    }

    /**
     * Method to map FormRequestDTO to MPLPolicyEncashment
     *
     * @param xGUID
     *            - xGUID
     * @param policy
     *            - MPLCustomerPolicy object
     * @return - MPLPolicyEncashment object
     */
    public static MPLCustomerUpdateStatus mapToMPLCustomerUpdateStatusEntity(final long policyId,
            final MPLCustomer mplCustomer) {

        MPLCustomerUpdateStatus mplCustomerUpdateStatus;
        mplCustomerUpdateStatus = new MPLCustomerUpdateStatus();

        mplCustomerUpdateStatus.setAttempts(0);
        mplCustomerUpdateStatus.setUpdatedCustomer(mplCustomer);
        mplCustomerUpdateStatus.setStatus(' ');
        mplCustomerUpdateStatus.setPolicyId(policyId);
        mplCustomerUpdateStatus.setTimestamp(DateUtils.getCurrentTimestamp());
        mplCustomerUpdateStatus.setCustomerUpdateAttempts(new HashSet<MPLCustomerUpdateAttempt>());
        return mplCustomerUpdateStatus;
    }

    /**
     * Method to map QuoteDetails to MPLQuoteDetails
     *
     * @param quote
     *            - QuoteDetails object
     * @param policy
     *            - MPLCustomerPolicy object
     * @return - MPLQuoteDetails object
     */
    public static MPLQuoteDetails mapToQuoteEntity(final QuoteDetails quote,
            final MPLCustomerPolicy policy, final BancsUpdateAttemptDTO bancsQuoteAttempt) {

        MPLQuoteDetails quoteDetails;
        quoteDetails = new MPLQuoteDetails();

        quoteDetails.setPaymentMethod(quote.getPaymentMethod().charAt(0));

        if (CommonUtils.stringNotBlankCheck(quote.getBankAcctNo())) {
            quoteDetails.setSortCode(quote.getSortCode().getBytes());
            quoteDetails.setBankAcctNo(quote.getBankAcctNo().getBytes());

            if (CommonUtils.stringNotBlankCheck(quote.getRollNo())) {
                quoteDetails.setRollNo(quote.getRollNo().getBytes());
            }
        }

        quoteDetails.setContactCustomer(MPLConstants.NO);
        quoteDetails.setCashInType(MPLConstants.UFPLS);
        quoteDetails.setCreatedOn(DateUtils.getCurrentTimestamp());
        quoteDetails.setCreatedBy(MPLConstants.SYSTEM);

        quoteDetails.setQuotePolicy(policy);

        if (quote.getContactCustomerFlag()) {
            quoteDetails.setContactCustomer(MPLConstants.YES);
        }

        if (quote.getSmallPotFlag()) {
            quoteDetails.setCashInType(MPLConstants.SMALL_POT);
        }

        quoteDetails.setQuoteUpdateStatus(mapToQuoteUpdateStatusEntity(quoteDetails, quote, bancsQuoteAttempt));

        return quoteDetails;
    }

    private static MPLQuoteUpdateStatus mapToQuoteUpdateStatusEntity(final MPLQuoteDetails quoteDetails,
            final QuoteDetails quote, final BancsUpdateAttemptDTO bancsQuoteAttempt) {

        MPLQuoteUpdateStatus quoteUpdateStatus;
        quoteUpdateStatus = new MPLQuoteUpdateStatus();

        quoteUpdateStatus.setAttempt(1);
        quoteUpdateStatus.setStatus(bancsQuoteAttempt.getUpdateStatus());
        quoteUpdateStatus.setTimestamp(DateUtils.formatDateTime(bancsQuoteAttempt.getUpdateTmst()));

        quoteUpdateStatus.setUpdatedQuote(quoteDetails);

        quoteUpdateStatus.setQuoteUpdateAttempts(new HashSet<MPLQuoteUpdateAttempt>(
                Collections.singletonList(mapToQuoteAttemptEntity(quoteUpdateStatus, bancsQuoteAttempt))));

        return quoteUpdateStatus;
    }

    public static MPLQuoteUpdateAttempt mapToQuoteAttemptEntity(final MPLQuoteUpdateStatus quoteUpdateStatus,
            final BancsUpdateAttemptDTO bancsQuoteAttempt) {

        MPLQuoteUpdateAttempt quoteUpdateAttempt;
        quoteUpdateAttempt = new MPLQuoteUpdateAttempt();

        quoteUpdateAttempt.setSentFlag('Y');
        quoteUpdateAttempt.setSentTmst(DateUtils.formatDateTime(bancsQuoteAttempt.getUpdateTmst()));
        quoteUpdateAttempt.setAckFlag(bancsQuoteAttempt.getAckFlg());

        if (CommonUtils.isObjectNotNull(bancsQuoteAttempt.getAckTmst())) {
            quoteUpdateAttempt.setAckTmst(DateUtils.formatDateTime(bancsQuoteAttempt.getAckTmst()));
        }
        quoteUpdateAttempt.setFailureReason(bancsQuoteAttempt.getFailureReason());

        quoteUpdateAttempt.setQuoteUpdateStatus(quoteUpdateStatus);

        return quoteUpdateAttempt;
    }

}
