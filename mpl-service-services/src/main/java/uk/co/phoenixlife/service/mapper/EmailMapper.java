package uk.co.phoenixlife.service.mapper;

import java.time.LocalDateTime;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import freemarker.template.Configuration;
import freemarker.template.Template;
import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.dao.entity.MPLCustomer;
import uk.co.phoenixlife.service.dao.entity.MPLEmailAttempt;
import uk.co.phoenixlife.service.dao.entity.MPLEmailStatus;
import uk.co.phoenixlife.service.dto.MPLException;
import uk.co.phoenixlife.service.dto.email.EmailRequestDTO;
import uk.co.phoenixlife.service.utils.CommonUtils;
import uk.co.phoenixlife.service.utils.DateUtils;

/**
 * EmailMapper.java
 */
public final class EmailMapper {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailMapper.class);

    /**
     * Constructor.
     */
    private EmailMapper() {
        super();
    }

    /**
     * Method to map email request
     *
     * @param freeMarkerConfiguration
     *            - {@link Configuration}
     * @param emailFrom
     *            - from email address
     * @param emailSubject
     *            - email subject
     * @param emailTo
     *            - to email address
     * @return - EmailRequestDTO object
     * @throws Exception
     *             - {@link Exception}
     */
    public static EmailRequestDTO mapEmailRequest(final Configuration freeMarkerConfiguration, final String emailFrom,
            final String emailSubject, final String emailTo) throws Exception {

        String emailBody;
        EmailRequestDTO emailRequest;

        emailBody = mapMailBody(freeMarkerConfiguration);

        if (CommonUtils.stringNotBlankCheck(emailTo) && CommonUtils.stringNotBlankCheck(emailBody)) {

            emailRequest = new EmailRequestDTO();

            emailRequest.setFrom(emailFrom);
            emailRequest.setSubject(emailSubject);
            emailRequest.setTo(emailTo);
            emailRequest.setBody(emailBody);
        } else {
            throw new MPLException("Mail request validation failed..");
        }

        return emailRequest;
    }

    private static String mapMailBody(final Configuration freeMarkerConfiguration) throws Exception {

        LOGGER.debug("Mapping email body for encashment mail.");

        Template emailTemplate;
        String emailBody;

        emailTemplate = freeMarkerConfiguration.getTemplate(MPLConstants.ENCASHMENTEMAILFTL);
        emailBody = FreeMarkerTemplateUtils.processTemplateIntoString(emailTemplate, null);

        return emailBody;
    }

    /**
     * Method to map MPLEmailStatus to mapToEmailStatusEntity
     *
     * @param mplPolicyEncashment
     *            mplPolicyEncashment
     * @param mplCustomer
     *            - mplCustomer
     * @param apiResult
     *            - apiResult object
     * @return - MPLEmailStatus object
     */
    public static MPLEmailStatus mapToEmailStatusEntity(final Long encashmentId,
            final MPLCustomer mplCustomer, final String encashRefNo) {

        MPLEmailStatus emailStatus;
        emailStatus = new MPLEmailStatus();

        emailStatus.setEmailCustomer(mplCustomer);
        emailStatus.setEncashmentId(encashmentId);
        emailStatus.setEmailType("ENC");

        if (CommonUtils.isEmpty(encashRefNo)) {
            // CreateQuote returned 500
            emailStatus.setStatus('X');
        } else {
            // CreateQuote is SUCCESS
            emailStatus.setStatus(' ');
        }
        emailStatus.setTimestamp(DateUtils.getCurrentTimestamp());

        return emailStatus;
    }

    /**
     * Method to map updated email status
     *
     * @param emailStatus
     *            - MPLEmailStatus object
     * @param status
     *            - 'S: success | F: failure'
     * @param sentFlag
     *            - 'Y | N'
     * @param sentTmst
     *            - {@link LocalDateTime}
     * @param failedTmst
     *            - {@link LocalDateTime}
     * @param failureReason
     *            - failureReason
     */
    public static void mapUpdatedEmailStatusAndAttempts(final MPLEmailStatus emailStatus, final char status,
            final char sentFlag, final LocalDateTime sentTmst, final LocalDateTime failedTmst,
            final String failureReason) {

        MPLEmailAttempt emailAttempt;
        Set<MPLEmailAttempt> emailAttempts;

        emailAttempt = mapToEmailAttempt(sentFlag, sentTmst, failedTmst, failureReason);
        emailAttempt.setEmailStatus(emailStatus);

        emailAttempts = emailStatus.getEmailAttempts();
        emailAttempts.add(emailAttempt);

        emailStatus.setEmailAttempts(emailAttempts);
        emailStatus.setAttempts(emailStatus.getAttempts() + 1);
        emailStatus.setStatus(status);
        emailStatus.setTimestamp(DateUtils.getCurrentTimestamp());

    }

    private static MPLEmailAttempt mapToEmailAttempt(final char sentFlag, final LocalDateTime sentTmst,
            final LocalDateTime failedTmst, final String failureReason) {

        MPLEmailAttempt emailAttempt;
        emailAttempt = new MPLEmailAttempt();

        emailAttempt.setSentFlag(sentFlag);
        emailAttempt.setSentTmst(DateUtils.formatDateTime(sentTmst));

        if (CommonUtils.isObjectNotNull(failedTmst)) {
            emailAttempt.setFailedTmst(DateUtils.formatDateTime(failedTmst));
        }

        emailAttempt.setFailureReason(failureReason);

        return emailAttempt;
    }

}
