package uk.co.phoenixlife.service.mapper;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import uk.co.phoenixlife.service.dao.entity.MPLCustomer;
import uk.co.phoenixlife.service.dao.entity.MPLCustomerPolicy;
import uk.co.phoenixlife.service.dto.activation.CustomerActivationDTO;
import uk.co.phoenixlife.service.dto.dashboard.PolicyDTO;

@Component
public class MPLCustomerMapper {

    private static final Logger LOGGER = LoggerFactory.getLogger(MPLCustomerMapper.class);

    public MPLCustomer mapCustomer(final CustomerActivationDTO customerActivationDTO,
            final List<PolicyDTO> identifiedPolicyDTOList) {

        Set<MPLCustomerPolicy> customerPolicies;
        customerPolicies = new HashSet<MPLCustomerPolicy>();

        MPLCustomer mPLCustomer = new MPLCustomer();
        mPLCustomer.setBancsCustNum(customerActivationDTO.getIdentifiedBancsUniqueCustomerNumber());
        mPLCustomer.setLegacyCustNum(customerActivationDTO.getLegacyCustNo());
        mPLCustomer.setUserName(customerActivationDTO.getUserName());
        mPLCustomer.setCreatedBy("SYSTEM");
        mPLCustomer.setCreatedOn(Timestamp.valueOf(LocalDateTime.now()));
        mPLCustomer.setFirstName(customerActivationDTO.getFirstName().getBytes());
        mPLCustomer.setLastName(customerActivationDTO.getSurName().getBytes());
        mPLCustomer.setPrimaryPhNo(customerActivationDTO.getPhoneNumber().getBytes());
        mPLCustomer.setPrimaryPhType(customerActivationDTO.getPhoneType());
        mPLCustomer.setTitle(customerActivationDTO.getTitle().getBytes());
        mPLCustomer.setEmailAddr(customerActivationDTO.getEmail().getBytes());
        identifiedPolicyDTOList.forEach(pol -> customerPolicies.add(mapCustomerPolicy(mPLCustomer, pol)));

        mPLCustomer.setCustomerPolicies(customerPolicies);
        LOGGER.debug("Customer Details Mapped for Unique Customer Number {} ",
                customerActivationDTO.getIdentifiedBancsUniqueCustomerNumber());
        return mPLCustomer;
    }

    public MPLCustomerPolicy mapCustomerPolicy(final MPLCustomer cust, final PolicyDTO policyDTO) {

        MPLCustomerPolicy custPol;
        custPol = new MPLCustomerPolicy();
        custPol.setBancsPolNum(policyDTO.getBancsPolNum());
        custPol.setLegacyPolNum(policyDTO.getPolNum());
        custPol.setPolicyCustomer(cust);
        custPol.setCreatedBy("SYSTEM");
        custPol.setCreatedOn(Timestamp.valueOf(LocalDateTime.now()));

        return custPol;
    }

    public MPLCustomer mapUpatedCustomer(final MPLCustomer mPLCustomer, final CustomerActivationDTO customerActivationDTO) {
        mPLCustomer.setUserName(customerActivationDTO.getUserName());
        mPLCustomer.setLastUpdatedBy("SYSTEM");
        mPLCustomer.setLastUpdatedOn(Timestamp.valueOf(LocalDateTime.now()));
        mPLCustomer.setTitle(customerActivationDTO.getTitle().getBytes());
        mPLCustomer.setFirstName(customerActivationDTO.getFirstName().getBytes());
        mPLCustomer.setLastName(customerActivationDTO.getSurName().getBytes());
        mPLCustomer.setPrimaryPhNo(customerActivationDTO.getPhoneNumber().getBytes());
        mPLCustomer.setPrimaryPhType(customerActivationDTO.getPhoneType());
        mPLCustomer.setEmailAddr(customerActivationDTO.getEmail().getBytes());
        return mPLCustomer;
    }
}
