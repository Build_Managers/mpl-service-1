package uk.co.phoenixlife.service.mapper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfArray;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.Pipeline;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.AbstractImageProvider;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;

import freemarker.template.Configuration;
import freemarker.template.Template;
import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.dao.entity.MPLCustomerPolicy;
import uk.co.phoenixlife.service.dao.entity.MPLDocumentAudit;
import uk.co.phoenixlife.service.dao.entity.MPLDocumentUpdateStatus;
import uk.co.phoenixlife.service.dto.pdf.FormRequestDTO;
import uk.co.phoenixlife.service.dto.policy.BenefitDTO;
import uk.co.phoenixlife.service.dto.policy.DashboardDTO;
import uk.co.phoenixlife.service.dto.policy.FinalDashboardDTO;
import uk.co.phoenixlife.service.dto.policy.FundDTO;
import uk.co.phoenixlife.service.dto.policy.PartyPolicyDTO;
import uk.co.phoenixlife.service.utils.CommonUtils;
import uk.co.phoenixlife.service.utils.DateUtils;

/**
 * PdfMapper
 * @author 1240916
 *
 */
public final class PdfMapper {
    private static final String RETIREMENTPACK = "RP";
    private static final String DATE = "date";
    private static final String POUND = "£";
    private static final String HOUSENAME = "houseName";
    private static final String ADDRESS1 = "address1";
    private static final String CITYTOWN = "cityTown";
    private static final String COUNTY = "county";
    /**
     * Constructor
     */
    private PdfMapper() {
        super();
    }
     /**
     * mapToDocumentEntity
     * @param formData - formData
     * @param mplPolicy - mplPolicy
     * @param freeMarkerConfiguration - freeMarkerConfiguration
     * @param context - context
     * @return document
     * @throws Exception - Exception
     */
    public static MPLDocumentAudit mapToDocumentEntity(final FormRequestDTO formData, final MPLCustomerPolicy mplPolicy,
            final Configuration freeMarkerConfiguration, final ServletContext context) throws Exception {
        MPLDocumentAudit document;
        document = new MPLDocumentAudit();
        document.setDocType(formData.getDocType());
        document.setDocName(formData.getDocumentName());
        document.setDocumentContents(getPdfContent(formData, freeMarkerConfiguration, context));
        document.setCreatedOn(Timestamp.valueOf(LocalDateTime.now()));
        document.setCreatedBy(MPLConstants.SYSTEM);
        document.setDocumentPolicy(mplPolicy);
        
        document.setDocumentUpdateStatus(mapToDocumentUpdateStatus(document,formData.getDocType()));
        return document;
    }
    
    private static MPLDocumentUpdateStatus mapToDocumentUpdateStatus(MPLDocumentAudit document, String docType){
    
    	MPLDocumentUpdateStatus docStatus;
    	docStatus = new MPLDocumentUpdateStatus();
    	
        if (RETIREMENTPACK.equalsIgnoreCase(docType)) {
        	docStatus.setStatus('X');
        } else {
        	docStatus.setStatus(' ');
        }
    	
    	
    	docStatus.setTimestamp(DateUtils.getCurrentTimestamp());
    	docStatus.setUpdatedDocument(document);
    	//docStatus.setDocumentUpdateAttempts(pDocumentUpdateAttempts);
    	
    	return docStatus;
    	
    }

    private static byte[] getPdfContent(final FormRequestDTO formData, final Configuration freeMarkerConfiguration,
            final ServletContext context) throws Exception {
        byte[] byteArray;
        String encodedPdf = null;
        String pdfString;
        Template template;
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
        final StringWriter writer = new StringWriter();
        template = freeMarkerConfiguration.getTemplate(getTemplateForPdfType(formData.getDocType()));
        template.process(mapPdfData(formData), writer);
        writer.flush();
        pdfString = writer.toString();
        if (RETIREMENTPACK.equalsIgnoreCase(formData.getDocType())) {
            byteArray = createRetirementPackPdf(baos, pdfString, context);
        } else {
            byteArray = createApplicationFormPDF(pdfString, baos, baos1, context);
        }
        final ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
        final PdfReader reader1 = new PdfReader(byteArray);
        final PdfStamper stamper = new PdfStamper(reader1, baos2);
        
        String testBaos1 = new String(byteArray);
        String testBaos2 = new String(baos2.toByteArray());
        setPdfPageSize(reader1, stamper);
        stamper.close();
        reader1.close();
        baos2.close();
        encodedPdf = Base64.encodeBase64String(baos2.toByteArray());
        return encodedPdf.getBytes();
    }

    private static void setPdfPageSize(final PdfReader reader1, final PdfStamper stamper) {
        final int numberOfPages = reader1.getNumberOfPages();
        PdfDictionary page1;
        final PdfArray crop = new PdfArray();
        PdfArray media;
        for (int count = 1; count <= numberOfPages; count++) {
            page1 = reader1.getPageN(count);
            media = page1.getAsArray(PdfName.CROPBOX);
            if (media == null) {
                media = page1.getAsArray(PdfName.MEDIABOX);
            }
            crop.add(instantiatingObject());
            crop.add(instantiatingObject());
            crop.add(instantiatingObjectWithMedia(media));
            crop.add(instantiatingObjectWithMediaThree(media));
            page1.put(PdfName.MEDIABOX, crop);
            page1.put(PdfName.CROPBOX, crop);
            stamper.getUnderContent(count).setLiteral("\nq 0.69 0 0 0.69 0 0 cm\nq\n");
            stamper.getOverContent(count).setLiteral("\nQ\nQ\n");
        }
    }
    private static PdfNumber instantiatingObject() {
        return new PdfNumber(0);
    }
    private static PdfNumber instantiatingObjectWithMedia(final PdfArray media) {
        return new PdfNumber(media.getAsNumber(2).floatValue() / MPLConstants.ONEPOINTONE);
    }
    private static PdfNumber instantiatingObjectWithMediaThree(final PdfArray media) {
        return new PdfNumber(media.getAsNumber(MPLConstants.THREE).floatValue() / MPLConstants.ONEPOINTONE);
    }
    private static byte[] createRetirementPackPdf(final ByteArrayOutputStream baos, final String pdfString, final ServletContext context)
            throws DocumentException, IOException {
        final Document document = new Document();
        final PdfWriter pdfwriter = PdfWriter.getInstance(document, baos);
        document.open();
        final HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
        htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
        htmlContext.setImageProvider(new AbstractImageProvider() {
            @Override
            public String getImageRootPath() {
                return context.getRealPath("/WEB-INF/resources/images");
            }
        });

        final CSSResolver cssResolver = XMLWorkerHelper.getInstance().getDefaultCssResolver(true);
        final Pipeline<?> pipeline = new CssResolverPipeline(cssResolver,
                new HtmlPipeline(htmlContext, new PdfWriterPipeline(document, pdfwriter)));
        final XMLWorker worker = new XMLWorker(pipeline, true);
        final XMLParser parser = new XMLParser(worker);
        parser.parse(new StringReader(pdfString));
        document.close();
        return baos.toByteArray();
    }

    private static byte[] createApplicationFormPDF(final String pdfString, final ByteArrayOutputStream baos,
            final ByteArrayOutputStream baos1, final ServletContext context) throws DocumentException, IOException {
        final ITextRenderer renderer = new ITextRenderer();
        renderer.setDocumentFromString(pdfString);
        renderer.layout();
        renderer.createPDF(baos);
        renderer.finishPDF();
        baos.close();

        final Document document = new Document();
        final PdfWriter pdfWriter = PdfWriter.getInstance(document, baos1);
        //final HeaderFooterPageEvent event = new HeaderFooterPageEvent();
        ///.setLogoPath(context.getRealPath("/WEB-INF/resources/images/logo.jpg"));
        //pdfWriter.setPageEvent(event);
        final PdfReader reader = new PdfReader(baos.toByteArray());
        document.open();
        final PdfContentByte contentByte = pdfWriter.getDirectContent();
        final int total = reader.getNumberOfPages() + 1;
        for (int count = 1; count < total; count++) {
            final PdfImportedPage page = pdfWriter.getImportedPage(reader, count);
            document.newPage();
            contentByte.addTemplate(page, 0, 0);
        }
        document.close();
        baos1.close();
        return baos1.toByteArray();
    }

    @SuppressWarnings("unchecked")
    private static Map<Object, Object> mapPdfData(final FormRequestDTO formData) {

        Map<Object, Object> formMap;
        formMap = (Map<Object, Object>) CommonUtils.getNullObject();

        /*if (MPLConstants.APPLICATION_FORM.equalsIgnoreCase(formData.getDocType())) {
            formMap = mapValuesForApplicationFormPdf(formData);
        } else {
            formMap = mapValuesForApplicationFormPdf(formData);
        }*/
        formMap = mapFormValuesToPdf(formData);
        return formMap;
    }

    private static Map<Object, Object> mapFormValuesToPdf(final FormRequestDTO formData) {

        Map<Object, Object> pdfMap;
        FinalDashboardDTO finalDashboard;
        pdfMap = new HashMap<Object, Object>();
        finalDashboard = formData.getFinalDashboard();
        pdfMap.put("policyNumber", formData.getPolNum());
        if (formData.getDocType().equalsIgnoreCase(MPLConstants.APPLICATION_FORM)) {
            insertApplicationFormValueInMap(pdfMap, formData, finalDashboard);
        } else {
            insertRetirementPackPdfValueInMap(formData, finalDashboard, pdfMap);
        }
        return pdfMap;
    }

    private static void insertRetirementPackPdfValueInMap(final FormRequestDTO formData, final FinalDashboardDTO finalDashboard,
            final Map<Object, Object> pdfMap) {
    	DashboardDTO eligible = null;
        for (DashboardDTO dashboardDTO : finalDashboard.getDashboardPolicyList()) {
            if (dashboardDTO.isEligible()) {
                eligible = dashboardDTO;
            }
        }
        pdfMap.put("title", finalDashboard.getTitle());
        pdfMap.put("firstName", CommonUtils.replaceEscapeCharacters(finalDashboard.getForename()));
        pdfMap.put("lastName", CommonUtils.replaceEscapeCharacters(finalDashboard.getSurname()));
        final StringBuffer planHolderName = new StringBuffer();
        planHolderName.append(finalDashboard.getTitle()).append(' ')
                .append(CommonUtils.replaceEscapeCharacters(finalDashboard.getForename())).append(' ')
                .append(CommonUtils.replaceEscapeCharacters(finalDashboard.getSurname()));
        pdfMap.put("planHolderName", CommonUtils.replaceEscapeCharacters(new String(planHolderName)));
        //pdfMap.put("pensionSavings", formData.getEstimatedPensionFund());
        pdfMap.put("pensionSavings", eligible.getTotValue());
        pdfMap.put(DATE, DateUtils.formatDate(DateUtils.getCurrentDate(), MPLConstants.DATE_FORMAT));
        pdfMap.put("pound", POUND);
        
        //pdfMap.put("beforeDeductionFundValue", formData.getTotValue());
        pdfMap.put("beforeDeductionFundValue", (eligible.getTotValue() + eligible.getPenValue() + eligible.getcUCharge() + eligible.getMvaValue()));
        insertValuesConditionBasis(formData, pdfMap, finalDashboard);
        insertValuesConditionBasisContinues(finalDashboard, pdfMap);
        if (finalDashboard.getCounty() != null) {
            pdfMap.put(COUNTY, CommonUtils.replaceEscapeCharacters(finalDashboard.getCounty()));
        }
        if (finalDashboard.getCountry() != null) {
            pdfMap.put("country", CommonUtils.replaceEscapeCharacters(finalDashboard.getCountry()));
        }
        if (finalDashboard.getPostCode() != null) {
            pdfMap.put("postcode", finalDashboard.getPostCode());
        }
        setPartyPolicyDTOValueInMap(formData.getPartyPolicyDTO(), pdfMap);
    }

    private static void insertValuesConditionBasisContinues(final FinalDashboardDTO finalDashboard, final Map<Object, Object> pdfMap) {
        if (finalDashboard.getHouseNum() != null) {
            pdfMap.put("houseNumber", CommonUtils.replaceEscapeCharacters(finalDashboard.getHouseNum()));
        }
        if (finalDashboard.getAddress1() != null) {
            pdfMap.put(ADDRESS1, CommonUtils.replaceEscapeCharacters(finalDashboard.getAddress1()));
        }
        if (finalDashboard.getAddress2() != null) {
            pdfMap.put("address2", CommonUtils.replaceEscapeCharacters(finalDashboard.getAddress2()));
        }
        if (finalDashboard.getCityTown() != null) {
            pdfMap.put(CITYTOWN, CommonUtils.replaceEscapeCharacters(finalDashboard.getCityTown()));
        }
    }
    private static void insertValuesConditionBasis(final FormRequestDTO formData, final Map<Object, Object> pdfMap,
            final FinalDashboardDTO finalDashboard) {
        if (formData.getMvaValue() + formData.getPenaltyValue() > 0) {
            pdfMap.put("deductions", MPLConstants.YES);
        }
        if (formData.getMvrValue().equalsIgnoreCase("Y")) {
            pdfMap.put("MVA", formData.getMvaValue());
        }
        if (formData.getPenaltyValue() > 0) {
            pdfMap.put("penaltyValue", formData.getPenaltyValue());
        }
        if (finalDashboard.getHouseName() != null) {
            pdfMap.put(HOUSENAME, CommonUtils.replaceEscapeCharacters(finalDashboard.getHouseName()));
        }
    }
    private static void insertApplicationFormValueInMap(final Map<Object, Object> pdfMap, final FormRequestDTO formData,
            final FinalDashboardDTO finalDashboard) {
        pdfMap.put("nino", formData.getBankDetailsNinoDTO().getNino());
        pdfMap.put(DATE, DateUtils.formatDate(LocalDate.now(), MPLConstants.DATE_PATTERN_D_MMM_YYYY));
        pdfMap.put("name", new StringBuffer().append(finalDashboard.getForename()).append(" ")
                .append(finalDashboard.getSurname()).toString());
        pdfMap.put("dob", formData.getDobWithSuffix());
        pdfMap.put("email", CommonUtils.replaceEscapeCharacters(finalDashboard.getEmail()));
        pdfMap.put(HOUSENAME, CommonUtils.replaceEscapeCharacters(finalDashboard.getHouseName()));
        pdfMap.put("houseNum", CommonUtils.replaceEscapeCharacters(finalDashboard.getHouseNum()));
        pdfMap.put(ADDRESS1, CommonUtils.replaceEscapeCharacters(finalDashboard.getAddress1()));
        pdfMap.put(CITYTOWN, CommonUtils.replaceEscapeCharacters(finalDashboard.getCityTown()));
        pdfMap.put(COUNTY, CommonUtils.replaceEscapeCharacters(finalDashboard.getCounty()));
        pdfMap.put("postCode", finalDashboard.getPostCode());
        pdfMap.put("smallpot", formData.getIsSmallPot());
        pdfMap.put("contact", formData.getContactCustomer());
        pdfMap.put("pensionWise", formData.getPensionGuidance());
        pdfMap.put("financeAdv", formData.getPensionAdvise());
        pdfMap.put("estmtPolicyValue", formData.getTotValue());
        insertionInMapContinues(pdfMap, formData);
    }

    private static void insertionInMapContinues(final Map<Object, Object> pdfMap, final FormRequestDTO formData) {
        pdfMap.put("taxFreeAmount", formData.getpCLS());
        pdfMap.put("taxDeducted", formData.getTax());
        pdfMap.put("estmtAmtPayble", formData.getAmountPayable());
        pdfMap.put("mvrApplies", formData.getMvrValue());
        pdfMap.put("earlyExistsApplies", formData.getEecValue());
        pdfMap.put("amtDropBy5", formData.getContactCustomer());
        pdfMap.put("bankAccountNumber", formData.getBankDetailsNinoDTO().getAccountNumber());
        pdfMap.put("bankAccHolderName", formData.getBankDetailsNinoDTO().getAccountHolderName());
        pdfMap.put("buildingSocietyName", formData.getBankDetailsNinoDTO().getBankOrBuildingSocietyName());
        pdfMap.put("rollNumber", formData.getBankDetailsNinoDTO().getRollNumber());
        pdfMap.put("bankSortCode", formData.getBankDetailsNinoDTO().getSortCode());
        pdfMap.put("primaryContactNum", formData.getPrimaryConactNum());
        pdfMap.put("secContactNum", formData.getSecConactNum());
        pdfMap.put("addContactNum", formData.getAdditonalConactNum());
        pdfMap.put("poundSign", POUND);
    }
    private static void setPartyPolicyDTOValueInMap(final PartyPolicyDTO partyPolicyDTO, final Map<Object, Object> pdfMap) {
        final boolean fundNameFlag = checkFundNameCondition(partyPolicyDTO);
        if (fundNameFlag) {
            pdfMap.put("fundName", MPLConstants.TRUE);
        }
        final boolean lifeCoverFlag = checkLifeCoverCondition(partyPolicyDTO);
        if (lifeCoverFlag) {
            pdfMap.put("lifeCover", MPLConstants.TRUE);
        }
        final boolean regularContributionsFlag = checkRegularContributionsCondition(partyPolicyDTO);
        if (regularContributionsFlag) {
            pdfMap.put("regularContributions", MPLConstants.TRUE);
        }
    }

    private static boolean checkRegularContributionsCondition(final PartyPolicyDTO partyPolicyDTO) {
        boolean regularContributionsFlag = false;
        if (CommonUtils.isValidList(partyPolicyDTO.getListOfBenefits())) {
            for (final BenefitDTO benefitDTO : partyPolicyDTO.getListOfBenefits()) {
                if (StringUtils.isNotBlank(benefitDTO.getBenefitName())
                        && !(benefitDTO.getBenefitName().equalsIgnoreCase(MPLConstants.EME_LC)
                                || benefitDTO.getBenefitName().equalsIgnoreCase(MPLConstants.EMR_LC))
                        && partyPolicyDTO.getPremiumDueDate() != null) {
                    regularContributionsFlag = true;
                    break;
                }
            }
        }
        return regularContributionsFlag;
    }

    private static boolean checkLifeCoverCondition(final PartyPolicyDTO partyPolicyDTO) {
        boolean lifeCoverFlag = false;
        if (CommonUtils.isValidList(partyPolicyDTO.getListOfBenefits())) {
            for (final BenefitDTO benefitDTO : partyPolicyDTO.getListOfBenefits()) {
                if (StringUtils.isNotBlank(benefitDTO.getBenefitName())
                        && (benefitDTO.getBenefitName().equalsIgnoreCase(MPLConstants.EME_LC)
                                || benefitDTO.getBenefitName().equalsIgnoreCase(MPLConstants.EMR_LC))
                        && partyPolicyDTO.getPremiumDueDate() != null) {
                    lifeCoverFlag = true;
                    break;
                }
            }
        }
        return lifeCoverFlag;
    }

    private static boolean checkFundNameCondition(final PartyPolicyDTO partyPolicyDTO) {
        boolean fundNameFlag = false;
        if (CommonUtils.isValidList(partyPolicyDTO.getListOfFunds())) {
            for (final FundDTO fundDTO : partyPolicyDTO.getListOfFunds()) {
                if (StringUtils.isNotBlank(fundDTO.getFundName())
                        && fundDTO.getFundName().equalsIgnoreCase("NPI Pens UWP Type 1")) {
                    fundNameFlag = true;
                    break;
                }
            }
        }
        return fundNameFlag;
    }

    private static String getTemplateForPdfType(final String docType) {

        String template;
        template = CommonUtils.getEmptyString();

        if (MPLConstants.APPLICATION_FORM.equalsIgnoreCase(docType)) {
            template = MPLConstants.APPLICATION_FORM_PDF_TEMPLATE;
        } else {
            template = MPLConstants.RETIREMENTPACK_PDF_TEMPLATE;
        }
        return template;
    }
}
