/*
 * UpdateAttemptsMapper.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.mapper;

import java.util.Set;

import uk.co.phoenixlife.security.service.utils.CommonUtils;
import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dao.entity.MPLCustomer;
import uk.co.phoenixlife.service.dao.entity.MPLCustomerUpdateAttempt;
import uk.co.phoenixlife.service.dao.entity.MPLCustomerUpdateStatus;
import uk.co.phoenixlife.service.dao.entity.MPLDocumentUpdateAttempt;
import uk.co.phoenixlife.service.dao.entity.MPLDocumentUpdateStatus;
import uk.co.phoenixlife.service.dao.entity.MPLQuoteUpdateAttempt;
import uk.co.phoenixlife.service.dao.entity.MPLQuoteUpdateStatus;
import uk.co.phoenixlife.service.dto.bancspost.BancsUpdateAttemptDTO;
import uk.co.phoenixlife.service.utils.DateUtils;

/**
 * UpdateAttemptsMapper.java
 */
public final class UpdateAttemptsMapper {

    /**
     * Constructor
     */
    private UpdateAttemptsMapper() {
        super();
    }

    /**
     * Method to map updated MPLCustomerUpdateStatus
     *
     * @param updateStatus
     *            - MPLCustomerUpdateStatus object
     * @param updateAck
     *            - BancsUpdateAttemptDTO object
     * @return - updated MPLCustomerUpdateStatus object
     */
    public static void mapUpdatedCustUpdtStatusAndAttempts(final MPLCustomerUpdateStatus updateStatus,
            final BancsUpdateAttemptDTO updateAck) {

        MPLCustomerUpdateAttempt customerUpdateAttempt;
        Set<MPLCustomerUpdateAttempt> customerUpdateAttempts;

        customerUpdateAttempt = mapToCustomerUpdateAttempt(updateAck);
        customerUpdateAttempt.setCustomerUpdateStatus(updateStatus);

        customerUpdateAttempts = updateStatus.getCustomerUpdateAttempts();
        customerUpdateAttempts.add(customerUpdateAttempt);

        updateStatus.setCustomerUpdateAttempts(customerUpdateAttempts);
        updateStatus.setAttempts(updateStatus.getAttempts() + 1);
        updateStatus.setStatus(updateAck.getUpdateStatus());
        updateStatus.setTimestamp(DateUtils.getCurrentTimestamp());

    }

    private static MPLCustomerUpdateAttempt mapToCustomerUpdateAttempt(final BancsUpdateAttemptDTO updateAck) {

        MPLCustomerUpdateAttempt customerUpdateAttempt;
        customerUpdateAttempt = new MPLCustomerUpdateAttempt();

        customerUpdateAttempt.setSentFlag(MPLServiceConstants.YES);
        customerUpdateAttempt.setSentTmst(DateUtils.formatDateTime(updateAck.getUpdateTmst()));
        customerUpdateAttempt.setAckFlag(updateAck.getAckFlg());

        if (CommonUtils.isObjectNotNull(updateAck.getAckTmst())) {
            customerUpdateAttempt.setAckTmst(DateUtils.formatDateTime(updateAck.getAckTmst()));
        }

        customerUpdateAttempt.setFailureReason(updateAck.getFailureReason());

        return customerUpdateAttempt;
    }

    /**
     * Method to map updated MPLQuoteUpdateStatus
     *
     * @param updateStatus
     *            - MPLQuoteUpdateStatus object
     * @param updateAck
     *            - BancsUpdateAttemptDTO object
     * @return - updated MPLQuoteUpdateStatus object
     */
    public static void mapUpdatedQteUpdtStatusAndAttempts(final MPLQuoteUpdateStatus updateStatus,
            final BancsUpdateAttemptDTO updateAck) {

        MPLQuoteUpdateAttempt quoteUpdateAttempt;
        Set<MPLQuoteUpdateAttempt> quoteUpdateAttempts;

        quoteUpdateAttempt = mapToQuoteUpdateAttempt(updateAck);
        quoteUpdateAttempt.setQuoteUpdateStatus(updateStatus);

        quoteUpdateAttempts = updateStatus.getQuoteUpdateAttempts();
        quoteUpdateAttempts.add(quoteUpdateAttempt);

        updateStatus.setQuoteUpdateAttempts(quoteUpdateAttempts);
        updateStatus.setAttempt(updateStatus.getAttempts() + 1);
        updateStatus.setStatus(updateAck.getUpdateStatus());
        updateStatus.setTimestamp(DateUtils.getCurrentTimestamp());

    }

    private static MPLQuoteUpdateAttempt mapToQuoteUpdateAttempt(final BancsUpdateAttemptDTO updateAck) {

        MPLQuoteUpdateAttempt quoteUpdateAttempt;
        quoteUpdateAttempt = new MPLQuoteUpdateAttempt();

        quoteUpdateAttempt.setSentFlag(MPLServiceConstants.YES);
        quoteUpdateAttempt.setSentTmst(DateUtils.formatDateTime(updateAck.getUpdateTmst()));
        quoteUpdateAttempt.setAckFlag(updateAck.getAckFlg());

        if (CommonUtils.isObjectNotNull(updateAck.getAckTmst())) {
            quoteUpdateAttempt.setAckTmst(DateUtils.formatDateTime(updateAck.getAckTmst()));
        }

        quoteUpdateAttempt.setFailureReason(updateAck.getFailureReason());

        return quoteUpdateAttempt;
    }

    /**
     * Method to map updated MPLDocumentUpdateStatus
     *
     * @param updateStatus
     *            - MPLDocumentUpdateStatus object
     * @param updateAck
     *            - BancsUpdateAttemptDTO object
     * @return - updated MPLDocumentUpdateStatus object
     */
    public static void mapUpdatedDocUpdtStatusAndAttempts(final MPLDocumentUpdateStatus docUpdateStatus,
            final BancsUpdateAttemptDTO updateAck) {

        MPLDocumentUpdateAttempt documentUpdateAttempt;
        Set<MPLDocumentUpdateAttempt> documentUpdateAttempts;

        documentUpdateAttempt = mapToDocumentUpdateAttempt(updateAck);
        documentUpdateAttempt.setDocumentUpdateStatus(docUpdateStatus);

        documentUpdateAttempts = docUpdateStatus.getDocumentUpdateAttempts();
        documentUpdateAttempts.add(documentUpdateAttempt);

        docUpdateStatus.setDocumentUpdateAttempts(documentUpdateAttempts);
        docUpdateStatus.setAttempts(docUpdateStatus.getAttempts() + 1);
        docUpdateStatus.setStatus(updateAck.getUpdateStatus());
        docUpdateStatus.setTimestamp(DateUtils.getCurrentTimestamp());

    }

    private static MPLDocumentUpdateAttempt mapToDocumentUpdateAttempt(final BancsUpdateAttemptDTO updateAck) {

        MPLDocumentUpdateAttempt docUpdateAttempt;
        docUpdateAttempt = new MPLDocumentUpdateAttempt();

        docUpdateAttempt.setSentFlag(MPLServiceConstants.YES);
        docUpdateAttempt.setSentTmst(DateUtils.formatDateTime(updateAck.getUpdateTmst()));
        docUpdateAttempt.setAckFlag(updateAck.getAckFlg());

        if (CommonUtils.isObjectNotNull(updateAck.getAckTmst())) {
            docUpdateAttempt.setAckTmst(DateUtils.formatDateTime(updateAck.getAckTmst()));
        }

        docUpdateAttempt.setFailureReason(updateAck.getFailureReason());

        return docUpdateAttempt;
    }

    /**
     * Method to map MPLCustomerUpdateStatus
     *
     * @param mplCustomer
     *            - {@link MPLCustomer}
     * @param policyId
     *            - mpl_policy id
     * @param updatedField
     *            - updated field ('U': userid | 'X': encashment)
     * @return - MPLCustomerUpdateStatus object
     */
    public static MPLCustomerUpdateStatus mapCustUpdateStatus(final MPLCustomer mplCustomer, final Long policyId,
            final char updatedField) {

        MPLCustomerUpdateStatus customerUpdateStatus;

        customerUpdateStatus = new MPLCustomerUpdateStatus();

        customerUpdateStatus.setPolicyId(policyId);
        customerUpdateStatus.setUpdatedField(updatedField);
        customerUpdateStatus.setStatus(' ');
        customerUpdateStatus.setTimestamp(DateUtils.getCurrentTimestamp());
        customerUpdateStatus.setUpdatedCustomer(mplCustomer);

        return customerUpdateStatus;
    }
}
