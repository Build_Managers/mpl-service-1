/*
 * PolicyActivationRestController.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.restcontroller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dto.activation.CustomerActivationDTO;
import uk.co.phoenixlife.service.dto.activation.CustomerDetails;
import uk.co.phoenixlife.service.dto.activation.CustomerPersonalDetailStatusResponse;
import uk.co.phoenixlife.service.dto.activation.PolicyExistLockStatus;
import uk.co.phoenixlife.service.dto.activation.VerificationCheckAnswerWrapperDTO;
import uk.co.phoenixlife.service.dto.activation.VerificationDisplayEligibleQuestionWrapperDTO;
import uk.co.phoenixlife.service.dto.activation.VerificationMapAndLockedCountDTO;
import uk.co.phoenixlife.service.dto.activation.VerificationReturnValuesDTO;
import uk.co.phoenixlife.service.interfaces.AccountActivationService;

/**
 * This controller controls request for Policy Activation
 * PolicyActivationRestController.java
 */
@RestController
@RequestMapping("/accountActivation")
public class AccountActivationRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountActivationRestController.class);
    @Autowired
    private AccountActivationService policyActivationService;

    /**
     * Method to check if a policy Exist in MYPHX or Bancs
     *
     * @param policyNumber
     *            policy Number of the user
     * @param xGUID
     *            xGUID of the user
     * @return Policy Exist of the user
     * @throws Exception
     *             exception
     */
    @RequestMapping(value = "/policyExistCheck", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public PolicyExistLockStatus getPolicyExist(@RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID,
            @RequestBody final CustomerActivationDTO customerActivationDTO)
            throws Exception {
        LOGGER.debug("INSIDE THE AccountActivationRestcontroller getPolicyExists");
        return policyActivationService.getPolicyExist(customerActivationDTO, xGUID);

    }

    /**
     * Method to check personal Details of a user
     *
     * @param xGUID
     *            xGUID of the User
     * @param customerPersonalDetail
     *            details of the customer
     * @return flag
     * @throws Exception
     *             exception
     */
    @RequestMapping(value = "/checkPersonalDetails", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public CustomerPersonalDetailStatusResponse checkPersonalDetails(
            @RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID,
            @RequestBody final CustomerActivationDTO customerActivationDTO) throws Exception {
        LOGGER.debug("Reached AccountActivationController to validate Personal Details");
        return policyActivationService.validatePersonalDetail(customerActivationDTO, xGUID);
    }

    @RequestMapping(value = "/checkPostCodeGoneAwayAndLifeStatus", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public CustomerDetails checkPersonalDetails(@RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID,
            @RequestBody final CustomerDetails customerDetails) throws Exception {
        LOGGER.debug("INSIDE THE checkPostCode");
        return policyActivationService.validatePostCodeGoneAwayAndLifeStatusFlag(customerDetails, xGUID);
    }

    @RequestMapping(value = "/getQuestionAnswer", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public VerificationReturnValuesDTO getQuestionAnswer(@RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID,
            @RequestBody final CustomerActivationDTO customerActivationDTO) throws Exception {
        LOGGER.debug("INSIDE THE getQuestionAnswer");
        return policyActivationService.getQuestionAnswer(customerActivationDTO, xGUID);
    }

    @RequestMapping(value = "/checkAnswer", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public VerificationMapAndLockedCountDTO getQuestionAnswer(
            @RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID,
            @RequestBody final VerificationCheckAnswerWrapperDTO verificationCheckAnswerWrapperDTO) throws Exception {
        LOGGER.debug("Inside AccountActivationRestController checkAnswer method");
        return policyActivationService.checkAnswer(verificationCheckAnswerWrapperDTO, xGUID);
    }

    @RequestMapping(value = "/getNextQuestionDetails", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public VerificationReturnValuesDTO getNextQuestionDetails(
            @RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID,
            @RequestBody final VerificationDisplayEligibleQuestionWrapperDTO verificationDisplayEligibleQuestionWrapperDTO)
            throws Exception {
        LOGGER.debug("INSIDE THE getNextQuestionDetails");
        return policyActivationService.getNextQuestionDetails(verificationDisplayEligibleQuestionWrapperDTO, xGUID);

    }

    @RequestMapping(value = "/getCustomerAndPolicyID", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public CustomerActivationDTO getCustomerAndPolicyId(@RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID,
            @RequestBody final CustomerActivationDTO customerActivationDTO)
            throws Exception {
        LOGGER.debug("inside getCustomerAndPolicyID");
        return policyActivationService.getCustomerAndPolicyId(customerActivationDTO, xGUID);
    }

    @RequestMapping(value = "/checkUsernameExistForDifferentUniqueCustomerNo", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Boolean checkUsernameExistForDifferentUniqueCustomerNo(
            @RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID,
            @RequestBody final CustomerActivationDTO customerActivationDTO)
            throws Exception {
        LOGGER.debug("Inside checkUsernameExistForDifferentUniqueCustomerNo method");
        return policyActivationService.checkUsernameExistForDifferentUniqueCustomerNo(customerActivationDTO, xGUID);
    }
}
