/*
 * BancsPostCallsController.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dto.policy.CustomerDTO;
import uk.co.phoenixlife.service.interfaces.EncashmentService;

/**
 * BancsPostCallsController.java
 */
@RestController
@RequestMapping("/bancspostcall")
public class BancsPostCallsController {

    @Autowired
    private EncashmentService encashmentService;

    /**
     * Method to get flag status of BancsPostCalls for a user
     *
     * @param xGUID
     *            xGUID of the User
     * @param customerDTO
     *            details of the customer in session
     * @throws Exception
     *             exception
     */
    @RequestMapping(value = "/", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void getBancsPostCallsStatus(@RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID,
            @RequestBody final CustomerDTO customerDTO) throws Exception {
        encashmentService.submitEncashmentPolicyDetailsAndBaNCSCalls(customerDTO, xGUID);
    }
}
