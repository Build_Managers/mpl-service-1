package uk.co.phoenixlife.service.restcontroller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import uk.co.phoenixlife.service.dto.bankvalidation.BankValidationRequest;
import uk.co.phoenixlife.service.dto.bankvalidation.BankValidationResponse;
import uk.co.phoenixlife.service.dto.bankvalidation.BankValidationService;


@RestController
@RequestMapping("/validatebankdetails")
public class BankValidationRestController {
	
    @Autowired
    private BankValidationService bankValidationService;

	private static final Logger LOGGER = LoggerFactory.getLogger(BankValidationRestController.class);
	
	@RequestMapping(value = "/validateaccountnumbersortcode", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
	public BankValidationResponse validateBankDetails(@RequestBody final BankValidationRequest vocalinkRequest)
			throws Exception {
		LOGGER.debug("Inside BankValidationRestController validateBankDetails");
    	return bankValidationService.validateBankDetails(vocalinkRequest,null);
	}
 }
