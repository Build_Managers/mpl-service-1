/*
 * DashboardRestController.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.restcontroller;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dto.activation.CustomerIdentifierDTO;
import uk.co.phoenixlife.service.dto.activation.CustomerPolicyDetail;
import uk.co.phoenixlife.service.dto.bancspost.BancsCalcCashInTaxOutput;
import uk.co.phoenixlife.service.dto.policy.CashInTypeDTO;
import uk.co.phoenixlife.service.dto.policy.DashBoardResponse;
import uk.co.phoenixlife.service.dto.policy.FinalDashboardDTO;
import uk.co.phoenixlife.service.dto.response.BancsPartyCommDetailsResponse;
import uk.co.phoenixlife.service.dto.response.PoliciesOwnedByAPartyResponse;
import uk.co.phoenixlife.service.dto.response.PoliciesOwnedByAPartyResponseWrapper;
import uk.co.phoenixlife.service.interfaces.DashboardService;
import uk.co.phoenixlife.service.interfaces.EncashmentService;

/**
 * DashboardRestController.java
 */
@RestController
@RequestMapping("/dashboard")
public class DashboardRestController {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(DashboardRestController.class);

    @Autowired
    private DashboardService dashboardService;

    @Autowired
    private EncashmentService encashmentService;

    /**
     * Request handler for policiesOwnedByParty
     *
     * @param xGUID
     *            - xGUID
     * @param uniqueCustomerNumber
     *            - uniqueCustomerNumber
     * @return - {@link PoliciesOwnedByAPartyResponse}
     * @throws Exception
     *             - {@link Exception}
     */

    @RequestMapping(value = "/policiesOwnedByParty/{uniqueCustomerNumber}", method = RequestMethod.GET,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PoliciesOwnedByAPartyResponseWrapper
           policiesOwnedByParty(@PathVariable("uniqueCustomerNumber") final String uniqueCustomerNumber,
                   @RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID) throws Exception {

        PoliciesOwnedByAPartyResponseWrapper response;
        LOGGER.debug("INSIDE THE policiesOwnedByParty");
        response = dashboardService.policiesOwnedByParty(uniqueCustomerNumber, xGUID);
        return response;
    }

    @RequestMapping(value = "/bancsPartyCommDetails/{bancsUniqueCustNo}", method = RequestMethod.GET,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public BancsPartyCommDetailsResponse
           partyCommDetailsResponse(@PathVariable("bancsUniqueCustNo") final String bancsUniqueCustNo,
                   @RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID) throws Exception {

        LOGGER.debug("INSIDE THE bancsPartyCommDetails");
        BancsPartyCommDetailsResponse response;
        response = dashboardService.getPartyCommDetailsResponse(bancsUniqueCustNo, xGUID);
        return response;
    }

    /**
     * Request handler for policiesDetails
     *
     * @param xGUID
     *            - xGUID
     * @param policyNumber
     *            - policyNumber
     * @return - {@link CustomerPolicyDetail}
     * @throws Exception
     *             - {@link Exception}
     */
    @RequestMapping(value = "/policiesDetails", method = RequestMethod.GET,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public CustomerPolicyDetail policiesDetails(@RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID,
            @RequestParam("policyNumber") final String policyNumber) throws Exception {

        CustomerPolicyDetail response;
        LOGGER.debug("INSIDE THE policiesDetails");
        response = dashboardService.getpolicyDetailValuationPremiumSum(policyNumber, xGUID);
        return response;
    }

    /**
     * Method to get encashmentDashboard of a user
     *
     * @param xGUID
     *            xGUID of the User
     * @param customerData
     *            details of the customer in session
     * @return FinalDashboard Object
     * @throws Exception
     *             exception
     */
    @RequestMapping(value = "/encashmentDashboard", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public FinalDashboardDTO getEncashmentDashboard(@RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID,
            @RequestBody final CustomerIdentifierDTO customerData) throws Exception {
        LOGGER.debug("INSIDE THE encashmentDashboard");
        return encashmentService.getEncashmentDashboard(customerData.getBancsUniqueCustNo(), customerData, xGUID);
    }

    /**
     * Request handler for CalculateTax API
     *
     */
    @RequestMapping(value = "/calculateTax", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public BancsCalcCashInTaxOutput calculateTax(@RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID,
            @RequestBody final CashInTypeDTO cashInTypeDTO) throws Exception {

        BancsCalcCashInTaxOutput response = null;
        if (cashInTypeDTO.getSource().equalsIgnoreCase(MPLConstants.DASHBOARDCASHINSOURCE)) {
            response = dashboardService.getBancsCalcCashInTaxOutput(cashInTypeDTO, xGUID);
        } else {
            response = dashboardService.getSyncBancsCalcCashInTaxOutput(cashInTypeDTO, xGUID);
        }
        return response;

    }

    /**
     * Request handler for CalculateTax API
     *
     *
     * @RequestMapping(value =
     *                       "/calculateTax/{uniqueCustomerNumber1}/{uniqueCustomerNumber2}",
     *                       method = RequestMethod.POST, consumes =
     *                       MediaType.APPLICATION_JSON_VALUE, produces =
     *                       MediaType.APPLICATION_JSON_VALUE) public
     *                       BancsCalcCashInTaxOutput
     *                       calculateTax1(@RequestHeader(MPLServiceConstants.XGUID_HEADER)
     *                       final String
     *                       xGUID, @PathVariable("uniqueCustomerNumber1") final
     *                       String
     *                       uniqueCustomerNumber1, @PathVariable("uniqueCustomerNumber2")
     *                       final String
     *                       uniqueCustomerNumber2, @RequestParam("bancsPolicyNumber")
     *                       final String bancsPolicyNumber) throws Exception {
     *
     *                       BancsCalcCashInTaxOutput response =
     *                       dashboardService.getBancsCalcCashInTaxOutput(
     *                       uniqueCustomerNumber1 + "/" +
     *                       uniqueCustomerNumber2, bancsPolicyNumber, xGUID);
     *                       return response;
     *
     *                       }
     */

    @RequestMapping(value = "/getCustomerDetail/{userName}", method = RequestMethod.GET,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public DashBoardResponse getUniqueCustomerNo(@PathVariable("userName") final String userName,
            @RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID) throws Exception {

        DashBoardResponse response;
        LOGGER.debug("INSIDE THE getCustomerDetail");
        response = dashboardService.getUniqueCustomerNo(userName, xGUID);
        return response;
    }

}
