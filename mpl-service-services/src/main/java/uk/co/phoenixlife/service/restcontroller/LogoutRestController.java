package uk.co.phoenixlife.service.restcontroller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dto.response.PoliciesOwnedByAPartyResponseWrapper;
import uk.co.phoenixlife.service.interfaces.LogoutService;

@RestController
@RequestMapping("/logout")
public class LogoutRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogoutRestController.class);

    @Autowired
    private LogoutService logoutService;

    @RequestMapping(value = "/clearCache", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void clearCache(@RequestBody final PoliciesOwnedByAPartyResponseWrapper policiesOwnedByAPartyResponseWrapper,
            @RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID)
            throws Exception {
        LOGGER.debug("Inside clearCache method of LogoutRestController");
        logoutService.clearCache(xGUID, policiesOwnedByAPartyResponseWrapper.getxGUIDBancsPolNumCacheKeyList());
    }
}
