package uk.co.phoenixlife.service.restcontroller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import uk.co.phoenixlife.service.dto.ErrorListDTO;
import uk.co.phoenixlife.service.interfaces.MIComplianceServiceInterface;

@RestController
@RequestMapping("/compliance")
public class MIComplianceRestController {
	
	
	 private static final Logger LOGGER = LoggerFactory.getLogger(MIComplianceRestController.class);
	
	/**
     * Method to save ErrorListDTO in DB
     *
     * @param request
     *            - request object
     * @throws Exception
     *             - exception
     * @return result
     */
   
	 @Autowired
	 private MIComplianceServiceInterface miComplianceServiceInterface;
	 
	 @RequestMapping(value = "/submitErrorList", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
     public Boolean submitErrorListDTO(@RequestBody final uk.co.phoenixlife.service.dto.MiErrorRequestDTO request) throws Exception {
		 List<ErrorListDTO> list = request.getErrorListDTO();
		 LOGGER.debug("INSIDE THE miComplianceServiceInterface");
		 miComplianceServiceInterface.submitErrorList(request);
		return true;
    }
}
