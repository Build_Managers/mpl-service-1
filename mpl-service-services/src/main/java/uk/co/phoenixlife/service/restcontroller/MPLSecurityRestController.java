package uk.co.phoenixlife.service.restcontroller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import uk.co.phoenixlife.security.service.dto.ForgotPasswordDto;
import uk.co.phoenixlife.security.service.dto.UserRegistrationDTO;
import uk.co.phoenixlife.security.service.dto.UserRegistrationFormDTO;
import uk.co.phoenixlife.security.service.interfaces.MPLSecurityService;

@RestController
@RequestMapping("/security")
public class MPLSecurityRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MPLSecurityRestController.class);

    @Autowired
    private MPLSecurityService mPLSecurityService;

    @RequestMapping(value = "/insertUserDetailInLDAP", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public String insertUserDetailInLDAP(@RequestBody final UserRegistrationFormDTO userRegistrationFormDTO)
            throws Exception {
        LOGGER.debug("Inside checkUsernameExistForDifferentUniqueCustomerNo method");
        mPLSecurityService.insertUserDetailInLDAP(userRegistrationFormDTO);
        return "success";
    }

    @RequestMapping(value = "/updateUserDetailInLDAP", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public String updateUserDetailInLDAP(@RequestBody final UserRegistrationFormDTO userRegistrationFormDTO)
            throws Exception {
        LOGGER.debug("Inside checkUsernameExistForDifferentUniqueCustomerNo method");
        mPLSecurityService.updateUserDetailInLDAP(userRegistrationFormDTO);
        return "success";
    }

    @RequestMapping(value = "/checkUserNameAlreadyExist/{userName}", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UserRegistrationDTO checkUserNameAlreadyExist(@PathVariable("userName") final String userName)
            throws Exception {
        LOGGER.debug("Inside checkUsernameExistForDifferentUniqueCustomerNo method");
        return mPLSecurityService.checkUserNameAlreadyExist(userName);
    }

    @RequestMapping(value = "/checkEmailAlreadyExist", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Boolean checkEmailAlreadyExist(@RequestBody final UserRegistrationFormDTO userRegistrationFormDTO)
            throws Exception {
        LOGGER.debug("Inside checkEmailAlreadyExist method");
        return mPLSecurityService.checkEmailAlreadyExist(userRegistrationFormDTO);
    }

    @RequestMapping(value = "/validateToken/{actid}", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UserRegistrationFormDTO activateAccount(@PathVariable("actid") final String token)
            throws Exception {
        LOGGER.debug("Inside activateAccount method");
        return mPLSecurityService.activateAccount(token);
    }

	@RequestMapping(value = "/lockAccount/{userName}/{isRegistration}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public void lockAccount(@PathVariable("userName") final String userName,
			@PathVariable("isRegistration") final boolean isRegistration) throws Exception {
		LOGGER.debug("Inside lockAccount method");
		mPLSecurityService.lockAccount(userName, isRegistration);
	}

	@RequestMapping(value = "/changePassUrlValidate/{rpid}", method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public String changePassUrlValidate(@PathVariable("rpid") final String Token)
			throws Exception {
		LOGGER.debug("Inside changePassUrlValidate method");
		return mPLSecurityService.changePassUrlValidate(Token);
	}
	
	@RequestMapping(value = "/fetchSecurityQuestion", method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public String fetchSecurityQuestion(@RequestBody final ForgotPasswordDto forgotPasswordDto)
			throws Exception {
		LOGGER.debug("Inside FetchSecurityQuestion method");
		return mPLSecurityService.fetchSecurityQuestion(forgotPasswordDto);

	}

	@RequestMapping(value = "/submitSecurityAnswer", method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public String submitSecurityAnswer(@RequestBody final ForgotPasswordDto forgotPasswordDto)
			throws Exception {
		LOGGER.debug("Inside SubmitSecurityAnswer method");
		return mPLSecurityService.submitSecurityAnswer(forgotPasswordDto);

	}

	@RequestMapping(value = "/changePasswordForgot", method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public String changePassword(@RequestBody final ForgotPasswordDto forgotPasswordDto)
			throws Exception {
		LOGGER.debug("Inside changePasswordForgot method");
		return mPLSecurityService.changePassword(forgotPasswordDto);
	}
	
	@RequestMapping(value = "/increaseAnswerAttemptCount/{userName}", method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public String increaseAnswerAttemptCount(@PathVariable final String userName)
			throws Exception {
		LOGGER.debug("Inside changePasswordForgot method");
		 String output = mPLSecurityService.increaseAnswerAttemptCount(userName);
		 return output;
	}

	@RequestMapping(value = "/validateResetToken/{rpid}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public UserRegistrationFormDTO ResetAccount(@PathVariable("rpid") final String Token) throws Exception {
		LOGGER.debug("Inside validateResetToken method");
		return mPLSecurityService.ResetAccount(Token);
	}

	@RequestMapping(value = "/updateSecurityQuestionInLDAP", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public String updateSecurityQuestionInLDAP(@RequestBody final UserRegistrationFormDTO userRegistrationFormDTO)
			throws Exception {
		LOGGER.debug("Inside updateSecurityQuestionInLDAP method");
		return mPLSecurityService.updateSecurityQuestionInLDAP(userRegistrationFormDTO);
	}
	
	@RequestMapping(value = "/fetchEmailIdFromLDAP/{userName}", method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public String fetchEmailIdFromLDAP(@PathVariable("userName") final String userName)
			throws Exception {
		LOGGER.debug("Inside fetchEmailIdFromLDAP method");
		return mPLSecurityService.fetchEmailIdFromLDAP(userName);
	}
	@RequestMapping(value = "/fetchDetailsFromLDAP/{userName}", method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public String fetchDetailsFromLDAP(@PathVariable("userName") final String userName)
			throws Exception {
		LOGGER.debug("Inside fetchDetailsFromLDAP method");
		return mPLSecurityService.fetchDetailsFromLDAP(userName);
	}

}
