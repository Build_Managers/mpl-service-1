/*
 * PDFCreationServiceRestController.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.restcontroller;

import java.util.Base64;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dto.pdf.FormRequestDTO;
import uk.co.phoenixlife.service.dto.pdf.FormResponseDTO;
import uk.co.phoenixlife.service.dto.pdf.PdfDataDTO;
import uk.co.phoenixlife.service.dto.policy.DashboardDTO;
import uk.co.phoenixlife.service.dto.policy.PartyPolicyDTO;
import uk.co.phoenixlife.service.interfaces.PDFService;

/**
 * PDFCreationServiceRestController.java
 */
@RestController
@RequestMapping("/pdf")
public class PDFServiceRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PDFServiceRestController.class);

    @Autowired
    private PDFService pdfService;

    /**
     * Request handler to get static data map
     *
     * @param groupId
     *            - groupId
     * @return - {@link Map}
     * @throws Exception
     *             - {@link Exception}
     */
    @RequestMapping(value = "/{pdfType}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public String getStaticPDF(@PathVariable("pdfType") final String pdfType) throws Exception {

        LOGGER.debug("Invoking getStaticPDF from PDFServiceRestController...");

        return new String(Base64.getEncoder().encode(pdfService.getStaticPdf(pdfType)));
    }

    /**
     * insertEligiblePolicyData
     *
     * @param request
     *            - request
     * @param customerId
     *            - customerId
     * @return policyId
     * @throws Exception
     *             - Exception
     */
    @RequestMapping(value = "/insertEligiblePolicyData", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Long insertEligiblePolicyData(@RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID,
            @RequestBody final DashboardDTO request, @RequestParam("customerId") final Long customerId)
            throws Exception {
        LOGGER.info("Invoking insertEligiblePolicyData from PDFServiceRestController ---->>>>");
        return pdfService.insertEligiblePolicyData(customerId, request, xGUID);
    }

    /**
     * insertEligiblePolicyData
     *
     * @param request
     *            - request
     * @param customerId
     *            - customerId
     * @return policyId
     * @throws Exception
     *             - Exception
     */
    @RequestMapping(value = "/showRetirementPack", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Boolean showRetirementPack(@RequestBody final DashboardDTO request,
            @RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID)
            throws Exception {
        LOGGER.info("Invoking showRetirementPack from PDFServiceRestController ---->>>>");
        return pdfService.showRetirementPack(request, xGUID);
    }

    /**
     * generatePdf
     *
     * @param request
     *            - request
     * @return response
     * @throws Exception
     *             - Exception
     */
    @RequestMapping(value = "/generatePdf", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public FormResponseDTO generatePdf(@RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID,
            @RequestBody final FormRequestDTO request) throws Exception {
        LOGGER.info("Invoking generatePdf from PDFCreationRestController ---->>>>");
        FormResponseDTO response;
        response = new FormResponseDTO();
        response.setPdfAsHtml(
                pdfService.generatePdf(request.getPolicyId(), request.getDocType(), xGUID));

        return response;
    }

    @RequestMapping(value = "/savePdfContentToDB", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Boolean savePdfContentToDB(@RequestBody final FormRequestDTO request,
            @RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID)
            throws Exception {
        LOGGER.info("Invoking showRetirementPack from PDFServiceRestController ---->>>>");
        return pdfService.savePdfContentToDB(request, xGUID);
    }

    @RequestMapping(value = "/generateRetirementPackPdf", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public byte[] generateRetirementPackPdf(@RequestBody final PdfDataDTO pdfData,
            @RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID)
            throws Exception {
        LOGGER.info("Invoking generateRetirementPackPdf from PDFServiceRestController ---->>>>");
        return pdfService.generateRetirementPackPdf(pdfData, xGUID);
    }

    @RequestMapping(value = "/generateApplicationPdf", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public byte[] generateApplicationPdf(@RequestBody final PdfDataDTO pdfData,
            @RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID)
            throws Exception {
        LOGGER.info("Invoking generateApplicationPdf from PDFServiceRestController ---->>>>");
        return pdfService.generateApplicationPdf(pdfData.getPdfByte(), xGUID);
    }

    @RequestMapping(value = "/getEligiblePolicyIdvData", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PartyPolicyDTO getEligiblePolicyIdvData(@RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID,
            @RequestBody final DashboardDTO eligible) throws Exception {
        LOGGER.info("Invoking getEligiblePolicyIdvData from PDFServiceRestController ---->>>>");

        return pdfService.getEligiblePolicyIdvData(eligible, xGUID);
    }

    /**
     * insertEligiblePolicyData
     *
     * @param request
     *            - request
     * @param customerId
     *            - customerId
     * @return policyId
     * @throws Exception
     *             - Exception
     */
    @RequestMapping(value = "/checkCustomerPolicyExistance", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Long checkCustomerPolicyExistance(@RequestBody final DashboardDTO request,
            @RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID,
            @RequestParam("customerId") final Long customerId)
            throws Exception {
        LOGGER.info("Invoking checkCustomerPolicyExistance from PDFServiceRestController ---->>>>");
        return pdfService.checkCustomerPolicyExistance(customerId, request, xGUID);
    }

}
