package uk.co.phoenixlife.service.restcontroller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import uk.co.phoenixlife.service.dto.pca.find.CaptureInteractiveFindV100ArrayOfResults;
import uk.co.phoenixlife.service.dto.pca.find.CaptureInteractiveFindV100Results;
import uk.co.phoenixlife.service.interfaces.PcaFindClientService;


@RestController
@RequestMapping("/pcaService")
public class PcaFindClientRestcontroller {
	
	 private static final Logger LOGGER = LoggerFactory.getLogger(PcaFindClientRestcontroller.class);
	 
	 @Autowired
	 private PcaFindClientService pcaFindClientService;
	    
	    /*@RequestMapping(value = "/addressvalidate", produces = "application/json")
	    public List<CaptureInteractiveFindV100Results> getAddressList(final HttpServletRequest request)
	            throws JsonMappingException, InterruptedException
	    {
	    	 List<CaptureInteractiveFindV100Results> list= ((CaptureInteractiveFindV100ArrayOfResults) request).getCaptureInteractiveFindV100Results();
	    	
	    	 pcaFindClientService.getAddressList(request);
	    	 
	    }*/
	 
	 @RequestMapping(value = "/find/{postCode}", method = RequestMethod.GET,
	            produces = MediaType.APPLICATION_JSON_VALUE)
	    public CaptureInteractiveFindV100ArrayOfResults find(@PathVariable("postCode") final String postCode) throws Exception {
		 
		 	LOGGER.info("Reached inside PcaFindClientRestcontroller");
	        return pcaFindClientService.find(postCode);
	    }
	 

}
