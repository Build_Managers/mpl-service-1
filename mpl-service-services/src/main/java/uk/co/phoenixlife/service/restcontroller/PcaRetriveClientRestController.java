package uk.co.phoenixlife.service.restcontroller;

import java.io.IOException;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.itextpdf.text.log.LoggerFactory;

import uk.co.phoenixlife.service.dto.pca.find.CaptureInteractiveFindV100ArrayOfResults;
import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100;
import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100ArrayOfResults;
import uk.co.phoenixlife.service.dto.policy.CustomerDTO;
import uk.co.phoenixlife.service.interfaces.PcaRetriveClientService;

@RestController
@RequestMapping("/pcaRetriveService")
public class PcaRetriveClientRestController {
	
	private static final com.itextpdf.text.log.Logger LOGGER = LoggerFactory.getLogger(PcaRetriveClientRestController.class);
	
	 @Autowired
	 private PcaRetriveClientService pcaRetriveClientService;
	 
	 
	 
	 @RequestMapping(value = "/retrieve/{retrieveRequest}", method = RequestMethod.POST,
		produces = MediaType.APPLICATION_JSON_VALUE)
	    public CaptureInteractiveRetrieveV100ArrayOfResults retrieve(@RequestBody final CaptureInteractiveRetrieveV100 addressId ) throws Exception {
		 
		 	LOGGER.info("Reached inside PcaRetriveClientRestcontroller");
	        return pcaRetriveClientService.retrieve(addressId);
	    }
	 
	
	
	

}
