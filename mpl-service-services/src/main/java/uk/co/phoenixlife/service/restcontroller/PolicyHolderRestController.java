/**
 *
 */
package uk.co.phoenixlife.service.restcontroller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dto.policy.CustomerDTO;
import uk.co.phoenixlife.service.dto.response.PartyBasicDetailByPartyNoResponse;
import uk.co.phoenixlife.service.interfaces.PolicyHolderService;

/**
 * @author 883555
 *
 */
@RestController
@RequestMapping("/policyholder")
public class PolicyHolderRestController {

    @Autowired
    private PolicyHolderService policyHolderService;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ObjectMapper mapper;

    private static final Logger LOGGER = LoggerFactory.getLogger(PolicyHolderRestController.class);

    /*
     * @RequestMapping(value = "/{bancsUniqueCustNo}/getpolicyholders", method =
     * RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces
     * = MediaType.APPLICATION_JSON_VALUE) public BancsPolicyHoldersResponse
     * getPolicyHolders(
     * 
     * @PathVariable("bancsUniqueCustNo") final String bancsUniqueCustNo,
     * 
     * @RequestBody final CustomerDTO customerDTO) throws JsonParseException,
     * JsonMappingException, IOException { BancsPolicyHoldersResponse
     * bancsPolicyHoldersResponse = null; try { bancsPolicyHoldersResponse =
     * policyHolderService.getPolicyHoldersDetails(bancsUniqueCustNo,
     * customerDTO); System.out.println("bancsPolicyHoldersResponse: " +
     * bancsPolicyHoldersResponse); } catch (final Exception exception) {
     * exception.printStackTrace(); LOGGER.
     * error("{} FAILED bancsPolicyHoldersResponse Exception trying to map with ErrorResponse{}"
     * , exception);
     * 
     * } return bancsPolicyHoldersResponse;
     * 
     * }
     */

    @RequestMapping(value = "/{bancsUniqueCustNo}/getPartyBasicDetailByPartyNo", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public PartyBasicDetailByPartyNoResponse getPolicyHolders(
            @PathVariable("bancsUniqueCustNo") final String bancsUniqueCustNo,
            @RequestHeader(MPLServiceConstants.XGUID_HEADER) final String xGUID)
            throws JsonParseException, JsonMappingException, IOException {
        PartyBasicDetailByPartyNoResponse partyBasicDetailByPartyNoResponse = null;
        try {
            partyBasicDetailByPartyNoResponse = policyHolderService.getPartyBasicDetailByPartyNo(bancsUniqueCustNo, xGUID);
            System.out.println("Received partyBasicDetailByPartyNoResponse ");

        } catch (final Exception exception) {
            exception.printStackTrace();
            LOGGER.error("{} FAILED partyBasicDetailByPartyNoResponse Exception trying to map with ErrorResponse{}",
                    exception);

        }

        return partyBasicDetailByPartyNoResponse;
    }

    /*
     * @RequestMapping(value = "/{bancsUniqueCustNo}/getBancsPartyCommDetails",
     * method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
     * public BancsPartyCommDetailsResponse getBancsPartyCommDetails(
     * 
     * @PathVariable("bancsUniqueCustNo") final String bancsUniqueCustNo)throws
     * JsonParseException, JsonMappingException, IOException {
     * BancsPartyCommDetailsResponse bancsPartyCommDetailsResponse =null; try {
     * bancsPartyCommDetailsResponse =
     * policyHolderService.getBancsPartyCommDetails(bancsUniqueCustNo);
     * System.out.println("BancsPartyCommDetailsResponse: " +
     * bancsPartyCommDetailsResponse);
     * 
     * } catch (final Exception exception) { exception.printStackTrace();
     * LOGGER.
     * error("{} FAILED partyBasicDetailByPartyNoResponse Exception trying to map with ErrorResponse{}"
     * , exception);
     * 
     * }
     * 
     * return bancsPartyCommDetailsResponse; }
     */

    @RequestMapping(value = "/{bancsUniqueCustNo}/updateemail", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public Boolean updateEmail(@PathVariable("bancsUniqueCustNo") final String bancsUniqueCustNo,
            @RequestBody final CustomerDTO customerDTO) throws JsonParseException, JsonMappingException, IOException {
        // BancsPolicyHoldersResponse bancsPolicyHoldersResponse = null;
        // String bancsUniqueCustNo = customerDTO.getBancsUniqueCustNo();
        /*
         * try { bancsPolicyHoldersResponse =
         * policyHolderService.getPolicyHoldersDetails(bancsUniqueCustNo,
         * customerDTO); System.out.println("bancsPolicyHoldersResponse: " +
         * bancsPolicyHoldersResponse); } catch (final Exception exception) {
         * exception.printStackTrace(); LOGGER.
         * error("{} FAILED bancsPolicyHoldersResponse Exception trying to map with ErrorResponse{}"
         * , exception);
         *
         * }
         */

        Boolean flag = policyHolderService.updateEmail(customerDTO);
        LOGGER.info("Inside Service RestController");
        return flag;

    }
}
