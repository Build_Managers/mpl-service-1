/*
 * PostCodeRestController.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.restcontroller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import uk.co.phoenixlife.service.dto.pca.find.CaptureInteractiveFindV100ArrayOfResults;
import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100;
import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100ArrayOfResults;
import uk.co.phoenixlife.service.interfaces.PostCodeService;

/**
 * PostCodeRestController.java
 */
@RestController
@RequestMapping("/postcode")
public class PostCodeRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PostCodeRestController.class);

    @Autowired
    private PostCodeService postCodeService;

    @RequestMapping(value = "/find/{postCode}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public CaptureInteractiveFindV100ArrayOfResults findAddress(@PathVariable("postCode") final String postCode)
            throws Exception {

        LOGGER.debug("Request for find address");
        return postCodeService.findAddress(postCode);
    }

    @RequestMapping(value = "/retrieve", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public CaptureInteractiveRetrieveV100ArrayOfResults retrieve(@RequestBody final CaptureInteractiveRetrieveV100 addressId)
            throws Exception {

        LOGGER.debug("Request for retrieve address");
        return postCodeService.retrieveAddress(addressId);
    }
}
