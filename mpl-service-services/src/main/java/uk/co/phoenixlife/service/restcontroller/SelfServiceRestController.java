/*
 * SelfServiceRestController.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.restcontroller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import uk.co.phoenixlife.service.dto.SecurityQuestionDTO;
import uk.co.phoenixlife.service.dto.SecurityQuestionsForAUserDTO;
import uk.co.phoenixlife.service.dto.activation.CustomerActivationDTO;
import uk.co.phoenixlife.service.interfaces.SelfService;

/**
 * This controller controls request for getting security question for a particular username
 * SelfServiceRestController.java
 */
@RestController
@RequestMapping("/selfService")
public class SelfServiceRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SelfServiceRestController.class);
    @Autowired
    private SelfService selfService;
    
    /**
     * Method to get security question for a particular username
     *
     * @param userName
     *            userName to get securityQuestions
     * @return SecurityQuestionDTO
     * @throws Exception
     *             exception
     */
    @RequestMapping(value = "/getQuestions/{username}", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public SecurityQuestionDTO getSecurityQuestionsForUser(@PathVariable("username") final String username) throws Exception {
        
    	   LOGGER.debug("Invoking getSecurityQuestionsForUser from SelfServiceRestController...");
    	   return selfService.getSecurityQuestions(username);
    }

    
    /**
     * Method to get security question for a particular username
     *
     * @param userName
     *            userName to get securityQuestions
     * @return SecurityQuestionDTO
     * @throws Exception
     *             exception
     */
   
    
    
    @RequestMapping(value = "/checkAnswer", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean checkAnswersForUser(@RequestBody final SecurityQuestionsForAUserDTO dto)throws Exception {
    	  LOGGER.debug("Invoking checkAnswersForUser from SelfServiceRestController...");
   	   return selfService.checkAnswerOfSecurityQuestion(dto);
    }
    
}
