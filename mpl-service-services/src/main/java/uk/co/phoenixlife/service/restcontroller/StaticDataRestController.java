/*
 * StaticDataRestController.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.restcontroller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import uk.co.phoenixlife.service.dto.staticdata.StaticDataRequestDTO;
import uk.co.phoenixlife.service.dto.staticdata.StaticDataResponseDTO;
import uk.co.phoenixlife.service.dto.staticdata.StaticDataService;

/**
 * StaticDataRestController.java
 */
@RestController
@RequestMapping("/staticdata")
public class StaticDataRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StaticDataRestController.class);

    @Autowired
    private StaticDataService staticDataService;

    /**
     * Request handler to get static data map
     *
     * @param groupId
     *            - groupId
     * @return - {@link Map}
     * @throws Exception
     *             - {@link Exception}
     */
    @RequestMapping(value = "/{groupId}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, String> getStaticDataMap(@PathVariable("groupId") final String groupId) throws Exception {

        //LOGGER.debug("Invoking getStaticDataMap from StaticDataRestController...");
        return staticDataService.getStaticDataMap(groupId);
    }

    /**
     * Request handler to get static data value
     *
     * @param groupId
     *            - groupId
     * @param key
     *            - key
     * @return - value of static data key
     * @throws Exception
     *             - {@link Exception}
     */
    @RequestMapping(value = "/{groupId}/{keyName}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public String getStaticDataValue(@PathVariable("groupId") final String groupId,
            @PathVariable("keyName") final String key) throws Exception {

        LOGGER.debug("Invoking getStaticDataValue from StaticDataRestController...");
        return staticDataService.getStaticDataValue(groupId, key);
    }

    /**
     * Request handler to get id&v static data
     *
     * @param request
     *            - StaticDataRequestDTO object
     * @return - StaticDataResponseDTO object
     * @throws Exception
     *             - {@link Exception}
     */
    @RequestMapping(value = "/getidvdata", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public StaticDataResponseDTO getIDVQuestionsData(@RequestBody final StaticDataRequestDTO request) throws Exception {

        LOGGER.info("Invoking getIDVQuestionsData from StaticDataRestController --->>>");
        StaticDataResponseDTO response;
        response = new StaticDataResponseDTO();
        response.setStaticIDVDataList(staticDataService.getIDVQuestionsData());

        return response;
    }
}
