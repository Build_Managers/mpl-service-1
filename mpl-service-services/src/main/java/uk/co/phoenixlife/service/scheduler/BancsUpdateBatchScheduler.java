/*
 * BancsUpdateBatchScheduler.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import net.javacrumbs.shedlock.core.SchedulerLock;
import uk.co.phoenixlife.service.async.BancsUpdateService;
import uk.co.phoenixlife.service.constants.MPLServiceConstants;

/**
 * BancsUpdateBatchScheduler.java
 */
@Component
public class BancsUpdateBatchScheduler {

    private static final Logger LOGGER = LoggerFactory.getLogger(BancsUpdateBatchScheduler.class);

    @Autowired
    private BancsUpdateService bancsUpdateService;

    @Value("${skip.batch}")
    private String batchSkipFlag;

    /**
     * Cron scheduler to send BaNCs update : off-working hours
     *
     * @throws Exception
     *             - {@link Exception}
     */
    @Scheduled(cron = "${cron.offWorkingHours}")
    @SchedulerLock(name = "bancsBatchOffHours", lockAtMostFor = MPLServiceConstants.FIVE_MINUTES,
            lockAtLeastFor = MPLServiceConstants.FIVE_MINUTES)
    public void runOutOfBusinessHours() throws Exception {

        if (MPLServiceConstants.TRUE.equalsIgnoreCase(batchSkipFlag)) {
            LOGGER.info("|BancsUpdateOffWorkingHours_Batch|- skipped...");
        } else {
            LOGGER.info("|BancsUpdateOffWorkingHours_Batch|- started...");
            bancsUpdateService.bancsUpdateBatch();
            LOGGER.info("|BancsUpdateOffWorkingHours_Batch|- completed...");
        }

    }

    /**
     * Cron scheduler to send BaNCs update : on-working hours
     *
     * @throws Exception
     *             - {@link Exception}
     */
    @Scheduled(cron = "${cron.onWorkingHours}")
    @SchedulerLock(name = "bancsBatchOnHours", lockAtMostFor = MPLServiceConstants.FIVE_MINUTES,
            lockAtLeastFor = MPLServiceConstants.FIVE_MINUTES)
    public void runDuringBusinessHours() throws Exception {

        if (MPLServiceConstants.TRUE.equalsIgnoreCase(batchSkipFlag)) {
            LOGGER.info("|BancsUpdateOnWorkingHours_Batch|- skipped...");
        } else {
            LOGGER.info("|BancsUpdateOnWorkingHours_Batch|- started...");
            bancsUpdateService.bancsUpdateBatch();
            LOGGER.info("|BancsUpdateOnWorkingHours_Batch|- completed...");
        }
    }

}
