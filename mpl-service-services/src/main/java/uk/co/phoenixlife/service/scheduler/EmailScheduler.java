package uk.co.phoenixlife.service.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import net.javacrumbs.shedlock.core.SchedulerLock;
import uk.co.phoenixlife.service.async.EmailService;
import uk.co.phoenixlife.service.constants.MPLServiceConstants;

/**
 * EmailScheduler.java
 */
@Component
public class EmailScheduler {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailScheduler.class);

    @Autowired
    private EmailService emailService;

    @Value("${skip.batch}")
    private String batchSkipFlag;

    /**
     * Method to send mail update batch
     *
     * @throws Exception
     *             - {@link Exception}
     */
    @Scheduled(cron = "${cron.email}")
    @SchedulerLock(name = "emailBatch", lockAtMostFor = MPLServiceConstants.FIVE_MINUTES,
            lockAtLeastFor = MPLServiceConstants.FIVE_MINUTES)
    public void runEmailService() throws Exception {

        if (MPLServiceConstants.TRUE.equalsIgnoreCase(batchSkipFlag)) {
            LOGGER.info("|Email_Batch|- skipped...");
        } else {
            LOGGER.info("|Email_Batch|- started...");
            emailService.emailBatch();
            LOGGER.info("|Email_Batch|- completed...");
        }
    }
}
