/*
 * MPLServiceScheduler.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.scheduler;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import net.javacrumbs.shedlock.core.SchedulerLock;
import net.sf.ehcache.CacheManager;
import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dao.repository.DocumentAuditRepository;
import uk.co.phoenixlife.service.dao.repository.StaticDataRepository;
import uk.co.phoenixlife.service.utils.CommonUtils;
import uk.co.phoenixlife.service.utils.DateUtils;

/**
 * MPLServiceScheduler.java
 */
@Component
public class MPLServiceScheduler {

    private static final Logger LOGGER = LoggerFactory.getLogger(MPLServiceScheduler.class);

    @Autowired
    private StaticDataRepository staticDataRepo;

    @Autowired
    private DocumentAuditRepository documentRepo;

    @Value("${document.removal.delay}")
    private String delayInHours;

    /**
     * Cron scheduler to refresh static data
     *
     * @throws Exception
     *             - {@link Exception}
     */
    @Scheduled(cron = "${cron.staticdata.refresh}")
    public void dailyStaticDataRefresh() throws Exception {

        CacheManager manager;
        manager = CacheManager.getInstance();
        manager.getCache("static_data_table").removeAll();
        manager.getCache("product_matrix").removeAll();

        LOGGER.info("|StaticDataRefresh_Batch|- cache deleted successfully...");

        staticDataRepo.getAllDistinctGroups()
                .forEach(group -> staticDataRepo.findByStaticDataGroup(group));

        LOGGER.info("|StaticDataRefresh_Batch|- cache refresh completed...");

    }

    /**
     * Cron scheduler to delete document content
     *
     * @throws Exception
     *             - {@link Exception}
     */
    @Scheduled(cron = "${document.removal.cron}")
    @SchedulerLock(name = "docContentRemoval", lockAtMostFor = MPLServiceConstants.FIVE_MINUTES,
            lockAtLeastFor = MPLServiceConstants.FIVE_MINUTES)
    public void deleteDocumentContent() throws Exception {

        List<Long> docIds;

        docIds = documentRepo.documentsToBeDeleted(
                DateUtils.formatDateTime(DateUtils.getCurrentLocalDateTime().minusHours(Integer.parseInt(delayInHours))));

        if (CommonUtils.isValidList(docIds)) {
            documentRepo.deleteDocumentContent(docIds);
        }

        LOGGER.info("|DocContentRemoval_Batch|- content deleted for documents : {}", docIds);
    }

}
