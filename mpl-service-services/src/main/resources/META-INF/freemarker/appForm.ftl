<html>
<head>
<title>
Web Request Acknowledgement
</title>

<style>
body{
			font-family:Arial,sans-serif;
		}

table,td,tr{
page-break-inside:avoid;
}

table#t02,td#t01
{
    border: 1px solid black;
    padding: 5px;
    page-break-inside:avoid;
}
table#t02
{
    border-collapse: collapse;
}
</style>
</head>
<body>
	<div id="container-0fluid" style="width:100%;height:100%;">
		<div id="container" style="width:80%;height:100%;margin:0px auto;">
		
		<!--MAIN AREA OF PDF BEGINS-->
		<div id="PDFPage1">
		<div id="container1" style="width:100%;margin-top:20px auto;float:left;font-size:18px;">
		<b>WEBSITE APPLICATION:<br></br>
		CLAIM FORM TO HAVE MY PHOENIX LIFE PENSION POLICY PAID OUT IN ONE GO</b>
		</div>	
		<br></br>
		<br></br>
		<br></br>
		<br></br>
		<div id="container2" style="width:100%;margin-top:15px auto;float:left;font-size:11px;">
		Thank you for completing the application process through our website on ${date}.
		<br></br>
		<br></br>
		We detail below a summary of the information that you have submitted.
		<br></br>
		<br></br>
		For the purposes of this form &quot;Phoenix Life&quot; shall mean either Phoenix Life Limited or Phoenix Life Assurance Limited, as applicable to your pension policy.
		</div>	
		<br></br>
		<br></br>
		<div id="container3" style="width:100%;margin:0px auto;float:left;margin-top:20px;font-size:12px;">
		<b>Part 1: Personal details</b>
		</div>		
		<br></br>
		<div id="container4" style="width:100%;margin:0px auto;float:left;margin-top:20px;">
			<table width="100%" style="font-size:11px;">
				<tr  width="100%">
					<td width="25%">
						Policy number:
					</td>
					<td width="75%" style="border:1px solid black;height:40px;">
						<div>
						<#if policyNumber??>
						${policyNumber}						
						</#if>
						</div>
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				
				
				
				<tr width="100%">
					<td width="25%">
						Name in full:
					</td>
					<td width="75%" style="border:1px solid black;height:40px;">
						
						<div>
						<#if name??>
						${name}
						</#if>
						</div>
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				
				<tr width="100%">
					<td width="25%">
						National insurance Number:
					</td>
					<td width="75%" style="border:1px solid black;height:40px;">
					<div>
					<#if nino??>
					${nino}
					</#if>
					</div>
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				
				<tr width="100%">
					<td id="td01" width="25%">
						Date of birth:
					</td>
					<td width="75%" style="border:1px solid black;height:40px;">
						<div>
						<#if dob??>
						${dob}
						</#if>
						</div>
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				
				<tr width="100%">
					<td width="25%">
						Primary phone number:
					</td>
					<td width="75%" style="border:1px solid black;height:40px;">
						<div>
						<#if primaryContactNum??>
						${primaryContactNum}
						</#if>
						</div>
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				
				<tr width="100%">
					<td width="25%">
						Secondary phone number:
					</td>
					<td width="75%" style="border:1px solid black;height:40px;">
						<div>
						<#if secContactNum??>
						${secContactNum}
						</#if>
						</div>
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				<tr width="100%">
					<td width="25%">
						Additional phone number :
					</td>
					<td width="75%" style="border:1px solid black;height:40px;">
						<div>
						<#if addContactNum??>
						${addContactNum}
						</#if>
						</div>
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				<tr width="100%">
					<td width="25%">
						E-Mail Address
					</td>
					<td width="75%" style="border:1px solid black;height:40px;">
						<div>
						<#if email??>
						${email}
						</#if>
						</div>
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				<tr width="100%">
					<td width="25%">
						House name or number:
					</td>
					<td width="75%" style="border:1px solid black;height:40px;">
						<div>
						<#if houseName??>
						${houseName}
						<#elseif houseNum??>
						${houseNum}
						</#if>
						</div>
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				<tr width="100%">
					<td width="25%">
						Address line 1:
					</td>
					<td width="75%" style="border:1px solid black;height:40px;">
						<div>
						<#if address1??>
							${address1}
						</#if>
						</div>
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				<tr width="100%">
					<td width="25%">
						City/Town:
					</td>
					<td width="75%" style="border:1px solid black;height:40px;">
						<div>
						<#if cityTown??>
							${cityTown}
						</#if>
						</div>
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				<tr width="100%">
					<td width="25%">
						County:
					</td>
					<td width="75%" style="border:1px solid black;height:40px;">
						<div>
						<#if county??>
							${county}
						</#if>
						</div>
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				<tr width="100%">
					<td width="25%">
						Post Code :
					</td>
					<td width="75%" style="border:1px solid black;height:40px;">
						<div>
						<#if postCode??>
							${postCode}
						</#if>						
						</div>
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						
					</td>
					<td width="75%">
						
					</td>
				</tr>
			</table>
		  </div>
		</div>
		<br></br><br></br><br></br>
		
		<div id="PDFPage2" style="page-break-before: always">
		<div id="container5" style="width:100%;margin:0px auto;float:left;margin-top:20px;font-size:12px;">
		<b>Part 2: Your application summary</b>
		</div>
		<br></br>
		<div id="container6" style="width:100%;margin:0px auto;float:left;margin-top:10px;">
			<table id="t02" width="100%" style="border:1px solid black;font-size:11px;">
				<tr width="100%">
					<td  id="t01" width="40%">
						Option chosen
					</td>
					<td id="t01" width="60%" style="height:40px;">
						<div>
						Take all of my pension savings in one go
						</div>
					</td>
				</tr>
				<tr  width="100%">
					<td id="t01" width="40%">
						Policy number
					</td>
					<td id="t01" width="60%" style="height:40px;">
						<div>
						<#if policyNumber??>
						${policyNumber}						
						</#if>
						</div>
					</td>
				</tr>
			    <tr  width="100%">
					<td  id="t01" width="40%">
						Estimated policy value
					</td>
					<td  id="t01" width="60%" style="height:40px;">
						<div>
						<#if estmtPolicyValue??>
						 ${poundSign} ${estmtPolicyValue}						
						</#if>
						</div>
					</td>
				</tr>
			</table>
		</div>	
		
		
		<br></br>
		
		<div id="container7" style="width:100%;margin:0px auto;float:left;margin-top:10px;font-size:12px;">
		<b>Part 3: Tax Implications</b>
		</div>
		<br></br>
		<div id="container8" style="width:100%;margin:0px auto;float:left;margin-top:10px;">
			<table id="t02" width="100%" style="border:1px solid black;font-size:11px;">
				<tr width="100%">
				<td  id="t01" colspan="2">TAX IMPLICATIONS:</td>				
				</tr>
				<tr  width="100%">
					<td  id="t01" width="40%">
						Tax-free amount
					</td>
					<td  id="t01" width="60%" style="height:40px;">
						<div>
						<#if taxFreeAmount??>
						 ${poundSign} ${taxFreeAmount}						
						</#if>
						</div>
					</td>
				</tr>
				<tr  width="100%">
					<td id="t01"  width="40%">
						Estimated amount of tax deducted
					</td>
					<td  id="t01" width="60%" style="height:40px;">
						<div>
						<#if taxDeducted??>
						 ${poundSign} ${taxDeducted}						
						</#if>
						</div>
					</td>
				</tr>
			    <tr  width="100%">
					<td  id="t01" width="40%">
						Estimated amount payable by Phoenix Life
					</td>
					<td  id="t01" width="60%" style="height:40px;">
						<div>
						<#if estmtAmtPayble??>
					    ${poundSign} ${estmtAmtPayble}						
						</#if>
						</div>
					</td>
				</tr>
			</table>
		</div>

		<br></br>
		<div id="container9" style="width:100%;margin:0px auto;float:left;margin-top:10px;">
			<table id="t02" width="100%" style="border:1px solid black;font-size:11px;">
				<tr width="100%">
				<td  id="t01" colspan="2">IS YOUR POLICY A SMALL POT?</td>				
				</tr>
				<#if smallpot??>
				<#if smallpot==false>
				<tr width="100%">
					<td  id="t01" width="40%">
						Is your policy a small pot?
					</td>
					<td  id="t01" width="60%" style="height:40px;">
						<div>
						You confirmed that you have already received 3 payments under &#39;Small Pots&#39; rules. <br></br>
						This means that the value of this policy cannot be taken as a small pot and your annual allowance will be reduced from ${poundSign}40,000 to ${poundSign}4,000 (2017/18).
						
						</div>
					</td>
				</tr>
				<tr  width="100%">
					<td  id="t01" width="40%">
						Lifetime allowance
					</td>
					<td  id="t01" width="60%" style="height:40px;">
						<div>
						You confirmed you will not use more than your lifetime allowance by taking the pension savings from this policy as a single lump sum.
						</div>
					</td>
				</tr>
								
				<#else>				
			    <tr  width="100%">
					<td  id="t01" width="40%">
						Is your policy a small pot?
					</td>
					<td  id="t01" width="60%" style="height:40px;">
						<div>						
				You confirmed that you <b>have not</b> already received 3 payments under &#39;Small Pots&#39; rules. <br></br>
				This means that this policy will be taken as a small pot and there will be no impact to your annual allowance.
						</div>
					</td>
				</tr>
				</#if>
				
				</#if>
				<tr  width="100%">
					<td  id="t01" colspan="2">We confirmed that the amount quoted in the tax calculator is not guaranteed. We will calculate the amount payable to you using the policy value at the time we receive all correctly completed documentation and your claim is processed. This means that the amount you actually receive might be more or less than is shown above.  </td>
				</tr>
				<tr  width="100%">
					<td  id="t01" width="40%">
						Do you wish to be contacted if the amount payable is more than 5% less than the amount quoted in the tax calculator?
					</td>
					<td width="60%" style="height:40px;padding:5px;">
						<div>						
						<#if amtDropBy5??>
						<#if amtDropBy5==true>
						YES
						<#else>
						NO						
						</#if>
						</#if> 
						</div>
					</td>
				</tr>
			</table>
		</div>
	  </div>		
		<div id="container10" style="width:100%;margin:0px auto;float:left;">
		  <#if mvrApplies?? || earlyExistsApplies??>		  
			<#if mvrApplies=='Y' || earlyExistsApplies=='Y'> 
			<table id="t02" width="100%" style="border:1px solid black;font-size:11px;margin-top:30px;">
			<tr  width="100%">
					<td  id="t01" colspan="2"><b>MARKET VALUE REDUCTION (MVR) AND EARLY EXIT CHARGE:</b></td>
				</tr>
			    <tr  width="100%">
					<td  id="t01" colspan="2">We confirmed that, if you are accessing your pension savings before your selected retirement date, there might be an early exit fee and/or a market value reduction (MVR) applied to your policy.<br></br>
					We asked you:</td>
				</tr>
				<tr  width="100%">
					<td  id="t01" width="40%">
						Are you happy to continue with your application on this basis?
					</td>
					<td  id="t01" width="60%" style="height:40px;">
						<div>						
				        You answered YES
						</div>
					</td>
				</tr>
			  </table>
			</#if>
		  </#if>
		</div>
	<div id="PDFPage3" style="page-break-before: always"></div>
	   <div id="container11" style="width:100%;margin:0px auto;float:left;margin-top:10px;font-size:12px;">
		<b>Part 4: Understanding the risks</b> <br/> <br/>
		<b>UNDERSTANDING THE RISKS:</b>
		</div>
		<p style="font-size:11px;">It is extremely important that you read and understand the risks associated with taking your pension savings as a single lump sum as your decision to do this is <b>irreversible</b>. These risks were confirmed to you during the application process and are also detailed below. </p>
			
		<div id="container12" style="width:100%;margin:0px auto;float:left;margin-top:0px;">
			<table id="t02" width="100%" style="border:1px solid black;font-size:11px;">
				<tr  width="100%">
					<td  id="t01" colspan="2">Your decision to take your pension savings in one go is final. <b> You cannot change your mind.</b></td>
				</tr>
			    <tr  width="100%">
					<td  id="t01" colspan="2">From your fund value of ${poundSign}${estmtPolicyValue}, you will potentially receive ${poundSign}${estmtAmtPayble} from Phoenix Life.</td>
				</tr>
				<tr  width="100%">
					<td  id="t01" colspan="2">There is a risk that you could run out of money for yourself and your dependants.</td>
				</tr>
			    <tr  width="100%">
					<td  id="t01" colspan="2">If you are accessing your pension savings before your selected retirement date, the fund value may be reduced by a market value reduction and/or an early exit charge.</td>
				</tr>
				<tr  width="100%">
					<td  id="t01" colspan="2">Taking a lump sum from your pension may reduce or remove your entitlement to any means tested benefits to which you might be entitled, either now or in the future. </td>
				</tr>
			    <tr  width="100%">
					<td  id="t01" colspan="2">Your creditors, in respect of debts owed by you, might have a claim on any money you take from your pension savings.</td>
				</tr>
				<tr  width="100%">
					<td  id="t01" colspan="2">If you have a student loan, taking a lump sum from your pension might mean that you have to start making loan repayments and/or your existing repayment increases.</td>
				</tr>
				<tr  width="100%">
					<td  id="t01" colspan="2">If you will be applying for a student loan or grant this tax year, your lump sum will be taken into account as part of your income and may reduce any amount of funding you or your children may be awarded.</td>
				</tr>
				<#if smallpot??>
				<#if smallpot==false>				
				<tr  width="100%">
					<td  id="t01" colspan="2">If you take a lump sum from this policy and this is not taken as a small pot, the amount you can invest into other defined contribution pensions, without tax penalty, will reduce from ${poundSign}40,000 to ${poundSign}10,000 a year. <br></br>
					The current limit remains at ${poundSign}10,000 however the expectation is that this may reduce to ${poundSign}4,000. We therefore strongly recommend that you seek independent financial advice if you intend to contribute more than ${poundSign}4,000.
					</td>
				</tr>
				</#if>
				</#if>
				<tr  width="100%">
					<td  id="t01" colspan="2">If you are considering re-investing your pension savings, the charges on your proposed investment could be higher than the charges on your current policy.</td>
				</tr>
				<tr  width="100%">
					<td  id="t01" colspan="2">Failure to consider different ways of taking your benefits and obtaining quotes from other providers may mean that you do not get the most suitable option for your needs.</td>
				</tr>
				<tr  width="100%">
					<td  id="t01" colspan="2">Failure to obtain quotes from other providers and consider an enhanced annuity may mean that you miss out on a much higher pension income. </td>
				</tr>
				<tr  width="100%">
					<td  id="t01" colspan="2">You risk losing all your pension savings if you put your money in unauthorised investment scams. </td>
				</tr>				
			</table>
		</div>
	
	<div style="clear:both;padding-top:10px;">
		<p style="font-size:11px;">You confirmed that you understood this option and the associated risks and you were happy to continue with your application to take all your pension savings as a single lump sum.</p>
		</div>
		<div id="container13" style="width:100%;margin:0px auto;float:left;margin-top:10px; font-size:12px;">
		<b>Part 5: Pension guidance / advice confirmation</b> <br/>
		</div>
		<div id="container14" style="width:100%;margin:0px auto;float:left;margin-top:10px;">
			<table id="t02" width="100%" style="border:1px solid black;font-size:11px;">
				<tr  width="100%">
					<td  id="t01" colspan="2"><b>Pension Guidance/Advice Confirmation</b></td>
				</tr>
				<tr  width="100%">
					<td  id="t01" colspan="2">We previously recommended that before making a decision about what to do with your pension savings, you seek guidance from Pension Wise, a free and impartial guidance service provided by the Government, or get advice from an Independent Financial Adviser.<br></br>We asked you:</td>
				</tr>				
				<tr  width="100%">
					<td  id="t01" width="40%">
						Have you received guidance from Pension Wise about what you could do with the pension savings from this policy?
					</td>
					<td  id="t01" width="60%" style="height:40px;">
						<div>						
						<#if pensionWise??>
						<#if pensionWise==true>
						YES
						<#else>
						NO						
						</#if>
						</#if> 
						</div>
					</td>
				</tr>
				<tr  width="100%">
					<td  id="t01" width="40%">
						Have you received advice from an Independent Financial Adviser about what you could do with the pension savings from this policy?
					</td>
					<td  id="t01" width="60%" style="height:40px;">
						<div>						
						<#if financeAdv??>
						<#if financeAdv==true>
						YES
						<#else>
						NO						
						</#if>
						</#if> 
						</div>
					</td>
				</tr>
			</table>
		</div>			
		<div id="PDFPage" style="page-break-before: always">	
		</div>						
	<div id="container15" style="width:100%;margin:0px auto;float:left;margin-top:20px;font-size:12px;">
		<b>Part 6: Payment method - PLEASE CHECK.</b>
		</div>		
		<br></br>		
		<#if bankAccountNumber??>
		<div id="container16" style="width:100%;margin:0px auto;float:left;margin-top:20px;font-size:11px;">
			<table width="100%">
				<tr  width="100%">
					<td width="25%">
						Bank/Building Society name:
					</td>
					<td width="75%" style="border:1px solid black;height:40px;">
						<div>
						<#if buildingSocietyName??>
						${buildingSocietyName}
						</#if>
						</div>
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						Account holder(s)name: 
					</td>
					<td width="75%" style="border:1px solid black;height:40px;">
						<div>
						<#if bankAccHolderName??>
						${bankAccHolderName}
						</#if>
						</div>
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						Account number:
					</td>
					<td width="75%" style="border:1px solid black;height:40px;">
						<div>
						<#if bankAccountNumber??>
						${bankAccountNumber}
						</#if>
						</div>
					</td>
				</tr>
				<tr  width="100%">
					<td width="25%">
						Sort code:  
					</td>
					<td width="75%" style="border:1px solid black;height:40px;">
						<div>
						<#if bankSortCode??>
						${bankSortCode}
						</#if>
						</div>
					</td>
				</tr>
			 <tr  width="100%">
					<td width="25%">
						Roll number:(Building Society only)  
					</td>
					<td width="75%" style="border:1px solid black;height:40px;">
						<div>
						<#if rollNumber??>
						${rollNumber}
						</#if>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<#else>
		<br></br>
		<p style="font-size:11px;">You confirmed that you would like us to pay your pension savings by a cheque. This will be sent to the address provided on this form, as long as it matches our system records.</p>
		<br></br>
		</#if>			
		<div id="container17" style="width:100%;margin:0px auto;float:left;margin-top:0px;font-size:12px;">
		<b>Part 7:  DECLARATION - PLEASE READ VERY CAREFULLY.</b>				
		<br></br>
		<br></br>
		<b>I hereby declare that:</b>
		<br></br>
		</div>
		<p style="font-size:11px;">I agree to the payment of benefits in line with my instructions contained in this form.</p>
		<p style="font-size:11px;">I understand that providing Phoenix Life false or incomplete information or the making of a false declaration may result in tax charges and other penalties.</p>
		<p style="font-size:11px;">I agree that payments of benefits in accordance with my instructions in this form will discharge Phoenix Life  of its obligation to make any further payments on the policy(ies). For the avoidance of doubt, this discharge shall not prohibit me from raising a claim relating to any act or omission by Phoenix Life in relation to the policy.</p>
		<p style="font-size:11px;">I understand that the exact value of the benefits that I will receive is not guaranteed. The amount that will be paid will be the value of the policy at the time Phoenix Life receives all correctly completed documentation and my claim is processed. </p>
		<p style="font-size:11px;">I am the legal owner of the policy. I am legally entitled to receive the benefits of the policy. I have never been adjudged bankrupt and there are no court orders affecting my policy.</p>
		<p style="font-size:11px;">I request, and agree to, any alteration to the terms and conditions of this policy which are necessary to allow a single lump sum to be paid.</p>
		<p style="font-size:11px;">In return for Phoenix Life paying the proceeds from the policy to me, I promise that I will be responsible for any losses and/or expenses which are the result, and which a reasonable person would consider to be the probable result, of any untrue, misleading or inaccurate information carelessly given by me, or on my behalf, either in this form or with respect to the benefits from the policy. I also promise that I will be responsible for any losses and/or expenses which are the result of any untrue, misleading, or inaccurate information deliberately given by me, or on my behalf, either in the form or with respect to the benefits of this policy. </p>
		<p style="font-size:11px;"><b><u>I agree that by submitting this application form electronically, it has the same force and effect as if I had signed a paper application form  in person.</u></b></p>
		<br></br>
		<br></br>
		<div align="right">
		<table width="30%" align="right" style="font-size:11px;">
				<tr  width="30%">
					<td width="30%" style="border:1px solid black;height:40px;text-align:center;"> 
					${date}
					</td>
					</tr>
		</table>
		</div>
	
		<!--MAIN AREA OF PDF ENDS-->
		</div>
		
	</div>
</body>
</html>	