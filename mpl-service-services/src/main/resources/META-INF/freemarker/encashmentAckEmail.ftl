<html>
<head>
</head>
<body style="font-size:20px !important;margin:0px;padding:0px;">
<p>Thank you for your online application regarding your Phoenix Life pension policy.</p></br>

<p><b>What happens now?</b></p>

<p>If everything is in order, dependent on what you have selected you will either receive your payment  into the nominated bank account or a cheque within 5 to 10 working days.</p>

<p>Should we need to make contact with you we will use the telephone number provided as part of your application.</p>

<p>We will send you a letter confirming that the payment has been made.</p>

<p>This is a system generated message. Please do not respond to this message.</p><br>


<p>Kind regards</p>

<p>Phoenix Life Customer Support Team </p>

</body>
</html>	