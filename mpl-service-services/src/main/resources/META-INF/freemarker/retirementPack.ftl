<html>
	
	<head>
	<title>
	DCH Retirement Pack
	</title>
	<style>
		body{
			font-family:Arial,sans-serif;
		}
		table {
    		border-collapse: collapse;
		}

		table, td {
    		border: 1px solid black;
		}
	</style>
	</head>
	<body>
		<div style="width:100%;height:100px;">
			<div style="float:left;">
				<img style="height:40px;width:180px;" src="logo.jpg"></img>
			</div>
			
			<div style="float:right;">
				<span style="font-size:11px;float:right;">LYNCH WOOD PARK</span><br></br>
				<span style="font-size:11px;float:right;">LYNCH WOOD</span><br></br>
				<span style="font-size:11px;float:right;">PETERBOROUGH</span><br></br>
				<span style="font-size:11px;float:right;">PE2 6FY</span><br></br><br></br>
				<span style="font-size:11px;float:right;">WWW.PHOENIXLIFE.CO.UK</span><br></br>
			</div>
		</div>
		<div style="clear:both;width:100%;height:120px;">
			<div style="float:left;">
				<span style="font-size:11px;"><#if planHolderName??>
												${planHolderName}
							                  </#if>
				</span><br></br>
				<#if houseName??><span style="font-size:11px;">${houseName} </span></#if>
				<#if houseNumber??><span style="font-size:11px;">${houseNumber} </span></#if>
				<#if address1??><span style="font-size:11px;">${address1}</span><br></br></#if>
				<#if address2??><span style="font-size:11px;">${address2}</span><br></br></#if>
				<#if cityTown??><span style="font-size:11px;">${cityTown}</span><br></br></#if>
				<#if county??><span style="font-size:11px;">${county}</span><br></br></#if>
				<#if country??><span style="font-size:11px;">${country}</span><br></br></#if>
				<#if postcode??><span style="font-size:11px;">${postcode}</span></#if>
			 
			</div>
			<div style="float:right;">
				<span style="font-weight: bold;font-size:12px;float:right;">Customer Contact Centre</span><br></br>
				<span style="font-size:11px;float:right;">0345 070 0029</span><br></br><br></br>
				<span style="font-weight: bold;font-size:12px;float:right;">Plan number</span><br></br>
				<span style="font-size:11px;float:right;"><#if policyNumber??>
							${policyNumber}
							</#if></span>
			</div>
		</div><br></br><br></br>
		<div id="mainContainer" style="margin:0px auto;width:100%;">
		<span style="font-size:11px;">${date}</span>
		<div style="height:10px;"></div>
			<p style="font-size:18px; text-decoration: underline;font-weight: bold;">RETIREMENT PACK</p>
			<div style="height:10px;"></div>
			<span style="font-size:11px;">Dear ${title!"Not Set"} ${lastName!"Not Set"}</span>
			<div style="height:20px;"></div>
			<div style="clear:both">
				<p style="font-size:11px;">Thank you for requesting details of the pension savings and options available to you under your plan which are detailed below. </p>
				<p style="font-size:12px;"><b>Help with your options</b></p>
			</div>
				<p style="font-size:11px; margin-bottom:0px !important">
					The government has set up Pension Wise - a free and impartial service to help you understand your options. Pension Wise is available:
					<ul>
						<li style="font-size:11px;">On-line at www.pensionwise.gov.uk</li>
						<li style="font-size:11px;">By telephone 0800 280 8880, and</li>
						<li style="font-size:11px;">If you prefer it, face to face, from Citizens Advice.</li>
					</ul>
				</p>
				<p style="font-size:11px;">
					You can find out more about Pension Wise further down in this Retirement Pack.
				</p>
				<div style="height:20px;"></div>
				<table style="width:100%;border: 3px solid black;">
					<tr>
						<td align="center" style="font-size:11px;padding:7px;font-weight: bold;">We strongly recommend using Pension Wise or seeking regulated financial advice. This is a key part of protecting yourself and your family when making an important and often irreversible decisions on your pension options.</td>
					</tr>
				</table>
			
			<p style="font-size:11px;">To get the best from Pension Wise you should have the below Pension Plan Summary to hand when you contact them.</p>
			
			<div style="clear:both;margin-top:30px;">
				<p style="font-size:12px;"><b>Pension plan summary</b></p>
				<table style="width:100%">
					<tr>
						<td style="padding:5px;font-size:11px;">
						Planholder's name
						</td>
						<td style="padding:5px;font-size:11px;">
						 <#if planHolderName??>
							${planHolderName!"Not Set"}
						</#if>
						</td>
					</tr>
					<tr>
						<td style="padding:5px;font-size:11px;">
						Plan number
						</td>
						<td style="padding:5px;font-size:11px;">
						 <#if policyNumber??>
							${policyNumber!"Not Set"}
						 </#if>
						</td>
					</tr>
					
					<tr>
						<td style="padding:5px;font-size:11px;">
						Assumed pension date
						</td>
						<td style="padding:5px;font-size:11px;">
						 <#if date??>
							${date!"Not Set"}
						 </#if>
						</td>
					</tr>
					<tr>
						<td style="padding:5px;font-size:11px;">
						Estimated value of pension savings <#if deductions??>(after deductions)</#if>
						</td>
						<td style="padding:5px;font-size:11px;">
							<#if pensionSavings??>
								${pound}${pensionSavings?string("0.00")} at your assumed pension date						
				       		</#if>
					    </td>
					</tr>
                <tr>
						<td style="padding:5px;font-size:11px;">
						Plan type
						</td>
						<td style="padding:5px;font-size:11px;">Defined contribution (money purchase)</td>
					</tr>
					<tr>
						<td style="padding:5px;font-size:11px;">
						Does a market value reduction apply?
						</td>
						<td style="padding:5px;font-size:11px;"><#if MVA??>Yes, please see market value reduction notes below<#else>No</#if></td>
					</tr>
					<tr>
						<td style="padding:5px;font-size:11px;">
						Does an early exit charge apply?
						</td>
						<td style="padding:5px;font-size:11px;"><#if penaltyValue??>Yes, please see early exit charge notes below <#else>No</#if></td>
					</tr>
					<tr>
						<td style="padding:5px;font-size:11px;">
						Guaranteed annuity rate or benefit available?
						</td>
						<td style="padding:5px;font-size:11px;">No</td>
					</tr>
					<#if penaltyValue??><tr>
						<td style="padding:5px;font-size:11px;">
						Life Cover
						</td>
						<td style="padding:5px;font-size:11px;">Please read the <b>Life cover</b> notes below</td>
					</tr></#if>
                
                </table>		
			</div>
			<br></br><p style="font-size:11px;">The above estimated value of pension savings is subject to change and is not guaranteed.</p>
			<br></br><br></br><br></br>
			<p style="text-align:center;font-size:10px;">&nbsp;&nbsp;Phoenix Life Limited No.1016269 and Phoenix Life Assurance Limited No.1419 are authorised by the Prudential Regulation Authority and <br/> regulated by the Financial Conduct Authority and the Prudential Regulation Authority. Both companies are registered in England and have their <br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;registered office at: 1 Wythall Green Way, Wythall, Birmingham, B47 6WG. www.phoenixlife.co.uk</p>
<div style="page-break-before: always">

</div>
			<#if fundName??>
				
					<p style="font-size:12px;"><b>Guaranteed annual bonus</b></p>
		
				<p style="font-size:11px;">Your pension savings are invested in the 'Unitised With-Profit Series 1 fund' and has a guaranteed annual bonus of 4% applied to accumulation units every year until age 75 is reached.  This guarantee would be lost if you decide to cash-in your plan.</p>
		
			</#if>
			<#if MVA??>
			<div style="clear:both;margin-top:30px;">
				<p style="font-size:12px;"><b>Market Value Reduction (MVR)</b></p>
			</div>
		
			<p style="font-size:11px;">A Market Value Reduction (MVR) currently applies to the  pension savings, as shown below.  An MVR may be used to reduce the value of the savings where the current value is greater than the  fair share of the underlying investments.</p>
			<p style="font-size:11px;">The pension plan has at least one date when we guarantee that no MVR will be applied, known as an MVR free date.  It may be that an MVR may not be applied in the future.  If you'd like to know more about the MVR free date, please contact us.</p>
	
			</#if>
			<#if penaltyValue??>
			<p style="font-size:11px;">An early exit charge applies to the savings as shown below. This charge would not apply if you were to take your pension savings at, or after, your original retirement date.</p>
			</#if>
			<#if penaltyValue?? || MVA??>
				<p style="font-size:11px;">Estimated pension savings calculation as at ${date}<br></br>
					The estimated pension savings value available was calculated as follows:
				</p>
				<table style="width:100%">
					<tr>
						<td style="padding:5px;font-size:11px;">Plan number</td>
						<td style="padding:5px;font-size:11px;">Estimated value of pension savings</td>
						<#if penaltyValue??><td style="padding:5px;font-size:11px;">Early exit charge</td></#if>
						<#if MVA??><td style="padding:5px;font-size:11px;">MVR</td></#if>
						<#if MVA?? || penaltyValue??><td style="padding:5px;font-size:11px;">Estimated value of pension savings <#if deductions??>(after deductions)</#if></td></#if>
					</tr>
					<tr>
						<#if policyNumber??><td style="padding:5px;font-size:11px;">
							${policyNumber}
						 </td></#if>
						 <#if beforeDeductionFundValue??><td style="padding:5px;font-size:11px;">${beforeDeductionFundValue?string("0.00")}</td></#if>
						 <#if penaltyValue??><td style="padding:5px;font-size:11px;">${penaltyValue?string("0.00")}</td></#if>
						 <#if MVA??><td style="padding:5px;font-size:11px;">${MVA?string("0.00")}</td></#if>
						 <#if pensionSavings??><td style="padding:5px;font-size:11px;">${pensionSavings?string("0.00")}</td></#if>
					</tr>
				</table>
				<p style="font-size:11px;">The above estimated value of pension savings is subject to change and is not guaranteed.</p>
			</#if>
		</div>
		<div>
			<#if lifeCover??>
				<div style="clear:both;margin-top:30px;">
						<p style="font-size:12px;"><b>Life Cover</b></p>
				</div>
				<p style="font-size:11px;margin-bottom:0px !important">The current contribution includes an element of life cover separate to the pension savings.  Once you take your pension savings  the following choices in relation to the life cover element are available:</p>
				<ul>
					<li style="font-size:11px;">Continue the life cover at the current level</li>
					<li style="font-size:11px;">Reduce the amount of life cover.  Please contact us for a quotation.  The premium will be amended when we receive your instructions.</li>
					<li style="font-size:11px;">Cancel the life cover from the date you take your pension savings </li>
				</ul>
				<p style="font-size:11px;">If you require any further  information on these options, please contact us.</p>
				<p style="font-size:11px;">If we do not receive instructions before the payment of your pension savings, we will continue the life cover at the current level. If you intend to cancel the life cover please remember to cancel the direct debit or bank standing order, but not before the payment has been made.</p>
				<p style="font-size:11px;">It may not be possible to reinstate life cover contributions once they have been cancelled.  If the life cover is reduced it cannot be increased in the future.</p>	
			</#if>
			
			<div style="clear:both;margin-top:30px;">
					<p style="font-size:12px;"><b>Pension options</b></p>
			</div>
			<div style="clear:both">
					<p style="font-size:11px;margin-bottom:0px !important">
						Before making a decision about what you would like to do with your pension savings it's important for you to consider all of the options that are available to you. A summary of your options is detailed here and further information on each option can be found in the Money Advice Service brochure 'Your Pension - it's time to choose.' further down in this Retirement Pack.
						<ul>
							<li style="font-size:11px;">Keep your pension savings where they are </li>
							<li style="font-size:11px;">Get a guaranteed income for life (an annuity) from a provider of your choice, or from us </li>
							<li style="font-size:11px;">Get a flexible retirement income, such as income drawdown</li>
							<li style="font-size:11px;">Take your pension savings as a number of lump sums  </li>
							<li style="font-size:11px;">Take all of your pension savings in one go </li>
							<li style="font-size:11px;">Choose more than one option and mix them </li>
						</ul>
					</p>
					<div style="height:20px;"></div>
					<table style="width:100%;border: 3px solid black;">
						<tr>
							<td align="center" style="width:100%;padding:7px;font-weight: bold;font-size:11px;">Please read the Money Advice Service brochure 'Your Pension - it's time to choose' which can be found further down in this Retirement Pack, to help you make an informed decision about what to do with your pension savings and how to shop around.</td>
						</tr>
					</table>
					
					<p style="font-size:11px;">Please note, if you are suffering from an advanced or rapidly progressing illness or injury where your life expectancy is less than 12 months from the date confirmatory medical evidence is obtained then you may be able to have some or all of your pension savings paid as a one-off (serious ill health) lump sum. Please contact us for the appropriate forms if you think you may be eligible.</p>
			</div>
			<p style="font-size:11px;">A serious ill health lump sum is tax-free if paid before your 75th birthday.</p>
		</div>
		<#if (penaltyValue?? || MVA??) && lifeCover??>
		<div style="page-break-before: always"></div>
			</#if>
		<div style="clear:both;">
				<p style="font-size:12px;"><b>Shopping around</b></p>
		</div>
			<p style="font-size:11px;">Other providers may offer different options for your pension savings choices and may calculate the amount of income you could receive differently. </p>
			<p style="font-size:11px;">They may offer you a higher level of retirement income and by shopping around you may find you're entitled to receive an 'enhanced' guaranteed income or  'annuity' from another provider. </p>
			<p style="font-size:11px;">Enhanced annuities pay a higher level of income than standard ones if you have a health condition, are on medication or have a lifestyle that could affect how long you might live. For further information please see the Money Advice Service brochure - 'Your Pension it's time to choose' further down in this Retirement Pack.</p>
		
		<#if !lifeCover?? && fundName?? && penaltyValue?? && MVA??>
		<div style="page-break-before: always"></div>
			</#if>
		<div style="height:10px;"></div>
				<table style="width:100%;border: 3px solid black;">
					<tr>
						<td align="center" style="padding:7px;font-size:11px;font-weight: bold;">It is important that you consider all of your options and shop around for the best deal. The right one for you depends on your circumstances and retirement needs. Other providers might offer products more appropriate to your needs and circumstances and may offer a higher level of retirement income. For details on how to shop around please see the Money Advice Service brochure 'Your Pension - it's time to choose' further down in this Retirement Pack.</td>
					</tr>
				</table>
		<#if regularContributions??>
				<div style="clear:both;margin-top:30px;">
					<br></br><p style="font-size:12px;"><b>Regular paying contributions</b></p>
				</div>
				<p style="font-size:11px;">If you intend to take the savings, please remember to cancel the direct debit or bank standing order.</p>

			</#if>
			<#if (!fundName??  && !lifeCover?? && (MVA?? || penaltyValue??)) || (lifeCover?? && !MVA?? && !penaltyValue??)>
		<div style="page-break-before: always"></div>
			</#if>
		<div style="clear:both;margin-top:30px;">
				<p style="font-size:12px;"><b>Pension scams</b></p>
		</div>
		<div style="height:10px;"></div>
		<table style="width:100%;border: 3px solid black;">
					<tr>
						<td align="center" style="padding:7px;font-size:11px;font-weight: bold;">BEWARE! You risk losing all your pension savings if you put your money in unauthorised investment scams. For further information on pension scams please see the enclosed pension scams leaflet further down in this Retirement Pack.</td>
					</tr>
		</table>
		<div style="clear:both;margin-top:30px;">
				<br></br><p style="font-size:11px;"><b>Contact us if you have any questions.</b></p>
		</div>
		<p style="font-size:11px;">If you require any further information, please call us on 0345 070 0029. We're open Monday to Friday, 8.30am to 5.30pm excluding Bank Holidays.  Calls to 03 numbers cost no more than a national rate call to 01 or 02 numbers and are included in inclusive minutes and discount schemes in the same way.  We may record or monitor calls for your protection.  Alternatively, you can go to the contact us section of our website www.phoenixlife.co.uk.  Please have your plan number to hand as this will help us deal with your enquiry.  We may also ask you for personal information like your address, date of birth and National Insurance number.  This is so we can confirm your identify and locate your plan.</p>
		<p style="font-size:11px;">Yours faithfully</p>
		<img style="height:60px;width:200px" src="MilindDhuru.jpg"></img><br></br>
		<span style="font-size:11px;">Milind Dhuru</span><br></br>
		<span style="font-size:11px;">Operations Director</span>
	</body>
</html>	