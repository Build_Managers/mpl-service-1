package uk.co.phoenixlife.service.restcontroller;

import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import uk.co.phoenixlife.service.dto.policy.CustomerDTO;
import uk.co.phoenixlife.service.interfaces.EncashmentService;


public class BancsPostCallsControllerTest {
	
	@InjectMocks
    private BancsPostCallsController bancsPostCallsController;
	@org.mockito.Mock
    private EncashmentService encashmentService;
    @BeforeTest
    public void runBeforeEveryTest() {
    MockitoAnnotations.initMocks(this);
    }
    @AfterTest         
    public void runAfterEveryTest() {
    	bancsPostCallsController = null;
    }
    
    @Test(enabled = true)
    public void testGetBancsPostCallsStatus() throws Exception{
    	String string = new String();
    	CustomerDTO customerDTO = new CustomerDTO();
    	//Mockito.when(encashmentService.getBancsPostCallsStatus(string, customerDTO)).thenReturn(null);
    	 bancsPostCallsController.getBancsPostCallsStatus(string, customerDTO);
    	 Assert.assertNotNull(bancsPostCallsController);
    }

}
