package uk.co.phoenixlife.service.restcontroller;

import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.bankvalidation.BankValidationRequest;
import uk.co.phoenixlife.service.dto.bankvalidation.BankValidationService;

public class BankValidationRestControllerTest {
	
	@InjectMocks
    private BankValidationRestController bankValidationRestController;
	
	@org.mockito.Mock
	private BankValidationService bankValidationService;
   
	@BeforeTest
    public void runBeforeEveryTest() {
		MockitoAnnotations.initMocks(this);
    }
	
    @AfterTest         
    public void runAfterEveryTest() {
    	bankValidationRestController = null;
    }
    
    @Test(enabled = true)
    public void testValidateBankDetails() throws Exception{
    	
    	BankValidationRequest bankValidationRequest = new BankValidationRequest();
    	bankValidationRestController.validateBankDetails(bankValidationRequest);
    	Assert.assertNotNull(bankValidationRestController);
    }

}
