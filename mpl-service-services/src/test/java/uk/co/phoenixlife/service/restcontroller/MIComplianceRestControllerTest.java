package uk.co.phoenixlife.service.restcontroller;

import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import uk.co.phoenixlife.service.dto.MiErrorRequestDTO;
import uk.co.phoenixlife.service.interfaces.MIComplianceServiceInterface;

public class MIComplianceRestControllerTest {
	@InjectMocks
    private MIComplianceRestController miComplianceRestController;
	@org.mockito.Mock
    private MIComplianceServiceInterface miComplianceServiceInterface;
    @BeforeTest
    public void runBeforeEveryTest() {
    MockitoAnnotations.initMocks(this);
    }
    @AfterTest         
    public void runAfterEveryTest() {
    	miComplianceRestController = null;
    }
    
    @Test(enabled = true)
    public void testsubmitErrorListDTO() throws Exception{
    	MiErrorRequestDTO miErrorRequestDTO = new MiErrorRequestDTO();
    	// Mockito.when(miComplianceServiceInterface.submitErrorListDTO(miErrorRequestDTO)).thenReturn(null);
    	 miComplianceRestController.submitErrorListDTO(miErrorRequestDTO);
    	 Assert.assertNotNull(miComplianceRestController);
    }

}
