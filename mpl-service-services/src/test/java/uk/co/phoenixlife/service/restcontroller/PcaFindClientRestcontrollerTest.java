package uk.co.phoenixlife.service.restcontroller;

import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.interfaces.PcaFindClientService;

public class PcaFindClientRestcontrollerTest {
	
	@InjectMocks
    private PcaFindClientRestcontroller pcaFindClientRestcontroller;
	@org.mockito.Mock
    private PcaFindClientService pcaFindClientService;
    @BeforeTest
    public void runBeforeEveryTest() {
    MockitoAnnotations.initMocks(this);
    }
    @AfterTest         
    public void runAfterEveryTest() {
    	pcaFindClientRestcontroller = null;
    }
    
    @Test(enabled = true)
    public void testfind() throws Exception{
    	String string = new String();
    	 Mockito.when(pcaFindClientService.find(string)).thenReturn(null);
    	 pcaFindClientRestcontroller.find(string);
    	 Assert.assertNotNull(pcaFindClientRestcontroller);
    }

}
