package uk.co.phoenixlife.service.restcontroller;

import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100;
import uk.co.phoenixlife.service.interfaces.PcaRetriveClientService;

public class PcaRetriveClientRestControllerTest {
	@InjectMocks
    private PcaRetriveClientRestController pcaRetriveClientRestController;
	@org.mockito.Mock
    private PcaRetriveClientService pcaRetriveClientService;
    @BeforeTest
    public void runBeforeEveryTest() {
    MockitoAnnotations.initMocks(this);
    }
    @AfterTest         
    public void runAfterEveryTest() {
    	pcaRetriveClientRestController = null;
    }
    
    @Test(enabled = true)
    public void testretrieve() throws Exception{
    	CaptureInteractiveRetrieveV100 captureInteractiveRetrieveV100 = new CaptureInteractiveRetrieveV100();
    	 Mockito.when(pcaRetriveClientService.retrieve(captureInteractiveRetrieveV100)).thenReturn(null);
    	 pcaRetriveClientRestController.retrieve(captureInteractiveRetrieveV100);
    	 Assert.assertNotNull(pcaRetriveClientRestController);
    }


}
