package uk.co.phoenixlife.service.restcontroller;

import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100;
import uk.co.phoenixlife.service.interfaces.PostCodeService;

public class PostCodeRestControllerTest {
	@InjectMocks
    private PostCodeRestController postCodeRestController;
	@org.mockito.Mock
    private PostCodeService postCodeService;
    @BeforeTest
    public void runBeforeEveryTest() {
    MockitoAnnotations.initMocks(this);
    }
    @AfterTest         
    public void runAfterEveryTest() {
    	postCodeRestController = null;
    }
    
    @Test(enabled = true)
    public void testRetrieve() throws Exception{
    	CaptureInteractiveRetrieveV100 captureInteractiveRetrieveV100 = new CaptureInteractiveRetrieveV100();
    	 Mockito.when(postCodeService.retrieveAddress(captureInteractiveRetrieveV100)).thenReturn(null);
    	 postCodeRestController.retrieve(captureInteractiveRetrieveV100);
    	 Assert.assertNotNull(postCodeRestController);
    }
    
    @Test(enabled = true)
    public void testFindAddress() throws Exception{
    	String string = new String();
    	 Mockito.when(postCodeService.findAddress(string)).thenReturn(null);
    	 postCodeRestController.findAddress(string);
    	 Assert.assertNotNull(postCodeRestController);
    }

}
