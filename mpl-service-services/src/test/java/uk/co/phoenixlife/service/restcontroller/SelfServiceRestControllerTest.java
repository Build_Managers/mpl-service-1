package uk.co.phoenixlife.service.restcontroller;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import uk.co.phoenixlife.service.dto.SecurityQuestionsForAUserDTO;
import uk.co.phoenixlife.service.interfaces.SelfService;

public class SelfServiceRestControllerTest {
	
	@InjectMocks
    private SelfServiceRestController selfServiceRestController;
	@org.mockito.Mock
    private SelfService selfService;
    @BeforeTest
    public void runBeforeEveryTest() {
    MockitoAnnotations.initMocks(this);
    }
    @AfterTest         
    public void runAfterEveryTest() {
    selfServiceRestController = null;
    }
    
    @Test(enabled = true)
    public void testgetSecurityQuestionsForUser() throws Exception{
    	String string = new String();
    	 Mockito.when(selfService.getSecurityQuestions(string)).thenReturn(null);
    	 selfServiceRestController.getSecurityQuestionsForUser(string);
    	 Assert.assertNotNull(selfServiceRestController);
    }
    
    @Test(enabled = true)
    public void testCheckAnswersForUser() throws Exception{
    	SecurityQuestionsForAUserDTO securityQuestionsForAUserDTO = new SecurityQuestionsForAUserDTO();
    	 Mockito.when(selfService.checkAnswerOfSecurityQuestion(securityQuestionsForAUserDTO)).thenReturn(true);
    	 selfServiceRestController.checkAnswersForUser(securityQuestionsForAUserDTO);
    	 Assert.assertNotNull(selfServiceRestController);
    }
    
    
}
