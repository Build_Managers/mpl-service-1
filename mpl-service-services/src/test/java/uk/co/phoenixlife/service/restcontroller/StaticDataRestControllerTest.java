package uk.co.phoenixlife.service.restcontroller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import uk.co.phoenixlife.service.PolicyRepositoryService;
import uk.co.phoenixlife.service.dto.staticdata.StaticDataRequestDTO;
import uk.co.phoenixlife.service.dto.staticdata.StaticDataService;
import uk.co.phoenixlife.service.dto.staticdata.StaticIDVDataDTO;
import uk.co.phoenixlife.service.restcontroller.StaticDataRestController;


public class StaticDataRestControllerTest {

    @InjectMocks
    private StaticDataRestController staticDataRestController;

    @org.mockito.Mock
    private StaticDataService staticDataService;
    
    @BeforeTest
    public void runBeforeEveryTest() {
        MockitoAnnotations.initMocks(this);

    }

    @AfterTest
    public void runAfterEveryTest() {
        staticDataRestController = null;
    }

   /* @Test(enabled = true)
    public void testRefreshStaticData() throws Exception {
        StaticDataRequestDTO request = new StaticDataRequestDTO();
        request.setStartFlag(true);
        staticDataRestController.refreshStaticData(request);
        Assert.assertNotNull(staticDataRestController);
    }*/

    @Test(enabled = true)
    public void testGetIDVQuestionsData() throws Exception {
        StaticDataRequestDTO request = new StaticDataRequestDTO();
        List<StaticIDVDataDTO> staticIDVDataDTO = new ArrayList<StaticIDVDataDTO>();
        Mockito.when(staticDataService.getIDVQuestionsData()).thenReturn(staticIDVDataDTO);
        staticDataRestController.getIDVQuestionsData(request);
        Assert.assertNotNull(staticDataRestController);
    }
    
    @Test(enabled = true)
    public void testGetStaticDataMap() throws Exception {
        StaticDataRequestDTO request = new StaticDataRequestDTO();
        Map<String, String> pStaticDataMap = null;
        request.setGroupId("abc");
        Mockito.when(staticDataService.getStaticDataMap(request.getGroupId())).thenReturn(pStaticDataMap);
        staticDataRestController.getStaticDataMap("abc");
        Assert.assertNotNull(staticDataRestController);
    }
    
    @Test(enabled = true)
    public void testGetStaticDataValue() throws Exception {
        StaticDataRequestDTO request = new StaticDataRequestDTO();
        request.setGroupId("abc");
        request.setKeyValue("aaa");
        Mockito.when(staticDataService.getStaticDataValue(request.getGroupId(), request.getKeyValue())).thenReturn("abc");
        staticDataRestController.getStaticDataValue("abc", "aaa");
        Assert.assertNotNull(staticDataRestController);
    }

}
