package uk.co.phoenixlife.service.webclient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dto.activation.CustomerActivationDTO;
import uk.co.phoenixlife.service.dto.activation.CustomerDetails;
import uk.co.phoenixlife.service.dto.activation.CustomerPersonalDetailStatusResponse;
import uk.co.phoenixlife.service.dto.activation.PolicyExistLockStatus;
import uk.co.phoenixlife.service.dto.activation.VerificationCheckAnswerWrapperDTO;
import uk.co.phoenixlife.service.dto.activation.VerificationDisplayEligibleQuestionWrapperDTO;
import uk.co.phoenixlife.service.dto.activation.VerificationMapAndLockedCountDTO;
import uk.co.phoenixlife.service.dto.activation.VerificationReturnValuesDTO;
import uk.co.phoenixlife.service.interfaces.AccountActivationService;

@Component
public class AccountActivationWebClient implements AccountActivationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountActivationWebClient.class);

    @Autowired
    private RestTemplate restTemplate;

    @Value("${mpl.service}")
    private String serviceUrl;

    @Override
    public PolicyExistLockStatus getPolicyExist(final CustomerActivationDTO customerActivationDTO, final String xGUID)
            throws Exception {
        LOGGER.info("{} Initiating service call for getPolicyExists", xGUID);
        ResponseEntity<PolicyExistLockStatus> policyExistLockStatusResponse;
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
        HttpEntity<Object> entity;
        entity = new HttpEntity<Object>(customerActivationDTO, headers);

        policyExistLockStatusResponse = restTemplate.exchange(
                serviceUrl + "MPLService/accountActivation/policyExistCheck",
                HttpMethod.POST, entity, PolicyExistLockStatus.class);
        LOGGER.info("{} Policy Exists Service call success returning response", xGUID);
        return policyExistLockStatusResponse.getBody();
    }

    @Override
    public CustomerPersonalDetailStatusResponse validatePersonalDetail(final CustomerActivationDTO customerPersonalDetail,
            final String xGUID) throws Exception {

        LOGGER.info("{} Validating Personal Details", xGUID);
        ResponseEntity<CustomerPersonalDetailStatusResponse> customerPersonalDetailStatusListResponse;
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
        HttpEntity<Object> entity;
        entity = new HttpEntity<Object>(customerPersonalDetail, headers);

        customerPersonalDetailStatusListResponse = restTemplate.exchange(
                serviceUrl + "MPLService/accountActivation/checkPersonalDetails", HttpMethod.POST, entity,
                CustomerPersonalDetailStatusResponse.class);
        LOGGER.debug("{} Returning result of Personal Details Validation", xGUID);
        return customerPersonalDetailStatusListResponse.getBody();
    }

    @Override
    public CustomerDetails validatePostCodeGoneAwayAndLifeStatusFlag(final CustomerDetails customerDetails, final String xGUID) {
        ResponseEntity<CustomerDetails> customerDetailsResponse;
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
        HttpEntity<Object> entity;
        entity = new HttpEntity<Object>(customerDetails, headers);

        customerDetailsResponse = restTemplate.exchange(
                serviceUrl + "MPLService/accountActivation/checkPostCodeGoneAwayAndLifeStatus", HttpMethod.POST, entity,
                CustomerDetails.class);
        return customerDetailsResponse.getBody();
    }

    @Override
    public VerificationReturnValuesDTO getQuestionAnswer(final CustomerActivationDTO customerPersonalDetail,
            final String xGUID)
            throws Exception {
        ResponseEntity<VerificationReturnValuesDTO> verificationReturnValuesResponse;
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
        HttpEntity<Object> entity;
        entity = new HttpEntity<Object>(customerPersonalDetail, headers);

        verificationReturnValuesResponse = restTemplate.exchange(
                serviceUrl + "MPLService/accountActivation/getQuestionAnswer", HttpMethod.POST, entity,
                VerificationReturnValuesDTO.class);
        return verificationReturnValuesResponse.getBody();
    }

    @Override
    public VerificationMapAndLockedCountDTO
           checkAnswer(final VerificationCheckAnswerWrapperDTO verificationCheckAnswerWrapperDTO, final String xGUID)
                   throws Exception {
        LOGGER.info("{} Initiating service call of checkAnswer method", xGUID);
        ResponseEntity<VerificationMapAndLockedCountDTO> checkAnswerResponse;
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
        HttpEntity<Object> entity;
        entity = new HttpEntity<Object>(verificationCheckAnswerWrapperDTO, headers);

        checkAnswerResponse = restTemplate.exchange(
                serviceUrl + "MPLService/accountActivation/checkAnswer", HttpMethod.POST, entity,
                VerificationMapAndLockedCountDTO.class);
        LOGGER.info("{} Returning from service call of checkAnswer method", xGUID);
        return checkAnswerResponse.getBody();
    }

    @Override
    public VerificationReturnValuesDTO getNextQuestionDetails(
            final VerificationDisplayEligibleQuestionWrapperDTO verificationDisplayEligibleQuestionWrapperDTO,
            final String xGUID) {

        ResponseEntity<VerificationReturnValuesDTO> checkAnswerResponse;
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
        HttpEntity<Object> entity;
        entity = new HttpEntity<Object>(verificationDisplayEligibleQuestionWrapperDTO, headers);

        checkAnswerResponse = restTemplate.exchange(
                serviceUrl + "MPLService/accountActivation/getNextQuestionDetails", HttpMethod.POST, entity,
                VerificationReturnValuesDTO.class);
        return checkAnswerResponse.getBody();
    }

    @Override
    public CustomerActivationDTO getCustomerAndPolicyId(final CustomerActivationDTO customerPersonalDetail,
            final String xGUID) {
        ResponseEntity<CustomerActivationDTO> customerPersonalDetailResponse;
        LOGGER.info("{} inside getCustomerAndPolicyId method ", xGUID);
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
        HttpEntity<Object> entity;
        entity = new HttpEntity<Object>(customerPersonalDetail, headers);
        customerPersonalDetailResponse = restTemplate.exchange(
                serviceUrl + "MPLService/accountActivation/getCustomerAndPolicyID", HttpMethod.POST, entity,
                CustomerActivationDTO.class);
        LOGGER.info("{} returning from getCustomerAndPolicyId method ", xGUID);
        return customerPersonalDetailResponse.getBody();
    }

    @Override
    public Boolean checkUsernameExistForDifferentUniqueCustomerNo(final CustomerActivationDTO customerPersonalDetail,
            final String xGUID) {

        LOGGER.info("{} Validating UserName existence ", xGUID);
        ResponseEntity<Boolean> response;
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
        HttpEntity<Object> entity;
        entity = new HttpEntity<Object>(customerPersonalDetail, headers);

        response = restTemplate.exchange(
                serviceUrl + "MPLService/accountActivation/checkUsernameExistForDifferentUniqueCustomerNo", HttpMethod.POST,
                entity, Boolean.class);
        LOGGER.debug("{} Returning result of UserName exists Validation", xGUID);
        return response.getBody();
    }
}
