package uk.co.phoenixlife.service.webclient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dto.activation.PolicyExistLockStatus;
import uk.co.phoenixlife.service.dto.bankvalidation.BankValidationRequest;
import uk.co.phoenixlife.service.dto.bankvalidation.BankValidationResponse;
import uk.co.phoenixlife.service.dto.bankvalidation.BankValidationService;


@Component
public class BankValidationClient implements BankValidationService{

	private static final Logger LOGGER = LoggerFactory.getLogger(BankValidationClient.class);
	
	  @Autowired
	  private RestTemplate restTemplate;
	  
	  @Value("${mpl.service}")
	  private String serviceUrl;
	
	  /*
		 * (non-Javadoc)
		 * 
		 * @see uk.co.phoenixlife.service.dto.bankvalidation.BankValidationService#
		 * validateBankDetails(uk.co.phoenixlife.service.dto.bankvalidation.
		 * BankValidationRequest, java.lang.String)
		 */
		@Override
		public BankValidationResponse validateBankDetails(final BankValidationRequest vocalinkRequest, final String xGUID)
				throws Exception {

			HttpHeaders headers;
			ResponseEntity<BankValidationResponse> response;
			HttpEntity<Object> requestEntity;

			headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(MPLConstants.XGUIDKEYFORAPI, xGUID);

			requestEntity = new HttpEntity<Object>(vocalinkRequest, headers);

			response = restTemplate.exchange(
	                serviceUrl + "MPLService/validatebankdetails/validateaccountnumbersortcode",
	                HttpMethod.POST, requestEntity, BankValidationResponse.class);
			return response.getBody();
		}

	
	
	

}
