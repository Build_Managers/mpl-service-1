package uk.co.phoenixlife.service.webclient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dto.activation.CustomerIdentifierDTO;
import uk.co.phoenixlife.service.dto.activation.CustomerPolicyDetail;
import uk.co.phoenixlife.service.dto.bancspost.BancsCalcCashInTaxOutput;
import uk.co.phoenixlife.service.dto.policy.CashInTypeDTO;
import uk.co.phoenixlife.service.dto.policy.DashBoardResponse;
import uk.co.phoenixlife.service.dto.policy.FinalDashboardDTO;
import uk.co.phoenixlife.service.dto.response.BancsPartyCommDetailsResponse;
import uk.co.phoenixlife.service.dto.response.PoliciesOwnedByAPartyResponseWrapper;
import uk.co.phoenixlife.service.interfaces.DashboardService;

@Component
public class DashBoardWebClient implements DashboardService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DashBoardWebClient.class);

    @Autowired
    private RestTemplate restTemplate;

    @Value("${mpl.service}")
    private String serviceUrl;

    private static final String POLICYDETAILAPI = "MPLService/dashboard/policiesDetails?policyNumber={policyNumber}";
    private static final String ENCASHMENTDASHBOARDAPI = "MPLService/dashboard/encashmentDashboard";

    @Override
    public PoliciesOwnedByAPartyResponseWrapper policiesOwnedByParty(final String uniqueCustomerNumber, final String xGUID)
            throws Exception {

        ResponseEntity<String> response;
        ObjectMapper mapper = new ObjectMapper();
        PoliciesOwnedByAPartyResponseWrapper policiesOwnedByAPartyResponseWrapper = null;
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
        HttpEntity<String> entity;
        entity = new HttpEntity<String>(headers);

        response = restTemplate.exchange(serviceUrl + "MPLService/dashboard/policiesOwnedByParty/{uniqueCustomerNumber}",
                HttpMethod.GET, entity, String.class, uniqueCustomerNumber);
        try {
            policiesOwnedByAPartyResponseWrapper = mapper.readValue(response.getBody(),
                    PoliciesOwnedByAPartyResponseWrapper.class);
        } catch (Exception exc) {

        }
        return policiesOwnedByAPartyResponseWrapper;
    }

    @Override
    public CustomerPolicyDetail getpolicyDetailValuationPremiumSum(final String policyNumber, final String xGUID) {
        // TODO Auto-generated method stub
        CustomerPolicyDetail customerPolicyDetail = null;
        ResponseEntity<String> response = null;
        try {
            HttpHeaders headers;
            headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
            HttpEntity<String> entity;
            entity = new HttpEntity<String>(headers);
            response = restTemplate.exchange(serviceUrl + POLICYDETAILAPI, HttpMethod.GET, entity, String.class,
                    policyNumber);
            final ObjectMapper mapper = new ObjectMapper();
            customerPolicyDetail = mapper.readValue(response.getBody(), CustomerPolicyDetail.class);
        } catch (final Exception exception) {
            // LOGGER.error("{} FAILED BancsPoliciesDetails Exception trying to
            // map with ErrorResponse{}", xGUID, exception);
            // final ErrorResponseDTO error =
            // mapper.readValue(response.getBody(), ErrorResponseDTO.class);
            // LOGGER.error("{} FAILED BancsPoliciesDetails Received Error
            // Response{}:: ", xGUID, error);
        }
        return customerPolicyDetail;

    }

    @Override
    public BancsCalcCashInTaxOutput getBancsCalcCashInTaxOutput(final CashInTypeDTO cashInType, final String xGUID)
            throws Exception {

        HttpHeaders headers;
        HttpEntity<CashInTypeDTO> requestEntity;
        ResponseEntity<BancsCalcCashInTaxOutput> responseEntity;

        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);

        requestEntity = new HttpEntity<CashInTypeDTO>(cashInType, headers);

        LOGGER.info("X-GUID {} :: API Request -> BancsCalcCashInTax");

        responseEntity = restTemplate.exchange(serviceUrl
                + "MPLService/dashboard/calculateTax",
                HttpMethod.POST, requestEntity, BancsCalcCashInTaxOutput.class);

        return responseEntity.getBody();
    }

    @Override
    public BancsCalcCashInTaxOutput getSyncBancsCalcCashInTaxOutput(final CashInTypeDTO cashInType, final String xGUID)
            throws Exception {

        HttpHeaders headers;
        HttpEntity<CashInTypeDTO> request;
        ResponseEntity<BancsCalcCashInTaxOutput> responseEntity;

        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLConstants.XGUIDKEYFORAPI, xGUID);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);

        request = new HttpEntity<CashInTypeDTO>(cashInType, headers);

        LOGGER.info("X-GUID {} :: API Request -> BancsCalcCashInTax");

        responseEntity = restTemplate.exchange(serviceUrl
                + "MPLService/dashboard/calculateTax/",
                HttpMethod.POST, request, BancsCalcCashInTaxOutput.class);

        return responseEntity.getBody();
    }

    @Override
    public FinalDashboardDTO getEncashmentDashboard(final CustomerIdentifierDTO customerData, final String xGUID)
            throws Exception {
        // TODO Auto-generated method stub
        FinalDashboardDTO finalDashboardDTO = null;
        ResponseEntity<String> response = null;
        HttpHeaders headers;
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
        HttpEntity<Object> entity;
        entity = new HttpEntity<Object>(customerData, headers);
        response = restTemplate.exchange(serviceUrl + ENCASHMENTDASHBOARDAPI, HttpMethod.POST, entity, String.class);
        if (response.getBody() != null) {
            final ObjectMapper mapper = new ObjectMapper();
            finalDashboardDTO = mapper.readValue(response.getBody(), FinalDashboardDTO.class);
        }
        return finalDashboardDTO;
    }

    @Override
    public DashBoardResponse getUniqueCustomerNo(final String userName, final String xGUID) throws Exception {

        ResponseEntity<DashBoardResponse> response;
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
        HttpEntity<Object> entity;
        entity = new HttpEntity<Object>(headers);
        response = restTemplate.exchange(serviceUrl + "MPLService/dashboard/getCustomerDetail/{userName}",
                HttpMethod.GET, entity, DashBoardResponse.class, userName);
        return response.getBody();
    }

    @Override
    public BancsPartyCommDetailsResponse getPartyCommDetailsResponse(final String bancsUniqueCustNo, final String xGUID)
            throws Exception {
        // TODO Auto-generated method stub
        ResponseEntity<String> response;
        ObjectMapper mapper = new ObjectMapper();
        BancsPartyCommDetailsResponse bancsPartyCommDetailsResponse = null;
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
        HttpEntity<String> entity;
        entity = new HttpEntity<String>(headers);

        response = restTemplate.exchange(serviceUrl + "MPLService/dashboard/bancsPartyCommDetails/{bancsUniqueCustNo}",
                HttpMethod.GET, entity, String.class, bancsUniqueCustNo);
        try {
            bancsPartyCommDetailsResponse = mapper.readValue(response.getBody(), BancsPartyCommDetailsResponse.class);
        } catch (Exception exc) {

        }
        return bancsPartyCommDetailsResponse;
    }
}
