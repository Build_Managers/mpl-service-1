/*
 * EncashmentWebClient.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.webclient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dto.activation.CustomerIdentifierDTO;
import uk.co.phoenixlife.service.dto.policy.CustomerDTO;
import uk.co.phoenixlife.service.dto.policy.FinalDashboardDTO;
import uk.co.phoenixlife.service.interfaces.EncashmentService;

/**
 * EncashmentWebClient.java
 */
@Component
public class EncashmentWebClient implements EncashmentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EncashmentWebClient.class);

    @Autowired
    private RestTemplate restTemplate;

    @Value("${mpl.service}")
    private String serviceUrl;

    private static final String ENCASHMENTREQUEST = "MPLService/bancspostcall/";

    @Override
    public void submitEncashmentPolicyDetailsAndBaNCSCalls(final CustomerDTO customerDTO, final String xGUID)
            throws Exception {
        ResponseEntity<String> response = null;
        HttpHeaders headers;
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
        HttpEntity<Object> entity;
        entity = new HttpEntity<Object>(customerDTO, headers);
        response = restTemplate.exchange(serviceUrl + ENCASHMENTREQUEST, HttpMethod.POST, entity, String.class);
    }

    @Override
    public FinalDashboardDTO getEncashmentDashboard(final String bancsUniqueCustNo, final CustomerIdentifierDTO customerData, final String xGUID)
            throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

}
