package uk.co.phoenixlife.service.webclient;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dto.response.PoliciesOwnedByAPartyResponseWrapper;
import uk.co.phoenixlife.service.interfaces.LogoutService;
import uk.co.phoenixlife.service.utils.CommonUtils;

@Component
public class LogoutWebClient implements LogoutService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogoutWebClient.class);

    @Autowired
    private RestTemplate restTemplate;

    @Value("${mpl.service}")
    private String serviceUrl;

    @Override
    public void clearCache(final String xGUID, final List<String> xGUIDBancsPolNumCacheKeyList) {
        LOGGER.info("{} Initiating service call for clearCache", xGUID);
        PoliciesOwnedByAPartyResponseWrapper policiesOwnedByAPartyResponseWrapper = new PoliciesOwnedByAPartyResponseWrapper();
        if (CommonUtils.isobjectNotNull(policiesOwnedByAPartyResponseWrapper)) {
            policiesOwnedByAPartyResponseWrapper.setxGUIDBancsPolNumCacheKeyList(xGUIDBancsPolNumCacheKeyList);
        }
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
        HttpEntity<Object> entity;
        entity = new HttpEntity<Object>(policiesOwnedByAPartyResponseWrapper, headers);
        restTemplate.exchange(serviceUrl + "MPLService/logout/clearCache",
                HttpMethod.POST, entity, void.class);
    }

}
