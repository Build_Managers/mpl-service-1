package uk.co.phoenixlife.service.webclient;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dto.ErrorListDTO;
import uk.co.phoenixlife.service.dto.MPLDropoutDTO;
import uk.co.phoenixlife.service.dto.MiErrorRequestDTO;
import uk.co.phoenixlife.service.dto.compliance.ComplianceDTO;
import uk.co.phoenixlife.service.interfaces.MIComplianceService;
import uk.co.phoenixlife.service.utils.CommonUtils;

@Component
public class MIComplianceWebClient implements MIComplianceService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MIComplianceWebClient.class);
	
	private static final String ERROR_SUBMIT_API = "MPLService/compliance/submitErrorList";

	@Autowired
	private RestTemplate restTemplate;
	
	
	@Value("${mpl.service}")
	private String serviceUrl;

	private MPLDropoutDTO setDropOutData(final List<ErrorListDTO> list,final String xGUID, final String emailAddress) {
		MPLDropoutDTO mplDropoutDTO;
		mplDropoutDTO = new MPLDropoutDTO();
    	final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLConstants.XGUIDKEY, xGUID);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
		try {
			
			if (!CommonUtils.isEmpty(xGUID)) {
				mplDropoutDTO.setSessionId(xGUID);
			} else {
				LOGGER.info("xGUID not set in session because it is null");
			}
			for (ErrorListDTO object : list) {
				if (object.getEmail() == null) {
					mplDropoutDTO.setEmailAddress(emailAddress);
				} else {
					mplDropoutDTO.setEmailAddress(object.getEmail());
				}
				mplDropoutDTO.setPageId(object.getPageId());
				mplDropoutDTO.setTimeSpent(object.getTimeSpent());
				mplDropoutDTO.setNextPageId(object.getNextPageId());
				LOGGER.debug("{} Setting drop out data for Page ID:: {}",mplDropoutDTO.getPageId());
				break;
			}
		} catch (Exception exception) {
			LOGGER.error("{} Exception occurred again at submitErrorList:: {}", exception);
		}
		return mplDropoutDTO;

	}

	@Override
	public void submitErrorList(List<ErrorListDTO> errorList, String xGUID, String emailAddress,
			List<ComplianceDTO> list) {
		HttpHeaders headers;
		MiErrorRequestDTO request;
		headers = new HttpHeaders();
		request = new MiErrorRequestDTO();
		headers.set(MPLConstants.XGUIDKEY, xGUID);
		request.setErrorListDTO(errorList);
		request.setDropOutDTO(setDropOutData(errorList, xGUID, emailAddress));
		request.setList(list);
		final HttpEntity<Object> entity = new HttpEntity<Object>(request, headers);
		try {
			if (CommonUtils.isobjectNotNull(xGUID) && CommonUtils.isobjectNotNull(emailAddress)) {
				LOGGER.debug(" Invoking submitErrorList from PolicyServiceClient");
				
				
				restTemplate.exchange(serviceUrl + ERROR_SUBMIT_API, HttpMethod.POST, entity, Boolean.class);
			}
		} catch (Exception submitErrorException) {
			LOGGER.debug("{} Exception occurred at submitErrorList:: Trying again {}", submitErrorException);
			for (ErrorListDTO element : errorList) {
				LOGGER.debug("pageid :: {}", element.getPageId());
				LOGGER.debug("nextpageid :: {}", element.getNextPageId());
			}
			try {
				restTemplate.exchange(serviceUrl + ERROR_SUBMIT_API, HttpMethod.POST, entity, Boolean.class);
			} catch (Exception exception) {
				LOGGER.debug(" Exception occurred again while retrying submitErrorList :: {}",
						submitErrorException);
			}
		}

	}

}
