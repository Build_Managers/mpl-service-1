package uk.co.phoenixlife.service.webclient;

import java.nio.charset.Charset;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import uk.co.phoenixlife.security.service.dto.ForgotPasswordDto;
import uk.co.phoenixlife.security.service.dto.UserRegistrationDTO;
import uk.co.phoenixlife.security.service.dto.UserRegistrationFormDTO;
import uk.co.phoenixlife.security.service.interfaces.MPLSecurityService;
import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.constants.MPLServiceConstants;

@Component
public class MPLSecurityServiceWebClient implements MPLSecurityService {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${mpl.service}")
    private String serviceUrl;

    public String createHeaders(final String username, final String password) {
        String auth = username + ":" + password;
        byte[] encodedAuth = Base64.encodeBase64(
                auth.getBytes(Charset.forName("US-ASCII")));
        String authHeader = "Basic " + new String(encodedAuth);
        return authHeader;
    }

    @Override
    public void insertUserDetailInLDAP(final UserRegistrationFormDTO userRegistrationFormDTO) {

        HttpEntity<Object> entity;
        entity = new HttpEntity<Object>(userRegistrationFormDTO, getDefaultHeaders());
        restTemplate.exchange(serviceUrl + "MPLService/security/insertUserDetailInLDAP", HttpMethod.POST, entity,
                String.class);
    }

    @Override
    public void updateUserDetailInLDAP(final UserRegistrationFormDTO userRegistrationFormDTO) throws Exception {

        HttpEntity<Object> entity;
        entity = new HttpEntity<Object>(userRegistrationFormDTO, getDefaultHeaders());
        restTemplate.exchange(serviceUrl + "MPLService/security/updateUserDetailInLDAP", HttpMethod.POST, entity,
                String.class);
    }

    @Override
    public UserRegistrationDTO checkUserNameAlreadyExist(final String userName) throws Exception {
        ResponseEntity<UserRegistrationDTO> response;

        HttpEntity<Object> entity;
        entity = new HttpEntity<Object>(getDefaultHeaders());
        response = restTemplate.exchange(serviceUrl + "MPLService/security/checkUserNameAlreadyExist/{userName}",
                HttpMethod.POST, entity, UserRegistrationDTO.class, userName);
        return response.getBody();
    }

    @Override
    public boolean checkEmailAlreadyExist(final UserRegistrationFormDTO userRegistrationFormDTO) throws Exception {
        ResponseEntity<Boolean> response;

        HttpEntity<Object> entity;
        entity = new HttpEntity<Object>(userRegistrationFormDTO, getDefaultHeaders());
        response = restTemplate.exchange(serviceUrl + "MPLService/security/checkEmailAlreadyExist", HttpMethod.POST,
                entity, Boolean.class);
        return response.getBody();
    }

    @Override
    public UserRegistrationFormDTO activateAccount(final String token) throws Exception {

        ResponseEntity<UserRegistrationFormDTO> response;

        HttpEntity<Object> entity;
        entity = new HttpEntity<Object>(getDefaultHeaders());
        response = restTemplate.exchange(serviceUrl + "MPLService/security/validateToken/{actid}", HttpMethod.POST,
                entity, UserRegistrationFormDTO.class, token);
        return response.getBody();
    }

    @Override
    public String fetchSecurityQuestion(final ForgotPasswordDto forgotPasswordDto) throws Exception {
        ResponseEntity<String> response;

        HttpEntity<Object> entity;
        entity = new HttpEntity<Object>(forgotPasswordDto, getDefaultHeaders());

        response = restTemplate.exchange(serviceUrl + "MPLService/security/fetchSecurityQuestion",
                HttpMethod.POST, entity, String.class);
        return response.getBody();
    }

    @Override
    public String submitSecurityAnswer(final ForgotPasswordDto forgotPasswordDto) throws Exception {
        ResponseEntity<String> response;

        HttpEntity<Object> entity;
        entity = new HttpEntity<Object>(forgotPasswordDto, getDefaultHeaders());
        response = restTemplate.exchange(serviceUrl + "MPLService/security/submitSecurityAnswer",
                HttpMethod.POST, entity, String.class);
        return response.getBody();
    }

    @Override
    public String changePassword(final ForgotPasswordDto forgotPasswordDto) throws Exception {
        ResponseEntity<String> response;

        HttpEntity<Object> entity;
        entity = new HttpEntity<Object>(forgotPasswordDto, getDefaultHeaders());
        response = restTemplate.exchange(serviceUrl + "MPLService/security/changePasswordForgot", HttpMethod.POST,
                entity, String.class);
        return response.getBody();
    }


    @Override
    public String changePassUrlValidate(final String token) throws Exception {
        ResponseEntity<String> response;

        HttpEntity<Object> entity;
        entity = new HttpEntity<Object>(getDefaultHeaders());
        response = restTemplate.exchange(serviceUrl + "MPLService/security/changePassUrlValidate/{rpid}",
                HttpMethod.POST, entity, String.class, token);
        return response.getBody();
    }

    @Override
    public void lockAccount(final String userName, final boolean isRegistration) throws Exception {
        // TODO Auto-generated method stub
    }

    @Override
    public String increaseAnswerAttemptCount(final String userName) throws Exception {
        ResponseEntity<String> response;

        HttpEntity<Object> entity;
        entity = new HttpEntity<Object>(getDefaultHeaders());
        response = restTemplate.exchange(serviceUrl + "MPLService/security/increaseAnswerAttemptCount/{userName}",
                HttpMethod.POST, entity, String.class, userName);
        return response.getBody();
    }

    @Override
    public String changePasswordDashboard(final ForgotPasswordDto forgotPasswordDto) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String changeEmailDashboard(final ForgotPasswordDto forgotPasswordDto) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void updateEmailStatusAndLDAPStatus(final String userName, final String status) throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    public UserRegistrationFormDTO ResetAccount(final String token) throws Exception {
        ResponseEntity<UserRegistrationFormDTO> response;

        HttpEntity<Object> entity;
        entity = new HttpEntity<Object>(getDefaultHeaders());
        response = restTemplate.exchange(serviceUrl + "MPLService/security/validateResetToken/{rpid}", HttpMethod.POST,
                entity, UserRegistrationFormDTO.class, token);
        return response.getBody();
    }

    @Override
    public String updateSecurityQuestionInLDAP(final UserRegistrationFormDTO userRegistrationFormDTO) throws Exception {
        final ResponseEntity<String> response;
        HttpEntity<Object> entity;
        entity = new HttpEntity<Object>(userRegistrationFormDTO, getDefaultHeaders());
        response = restTemplate.exchange(serviceUrl + "MPLService/security/updateSecurityQuestionInLDAP",
                HttpMethod.POST, entity, String.class);
        return response.getBody();
    }

    @Override
    public void sendFailureRegMail(final String userName) throws Exception {
        // TODO Auto-generated method stub

    }

    private HttpHeaders getDefaultHeaders() {

        final HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                .getRequest();
        HttpHeaders headers;
        headers = new HttpHeaders();
        headers.set(MPLServiceConstants.XGUID_HEADER, (String) req.getSession().getAttribute(MPLConstants.XGUIDKEY));
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }
@Override
	public String fetchEmailIdFromLDAP(String userName) throws Exception {
		
		ResponseEntity<String> response;
		HttpEntity<Object> entity;
		entity = new HttpEntity<Object>(getDefaultHeaders());
		response = restTemplate.exchange(serviceUrl + "MPLService/security/fetchEmailIdFromLDAP/{userName}",
				HttpMethod.POST, entity, String.class, userName);
		return response.getBody();
	}

@Override
public String fetchDetailsFromLDAP(String userName) throws Exception {
	ResponseEntity<String> response;
	HttpEntity<Object> entity;
	entity = new HttpEntity<Object>(getDefaultHeaders());
	response = restTemplate.exchange(serviceUrl + "MPLService/security/fetchDetailsFromLDAP/{userName}",
			HttpMethod.POST, entity, String.class, userName);
	return response.getBody();
}
}
