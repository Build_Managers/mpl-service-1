/*
 * PDFServiceClient.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.webclient;

import java.awt.Rectangle;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate

;

import uk.co.phoenixlife.service.constants.MPLConstants;
import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.interfaces.PDFService;
import uk.co.phoenixlife.service.dto.pdf.FormRequestDTO;
import uk.co.phoenixlife.service.dto.pdf.FormResponseDTO;
import uk.co.phoenixlife.service.dto.pdf.PdfDataDTO;
import uk.co.phoenixlife.service.dto.policy.DashboardDTO;
import uk.co.phoenixlife.service.dto.policy.IdAndVDataDTO;
import uk.co.phoenixlife.service.dto.policy.PartyPolicyDTO;

/**
 * PDFServiceClient.java
 */
@Component("pdfCreation")
public class PDFServiceWebClient implements PDFService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PDFServiceWebClient.class);

    @Autowired
    private RestTemplate restTemplate;

    @Value("${mpl.service}")
    private String serviceUrl;

    

    @Override
	public byte[] getStaticPdf(String pdfType) throws Exception {
		 LOGGER.debug("Invoking getStaticPdf method of PDFServiceWebClient...");		
		    final HttpHeaders headers = new HttpHeaders();
	        headers.setContentType(MediaType.APPLICATION_JSON);	 
	        
	        ResponseEntity<String> response = null;
	        HttpEntity requestEntity;
	        
	        requestEntity= new HttpEntity<>(headers);
	        
	        response = restTemplate.exchange(new StringBuffer().append(serviceUrl).append("MPLService/pdf/{pdfType}").toString(), HttpMethod.GET, requestEntity, String.class, pdfType);
	        
	        return Base64.getDecoder().decode(response.getBody());
	}
	     @Override
	    public Long insertEligiblePolicyData(final Long customerId, final DashboardDTO eligible, final String xGUID) throws Exception {
	        
		 
		 	final HttpHeaders headers = new HttpHeaders();
	        headers.setContentType(MediaType.APPLICATION_JSON);
	        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
	        final HttpEntity<DashboardDTO> entity = new HttpEntity<DashboardDTO>(eligible, headers);
	        ResponseEntity<Long> response = null;
	        
	        
	        response = restTemplate.exchange(new StringBuffer().append(serviceUrl).append("MPLService/pdf/insertEligiblePolicyData?customerId="
	        + customerId).toString(), HttpMethod.POST, entity, Long.class);
	        return response.getBody();
	    }
	@Override
	public Boolean showRetirementPack(DashboardDTO dashboardDTO, String xGUID) {
		final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
        final HttpEntity<DashboardDTO> entity = new HttpEntity<DashboardDTO>(dashboardDTO, headers);
        ResponseEntity<Boolean> response = null;
        response = restTemplate.exchange(new StringBuffer().append(serviceUrl).append("MPLService/pdf/showRetirementPack").toString(), HttpMethod.POST, entity, Boolean.class);
        return response.getBody();
	
	}
	@Override
    public String generatePdf(final long policyId, final String docType, final String xGUID) throws Exception {
        LOGGER.info("{} Invoking generatePdf of RetirementPackServiceClient --->>>");
        HttpHeaders headers;
        final FormRequestDTO request = new FormRequestDTO();
        FormResponseDTO formResponseObject;
        ResponseEntity<FormResponseDTO> response = null;
        request.setPolicyId(policyId);
        request.setDocType(docType);
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
        final HttpEntity<Object> entity = new HttpEntity<Object>(request, headers);
        response = restTemplate.exchange(new StringBuffer().append(serviceUrl).append("MPLService/pdf/generatePdf").toString(),
            HttpMethod.POST, entity, FormResponseDTO.class);
        formResponseObject = response.getBody();
        return formResponseObject.getPdfAsHtml();

    }
	@Override
	public byte[] merge(List<ByteArrayInputStream> inputPdfList, ByteArrayOutputStream baos1) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public byte[] doMerge() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public byte[] mergeSign(String string) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Boolean savePdfContentToDB(FormRequestDTO formRequestDTO, String xGUID) {
		// TODO Auto-generated method stub
		final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
        final HttpEntity<FormRequestDTO> entity = new HttpEntity<FormRequestDTO>(formRequestDTO, headers);
        ResponseEntity<Boolean> response = null;     
        response = restTemplate.exchange(new StringBuffer().append(serviceUrl).append("MPLService/pdf/savePdfContentToDB").toString(), HttpMethod.POST, entity, Boolean.class);
        return response.getBody();
	}
	@Override
	public byte[] generateRetirementPackPdf(PdfDataDTO pdfData, String xGUID) throws Exception {
		// TODO Auto-generated method stub
				final HttpHeaders headers = new HttpHeaders();
		        headers.setContentType(MediaType.APPLICATION_JSON);
		        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
		        final HttpEntity<PdfDataDTO> entity = new HttpEntity<PdfDataDTO>(pdfData, headers);
		        ResponseEntity<byte[]> response = null;     
		        response = restTemplate.exchange(new StringBuffer().append(serviceUrl).append("MPLService/pdf/generateRetirementPackPdf").toString(), HttpMethod.POST, entity, byte[].class);
		        return response.getBody();
	}
	@Override
	public byte[] generateApplicationPdf(byte[] decodedPDF, String xGUID) throws Exception {
		final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
        PdfDataDTO pdfData;
        pdfData = new PdfDataDTO();
        pdfData.setPdfByte(decodedPDF);
        final HttpEntity<PdfDataDTO> entity = new HttpEntity<PdfDataDTO>(pdfData,headers);
        ResponseEntity<byte[]> response = null;     
        response = restTemplate.exchange(new StringBuffer().append(serviceUrl).append("MPLService/pdf/generateApplicationPdf").toString(), HttpMethod.POST, entity, byte[].class);
        return response.getBody();
	}
	@Override
	public PartyPolicyDTO getEligiblePolicyIdvData(DashboardDTO eligible, String xGUID) throws Exception {
		LOGGER.info("{} Invoking getEligiblePolicyIdvData of RetirementPackServiceClient --->>>", xGUID);
        HttpHeaders headers;
        ResponseEntity<PartyPolicyDTO> response = null;
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
        final HttpEntity<DashboardDTO> entity = new HttpEntity<DashboardDTO>(eligible,headers);
        response = restTemplate.exchange(new StringBuffer().append(serviceUrl).append("MPLService/pdf/getEligiblePolicyIdvData").toString(),
            HttpMethod.POST, entity, PartyPolicyDTO.class);
        response.getBody();
        return response.getBody();

	}
	@Override
	public Long checkCustomerPolicyExistance(Long customerId, DashboardDTO eligible, String xGUID) throws Exception {
		final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
        final HttpEntity<DashboardDTO> entity = new HttpEntity<DashboardDTO>(eligible, headers);
        ResponseEntity<Long> response = null;
        
        
        response = restTemplate.exchange(new StringBuffer().append(serviceUrl).append("MPLService/pdf/checkCustomerPolicyExistance?customerId="
        + customerId).toString(), HttpMethod.POST, entity, Long.class);
        return response.getBody();
	}
	
	
}