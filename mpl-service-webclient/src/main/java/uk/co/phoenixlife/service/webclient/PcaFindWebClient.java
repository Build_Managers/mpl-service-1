package uk.co.phoenixlife.service.webclient;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import uk.co.phoenixlife.service.dto.activation.PolicyExistLockStatus;
import uk.co.phoenixlife.service.dto.pca.find.CaptureInteractiveFindV100;
import uk.co.phoenixlife.service.dto.pca.find.CaptureInteractiveFindV100ArrayOfResults;
import uk.co.phoenixlife.service.dto.pca.find.CaptureInteractiveFindV100Results;
import uk.co.phoenixlife.service.interfaces.PcaFindClientService;
import uk.co.phoenixlife.service.utils.CommonUtils;

@Component
public class PcaFindWebClient implements PcaFindClientService {
	
    /** The key. */
    private String key;
   		
    
		@Autowired
	    private RestTemplate restTemplate;

		@Value("${mpl.service}")
	    private String serviceUrl;
		
		

		/*@Override
		 public void getAddereelist(final String postcode) {
	        List<CaptureInteractiveFindV100Results> listOfResults;

	        listOfResults = new ArrayList<CaptureInteractiveFindV100Results>();

	        CaptureInteractiveFindV100 findRequest = new CaptureInteractiveFindV100();

	        findRequest.setKey(key);
	        findRequest.setText(postcode);
	        findRequest.setCountries("GB");
	        findRequest.setLanguage("en");

	        //LOGGER.info(" --->>> Calling findAddress() method");

	        listOfResults = addressFindService.findAddress(findRequest, listOfResults);
			
			//LOGGER.info("Addr. Service Obj : {}", addressFindService.toString());
			
	        if (CommonUtils.isobjectNotNull(listOfResults)) {
	            super.captureInteractiveFindV100Results = listOfResults;
	        }
	        //return this;
	    }

	    /**
	     * Sets the capture interactive find V 100 results.
	     *
	     * @param results
	     *            the new capture interactive find V 100 results
	     
	    public void setCaptureInteractiveFindV100Results(
	            final List<CaptureInteractiveFindV100Results> results) {
	        captureInteractiveFindV100Results = results;
	    }
*/
		@Override
		public CaptureInteractiveFindV100ArrayOfResults find(final String postCode) {
			// TODO Auto-generated method stub
			
			    ResponseEntity<CaptureInteractiveFindV100ArrayOfResults> captureInteractiveFindV100ArrayOfResults;
		        final HttpHeaders headers = new HttpHeaders();
		        headers.setContentType(MediaType.APPLICATION_JSON);
		        HttpEntity<String> entity;
		        entity = new HttpEntity<String>(headers);

		        captureInteractiveFindV100ArrayOfResults = restTemplate.exchange(
		                serviceUrl + "MPLService/pcaService/find/{postCode}",
		                HttpMethod.GET, entity, CaptureInteractiveFindV100ArrayOfResults.class, postCode);
		        return captureInteractiveFindV100ArrayOfResults.getBody();
		}
	}


