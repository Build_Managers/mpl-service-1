package uk.co.phoenixlife.service.webclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100;
import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100ArrayOfResults;
import uk.co.phoenixlife.service.interfaces.PcaRetriveClientService;

@Component
public class PcaRetriveWebClient implements PcaRetriveClientService  {
	
	
	@Autowired
	private RestTemplate restTemplate;

	@Value("${mpl.service}")
    private String serviceUrl;

	@Override
	public CaptureInteractiveRetrieveV100ArrayOfResults retrieve ( final CaptureInteractiveRetrieveV100 retrieveRequest) {
		// TODO Auto-generated method stub
		ResponseEntity<CaptureInteractiveRetrieveV100ArrayOfResults> CaptureInteractiveRetrieveV100ArrayOfResults;
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        
        HttpEntity<CaptureInteractiveRetrieveV100> entity;
        entity = new HttpEntity<CaptureInteractiveRetrieveV100>(retrieveRequest,headers);

        CaptureInteractiveRetrieveV100ArrayOfResults = restTemplate.exchange(
                serviceUrl + "MPLService/pcaRetriveService/retrieve/{retrieveRequest}",
                HttpMethod.POST, entity, CaptureInteractiveRetrieveV100ArrayOfResults.class, retrieveRequest);
        		return CaptureInteractiveRetrieveV100ArrayOfResults.getBody();
        		
		
		
	}



}
