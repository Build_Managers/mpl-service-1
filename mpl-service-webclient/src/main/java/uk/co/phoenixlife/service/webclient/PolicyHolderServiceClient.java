/*
 * PolicyHolderServiceClient.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.webclient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import uk.co.phoenixlife.service.constants.MPLServiceConstants;
import uk.co.phoenixlife.service.dto.policy.CustomerDTO;
import uk.co.phoenixlife.service.dto.response.PartyBasicDetailByPartyNoResponse;
import uk.co.phoenixlife.service.interfaces.PolicyHolderService;

/**
 * PolicyHolderServiceClient.java
 */
@Component
public class PolicyHolderServiceClient implements PolicyHolderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PolicyHolderServiceClient.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @Value("${mpl.service}")
    private String serviceUrl;

    /*
     * @Override public BancsPolicyHoldersResponse getPolicyHoldersDetails(final
     * String bancsUniqueCustNo, final CustomerDTO customerDTO) throws
     * JsonParseException, JsonMappingException, IOException {
     * BancsPolicyHoldersResponse policyHolders = null; ResponseEntity<String>
     * response = null;
     * 
     * try { HttpHeaders headers; headers = new HttpHeaders();
     * headers.setContentType(MediaType.APPLICATION_JSON); //
     * headers.set("customerDTO", customerDTO); HttpEntity<CustomerDTO> entity;
     * entity = new HttpEntity<CustomerDTO>(customerDTO, headers); response =
     * restTemplate.exchange(serviceUrl+
     * "MPLService/policyholder/{bancsUniqueCustNo}/getpolicyholders",
     * HttpMethod.POST, entity, String.class, bancsUniqueCustNo);
     * 
     * // LOGGER.info("{} :: Received BancsPolicyHoldersResponse Response //
     * {}", // response.getStatusCode()); policyHolders =
     * objectMapper.readValue(response.getBody(),
     * BancsPolicyHoldersResponse.class); LOGGER.
     * info("PolicyHolderServiceClient--> bancsPolicyHoldersResponse : {}",
     * policyHolders);
     * 
     * } catch (final Exception exception) { exception.printStackTrace();
     * LOGGER.
     * error("{} FAILED BancsPolicyHoldersResponse Exception trying to map with ErrorResponse{}"
     * , exception); }
     * 
     * return policyHolders; }
     */

    /**
     * @param customerDTO
     * @return (non-Javadoc)
     * @see uk.co.phoenixlife.service.interfaces.PolicyHolderService#updateEmail(uk.co.phoenixlife.service.dto.policy.CustomerDTO)
     */
    @Override
    public Boolean updateEmail(final CustomerDTO customerDTO) {
        // TODO Auto-generated method stub
        LOGGER.info("Inside Webclient update email");
        Boolean updateFlagResponse = false;
        ResponseEntity<String> response = null;
        String bancsUniqueCustNo = customerDTO.getBancsUniqueCustNo();
        LOGGER.info("bancsUniqueCustNo -->{}", bancsUniqueCustNo);
        try {
            HttpHeaders headers;
            headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<CustomerDTO> entity;
            entity = new HttpEntity<CustomerDTO>(customerDTO, headers);
            response = restTemplate.exchange(serviceUrl + "MPLService/policyholder/{bancsUniqueCustNo}/updateemail",
                    HttpMethod.POST, entity, String.class, bancsUniqueCustNo);
            LOGGER.info("Inside Web Client after response");
            LOGGER.info("{} :: Received BancsPolicyHoldersResponse Response  {}", response.getStatusCode());

            HttpStatus status = response.getStatusCode();
            if (status == HttpStatus.OK) {
                updateFlagResponse = true;

            } else {
                updateFlagResponse = false;
            }
            // policyHolders = objectMapper.readValue(response.getBody(),
            // BancsPolicyHoldersResponse.class);
            LOGGER.info("PolicyHolderServiceClient--> bancsPolicyHoldersResponse : {}", response);

        } catch (final Exception exception) {

            LOGGER.error("FAILED BancsPolicyHoldersResponse Exception trying to map with ErrorResponse{}",
                    exception);
        }

        return updateFlagResponse;
    }

    @Override
    public PartyBasicDetailByPartyNoResponse getPartyBasicDetailByPartyNo(final String bancsUniqueCustNo,
            final String xGUID) {

        // TODO Auto-generated method stub

        // PartyBasicDetailByPartyNoResponse policyHolders = null;
        // ResponseEntity<String> response = null;
        ResponseEntity<PartyBasicDetailByPartyNoResponse> partyBasicDetailByPartyNoResponse = null;

        try {

            final HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set(MPLServiceConstants.XGUID_HEADER, xGUID);
            HttpEntity<String> entity;
            entity = new HttpEntity<String>(headers);

            partyBasicDetailByPartyNoResponse = restTemplate.exchange(
                    serviceUrl + "MPLService/policyholder/{bancsUniqueCustNo}/getPartyBasicDetailByPartyNo",
                    HttpMethod.GET, entity, PartyBasicDetailByPartyNoResponse.class, bancsUniqueCustNo);

        } catch (final Exception exception) {
            exception.printStackTrace();
            LOGGER.error("{} FAILED BancsPolicyHoldersResponse Exception trying to map with ErrorResponse{}",
                    exception);
        }

        return partyBasicDetailByPartyNoResponse.getBody();

    }

    /*
     * @Override public BancsPartyCommDetailsResponse
     * getBancsPartyCommDetails(String bancsUniqueCustNo) { // TODO
     * Auto-generated method stub ResponseEntity<BancsPartyCommDetailsResponse>
     * bancsPartyCommDetailsResponse =null;
     * 
     * try {
     * 
     * final HttpHeaders headers = new HttpHeaders();
     * headers.setContentType(MediaType.APPLICATION_JSON); HttpEntity<String>
     * entity; entity = new HttpEntity<String>(headers);
     * 
     * bancsPartyCommDetailsResponse = restTemplate.exchange(serviceUrl+
     * "MPLService/policyholder/{bancsUniqueCustNo}/getPartyBasicDetailByPartyNo",
     * HttpMethod.GET,entity,BancsPartyCommDetailsResponse.class,
     * bancsUniqueCustNo);
     * 
     * 
     * 
     * } catch (final Exception exception) { exception.printStackTrace();
     * LOGGER.
     * error("{} FAILED BancsPolicyHoldersResponse Exception trying to map with ErrorResponse{}"
     * , exception); }
     * 
     * return bancsPartyCommDetailsResponse.getBody();
     * 
     * }
     */

}