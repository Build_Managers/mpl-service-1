/*
 * PostCodeWebClient.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.webclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import uk.co.phoenixlife.service.dto.pca.find.CaptureInteractiveFindV100ArrayOfResults;
import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100;
import uk.co.phoenixlife.service.dto.pca.retrieve.CaptureInteractiveRetrieveV100ArrayOfResults;
import uk.co.phoenixlife.service.interfaces.PostCodeService;

/**
 * PostCodeWebClient.java
 */
@Component
public class PostCodeWebClient implements PostCodeService {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${mpl.service}")
    private String serviceUrl;

    @Override
    public CaptureInteractiveFindV100ArrayOfResults findAddress(final String postcode) throws Exception {
        ResponseEntity<CaptureInteractiveFindV100ArrayOfResults> captureInteractiveFindV100ArrayOfResults;
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity;
        entity = new HttpEntity<String>(headers);

        captureInteractiveFindV100ArrayOfResults = restTemplate.exchange(
                serviceUrl + "MPLService/postcode/find/{postCode}",
                HttpMethod.GET, entity, CaptureInteractiveFindV100ArrayOfResults.class, postcode);
        return captureInteractiveFindV100ArrayOfResults.getBody();
    }

    @Override
    public CaptureInteractiveRetrieveV100ArrayOfResults retrieveAddress(final CaptureInteractiveRetrieveV100 retrieveRequest)
            throws Exception {
        ResponseEntity<CaptureInteractiveRetrieveV100ArrayOfResults> CaptureInteractiveRetrieveV100ArrayOfResults;
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<CaptureInteractiveRetrieveV100> entity;
        entity = new HttpEntity<CaptureInteractiveRetrieveV100>(retrieveRequest, headers);

        CaptureInteractiveRetrieveV100ArrayOfResults = restTemplate.exchange(
                serviceUrl + "MPLService/postcode/retrieve",
                HttpMethod.POST, entity, CaptureInteractiveRetrieveV100ArrayOfResults.class, retrieveRequest);
        return CaptureInteractiveRetrieveV100ArrayOfResults.getBody();

    }

}
