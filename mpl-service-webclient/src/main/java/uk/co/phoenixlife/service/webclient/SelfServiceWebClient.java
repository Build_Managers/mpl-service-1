/*
 * SelfServiceWebClient.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.webclient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import uk.co.phoenixlife.service.dto.SecurityQuestionDTO;
import uk.co.phoenixlife.service.dto.SecurityQuestionsForAUserDTO;
import uk.co.phoenixlife.service.dto.staticdata.StaticDataRequestDTO;
import uk.co.phoenixlife.service.dto.staticdata.StaticDataResponseDTO;
import uk.co.phoenixlife.service.interfaces.SelfService;



/**
 * SelfServiceWebClient.java
 */
@Component
public class SelfServiceWebClient implements SelfService{
	

    private static final Logger LOGGER = LoggerFactory.getLogger(SelfServiceWebClient.class);

    @Autowired
    private RestTemplate restTemplate;

    @Value("${mpl.service}")
    private String serviceUrl;
    
    /**
     * @param username
     * @return (non-Javadoc)
     * @see uk.co.phoenixlife.service.interfaces.SelfService#getSecurityQuestions(java.lang.String)
     */
	@Override
	public SecurityQuestionDTO getSecurityQuestions(String username) {
	
		LOGGER.debug("Invoking getSecurityQuestions method of SelfServiceWebClient...");		
	    final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);	    
        ResponseEntity<SecurityQuestionDTO> response = null;
        HttpEntity requestEntity;
        
        requestEntity= new HttpEntity<>(headers);
        
        response = restTemplate.exchange(new StringBuffer().append(serviceUrl).append("MPLService/selfService/getQuestions/{username}").toString(), HttpMethod.POST, requestEntity, SecurityQuestionDTO.class, username);
        
        return response.getBody();
		
	}

	@Override
	public boolean checkAnswerOfSecurityQuestion(SecurityQuestionsForAUserDTO dto) {
	
		LOGGER.debug("Invoking getSecurityQuestions method of SelfServiceWebClient...");		
	    final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);	    
        
        ResponseEntity<Boolean> response=null;
        HttpEntity<Object> requestEntity;
        requestEntity = new HttpEntity<Object>(dto, headers);
        
        response = restTemplate.exchange(new StringBuffer().append(serviceUrl).append("MPLService/selfService/checkAnswer").toString(),HttpMethod.POST,  requestEntity, Boolean.class);
        
        return response.getBody();
		
		
		
	}

	
}
