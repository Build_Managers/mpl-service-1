/*
 * StaticDataServiceClient.java
 * Copyright 2017 Phoenix Life.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Phoenix Life ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Phoenix Life.
 */
package uk.co.phoenixlife.service.webclient;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import uk.co.phoenixlife.service.dto.staticdata.StaticDataRequestDTO;
import uk.co.phoenixlife.service.dto.staticdata.StaticDataResponseDTO;
import uk.co.phoenixlife.service.dto.staticdata.StaticDataService;
import uk.co.phoenixlife.service.dto.staticdata.StaticIDVDataDTO;

/**
 * StaticDataServiceClient.java
 */
@Component
public class StaticDataServiceClient implements StaticDataService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StaticDataServiceClient.class);

    @Autowired
    private RestTemplate restTemplate;

    @Value("${mpl.service}")
    private String serviceUrl;

    @Override
    @SuppressWarnings("unchecked")
    public Map<String, String> getStaticDataMap(final String groupId) throws Exception {

        LOGGER.debug("Invoking getStaticDataMap method of StaticDataServiceClient...");
        Map<String, String> staticDataMap;

        staticDataMap = restTemplate.getForObject(
                new StringBuffer(serviceUrl.trim()).append("MPLService/staticdata/{groupId}").toString(),
                Map.class, groupId);

        return staticDataMap;
    }

    @Override
    public String getStaticDataValue(final String groupId, final String key) throws Exception {

        LOGGER.debug("Invoking getStaticDataValue method of StaticDataServiceClient...");

        String staticDataValue;

        staticDataValue = restTemplate.getForObject(
                new StringBuffer(serviceUrl.trim()).append("MPLService/staticdata/{groupId}/{keyName}").toString(),
                String.class, groupId, key);

        return staticDataValue;
    }

    @Override
    public List<StaticIDVDataDTO> getIDVQuestionsData() throws Exception {
        StaticDataRequestDTO request;
        StaticDataResponseDTO response;
        HttpHeaders headers;

        request = new StaticDataRequestDTO();
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        response = restTemplate.postForObject(
                new StringBuffer(serviceUrl.trim()).append("MPLService/staticdata/getidvdata").toString(), request,
                StaticDataResponseDTO.class);

        return response.getStaticIDVDataList();
    }

}
